# Install Make ```choco install make```
```
make help
```
```
"Available targets:"
"  up           - Start the services"
"  up_build     - Start the services and rebuild containers"
"  down         - Stop and remove services and orphaned containers"
"  down_dev     - Stop and remove the development services"
"  down_service - Stop and remove the service services"
"  migrations   - Run database migrations"
```
