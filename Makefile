.PHONY: help up up_build down down_dev down_service migrations

help:
	@echo "Available targets:"
	@echo "  up           - Start the services"
	@echo "  up_build     - Start the services and rebuild containers"
	@echo "  down         - Stop and remove services and orphaned containers"
	@echo "  down_dev     - Stop and remove the development services"
	@echo "  down_service - Stop and remove the service services"

up:
	docker compose -f docker-compose.service.yml up -d
	docker compose -f docker-compose.dev.yml up -d

up_build:
	docker compose -f docker-compose.service.yml up --build -d
	docker compose -f docker-compose.dev.yml up --build -d

down:
	docker compose -f docker-compose.service.yml down --remove-orphans
	docker compose -f docker-compose.dev.yml down --remove-orphans

down_dev:
	docker compose -f docker-compose.dev.yml down --remove-orphans

down_service:
	docker compose -f docker-compose.service.yml down --remove-orphans
