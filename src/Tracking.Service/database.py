from settings import DataBaseSettings
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, async_sessionmaker
from sqlmodel import SQLModel, create_engine

database_settings = DataBaseSettings()

DATABASE_URL = f"postgresql+asyncpg://{database_settings.user}:{database_settings.password}@{database_settings.host}:{database_settings.port}/{database_settings.name}"

engine = AsyncEngine(create_engine(DATABASE_URL, future=True))


async def init_db():
    async with engine.begin() as conn:
        await conn.run_sync(SQLModel.metadata.create_all)


async_session = async_sessionmaker(
    engine, class_=AsyncSession, expire_on_commit=False
)


async def get_async_session() -> AsyncSession:
    async with async_session() as session:
        yield session
