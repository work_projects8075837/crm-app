from json import loads
from typing import Optional, Union
from fastapi import BackgroundTasks
from pydantic import UUID4, BaseModel
from sqlmodel.ext.asyncio.session import AsyncSession
from manager.mixin.utils import Utils
from manager.models import ModelType

from manager.mixin.post import MixinPost
from manager.mixin.patch import MixinPatch
from manager.mixin.hide import HideMixin
from manager.mixin.delete import MixinDelete


class StoreMixin(MixinPost, MixinPatch, HideMixin, MixinDelete):
    @classmethod
    async def store_in_history(
        cls: ModelType,
        entity_history_instance: ModelType,
        model_validator: BaseModel,
        event_type: str,
        user_id: UUID4 | str,
        session: AsyncSession,
        previous_state: Optional[Union[ModelType, Utils]],
        current_state: Optional[Union[ModelType, Utils]]):
        
        if previous_state:
            entity_id = previous_state.id
            previous_state = cls.validate_entity(previous_state, model_validator)

        if current_state:
            entity_id = current_state.id
            current_state = cls.validate_entity(current_state, model_validator)

        data = {
            "previous_state": previous_state,
            "current_state": current_state,
            "event": event_type,
            "user_id": user_id,
            "ticket_id": str(entity_id)
        }

        await entity_history_instance.post(session, data)

    @classmethod
    def validate_entity(cls, target, validator: BaseModel):
        entity_json = validator.model_validate(target).model_dump_json()
        return loads(entity_json)


    @classmethod
    async def persisting_creation(
        cls: Union[ModelType, Utils, MixinPost],
        background_task_manager: BackgroundTasks,
        entity_history_instance: ModelType,
        model_validator: BaseModel,
        session: AsyncSession,
        data: dict,
        user_id: UUID4 | str):

        create_result = await cls.post(session, data)
        background_task_manager.add_task(
            cls.store_in_history,
            event_type="create",
            entity_history_instance=entity_history_instance,
            model_validator=model_validator,
            user_id=user_id,
            session=session,
            previous_state=None,
            current_state=create_result
        )

        return create_result


    @classmethod
    async def persisting_update(
        cls: Union[ModelType, Utils, MixinPatch],
        background_task_manager: BackgroundTasks,
        entity_history_instance: ModelType,
        model_validator: BaseModel,
        session: AsyncSession,
        target_id: UUID4,
        data: dict,
        user_id: UUID4 | str,):


        previous_state = await cls.query(session, id=target_id)
        update_result = await cls.patch(session, target_id, data)

        background_task_manager.add_task(
            cls.store_in_history,
            event_type="update",
            entity_history_instance=entity_history_instance,
            model_validator=model_validator,
            user_id=user_id,
            session=session,
            previous_state=previous_state,
            current_state=update_result
        )

        return update_result


    @classmethod
    async def persisting_hiding(
        cls: Union[ModelType, Utils, HideMixin],
        background_task_manager: BackgroundTasks,
        entity_history_instance: ModelType,
        model_validator: BaseModel,
        session: AsyncSession,
        target_id: UUID4,
        user_id: UUID4 | str,
        is_visible: bool=True,):

        previous_state = await cls.query(session, id=target_id)
        model_hidden = await cls.hide(id=target_id, session=session, is_hide=is_visible)

        background_task_manager.add_task(cls.store_in_history,
            event_type="hiding",
            entity_history_instance=entity_history_instance,
            model_validator=model_validator,
            user_id=user_id,
            session=session,
            previous_state=previous_state,
            current_state=model_hidden
        )

        return model_hidden


    @classmethod
    async def persisting_deletion(
        cls: Union[ModelType, Utils, MixinDelete],
        background_task_manager: BackgroundTasks,
        entity_history_instance: ModelType,
        model_validator: BaseModel,
        session: AsyncSession,
        target_id: UUID4,
        user_id: UUID4 | str):

        previous_state = await cls.query(session, id=target_id)
        deleted_model = await cls.delete(session=session, id=target_id)

        background_task_manager.add_task(cls.store_in_history,
            event_type="delete",
            entity_history_instance=entity_history_instance,
            model_validator=model_validator,
            user_id=user_id,
            session=session,
            previous_state=previous_state,
            current_state=None
        )

        return deleted_model