from typing import Union
from pydantic import UUID4
from sqlalchemy import update
from sqlalchemy.future import select
from sqlmodel import SQLModel
from sqlmodel.ext.asyncio.session import AsyncSession
from manager.mixin.utils import Utils
from manager.models import ModelType


class HideMixin(SQLModel):
    @classmethod
    async def hide(
            cls: Union[ModelType, Utils],
            id: UUID4,
            session: AsyncSession,
            is_hide: bool = True,
    ):
        result = await session.execute(select(cls).filter_by(id=id))
        entity = result.scalars().unique().one_or_none()

        if entity is None:
            raise ValueError(f"Entity with id {id} does not exist")

        await session.execute(
            update(cls)
            .where(cls.id == id)
            .values(is_hide=is_hide)
        )

        entity_id_param = f"{cls.__tablename__.lower()}_id"

        await cls._recursive_hide(session, id, is_hide, entity_id_param)

        await session.commit()

        await session.refresh(entity)

        return entity

    @classmethod
    async def _recursive_hide(
            cls,
            session: AsyncSession,
            id: UUID4,
            is_hide: bool,
            entity_id_param: str,
    ):
        if getattr(cls, entity_id_param, False):
            result = await session.execute(select(cls).filter(getattr(cls, entity_id_param) == id))
            entities = result.scalars().all()

            for entity in entities:
                await session.execute(
                    update(cls)
                    .where(cls.id == entity.id)
                    .values(is_hide=is_hide)
                )
                await cls._recursive_hide(session, entity.id, is_hide, entity_id_param)
