from typing import Optional
from fastapi import HTTPException, status
from sqlalchemy.exc import NoResultFound
from sqlmodel import SQLModel, select, and_
from sqlmodel.ext.asyncio.session import AsyncSession
from manager.models import ModelType


class MixinQuery(SQLModel):
    @classmethod
    async def query(cls: ModelType, session: AsyncSession, model: Optional[ModelType] = None, **kwargs):
        if model is None:
            model = cls
        try:
            query = select(model).filter(
                and_(*[getattr(model, key) == value for key, value in kwargs.items()]))
            result = await session.execute(query)
            entity = result.unique().scalar_one()
            return entity
        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"{cls.__name__} with {kwargs} not found"
            )
        finally:
            await session.close()
