from typing import Union
from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.exc import IntegrityError, DBAPIError, NoResultFound
from sqlmodel.ext.asyncio.session import AsyncSession
from manager.mixin.query import MixinQuery
from manager.mixin.utils import Utils
from manager.models import ModelType


class MixinPost(Utils):
    @classmethod
    async def post(cls: Union[ModelType, MixinQuery, Utils], session: AsyncSession, data: dict):
        data = await cls.filter_unique_ids(data)
        try:
            entity = cls()

            for key, value in data.items():
                if not isinstance(value, list):
                    setattr(entity, key, value)
                else:
                    relationships = []
                    for name in value:
                        relationship_entity = await session.execute(
                            select(getattr(cls, key).property.mapper.class_).filter_by(
                                id=name.get("id"))
                        )
                        related_request_result = relationship_entity.unique().scalar_one_or_none()
                        if related_request_result:
                            relationships.append(related_request_result)

                    setattr(entity, key, relationships)
            session.add(entity)
            await session.commit()
            await session.refresh(entity)
            return entity
        except NoResultFound:
            raise HTTPException(404, "Not found with this parameters")
        except IntegrityError as e:
            errorInfo = str(e.orig)
            raise HTTPException(422, errorInfo.split("\n")[1])
        except DBAPIError as e:
            raise HTTPException(400, "Somethings wrong...")
        finally:
            await session.close()
