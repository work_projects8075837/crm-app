from math import ceil
from fastapi import Request
from typing import List, Union

from sqlalchemy import cast, String
from sqlalchemy.ext.asyncio import AsyncSession
from manager.mixin.mixin_utils import Configurator, DefaultSearcher
from manager.mixin.utils import Utils
from manager.models import ModelType


class MixinGet(Utils):

    @classmethod
    async def get(
            cls: Union[ModelType, Utils],
            session: AsyncSession,
            request: Request,
            page: int,
            offset: int,
            search_schemas=None,
            extra_search_condition=None,
            is_hide: bool = False,
    ) -> dict[str, List]:

        if search_schemas is None:
            search_schemas = {}

        if extra_search_condition is None:
            extra_search_condition = []

        filtering_by = await Configurator().convert_query_to_expresions(
            request=request, search_schemas=search_schemas,
            extra_search_condition=extra_search_condition,
            main_model=cls
        )
        search_mode = await Configurator().get_search_mode(request)
        searcher = DefaultSearcher(model=cls, search_mode=search_mode)
        filtering_by.append(cast(getattr(cls, "is_hide"), String).ilike(f"%{is_hide}%"))
        data = await searcher.searching(filtering_by=filtering_by, session=session, page=page, records_per_page=offset)
        rows = await searcher.count_rows_in_model(filtering_by=filtering_by, session=session)
        next_page, prev_page = await cls.generate_pagination_urls(
            request=request, page=page, offset=offset, row_counts=rows
        )
        pages = ceil(rows / offset)
        return {"next": next_page, "prev": prev_page, "pages": pages, "data": data}
