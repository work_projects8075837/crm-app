from typing import Union
from pydantic import UUID4
from fastapi import HTTPException
from sqlmodel import SQLModel, select
from sqlmodel.ext.asyncio.session import AsyncSession
from sqlalchemy.exc import IntegrityError, DBAPIError, NoResultFound
from manager.mixin.utils import Utils
from manager.models import ModelType


class MixinPatch(SQLModel):
    @classmethod
    async def patch(cls: Union[ModelType, Utils], session: AsyncSession, id: UUID4, data: dict):
        data = await cls.filter_unique_ids(data)
        try:
            entity = (await session.execute(select(cls).filter_by(id=id))).unique().scalar_one()

            for key, value in data.items():
                if not isinstance(value, list):
                    setattr(entity, key, value)
                else:
                    relationships = []
                    for name in value:
                        relationship_entity = await session.execute(
                            select(getattr(cls, key).property.mapper.class_).filter_by(
                                id=name.get("id"))
                        )
                        relationships.append(
                            relationship_entity.unique().scalar_one_or_none())

                    setattr(entity, key, relationships)

            await session.commit()
            await session.refresh(entity)
            return entity
        except NoResultFound:
            raise HTTPException(404, "Not found with this parameters")
        except IntegrityError as e:
            errorInfo = str(e.orig)
            raise HTTPException(422, errorInfo.split("\n")[1])
        except DBAPIError:
            raise HTTPException(400, "Somethings wrong...")
        finally:
            await session.close()
