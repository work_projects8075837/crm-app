from typing import List
from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.exc import IntegrityError, DBAPIError
from sqlmodel import select, func, or_, and_


class DefaultSearcher:
    def __init__(self, model, search_mode: bool = False) -> None:
        self.model = model
        self.search_mode = and_ if search_mode == True else or_

    async def searching(self, filtering_by: List, session: AsyncSession, page: int, records_per_page: int, search_mode: bool=False):
        try:
            stmt = (
                select(self.model)
                .select_from(self.model)
                .offset((page - 1) * records_per_page)
                .limit(records_per_page))

            if len(filtering_by) > 0:
                stmt = stmt.filter(self.search_mode(*filtering_by))

            result = await session.execute(stmt)
            records = result.unique().scalars().all()

            return records.__reversed__()

        
        except (IntegrityError, DBAPIError) as e:
            raise HTTPException(
                status_code=409,
                detail=f"Search exit with conflict"
            )
        finally:
            await session.close()

    async def count_rows_in_model(self, filtering_by: list, session: AsyncSession) -> int:
        try:
            stmt = (
                select(func.count("*"))
                .select_from(self.model))
            if len(filtering_by) > 0:
                stmt = stmt.filter(self.search_mode(*filtering_by))

            result = await session.execute(stmt)
            result = result.unique().scalar()
            return result
        except (IntegrityError, DBAPIError):
            raise HTTPException(
                status_code=409,
                detail=f"Search exit with conflict"
            )
        finally:
            await session.close()
