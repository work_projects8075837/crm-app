from fastapi import Request
from pydantic import BaseModel
from urllib.parse import parse_qsl
from starlette.datastructures import MultiDict
from sqlalchemy import String, cast


class Configurator:
    async def __call__(self, request: Request, search_schemas: dict, extra_search_condition=None):
        if extra_search_condition is None:
            extra_search_condition = []
        return await self.convert_query_to_expresions(request, search_schemas, extra_search_condition)

    async def convert_query_to_expresions(self, request: Request, search_schemas, extra_search_condition=None,
                                          main_model=None):

        if extra_search_condition is None:
            extra_search_condition = []

        filtering_by = []

        queries, models = await self.get_query_params_with_models(request, search_schemas)

        for query_name, query_value in queries.items():
            if query_name in extra_search_condition:
                extra_condition = await self.get_sub_condition(query_name, query_value, extra_search_condition)

                if not extra_condition is None:
                    filtering_by.append(extra_condition)
                continue

            query_name, model = await self.check_start_prefix(query_name, models)

            if not query_name or not model:
                continue

            for separated_value in query_value.split(";"):
                separated_value = separated_value.strip()
                if separated_value != "":
                    query_expression = (
                        await self.form_filtering(
                            column_name=query_name,
                            column_value=separated_value,
                            model=model,
                            main_model=main_model))

                    if query_expression is None:
                        continue

                    filtering_by.append(query_expression)

        return filtering_by

    @staticmethod
    async def get_sub_condition(query_name: str, query_value: str, extra_search_condition, ):
        condition_config_index = extra_search_condition.index(query_name)
        condition_config = extra_search_condition[condition_config_index]
        current_condition = condition_config.get_condition(query_value)

        if current_condition is None:
            return

        return current_condition

    @staticmethod
    async def get_query_params_with_models(request: Request, search_schemas: BaseModel):
        query_settings = request.url.remove_query_params(["page", "records_per_page", "is_hard"])
        queries = MultiDict(parse_qsl(query_settings.query, keep_blank_values=True))._dict
        models = search_schemas.keys()

        return queries, models

    @staticmethod
    async def form_filtering(column_name, column_value, model, main_model):
        try:
            column = getattr(model, column_name)
        except AttributeError:
            return None

        if str(column_value).strip() == "null":
            return (column == None)

        if model != main_model:
            try:
                main = getattr(main_model, model.__tablename__)
            except AttributeError:
                return None

            try:
                return main.has(cast(column, String).ilike(f"%{column_value}%"))
            except:
                return main.any(cast(column, String).ilike(f"%{column_value}%"))

        return cast(column, String).ilike(f"%{column_value}%")

    @staticmethod
    async def check_start_prefix(query_name: str, models: list):
        for model in models:
            if query_name.startswith(model.__tablename__):
                return query_name.removeprefix(f"{model.__tablename__}_"), model

        return None, None

    @staticmethod
    async def get_search_mode(request: Request) -> bool:
        query_parameters = request.url
        query_parameters_as_dict = MultiDict(parse_qsl(query_parameters.query, keep_blank_values=True))._dict
        search_mode = query_parameters_as_dict.get("is_hard")

        if search_mode == "false":
            return False

        return True
