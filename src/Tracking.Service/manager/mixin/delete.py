from typing import Union
from fastapi import HTTPException
from pydantic import UUID4
from sqlmodel import select
from sqlmodel.ext.asyncio.session import AsyncSession
from sqlalchemy.exc import IntegrityError, DBAPIError, NoResultFound

from manager.mixin.query import MixinQuery
from manager.mixin.utils import Utils
from manager.models import Response, ModelType


class MixinDelete(Utils):

    @classmethod
    async def delete(cls: Union[ModelType, Utils, MixinQuery], session: AsyncSession, id: UUID4) -> Response:
        try:
            entity = (await session.execute(select(cls).filter_by(id=id))).unique().scalar_one()
            await session.delete(entity)
            await session.commit()
            return Response(detail=f"{cls.__name__} with id {id} delete")
        except NoResultFound as e:
            raise HTTPException(404, f"Not found with id {id}")
        # except IntegrityError as e:
            # raise HTTPException(422, "Some incorrect data")
        # except DBAPIError as e:
            # raise HTTPException(400, "Server or  network error")

        finally:
            await session.close()
