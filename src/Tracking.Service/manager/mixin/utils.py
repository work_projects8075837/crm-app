from typing import Union, Optional
from fastapi import Request
from sqlalchemy import func
from sqlmodel import SQLModel, select
from sqlmodel.ext.asyncio.session import AsyncSession
from manager.mixin.query import MixinQuery
from manager.models import ModelType


class Utils(SQLModel):
    @staticmethod
    async def filter_unique_ids(data):
        unique_ids = set()
        for key, value in data.items():
            if isinstance(value, list):
                data[key] = [entry for entry in value if
                             entry['id'] not in unique_ids and not unique_ids.add(entry['id'])]
        return data

    @classmethod
    async def process_list_data(cls: Union[ModelType, MixinQuery, "Utils"], session: AsyncSession, data: dict) -> dict:
        data = await cls.filter_unique_ids(data)

        for key, value in data.items():
            if isinstance(value, list):
                relationship = getattr(cls, key).property
                if relationship.uselist:
                    for index, entity in enumerate(value):
                        entity_by_id = await cls.query(session, relationship.mapper.class_, id=entity.get("id"))
                        if entity_by_id:
                            data[key][index] = entity_by_id

        return data

    @staticmethod
    async def generate_pagination_urls(
            request: Request,
            page: int,
            offset: int,
            row_counts: int
    ) -> tuple[Optional[str], Optional[str]]:
        page = max(page, 1)
        # page =
        url = request.url
        base_url = f"{url.scheme}://{url.netloc}{url.path}"
        next_page = f"{base_url}?page={page + 1}&offset={offset}" if row_counts > page * offset else None
        prev_page = f"{base_url}?page={page - 1}&offset={offset}" if page > 1 else None
        return next_page, prev_page

    @classmethod
    async def count_rows_model(cls, session: AsyncSession) -> int:
        try:
            return int((await session.execute(select(func.count()).select_from(cls))).scalar())
        finally:
            await session.close()
