from typing import TypeVar, Optional, Annotated
from pydantic import UUID4, BaseModel
from sqlmodel import SQLModel
from fastapi import Query

SearchType = Annotated[Optional[str], Query(default=None)]

ModelType = TypeVar("ModelType")


class Response(BaseModel):
    detail: str


class EntityRead(SQLModel):
    id: UUID4
    is_hide: Optional[bool]


class EntityReadRequest(SQLModel):
    id: UUID4


class EntityListRead(SQLModel):
    ids: list[UUID4]


def pagination_schemas(read_schema: ModelType):
    class PaginationResponse(BaseModel):
        data: Optional[list[read_schema]]
        next: Optional[str]
        prev: Optional[str]
        pages: int

    PaginationResponse.__name__ = f"{read_schema.__name__}PaginationResponse"
    return PaginationResponse
