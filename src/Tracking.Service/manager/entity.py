from typing import Optional
import uuid
from pydantic import UUID4
from sqlmodel import Field
from manager.mixin.get import MixinGet
from manager.mixin.query import MixinQuery
from manager.mixin.history import StoreMixin


class EntityBase(MixinGet, MixinQuery, StoreMixin):
    """Абстрактный базовый класс, представляющий сущность в базе данных."""

    id: UUID4 = Field(
        default_factory=uuid.uuid4,
        primary_key=True,
        index=True,
        nullable=False,
    )
    is_hide: Optional[bool] = Field(nullable=True, default=False)

    @property
    def identity(self) -> UUID4:
        """
        Возвращает идентификатор сущности.

        Returns: Идентификатор сущности.

        Example:
            entity = Entity()
            entity_id = entity.identity
        """
        return self.id

    def __str__(self) -> str:
        """
        Возвращает строковое представление сущности (идентификатор).

        Returns: Строковое представление сущности.

        Example:
            entity = Entity()
            str_representation = str(entity)
        """
        return f"{self.id}"
