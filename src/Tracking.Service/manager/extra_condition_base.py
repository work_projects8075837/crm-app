class CustomConditionConfig:
    def __init__(self, field: str, label: str, model) -> None:
        self.field = field
        self.label = f"{model.__tablename__}_{label}"

    def __str__(self) -> str:
        return self.label

    def __eq__(self, __value: object) -> bool:
        return self.__str__() == __value


    def get_condition(self, value):
        raise NotImplementedError("Must be implemented")
