from enum import Enum
from fastapi import HTTPException, status, Depends
from sqlmodel import select
from settings import instance_auth
from database import async_session
from manager.models import ModelType
from repository.permission.model import Permission
from repository.user.model import User


class PermissionType(Enum):
    READ = "READ"
    CREATE = "CREATE"
    UPDATE = "UPDATE"
    DELETE = "DELETE"


async def permission(id: str, model: ModelType, name: PermissionType):
    __permission__ = f"{name.value}_{model.__tablename__.upper()}"

    async with async_session() as session:
        try:
            user = (await session.execute(
                select(User)
                .filter(User.id == id, Permission.name == __permission__)
            )).unique().scalar_one_or_none()
            # if user is None:
            #     raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=f"Access denied {__permission__}")
        finally:
            await session.close()


def check(authorize: instance_auth = Depends()) -> instance_auth:
    try:
        authorize.jwt_required()
        return authorize
    except Exception:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token")