import asyncio
from logging.config import fileConfig
from sqlalchemy import pool
from sqlalchemy.engine import Connection
from sqlalchemy.ext.asyncio import async_engine_from_config
from sqlmodel import SQLModel
from alembic import context
from settings import DataBaseSettings

import repository.connection.model
import repository.case.model
import repository.channel.model
import repository.character_problem.model
import repository.comment.model
import repository.configuration.model
import repository.contact.model
import repository.counterparty.model
import repository.direction.model
import repository.file.model
import repository.inventory.model
import repository.job.model
import repository.job_result.model
import repository.manager.model
import repository.permission.model
import repository.phone_number.model
import repository.position.model
import repository.role.model
import repository.sale_point.model
import repository.software.model
import repository.status.model
import repository.status_object.model
import repository.tag.model
import repository.ticket.model
import repository.user.model
import repository.notification.model
import repository.read_status.model
import repository.history.model

config = context.config

if config.config_file_name is not None:
    fileConfig(config.config_file_name)

target_metadata = SQLModel.metadata

section = config.config_ini_section

database_settings = DataBaseSettings()

config.set_section_option(section, "DB_HOST", database_settings.host)
config.set_section_option(section, "DB_PORT", str(database_settings.port))
config.set_section_option(section, "DB_NAME", database_settings.name)
config.set_section_option(section, "DB_USER", database_settings.user)
config.set_section_option(section, "DB_PASS", database_settings.password)


def run_migrations_offline() -> None:
    """
    Запускайте миграции в "автономном" режиме.

    Это настраивает контекст только с помощью URL,
    а не движка, хотя движок и здесь приемлем.
    Пропуская создание движка , нам даже не
    нужен DBAPI для доступности.

    Вызовы context.execute() здесь передают заданную строку на вывод
    скрипта.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def do_run_migrations(connection: Connection) -> None:
    context.configure(connection=connection, target_metadata=target_metadata)

    with context.begin_transaction():
        context.run_migrations()


async def run_async_migrations() -> None:
    """
    В этом сценарии нам нужно создать движок
    и связать соединение с контекстом.
    """

    connectable = async_engine_from_config(
        config.get_section(config.config_ini_section, {}),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    async with connectable.connect() as connection:
        await connection.run_sync(do_run_migrations)

    await connectable.dispose()


def run_migrations_online() -> None:
    """Запускайте миграции в режиме "онлайн"."""

    asyncio.run(run_async_migrations())


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
