from typing import Optional
from pydantic import UUID4
from manager.models import EntityRead
from repository.counterparty.read import CounterpartyRead, CounterpartySalePointList
from repository.file.bond import FileWithUser
from repository.inventory.bond import InventoryWithConnection
from repository.phone_number.read import PhoneNumberRead
from repository.sale_point.base import SalePointBase
from repository.status_object.model import StatusObject
from repository.tag.read import TagRead


class SalePointRead(SalePointBase, EntityRead):
    file: Optional[list[FileWithUser]]
    sale_point_id: Optional[UUID4]
    inventory: Optional[list[InventoryWithConnection]]
    counterparty: Optional[list[CounterpartyRead]]
    status_object: Optional[StatusObject]
    tag: Optional[list["TagRead"]]
    phone_number: Optional[list["PhoneNumberRead"]]


class SalePointListRead(EntityRead):
    name: Optional[str]
    email: Optional[str]
    phone: Optional[str]
    regulations: Optional[str]
    address: Optional[str]
    is_parent: bool
    status: str
    service_finish: Optional[str]
    sale_point_id: Optional[UUID4]
    counterparty: Optional[list[CounterpartySalePointList]]
    status_object: Optional[StatusObject]
    tag: Optional[list["TagRead"]]
    phone_number: Optional[list["PhoneNumberRead"]]
