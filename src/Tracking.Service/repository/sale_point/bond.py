from typing import Optional

from pydantic import UUID4
from sqlalchemy import Column, ForeignKey, UUID
from sqlmodel import Field

from manager.entity import EntityBase
from manager.models import EntityRead
from repository.comment.bond import CommentWithUser
from repository.sale_point.read import SalePointRead, SalePointListRead
from repository.user.read import UserRead


class SalePointCommentAssociation(EntityBase, table=True):
    comment_id: UUID4 = Field(
        sa_column=Column(UUID(as_uuid=True), ForeignKey("comment.id", ondelete="CASCADE"), index=True, default=None,
                         nullable=True))
    sale_point_id: UUID4 = Field(
        sa_column=Column(UUID(as_uuid=True), ForeignKey("sale_point.id", ondelete="CASCADE"), index=True, default=None,
                         nullable=True))


class SalePointTagAssociation(EntityBase, table=True):
    tag_id: UUID4 = Field(
        sa_column=Column(UUID(as_uuid=True), ForeignKey("tag.id", ondelete="CASCADE"), index=True, default=None,
                         nullable=True))
    sale_point_id: UUID4 = Field(
        sa_column=Column(UUID(as_uuid=True), ForeignKey("sale_point.id", ondelete="CASCADE"), index=True, default=None,
                         nullable=True))


class PhoneNumberSalePointAssociation(EntityBase, table=True):
    sale_point_id: UUID4 = Field(
        sa_column=Column(UUID(as_uuid=True), ForeignKey("sale_point.id", ondelete="CASCADE"), default=None,
                         nullable=True))
    phone_number_id: UUID4 = Field(
        sa_column=Column(UUID(as_uuid=True), ForeignKey("phone_number.id", ondelete="CASCADE"), default=None,
                         nullable=True))


class SalePointWithPosition(EntityRead):
    email: Optional[str]
    name: str


class SalePointWithUser(SalePointRead):
    user: Optional[UserRead]


class SalePointListWithUser(SalePointListRead):
    user: Optional[UserRead]


class SalePointWithComment(SalePointWithUser):
    comment: Optional[list[CommentWithUser]]
