from typing import Optional

from sqlmodel import SQLModel, Field


class SalePointBase(SQLModel):
    name: Optional[str] = Field(default=None, nullable=True)
    email: Optional[str] = Field(default=None, nullable=True)
    phone: Optional[str] = Field(unique=True, nullable=True, default=None)
    address: Optional[str] = Field(unique=True, nullable=True, default=None)
    description: Optional[str] = Field(default=None, nullable=True)
    is_parent: bool = Field(default=False)
    regulations: Optional[str] = Field(default=None, nullable=True)
    status: str = Field(default="Не абонент")
    service_finish: Optional[str] = Field(default=None, nullable=True)
