from typing import Optional

from pydantic import UUID4
from sqlmodel import Relationship, Field

from manager.entity import EntityBase
from repository.counterparty.model import SalePointCounterpartyAssociation
from repository.inventory.model import Inventory
from repository.sale_point.base import SalePointBase
from repository.sale_point.bond import SalePointCommentAssociation, SalePointTagAssociation, \
    PhoneNumberSalePointAssociation
from repository.tag.model import Tag


class SalePoint(SalePointBase, EntityBase, table=True):
    __tablename__ = "sale_point"

    file: Optional[list["File"]] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete", "single_parent": True}
    )
    inventory: Optional[list["Inventory"]] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete", "single_parent": True}
    )
    position: Optional[list["Position"]] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete", "single_parent": True}
    )
    sale_point_id: UUID4 = Field(
        foreign_key="sale_point.id",
        default=None,
        nullable=True,
    )
    status_object_id: UUID4 = Field(
        foreign_key="status_object.id",
        index=True, default=None, nullable=True
    )
    status_object: Optional["StatusObject"] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False, "single_parent": True}
    )
    user_id: UUID4 = Field(
        foreign_key="user.id",
        index=True, default=None, nullable=True
    )
    user: Optional["User"] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False, "single_parent": True}
    )
    counterparty: Optional[list["Counterparty"]] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete", "single_parent": True},
        link_model=SalePointCounterpartyAssociation
    )
    comment: Optional[list["Comment"]] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete", "single_parent": True},
        link_model=SalePointCommentAssociation
    )
    tag: Optional[list["Tag"]] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete", "single_parent": True},
        link_model=SalePointTagAssociation
    )
    ticket: "Ticket" = Relationship(back_populates="sale_point")

    phone_number: Optional[list["PhoneNumber"]] = Relationship(
        back_populates="sale_point",
        sa_relationship_kwargs={"lazy": "subquery"},
        link_model=PhoneNumberSalePointAssociation
    )
