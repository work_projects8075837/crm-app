from typing import Optional

from sqlmodel import Field
from pydantic import UUID4

from manager.models import EntityReadRequest
from repository.sale_point.base import SalePointBase


class SalePointCreate(SalePointBase):
    file: Optional[list[EntityReadRequest]] = Field(default=None)
    sale_point_id: Optional[UUID4] = Field(default=None)
    inventory: Optional[list[EntityReadRequest]] = Field(default=None)
    user_id: Optional[UUID4] = Field(default=None)
    counterparty: Optional[list[EntityReadRequest]] = Field(default=None)
    comment: Optional[list[EntityReadRequest]] = Field(default=None)
    status_object_id: Optional[UUID4] = Field(default=None)
    regulations: Optional[str] = Field(default=None)
    tag: Optional[list[EntityReadRequest]] = Field(default=None)
    phone_number: Optional[list[EntityReadRequest]] = Field(default=None)
