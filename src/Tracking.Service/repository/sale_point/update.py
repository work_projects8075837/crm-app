from typing import Optional
from sqlmodel import Field, SQLModel
from pydantic import UUID4
from manager.models import EntityReadRequest


class SalePointUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    email: Optional[str] = Field(default=None)
    phone: Optional[str] = Field(default=None)
    address: Optional[str] = Field(default=None)
    description: Optional[str] = Field(default=None)
    is_parent: Optional[bool] = Field(default=None)
    regulations: Optional[str] = Field(default=None)
    file: Optional[list[EntityReadRequest]] = Field(default=None)
    sale_point_id: Optional[UUID4] = Field(default=None)
    inventory: Optional[list[EntityReadRequest]] = Field(default=None)
    user_id: Optional[UUID4] = Field(default=None)
    counterparty: Optional[list[EntityReadRequest]] = Field(default=None)
    comment: Optional[list[EntityReadRequest]] = Field(default=None)
    status_object_id: Optional[UUID4] = Field(default=None)
    tag: Optional[list[EntityReadRequest]] = Field(default=None)
    status: str = Field(default="Не абонент")
    is_hide: Optional[bool] = Field(default=None)
    phone_number: Optional[list[EntityReadRequest]] = Field(default=None)
