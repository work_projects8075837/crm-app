from sqlmodel import SQLModel


class TagBase(SQLModel):
    name: str
