from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, ForeignKey, UUID

from manager.entity import EntityBase

class TagCharacterProblemAssociation(EntityBase, table=True):
    tag_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("tag.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    character_problem_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("character_problem.id", ondelete="CASCADE"), index=True, default=None, nullable=True))

