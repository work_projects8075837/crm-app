from typing import Optional
from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.sale_point.bond import SalePointTagAssociation
from repository.tag.base import TagBase
from repository.tag.bond import TagCharacterProblemAssociation
from repository.ticket.bond import TagTicketAssociation


class Tag(TagBase, EntityBase, table=True):
    character_problem: Optional[list["CharacterProblem"]] = Relationship(
        back_populates="tag", sa_relationship_kwargs={"cascade": "all"},
        link_model=TagCharacterProblemAssociation
    )
    sale_point: Optional[list["SalePoint"]] = Relationship(
        back_populates="tag", sa_relationship_kwargs={"cascade": "all"},
        link_model=SalePointTagAssociation
    )
    ticket: Optional[list["Ticket"]] = Relationship(
        back_populates="tag", sa_relationship_kwargs={"cascade": "all"},
        link_model=TagTicketAssociation
    )
