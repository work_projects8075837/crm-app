from manager.models import EntityRead
from repository.tag.base import TagBase


class TagRead(TagBase, EntityRead):
    ...
