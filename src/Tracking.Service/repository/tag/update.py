from typing import Optional
from sqlmodel import SQLModel, Field


class TagUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
