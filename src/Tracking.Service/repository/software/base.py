from sqlmodel import SQLModel


class SoftwareBase(SQLModel):
    name: str
    version: str
