from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel


class SoftwareUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    version: Optional[str] = Field(default=None)
    inventory_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
