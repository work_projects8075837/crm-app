from typing import Optional
from pydantic import UUID4
from sqlmodel import Field

from repository.software.base import SoftwareBase


class SoftwareCreate(SoftwareBase):
    inventory_id: Optional[UUID4] = Field(default=None)
