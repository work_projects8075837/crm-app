from typing import Optional

from pydantic import UUID4

from manager.models import EntityRead
from repository.software.base import SoftwareBase


class SoftwareRead(SoftwareBase, EntityRead):
    inventory_id: Optional[UUID4]
