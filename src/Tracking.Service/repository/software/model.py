from pydantic import UUID4
from sqlmodel import Relationship, Field
from manager.entity import EntityBase
from repository.software.base import SoftwareBase


class Software(SoftwareBase, EntityBase, table=True):
    inventory: "Inventory" = Relationship(
        back_populates="software", sa_relationship_kwargs={"lazy": "subquery", }
    )
    inventory_id: UUID4 = Field(foreign_key="inventory.id", index=True, default=None, nullable=True)
