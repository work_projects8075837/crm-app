from typing import Optional, List
from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.notification.base import NotificationBase

class Notification(NotificationBase, EntityBase, table=True):
    __tablename__ = "notification"

    read_status: Optional[List["ReadStatus"]] = Relationship(
        back_populates="notification",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "all"}
    )