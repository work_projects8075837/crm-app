from typing import Optional
from datetime import datetime
from sqlmodel import SQLModel, Field

class NotificationBase(SQLModel):
    model: str
    name: str
    link_id: Optional[str] = Field(default=None, nullable=True)
    description: Optional[str] = Field(default=None, nullable=True)
    created_at: Optional[str] = Field(default=datetime.now().isoformat(), nullable=True)
