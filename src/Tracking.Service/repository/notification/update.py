from typing import Optional
from sqlmodel import Field, SQLModel

class NotificationUpdate(SQLModel):
    model: Optional[str] = Field(default=None)
    name: Optional[str] = Field(default=None)
    description: Optional[str] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
