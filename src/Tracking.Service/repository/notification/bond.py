from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel


class NotificationWithStatusRead(SQLModel):
    id: Optional[UUID4] = Field(None)
    is_read: Optional[bool] = Field(None)
    notification_id: Optional[UUID4] = Field(None)
    user_id: Optional[UUID4] = Field(None)