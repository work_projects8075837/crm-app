from typing import Optional, List
from manager.models import EntityRead

from repository.notification.base import NotificationBase
from repository.notification.bond import NotificationWithStatusRead

class NotificationModelRead(NotificationBase, EntityRead):
    read_status: Optional[List["NotificationWithStatusRead"]]
