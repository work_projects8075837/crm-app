from sqlmodel import SQLModel, Field


class ReadStatusBase(SQLModel):
    is_read: bool = Field(default=False)
