from pydantic import UUID4
from typing import Optional
from sqlmodel import Relationship, Field
from manager.entity import EntityBase
from repository.read_status.base import ReadStatusBase


class ReadStatus(ReadStatusBase, EntityBase, table=True):
    __tablename__ = "read_status"

    notification_id: UUID4 = Field(foreign_key="notification.id", default=None, nullable=True)
    notification: Optional["Notification"] = Relationship(
        back_populates="read_status",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False}
    )
    user_id: UUID4 = Field(default=None, foreign_key="user.id", nullable=True)
    user: Optional["User"] = Relationship(
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False}
    )