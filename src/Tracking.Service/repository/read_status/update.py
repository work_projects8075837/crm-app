from pydantic import UUID4
from typing import Optional
from sqlmodel import Field, SQLModel


class ReadStatusUpdate(SQLModel):
    is_read: Optional[bool] = Field(default=None)
    user_id: Optional[UUID4] = Field(default=None)
    notification_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
