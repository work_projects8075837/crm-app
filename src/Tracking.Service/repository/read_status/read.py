from typing import Optional
from sqlmodel import Field
from manager.models import EntityRead
from repository.read_status.base import ReadStatusBase
from repository.notification.base import NotificationBase
from repository.user.read import UserRead


class NotificationStatusRead(ReadStatusBase, EntityRead):
    user: Optional["UserRead"] = Field(default=None)
    notification: Optional["NotificationBase"] = Field(default=None)
