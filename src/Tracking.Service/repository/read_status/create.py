from pydantic import UUID4
from sqlmodel import Field
from typing import Optional
from repository.read_status.base import ReadStatusBase


class ReadStatusCreate(ReadStatusBase):
    user_id: Optional[UUID4] = Field(default=None)
    notification_id: Optional[UUID4] = Field(default=None)
