from manager.models import EntityRead
from repository.position.base import PositionBase


class PositionRead(PositionBase, EntityRead):
    ...
