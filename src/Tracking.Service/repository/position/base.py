from sqlmodel import SQLModel, Field


class PositionBase(SQLModel):
    is_responsible: bool = Field(default=False)
    position: str = Field(default="Сотрудник")
