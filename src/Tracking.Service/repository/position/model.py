from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, Relationship
from manager.entity import EntityBase
from repository.position.base import PositionBase


class Position(PositionBase, EntityBase, table=True):
    sale_point_id: UUID4 = Field(foreign_key="sale_point.id", index=True, default=None, nullable=True)
    sale_point: Optional["SalePoint"] = Relationship(
        back_populates="position", sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False}
    )
    contact_id: UUID4 = Field(foreign_key="contact.id", index=True, default=None, nullable=True)
    contact: Optional["Contact"] = Relationship(
        back_populates="position", sa_relationship_kwargs={"lazy": "subquery", "cascade": "all"}
    )
