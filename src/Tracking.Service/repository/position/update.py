from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel


class PositionUpdate(SQLModel):
    is_responsible: Optional[bool] = Field(default=None)
    position: Optional[str] = Field(default=None)
    sale_point_id: Optional[UUID4] = Field(default=None)
    contact_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
