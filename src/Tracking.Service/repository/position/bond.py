from typing import Optional
from repository.contact.bond import ContactWithPhoneNumber
from repository.position.read import PositionRead
from repository.sale_point.bond import SalePointWithPosition, SalePointWithComment, SalePointListWithUser


class PositionWithContact(PositionRead):
    contact: Optional[ContactWithPhoneNumber]


class ContactPositionWithSalePointList(SalePointListWithUser):
    ...


class ContactPositionWithSalePoint(SalePointWithComment):
    position: Optional[list[PositionWithContact]]


class PositionWithSalePoint(PositionRead):
    sale_point: Optional[SalePointWithPosition]


class ContactWithPosition(ContactWithPhoneNumber):
    position: Optional[list[PositionWithSalePoint]]
