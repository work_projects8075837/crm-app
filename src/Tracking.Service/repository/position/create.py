from typing import Optional

from pydantic import UUID4
from sqlmodel import Field

from repository.position.base import PositionBase


class PositionCreate(PositionBase):
    sale_point_id: Optional[UUID4] = Field(default=None)
    contact_id: Optional[UUID4] = Field(default=None)