from typing import Optional

from pydantic import UUID4
from sqlmodel import Field

from repository.job.base import JobBase


class JobCreate(JobBase):
    job_id: Optional[UUID4] = Field(default=None)
