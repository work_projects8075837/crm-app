from typing import Optional

from pydantic import UUID4

from manager.models import EntityRead
from repository.job.base import JobBase


class JobRead(JobBase, EntityRead):
    job_id: Optional[UUID4]
