from pydantic import UUID4
from sqlalchemy import UUID, Column, ForeignKey
from sqlmodel import Field

from manager.entity import EntityBase


class JobCharacterProblemAssociation(EntityBase, table=True):
    job_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("job.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    character_problem_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("character_problem.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
