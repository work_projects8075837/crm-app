from typing import Optional

from pydantic import UUID4
from sqlmodel import Field, Relationship

from manager.entity import EntityBase
from repository.job.base import JobBase
from repository.job.bond import JobCharacterProblemAssociation
from repository.job_result.model import JobResult


class Job(JobBase, EntityBase, table=True):
    job_id: UUID4 = Field(foreign_key="job.id", default=None, nullable=True)
    character_problem: Optional[list["CharacterProblem"]] = Relationship(
        back_populates="job", sa_relationship_kwargs={"lazy": "subquery", "cascade": "all"},
        link_model=JobCharacterProblemAssociation
    )
    job_result: "JobResult" = Relationship(back_populates="job")
