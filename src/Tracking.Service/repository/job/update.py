from typing import Optional

from pydantic import UUID4
from sqlmodel import Field, SQLModel


class JobUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    is_parent: Optional[bool] = Field(default=None)
    duration: Optional[int] = Field(default=None)
    job_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
