from typing import Optional

from sqlmodel import SQLModel, Field


class JobBase(SQLModel):
    name: Optional[str] = Field(nullable=True, default=None)
    is_parent: bool = Field(default=False)
    duration: Optional[int] = Field(nullable=True, default=None)
