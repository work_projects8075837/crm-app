from datetime import datetime
from typing import Dict, Optional
from sqlalchemy import JSON, Column
from sqlmodel import SQLModel, Field


class TicketHistoryBase(SQLModel):
    event: str = Field(nullable=True, default=None)
    ticket_id: Optional[str] = Field(nullable=True, default=None)
    created_at: Optional[str] = Field(default=datetime.now().isoformat(), nullable=True)
    previous_state: Optional[Dict] = Field(sa_column=Column(JSON(), nullable=True))
    current_state: Optional[Dict] = Field(sa_column=Column(JSON(), nullable=True))
