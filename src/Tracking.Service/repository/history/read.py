from typing import Optional
from manager.models import EntityRead
from repository.history.base import TicketHistoryBase
from repository.user.read import UserRead


class TicketHistoryRead(TicketHistoryBase, EntityRead):
    user: Optional["UserRead"]
