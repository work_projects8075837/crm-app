from typing import Optional

from pydantic import UUID4
from sqlmodel import Field, Relationship
from manager.entity import EntityBase
from repository.history.base import TicketHistoryBase


class TicketHistory(TicketHistoryBase, EntityBase, table=True):
    __tablename__ = "ticket_history"

    user_id: UUID4 = Field(foreign_key="user.id")
    user: "User" = Relationship(back_populates="ticket_history", sa_relationship_kwargs={"lazy": "subquery"})
