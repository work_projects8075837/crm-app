from typing import Optional
from pydantic import UUID4
from manager.models import EntityRead
from repository.character_problem.base import CharacterProblemBase
from repository.job.read import JobRead
from repository.tag.read import TagRead


class CharacterProblemRead(CharacterProblemBase, EntityRead):
    character_problem_id: Optional[UUID4]
    job: Optional[list["JobRead"]]
    tag: Optional[list["TagRead"]]
