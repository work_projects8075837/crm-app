from typing import Optional

from repository.character_problem.read import CharacterProblemRead
from repository.tag.model import Tag


class CharacterProblemWithTag(CharacterProblemRead):
    tag: Optional[list[Tag]]
