from typing import Optional

from sqlmodel import SQLModel, Field


class CharacterProblemBase(SQLModel):
    name: Optional[str] = Field(default=None, nullable=True)
    sla: Optional[int] = Field(default=None, nullable=True)
    url: Optional[str] = Field(default=None, nullable=True)
    is_parent: bool = Field(default=False)
