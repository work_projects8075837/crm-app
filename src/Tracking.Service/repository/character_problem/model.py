from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, Relationship
from manager.entity import EntityBase
from repository.character_problem.base import CharacterProblemBase
from repository.job.bond import JobCharacterProblemAssociation
from repository.job.model import Job
from repository.tag.bond import TagCharacterProblemAssociation
from repository.tag.model import Tag


class CharacterProblem(CharacterProblemBase, EntityBase, table=True):
    __tablename__ = "character_problem"

    character_problem_id: UUID4 = Field(foreign_key="character_problem.id", default=None, nullable=True)
    tag: Optional[list["Tag"]] = Relationship(
        back_populates="character_problem", sa_relationship_kwargs={"lazy": "subquery"},
        link_model=TagCharacterProblemAssociation
    )
    job: Optional[list["Job"]] = Relationship(
        back_populates="character_problem", sa_relationship_kwargs={"lazy": "subquery"},
        link_model=JobCharacterProblemAssociation
    )
    ticket: "Ticket" = Relationship(back_populates="character_problem")
