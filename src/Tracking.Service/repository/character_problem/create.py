from typing import Optional

from pydantic import UUID4
from sqlmodel import Field

from manager.models import EntityReadRequest
from repository.character_problem.base import CharacterProblemBase


class CharacterProblemCreate(CharacterProblemBase):
    character_problem_id: Optional[UUID4] = Field(default=None)
    tag: Optional[list[EntityReadRequest]] = Field(default=None)
    job: Optional[list[EntityReadRequest]] = Field(default=None)
