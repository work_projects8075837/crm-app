from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel

from manager.models import EntityReadRequest

class CharacterProblemUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    sla: Optional[int] = Field(default=None)
    url: Optional[str] = Field(default=None)
    is_parent: Optional[bool] = Field(default=None)
    character_problem_id: Optional[UUID4] = Field(default=None)
    tag: Optional[list[EntityReadRequest]] = Field(default=None)
    job: Optional[list[EntityReadRequest]] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
