from typing import Optional
from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.direction.base import DirectionBase
from repository.status.bond import StatusDirectionAssociation
from repository.status.model import Status


class Direction(DirectionBase, EntityBase, table=True):
    status: Optional[list["Status"]] = Relationship(
        back_populates="direction", sa_relationship_kwargs={"lazy": "subquery"},
        link_model=StatusDirectionAssociation
    )
    ticket: "Ticket" = Relationship(back_populates="direction")
