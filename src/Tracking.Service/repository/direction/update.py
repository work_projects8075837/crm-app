from typing import Optional
from sqlmodel import Field, SQLModel

from manager.models import EntityReadRequest


class DirectionUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    status: Optional[list[EntityReadRequest]] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
