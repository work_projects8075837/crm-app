from typing import Optional

from sqlmodel import Field

from manager.models import EntityReadRequest
from repository.direction.base import DirectionBase


class DirectionCreate(DirectionBase):
    status: Optional[list[EntityReadRequest]] = Field(default=None)