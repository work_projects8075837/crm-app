from typing import Optional

from manager.models import EntityRead
from repository.direction.base import DirectionBase
from repository.status.read import StatusRead


class DirectionRead(DirectionBase, EntityRead):
    status: Optional[list[StatusRead]]
