from typing import Optional
from sqlmodel import SQLModel, Field

class ChannelUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    multiple: Optional[float] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
