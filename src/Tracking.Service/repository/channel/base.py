from sqlmodel import SQLModel


class ChannelBase(SQLModel):
    name: str
    multiple: float
