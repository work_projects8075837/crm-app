from typing import Optional
from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.channel.base import ChannelBase


class Channel(ChannelBase, EntityBase, table=True):
    ticket: "Ticket" = Relationship(back_populates="channel")
    user: Optional[list["User"]] = Relationship(back_populates="channel")
