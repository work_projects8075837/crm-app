from manager.models import EntityRead
from repository.channel.base import ChannelBase


class ChannelRead(ChannelBase, EntityRead):
    ...
