from typing import Optional
from pydantic import UUID4
from sqlmodel import Field
from repository.configuration.base import ConfigurationBase


class ConfigurationCreate(ConfigurationBase):
    connection_id: Optional[UUID4] = Field(default=None)
