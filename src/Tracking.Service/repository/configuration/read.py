from manager.models import EntityRead
from repository.configuration.base import ConfigurationBase


class ConfigurationRead(ConfigurationBase, EntityRead):
    ...
