from typing import Optional
from sqlmodel import SQLModel, Field


class ConfigurationBase(SQLModel):
    login: Optional[str] = Field(default=None, nullable=True)
    password: Optional[str] = Field(default=None, nullable=True)
    other: Optional[str] = Field(default=None, nullable=True)