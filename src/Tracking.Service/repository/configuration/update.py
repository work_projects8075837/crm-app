from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel

from manager.models import EntityRead


class ConfigurationUpdate(SQLModel):
    login: Optional[str] = Field(default=None)
    password: Optional[str] = Field(default=None)
    other: Optional[str] = Field(default=None)
    connection_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
