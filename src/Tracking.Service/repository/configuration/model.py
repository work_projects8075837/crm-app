from pydantic import UUID4
from sqlmodel import Relationship, Field
from manager.entity import EntityBase
from repository.configuration.base import ConfigurationBase


class Configuration(ConfigurationBase, EntityBase, table=True):
    connection_id: UUID4 = Field(foreign_key="connection.id", index=True, default=None, nullable=True)
    connection: "Connection" = Relationship(
        back_populates="configuration", sa_relationship_kwargs={"lazy": "subquery", "cascade": ""}
    )
