from typing import Optional
from repository.configuration.read import ConfigurationRead
from repository.connection.read import ConnectionRead


class ConfigurationWithConnection(ConfigurationRead):
    connection: Optional[ConnectionRead]
