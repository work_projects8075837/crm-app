from typing import Optional
from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, ForeignKey, UUID
from manager.entity import EntityBase
from repository.configuration.read import ConfigurationRead
from repository.connection.read import ConnectionRead


class ConnectionInventoryAssociation(EntityBase, table=True):
    inventory_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("inventory.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    connection_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("connection.id", ondelete="CASCADE"), index=True, default=None, nullable=True))



class ConnectionWithConfiguration(ConnectionRead):
    configuration: Optional[list[ConfigurationRead]]
