from typing import Optional

from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.configuration.model import Configuration
from repository.connection.base import ConnectionBase
from repository.connection.bond import ConnectionInventoryAssociation
from repository.inventory.model import Inventory


class Connection(ConnectionBase, EntityBase, table=True):
    configuration: Optional[list["Configuration"]] = Relationship(
        back_populates="connection", sa_relationship_kwargs={"lazy": "subquery"}
    )
    inventory: Optional["Inventory"] = Relationship(
        back_populates="connection", sa_relationship_kwargs={"lazy": "subquery"},
        link_model=ConnectionInventoryAssociation
    )
