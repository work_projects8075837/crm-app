from manager.models import EntityRead
from repository.connection.base import ConnectionBase


class ConnectionRead(ConnectionBase, EntityRead):
    ...
