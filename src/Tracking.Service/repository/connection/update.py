from typing import Optional
from sqlmodel import Field, SQLModel

from manager.models import EntityReadRequest


class ConnectionUpdate(SQLModel):
    type: Optional[str] = Field(default=None)
    configuration: Optional[list[EntityReadRequest]] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
