from sqlmodel import SQLModel


class ConnectionBase(SQLModel):
    type: str
