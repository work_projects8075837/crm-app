from typing import Optional
from sqlmodel import Field
from manager.models import EntityReadRequest
from repository.connection.base import ConnectionBase


class ConnectionCreate(ConnectionBase):
    configuration: Optional[list[EntityReadRequest]] = Field(default=None)
