from typing import Optional

from pydantic import UUID4
from sqlmodel import Field

from manager.models import EntityReadRequest
from repository.comment.base import CommentBase


class CommentCreate(CommentBase):
    user_id: Optional[UUID4] = Field(default=None)
    ticket: Optional[list[EntityReadRequest]] = Field(default=None)
