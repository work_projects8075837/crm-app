from typing import Optional

from pydantic import UUID4
from sqlmodel import Field, SQLModel

from manager.models import EntityReadRequest


class CommentUpdate(SQLModel):
    description: Optional[str] = Field(default=None)
    date_create: Optional[str] = Field(default=None)
    date_update: Optional[str] = Field(default=None)
    user_id: Optional[UUID4] = Field(default=None)
    ticket: Optional[list[EntityReadRequest]] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
