from datetime import datetime
from sqlmodel import SQLModel, Field


class CommentBase(SQLModel):
    description: str
    date_create: str = Field(default=datetime.now().isoformat())
    date_update: str = Field(default=datetime.now().isoformat())
