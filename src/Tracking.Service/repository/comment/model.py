from datetime import datetime
from typing import Optional
from pydantic import UUID4
from sqlalchemy import event
from sqlmodel import Field, Relationship
from manager.entity import EntityBase
from repository.comment.base import CommentBase
from repository.comment.bond import CommentTicketAssociation
from repository.sale_point.bond import SalePointCommentAssociation


class Comment(CommentBase, EntityBase, table=True):
    user_id: UUID4 = Field(foreign_key="user.id", index=True, nullable=True, default=None)
    user: "User" = Relationship(
        back_populates="comment", sa_relationship_kwargs={"lazy": "subquery"}
    )
    sale_point: Optional[list["SalePoint"]] = Relationship(back_populates="comment", link_model=SalePointCommentAssociation)
    ticket: Optional[list["Ticket"]] = Relationship(
        link_model=CommentTicketAssociation
    )


@event.listens_for(Comment, 'before_update')
def before_update_listener(mapper, connection, target):
    if isinstance(target, Comment):
        target.date_update = datetime.now().isoformat()
