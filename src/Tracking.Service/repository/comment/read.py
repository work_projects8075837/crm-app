from manager.models import EntityRead
from repository.comment.base import CommentBase


class CommentRead(CommentBase, EntityRead):
    ...
