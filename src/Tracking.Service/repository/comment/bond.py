from typing import Optional
from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, UUID, ForeignKey
from manager.entity import EntityBase
from repository.comment.read import CommentRead
from repository.user.read import UserRead



class CommentTicketAssociation(EntityBase, table=True):
    comment_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("comment.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    ticket_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("ticket.id", ondelete="CASCADE"), index=True, default=None, nullable=True))


class CommentWithUser(CommentRead):
    user: Optional[UserRead]
