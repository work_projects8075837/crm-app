from manager.models import EntityRead
from repository.file.base import FileBase


class FileRead(FileBase, EntityRead):
    ...
