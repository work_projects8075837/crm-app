from pydantic import UUID4
from sqlmodel import Field, Relationship
from manager.entity import EntityBase
from repository.counterparty.model import Counterparty
from repository.file.base import FileBase
from repository.sale_point.model import SalePoint


class File(FileBase, EntityBase, table=True):
    user_id: UUID4 = Field(foreign_key="user.id")
    user: "User" = Relationship(back_populates="file", sa_relationship_kwargs={"lazy": "subquery"})
    counterparty_id: UUID4 = Field(foreign_key="counterparty.id", default=None, nullable=True)
    counterparty: "Counterparty" = Relationship(back_populates="file")
    sale_point_id: UUID4 = Field(foreign_key="sale_point.id", default=None, nullable=True)
    sale_point: "SalePoint" = Relationship(back_populates="file")
    case_id: UUID4 = Field(foreign_key="case.id", default=None, nullable=True)
    case: "Case" = Relationship(back_populates="file")
    ticket_id: UUID4 = Field(foreign_key="ticket.id", default=None, nullable=True)
    ticket: "Ticket" = Relationship(back_populates="file")
