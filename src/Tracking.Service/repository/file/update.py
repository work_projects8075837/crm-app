from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel


class FileUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    path: Optional[str] = Field(default=None)
    created_at: Optional[str] = Field(default=None)
    size: Optional[int] = Field(default=None)
    user_id: Optional[UUID4] = Field(default=None)
    counterparty_id: Optional[UUID4] = Field(default=None)
    sale_point_id: Optional[UUID4] = Field(default=None)
    case_id: Optional[UUID4] = Field(default=None)
    ticket_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
