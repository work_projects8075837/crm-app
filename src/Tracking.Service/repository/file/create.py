from pydantic import UUID4

from repository.file.base import FileBase


class FileCreate(FileBase):
    user_id: UUID4