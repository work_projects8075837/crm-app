from repository.file.read import FileRead
from repository.user.read import UserRead


class FileWithUser(FileRead):
    user: UserRead
