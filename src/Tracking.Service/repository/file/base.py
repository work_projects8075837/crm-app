from datetime import datetime
from sqlmodel import SQLModel, Field


class FileBase(SQLModel):
    name: str
    path: str
    created_at: str = Field(default=datetime.now().isoformat())
    size: int
