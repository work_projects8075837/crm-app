from sqlmodel import SQLModel, Field


class PhoneNumberBase(SQLModel):
    name: str = Field(unique=True)