from typing import Optional
from sqlmodel import Field, SQLModel


class PhoneNumberUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
