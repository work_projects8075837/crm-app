from manager.models import EntityRead
from repository.phone_number.base import PhoneNumberBase


class PhoneNumberRead(PhoneNumberBase, EntityRead):
    ...
