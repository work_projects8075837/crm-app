from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.phone_number.base import PhoneNumberBase
from repository.phone_number.bond import PhoneNumberContactAssociation
from repository.sale_point.bond import PhoneNumberSalePointAssociation


class PhoneNumber(PhoneNumberBase, EntityBase, table=True):
    __tablename__ = "phone_number"

    contact: list["Contact"] = Relationship(
        back_populates="phone_number",
        sa_relationship_kwargs={"cascade": "all"},
        link_model=PhoneNumberContactAssociation
    )
    sale_point: list["SalePoint"] = Relationship(
        back_populates="phone_number",
        sa_relationship_kwargs={"cascade": "all"},
        link_model=PhoneNumberSalePointAssociation
    )
