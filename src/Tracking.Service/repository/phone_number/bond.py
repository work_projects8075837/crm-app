from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, ForeignKey, UUID

from manager.entity import EntityBase


class PhoneNumberContactAssociation(EntityBase, table=True):
    contact_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("contact.id", ondelete="CASCADE"), default=None, nullable=True))
    phone_number_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("phone_number.id", ondelete="CASCADE"), default=None, nullable=True))
