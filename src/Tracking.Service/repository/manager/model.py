from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, Relationship
from manager.entity import EntityBase
from repository.manager.base import ManagerBase


class Manager(ManagerBase, EntityBase, table=True):
    user_id: UUID4 = Field(foreign_key="user.id")
    user: "User" = Relationship(
        back_populates="manager",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False})
    job_result: "JobResult" = Relationship(
        back_populates="manager",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False},
    )
    case: "Case" = Relationship(back_populates="manager")
    ticket_id: UUID4 = Field(foreign_key="ticket.id", nullable=True, default=None)
    ticket: Optional["Ticket"] = Relationship(back_populates="manager", sa_relationship_kwargs={"lazy": "subquery"})
