from typing import Optional
from pydantic import UUID4
from manager.entity import EntityBase
from repository.manager.base import ManagerBase
from repository.user.read import UserRead


class ManagerRead(ManagerBase, EntityBase):
    user: UserRead
    ticket_id: Optional[UUID4]
