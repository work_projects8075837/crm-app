from typing import Optional
from pydantic import UUID4
from sqlmodel import Field
from repository.manager.base import ManagerBase


class ManagerCreate(ManagerBase):
    user_id: UUID4
    ticket_id: Optional[UUID4] = Field(default=None)
