from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel


class ManagerUpdate(SQLModel):
    type: Optional[str] = Field(default=None)
    user_id: Optional[UUID4] = Field(default=None)
    ticket_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)

