from typing import Optional
from pydantic import UUID4
from manager.models import EntityRead
from repository.case.base import CaseBase
from repository.file.bond import FileWithUser
from repository.manager.read import ManagerRead


class CaseRead(CaseBase, EntityRead):
    file: Optional[list[FileWithUser]]
    ticket_id: Optional[UUID4]
    manager: Optional["ManagerRead"]
