from typing import Optional
from sqlmodel import SQLModel, Field


class CaseBase(SQLModel):
    name: str
    finish_date: str
    finish_time: str
    is_default: Optional[bool] = Field(default=False, nullable=True)
    is_completed: bool = Field(default=False)
    comment: Optional[str] = Field(default=None, nullable=True)
