from typing import Optional

from pydantic import UUID4
from sqlmodel import Field
from manager.models import EntityReadRequest
from repository.case.base import CaseBase


class CaseCreate(CaseBase):
    file: Optional[list[EntityReadRequest]] = Field(default=None)
    ticket_id: Optional[UUID4] = Field(default=None)
    manager_id: Optional[UUID4] = Field(default=None)
