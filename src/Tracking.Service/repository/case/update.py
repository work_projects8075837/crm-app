from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel


class CaseUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    finish_date: Optional[str] = Field(default=None)
    finish_time: Optional[str] = Field(default=None)
    is_completed: Optional[bool] = Field(default=None)
    ticket_id: Optional[UUID4] = Field(default=None)
    manager_id: Optional[UUID4] = Field(default=None)
    comment: Optional[str] = Field(default=None)
    is_default: Optional[bool] = Field(default=False)
    is_hide: Optional[bool] = Field(default=None)
