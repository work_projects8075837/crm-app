from typing import Optional
from pydantic import UUID4
from sqlmodel import Relationship, Field
from manager.entity import EntityBase
from repository.case.base import CaseBase


class Case(CaseBase, EntityBase, table=True):
    file: Optional[list["File"]] = Relationship(
        back_populates="case", sa_relationship_kwargs={"lazy": "subquery"}
    )
    ticket_id: UUID4 = Field(foreign_key="ticket.id", default=None, nullable=True)
    ticket: Optional["Ticket"] = Relationship(back_populates="case", sa_relationship_kwargs={"cascade": "", "passive_deletes": False})
    manager_id: UUID4 = Field(foreign_key="manager.id", default=None, nullable=True)
    manager: Optional["Manager"] = Relationship(sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False},
                                                back_populates="case")
