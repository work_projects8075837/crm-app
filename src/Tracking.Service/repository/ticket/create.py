from pydantic import UUID4
from sqlmodel import Field
from typing import Optional, Dict
from manager.models import EntityReadRequest
from repository.ticket.base import TicketBase


class TicketCreate(TicketBase):
    file: Optional[list[EntityReadRequest]] = Field(default=None)
    status_id: Optional[UUID4] = Field(default=None)
    channel_id: Optional[UUID4] = Field(default=None)
    direction_id: Optional[UUID4] = Field(default=None)
    character_problem_id: Optional[UUID4] = Field(default=None)
    contact_id: Optional[UUID4] = Field(default=None)
    sale_point_id: Optional[UUID4] = Field(default=None)
    counterparty_id: Optional[UUID4] = Field(default=None)
    ticket_id: Optional[UUID4] = Field(default=None)
    tag: Optional[list[EntityReadRequest]] = Field(default=None)
    comment: Optional[list[EntityReadRequest]] = Field(default=None)
    job_result: Optional[list[EntityReadRequest]] = Field(default=None)
    case: Optional[list[EntityReadRequest]] = Field(default=None)
    # manager: Optional[list[EntityReadRequest]] = Field(default=None)
    decision: Optional[str] = Field(default=None)
    inventory: Optional[list[EntityReadRequest]] = Field(default=None)