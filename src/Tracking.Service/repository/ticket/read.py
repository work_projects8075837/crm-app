from typing import Dict, List, Optional

from pydantic import UUID4

from manager.models import EntityRead
from repository.case.read import CaseRead
from repository.channel.read import ChannelRead
from repository.character_problem.read import CharacterProblemRead
# from repository.comment.read import CommentRead
from repository.contact.read import ContactRead
from repository.counterparty.read import CounterpartyRead
from repository.direction.read import DirectionRead
from repository.file.bond import FileWithUser
from repository.job_result.read import JobResultRead
from repository.manager.read import ManagerRead
from repository.sale_point.read import SalePointRead
from repository.status.read import StatusRead
from repository.tag.read import TagRead
from repository.inventory.read import InventoryRead
from repository.ticket.base import TicketBase


class TicketRead(TicketBase, EntityRead):
    number: Optional[int]
    file: Optional[list[FileWithUser]]
    status: Optional[StatusRead]
    channel: Optional[ChannelRead]
    direction: Optional[DirectionRead]
    character_problem: Optional[CharacterProblemRead]
    contact: Optional[ContactRead]
    sale_point: Optional[SalePointRead]
    counterparty: Optional[CounterpartyRead]
    ticket_id: Optional[UUID4]
    tag: Optional[list[TagRead]]
    manager: Optional[list[ManagerRead]]
    job_result: Optional[list[JobResultRead]]
    case: Optional[list[CaseRead]]
    inventory: Optional[list[InventoryRead]]
    # comment: Optional[list[CommentRead]]
