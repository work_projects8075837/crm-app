from datetime import datetime
from typing import Optional
from sqlmodel import SQLModel, Field


class TicketBase(SQLModel):
    description: Optional[str] = Field(default=None, nullable=True)
    start_at: Optional[str] = Field(default=datetime.now().isoformat(), nullable=True)
    created_at: Optional[str] = Field(default=datetime.now().isoformat(), nullable=True)
    finished_at: Optional[str] = Field(default=None, nullable=True)
    closed_at: Optional[str] = Field(default=None, nullable=True)
    is_parent: bool = Field(default=False)
    duration_time: Optional[int] = Field(default=None, nullable=True)
    jobs_time: Optional[int] = Field(default=None, nullable=True)
    decision: Optional[str] = Field(nullable=True, default=None)
