from typing import List, Optional
from pydantic import UUID4
from sqlalchemy import UUID, Column, ForeignKey, Integer, Sequence
from sqlmodel import Relationship, Field
from manager.entity import EntityBase
from repository.channel.model import Channel
from repository.character_problem.model import CharacterProblem
from repository.comment.bond import CommentTicketAssociation
from repository.comment.model import Comment
from repository.contact.model import Contact
from repository.job_result.bond import JobResultTicketAssociation
from repository.status.model import Status
from repository.ticket.base import TicketBase
from repository.ticket.bond import TagTicketAssociation, InventoryTicketAssociation
from repository.tag.model import Tag


class Ticket(TicketBase, EntityBase, table=True):
    number: int = Field(sa_column=Column(Integer(), Sequence(name="ticket_number", start=0, increment=1), nullable=True,))
    file: Optional[list["File"]] = Relationship(back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete"})
    status_id: UUID4 = Field(foreign_key="status.id", default=None, nullable=True)
    status: Optional["Status"] = Relationship(back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False})
    channel_id: UUID4 = Field(foreign_key="channel.id", default=None, nullable=True)
    channel: Optional["Channel"] = Relationship(back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False})
    direction_id: UUID4 = Field(foreign_key="direction.id", default=None, nullable=True)
    direction: Optional["Direction"] = Relationship(
        back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "passive_deletes": False}
    )
    character_problem_id: UUID4 = Field(foreign_key="character_problem.id", default=None, nullable=True)
    character_problem: Optional["CharacterProblem"] = Relationship(
        back_populates="ticket",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False}
    )
    contact_id: UUID4 = Field(foreign_key="contact.id", default=None, nullable=True)
    contact: Optional["Contact"] = Relationship(back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False})
    sale_point_id: UUID4 = Field(foreign_key="sale_point.id", default=None, nullable=True)
    sale_point: Optional["SalePoint"] = Relationship(
        back_populates="ticket",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False,},
    )
    counterparty_id: UUID4 = Field(foreign_key="counterparty.id", default=None, nullable=True)
    counterparty: Optional["Counterparty"] = Relationship(
        back_populates="ticket",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False}
    )
    ticket_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey('ticket.id', ondelete='SET NULL'), nullable=True))
    tag: Optional[list["Tag"]] = Relationship(
        back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete"},
        link_model=TagTicketAssociation
    )
    manager: Optional[list["Manager"]] = Relationship(back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery"})
    comment: Optional[list["Comment"]] = Relationship(
        back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete"},
        link_model=CommentTicketAssociation
    )
    job_result: Optional[list["JobResult"]] = Relationship(
        back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete"},
        link_model=JobResultTicketAssociation
    )
    case: Optional[list["Case"]] = Relationship(back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery", "cascade": "delete"})
    inventory: Optional[List["Inventory"]] = Relationship(
        back_populates="ticket", sa_relationship_kwargs={"lazy": "subquery"},
        link_model=InventoryTicketAssociation
    )
    last_user: Optional[str] = Field(default=None, nullable=True)
