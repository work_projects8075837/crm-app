from pydantic import UUID4
from typing import Optional, Dict
from sqlmodel import Field, SQLModel
from manager.models import EntityReadRequest


class TicketUpdate(SQLModel):
    description: Optional[str] = Field(default=None)
    created_at: Optional[str] = Field(default=None)
    start_at: Optional[str] = Field(default=None)
    finished_at: Optional[str] = Field(default=None)
    closed_at: Optional[str] = Field(default=None)
    is_parent: Optional[bool] = Field(default=None)
    duration_time: int = Field(default=None)
    jobs_time: int = Field(default=None)
    file: Optional[list[EntityReadRequest]] = Field(default=None)
    status_id: Optional[UUID4] = Field(default=None)
    channel_id: Optional[UUID4] = Field(default=None)
    direction_id: Optional[UUID4] = Field(default=None)
    character_problem_id: Optional[UUID4] = Field(default=None)
    contact_id: Optional[UUID4] = Field(default=None)
    sale_point_id: Optional[UUID4] = Field(default=None)
    counterparty_id: Optional[UUID4] = Field(default=None)
    ticket_id: Optional[UUID4] = Field(default=None)
    tag: Optional[list[EntityReadRequest]] = Field(default=None)
    comment: Optional[list[EntityReadRequest]] = Field(default=None)
    job_result: Optional[list[EntityReadRequest]] = Field(default=None)
    case: Optional[list[EntityReadRequest]] = Field(default=None)
    # manager: Optional[list[EntityReadRequest]] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
    decision: Optional[str] = Field(default=None)
    inventory: Optional[list[EntityReadRequest]] = Field(default=None)
