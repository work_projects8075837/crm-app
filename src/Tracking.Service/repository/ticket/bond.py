from typing import Optional

from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, ForeignKey, UUID
from manager.entity import EntityBase
from repository.position.bond import ContactWithPosition
from repository.ticket.read import TicketRead


class TagTicketAssociation(EntityBase, table=True):
    tag_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("tag.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    ticket_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("ticket.id", ondelete="CASCADE"), index=True, default=None, nullable=True))


class InventoryTicketAssociation(EntityBase, table=True):
    inventory_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("inventory.id"), index=True, default=None, nullable=True))
    ticket_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("ticket.id"), index=True, default=None, nullable=True))


class TicketWithContact(TicketRead):
    contact: Optional[ContactWithPosition]

