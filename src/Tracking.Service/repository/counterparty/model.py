from typing import Optional
from pydantic import UUID4
from sqlmodel import Relationship, Field
from manager.entity import EntityBase
from repository.counterparty.base import CounterpartyBase
from repository.counterparty.bond import SalePointCounterpartyAssociation
from repository.ticket.model import Ticket


class Counterparty(CounterpartyBase, EntityBase, table=True):
    file: Optional[list["File"]] = Relationship(
        back_populates="counterparty", sa_relationship_kwargs={"lazy": "subquery"}
    )
    sale_point: Optional[list["SalePoint"]] = Relationship(
        back_populates="counterparty", link_model=SalePointCounterpartyAssociation,
        sa_relationship_kwargs={"cascade": "all"}
    )
    counterparty_id: UUID4 = Field(foreign_key="counterparty.id", default=None, nullable=True)
    ticket: "Ticket" = Relationship(back_populates="counterparty")
