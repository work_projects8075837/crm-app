from typing import Optional

from pydantic import UUID4
from sqlmodel import Field, SQLModel

from manager.models import EntityRead


class CounterpartyUpdate(SQLModel):
    tag: Optional[str] = Field(default=None)
    name: Optional[str] = Field(default=None)
    tin: Optional[str] = Field(default=None)
    comment: Optional[str] = Field(default=None)
    is_parent: Optional[bool] = Field(default=None)
    subscription: Optional[str] = Field(default=None)
    file: Optional[list["EntityRead"]] = Field(default=None)
    counterparty_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
