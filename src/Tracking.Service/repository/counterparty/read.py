from typing import Optional
from pydantic import UUID4
from sqlmodel import SQLModel

from manager.models import EntityRead
from repository.counterparty.base import CounterpartyBase
from repository.file.bond import FileWithUser


class CounterpartySalePointList(EntityRead):
    name: str


class CounterpartyRead(CounterpartyBase, EntityRead):
    file: Optional[list[FileWithUser]]
    counterparty_id: Optional[UUID4]
