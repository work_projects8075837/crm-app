from pydantic import UUID4
from sqlalchemy import Column, ForeignKey, UUID
from sqlmodel import Field

from manager.entity import EntityBase


class SalePointCounterpartyAssociation(EntityBase, table=True):
    sale_point_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("sale_point.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    counterparty_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("counterparty.id", ondelete="CASCADE"), index=True, default=None, nullable=True))

