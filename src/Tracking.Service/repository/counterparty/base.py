from typing import Optional
from sqlmodel import SQLModel, Field


class CounterpartyBase(SQLModel):
    tag: Optional[str] = Field(default=None, nullable=True)
    name: Optional[str] = Field(default=None, nullable=True)
    tin: Optional[str] = Field(default=None, nullable=True)
    comment: Optional[str] = Field(default=None, nullable=True)
    is_parent: bool = Field(default=False)
    subscription: Optional[str] = Field(default=None, nullable=True)
