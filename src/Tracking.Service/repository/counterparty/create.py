from typing import Optional

from pydantic import UUID4
from sqlmodel import Field

from manager.models import EntityReadRequest
from repository.counterparty.base import CounterpartyBase


class CounterpartyCreate(CounterpartyBase):
    file: Optional[list[EntityReadRequest]] = Field(default=None)
    counterparty_id: Optional[UUID4] = Field(default=None)
