from typing import Optional
from pydantic import UUID4
from sqlmodel import Field
from manager.entity import EntityBase
from repository.connection.bond import ConnectionWithConfiguration
from repository.inventory.read import InventoryRead
from repository.software.read import SoftwareRead
from sqlalchemy import Column, ForeignKey, UUID


class InventorySoftwareAssociation(EntityBase, table=True):
    inventory_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("inventory.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    software_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("software.id", ondelete="CASCADE"), index=True, default=None, nullable=True))


class InventoryWithSoftware(InventoryRead):
    software: Optional[list[SoftwareRead]]


class InventoryWithConnection(InventoryWithSoftware):
    connection: Optional[list[ConnectionWithConfiguration]]


class InventoryWithSalePoint(InventoryWithConnection):
    sale_point_id: Optional[UUID4]
