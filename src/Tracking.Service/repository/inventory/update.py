from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, SQLModel

from manager.models import EntityReadRequest


class InventoryUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    serial_number: Optional[str] = Field(default=None)
    is_work: Optional[bool] = Field(default=None)
    is_parent: Optional[bool] = Field(default=None)
    inventory_id: Optional[UUID4] = Field(default=None)
    software: Optional[list[EntityReadRequest]] = Field(default=None)
    connection: Optional[list[EntityReadRequest]] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
