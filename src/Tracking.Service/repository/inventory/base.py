from typing import Optional

from sqlmodel import SQLModel, Field


class InventoryBase(SQLModel):
    name: Optional[str] = Field(default=None, nullable=True)
    serial_number: Optional[str] = Field(default=None, nullable=True)
    is_work: bool = Field(default=False)
    is_parent: bool = Field(default=False)
