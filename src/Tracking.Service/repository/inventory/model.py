from typing import List, Optional
from pydantic import UUID4
from sqlmodel import Field, Relationship

from manager.entity import EntityBase
from repository.connection.bond import ConnectionInventoryAssociation
from repository.inventory.base import InventoryBase
from repository.inventory.bond import InventorySoftwareAssociation
from repository.software.model import Software
from repository.ticket.bond import InventoryTicketAssociation


class Inventory(InventoryBase, EntityBase, table=True):
    inventory_id: UUID4 = Field(foreign_key="inventory.id", default=None, nullable=True)
    software: list["Software"] = Relationship(
        back_populates="inventory", sa_relationship_kwargs={"lazy": "subquery"},
        link_model=InventorySoftwareAssociation
    )
    connection: list["Connection"] = Relationship(
        back_populates="inventory", sa_relationship_kwargs={"lazy": "subquery"},
        link_model=ConnectionInventoryAssociation
    )
    sale_point_id: UUID4 = Field(foreign_key="sale_point.id", index=True, default=None, nullable=True)
    sale_point: "SalePoint" = Relationship(back_populates="inventory", sa_relationship_kwargs={"passive_deletes": False})
    ticket: Optional[List["Ticket"]] = Relationship(
        back_populates="inventory", link_model=InventoryTicketAssociation
    )
