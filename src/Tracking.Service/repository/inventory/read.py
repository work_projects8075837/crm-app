from typing import Optional
from pydantic import UUID4
from manager.models import EntityRead
from repository.inventory.base import InventoryBase


class InventoryRead(InventoryBase, EntityRead):
    inventory_id: Optional[UUID4]

