from typing import Optional
from pydantic import UUID4
from sqlmodel import Field
from manager.models import EntityReadRequest
from repository.inventory.base import InventoryBase


class InventoryCreate(InventoryBase):
    inventory_id: Optional[UUID4] = Field(default=None)
    software: Optional[list[EntityReadRequest]] = Field(default=None)
    connection: Optional[list[EntityReadRequest]] = Field(default=None)
