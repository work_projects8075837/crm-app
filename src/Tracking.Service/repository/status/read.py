from manager.models import EntityRead
from repository.status.base import StatusBase


class StatusRead(StatusBase, EntityRead):
    ...
