from typing import Optional
from sqlmodel import Field, SQLModel


class StatusUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    sla: Optional[bool] = Field(default=None)
    color: Optional[str] = Field(default=None)
    background: Optional[str] = Field(default=None)
    preview_link: Optional[str] = Field(default=None)
    is_finish: Optional[bool] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
