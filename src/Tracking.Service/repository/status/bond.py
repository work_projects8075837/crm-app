from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, ForeignKey, UUID

from manager.entity import EntityBase


class StatusDirectionAssociation(EntityBase, table=True):
    status_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("status.id"), index=True, default=None, nullable=True))
    direction_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("direction.id"), index=True, default=None, nullable=True))
