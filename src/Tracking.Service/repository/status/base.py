from typing import Optional

from sqlmodel import SQLModel, Field


class StatusBase(SQLModel):
    name: str
    sla: Optional[bool] = Field(default=True, nullable=True)
    color: str
    background: str
    preview_link: str
    is_finish: Optional[bool] = Field(default=False, nullable=True)
