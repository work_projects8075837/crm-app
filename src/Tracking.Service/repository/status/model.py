from typing import Optional
from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.status.base import StatusBase
from repository.status.bond import StatusDirectionAssociation


class Status(StatusBase, EntityBase, table=True):
    direction: Optional[list["Direction"]] = Relationship(
        back_populates="status", sa_relationship_kwargs={"cascade": "all"},
        link_model=StatusDirectionAssociation
    )
    ticket: "Ticket" = Relationship(back_populates="status")
