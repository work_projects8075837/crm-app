from typing import Optional
from sqlmodel import SQLModel, Field


class UserBase(SQLModel):
    name: str
    email: str = Field(..., unique=True, index=True, nullable=False, max_length=256)
    password: str = Field(..., max_length=256)
    status: Optional[str] = Field(nullable=True, default="free")
    position: Optional[str] = Field(nullable=True, default=None)
    phone: Optional[str] = Field(nullable=True, default=None)
    avatar: Optional[str] = Field(nullable=True, default=None)
    activate: bool = Field(default=True)
    blocking: bool = Field(default=False)
