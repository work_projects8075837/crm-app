from typing import Optional

from manager.models import EntityReadRequest
from repository.user.base import UserBase


class UserCreate(UserBase):
    role: Optional[list[EntityReadRequest]]