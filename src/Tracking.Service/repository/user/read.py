from typing import Optional
from manager.models import EntityRead
from repository.channel.read import ChannelRead
from repository.direction.read import DirectionRead


class UserRead(EntityRead):
    name: str
    email: str
    status: Optional[str]
    position: Optional[str]
    phone: Optional[str]
    avatar: Optional[str]
    activate: bool
    blocking: bool
    channel: Optional[ChannelRead]
    direction: Optional[DirectionRead]