from pydantic import BaseModel


class LoginResponse(BaseModel):
    access: str
    refresh: str


class RefreshResponse(BaseModel):
    access: str


class Recovery(BaseModel):
    email: str


class ResetPassword(BaseModel):
    token: str
    password: str


class UserSignUp(BaseModel):
    name: str
    email: str
    password: str


class UserLogin(BaseModel):
    email: str
    password: str
