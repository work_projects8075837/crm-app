from typing import Optional
from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, ForeignKey, UUID
from manager.entity import EntityBase
from repository.role.bond import RoleWithPermission
from repository.user.read import UserRead


class UserRoleAssociation(EntityBase, table=True):
    role_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("role.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    user_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("user.id", ondelete="CASCADE"), index=True, default=None, nullable=True))

class UserWithRole(UserRead):
    role: Optional[list[RoleWithPermission]]
