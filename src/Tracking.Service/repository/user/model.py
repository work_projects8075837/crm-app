import asyncio
from typing import Optional, Union
import bcrypt
from fastapi import HTTPException, status
from pydantic import UUID4
from sqlalchemy import UUID, Column, ForeignKey, event
from sqlalchemy.exc import IntegrityError
from sqlmodel import Field, Relationship
from sqlmodel.ext.asyncio.session import AsyncSession

from database import async_session
from manager.entity import EntityBase
from manager.mixin.query import MixinQuery
from manager.mixin.utils import Utils
from manager.models import ModelType
from repository.file.model import File
from repository.user.base import UserBase
from repository.user.bond import UserRoleAssociation
from user.handlers.handlers_user import HandlersUser


class User(UserBase, EntityBase, HandlersUser, table=True):
    role: Optional[list["Role"]] = Relationship(
        back_populates="user", link_model=UserRoleAssociation,
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "all"}
    )
    file: Optional[list["File"]] = Relationship(back_populates="user")
    ticket_history: Optional[list["TicketHistory"]] = Relationship(back_populates="user")
    manager: Optional[list["Manager"]] = Relationship(back_populates="user")
    sale_point: Optional[list["SalePoint"]] = Relationship(back_populates="user")
    comment: Optional[list["Comment"]] = Relationship(back_populates="user")
    channel_id: UUID4 = Field(foreign_key="channel.id", index=True, nullable=True, default=None)
    channel: "Channel" = Relationship(back_populates="user", sa_relationship_kwargs={"lazy": "subquery"})
    direction_id: UUID4 = Field(
        sa_column=Column(UUID(as_uuid=True), ForeignKey("direction.id", ondelete="SET NULL"), default=None,
                         nullable=True))
    direction: "Direction" = Relationship(sa_relationship_kwargs={"lazy": "subquery"})

    @classmethod
    async def post(cls: Union[ModelType, MixinQuery, Utils, UserBase], session: AsyncSession, data: dict):
        try:
            entity = cls(**(await cls.process_list_data(session, data)))
            entity.password = bcrypt.hashpw(entity.password.encode(), bcrypt.gensalt()).decode()
            await session.merge(entity, load=True)
            await session.commit()
            return await cls.query(session, id=entity.id)
        except IntegrityError:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail=f"{cls.__name__} with conflict"
            )
        finally:
            await session.close()

    def __str__(self) -> str:
        return f"{self.email}"


utils_user = User


async def add_user(data, id):
    async with async_session() as session:
        return await utils_user.patch(session, id, data)


@event.listens_for(User, 'after_insert')
def after_insert_listener(mapper, connection, target):
    data = {
        "role": [
            {
                "id": "7fde0e09-4996-4453-a4ec-e44d9d42c6a2"
            }
        ]
    }
    asyncio.create_task(add_user(data, target.id))
