from typing import Optional
from sqlmodel import Field, SQLModel
from pydantic import UUID4
from manager.models import EntityReadRequest


class UserUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    email: Optional[str] = Field(default=None)
    password: Optional[str] = Field(default=None)
    status: Optional[str] = Field(default=None)
    position: Optional[str] = Field(default=None)
    phone: Optional[str] = Field(default=None)
    avatar: Optional[str] = Field(default=None)
    activate: Optional[bool] = Field(default=None)
    blocking: Optional[bool] = Field(default=None)
    role: Optional[list[EntityReadRequest]] = Field(default=None)
    channel_id: Optional[UUID4] = Field(default=None)
    direction_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
