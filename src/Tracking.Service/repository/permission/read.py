from manager.models import EntityRead
from repository.permission.base import PermissionBase


class PermissionRead(PermissionBase, EntityRead):
    pass
