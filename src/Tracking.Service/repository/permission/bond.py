from typing import Optional
from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, ForeignKey, UUID
from manager.entity import EntityBase
from repository.permission.read import PermissionRead
from repository.role.read import RoleRead


class PermissionRoleAssociation(EntityBase, table=True):
    role_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("role.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    permission_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("permission.id", ondelete="CASCADE"), index=True, default=None, nullable=True))


class PermissionWithRole(PermissionRead):
    role: Optional[list[RoleRead]]
