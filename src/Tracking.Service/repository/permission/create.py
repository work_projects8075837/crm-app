from typing import Optional

from sqlmodel import Field

from manager.models import EntityReadRequest
from repository.permission.base import PermissionBase


class PermissionCreate(PermissionBase):
    role: Optional[list[EntityReadRequest]] = Field(default=None)
