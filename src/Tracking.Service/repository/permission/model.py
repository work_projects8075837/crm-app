from typing import Optional
from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.permission.base import PermissionBase
from repository.permission.bond import PermissionRoleAssociation
from repository.role.model import Role


class Permission(PermissionBase, EntityBase, table=True):
    role: Optional[list["Role"]] = Relationship(
        back_populates="permissions", link_model=PermissionRoleAssociation,
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "all"}
    )

    def __str__(self) -> str:
        return f"{self.name}"
