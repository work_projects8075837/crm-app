from sqlmodel import SQLModel


class PermissionBase(SQLModel):
    name: str
