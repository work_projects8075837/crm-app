from typing import Optional

from pydantic import UUID4
from sqlmodel import Field, SQLModel


class JobResultUpdate(SQLModel):
    time: Optional[int] = Field(default=None)
    manager_id: Optional[UUID4] = Field(default=None)
    job_id: Optional[UUID4] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
