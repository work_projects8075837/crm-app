from typing import Optional

from pydantic import UUID4
from sqlmodel import Field

from repository.job_result.base import JobResultBase


class JobResultCreate(JobResultBase):
    manager_id: Optional[UUID4] = Field(default=None)
    job_id: Optional[UUID4] = Field(default=None)