from pydantic import UUID4
from sqlmodel import Field
from sqlalchemy import Column, ForeignKey, UUID

from manager.entity import EntityBase


class JobResultTicketAssociation(EntityBase, table=True):
    job_result_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("job_result.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
    ticket_id: UUID4 = Field(sa_column=Column(UUID(as_uuid=True), ForeignKey("ticket.id", ondelete="CASCADE"), index=True, default=None, nullable=True))
