from typing import Optional
from pydantic import UUID4
from sqlmodel import Field, Relationship
from manager.entity import EntityBase
from repository.job_result.base import JobResultBase
from repository.job_result.bond import JobResultTicketAssociation
from repository.manager.model import Manager


class JobResult(JobResultBase, EntityBase, table=True):
    __tablename__ = "job_result"

    manager_id: UUID4 = Field(foreign_key="manager.id", default=None, nullable=True)
    manager: Optional["Manager"] = Relationship(
        back_populates="job_result", 
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False},
    )
    job_id: UUID4 = Field(foreign_key="job.id", default=None, nullable=True)
    job: Optional["Job"] = Relationship(
        back_populates="job_result",
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "", "passive_deletes": False},
    )
    ticket: Optional[list["Ticket"]] = Relationship(
        back_populates="job_result",
        link_model=JobResultTicketAssociation
    )
