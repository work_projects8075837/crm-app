from typing import Optional

from manager.models import EntityRead
from repository.job.read import JobRead
from repository.job_result.base import JobResultBase
from repository.manager.read import ManagerRead


class JobResultRead(JobResultBase, EntityRead):
    manager: Optional[ManagerRead]
    job: Optional[JobRead]
