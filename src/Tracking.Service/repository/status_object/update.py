from typing import Optional

from sqlmodel import Field, SQLModel


class StatusObjectUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
