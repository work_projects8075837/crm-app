from manager.models import EntityRead
from repository.status_object.base import StatusObjectBase


class StatusObjectRead(StatusObjectBase, EntityRead):
    ...
