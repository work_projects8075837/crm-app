from typing import Optional

from sqlmodel import Relationship

from manager.entity import EntityBase
from repository.status_object.base import StatusObjectBase


class StatusObject(StatusObjectBase, EntityBase, table=True):
    __tablename__ = "status_object"

    sale_point: Optional[list["SalePoint"]] = Relationship(back_populates="status_object")
