from typing import Optional
from sqlmodel import Field, SQLModel
from manager.models import EntityReadRequest


class RoleUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    user: Optional[list[EntityReadRequest]] = Field(default=None)
    permissions: Optional[list[EntityReadRequest]] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
