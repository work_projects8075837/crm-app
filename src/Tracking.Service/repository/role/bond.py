from typing import Optional
from repository.permission.read import PermissionRead
from repository.role.read import RoleRead
from repository.user.read import UserRead


class RoleWithPermission(RoleRead):
    permissions: Optional[list[PermissionRead]]


class RoleWithUser(RoleWithPermission):
    user: Optional[list[UserRead]]
