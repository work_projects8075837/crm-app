from typing import Optional
from sqlmodel import Field
from manager.models import EntityRead, EntityReadRequest
from repository.role.base import RoleBase


class RoleRead(RoleBase, EntityRead):
    permissions: Optional[list[EntityReadRequest]] = Field(default=None)
