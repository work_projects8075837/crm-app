from typing import Optional
from sqlmodel import Relationship
from manager.entity import EntityBase
from repository.permission.bond import PermissionRoleAssociation
from repository.role.base import RoleBase
from repository.user.bond import UserRoleAssociation


class Role(RoleBase, EntityBase, table=True):
    user: Optional[list["User"]] = Relationship(
        back_populates="role", link_model=UserRoleAssociation,
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "all"}
    )
    permissions: Optional[list["Permission"]] = Relationship(
        back_populates="role", link_model=PermissionRoleAssociation,
        sa_relationship_kwargs={"lazy": "subquery", "cascade": "all"}
    )

    def __str__(self) -> str:
        return f"{self.name}"
