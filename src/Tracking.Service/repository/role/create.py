from typing import Optional
from sqlmodel import Field
from manager.models import EntityReadRequest
from repository.role.base import RoleBase


class RoleCreate(RoleBase):
    user: Optional[list[EntityReadRequest]] = Field(default=None)
    permissions: Optional[list[EntityReadRequest]] = Field(default=None)
