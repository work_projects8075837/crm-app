from typing import Optional

from sqlmodel import Field

from manager.models import EntityReadRequest
from repository.contact.base import ContactBase


class ContactCreate(ContactBase):
    phone_number: Optional[list[EntityReadRequest]] = Field(default=None)
