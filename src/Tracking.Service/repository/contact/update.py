from typing import Optional
from sqlmodel import Field, SQLModel

from manager.models import EntityReadRequest


class ContactUpdate(SQLModel):
    name: Optional[str] = Field(default=None)
    email: Optional[str] = Field(default=None)
    phone_number: Optional[list[EntityReadRequest]] = Field(default=None)
    is_hide: Optional[bool] = Field(default=None)
