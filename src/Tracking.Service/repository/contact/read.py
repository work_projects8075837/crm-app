from manager.models import EntityRead
from repository.contact.base import ContactBase


class ContactRead(ContactBase, EntityRead):
    ...
