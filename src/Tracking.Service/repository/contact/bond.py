from typing import Optional

from repository.contact.read import ContactRead
from repository.phone_number.read import PhoneNumberRead


class ContactWithPhoneNumber(ContactRead):
    phone_number: Optional[list[PhoneNumberRead]]