from typing import Optional

from sqlmodel import Relationship

from manager.entity import EntityBase
from repository.contact.base import ContactBase
from repository.phone_number.bond import PhoneNumberContactAssociation
from repository.phone_number.model import PhoneNumber
from repository.position.model import Position


class Contact(ContactBase, EntityBase, table=True):
    phone_number: Optional[list["PhoneNumber"]] = Relationship(
        back_populates="contact",
        sa_relationship_kwargs={"lazy": "subquery"},
        link_model=PhoneNumberContactAssociation
    )
    position: Optional[list["Position"]] = Relationship(
        sa_relationship_kwargs={"lazy": "subquery"}
    )
    ticket: "Ticket" = Relationship(back_populates="contact")
