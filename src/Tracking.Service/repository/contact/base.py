from typing import Optional

from sqlmodel import SQLModel, Field


class ContactBase(SQLModel):
    name: str
    email: Optional[str] = Field(default=None, nullable=True)
