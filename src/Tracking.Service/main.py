from os import path, mkdir
import warnings
from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from sqladmin import Admin
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY
from database import engine
from service.router.router import router as router_service
from user.admin import AdminAuth, UserAdmin, RoleAdmin, PermissionAdmin
from user.router.router import router as router_user
from service.websocket.router import router as router_websocket
from settings import AuthSettings

warnings.filterwarnings("ignore")

app = FastAPI(
    docs_url="/api/docs",
    openapi_url="/api/openapi.json"
)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    try:
        modified_details = [
            {"location": error["loc"][1], "message": error["msg"]}
            for error in exc.errors()
        ]
    except IndexError:
        return JSONResponse(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY,
            content={"detail": "Invalid JSON format"}, )

    return JSONResponse(
        status_code=HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": modified_details},
    )


if not path.isdir('media'):
    mkdir("media")

app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/media", StaticFiles(directory='media'), name="media")

config = AuthSettings()

authentication_backend = AdminAuth(secret_key=config.authjwt_secret_key)

admin = Admin(app, engine, authentication_backend=authentication_backend, title="Администрация", base_url="/admin")

admin.add_view(UserAdmin)
admin.add_view(RoleAdmin)
admin.add_view(PermissionAdmin)

app.include_router(router_user)
app.include_router(router_service)
app.include_router(router_service)
app.include_router(router_websocket)



