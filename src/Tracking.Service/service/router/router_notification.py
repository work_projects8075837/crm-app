from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy import desc
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.read_status.model import ReadStatus
from repository.notification.create import NotificationCreate
from repository.notification.model import Notification
from repository.notification.update import NotificationUpdate
from repository.notification.read import NotificationModelRead
from service.utils.organizations.searchable_schemas import NotificationSearch, ReadStatusSearch

router_notification = APIRouter(prefix="/notification", tags=["Уведомления"])

notification_utils = Notification

search_pack = {
    Notification: NotificationSearch,
    ReadStatus: ReadStatusSearch,
}


@router_notification.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_notification(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(NotificationModelRead):
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.READ)
    return await notification_utils.get(session, request, page,
                                        offset, search_schemas=search_pack, is_hide=is_hide)


@router_notification.get("/{id}/")
async def get_notification_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> NotificationModelRead:
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.READ)
    return await notification_utils.query(session, id=id)


@router_notification.post("/")
async def post_notification(
        data: NotificationCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> NotificationModelRead:
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.CREATE)
    return await notification_utils.post(session, data.model_dump(exclude_none=True))


@router_notification.patch("/{id}/")
async def patch_notification(
        id: UUID4, data: NotificationUpdate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> NotificationModelRead:
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.UPDATE)
    return await notification_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_notification.delete("/{id}/")
async def delete_notification(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> Response:
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.DELETE)
    return await notification_utils.delete(session, id)


@router_notification.post("/delete/list/")
async def delete_notification(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> list[Response]:
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.DELETE)
    return [(await notification_utils.delete(session, id)) for id in data.ids]


@router_notification.patch("/hide/list/")
async def notification_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.DELETE)
    return [(await notification_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_notification.patch("/show/list/")
async def notification_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.DELETE)
    return [(await notification_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_notification.patch("/show/{id}/")
async def notification_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.DELETE)
    return await notification_utils.hide(id=id, session=session, is_hide=False)


@router_notification.patch("/hide/{id}/")
async def notification_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), notification_utils, PermissionType.DELETE)
    return await notification_utils.hide(id=id, session=session, is_hide=True)
