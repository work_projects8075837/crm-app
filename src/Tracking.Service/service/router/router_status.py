from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession

from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.status.create import StatusCreate
from repository.status.model import Status
from repository.status.read import StatusRead
from repository.status.update import StatusUpdate
from service.utils.ticket.searchable_schemas import StatusSearch

router_status = APIRouter(prefix="/status", tags=["Статусы"])

status_utils = Status

search_pack = {
    Status: StatusSearch
}


@router_status.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_status(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: AuthJWT = Depends(check)
) -> pagination_schemas(StatusRead):
    # await permission(auth.get_jwt_subject(), status_utils, PermissionType.READ)
    return await status_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_status.get("/{id}/")
async def get_status_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> StatusRead:
    await permission(auth.get_jwt_subject(), status_utils, PermissionType.READ)
    return await status_utils.query(session, id=id)


@router_status.post("/")
async def post_status(
        data: StatusCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> StatusRead:
    # await permission(auth.get_jwt_subject(), status_utils, PermissionType.CREATE)
    return await status_utils.post(session, data.model_dump(exclude_none=True))


@router_status.patch("/{id}/")
async def patch_status(
        id: UUID4, data: StatusUpdate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> StatusRead:
    # await permission(auth.get_jwt_subject(), status_utils, PermissionType.UPDATE)
    return await status_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_status.delete("/{id}/")
async def delete_status(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), status_utils, PermissionType.DELETE)
    return await status_utils.delete(session, id)


@router_status.post("/delete/list/")
async def delete_status(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), status_utils, PermissionType.DELETE)
    return [(await status_utils.delete(session, id)) for id in data.ids]


@router_status.patch("/hide/list/")
async def status_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), status_utils, PermissionType.DELETE)
    return [(await status_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_status.patch("/show/list/")
async def status_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), status_utils, PermissionType.DELETE)
    return [(await status_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_status.patch("/hide/{id}/")
async def status_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), status_utils, PermissionType.DELETE)
    return await status_utils.hide(id=id, session=session, is_hide=True)


@router_status.patch("/show/{id}/")
async def status_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), status_utils, PermissionType.DELETE)
    return await status_utils.hide(id=id, session=session, is_hide=False)
