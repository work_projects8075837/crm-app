from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.direction.create import DirectionCreate
from repository.direction.model import Direction
from repository.direction.read import DirectionRead
from repository.direction.update import DirectionUpdate
from repository.status.model import Status
from service.utils.direction.utils import process_status
from service.utils.ticket.searchable_schemas import DirectionSearch, StatusSearch

router_direction = APIRouter(prefix="/direction", tags=["Направления"])

direction_utils = Direction

search_pack = {
    Direction: DirectionSearch,
    Status: StatusSearch,
}


@router_direction.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_direction(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: AuthJWT = Depends(check),

) -> pagination_schemas(DirectionRead):
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.READ)
    return await direction_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_direction.get("/{id}/")
async def get_direction_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: AuthJWT = Depends(check)
) -> DirectionRead:
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.READ)
    return await direction_utils.query(session, id=id)


@router_direction.post("/")
async def post_direction(
        data: DirectionCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: AuthJWT = Depends(check)
) -> DirectionRead:
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.CREATE)
    data = await process_status(data)
    return await direction_utils.post(session, data.model_dump(exclude_none=True))


@router_direction.patch("/{id}/")
async def patch_direction(
        id: UUID4, data: DirectionUpdate,
        session: AsyncSession = Depends(get_async_session),
        # auth: AuthJWT = Depends(check)
) -> DirectionRead:
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.UPDATE)
    return await direction_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_direction.delete("/{id}/")
async def delete_direction(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: AuthJWT = Depends(check)
) -> Response:
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.DELETE)
    return await direction_utils.delete(session, id)


@router_direction.post("/delete/list/")
async def delete_direction(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.DELETE)
    return [(await direction_utils.delete(session, id)) for id in data.ids]


@router_direction.patch("/hide/list/")
async def direction_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.DELETE)
    return [(await direction_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_direction.patch("/show/list/")
async def direction_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.DELETE)
    return [(await direction_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_direction.patch("/show/{id}/")
async def direction_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.DELETE)
    return await direction_utils.hide(id=id, session=session, is_hide=False)


@router_direction.patch("/hide/{id}/")
async def direction_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), direction_utils, PermissionType.DELETE)
    return await direction_utils.hide(id=id, session=session, is_hide=True)
