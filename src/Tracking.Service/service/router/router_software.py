from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession

from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.software.create import SoftwareCreate
from repository.software.model import Software
from repository.software.read import SoftwareRead
from repository.software.update import SoftwareUpdate
from service.utils.organizations.searchable_schemas import SoftwareSearch

router_software = APIRouter(prefix="/software", tags=["Программы"])

software_utils = Software

search_pack = {
    Software: SoftwareSearch
}


@router_software.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_software(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(SoftwareRead):
    # await permission(auth.get_jwt_subject(), software_utils, PermissionType.READ)
    return await software_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_software.get("/{id}/")
async def get_software_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> SoftwareRead:
    await permission(auth.get_jwt_subject(), software_utils, PermissionType.READ)
    return await software_utils.query(session, id=id)


@router_software.post("/")
async def post_software(
        data: SoftwareCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> SoftwareRead:
    # await permission(auth.get_jwt_subject(), software_utils, PermissionType.CREATE)
    return await software_utils.post(session, data.model_dump(exclude_none=True))


@router_software.patch("/{id}/")
async def patch_software(
        id: UUID4, data: SoftwareUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> SoftwareRead:
    await permission(auth.get_jwt_subject(), software_utils, PermissionType.UPDATE)
    return await software_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_software.delete("/{id}/")
async def delete_software(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), software_utils, PermissionType.DELETE)
    return await software_utils.delete(session, id)


@router_software.post("/delete/list/")
async def delete_software(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), software_utils, PermissionType.DELETE)
    return [(await software_utils.delete(session, id)) for id in data.ids]


@router_software.patch("/hide/list/")
async def software_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), software_utils, PermissionType.DELETE)
    return [(await software_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_software.patch("/show/list/")
async def software_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), software_utils, PermissionType.DELETE)
    return [(await software_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_software.patch("/show/{id}/")
async def software_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), software_utils, PermissionType.DELETE)
    return await software_utils.hide(id=id, session=session, is_hide=False)


@router_software.patch("/hide/{id}/")
async def software_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), software_utils, PermissionType.DELETE)
    return await software_utils.hide(id=id, session=session, is_hide=True)
