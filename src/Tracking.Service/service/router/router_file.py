import uuid
from os import path
from fastapi import APIRouter, Request, status, Depends, UploadFile, File
from sqlmodel.ext.asyncio.session import AsyncSession
from settings import instance_auth
from database import get_async_session
from repository.file.bond import FileWithUser
from repository.file.create import FileCreate
from repository.file.model import File as FileModel
from manager.models import Response
from manager.utils import check

router_file = APIRouter(prefix="/file", tags=["Файлы"])

utils_file = FileModel


@router_file.post(
    "/",
    responses={
        status.HTTP_401_UNAUTHORIZED: {"model": Response},
        status.HTTP_404_NOT_FOUND: {"model": Response}
    }
)
async def create_files(
        request: Request,
        authorize: instance_auth = Depends(check),
        files: list[UploadFile] = File(...),
        session: AsyncSession = Depends(get_async_session),
) -> list[FileWithUser]:
    id = authorize.get_jwt_subject()

    created_files = []

    for file in files:
        new_filename = f"{uuid.uuid4()}.{file.filename.split('.')[-1]}"
        file_path = f"{request.url.scheme}://{request.url.netloc.split(':')[0]}/media/{str(new_filename)}"

        binary = path.join("media", new_filename)

        with open(binary, "wb") as new_file:
            new_file.write(file.file.read())

        data = FileCreate(user_id=id, path=file_path, name=file.filename, size=path.getsize(binary))
        db_file = await utils_file.post(session, data.model_dump(exclude_none=True))

        created_files.append(db_file)

    return created_files
