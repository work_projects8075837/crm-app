from fastapi import APIRouter
from service.router.router_case import router_case
from service.router.router_channel import router_channel
from service.router.router_character_problem import router_character_problem
from service.router.router_configuration import router_configuration
from service.router.router_connection import router_connection
from service.router.router_contact import router_contact
from service.router.router_counterparty import router_counterparty
from service.router.router_direction import router_direction
from service.router.router_file import router_file
from service.router.router_inventory import router_inventory
from service.router.router_job import router_job
from service.router.router_job_result import router_job_result
from service.router.router_manager import router_manager
from service.router.router_phone_number import router_phone_number
from service.router.router_position import router_position
from service.router.router_sale_point import router_sale_point
from service.router.router_software import router_software
from service.router.router_status import router_status
from service.router.router_status_object import router_status_object
from service.router.router_comment import router_comment
from service.router.router_tag import router_tag
from service.router.router_ticket import router_ticket
from service.router.router_read_status import router_read_status
from service.router.router_notification import router_notification
from service.router.router_history import router_history


router = APIRouter(prefix="/api")

router.include_router(router_counterparty)
router.include_router(router_configuration)
router.include_router(router_connection)
router.include_router(router_software)
router.include_router(router_inventory)
router.include_router(router_status_object)
router.include_router(router_status)
router.include_router(router_sale_point)
router.include_router(router_phone_number)
router.include_router(router_contact)
router.include_router(router_position)
router.include_router(router_file)
router.include_router(router_case)
router.include_router(router_channel)
router.include_router(router_direction)
router.include_router(router_job)
router.include_router(router_character_problem)
router.include_router(router_manager)
router.include_router(router_job_result)
router.include_router(router_ticket)
router.include_router(router_comment)
router.include_router(router_tag)
router.include_router(router_read_status)
router.include_router(router_notification)
router.include_router(router_history)
