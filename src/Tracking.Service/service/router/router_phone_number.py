from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.phone_number.create import PhoneNumberCreate
from repository.phone_number.model import PhoneNumber
from repository.phone_number.read import PhoneNumberRead
from repository.phone_number.update import PhoneNumberUpdate
from service.utils.organizations.searchable_schemas import PhoneNumberSearch

router_phone_number = APIRouter(prefix="/phone_number", tags=["Номера телефона"])

phone_number_utils = PhoneNumber

search_pack = {
    PhoneNumber: PhoneNumberSearch
}


@router_phone_number.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_phone_number(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(PhoneNumberRead):
    # await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.READ)
    return await phone_number_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_phone_number.get("/{id}/")
async def get_phone_number_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> PhoneNumberRead:
    await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.READ)
    return await phone_number_utils.query(session, id=id)


@router_phone_number.post("/")
async def post_phone_number(
        data: PhoneNumberCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> PhoneNumberRead:
    # await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.CREATE)
    return await phone_number_utils.post(session, data.model_dump(exclude_none=True))


@router_phone_number.patch("/{id}/")
async def patch_phone_number(
        id: UUID4, data: PhoneNumberUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> PhoneNumberRead:
    await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.UPDATE)
    return await phone_number_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_phone_number.delete("/{id}/")
async def delete_phone_number(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.DELETE)
    return await phone_number_utils.delete(session, id)


@router_phone_number.post("/delete/list/")
async def delete_phone_number(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.DELETE)
    return [(await phone_number_utils.delete(session, id)) for id in data.ids]


@router_phone_number.patch("/hide/list/")
async def phone_number_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.DELETE)
    return [(await phone_number_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_phone_number.patch("/show/list/")
async def phone_number_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.DELETE)
    return [(await phone_number_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]

@router_phone_number.patch("/show/{id}/")
async def phone_number_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.DELETE)
    return await phone_number_utils.hide(id=id, session=session, is_hide=False)


@router_phone_number.patch("/hide/{id}/")
async def phone_number_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), phone_number_utils, PermissionType.DELETE)
    return await phone_number_utils.hide(id=id, session=session, is_hide=True)
