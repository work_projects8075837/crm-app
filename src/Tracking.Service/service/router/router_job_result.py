from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.job.model import Job
from repository.job_result.create import JobResultCreate
from repository.job_result.model import JobResult
from repository.job_result.read import JobResultRead
from repository.job_result.update import JobResultUpdate
from repository.manager.model import Manager
from repository.permission.model import Permission
from repository.role.model import Role
from repository.user.model import User
from service.utils.ticket.searchable_schemas import ManagerSearch, JobResultSearch, JobSearch
from user.utils.searchable_schemas import UserSearch, RoleSearch, PermissionSearch

router_job_result = APIRouter(prefix="/job_result", tags=["Результат работы"])

job_result_utils = JobResult

search_pack = {
    JobResult: JobResultSearch,
    Job: JobSearch,
    Manager: ManagerSearch,
    User: UserSearch,
    Role: RoleSearch,
    Permission: PermissionSearch
}


@router_job_result.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_job_result(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check),

) -> pagination_schemas(JobResultRead):
    # await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.READ)
    return await job_result_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_job_result.get("/{id}/")
async def get_job_result_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> JobResultRead:
    await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.READ)
    return await job_result_utils.query(session, id=id)


@router_job_result.post("/")
async def post_job_result(
        data: JobResultCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> JobResultRead:
    # await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.CREATE)
    return await job_result_utils.post(session, data.model_dump(exclude_none=True))


@router_job_result.patch("/{id}/")
async def patch_job_result(
        id: UUID4, data: JobResultUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> JobResultRead:
    await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.UPDATE)
    return await job_result_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_job_result.delete("/{id}/")
async def delete_job_result(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> Response:
    # await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.DELETE)
    return await job_result_utils.delete(session, id)


@router_job_result.post("/delete/list/")
async def delete_job_result(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> list[Response]:
    # await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.DELETE)
    return [(await job_result_utils.delete(session, id)) for id in data.ids]


@router_job_result.patch("/hide/list/")
async def job_result_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.DELETE)
    return [(await job_result_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_job_result.patch("/show/list/")
async def job_result_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.DELETE)
    return [(await job_result_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_job_result.patch("/hide/{id}/")
async def job_result_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.DELETE)
    return await job_result_utils.hide(id=id, session=session, is_hide=True)


@router_job_result.patch("/show/{id}/")
async def job_result_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), job_result_utils, PermissionType.DELETE)
    return await job_result_utils.hide(id=id, session=session, is_hide=False)
