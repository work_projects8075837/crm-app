from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.job.create import JobCreate
from repository.job.model import Job
from repository.job.read import JobRead
from repository.job.update import JobUpdate
from service.utils.ticket.searchable_schemas import JobSearch

router_job = APIRouter(prefix="/job", tags=["Работы"])

job_utils = Job

search_pack = {
    Job: JobSearch
}


@router_job.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_job(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check),

) -> pagination_schemas(JobRead):
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.READ)
    return await job_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_job.get("/{id}/")
async def get_job_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> JobRead:
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.READ)
    return await job_utils.query(session, id=id)


@router_job.post("/")
async def post_job(
        data: JobCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> JobRead:
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.CREATE)
    return await job_utils.post(session, data.model_dump(exclude_none=True))


@router_job.patch("/{id}/")
async def patch_job(
        id: UUID4, data: JobUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> JobRead:
    await permission(auth.get_jwt_subject(), job_utils, PermissionType.UPDATE)
    return await job_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_job.delete("/{id}/")
async def delete_job(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> Response:
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.DELETE)
    return await job_utils.delete(session, id)


@router_job.post("/delete/list/")
async def delete_job(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.DELETE)
    return [(await job_utils.delete(session, id)) for id in data.ids]


@router_job.patch("/hide/list/")
async def job_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.DELETE)
    return [(await job_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_job.patch("/show/list/")
async def job_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.DELETE)
    return [(await job_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_job.patch("/show/{id}/")
async def job_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.DELETE)
    return await job_utils.hide(id=id, session=session, is_hide=False)


@router_job.patch("/hide/{id}/")
async def job_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), job_utils, PermissionType.DELETE)
    return await job_utils.hide(id=id, session=session, is_hide=True)
