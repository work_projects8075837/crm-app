from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from repository.read_status.create import ReadStatusCreate
from repository.read_status.model import ReadStatus
from repository.read_status.update import ReadStatusUpdate
from repository.read_status.read import NotificationStatusRead
from repository.user.model import User
from repository.role.model import Role
from repository.permission.model import Permission
from repository.notification.model import Notification
from user.utils.searchable_schemas import UserSearch, RoleSearch, PermissionSearch
from service.utils.organizations.searchable_schemas import ReadStatusSearch, NotificationSearch

router_read_status = APIRouter(prefix="/read_status", tags=["Статусы уведомлений"])

read_status_utils = ReadStatus

search_pack = {
    ReadStatus: ReadStatusSearch,
    User: UserSearch,
    Role: RoleSearch,
    Permission: PermissionSearch,
    Notification: NotificationSearch
}


@router_read_status.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_read_status(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(NotificationStatusRead):
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.READ)
    return await read_status_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_read_status.get("/{id}/")
async def get_read_status_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)

) -> NotificationStatusRead:
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.READ)
    return await read_status_utils.query(session, id=id)


@router_read_status.post("/")
async def post_read_status(
        data: ReadStatusCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)

) -> NotificationStatusRead:
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.CREATE)
    return await read_status_utils.post(session, data.model_dump(exclude_none=True))


@router_read_status.patch("/{id}/")
async def patch_read_status(
        id: UUID4, data: ReadStatusUpdate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)

) -> NotificationStatusRead:
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.UPDATE)
    return await read_status_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_read_status.delete("/{id}/")
async def delete_read_status(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)

) -> Response:
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.DELETE)
    return await read_status_utils.delete(session, id)


@router_read_status.post("/delete/list/")
async def delete_read_status(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)

) -> list[Response]:
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.DELETE)
    return [(await read_status_utils.delete(session, id)) for id in data.ids]


@router_read_status.post("/confirm/")
async def delete_read_status(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)

) -> list[NotificationStatusRead]:
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.DELETE)
    return [(await read_status_utils.patch(session, id, data={"is_read": True})) for id in data.ids]


@router_read_status.patch("/hide/list/")
async def read_status_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.DELETE)
    return [(await read_status_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_read_status.patch("/show/list/")
async def read_status_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.DELETE)
    return [(await read_status_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_read_status.patch("/hide/{id}/")
async def read_status_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.DELETE)
    return await read_status_utils.hide(id=id, session=session, is_hide=True)


@router_read_status.patch("/show/{id}/")
async def read_status_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), read_status_utils, PermissionType.DELETE)
    return await read_status_utils.hide(id=id, session=session, is_hide=False)
