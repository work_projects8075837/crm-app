from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.connection.model import Connection
from repository.inventory.bond import InventoryWithSalePoint
from repository.inventory.create import InventoryCreate
from repository.inventory.model import Inventory
from repository.inventory.update import InventoryUpdate
from repository.software.model import Software
from service.utils.organizations.searchable_schemas import InventorySearch, ConnectionSearch, SoftwareSearch
from service.utils.organizations.extra_conditions import EmptyConnectionCondition

router_inventory = APIRouter(prefix="/inventory", tags=["Инвентарь"])

inventory_utils = Inventory

search_pack = {
    Inventory: InventorySearch,
    Connection: ConnectionSearch,
    Software: SoftwareSearch,
}

extra_conditions = [
    EmptyConnectionCondition(
        field="connection",
        label="is_connection",
        model=Inventory,
    )
]


@router_inventory.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_inventory(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(InventoryWithSalePoint):
    # await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.READ)
    return (await inventory_utils.get(session, request, page, offset, is_hide=is_hide, search_schemas=search_pack, extra_search_condition=extra_conditions))


@router_inventory.get("/{id}/")
async def get_inventory_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> InventoryWithSalePoint:
    # await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.READ)
    return await inventory_utils.query(session, id=id)


@router_inventory.post("/")
async def post_inventory(
        data: InventoryCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> InventoryWithSalePoint:
    # await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.CREATE)
    return await inventory_utils.post(session, data.model_dump(exclude_none=True))


@router_inventory.patch("/{id}/")
async def patch_inventory(
        id: UUID4, data: InventoryUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> InventoryWithSalePoint:
    await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.UPDATE)
    return await inventory_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_inventory.delete("/{id}/")
async def delete_inventory(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.DELETE)
    return await inventory_utils.delete(session, id)


@router_inventory.post("/delete/list/")
async def delete_inventory(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.DELETE)
    return [(await inventory_utils.delete(session, id)) for id in data.ids]


@router_inventory.patch("/hide/list/")
async def inventory_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.DELETE)
    return [(await inventory_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_inventory.patch("/show/list/")
async def inventory_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.DELETE)
    return [(await inventory_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_inventory.patch("/show/{id}/")
async def inventory_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.DELETE)
    return await inventory_utils.hide(id=id, session=session, is_hide=False)


@router_inventory.patch("/hide/{id}/")
async def inventory_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), inventory_utils, PermissionType.DELETE)
    return await inventory_utils.hide(id=id, session=session, is_hide=True)
