from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.contact.create import ContactCreate
from repository.contact.model import Contact
from repository.contact.update import ContactUpdate
from repository.phone_number.model import PhoneNumber
from repository.position.bond import ContactWithPosition

from service.utils.organizations.searchable_schemas import ContactSearch, PhoneNumberSearch

router_contact = APIRouter(prefix="/contact", tags=["Контакты"])

contact_utils = Contact

search_pack = {
    Contact: ContactSearch,
    PhoneNumber: PhoneNumberSearch,
}


@router_contact.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_contact(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(ContactWithPosition):
    # await permission(auth.get_jwt_subject(), contact_utils, PermissionType.READ)
    return await contact_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_contact.get("/{id}/")
async def get_contact_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ContactWithPosition:
    await permission(auth.get_jwt_subject(), contact_utils, PermissionType.READ)
    return await contact_utils.query(session, id=id)


@router_contact.post("/")
async def post_contact(
        data: ContactCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> ContactWithPosition:
    # await permission(auth.get_jwt_subject(), contact_utils, PermissionType.CREATE)
    return await contact_utils.post(session, data.model_dump(exclude_none=True))


@router_contact.patch("/{id}/")
async def patch_contact(
        id: UUID4, data: ContactUpdate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> ContactWithPosition:
    # await permission(auth.get_jwt_subject(), contact_utils, PermissionType.UPDATE)
    return await contact_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_contact.delete("/{id}/")
async def delete_contact(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> Response:
    # await permission(auth.get_jwt_subject(), contact_utils, PermissionType.DELETE)
    return await contact_utils.delete(session, id)


@router_contact.post("/delete/list/")
async def delete_contact(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), contact_utils, PermissionType.DELETE)
    return [(await contact_utils.delete(session, id)) for id in data.ids]


@router_contact.patch("/hide/list/")
async def contact_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), contact_utils, PermissionType.DELETE)
    return [(await contact_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_contact.patch("/show/list/")
async def contact_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), contact_utils, PermissionType.DELETE)
    return [(await contact_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_contact.patch("/show/{id}/")
async def contact_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), contact_utils, PermissionType.DELETE)
    return await contact_utils.hide(id=id, session=session, is_hide=False)


@router_contact.patch("/hide/{id}/")
async def contact_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), contact_utils, PermissionType.DELETE)
    return await contact_utils.hide(id=id, session=session, is_hide=True)
