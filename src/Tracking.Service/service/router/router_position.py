from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.position.bond import PositionWithContact
from repository.position.create import PositionCreate
from repository.position.model import Position
from repository.position.update import PositionUpdate

router_position = APIRouter(prefix="/position", tags=["Рабочий контакт"])

position_utils = Position


@router_position.get("/")
async def get_position(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> pagination_schemas(PositionWithContact):
    await permission(auth.get_jwt_subject(), position_utils, PermissionType.READ)
    return await position_utils.get(session, request, page, offset, is_hide=is_hide)


@router_position.get("/{id}/")
async def get_position_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> PositionWithContact:
    await permission(auth.get_jwt_subject(), position_utils, PermissionType.READ)
    return await position_utils.query(session, id=id)


@router_position.post("/")
async def post_position(
        data: PositionCreate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> PositionWithContact:
    await permission(auth.get_jwt_subject(), position_utils, PermissionType.CREATE)
    return await position_utils.post(session, data.model_dump(exclude_none=True))


@router_position.patch("/{id}/")
async def patch_position(
        id: UUID4, data: PositionUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> PositionWithContact:
    await permission(auth.get_jwt_subject(), position_utils, PermissionType.UPDATE)
    return await position_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_position.delete("/{id}/")
async def delete_position(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), position_utils, PermissionType.DELETE)
    return await position_utils.delete(session, id)


@router_position.post("/delete/list/")
async def delete_position(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), position_utils, PermissionType.DELETE)
    return [(await position_utils.delete(session, id)) for id in data.ids]


@router_position.patch("/hide/list/")
async def position_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), position_utils, PermissionType.DELETE)
    return [(await position_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_position.patch("/show/list/")
async def position_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), position_utils, PermissionType.DELETE)
    return [(await position_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_position.patch("/hide/{id}/")
async def position_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), position_utils, PermissionType.DELETE)
    return await position_utils.hide(id=id, session=session, is_hide=True)


@router_position.patch("/show/{id}/")
async def position_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), position_utils, PermissionType.DELETE)
    return await position_utils.hide(id=id, session=session, is_hide=False)
