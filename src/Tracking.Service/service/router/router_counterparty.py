from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.counterparty.create import CounterpartyCreate
from repository.counterparty.model import Counterparty
from repository.counterparty.read import CounterpartyRead
from repository.counterparty.update import CounterpartyUpdate
from service.utils.organizations.searchable_schemas import CounterpartySearch

router_counterparty = APIRouter(prefix="/counterparty", tags=["Контрагенты"])

counterparty_utils = Counterparty

search_pack = {
    Counterparty: CounterpartySearch
}


@router_counterparty.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_counterparty(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(CounterpartyRead):
    # await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.READ)
    return await counterparty_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_counterparty.get("/{id}/")
async def get_counterparty_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> CounterpartyRead:
    # await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.READ)
    return await counterparty_utils.query(session, id=id)


@router_counterparty.post("/")
async def post_counterparty(
        data: CounterpartyCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> CounterpartyRead:
    # await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.CREATE)
    return await counterparty_utils.post(session, data.model_dump(exclude_none=True))


@router_counterparty.patch("/{id}/")
async def patch_counterparty(
        id: UUID4, data: CounterpartyUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> CounterpartyRead:
    await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.UPDATE)
    return await counterparty_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_counterparty.delete("/{id}/")
async def delete_counterparty(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.DELETE)
    return await counterparty_utils.delete(session, id)


@router_counterparty.post("/delete/list/")
async def delete_counterparty(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.DELETE)
    return [(await counterparty_utils.delete(session, id)) for id in data.ids]


@router_counterparty.patch("/hide/list/")
async def counterparty_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.DELETE)
    return [(await counterparty_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_counterparty.patch("/show/list/")
async def counterparty_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.DELETE)
    return [(await counterparty_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_counterparty.patch("/show/{id}/")
async def counterparty_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.DELETE)
    return await counterparty_utils.hide(id=id, session=session, is_hide=False)


@router_counterparty.patch("/hide/{id}/")
async def counterparty_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), counterparty_utils, PermissionType.DELETE)
    return await counterparty_utils.hide(id=id, session=session, is_hide=True)
