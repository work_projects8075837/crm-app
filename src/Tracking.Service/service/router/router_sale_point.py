from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.connection.model import Connection
from repository.contact.model import Contact
from repository.counterparty.model import Counterparty
from repository.inventory.model import Inventory
from repository.permission.model import Permission
from repository.phone_number.model import PhoneNumber
from repository.position.bond import ContactPositionWithSalePoint, ContactPositionWithSalePointList
from repository.position.model import Position
from repository.role.model import Role
from repository.sale_point.create import SalePointCreate
from repository.sale_point.model import SalePoint
from repository.sale_point.update import SalePointUpdate
from repository.software.model import Software
from repository.status_object.model import StatusObject
from repository.user.model import User
from user.utils.searchable_schemas import UserSearch, RoleSearch, PermissionSearch
from service.utils.organizations.searchable_schemas import (
    SalePointSearch, CounterpartySearch, ContactSearch,
    PositionSearch, InventorySearch, ConnectionSearch,
    SoftwareSearch, PhoneNumberSearch, StatusObjectSearch
)

router_sale_point = APIRouter(prefix="/sale_point", tags=["Торговые точки"])

sale_point_utils = SalePoint

search_pack = {
    SalePoint: SalePointSearch,
    User: UserSearch,
    Role: RoleSearch,
    Permission: PermissionSearch,
    Counterparty: CounterpartySearch,
    Position: PositionSearch,
    Inventory: InventorySearch,
    Connection: ConnectionSearch,
    Software: SoftwareSearch,
    PhoneNumber: PhoneNumberSearch,
    Contact: ContactSearch,
    StatusObject: StatusObjectSearch,
}


@router_sale_point.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_sale_point(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(ContactPositionWithSalePointList):
    # await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.READ)
    return await sale_point_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_sale_point.get("/{id}/")
async def get_sale_point_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ContactPositionWithSalePoint:
    await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.READ)
    return await sale_point_utils.query(session, id=id)


@router_sale_point.post("/")
async def post_sale_point(
        data: SalePointCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> ContactPositionWithSalePoint:
    # await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.CREATE)
    return await sale_point_utils.post(session, data.model_dump(exclude_unset=True))


@router_sale_point.patch("/{id}/")
async def patch_sale_point(
        id: UUID4, data: SalePointUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ContactPositionWithSalePoint:
    await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.UPDATE)
    return await sale_point_utils.patch(session, id, data.model_dump(exclude_unset=True))


@router_sale_point.delete("/{id}/")
async def delete_sale_point(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.DELETE)
    return await sale_point_utils.delete(session, id)


@router_sale_point.post("/delete/list/")
async def delete_sale_point(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.DELETE)
    return [(await sale_point_utils.delete(session, id)) for id in data.ids]


@router_sale_point.patch("/hide/list/")
async def sale_point_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.DELETE)
    return [(await sale_point_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_sale_point.patch("/show/list/")
async def sale_point_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.DELETE)
    return [(await sale_point_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_sale_point.patch("/hide/{id}/")
async def sale_point_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.DELETE)
    return await sale_point_utils.hide(id=id, session=session, is_hide=True)


@router_sale_point.patch("/show/{id}/")
async def sale_point_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), sale_point_utils, PermissionType.DELETE)
    return await sale_point_utils.hide(id=id, session=session, is_hide=False)
