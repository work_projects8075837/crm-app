from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.comment.bond import CommentWithUser
from repository.comment.create import CommentCreate
from repository.comment.model import Comment
from repository.comment.update import CommentUpdate
from repository.ticket.model import Ticket
from repository.permission.model import Permission
from repository.role.model import Role
from repository.user.model import User
from service.utils.ticket.searchable_schemas import CommentSearch, TicketSearch
from user.utils.searchable_schemas import UserSearch, RoleSearch, PermissionSearch

router_comment = APIRouter(prefix="/comment", tags=["Комментарии"])

comment_utils = Comment

search_pack = {
    Comment: CommentSearch,
    Ticket: TicketSearch,
    User: UserSearch,
    Role: RoleSearch,
    Permission: PermissionSearch
}


@router_comment.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_comment(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(CommentWithUser):
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.READ)
    return await comment_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_comment.get("/{id}/")
async def get_comment_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> CommentWithUser:
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.READ)
    return await comment_utils.query(session, id=id)


@router_comment.post("/")
async def post_comment(
        data: CommentCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> CommentWithUser:
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.CREATE)
    return await comment_utils.post(session, data.model_dump(exclude_none=True))


@router_comment.patch("/{id}/")
async def patch_comment(
        id: UUID4, data: CommentUpdate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> CommentWithUser:
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.UPDATE)
    return await comment_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_comment.delete("/{id}/")
async def delete_comment(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), comment_utils, PermissionType.DELETE)
    return await comment_utils.delete(session, id)


@router_comment.post("/delete/list/")
async def delete_comment(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), comment_utils, PermissionType.DELETE)
    return [(await comment_utils.delete(session, id)) for id in data.ids]


@router_comment.patch("/hide/list/")
async def comment_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.DELETE)
    return [(await comment_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_comment.patch("/show/list/")
async def comment_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.DELETE)
    return [(await comment_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_comment.patch("/hide/{id}/")
async def comment_hide_record_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.DELETE)
    return await comment_utils.hide(id=id, session=session, is_hide=True)


@router_comment.patch("/show/{id}/")
async def comment_show_record_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.DELETE)
    return await comment_utils.hide(id=id, session=session, is_hide=False)
