from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from database import get_async_session
from repository.history.model import TicketHistory
from repository.history.read import TicketHistoryRead
from service.utils.ticket.searchable_schemas import HistorySearch
from manager.models import  pagination_schemas


router_history = APIRouter(prefix="/history", tags=["История заявки"])

history_utils = TicketHistory

search_pack = {
    TicketHistory: HistorySearch
}


@router_history.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_comment(
        request: Request,
        page: int = 1, offset: int = 50,
        session: AsyncSession = Depends(get_async_session),
        # auth: AuthJWT = Depends(check)
) -> pagination_schemas(TicketHistoryRead):
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.READ)
    return await history_utils.get(session, request, page, offset, search_schemas=search_pack)


@router_history.get("/{id}/")
async def get_comment_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: AuthJWT = Depends(check)
) -> TicketHistoryRead:
    # await permission(auth.get_jwt_subject(), comment_utils, PermissionType.READ)
    return await history_utils.query(session, id=id)
