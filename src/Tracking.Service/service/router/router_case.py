from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.case.create import CaseCreate
from repository.case.model import Case
from repository.case.read import CaseRead
from repository.case.update import CaseUpdate
from repository.manager.model import Manager
from repository.permission.model import Permission
from repository.role.model import Role
from repository.user.model import User
from service.utils.personality.searchable_schemas import CaseSearch
from service.utils.ticket.searchable_schemas import ManagerSearch
from user.utils.searchable_schemas import UserSearch, RoleSearch, PermissionSearch

router_case = APIRouter(prefix="/case", tags=["Дела"])

case_utils = Case

search_pack = {
    Case: CaseSearch,
    Manager: ManagerSearch,
    User: UserSearch,
    Role: RoleSearch,
    Permission: PermissionSearch
}


@router_case.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_case(
        request: Request,
        is_hide: bool = False,
        page: int = 1, offset: int = 50,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check),

) -> pagination_schemas(CaseRead):
    # await permission(auth.get_jwt_subject(), case_utils, PermissionType.READ)
    return await case_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_case.get("/{id}/")
async def get_case_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> CaseRead:
    await permission(auth.get_jwt_subject(), case_utils, PermissionType.READ)
    return await case_utils.query(session, id=id)


@router_case.post("/")
async def post_case(
        data: CaseCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> CaseRead:
    # await permission(auth.get_jwt_subject(), case_utils, PermissionType.CREATE)
    return await case_utils.post(session, data.model_dump(exclude_none=True))


@router_case.patch("/{id}/")
async def patch_case(
        id: UUID4, data: CaseUpdate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> CaseRead:
#     await permission(auth.get_jwt_subject(), case_utils, PermissionType.UPDATE)
    return await case_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_case.delete("/{id}/")
async def delete_case(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> Response:
#     await permission(auth.get_jwt_subject(), case_utils, PermissionType.DELETE)
    return await case_utils.delete(session, id)


@router_case.post("/delete/list/")
async def delete_case(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> list[Response]:
#     await permission(auth.get_jwt_subject(), case_utils, PermissionType.DELETE)
    return [(await case_utils.delete(session, id)) for id in data.ids]


@router_case.patch("/hide/list/")
async def case_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), case_utils, PermissionType.DELETE)
    return [(await case_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_case.patch("/show/list/")
async def case_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), case_utils, PermissionType.DELETE)
    return [(await case_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_case.patch("/show/{id}/")
async def case_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), case_utils, PermissionType.DELETE)
    return await case_utils.hide(id=id, session=session, is_hide=False)


@router_case.patch("/hide/{id}")
async def case_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), case_utils, PermissionType.DELETE)
    return await case_utils.hide(id=id, session=session, is_hide=True)
