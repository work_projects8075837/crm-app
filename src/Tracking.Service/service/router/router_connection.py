from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.connection.bond import ConnectionWithConfiguration
from repository.connection.create import ConnectionCreate
from repository.connection.model import Connection
from repository.connection.update import ConnectionUpdate
from service.utils.organizations.searchable_schemas import ConnectionSearch

router_connection = APIRouter(prefix="/connection", tags=["Подключения"])

connection_utils = Connection

search_pack = {
    Connection: ConnectionSearch,
}


@router_connection.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_connection(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(ConnectionWithConfiguration):
    # await permission(auth.get_jwt_subject(), connection_utils, PermissionType.READ)
    return await connection_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_connection.get("/{id}/")
async def get_connection_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ConnectionWithConfiguration:
    await permission(auth.get_jwt_subject(), connection_utils, PermissionType.READ)
    return await connection_utils.query(session, id=id)


@router_connection.post("/")
async def post_connection(
        data: ConnectionCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> ConnectionWithConfiguration:
    # await permission(auth.get_jwt_subject(), connection_utils, PermissionType.CREATE)
    return await connection_utils.post(session, data.model_dump(exclude_none=True))


@router_connection.patch("/{id}/")
async def patch_connection(
        id: UUID4, data: ConnectionUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ConnectionWithConfiguration:
    await permission(auth.get_jwt_subject(), connection_utils, PermissionType.UPDATE)
    return await connection_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_connection.delete("/{id}/")
async def delete_connection(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), connection_utils, PermissionType.DELETE)
    return await connection_utils.delete(session, id)


@router_connection.post("/delete/list/")
async def delete_connection(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), connection_utils, PermissionType.DELETE)
    return [(await connection_utils.delete(session, id)) for id in data.ids]


@router_connection.patch("/hide/list/")
async def connection_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), connection_utils, PermissionType.DELETE)
    return [(await connection_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_connection.patch("/show/list/")
async def connection_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), connection_utils, PermissionType.DELETE)
    return [(await connection_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_connection.patch("/hide/{id}/")
async def connection_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), connection_utils, PermissionType.DELETE)
    return await connection_utils.hide(id=id, session=session, is_hide=True)


@router_connection.patch("/show/{id}/")
async def connection_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), connection_utils, PermissionType.DELETE)
    return await connection_utils.hide(id=id, session=session, is_hide=False)
