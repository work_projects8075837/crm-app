from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.manager.create import ManagerCreate
from repository.manager.model import Manager
from repository.manager.read import ManagerRead
from repository.manager.update import ManagerUpdate
from repository.permission.model import Permission
from repository.role.model import Role
from repository.user.model import User
from service.utils.ticket.searchable_schemas import ManagerSearch
from user.utils.searchable_schemas import UserSearch, RoleSearch, PermissionSearch

router_manager = APIRouter(prefix="/manager", tags=["Менеджеры"])

manager_utils = Manager

search_pack = {
    Manager: ManagerSearch,
    User: UserSearch,
    Role: RoleSearch,
    Permission: PermissionSearch
}


@router_manager.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_manager(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check),

) -> pagination_schemas(ManagerRead):
    # await permission(auth.get_jwt_subject(), manager_utils, PermissionType.READ)
    return await manager_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_manager.get("/{id}/")
async def get_manager_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ManagerRead:
    await permission(auth.get_jwt_subject(), manager_utils, PermissionType.READ)
    return await manager_utils.query(session, id=id)


@router_manager.post("/")
async def post_manager(
        data: ManagerCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> ManagerRead:
    # await permission(auth.get_jwt_subject(), manager_utils, PermissionType.CREATE)
    return await manager_utils.post(session, data.model_dump(exclude_none=True))


@router_manager.patch("/{id}/")
async def patch_manager(
        id: UUID4, data: ManagerUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ManagerRead:
    await permission(auth.get_jwt_subject(), manager_utils, PermissionType.UPDATE)
    return await manager_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_manager.delete("/{id}/")
async def delete_manager(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> Response:
    # await permission(auth.get_jwt_subject(), manager_utils, PermissionType.DELETE)
    return await manager_utils.delete(session, id)


@router_manager.post("/delete/list/")
async def delete_manager(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> list[Response]:
    # await permission(auth.get_jwt_subject(), manager_utils, PermissionType.DELETE)
    return [(await manager_utils.delete(session, id)) for id in data.ids]


@router_manager.patch("/hide/list/")
async def manager_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), manager_utils, PermissionType.DELETE)
    return [(await manager_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_manager.patch("/show/list/")
async def manager_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), manager_utils, PermissionType.DELETE)
    return [(await manager_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_manager.patch("/show/{id}/")
async def manager_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), manager_utils, PermissionType.DELETE)
    return await manager_utils.hide(id=id, session=session, is_hide=False)


@router_manager.patch("/hide/{id}/")
async def manager_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), manager_utils, PermissionType.DELETE)
    return await manager_utils.hide(id=id, session=session, is_hide=True)
