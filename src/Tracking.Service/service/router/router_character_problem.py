from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.character_problem.create import CharacterProblemCreate
from repository.character_problem.model import CharacterProblem
from repository.character_problem.read import CharacterProblemRead
from repository.character_problem.update import CharacterProblemUpdate
from repository.tag.model import Tag
from service.utils.ticket.searchable_schemas import CharacterProblemSearch, TagSearch

router_character_problem = APIRouter(prefix="/character_problem", tags=["Характер проблемы"])

character_problem_utils = CharacterProblem

search_pack = {
    CharacterProblem: CharacterProblemSearch,
    Tag: TagSearch
}


@router_character_problem.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_character_problem(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check),

) -> pagination_schemas(CharacterProblemRead):
    await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.READ)
    return await character_problem_utils.get(session, request, page, offset, search_schemas=search_pack,
                                             is_hide=is_hide)


@router_character_problem.get("/{id}/")
async def get_character_problem_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> CharacterProblemRead:
    await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.READ)
    return await character_problem_utils.query(session, id=id)


@router_character_problem.post("/")
async def post_character_problem(
        data: CharacterProblemCreate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> CharacterProblemRead:
    await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.CREATE)
    return await character_problem_utils.post(session, data.model_dump(exclude_none=True))


@router_character_problem.patch("/{id}/")
async def patch_character_problem(
        id: UUID4, data: CharacterProblemUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> CharacterProblemRead:
    await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.UPDATE)
    return await character_problem_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_character_problem.delete("/{id}/")
async def delete_character_problem(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.DELETE)
    return await character_problem_utils.delete(session, id)


@router_character_problem.post("/delete/list/")
async def delete_character_problem(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.DELETE)
    return [(await character_problem_utils.delete(session, id)) for id in data.ids]


@router_character_problem.patch("/hide/list/")
async def character_problem_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.DELETE)
    return [(await character_problem_utils.hide(id=id, session=session, is_hide=True)) for id
            in
            data.ids]


@router_character_problem.patch("/show/list/")
async def character_problem_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.DELETE)
    return [(await character_problem_utils.hide(id=id, session=session, is_hide=False)) for
            id in
            data.ids]


@router_character_problem.patch("/show/{id}/")
async def character_problem_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.DELETE)
    return await character_problem_utils.hide(id=id, session=session, is_hide=False)


@router_character_problem.patch("/hide/{id}/")
async def character_problem_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), character_problem_utils, PermissionType.DELETE)
    return await character_problem_utils.hide(id=id, session=session, is_hide=True)
