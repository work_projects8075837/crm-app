from fastapi import APIRouter, Depends, Request, BackgroundTasks
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.case.model import Case
from repository.channel.model import Channel
from repository.character_problem.model import CharacterProblem
from repository.comment.model import Comment
from repository.connection.model import Connection
from repository.contact.model import Contact
from repository.counterparty.model import Counterparty
from repository.direction.model import Direction
from repository.inventory.model import Inventory
from repository.job.model import Job
from repository.job_result.model import JobResult
from repository.manager.model import Manager
from repository.permission.model import Permission
from repository.phone_number.model import PhoneNumber
from repository.position.model import Position
from repository.role.model import Role
from repository.sale_point.model import SalePoint
from repository.software.model import Software
from repository.status.model import Status
from repository.status_object.model import StatusObject
from repository.ticket.bond import TicketWithContact
from repository.ticket.create import TicketCreate
from repository.ticket.model import Ticket
from repository.ticket.update import TicketUpdate
from repository.user.model import User
from repository.history.model import TicketHistory
from user.utils.searchable_schemas import UserSearch, RoleSearch, PermissionSearch
from service.utils.organizations.searchable_schemas import (
    SalePointSearch, CounterpartySearch, ContactSearch,
    PositionSearch, InventorySearch, ConnectionSearch,
    SoftwareSearch, PhoneNumberSearch, StatusObjectSearch,
)
from service.utils.ticket.searchable_schemas import (
    StatusSearch, ChannelSearch,
    DirectionSearch, CharacterProblemSearch, ManagerSearch,
    CommentSearch, JobResultSearch, JobSearch,
    TicketSearch,
)
from service.utils.personality.searchable_schemas import CaseSearch
from service.utils.ticket.extra_conditions import TicketCreatedAtFrom, TicketCreatedAtTo

router_ticket = APIRouter(prefix="/ticket", tags=["Заявки"])

ticket_utils = Ticket
history_utils = TicketHistory

search_pack = {
    Ticket: TicketSearch,
    Status: StatusSearch,
    Channel: ChannelSearch,
    Direction: DirectionSearch,
    CharacterProblem: CharacterProblemSearch,
    SalePoint: SalePointSearch,
    User: UserSearch,
    Role: RoleSearch,
    Permission: PermissionSearch,
    Counterparty: CounterpartySearch,
    Position: PositionSearch,
    Inventory: InventorySearch,
    Connection: ConnectionSearch,
    Software: SoftwareSearch,
    PhoneNumber: PhoneNumberSearch,
    Contact: ContactSearch,
    StatusObject: StatusObjectSearch,
    Manager: ManagerSearch,
    Comment: CommentSearch,
    JobResult: JobResultSearch,
    Job: JobSearch,
    Case: CaseSearch,
}

extra_search_condition = [
    TicketCreatedAtFrom(field="created_at", label="created_at_from", model=Ticket),
    TicketCreatedAtTo(field="created_at", label="created_at_to", model=Ticket),
]


@router_ticket.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_ticket(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check),
) -> pagination_schemas(TicketWithContact):
    await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.READ)
    return await ticket_utils.get(session, request, page, offset, search_schemas=search_pack,
                                  extra_search_condition=extra_search_condition, is_hide=is_hide)


@router_ticket.get("/{id}/")
async def get_ticket_by_id(
        id: UUID4,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check),
) -> TicketWithContact:
    await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.READ)
    return await ticket_utils.query(session, id=id)


@router_ticket.post("/")
async def post_ticket(
        data: TicketCreate,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> TicketWithContact:
    await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.CREATE)
    ticket: Ticket = await ticket_utils.persisting_creation(
        background_task_manager=background_tasks,
        entity_history_instance=history_utils,
        model_validator=TicketWithContact,
        session=session,
        data=data.model_dump(exclude_unset=True),
        user_id=str(auth.get_jwt_subject())
    )

    return await ticket_utils.query(session, id=ticket.id)


@router_ticket.patch("/{id}/")
async def patch_ticket(
        id: UUID4, data: TicketUpdate,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> TicketWithContact:
    await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.UPDATE)
    return await ticket_utils.persisting_update(
        background_task_manager=background_tasks,
        entity_history_instance=history_utils, 
        model_validator=TicketWithContact, 
        session=session, 
        target_id=id, 
        data=data.model_dump(exclude_unset=True), 
        user_id=str(auth.get_jwt_subject())
    )


@router_ticket.delete("/{id}/")
async def delete_ticket(
        id: UUID4,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.DELETE)
    return await ticket_utils.persisting_deletion(
        background_task_manager=background_tasks,
        entity_history_instance=history_utils,
        model_validator=TicketWithContact,
        session=session,
        target_id=id, 
        user_id=str(auth.get_jwt_subject())
    )


@router_ticket.post("/delete/list/")
async def delete_ticket(
        data: EntityListRead,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.DELETE)
    return [(await ticket_utils.persisting_deletion(background_tasks, history_utils, TicketWithContact, session, id, str(auth.get_jwt_subject()))) for id in data.ids]


@router_ticket.patch("/hide/list/")
async def ticket_hide_records(
        data: EntityListRead,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.DELETE)
    return [(await ticket_utils.persisting_hiding(background_tasks, history_utils, TicketWithContact, session, id, str(auth.get_jwt_subject()), True)) for id in data.ids]



@router_ticket.patch("/show/list/")
async def ticket_show_records(
        data: EntityListRead,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.DELETE)
    return [(await ticket_utils.persisting_hiding(background_tasks, history_utils, TicketWithContact, session, id, str(auth.get_jwt_subject()), False)) for id in data.ids]


@router_ticket.patch("/show/{id}/")
async def ticket_show_record_by_id(
        id: UUID4,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.DELETE)
    return await ticket_utils.persisting_hiding(
        background_task_manager=background_tasks,
        entity_history_instance=history_utils,
        model_validator=TicketWithContact,
        session=session,
        target_id=id,
        user_id=str(auth.get_jwt_subject()),
        is_visible=False
    )

@router_ticket.patch("/hide/{id}/")
async def ticket_hide_record_by_id(
        id: UUID4,
        background_tasks: BackgroundTasks,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), ticket_utils, PermissionType.DELETE)
    return await ticket_utils.persisting_hiding(
        background_task_manager=background_tasks,
        entity_history_instance=history_utils,
        model_validator=TicketWithContact,
        session=session,
        target_id=id,
        user_id=str(auth.get_jwt_subject()),
        is_visible=True,
    )
