from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.status_object.create import StatusObjectCreate
from repository.status_object.model import StatusObject
from repository.status_object.read import StatusObjectRead
from repository.status_object.update import StatusObjectUpdate
from service.utils.organizations.searchable_schemas import StatusObjectSearch

router_status_object = APIRouter(prefix="/status_object", tags=["Статусы торговых точек"])

status_object_utils = StatusObject

search_pack = {
    StatusObject: StatusObjectSearch,
}


@router_status_object.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_status_object(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(StatusObjectRead):
    # await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.READ)
    return await status_object_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_status_object.get("/{id}/")
async def get_status_object_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> StatusObjectRead:
    await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.READ)
    return await status_object_utils.query(session, id=id)


@router_status_object.post("/")
async def post_status_object(
        data: StatusObjectCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> StatusObjectRead:
    # await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.CREATE)
    return await status_object_utils.post(session, data.model_dump(exclude_none=True))


@router_status_object.patch("/{id}/")
async def patch_status_object(
        id: UUID4, data: StatusObjectUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> StatusObjectRead:
    await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.UPDATE)
    return await status_object_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_status_object.delete("/{id}/")
async def delete_status_object(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.DELETE)
    return await status_object_utils.delete(session, id)


@router_status_object.post("/delete/list/")
async def delete_status_object(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.DELETE)
    return [(await status_object_utils.delete(session, id)) for id in data.ids]


@router_status_object.patch("/hide/list/")
async def status_object_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.DELETE)
    return [(await status_object_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_status_object.patch("/show/list/")
async def status_object_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.DELETE)
    return [(await status_object_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_status_object.patch("/hide/{id}/")
async def status_object_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.DELETE)
    return await status_object_utils.hide(id=id, session=session, is_hide=True)


@router_status_object.patch("/show/{id}/")
async def status_object_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), status_object_utils, PermissionType.DELETE)
    return await status_object_utils.hide(id=id, session=session, is_hide=False)
