from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.channel.create import ChannelCreate
from repository.channel.model import Channel
from repository.channel.read import ChannelRead
from repository.channel.update import ChannelUpdate
from service.utils.ticket.searchable_schemas import ChannelSearch

router_channel = APIRouter(prefix="/channel", tags=["Каналы связи"])

channel_utils = Channel

search_pack = {
    Channel: ChannelSearch
}


@router_channel.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_channel(
        request: Request,
        is_hide: bool = False,
        page: int = 1, offset: int = 50,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check),

) -> pagination_schemas(ChannelRead):
    await permission(auth.get_jwt_subject(), channel_utils, PermissionType.READ)
    return await channel_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_channel.get("/{id}/")
async def get_channel_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ChannelRead:
    await permission(auth.get_jwt_subject(), channel_utils, PermissionType.READ)
    return await channel_utils.query(session, id=id)


@router_channel.post("/")
async def post_channel(
        data: ChannelCreate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ChannelRead:
    await permission(auth.get_jwt_subject(), channel_utils, PermissionType.CREATE)
    return await channel_utils.post(session, data.model_dump(exclude_none=True))


@router_channel.patch("/{id}/")
async def patch_channel(
        id: UUID4, data: ChannelUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ChannelRead:
    await permission(auth.get_jwt_subject(), channel_utils, PermissionType.UPDATE)
    return await channel_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_channel.delete("/{id}/")
async def delete_channel(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), channel_utils, PermissionType.DELETE)
    return await channel_utils.delete(session, id)


@router_channel.post("/delete/list/")
async def delete_channel(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), channel_utils, PermissionType.DELETE)
    return [(await channel_utils.delete(session, id)) for id in data.ids]


@router_channel.patch("/hide/list/")
async def channel_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), channel_utils, PermissionType.DELETE)
    return [(await channel_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_channel.patch("/show/list/")
async def channel_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), channel_utils, PermissionType.DELETE)
    return [(await channel_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_channel.patch("/show/{id}/")
async def channel_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), channel_utils, PermissionType.DELETE)
    return await channel_utils.hide(id=id, session=session, is_hide=False)


@router_channel.patch("/hide/{id}/")
async def channel_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), channel_utils, PermissionType.DELETE)
    return await channel_utils.hide(id=id, session=session, is_hide=True)
