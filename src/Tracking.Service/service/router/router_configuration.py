from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.configuration.bond import ConfigurationWithConnection
from repository.configuration.create import ConfigurationCreate
from repository.configuration.model import Configuration
from repository.configuration.update import ConfigurationUpdate
from repository.connection.model import Connection
from service.utils.organizations.searchable_schemas import ConnectionSearch, ConfigurationSearch

router_configuration = APIRouter(prefix="/configuration", tags=["Конфигурация подключения"])

configuration_utils = Configuration

search_pack = {
    Configuration: ConfigurationSearch,
    Connection: ConnectionSearch,
}


@router_configuration.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_configuration(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> pagination_schemas(ConfigurationWithConnection):
    await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.READ)
    return await configuration_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_configuration.get("/{id}/")
async def get_configuration_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ConfigurationWithConnection:
    await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.READ)
    return await configuration_utils.query(session, id=id)


@router_configuration.post("/")
async def post_configuration(
        data: ConfigurationCreate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ConfigurationWithConnection:
    await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.CREATE)
    return await configuration_utils.post(session, data.model_dump(exclude_none=True))


@router_configuration.patch("/{id}/")
async def patch_configuration(
        id: UUID4, data: ConfigurationUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> ConfigurationWithConnection:
    await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.UPDATE)
    return await configuration_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_configuration.delete("/{id}/")
async def delete_configuration(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.DELETE)
    return await configuration_utils.delete(session, id)


@router_configuration.post("/delete/list/")
async def delete_configuration(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.DELETE)
    return [(await configuration_utils.delete(session, id)) for id in data.ids]


@router_configuration.patch("/hide/list/")
async def configuration_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.DELETE)
    return [(await configuration_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_configuration.patch("/show/list/")
async def configuration_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.DELETE)
    return [(await configuration_utils.hide(id=id, session=session, is_hide=False)) for id in
            data.ids]


@router_configuration.patch("/show/{id}/")
async def configuration_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.DELETE)
    return await configuration_utils.hide(id=id, session=session, is_hide=False)


@router_configuration.patch("/hide/{id}/")
async def configuration_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), configuration_utils, PermissionType.DELETE)
    return await configuration_utils.hide(id=id, session=session, is_hide=True)
