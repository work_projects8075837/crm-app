from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas, EntityRead, EntityListRead
from manager.utils import check, PermissionType, permission
from repository.tag.create import TagCreate
from repository.tag.model import Tag
from repository.tag.read import TagRead
from repository.tag.update import TagUpdate
from service.utils.ticket.searchable_schemas import TagSearch

router_tag = APIRouter(prefix="/tag", tags=["Теги"])

tag_utils = Tag

search_pack = {
    Tag: TagSearch
}


@router_tag.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_tag(
        request: Request,
        page: int = 1, offset: int = 50,
        is_hide: bool = False,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check),

) -> pagination_schemas(TagRead):
    # await permission(auth.get_jwt_subject(), tag_utils, PermissionType.READ)
    return await tag_utils.get(session, request, page, offset, search_schemas=search_pack, is_hide=is_hide)


@router_tag.get("/{id}/")
async def get_tag_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> TagRead:
    await permission(auth.get_jwt_subject(), tag_utils, PermissionType.READ)
    return await tag_utils.query(session, id=id)


@router_tag.post("/")
async def post_tag(
        data: TagCreate,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> TagRead:
    # await permission(auth.get_jwt_subject(), tag_utils, PermissionType.CREATE)
    return await tag_utils.post(session, data.model_dump(exclude_none=True))


@router_tag.patch("/{id}/")
async def patch_tag(
        id: UUID4, data: TagUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> TagRead:
    await permission(auth.get_jwt_subject(), tag_utils, PermissionType.UPDATE)
    return await tag_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_tag.delete("/{id}/")
async def delete_tag(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), tag_utils, PermissionType.DELETE)
    return await tag_utils.delete(session, id)


@router_tag.post("/delete/list/")
async def delete_tag(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> list[Response]:
    await permission(auth.get_jwt_subject(), tag_utils, PermissionType.DELETE)
    return [(await tag_utils.delete(session, id)) for id in data.ids]


@router_tag.patch("/hide/list/")
async def tag_hide_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), tag_utils, PermissionType.DELETE)
    return [(await tag_utils.hide(id=id, session=session, is_hide=True)) for id in
            data.ids]


@router_tag.patch("/show/list/")
async def tag_show_records(
        data: EntityListRead,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), tag_utils, PermissionType.DELETE)
    return [(await tag_utils.hide(id=id, session=session, is_hide=False)) for id in data.ids]


@router_tag.patch("/hide/{id}/")
async def tag_hide_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), tag_utils, PermissionType.DELETE)
    return await tag_utils.hide(id=id, session=session, is_hide=True)


@router_tag.patch("/show/{id}/")
async def tag_show_record_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
):
    # await permission(auth.get_jwt_subject(), tag_utils, PermissionType.DELETE)
    return await tag_utils.hide(id=id, session=session, is_hide=False)
