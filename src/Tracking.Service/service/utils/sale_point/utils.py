async def process_regulations(data):
    if len(data.data.regulations) >= 50:
        data = data.regulations[:50]
    return data
