from typing import Optional

from pydantic import Field

from service.utils.base import BaseSearchModel
from manager.models import SearchType


class CounterpartySearch(BaseSearchModel):
    counterparty_id: SearchType
    counterparty_tag: SearchType
    counterparty_name: SearchType
    counterparty_phone: SearchType
    counterparty_tin: SearchType
    counterparty_address: SearchType
    counterparty_comment: SearchType
    counterparty_email: SearchType
    counterparty_is_parent: SearchType
    counterparty_counterparty_id: SearchType


class ConnectionSearch(BaseSearchModel):
    connection_id: SearchType
    connection_type: SearchType


class ConfigurationSearch(BaseSearchModel):
    configuration_id: SearchType
    configuration_connection_id: SearchType


class SoftwareSearch(BaseSearchModel):
    software_id: SearchType
    software_name: SearchType
    software_version: SearchType


class InventorySearch(BaseSearchModel):
    inventory_id: SearchType
    inventory_sale_point_id: SearchType
    inventory_name: SearchType
    inventory_serial_number: SearchType
    inventory_is_work: SearchType
    inventory_is_parent: SearchType
    inventory_is_connection: SearchType
    inventory_inventory_id: SearchType


class StatusObjectSearch(BaseSearchModel):
    status_object_id: SearchType
    status_object_name: SearchType


class SalePointSearch(BaseSearchModel):
    sale_point_id: SearchType
    sale_point_name: SearchType
    sale_point_email: SearchType
    sale_point_phone: SearchType
    sale_point_address: SearchType
    sale_point_description: SearchType
    sale_point_is_parent: SearchType
    sale_point_id: SearchType
    sale_point_regulations: SearchType


class PhoneNumberSearch(BaseSearchModel):
    phone_number_id: SearchType
    phone_number_name: SearchType


class ContactSearch(BaseSearchModel):
    contact_id: SearchType
    contact_name: SearchType
    contact_email: SearchType


class PositionSearch(BaseSearchModel):
    position_id: SearchType
    position_is_responsible: SearchType
    position_position: SearchType


class NotificationSearch(BaseSearchModel):
    notification_id: SearchType
    notification_model: SearchType
    notification_name: SearchType
    notification_description: SearchType
    notification_created_at: SearchType


class ReadStatusSearch(BaseSearchModel):
    read_status_id: SearchType
    read_status_is_read: SearchType
    read_status_user_id: SearchType
    read_status_notification_id: SearchType

