from manager.extra_condition_base import CustomConditionConfig
from repository.inventory.model import Inventory


class EmptyConnectionCondition(CustomConditionConfig):
    def get_condition(self, value: str):
        if value == "true":
            return Inventory.connection.any()
        if value == "false":
            return ~Inventory.connection.any()
        else:
            return
