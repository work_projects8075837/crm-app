from typing import Optional

from pydantic import Field

from service.utils.base import BaseSearchModel
from manager.models import SearchType


class CaseSearch(BaseSearchModel):
    case_id: SearchType
    case_manager_id: SearchType
    case_ticket_id: SearchType
    case_name: SearchType
    case_finish_date: SearchType
    case_finish_time: SearchType
    case_is_completed: SearchType
