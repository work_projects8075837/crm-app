from typing import Optional
from pydantic import BaseModel, Field


class BaseSearchModel(BaseModel):
    is_hard: Optional[bool] = Field(True)

