async def process_status(data):
    value = [
        {"id": "b715e575-ec60-42da-9ad5-c1c1468a1038"},
        {"id": "08bf1461-c99d-4903-a965-a2966a39ea65"},
        {"id": "180577be-b1d3-46af-a1fe-74bac01f17e3"},
        {"id": "54b170e9-fc38-4c5b-a822-c10b5cc26982"}
    ]
    if not isinstance(data.status, list):
        data.status = value
    else:
        data.status.extend(value)
    return data
