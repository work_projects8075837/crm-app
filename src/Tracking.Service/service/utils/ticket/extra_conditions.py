from dateutil import parser
from sqlalchemy import DateTime, cast
from manager.extra_condition_base import CustomConditionConfig
from repository.ticket.model import Ticket


class TicketCreatedAtFrom(CustomConditionConfig):
    def get_condition(self, value: str):
        return parser.parse(value) > cast(Ticket.created_at, DateTime)


class TicketCreatedAtTo(CustomConditionConfig):
    def get_condition(self, value: str):
        return parser.parse(value)  < cast(Ticket.created_at, DateTime)
