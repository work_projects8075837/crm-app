from service.utils.base import BaseSearchModel
from manager.models import SearchType


class CommentSearch(BaseSearchModel):
    comment_id: SearchType
    comment_description: SearchType
    comment_date_create: SearchType
    comment_date_update: SearchType


class TagSearch(BaseSearchModel):
    tag_id: SearchType
    tag_name: SearchType


class ChannelSearch(BaseSearchModel):
    channel_id: SearchType
    channel_name: SearchType
    channel_multiple: SearchType


class StatusSearch(BaseSearchModel):
    status_id: SearchType
    status_name: SearchType
    status_sla: SearchType
    status_color: SearchType
    status_background: SearchType
    status_preview_link: SearchType
    status_is_finish: SearchType


class DirectionSearch(BaseSearchModel):
    direction_id: SearchType
    direction_name: SearchType


class JobSearch(BaseSearchModel):
    job_id: SearchType
    job_name: SearchType
    job_job_id: SearchType
    job_is_parent: SearchType
    job_duration: SearchType


class CharacterProblemSearch(BaseSearchModel):
    character_problem_id: SearchType
    character_problem_name: SearchType
    character_problem_sla: SearchType
    character_problem_url: SearchType
    character_problem_is_parent: SearchType


class ManagerSearch(BaseSearchModel):
    manager_id: SearchType
    manager_type: SearchType
    manager_user_id: SearchType


class JobResultSearch(BaseSearchModel):
    job_result_id: SearchType
    job_result_time: SearchType


class TicketSearch(BaseSearchModel):
    ticket_id: SearchType
    ticket_number: SearchType
    ticket_description: SearchType
    ticket_created_at: SearchType
    ticket_finished_at: SearchType
    ticket_closed_at: SearchType
    ticket_is_parent: SearchType
    ticket_ticket_id: SearchType
    ticket_created_at_from: SearchType
    ticket_created_at_to: SearchType


class HistorySearch(BaseSearchModel):
    ticket_history_id: SearchType
    ticket_history_event: SearchType
    ticket_history_user_id: SearchType
    ticket_history_ticket_id: SearchType
