from asyncio import create_task
from fastapi import APIRouter, HTTPException, Query, Depends
from fastapi.encoders import jsonable_encoder
from starlette.websockets import WebSocket, WebSocketDisconnect
from sqlalchemy import event
from settings import instance_auth
from manager.mixin.websocket import WebSocketManager
from manager.models import Response

from repository.case.model import Case
from repository.case.read import CaseRead
from repository.user.model import User
from repository.manager.model import Manager
from repository.read_status.read import NotificationStatusRead

from service.websocket.utils import (
    add_notification,
    add_read_status,
    get_ticket_by_id
)

websocket_router = APIRouter()
websocket_manager = WebSocketManager()


async def add_notification_and_broadcast(data, manager: Manager):
    notification = await add_notification(data)
    read_status = await add_read_status({
        "is_read": False,
        "notification_id": str(notification.id),
        "user_id": str(manager.user_id)
    })

    message = {
        "type": "notification",
        "data": jsonable_encoder(NotificationStatusRead.model_validate(read_status))
    }

    await websocket_manager.broadcast(message)


async def broadcast_case(target: Case):
    case_dict_validated = CaseRead.model_validate(Case(**target.__dict__)).model_dump()
    case_dict_validated["manager_id"] = None
    
    if hasattr(target, "manager_id"):
        case_dict_validated["manager_id"] = target.manager_id

    message = {
        "type": "case",
        "data": {
            "event": "insert",
            "data": jsonable_encoder(case_dict_validated)
        }
    }
    await websocket_manager.broadcast(message)


async def broadcast_manager(target: Manager):
    try:
        ticket = await get_ticket_by_id(target.ticket_id)

        data = {
            "model": "ticket",
            "name": f"Вы были вовлечены в заявку под номером {ticket.number}",
            "link_id": f"{str(target.ticket_id)}",
        }
        await add_notification_and_broadcast(data, target)
    
    except HTTPException:
        await websocket_manager.broadcast({ "detail": "Ticket id is wrong!" })


@event.listens_for(Case, 'after_insert')
def after_insert_listener(mapper, connection, target: Case):
    create_task(broadcast_case(target))


@event.listens_for(Manager, 'after_insert')
def after_insert_listener(mapper, connection, target: Manager):
    create_task(broadcast_manager(target))


@event.listens_for(User, 'after_update')
def after_update_listener(mapper, connection, target: User):
    for role in target.role:
        if role:
            message = {
                "type": "user",
                "data": {"user_id": str(target.id)}
            }
            create_task(websocket_manager.broadcast(message))


@websocket_router.websocket("/ws")
async def websocket_endpoint(
        websocket: WebSocket,
        token: str = Query(...),
        auth: instance_auth = Depends()):
    await websocket.accept()
    try:
        websocket_manager.add_connection(websocket)
        auth.jwt_required(auth_from="websocket", token=token)
        await websocket.send_text(str(Response(detail="Successfully Login").model_dump()))

        while True:
            await websocket.receive_text()

    except WebSocketDisconnect:
        websocket_manager.remove_connection(websocket)

    except Exception:
        websocket_manager.remove_connection(websocket)
        await websocket.close()
