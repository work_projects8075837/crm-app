from database import async_session

from repository.read_status.model import ReadStatus
from repository.ticket.model import Ticket
from repository.notification.model import Notification


notification_utils = Notification
read_status_utils = ReadStatus
ticket_utils = Ticket


async def add_notification(data) -> Notification:
    async with async_session() as session:
        return await notification_utils.post(session, data)


async def add_read_status(data) -> ReadStatus:
    async with async_session() as session:
        return await read_status_utils.post(session, data)


async def get_ticket_by_id(record_id) -> Ticket:
    async with async_session() as session:
        return await ticket_utils.query(session, Ticket, id=record_id)
