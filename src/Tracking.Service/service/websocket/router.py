from fastapi import APIRouter
from service.websocket.websocket import websocket_router

router = APIRouter()

router.include_router(websocket_router)
