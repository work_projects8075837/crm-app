from dotenv import load_dotenv
from fastapi_entity_auth import EntityAuth
from pydantic import Field
from pydantic_settings import BaseSettings

load_dotenv()


class DataBaseSettings(BaseSettings):
    host: str = Field(alias="DB_HOST")
    port: int = Field(alias="DB_PORT")
    name: str = Field(alias="DB_NAME")
    user: str = Field(alias="DB_USER")
    password: str = Field(alias="DB_PASS")


class SMTP(BaseSettings):
    host: str = Field(alias="SMTP_HOST")
    port: int = Field(alias="SMTP_PORT")
    password: str = Field(alias="SMTP_PASSWORD")
    user: str = Field(alias="SMTP_USER")
    base_url: str = Field(alias="BASE_URL")


class AuthSettings(BaseSettings):
    authjwt_secret_key: str = Field(alias="SECRET")


auth_settings = AuthSettings()


class UtilsEntityAuth(EntityAuth):
    secret_key = auth_settings.authjwt_secret_key


instance_auth = UtilsEntityAuth
