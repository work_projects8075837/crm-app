from datetime import timedelta, datetime
from typing import Union
from sqlmodel import SQLModel
from sqlmodel.ext.asyncio.session import AsyncSession
from settings import instance_auth
from manager.mixin.patch import MixinPatch
from manager.mixin.query import MixinQuery
from manager.models import ModelType
from fastapi import HTTPException, Depends, status
import bcrypt
import jwt
from settings import AuthSettings

config = AuthSettings()


class HandlersUser(SQLModel):
    @staticmethod
    async def create_token_password(id: str, expires_delta: timedelta = timedelta(minutes=15)):
        return jwt.encode(
            {
                "sub": str(id),
                "exp": datetime.now() + expires_delta
            },
            config.authjwt_secret_key,
            algorithm="HS256"
        )

    @classmethod
    async def check_password(cls: Union[ModelType, MixinQuery], session: AsyncSession, email: str, password: str):
        data = await cls.query(session, email=email)
        if not bcrypt.checkpw(password.encode(), data.password.encode()):
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid password")
        return data

    @classmethod
    async def update_password(cls: Union[ModelType, MixinPatch], session: AsyncSession, token: str, password: str):
        try:
            id = jwt.decode(token, config.authjwt_secret_key, algorithms=["HS256"]).get("sub")
        except jwt.ExpiredSignatureError:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Token expired")
        except jwt.InvalidTokenError:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token")

        await cls.patch(session, id, {"password": bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()})
