import smtplib
from email.message import EmailMessage

from settings import SMTP

config = SMTP()


def email_template_dashboard(username: str, token: str):
    email = EmailMessage()
    email['Subject'] = 'Флагман Поддержка'
    email['From'] = config.user
    email['To'] = username

    email.set_content(
        '<div>'
        f'<p>Здравствуйте, {username}! </p>'
        f'<p>{config.base_url}/reset?token={token}</p>'
        '</div>',
        subtype='html'
    )
    return email


def send_email(username: str, token: str):
    with smtplib.SMTP_SSL(config.host, config.port) as server:
        server.login(config.user, config.password)
        server.send_message(email_template_dashboard(username, token))
