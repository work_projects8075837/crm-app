from fastapi import APIRouter
from user.router.router_auth import router_auth
from user.router.router_permission import router_permission
from user.router.router_role import router_role
from user.router.router_user import router_user

router = APIRouter(prefix="/api/auth")

router.include_router(router_auth)
router.include_router(router_user)
router.include_router(router_role)
router.include_router(router_permission)
