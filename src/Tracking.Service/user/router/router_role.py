from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas
from manager.utils import permission, check, PermissionType
from repository.role.create import RoleCreate
from repository.role.model import Role
from repository.role.update import RoleUpdate
from repository.role.read import RoleRead
from repository.user.bond import UserWithRole

router_role = APIRouter(prefix="/role", tags=["Роли"])

role_utils = Role


@router_role.get("/")
async def get_role(
        request: Request,
        page: int = 1, offset: int = 50,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> pagination_schemas(RoleRead):
    await permission(auth.get_jwt_subject(), role_utils, PermissionType.READ)
    return await role_utils.get(session, request, page, offset)


@router_role.get("/{id}/")
async def get_role_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> UserWithRole:
    await permission(auth.get_jwt_subject(), role_utils, PermissionType.READ)
    return await role_utils.query(session, id=id)


@router_role.post("/")
async def post_role(
        data: RoleCreate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> UserWithRole:
    await permission(auth.get_jwt_subject(), role_utils, PermissionType.CREATE)
    return await role_utils.post(session, data.model_dump(exclude_none=True))


@router_role.patch("/{id}/")
async def patch_role(
        id: UUID4,
        data: RoleUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> UserWithRole:
    await permission(auth.get_jwt_subject(), role_utils, PermissionType.UPDATE)
    return await role_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_role.delete("/{id}/")
async def delete_role(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), role_utils, PermissionType.DELETE)
    return await role_utils.delete(session, id)
