from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas
from manager.utils import check, permission, PermissionType
from repository.permission.model import Permission
from repository.role.model import Role
from repository.user.bond import UserWithRole
from repository.user.create import UserCreate
from repository.user.model import User
from repository.user.update import UserUpdate
from user.utils.searchable_schemas import UserSearch, RoleSearch, PermissionSearch

router_user = APIRouter(prefix="/user", tags=["Пользователи"])

user_utils = User

search_pack = {
    User: UserSearch,
    Role: RoleSearch,
    Permission: PermissionSearch
}


@router_user.get("/", dependencies=[Depends(i) for i in search_pack.values()])
async def get_user(
        request: Request,
        page: int = 1, offset: int = 50,
        session: AsyncSession = Depends(get_async_session),
        # auth: instance_auth = Depends(check)
) -> pagination_schemas(UserWithRole):
    # await permission(auth.get_jwt_subject(), user_utils, PermissionType.READ)
    return await user_utils.get(session, request, page, offset, search_schemas=search_pack)


@router_user.get("/{id}/")
async def get_user_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> UserWithRole:
    await permission(auth.get_jwt_subject(), user_utils, PermissionType.READ)
    return await user_utils.query(session, id=id)


@router_user.post("/")
async def post_user(
        data: UserCreate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> UserWithRole:
    await permission(auth.get_jwt_subject(), user_utils, PermissionType.CREATE)
    return await user_utils.post(session, data.model_dump(exclude_none=True))


@router_user.patch("/{id}/")
async def patch_user(
        id: UUID4,
        data: UserUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> UserWithRole:
    await permission(auth.get_jwt_subject(), user_utils, PermissionType.UPDATE)
    return await user_utils.patch(session, id, data.model_dump(exclude_unset=True))


@router_user.delete("/{id}/")
async def delete_user(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), user_utils, PermissionType.DELETE)
    return await user_utils.delete(session, id)
