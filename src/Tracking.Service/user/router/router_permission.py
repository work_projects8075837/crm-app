from fastapi import APIRouter, Depends, Request
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response, pagination_schemas
from manager.utils import check, permission, PermissionType
from repository.permission.bond import PermissionWithRole
from repository.permission.create import PermissionCreate
from repository.permission.model import Permission
from repository.permission.update import PermissionUpdate

router_permission = APIRouter(prefix="/permission", tags=["Права"])

permission_utils = Permission


@router_permission.get("/")
async def get_permission(
        request: Request,
        page: int = 1, offset: int = 50,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> pagination_schemas(PermissionWithRole):
    await permission(auth.get_jwt_subject(), permission_utils, PermissionType.READ)
    return await permission_utils.get(session, request, page, offset)


@router_permission.get("/{id}/")
async def get_permission_by_id(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> PermissionWithRole:
    await permission(auth.get_jwt_subject(), permission_utils, PermissionType.READ)
    return await permission_utils.query(session, id=id)


@router_permission.post("/")
async def post_permission(
        data: PermissionCreate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> PermissionWithRole:
    await permission(auth.get_jwt_subject(), permission_utils, PermissionType.CREATE)
    return await permission_utils.post(session, data.model_dump(exclude_none=True))


@router_permission.patch("/{id}/")
async def patch_permission(
        id: UUID4, data: PermissionUpdate,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> PermissionWithRole:
    await permission(auth.get_jwt_subject(), permission_utils, PermissionType.UPDATE)
    return await permission_utils.patch(session, id, data.model_dump(exclude_none=True))


@router_permission.delete("/{id}/")
async def delete_permission(
        id: UUID4,
        session: AsyncSession = Depends(get_async_session),
        auth: instance_auth = Depends(check)
) -> Response:
    await permission(auth.get_jwt_subject(), permission_utils, PermissionType.DELETE)
    return await permission_utils.delete(session, id)
