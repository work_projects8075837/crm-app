from fastapi import APIRouter, status, HTTPException, BackgroundTasks
from fastapi.params import Depends
from sqlmodel.ext.asyncio.session import AsyncSession
from settings import instance_auth
from database import get_async_session
from manager.models import Response
from manager.utils import check
from repository.user.bond import UserWithRole
from repository.user.model import User
from repository.user.other import UserSignUp, LoginResponse, UserLogin, RefreshResponse, Recovery, ResetPassword
from repository.user.update import UserUpdate
from user.handlers.handlers_email import send_email

router_auth = APIRouter(tags=["Аутентификация"])

user_utils = User


@router_auth.post(
    "/signup/",
    status_code=status.HTTP_201_CREATED,
    summary="Регистрация пользователя",
    responses={
        status.HTTP_400_BAD_REQUEST: {"model": Response},
        status.HTTP_409_CONFLICT: {"model": Response},
    }
)
async def post_signup_router(data: UserSignUp, session: AsyncSession = Depends(get_async_session)) -> UserWithRole:
    return await user_utils.post(session, data.model_dump(exclude_none=True))


@router_auth.post(
    "/login/",
    status_code=status.HTTP_200_OK,
    summary="Аутентификация пользователя",
    response_model=LoginResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"model": Response},
        status.HTTP_409_CONFLICT: {"model": Response},
    }
)
async def post_login_router(
        data: UserLogin,
        auth: instance_auth = Depends(),
        session: AsyncSession = Depends(get_async_session)
) -> LoginResponse:

    entity = await user_utils.check_password(session, data.email, data.password)
    roles = [str(role.name) for role in entity.role]
    access_token = auth.create_access_token(subject=str(entity.id), user_claims={"roles": roles})
    refresh_token = auth.create_refresh_token(subject=str(entity.id))
    return LoginResponse(access=access_token, refresh=refresh_token)


@router_auth.post(
    "/refresh/",
    status_code=status.HTTP_200_OK,
    summary="Получение токена",
    responses={
        status.HTTP_400_BAD_REQUEST: {"model": Response},
        status.HTTP_401_UNAUTHORIZED: {"model": Response},
    }
)
async def post_refresh_router(
        auth: instance_auth = Depends(),
        session: AsyncSession = Depends(get_async_session)
) -> RefreshResponse:
    current_id = auth.get_jwt_subject()

    if current_id is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Token expired")

    entity = await user_utils.query(session, id=current_id)
    
    roles = [str(role.name) for role in entity.role]
    access_token = auth.create_access_token(subject=str(entity.id), user_claims={"roles": roles})
    return RefreshResponse(access=access_token)


@router_auth.get(
    "/me/",
    status_code=status.HTTP_200_OK,
    summary="Информация о текущем пользователе",
    responses={
        status.HTTP_401_UNAUTHORIZED: {"model": Response}
    },
)
async def get_me_router(
        auth: instance_auth = Depends(check),
        session: AsyncSession = Depends(get_async_session)
) -> UserWithRole:
    return await user_utils.query(session, id=auth.get_jwt_subject())


@router_auth.patch(
    "/me/",
    status_code=status.HTTP_200_OK,
    summary="Изменения информации о текущем пользователе",
    responses={
        status.HTTP_401_UNAUTHORIZED: {"model": Response}
    },
)
async def patch_me_router(
        data: UserUpdate,
        auth: instance_auth = Depends(check),
        session: AsyncSession = Depends(get_async_session)
) -> UserWithRole:
    return await user_utils.patch(session, auth.get_jwt_subject(), data.model_dump(exclude_unset=True))


@router_auth.post(
    "/recovery-password/",
    summary="Получения токена на почту для сброса пароля",
    responses={
        status.HTTP_404_NOT_FOUND: {"model": Response}
    },
)
async def post_recovery(
        tasks: BackgroundTasks,
        data: Recovery,
        session: AsyncSession = Depends(get_async_session)
) -> Response:
    entity = await user_utils.query(session, email=data.email)
    tasks.add_task(send_email, data.email, await user_utils.create_token_password(id=entity.id))
    return Response(detail="Отправлено электронное письмо для восстановления пароля")


@router_auth.post(
    "/reset-password/",
    summary="Сброс пароля по токену",
    responses={
        status.HTTP_404_NOT_FOUND: {"model": Response},
        status.HTTP_401_UNAUTHORIZED: {"model": Response},
    },
)
async def post_reset_password(
        data: ResetPassword,
        session: AsyncSession = Depends(get_async_session)
) -> Response:
    await user_utils.update_password(session, data.token, data.password)
    return Response(detail="Успешно!")
