from fastapi.responses import RedirectResponse
from fastapi import Request, Depends
from sqladmin import ModelView
from sqladmin.authentication import AuthenticationBackend
from database import async_session
from settings import instance_auth
from repository.permission.model import Permission
from repository.role.model import Role
from repository.user.model import User

user_utils = User


class AdminAuth(AuthenticationBackend):
    async def login(self, request: Request, authorize: instance_auth = Depends()) -> bool:
        form = await request.form()
        email, password = form["username"], form["password"]
        admin_id = "2aba5604-886a-4558-b4b3-f300d48f8a3a"

        async with async_session() as session:
            data = await user_utils.query(session, email=email)

            if not data:
                return False
            else:
                if not any([admin_id == str(role.id) for role in data.role]):
                    return False

            if await user_utils.check_password(session, password=password, email=email):
                request.session.update({"token": instance_auth(request).create_access_token(subject=email)})
                return True

        return False

    async def logout(self, request: Request) -> bool:
        request.session.clear()
        return True

    async def authenticate(self, request: Request) -> RedirectResponse | bool:
        token = request.session.get("token")
        if not token:
            return RedirectResponse(request.url_for("admin:login"), status_code=302)
        return True


class UserAdmin(ModelView, model=User):
    name = "Пользователь"
    name_plural = "Пользователи"
    category = "Авторизация"
    column_exclude_list = ["hashed_password"]
    column_details_exclude_list = ["hashed_password"]
    form_ajax_refs = {
        "role": {
            "fields": ("name",),
        }
    }


class RoleAdmin(ModelView, model=Role):
    name = "Роль"
    name_plural = "Роли"
    category = "Авторизация"
    column_list = "__all__"

    form_ajax_refs = {
        "user": {
            "fields": ("email",),
        }
    }


class PermissionAdmin(ModelView, model=Permission):
    name = "Права"
    name_plural = "Права"
    category = "Авторизация"
    column_list = "__all__"
