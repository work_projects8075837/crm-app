
from pydantic import BaseModel

from manager.models import SearchType


class PermissionSearch(BaseModel):
    permission_name: SearchType


class RoleSearch(BaseModel):
    role_name: SearchType


class UserSearch(BaseModel):
    user_name: SearchType
    user_email: SearchType
    user_password: SearchType
    user_status: SearchType
    user_position: SearchType
    user_phone: SearchType
    user_avatar: SearchType
    user_activate: SearchType
    user_blocking: SearchType

