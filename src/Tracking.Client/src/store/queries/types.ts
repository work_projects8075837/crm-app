export interface UseSearchProps {
    search?: string;
}

export interface UsePaginationQueryProps {
    page: number;
    offset: number;
}
