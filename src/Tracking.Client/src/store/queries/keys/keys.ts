import { caseKeys } from '@/services/case/case';
import { channelKeys } from '@/services/channel/channel';
import { characterProblemKeys } from '@/services/character_problem/character_problem';
import { commentKeys } from '@/services/comment/comment';
import { contactKeys } from '@/services/contact/contact';
import { counterPartyKeys } from '@/services/counterparty/counterparty';
import { directionKeys } from '@/services/direction/direction';
import { inventoryKeys } from '@/services/inventory/inventory';
import { jobKeys } from '@/services/job/job';
import { memberKeys } from '@/services/member/member';
import { notificationKeys } from '@/services/notification/notification';
import { phoneNumberKeys } from '@/services/phone_number/phone_number';
import { readStatusKeys } from '@/services/read_status/read_status';
import { salePointKeys } from '@/services/sale_point/sale_point';
import { softwareKeys } from '@/services/software/software';
import { statusKeys } from '@/services/status/status';
import { tagKeys } from '@/services/tag/tag';
import { ticketKeys } from '@/services/ticket/ticket';

export const GET_CURRENT_USER_KEY = 'GET_CURRENT_USER_KEY';

export const GET_SEARCH_USERS_KEY = 'GET_SEARCH_USERS_KEY';

/* ---------------------------------------------------------------------- */

export const GET_MEMBERS_BY_SALE_POINT_KEY = memberKeys.GET_ALL_KEY;

/* ---------------------------------------------------------------------- */

export const GET_All_TICKETS_KEY = ticketKeys.GET_ALL_KEY;
export const GET_SEARCH_TICKETS_KEY = ticketKeys.GET_ALL_KEY;
export const GET_ONE_TICKET_BY_ID_KEY = ticketKeys.GET_ONE_KEY;

export const TICKETS_ENDPOINTS_KEYS = [GET_All_TICKETS_KEY] as const;

/* ---------------------------------------------------------------------- */

export const GET_ALL_CHANNELS_KEY = channelKeys.GET_ALL_KEY;
export const GET_SEARCH_CHANNELS_KEY = channelKeys.GET_ALL_KEY;
export const GET_ONE_CHANNELS_BY_ID_KEY = channelKeys.GET_ONE_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_DIRECTIONS_KEY = directionKeys.GET_ALL_KEY;
export const GET_SEARCH_DIRECTIONS_KEY = directionKeys.GET_ALL_KEY;
export const GET_ONE_DIRECTION_BY_ID_KEY = directionKeys.GET_ONE_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_CHARACTER_PROBLEMS_KEY = characterProblemKeys.GET_ALL_KEY;
export const GET_SEARCH_CHARACTER_PROBLEMS_KEY =
    characterProblemKeys.GET_ALL_KEY;
export const GET_ONE_CHARACTER_PROBLEM_BY_ID_KEY =
    characterProblemKeys.GET_ONE_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_SALE_POINTS_KEY = salePointKeys.GET_ALL_KEY;
export const GET_SEARCH_SALE_POINTS_KEY = salePointKeys.GET_ALL_KEY;
export const GET_ONE_SALE_POINT_BY_ID_KEY = salePointKeys.GET_ONE_KEY;

export const SALE_POINTS_ENDPOINTS_KEYS = [GET_ALL_SALE_POINTS_KEY] as const;

/* ---------------------------------------------------------------------- */

export const GET_ALL_COUNTER_PARTIES_KEY = counterPartyKeys.GET_ALL_KEY;
export const GET_SEARCH_COUNTER_PARTIES_KEY = counterPartyKeys.GET_ALL_KEY;

export const GET_ONE_COUNTER_PARTY_BY_ID_KEY = counterPartyKeys.GET_ONE_KEY;
export const GET_COUNTER_PARTIES_BY_SALE_POINT_KEY =
    'GET_COUNTER_PARTIES_BY_SALE_POINT_KEY';

/* ---------------------------------------------------------------------- */

export const GET_ALL_CONTACTS_KEY = contactKeys.GET_ALL_KEY;
export const GET_CONTACTS_BY_SALE_POINT_KEY = salePointKeys.GET_ONE_KEY;

/* ---------------------------------------------------------------------- */

export const GET_CONTACTS_BY_COUNTER_PARTY_KEY =
    'GET_CONTACTS_BY_COUNTER_PARTY_KEY';
export const GET_SEARCH_CONTACTS_KEY = contactKeys.GET_ALL_KEY;
export const GET_ONE_CONTACT_BY_ID_KEY = contactKeys.GET_ONE_KEY;

/* ---------------------------------------------------------------------- */

export const GET_SEARCH_PHONE_NUMBERS_KEY = phoneNumberKeys.GET_ALL_KEY;

/* ---------------------------------------------------------------------- */

export const GET_INVENTORIES_BY_SALE_POINT_KEY =
    'GET_INVENTORIES_BY_SALE_POINT_KEY';
export const GET_ALL_INVENTORIES_KEY = inventoryKeys.GET_ALL_KEY;
export const GET_SEARCH_INVENTORIES_KEY = inventoryKeys.GET_ALL_KEY;
export const GET_ONE_INVENTORY_BY_ID_KEY = inventoryKeys.GET_ONE_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_SOFTWARE_KEY = softwareKeys.GET_ALL_KEY;
export const GET_SEARCH_SOFTWARE_KEY = softwareKeys.GET_ALL_KEY;

/* ---------------------------------------------------------------------- */

export const GET_SEARCH_TAGS_KEY = tagKeys.GET_ALL_KEY;
export const GET_ALL_TAGS_KEY = tagKeys.GET_ALL_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_JOBS_KEY = jobKeys.GET_ALL_KEY;
export const GET_SEARCH_JOBS_KEY = jobKeys.GET_ALL_KEY;
export const GET_ONE_JOB_BY_ID_KEY = jobKeys.GET_ONE_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_STATUSES_KEY = statusKeys.GET_ALL_KEY;
export const GET_SEARCH_STATUSES_KEY = statusKeys.GET_ALL_KEY;
export const GET_STATUSES_BY_DIRECTION_KEY = 'GET_STATUSES_BY_DIRECTION_KEY';

/* ---------------------------------------------------------------------- */

export const GET_BREADCRUMBS_KEY = 'GET_BREADCRUMBS_KEY';

/* ---------------------------------------------------------------------- */

export const GET_ALL_CASES_KEY = caseKeys.GET_ALL_KEY;
export const GET_ONE_CASE_BY_ID_KEY = caseKeys.GET_ONE_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_NOTIFICATIONS_KEY = notificationKeys.GET_ALL_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_READ_STATUSES_KEY = readStatusKeys.GET_ALL_KEY;

/* ---------------------------------------------------------------------- */

export const GET_ALL_COMMENTS_KEY = commentKeys.GET_ALL_KEY;
