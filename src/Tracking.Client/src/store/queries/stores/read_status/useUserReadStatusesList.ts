import { useAuthQuery } from '@/components/Providers/AuthGuard/useAuthQuery';
import { readStatusHooks } from '@/services/read_status/read_status';
import { ReadStatusFilter } from '@/services/read_status/types';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useUserReadStatusesList = (
    filter?: ReadStatusFilter,
    options?: IOptionsProps,
) => {
    const { data } = useAuthQuery();

    return readStatusHooks.useList(
        {
            read_status_user_id: data?.userId,
            ...filter,
        },
        options,
    );
};
