import { salePointApi } from '@/services/sale_point/sale_point';
import { IOptionsProps } from '../base/useAuthorizedQuery';
import { useDirectoryBreadCrumbs } from '../base/useDirectoryBreadCrumbs';

export const useSalePointBreadCrumbs = (options?: IOptionsProps) => {
    const query = useDirectoryBreadCrumbs({
        parentName: 'sale_point',
        idName: 'salePointId',
        getOne: salePointApi.getOne,
        options,
    });

    return query;
};
