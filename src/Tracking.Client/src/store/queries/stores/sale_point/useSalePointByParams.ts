import { salePointHooks } from '@/services/sale_point/sale_point';
import { useParams } from 'react-router-dom';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useSalePointByParams = (options?: IOptionsProps) => {
    const { salePointId } = useParams();
    const query = salePointHooks.useOne(salePointId, options);
    return query;
};
