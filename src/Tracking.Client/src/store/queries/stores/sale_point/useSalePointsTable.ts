import { salePointHooks } from '@/services/sale_point/sale_point';
import { useParams } from 'react-router-dom';
import { UsePaginationQueryProps } from '../../types';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useSalePointsTable = (
    pag: UsePaginationQueryProps,
    options?: IOptionsProps,
) => {
    const { salePointId } = useParams();
    const query = salePointHooks.useList(
        { ...pag, sale_point_sale_point_id: salePointId || null },
        options,
    );
    return query;
};
