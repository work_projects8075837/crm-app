import { jobHooks } from '@/services/job/job';
import { useParams } from 'react-router-dom';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useJobByParams = (options?: IOptionsProps) => {
    const { jobId } = useParams();
    const query = jobHooks.useOne(jobId, options);
    return query;
};
