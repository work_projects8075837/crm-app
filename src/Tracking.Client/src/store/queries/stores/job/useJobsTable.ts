import { jobHooks } from '@/services/job/job';
import { useParams } from 'react-router-dom';
import { IOptionsProps } from '../base/useAuthorizedQuery';

type UseJobsListProps = typeof jobHooks.useList;

export const useJobsTable: UseJobsListProps = (
    filter,
    options?: IOptionsProps,
) => {
    const { jobId } = useParams();
    const q = jobHooks.useList(
        { ...filter, job_job_id: jobId || null },
        options,
    );
    return q;
};
