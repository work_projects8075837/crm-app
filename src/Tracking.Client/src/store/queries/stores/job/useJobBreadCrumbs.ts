import { jobApi } from '@/services/job/job';
import { IOptionsProps } from '../base/useAuthorizedQuery';
import { useDirectoryBreadCrumbs } from '../base/useDirectoryBreadCrumbs';

export const useJobBreadCrumbs = (options?: IOptionsProps) => {
    const q = useDirectoryBreadCrumbs({
        idName: 'jobId',
        parentName: 'job',
        getOne: jobApi.getOne,
        options,
    });
    return q;
};
