import { characterProblemApi } from '@/services/character_problem/character_problem';
import { IOptionsProps } from '../base/useAuthorizedQuery';
import { useDirectoryBreadCrumbs } from '../base/useDirectoryBreadCrumbs';

export const useCharacterProblemBreadCrumbs = (options?: IOptionsProps) => {
    const query = useDirectoryBreadCrumbs({
        getOne: characterProblemApi.getOne,
        idName: 'characterProblemId',
        parentName: 'character_problem',
        options,
    });
    return query;
};
