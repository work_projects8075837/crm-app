import { characterProblemHooks } from '@/services/character_problem/character_problem';
import { useParams } from 'react-router-dom';

export const useCharacterProblemsTable: typeof characterProblemHooks.useList = (
    filter,
    options,
) => {
    const { characterProblemId } = useParams();
    const query = characterProblemHooks.useList(
        {
            ...filter,
            character_problem_character_problem_id: characterProblemId || null,
        },
        options,
    );
    return query;
};
