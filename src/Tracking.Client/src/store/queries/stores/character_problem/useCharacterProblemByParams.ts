import { characterProblemHooks } from '@/services/character_problem/character_problem';
import { useParams } from 'react-router-dom';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useCharacterProblemByParams = (options?: IOptionsProps) => {
    const { characterProblemId } = useParams();
    const query = characterProblemHooks.useOne(characterProblemId, options);
    return query;
};
