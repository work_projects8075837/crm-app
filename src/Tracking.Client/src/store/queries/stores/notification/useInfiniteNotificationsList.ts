import { useAuthQuery } from '@/components/Providers/AuthGuard/useAuthQuery';
import { notificationApi } from '@/services/notification/notification';
import { NotificationFilter } from '@/services/notification/types';
import { GET_ALL_NOTIFICATIONS_KEY } from '@/store/queries/keys/keys';
import { useInfiniteQuery } from '@tanstack/react-query';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useInfiniteNotificationsList = (
    filter?: NotificationFilter,
    options?: IOptionsProps,
) => {
    const { data } = useAuthQuery();
    return useInfiniteQuery({
        queryKey: [
            GET_ALL_NOTIFICATIONS_KEY,
            { userId: data?.userId, ...filter },
        ],
        enabled: !!data?.userId,
        ...options,
        queryFn: async ({ pageParam = 1 }) => {
            return await notificationApi
                .getAll({
                    offset: 12,
                    page: pageParam,
                    ...filter,
                })
                .then((res) => res.data);
        },
        getNextPageParam: (lastPage, pages) =>
            lastPage.next ? pages.length + 1 : undefined,
    });
};
