import { useAuthQuery } from '@/components/Providers/AuthGuard/useAuthQuery';
import { IOptionsProps } from '../base/useAuthorizedQuery';
import { useInfiniteNotificationsList } from './useInfiniteNotificationsList';

export const useUserNotifications = (options?: IOptionsProps) => {
    const authQ = useAuthQuery();
    const q = useInfiniteNotificationsList(
        {
            read_status_user_id: authQ.data?.userId,
        },

        options,
    );

    return {
        ...q,
        items: q.data?.pages
            .map((p) => p.data)
            ?.reduce((notes, notePage) => {
                return [...notes, ...notePage];
            }, []),
    };
};
