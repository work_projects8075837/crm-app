import { ticketApi } from '@/services/ticket/ticket';
import { IOptionsProps } from '../base/useAuthorizedQuery';
import { useDirectoryBreadCrumbs } from '../base/useDirectoryBreadCrumbs';

export const useTicketBreadCrumbs = (options?: IOptionsProps) => {
    const query = useDirectoryBreadCrumbs({
        getOne: ticketApi.getOne,
        idName: 'ticketId',
        parentName: 'ticket',
        options,
    });

    return {
        ...query,
        data: query.data?.map((t) => ({
            ...t,
            name: `Заявка ${t.number}`,
        })),
    };
};
