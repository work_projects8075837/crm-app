import { useTicketList } from '@/services/ticket/ticket';
import { useParams } from 'react-router-dom';
import { UsePaginationQueryProps } from '../../types';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useTicketsTable = (
    pag: UsePaginationQueryProps,
    options?: IOptionsProps,
) => {
    const { ticketId } = useParams();
    const query = useTicketList(
        { ...pag, ticket_ticket_id: ticketId || null },
        options,
    );
    return query;
};
