import { useTicketById } from '@/services/ticket/ticket';
import { useParams } from 'react-router-dom';

export const useTicketByParams = () => {
    const { ticketId } = useParams();
    const query = useTicketById(ticketId);
    return query;
};
