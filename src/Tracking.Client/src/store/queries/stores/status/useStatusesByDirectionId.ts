import { directionApi } from '@/services/direction/direction';
import { GET_STATUSES_BY_DIRECTION_KEY } from '../../keys/keys';
import { useAuthorizedQuery } from '../base/useAuthorizedQuery';

interface UseStatusesByDirectionIdProps {
    directionId?: string;
}

export const useStatusesByDirectionId = (directionId?: string) => {
    const query = useAuthorizedQuery({
        mainKey: GET_STATUSES_BY_DIRECTION_KEY,
        queryFn: async () => {
            if (!directionId) throw Error();
            return await directionApi.getOne(directionId).then((res) => {
                return res.data.status;
            });
        },
        variablesKeys: { directionId },
        enabled: !!directionId,
    });

    return query;
};
