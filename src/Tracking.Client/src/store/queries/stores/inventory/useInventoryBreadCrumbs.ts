import { inventoryApi } from '@/services/inventory/inventory';
import { IOptionsProps } from '../base/useAuthorizedQuery';
import { useDirectoryBreadCrumbs } from '../base/useDirectoryBreadCrumbs';

export const useInventoryBreadCrumbs = (options?: IOptionsProps) => {
    const query = useDirectoryBreadCrumbs({
        getOne: inventoryApi.getOne,
        idName: 'inventoryId',
        parentName: 'parent',
        options: options,
    });

    return query;
};
