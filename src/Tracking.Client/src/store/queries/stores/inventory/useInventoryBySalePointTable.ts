import { inventoryHooks } from '@/services/inventory/inventory';
import { useParams } from 'react-router-dom';

export const useInventoryTable: typeof inventoryHooks.useList = (
    filter,
    options,
) => {
    const { inventoryId, salePointId } = useParams();
    const query = inventoryHooks.useList(
        {
            ...filter,
            inventory_inventory_id: inventoryId || null,
            inventory_sale_point_id: salePointId,
        },
        options,
    );
    return query;
};
