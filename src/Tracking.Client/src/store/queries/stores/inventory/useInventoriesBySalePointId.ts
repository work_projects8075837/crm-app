import { salePointApi } from '@/services/sale_point/sale_point';
import { GET_INVENTORIES_BY_SALE_POINT_KEY } from '../../keys/keys';
import { useAuthorizedQuery } from '../base/useAuthorizedQuery';

export const useInventoriesBySalePointId = (salePointId?: string) => {
    const query = useAuthorizedQuery({
        mainKey: GET_INVENTORIES_BY_SALE_POINT_KEY,
        variablesKeys: { salePointId },
        enabled: !!salePointId,
        queryFn: async () => {
            if (!salePointId) {
                throw Error();
            }
            return await salePointApi.getOne(salePointId).then((res) => {
                return res.data.inventory;
            });
        },
    });

    return query;
};
