import { inventoryHooks } from '@/services/inventory/inventory';
import { useParams } from 'react-router-dom';

export const useSearchInventory = (search?: string, bySalePoint = true) => {
    const { salePointId } = useParams();

    return inventoryHooks.useSearch(search, {
        inventory_sale_point_id: bySalePoint ? salePointId : undefined,
    });
};
