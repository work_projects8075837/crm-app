import { inventoryHooks } from '@/services/inventory/inventory';
import { useParams } from 'react-router-dom';

export const useInventoryByParams = () => {
    const { inventoryId } = useParams();
    const query = inventoryHooks.useOne(inventoryId);
    return query;
};
