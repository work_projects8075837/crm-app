import { directionHooks } from '@/services/direction/direction';
import { useParams } from 'react-router-dom';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useDirectionByParams = (options?: IOptionsProps) => {
    const { directionId } = useParams();
    const query = directionHooks.useOne(directionId, options);
    return query;
};
