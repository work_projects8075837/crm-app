import { counterPartyHooks } from '@/services/counterparty/counterparty';
import { useParams } from 'react-router-dom';
import { IOptionsProps } from '../base/useAuthorizedQuery';

export const useCounterPartyByParams = (options?: IOptionsProps) => {
    const { counterPartyId } = useParams();
    const query = counterPartyHooks.useOne(counterPartyId, options);
    return query;
};
