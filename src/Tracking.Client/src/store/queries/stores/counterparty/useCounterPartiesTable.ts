import { counterPartyHooks } from '@/services/counterparty/counterparty';
import { useParams } from 'react-router-dom';

export const useCounterPartiesTable: typeof counterPartyHooks.useList = (
    filter,
    options,
) => {
    const { counterPartyId } = useParams();
    const query = counterPartyHooks.useList(
        { ...filter, counterparty_counterparty_id: counterPartyId || null },
        options,
    );

    return query;
};
