import { counterPartyApi } from '@/services/counterparty/counterparty';
import { IOptionsProps } from '../base/useAuthorizedQuery';
import { useDirectoryBreadCrumbs } from '../base/useDirectoryBreadCrumbs';

export const useCounterPartyBreadCrumbs = (options?: IOptionsProps) => {
    const query = useDirectoryBreadCrumbs({
        getOne: counterPartyApi.getOne,
        idName: 'counterPartyId',
        parentName: 'counterparty',
        options,
    });
    return query;
};
