import { salePointApi } from '@/services/sale_point/sale_point';
import { GET_COUNTER_PARTIES_BY_SALE_POINT_KEY } from '../../keys/keys';
import { useAuthorizedQuery } from '../base/useAuthorizedQuery';

interface UseCounterPartyBySalePoint {
    salePointId?: string;
}

export const useCounterPartiesBySalePoint = ({
    salePointId,
}: UseCounterPartyBySalePoint) => {
    const query = useAuthorizedQuery({
        mainKey: GET_COUNTER_PARTIES_BY_SALE_POINT_KEY,
        variablesKeys: { salePointId },
        queryFn: async () => {
            if (!salePointId) throw Error();
            return await salePointApi.getOne(salePointId).then((res) => {
                return res.data.counterparty;
            });
        },
        enabled: !!salePointId,
    });

    return query;
};
