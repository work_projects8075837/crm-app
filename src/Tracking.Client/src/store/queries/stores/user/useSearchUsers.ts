import { useAuthQuery } from '@/components/Providers/AuthGuard/useAuthQuery';
import { UseSearchProps } from '../../types';
import { useDebounce } from '@/hooks/useDebounce';
import { useQuery } from '@tanstack/react-query';
import { GET_SEARCH_USERS_KEY } from '../../keys/keys';
import { userService } from '@/services/user/user';

export const useSearchUsers = ({ search }: UseSearchProps) => {
    const { data } = useAuthQuery();

    const debouncedSearch = useDebounce(search, 500);

    const query = useQuery({
        queryKey: [
            GET_SEARCH_USERS_KEY,
            { userId: data?.userId, debouncedSearch },
        ],
        enabled: !!data?.userId,
        queryFn: async () => {
            return userService.getAll(debouncedSearch).then((res) => {
                return res.data;
            });
        },
    });

    return query;
};
