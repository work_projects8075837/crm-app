import { userService } from '@/services/user/user';
import { GET_CURRENT_USER_KEY } from '../../keys/keys';
import { IOptionsProps, useAuthorizedQuery } from '../base/useAuthorizedQuery';

export const useCurrentUser = (options?: IOptionsProps) => {
    const query = useAuthorizedQuery({
        mainKey: GET_CURRENT_USER_KEY,
        queryFn: async () => {
            return await userService.getCurrentUser().then((res) => res.data);
        },
        ...options,
    });

    return query;
};
