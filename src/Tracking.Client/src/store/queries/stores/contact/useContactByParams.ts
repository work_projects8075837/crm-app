import { contactHooks } from '@/services/contact/contact';
import { useParams } from 'react-router-dom';

export const useContactByParams = () => {
    const { contactId } = useParams();
    const query = contactHooks.useOne(contactId);
    return query;
};
