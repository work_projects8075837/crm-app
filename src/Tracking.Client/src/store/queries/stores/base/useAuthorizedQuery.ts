import { useAuthQuery } from '@/components/Providers/AuthGuard/useAuthQuery';
import { useQuery } from '@tanstack/react-query';

export type TVariablesKeys = {
    [key: string]: unknown | undefined;
};

export interface IOptionsProps {
    refetchOnMount?: boolean;
}

interface UseAuthorizedQueryProps<R> extends IOptionsProps {
    mainKey: string | symbol;
    variablesKeys?: TVariablesKeys;
    queryFn: () => Promise<R>;
    enabled?: boolean;
}

export const useAuthorizedQuery = <R>({
    refetchOnMount = false,
    mainKey,
    variablesKeys,
    queryFn,
    enabled,
}: UseAuthorizedQueryProps<R>) => {
    const { data } = useAuthQuery();

    const query = useQuery({
        queryKey: [
            mainKey,
            {
                userId: data?.userId,
                ...variablesKeys,
            },
        ],
        enabled: !!data?.userId && enabled,
        queryFn,
        refetchOnMount,
    });

    return query;
};
