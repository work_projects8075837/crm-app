import { TVariablesKeys, useAuthorizedQuery } from './useAuthorizedQuery';

export type TPagination = {
    page?: number;
    offset?: number;
};

interface UsePaginationQueryProps<R> {
    refetchOnMount?: boolean;
    pagination?: TPagination;
    variablesKeys?: TVariablesKeys;
    mainKey: string;
    queryFn: (pagination?: TPagination) => Promise<R>;
}

export const usePaginationQuery = <R>({
    mainKey,
    variablesKeys,
    queryFn,
    pagination,
    refetchOnMount,
}: UsePaginationQueryProps<R>) => {
    const query = useAuthorizedQuery({
        queryFn: async () =>
            await queryFn({
                page: pagination?.page,
                offset: pagination?.offset,
            }),
        mainKey,
        variablesKeys: {
            page: pagination?.page,
            offset: pagination?.offset,
            ...variablesKeys,
        },
        refetchOnMount,
    });

    return query;
};
