import { AxiosResponse } from 'axios';
import { useParams } from 'react-router-dom';
import { GET_BREADCRUMBS_KEY } from '../../keys/keys';
import { IOptionsProps, useAuthorizedQuery } from './useAuthorizedQuery';

export interface IChildEntity {
    id: string;
    name?: string;
    [key: string]: any | undefined;
}

export interface UseDirectoryBreadCrumbsProps<E extends IChildEntity> {
    idName: string;
    parentName: string;
    getOne: (id: string) => Promise<AxiosResponse<E>>;
    options?: IOptionsProps;
}

export const useDirectoryBreadCrumbs = <E extends IChildEntity>({
    idName,
    parentName,
    getOne,
    options,
}: UseDirectoryBreadCrumbsProps<E>) => {
    const params = useParams();
    const query = useAuthorizedQuery({
        mainKey: `${parentName}_${GET_BREADCRUMBS_KEY}`,
        variablesKeys: { parentId: params[idName] },
        ...options,
        queryFn: async () => {
            const id = params[idName];
            if (!id) {
                return [];
            }
            const entity = await getOne(id).then((res) => res.data);

            const breadcrumbs: E[] = [];

            const getParentEntityOrNot = async (entity: E) => {
                const parentId = entity[`${parentName}_id`];
                if (parentId) {
                    const parentEntity = await getOne(parentId).then(
                        (res) => res.data,
                    );
                    breadcrumbs.push(parentEntity);
                    await getParentEntityOrNot(parentEntity);
                }
            };

            breadcrumbs.push(entity);
            await getParentEntityOrNot(entity);

            return breadcrumbs.reverse();
        },
    });

    return { ...query, current: query.data?.[query.data?.length - 1] };
};
