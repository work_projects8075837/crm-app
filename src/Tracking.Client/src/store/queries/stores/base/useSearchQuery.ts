import { useDebounce } from '@/hooks/useDebounce';
import { TVariablesKeys, useAuthorizedQuery } from './useAuthorizedQuery';

interface UseSearchQueryProps<R> {
    mainKey: string;
    search?: string;
    searchFn: (search?: string) => Promise<R>;
    variablesKeys?: TVariablesKeys;
}

export const useSearchQuery = <R>({
    mainKey,
    search,
    searchFn,
    variablesKeys,
}: UseSearchQueryProps<R>) => {
    const debouncedSearch = useDebounce(search, 500);

    const query = useAuthorizedQuery({
        mainKey,
        variablesKeys: { debouncedSearch, ...variablesKeys },
        queryFn: async () => {
            return await searchFn(debouncedSearch);
        },
    });

    return query;
};
