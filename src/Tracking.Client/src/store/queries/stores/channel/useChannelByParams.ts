import { channelHooks } from '@/services/channel/channel';
import { useParams } from 'react-router-dom';

export const useChannelByParams = () => {
    const { channelId } = useParams();
    const q = channelHooks.useOne(channelId);
    return q;
};
