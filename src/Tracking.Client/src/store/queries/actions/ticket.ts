import { ICurrentTicket, ITicket } from '@/services/ticket/types';
import { queryClient } from '../client/client';
import { GET_ONE_TICKET_BY_ID_KEY } from '../keys/keys';
import { getUserId } from './user';

type TQueryTicket = Partial<ICurrentTicket> & { status_id?: string };
type TicketCallback = (currentTicket: ITicket) => ITicket;

export const updateTicket = (
    ticketData: TQueryTicket | TicketCallback,
    ticketId?: string,
) => {
    queryClient.setQueryData<ITicket>(
        [GET_ONE_TICKET_BY_ID_KEY, { userId: getUserId(), id: ticketId }],
        (currentTicket) => {
            return currentTicket
                ? typeof ticketData === 'function'
                    ? ticketData(currentTicket)
                    : { ...currentTicket, ...ticketData }
                : currentTicket;
        },
    );
};
