import { queryClient } from '../client/client';

export const totalClearKey = (key: string | symbol) => {
    queryClient.invalidateQueries({
        queryKey: [key],
        exact: false,
        refetchType: 'all',
        type: 'all',
    });
    queryClient.removeQueries({
        queryKey: [key],
        exact: false,
        type: 'all',
    });
};

export const invalidateKey = (key: string | symbol) => {
    queryClient.invalidateQueries({
        queryKey: [key],
        exact: false,
        refetchType: 'all',
        type: 'all',
    });
};

export const clearKey = (key: string | symbol) => {
    queryClient.removeQueries({
        queryKey: [key],
        exact: false,
        type: 'all',
    });
};
