import { IComment } from '@/services/comment/types';
import { PaginatedData } from '@/services/types';
import { queryClient } from '../client/client';
import { GET_ALL_COMMENTS_KEY } from '../keys/keys';
import { getUserId } from './user';

export const addComment = (comment: IComment, ticketId: string) => {
    queryClient.setQueryData<PaginatedData<IComment[]>>(
        [
            GET_ALL_COMMENTS_KEY,
            {
                userId: getUserId(),
                ticket_id: ticketId,
            },
        ],
        (prev) => {
            return prev ? { ...prev, data: [...prev.data, comment] } : prev;
        },
    );
};

export const removeComment = (id: string, ticketId: string) => {
    queryClient.setQueryData<PaginatedData<IComment[]>>(
        [
            GET_ALL_COMMENTS_KEY,
            {
                userId: getUserId(),
                ticket_id: ticketId,
            },
        ],
        (prev) => {
            return prev
                ? { ...prev, data: prev.data.filter((c) => c.id !== id) }
                : prev;
        },
    );
};

export const updateComment = (
    id: string,
    data: Partial<IComment>,
    ticketId?: string,
) => {
    queryClient.setQueryData<PaginatedData<IComment[]>>(
        [
            GET_ALL_COMMENTS_KEY,
            {
                userId: getUserId(),
                ticket_id: ticketId,
            },
        ],
        (prev) => {
            return prev
                ? {
                      ...prev,
                      data: prev.data.map((c) => {
                          if (c.id === id) {
                              return { ...c, ...data };
                          }
                          return c;
                      }),
                  }
                : prev;
        },
    );
};
