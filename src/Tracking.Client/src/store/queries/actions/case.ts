import { CaseFilter, ICase } from '@/services/case/types';
import { PaginatedData } from '@/services/types';
import { queryClient } from '../client/client';
import { GET_ALL_CASES_KEY } from '../keys/keys';
import { getUserId } from './user';

export const updateCases = (newCases: ICase[], filters?: CaseFilter) => {
    queryClient.setQueryData<PaginatedData<ICase[]>>(
        [GET_ALL_CASES_KEY, { userId: getUserId(), ...filters }],
        (currentCases) => {
            return currentCases
                ? {
                      ...currentCases,
                      data: newCases,
                  }
                : currentCases;
        },
    );
};
