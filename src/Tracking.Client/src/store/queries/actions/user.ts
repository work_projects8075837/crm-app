import { IUser } from '@/services/user/types';
import { queryClient } from '../client/client';
import { GET_AUTHORIZATION_KEY } from '../keys/auth';
import { GET_CURRENT_USER_KEY } from '../keys/keys';

export const updateUser = (userData: Partial<IUser>, userId?: string) => {
    queryClient.setQueryData<IUser>(
        [GET_CURRENT_USER_KEY, { userId }],
        (currentUser) => {
            return currentUser ? { ...currentUser, ...userData } : currentUser;
        },
    );
};

export const getUserId = () =>
    queryClient.getQueryData<{ userId: string }>([GET_AUTHORIZATION_KEY])
        ?.userId;
