import { makeAutoObservable } from 'mobx';

export class TimerState {
    nowDate = new Date().getTime();
    nowCounter: any = undefined;
    constructor() {
        makeAutoObservable(this);
    }
    setNowDate(t: number) {
        this.nowDate = t;
    }
    start() {
        this.nowCounter = setInterval(() => {
            this.setNowDate(new Date().getTime());
        }, 1000);
    }

    clear() {
        clearInterval(this.nowCounter);
    }
}
