import { makeAutoObservable } from 'mobx';

export class PaginationHandler {
    STORAGE_PREFIX: string;
    page = 1;
    offset: number;

    constructor(STORAGE_PREFIX: string) {
        this.STORAGE_PREFIX = STORAGE_PREFIX;
        const storagePrefix = localStorage.getItem(
            `${STORAGE_PREFIX}_pagination_offset`,
        );
        if (storagePrefix) {
            this.offset = Number(storagePrefix);
        } else {
            this.offset = 20;
            localStorage.setItem(`${STORAGE_PREFIX}_pagination_offset`, '20');
        }
        makeAutoObservable(this);
    }

    setPage(page: number) {
        this.page = page;
    }

    setOffset(offset: number) {
        this.offset = offset;
        localStorage.setItem(
            `${this.STORAGE_PREFIX}_pagination_offset`,
            `${offset}`,
        );
    }
}
