import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import { makeAutoObservable } from 'mobx';

export class CheckEntitiesHandler<E extends Entity = Entity> {
    isAllChecked = false;
    entities: E[] = [];
    checkedEntities: E[] = [];

    constructor() {
        makeAutoObservable(this);
    }

    set(entities: E[]) {
        this.entities = [...entities];
    }

    check(entity: E) {
        this.checkedEntities.unshift(entity);
    }

    checkAll() {
        this.checkedEntities = [...this.entities];
        this.isAllChecked = true;
    }

    uncheck(id: string) {
        this.checkedEntities = this.checkedEntities.filter(
            (entity) => entity.id !== id,
        );
        this.isAllChecked = false;
    }

    uncheckAll() {
        this.checkedEntities = [];
        this.isAllChecked = false;
    }

    toggle(entity: E) {
        if (this.isChecked(entity.id)) {
            this.uncheck(entity.id);
        } else {
            this.check(entity);
        }
    }

    toggleAll() {
        if (this.isAllChecked) {
            this.uncheckAll();
        } else {
            this.checkAll();
        }
    }

    isChecked(id: string) {
        return this.checkedEntities.some((entity) => entity.id === id);
    }
}
