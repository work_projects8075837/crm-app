import { makeAutoObservable } from 'mobx';

interface iDirectoryEntity {
    id: string | null;
}

export class DirectoryHandler {
    id: string | null = null;

    constructor() {
        makeAutoObservable(this);
    }

    setDirectory(id: string | null) {
        this.id = id;
    }
}
