import { createRef } from 'react';
import { OpenHandler } from './OpenHandler';

export class FilterOpener {
    public ref = createRef<HTMLDivElement>();
    constructor(public openHandler: OpenHandler) {}

    open() {
        const elem = this.ref.current;
        if (elem) {
            elem.style.transitionDuration = '0';

            elem.style.opacity = '0';
            elem.style.position = 'absolute';
            elem.style.height = `max-content`;
            const h = elem.clientHeight;

            elem.style.height = `0`;
            elem.style.position = 'static';
            elem.style.opacity = '1';

            elem.style.transitionDuration = '0.8s';
            setTimeout(() => {
                elem.style.height = `${h}px`;
                setTimeout(() => {
                    elem.style.height = '100%';
                }, 800);
            });
        }
        this.openHandler.open();
    }

    close() {
        const elem = this.ref.current;
        if (elem) {
            elem.style.transitionDuration = '0';

            const h = elem.clientHeight;

            setTimeout(() => {
                elem.style.height = `${h}px`;
                setTimeout(() => {
                    elem.style.transitionDuration = '0.8s';
                    setTimeout(() => {
                        elem.style.height = '0';
                    }, 800);
                });
            }, 10);
        }
        this.openHandler.close();
    }

    toggle() {
        if (this.openHandler.isOpen) {
            this.close();
        } else {
            this.open();
        }
    }
}
