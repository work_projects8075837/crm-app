import { makeAutoObservable } from 'mobx';
import { createRef } from 'react';

export class MenuHandlerState<E extends HTMLElement> {
    ref = createRef<E>();
    isOpen = false;

    constructor(private side: 'right' | 'left' = 'left') {
        makeAutoObservable(this);
    }

    open() {
        if (this.ref.current) {
            this.ref.current.style.transform = 'translateX(0)';
            this.isOpen = true;
        }
    }

    close() {
        if (this.ref.current) {
            this.ref.current.style.transform = `translateX(${
                this.side === 'left' ? '-100%' : '100%'
            })`;
            this.isOpen = false;
        }
    }
}
