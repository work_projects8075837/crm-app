import { makeAutoObservable } from 'mobx';
import { OpenHandler } from './OpenHandler.ts';

class ResultJobModalState {
    jobId: string | null = null;
    jobName: string | null = null;
    constructor(public openHandler: OpenHandler) {
        makeAutoObservable(this);
    }
    setTicketJob(id: string, name: string) {
        this.jobId = id;
        this.jobName = name;
    }
    clear() {
        this.jobId = null;
        this.jobName = null;
    }
}

export const resultJobModalState = new ResultJobModalState(
    new OpenHandler(false),
);
