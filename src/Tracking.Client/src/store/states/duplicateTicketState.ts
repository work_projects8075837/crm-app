import { ITicketForm } from '@/components/Fields/EntitiesFields/ticket.ts';
import { makeAutoObservable } from 'mobx';

class DuplicateTicketState {
    ticketForm: ITicketForm | null = null;
    constructor() {
        makeAutoObservable(this);
    }
    setTicketForm(ticketForm: ITicketForm) {
        this.ticketForm = ticketForm;
    }
    clear() {
        this.ticketForm = null;
    }
}

export const duplicateTicketState = new DuplicateTicketState();
