import { makeAutoObservable } from 'mobx';

export class TabHandler {
    constructor(public tab: string) {
        makeAutoObservable(this);
    }

    setTab(tab: string) {
        this.tab = tab;
    }
}
