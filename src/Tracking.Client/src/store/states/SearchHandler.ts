import { makeAutoObservable } from 'mobx';

export class ValueHandler<T = string> {
    value: T | null;

    constructor(v: T | null = null) {
        makeAutoObservable(this);
        this.value = v;
    }

    setValue(v: T | null) {
        if (this.value !== v) this.value = v;
    }
}
