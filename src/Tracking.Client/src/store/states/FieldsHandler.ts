import { makeAutoObservable } from 'mobx';
import React from 'react';

export interface IField<TEntity> {
    key: string;
    name: string;
    getValue: (data: TEntity) => React.ReactNode;
    size: string;
}

export class FieldsHandler<TEntity, TKey = string> {
    fields: IField<TEntity>[] = [];
    selectedFields: TKey[] = [];
    step = 0;
    private firstWidth = 12 + 5 + 14;

    constructor(
        fields: IField<TEntity>[],
        public defaultSelect: TKey[],
        private key: string,
    ) {
        this.fields = fields;

        const storageSelectedFieldsJson = localStorage.getItem(this.key);

        if (storageSelectedFieldsJson) {
            try {
                const storageSelectedFields: TKey[] = JSON.parse(
                    storageSelectedFieldsJson,
                );

                this.selectedFields = storageSelectedFields.filter((k) =>
                    this.fields.some(({ key }) => key === k),
                );
            } catch (err) {
                localStorage.removeItem(this.key);
                this.selectedFields = [...new Set(defaultSelect)];
            }
        } else {
            this.selectedFields = [...new Set(defaultSelect)];
        }

        makeAutoObservable(this);
    }

    update() {
        localStorage.setItem(this.key, JSON.stringify(this.selectedFields));
    }

    setSelectedFields(fields: TKey[]) {
        this.selectedFields = fields;
        this.update();
    }

    setStep(step: number) {
        this.step = step;
    }

    getSelectedFields() {
        return this.selectedFields.map(
            (key) => this.fields.find((f) => f.key === key) as IField<TEntity>,
        );
    }

    getGridTemplateStyle() {
        return {
            gridTemplate: `1fr / ${
                this.firstWidth + 12 * this.step
            }px ${this.getSelectedFields()
                .map((f) => f.size)
                .join(' ')}`,
        };
    }
}
