import { makeAutoObservable } from 'mobx';

export class OpenHandler {
    isOpen: boolean;

    constructor(defaultOpen = false) {
        this.isOpen = defaultOpen;
        makeAutoObservable(this);
    }

    open() {
        if (!this.isOpen) this.isOpen = true;
    }

    close() {
        if (this.isOpen) this.isOpen = false;
    }

    toggle() {
        this.isOpen = !this.isOpen;
    }
}
