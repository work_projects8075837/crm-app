import { makeAutoObservable } from 'mobx';

export class BoolHandler {
    is = false;

    constructor() {
        makeAutoObservable(this);
    }

    toggle() {
        this.is = !this.is;
    }

    on() {
        if (!this.is) this.is = true;
    }

    off() {
        if (this.is) this.is = false;
    }
}
