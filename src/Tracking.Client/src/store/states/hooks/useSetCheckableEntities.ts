import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import { useEffect } from 'react';
import { CheckEntitiesHandler } from '../CheckEntitiesHandler';

export const useSetCheckableEntities = (
    checkHandler: CheckEntitiesHandler,
    data?: Entity[],
) => {
    useEffect(() => {
        if (data) {
            checkHandler.set(data);
        }
    }, [data]);

    useEffect(() => {
        return () => checkHandler.uncheckAll();
    }, []);
};
