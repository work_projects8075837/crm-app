import { makeAutoObservable } from 'mobx';
import React from 'react';

export type TFilterFieldType =
    | 'string'
    | 'select'
    | 'date'
    | 'phone'
    | 'duration'
    | 'number';

export type TUnionNames<TKey extends string> =
    | `${TKey}/${TKey}`
    | `${TKey}/${TKey}/${TKey}`
    | `${TKey}/${TKey}/${TKey}/${TKey}`
    | `${TKey}/${TKey}/${TKey}/${TKey}/${TKey}`;

export interface IFilterField<TKey extends string = string> {
    key: TKey | `${TKey}-${TKey}`;
    name: string;
    InputElem: React.FC<ISelectEntityIdProps>;
}

export class FilterHandler<TKey extends string = string> {
    selectedFields: TKey[] = [];
    fields: IFilterField<TKey>[] = [];

    constructor(
        fields: IFilterField<TKey>[],
        public defaultSelect: TKey[],
        private key: string,
    ) {
        this.fields = fields;

        const storageSelectedFieldsJson = localStorage.getItem(this.key);

        if (storageSelectedFieldsJson) {
            try {
                const storageSelectedFields: TKey[] = JSON.parse(
                    storageSelectedFieldsJson,
                );

                this.selectedFields = storageSelectedFields.filter((k) =>
                    this.fields.some(({ key }) => key === k),
                );
            } catch (err) {
                localStorage.removeItem(this.key);
                this.selectedFields = [...new Set(defaultSelect)];
            }
        } else {
            this.selectedFields = [...new Set(defaultSelect)];
        }

        makeAutoObservable(this);
    }

    update() {
        localStorage.setItem(this.key, JSON.stringify(this.selectedFields));
    }

    select(key: TKey) {
        const foundField = this.fields.find((field) => field.key === key);
        if (foundField && !this.selectedFields.includes(key)) {
            this.selectedFields.push(key);
            this.update();
        }
    }

    remove(key: TKey) {
        this.selectedFields.filter((field) => field !== key);
        this.update();
    }

    toggle(key: TKey) {
        if (!this.selectedFields.some((field) => field === key)) {
            this.select(key);
        } else {
            this.remove(key);
        }
        this.update();
    }

    setSelectedFields(fields: TKey[]) {
        this.selectedFields = fields;
        this.update();
    }
}
