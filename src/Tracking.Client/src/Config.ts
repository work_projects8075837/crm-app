type TConfig = {
    [key: string]: string;
};

export class ConfigurationManager {
    private static _instance: ConfigurationManager;
    private _config: TConfig | null = null;

    async fetch(filename = '../config.json'): Promise<void> {
        const r = await fetch(filename);

        this._config = await r.json();
    }

    getItem<T>(key: string, defaultValue: T): T {
        return this._config?.[key] !== undefined
            ? (this._config[key] as T)
            : defaultValue;
    }

    static GetInstance(): ConfigurationManager {
        if (!this._instance) {
            this._instance = new ConfigurationManager();
        }

        return this._instance;
    }
}
