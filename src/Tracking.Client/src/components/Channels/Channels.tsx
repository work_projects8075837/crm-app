import React from 'react';
import BlueButton from '../Fields/Buttons/BlueButton';
import Layout from '../NavbarLayout/Layout';
import BaseEntityHeader from '../SalePoints/SalePointsHeader/BaseEntityHeader';
import ChannelsCreateModal, {
    channelsCreateModalOpen,
} from './ChannelsCreateModal/ChannelsCreateModal';
import ChannelsEditModal from './ChannelsEditModal/ChannelsEditModal';
import ChannelsList from './ChannelsList/ChannelsList';

const Channels: React.FC = () => {
    return (
        <>
            <Layout>
                <BaseEntityHeader
                    title='Список каналов связи'
                    headerButton={
                        <>
                            <BlueButton
                                onClick={() => channelsCreateModalOpen.open()}
                            >
                                Новый
                            </BlueButton>
                        </>
                    }
                />
                <ChannelsList />
                <ChannelsCreateModal />
                <ChannelsEditModal />
            </Layout>
        </>
    );
};

export default Channels;
