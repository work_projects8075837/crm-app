import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import React from 'react';
import style from '../../../style/layout/channel.module.scss';
import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import ChannelPagination from './ChannelPagination';
import { channelCheckHandler } from './ChannelsList';

interface ChannelsTableLayoutProps {
    children: React.ReactNode;
}

const fields = [{ name: 'Название' }, { name: 'Коэффициент' }];

const ChannelsTableLayout: React.FC<ChannelsTableLayoutProps> = ({
    children,
}) => {
    return (
        <>
            <TableLayout
                layoutClass={style.entityPaginatedTable}
                header={
                    <TableHeader
                        checkHandler={channelCheckHandler}
                        fields={fields}
                        layoutClass={style.rowLayout}
                        fieldClass={style.headerField}
                    />
                }
                pagination={<ChannelPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default ChannelsTableLayout;
