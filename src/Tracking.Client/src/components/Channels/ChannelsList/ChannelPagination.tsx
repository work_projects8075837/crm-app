import Pagination from '@/components/Tables/Pagination/Pagination';
import PaginationButtonsList from '@/components/Tickets/TiketsPaginationButtonsList/PaginationButtonsList';
import { channelHooks } from '@/services/channel/channel';
import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';

const CHANNELS_PAGINATION = 'CHANNELS_PAGINATION';

export const channelsPaginationHandler = new PaginationHandler(
    CHANNELS_PAGINATION,
);

const ChannelPagination: React.FC = () => {
    const { data } = channelHooks.useList(channelsPaginationHandler);
    return (
        <>
            <Pagination
                paginationHandler={channelsPaginationHandler}
                pages={
                    <PaginationButtonsList
                        data={data}
                        paginationHandler={channelsPaginationHandler}
                    />
                }
            />
        </>
    );
};

export default observer(ChannelPagination);
