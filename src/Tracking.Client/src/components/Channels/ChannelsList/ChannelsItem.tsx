import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import { IChannel } from '@/services/channel/types';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/channel.module.scss';
import { channelCheckHandler } from './ChannelsList';

type ChannelsItemProps = IChannel;

const ChannelsItem: React.FC<ChannelsItemProps> = ({ id, name, multiple }) => {
    const navigate = useNavigate();
    return (
        <>
            <div
                className={style.tableItem}
                onDoubleClick={() => {
                    navigate(`/channel/${id}/`);
                }}
            >
                <CheckItem
                    checkHandler={channelCheckHandler}
                    entity={{ id, name, multiple }}
                />
                <span className={style.tableItemName}>
                    <span>{name}</span>
                </span>

                <span className={style.tableItemField}>
                    <span>{multiple}</span>
                </span>
            </div>
        </>
    );
};

export default ChannelsItem;
