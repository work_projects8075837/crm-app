import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import NullEntities from '@/components/Elements/NullEntities/NullEntities';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { channelApi, channelHooks } from '@/services/channel/channel';
import {
    GET_ALL_CHANNELS_KEY,
    GET_SEARCH_CHANNELS_KEY,
} from '@/store/queries/keys/keys';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import React from 'react';
import style from '../../../style/elements/loader_text.module.scss';
import { channelsPaginationHandler } from './ChannelPagination';
import ChannelsItem from './ChannelsItem';
import ChannelsTableLayout from './ChannelsTableLayout';

export const channelCheckHandler = new CheckEntitiesHandler();

const ChannelsList: React.FC = () => {
    const { data, refetch } = channelHooks.useList(channelsPaginationHandler, {
        refetchOnMount: true,
    });

    useSetCheckableEntities(channelCheckHandler, data?.data);

    return (
        <>
            {data ? (
                <>
                    {data.data.length ? (
                        <>
                            <ChannelsTableLayout>
                                {data.data.map((c) => (
                                    <ChannelsItem key={c.id} {...c} />
                                ))}
                            </ChannelsTableLayout>
                        </>
                    ) : (
                        <NullEntities text='Нет каналов связи' />
                    )}
                </>
            ) : (
                <LoaderCircle size={100} className={style.loaderPlaceFull} />
            )}
            <EntitySelectedActions
                keys={[GET_ALL_CHANNELS_KEY, GET_SEARCH_CHANNELS_KEY]}
                checkHandler={channelCheckHandler}
                service={channelApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default ChannelsList;
