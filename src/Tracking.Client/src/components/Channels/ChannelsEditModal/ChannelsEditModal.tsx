import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import ChannelFields from '@/components/Form/CreateChannelForm/ChannelFields';
import { OpenHandler } from '@/store/states/OpenHandler';
import React, { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import ChannelsEditProvider from './ChannelsEditProvider';
import ChannelsEditTitle from './ChannelsEditTitle';

export const channelsEditOpen = new OpenHandler();

const ChannelsEditModal: React.FC = () => {
    const navigate = useNavigate();
    const { channelId } = useParams();
    useEffect(() => {
        if (channelId) {
            channelsEditOpen.open();
        }
    }, [channelId]);
    return (
        <>
            <ModalWindow
                onClose={() => {
                    navigate('/channel/');
                }}
                title={<ChannelsEditTitle />}
                openHandler={channelsEditOpen}
            >
                <ChannelsEditProvider>
                    <ChannelFields />
                </ChannelsEditProvider>
            </ModalWindow>
        </>
    );
};

export default ChannelsEditModal;
