import { useChannelByParams } from '@/store/queries/stores/channel/useChannelByParams'
import React from 'react'

const ChannelsEditTitle: React.FC = () => {
    const { data } = useChannelByParams();
    return (
        <>
            <>{data?.name}</>
        </>
    );
};

export default ChannelsEditTitle;
