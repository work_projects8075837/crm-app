import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import UpdateChannelForm from '@/components/Form/UpdateChannelForm/UpdateChannelForm';
import { useChannelByParams } from '@/store/queries/stores/channel/useChannelByParams';
import React from 'react';

interface ChannelsEditProviderProps {
    children: React.ReactNode;
}

const ChannelsEditProvider: React.FC<ChannelsEditProviderProps> = ({
    children,
}) => {
    const { data } = useChannelByParams();
    return (
        <>
            {data ? (
                <UpdateChannelForm defaultValues={data}>
                    {children}
                </UpdateChannelForm>
            ) : (
                <LoaderCircle />
            )}
        </>
    );
};

export default ChannelsEditProvider;
