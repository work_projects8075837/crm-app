import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import CreateChannelForm from '@/components/Form/CreateChannelForm/CreateChannelForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';

export const channelsCreateModalOpen = new OpenHandler(false);

const ChannelsCreateModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                title='Добавить канал связи'
                openHandler={channelsCreateModalOpen}
            >
                <CreateChannelForm openHandler={channelsCreateModalOpen} />
            </ModalWindow>
        </>
    );
};

export default ChannelsCreateModal;
