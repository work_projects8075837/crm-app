import { FilterHandler, IFilterField } from '@/store/states/FilterHandler';
import { observer } from 'mobx-react-lite';
import { Fragment } from 'react';

interface FilterTableProps<TKey extends string> {
    filterHandler: FilterHandler<TKey>;
    // fields: Partial<{
    //     [K in TKey as `${TKey}` | `${TKey}-${TKey}`]: React.ReactNode;
    // }>;
}

const FilterTable = <T extends string>({
    filterHandler,
}: FilterTableProps<T>) => {
    return (
        <>
            {filterHandler.selectedFields
                .map(
                    (k) =>
                        filterHandler.fields.find(
                            (f) => f.key === k,
                        ) as IFilterField,
                )
                .map(({ key, InputElem, name }) => {
                    return (
                        <Fragment key={key}>
                            <InputElem name={key} label={name} />
                        </Fragment>
                    );
                })}
        </>
    );
};

export default observer(FilterTable);
