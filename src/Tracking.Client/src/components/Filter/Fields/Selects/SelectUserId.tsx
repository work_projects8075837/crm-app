import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { useSearchUsers } from '@/store/queries/stores/user/useSearchUsers';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectUserIdProps = ISelectEntityIdProps;

const SelectUserId: React.FC<SelectUserIdProps> = ({
    name,
    label = 'Вовлеченный',
}) => {
    const model = useSearchInputModel();
    const { data } = useSearchUsers({ search: model.value });

    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectUserId;
