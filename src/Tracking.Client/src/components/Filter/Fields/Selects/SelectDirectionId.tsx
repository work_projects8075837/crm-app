import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { directionHooks } from '@/services/direction/direction';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectDirectionIdProps = ISelectEntityIdProps;

const SelectDirectionId: React.FC<SelectDirectionIdProps> = ({
    name,
    label = 'Направление',
}) => {
    const model = useSearchInputModel();
    const { data } = directionHooks.useSearch(model.value);

    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectDirectionId;
