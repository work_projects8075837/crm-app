import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { counterPartyHooks } from '@/services/counterparty/counterparty';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectCounterPartyIdProps = ISelectEntityIdProps;

const SelectCounterPartyId: React.FC<SelectCounterPartyIdProps> = ({
    name,
    label = 'Контрагент',
}) => {
    const model = useSearchInputModel();
    const { data } = counterPartyHooks.useSearch(model.value);

    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectCounterPartyId;
