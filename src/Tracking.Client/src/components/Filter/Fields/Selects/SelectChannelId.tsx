import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { channelHooks } from '@/services/channel/channel';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectChannelIdProps = ISelectEntityIdProps;

const SelectChannelId: React.FC<SelectChannelIdProps> = ({
    name,
    label = 'Канал связи',
}) => {
    const model = useSearchInputModel();
    const { data } = channelHooks.useSearch(model.value);

    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectChannelId;
