import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { tagHooks } from '@/services/tag/tag';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectTagIdProps = ISelectEntityIdProps;

const SelectTagId: React.FC<SelectTagIdProps> = ({
    name,
    label = 'Модуль',
}) => {
    const model = useSearchInputModel();
    const { data } = tagHooks.useSearch(model.value);

    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectTagId;
