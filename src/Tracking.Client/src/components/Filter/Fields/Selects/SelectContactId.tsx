import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { contactHooks } from '@/services/contact/contact';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectContactIdProps = ISelectEntityIdProps;

const SelectContactId: React.FC<SelectContactIdProps> = ({
    name,
    label = 'Контакт',
}) => {
    const model = useSearchInputModel();
    const { data } = contactHooks.useSearch(model.value);

    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectContactId;
