import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { characterProblemHooks } from '@/services/character_problem/character_problem';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectCharacterProblemIdProps = ISelectEntityIdProps;

const SelectCharacterProblemIds: React.FC<SelectCharacterProblemIdProps> = ({
    name,
    label = 'Характер проблемы',
}) => {
    const model = useSearchInputModel();
    const { data } = characterProblemHooks.useSearch(model.value);
    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectCharacterProblemIds;
