import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { salePointHooks } from '@/services/sale_point/sale_point';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectSalePointIdProps = ISelectEntityIdProps;

const SelectSalePointId: React.FC<SelectSalePointIdProps> = ({
    name,
    label = 'Заведение',
}) => {
    const model = useSearchInputModel();
    const { data } = salePointHooks.useSearch(model.value);
    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectSalePointId;
