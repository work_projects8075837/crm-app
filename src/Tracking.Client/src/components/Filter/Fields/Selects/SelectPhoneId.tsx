import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { phoneNumberHooks } from '@/services/phone_number/phone_number';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectPhoneIdProps = ISelectEntityIdProps;

const SelectPhoneId: React.FC<SelectPhoneIdProps> = ({
    name,
    label = 'Номер телефона',
}) => {
    const model = useSearchInputModel('', /^\+?[0-9\s\-\(\)]{0,}$/);
    const { data } = phoneNumberHooks.useSearch(model.value);
    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectPhoneId;
