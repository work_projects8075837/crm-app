import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { useStatusSearch } from '@/services/status/status';
import React from 'react';
import FilterLabelLayout from '../FilterLabelLayout';
import SelectIds from '../SelectIds/SelectIds';

type SelectStatusIdProps = ISelectEntityIdProps;

const SelectStatusId: React.FC<SelectStatusIdProps> = ({
    name,
    label = 'Статус',
}) => {
    const model = useSearchInputModel();
    const { data } = useStatusSearch(model.value);

    return (
        <>
            <FilterLabelLayout label={label}>
                <SelectIds
                    name={name}
                    searchModel={model}
                    entities={data?.data}
                />
            </FilterLabelLayout>
        </>
    );
};

export default SelectStatusId;
