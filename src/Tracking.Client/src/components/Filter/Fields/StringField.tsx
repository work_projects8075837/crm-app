import FormInput from '@/components/Fields/BaseFields/FormInput';
import LabelFieldLayout from '@/components/Form/LoginForm/layouts/LabelFieldLayout';
import { StyleFilter } from '@/style/layout/filter/filter';
import classNames from 'classnames';
import React from 'react';

interface StringFieldProps {
    label?: string;
    name: string;
}

const StringField: React.FC<StringFieldProps> = ({ name, label = '' }) => {
    return (
        <>
            <LabelFieldLayout
                label={label}
                className={classNames('mb0', StyleFilter.label)}
            >
                <FormInput
                    wrapperClass={StyleFilter.inputWrapper}
                    options={{ required: false }}
                    name={name}
                    className={StyleFilter.input}
                />
            </LabelFieldLayout>
        </>
    );
};

export default StringField;
