import Dash from '@/components/Elements/Dash/Dash';
import DateInput from '@/components/Fields/DateInput/DateInput';
import LabelFieldLayout from '@/components/Form/LoginForm/layouts/LabelFieldLayout';
import { StyleFilter } from '@/style/layout/filter/filter';
import classNames from 'classnames';
import React from 'react';

interface DateDurationProps {
    name: `${string}-${string}` | string;
    label?: string;
}

const DateDuration: React.FC<DateDurationProps> = ({
    label = 'Дата',
    name,
}) => {
    return (
        <>
            <LabelFieldLayout
                label={label}
                className={classNames('mb0', StyleFilter.label)}
            >
                <div className={StyleFilter.inputRow}>
                    <DateInput
                        options={{
                            setValueAs: (v) => {
                                return v ? new Date(v).toISOString() : v;
                            },
                        }}
                        name={name.split('-')[0]}
                        className={StyleFilter.dateInput}
                    />
                    <Dash className={StyleFilter.dash} />
                    <DateInput
                        name={name.split('-')[1]}
                        className={StyleFilter.dateInput}
                    />
                </div>
            </LabelFieldLayout>
        </>
    );
};

export default DateDuration;
