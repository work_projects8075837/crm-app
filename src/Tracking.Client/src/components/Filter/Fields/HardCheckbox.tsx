import Checkbox from '@/components/Elements/Checkbox/Checkbox';
import { StyleFilter } from '@/style/layout/filter/filter';
import React from 'react';
import { useFormContext, useWatch } from 'react-hook-form';

const HardCheckbox: React.FC = () => {
    const { getValues, setValue } = useFormContext();
    const isHard = useWatch({ name: 'is_hard' });
    return (
        <>
            <div
                className={StyleFilter.hard}
                onClick={() => {
                    setValue(
                        'is_hard',
                        getValues('is_hard') === 'true' ? 'false' : 'true',
                    );
                }}
            >
                <Checkbox
                    isChecked={isHard === 'true'}
                    className={StyleFilter.hardCheckbox}
                />

                <span>Жесткая фильтрация</span>
            </div>
        </>
    );
};

export default HardCheckbox;
