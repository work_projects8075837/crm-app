import LabelFieldLayout from '@/components/Form/LoginForm/layouts/LabelFieldLayout';
import { StyleFilter } from '@/style/layout/filter/filter';
import classNames from 'classnames';
import React from 'react';

interface FilterLabelLayoutProps {
    children?: React.ReactNode;
    label: string;
}

const FilterLabelLayout: React.FC<FilterLabelLayoutProps> = ({
    children,
    label,
}) => {
    return (
        <>
            <LabelFieldLayout
                label={label}
                className={classNames('mb0', StyleFilter.label)}
            >
                {children}
            </LabelFieldLayout>
        </>
    );
};

export default FilterLabelLayout;
