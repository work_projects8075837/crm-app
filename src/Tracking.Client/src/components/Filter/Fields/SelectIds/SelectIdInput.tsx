import { SearchInputModelState } from '@/components/Fields/SelectEntity/SelectEntity';
import { StyleSelectId } from '@/style/elements/select_id/select_id';
import React, { InputHTMLAttributes } from 'react';

interface SelectIdInputProps extends InputHTMLAttributes<HTMLInputElement> {
    name: string;
    searchModel: SearchInputModelState;
}

const SelectIdInput: React.FC<SelectIdInputProps> = ({
    searchModel,
    ...inputProps
}) => {
    return (
        <>
            <input
                value={searchModel.value}
                onChange={(event) => {
                    searchModel.setSearch(event.currentTarget.value);
                }}
                className={StyleSelectId.input}
                type='text'
                {...inputProps}
            />
        </>
    );
};

export default SelectIdInput;
