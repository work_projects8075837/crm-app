import { StyleSelectId } from '@/style/elements/select_id/select_id';
import React from 'react';

interface SelectIdInputLayoutProps {
    children: React.ReactNode;
}

const SelectIdInputLayout: React.FC<SelectIdInputLayoutProps> = ({
    children,
}) => {
    return <div className={StyleSelectId.inputWrapper}>{children}</div>;
};

export default SelectIdInputLayout;
