import { StyleSelectId } from '@/style/elements/select_id/select_id';
import React from 'react';
import { useWatch } from 'react-hook-form';

interface SelectIdCountProps {
    name: string;
}

const SelectIdCount: React.FC<SelectIdCountProps> = ({ name }) => {
    const selected = useWatch({ name });

    return (
        <>
            {!!selected && !!selected?.length && (
                <div className={StyleSelectId.count}>
                    Выбрано {selected instanceof Array ? selected?.length : '1'}
                </div>
            )}
        </>
    );
};

export default SelectIdCount;
