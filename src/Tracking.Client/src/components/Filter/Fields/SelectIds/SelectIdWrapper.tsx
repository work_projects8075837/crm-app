import { useOutsideClose } from '@/hooks/useOutsideClose';
import { StyleSelectId } from '@/style/elements/select_id/select_id';
import React from 'react';

interface SelectIdWrapperProps {
    children: React.ReactNode;
    off: () => void;
    on: () => void;
}

const SelectIdWrapper: React.FC<SelectIdWrapperProps> = ({
    children,
    off,
    on,
}) => {
    const ref = useOutsideClose(off);

    return (
        <>
            <div ref={ref} className={StyleSelectId.select} onClick={on}>
                {children}
            </div>
        </>
    );
};

export default SelectIdWrapper;
