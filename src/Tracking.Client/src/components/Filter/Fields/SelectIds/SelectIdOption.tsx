import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import { StyleSelectId } from '@/style/elements/select_id/select_id';
import React from 'react';
import { useFormContext } from 'react-hook-form';

interface SelectIdOptionProps {
    entity: Entity;
    name: string;
}

const SelectIdOption: React.FC<SelectIdOptionProps> = ({ entity, name }) => {
    const { register } = useFormContext();

    return (
        <>
            <label key={entity.id} className={StyleSelectId.optionLabel}>
                <input
                    type='checkbox'
                    value={entity.id}
                    {...register(name)}
                    className={StyleSelectId.checker}
                />
                <div className={StyleSelectId.option}>{entity.name}</div>
            </label>
        </>
    );
};

export default SelectIdOption;
