import {
    Entity,
    SearchInputModelState,
} from '@/components/Fields/SelectEntity/SelectEntity';
import { useBooleanState } from '@/hooks/useBooleanState';
import React from 'react';
import SelectIdCount from './SelectIdCount';
import SelectIdInput from './SelectIdInput';
import SelectIdInputLayout from './SelectIdInputLayout';
import SelectIdLoading from './SelectIdLoading';
import SelectIdOption from './SelectIdOption';
import SelectIdReload from './SelectIdReload';
import SelectIdWrapper from './SelectIdWrapper';

interface SelectIdsProps {
    entities?: Entity[];
    name: string;
    searchModel: SearchInputModelState;
}

const SelectIds: React.FC<SelectIdsProps> = ({
    entities,
    name,
    searchModel,
}) => {
    const [isOpen, { on, off }] = useBooleanState();

    return (
        <>
            <SelectIdWrapper {...{ off, on }}>
                <SelectIdInputLayout>
                    <SelectIdInput searchModel={searchModel} name={name} />
                    <SelectIdReload {...{ name }} />
                    {!isOpen && <SelectIdCount {...{ name }} />}
                </SelectIdInputLayout>

                <SelectIdLoading {...{ entities, isOpen }}>
                    {entities?.map((entity) => (
                        <SelectIdOption key={entity.id} {...{ entity, name }} />
                    ))}
                </SelectIdLoading>
            </SelectIdWrapper>
        </>
    );
};

export default SelectIds;
