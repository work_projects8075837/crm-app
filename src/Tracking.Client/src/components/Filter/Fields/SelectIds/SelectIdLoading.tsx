import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import OptionLoader from '@/components/Fields/SelectEntity/SelectEntityOption/OptionLoader';
import { StyleSelectId } from '@/style/elements/select_id/select_id';
import React from 'react';

interface SelectIdLoadingProps {
    children?: React.ReactNode;
    entities?: Entity[];
    isOpen: boolean;
}

const SelectIdLoading: React.FC<SelectIdLoadingProps> = ({
    children,
    isOpen,
    entities,
}) => {
    return (
        <>
            {isOpen && (
                <div className={StyleSelectId.options}>
                    {entities ? (
                        entities.length ? (
                            children
                        ) : (
                            <div className={StyleSelectId.optionEmpty}>
                                Нет вариантов
                            </div>
                        )
                    ) : (
                        <OptionLoader
                            size={16}
                            className={StyleSelectId.loader}
                        />
                    )}
                </div>
            )}
        </>
    );
};

export default SelectIdLoading;
