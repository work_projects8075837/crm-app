import Svg from '@/components/Elements/Svg/Svg';
import { StyleSelectId } from '@/style/elements/select_id/select_id';
import React from 'react';
import { useFormContext, useWatch } from 'react-hook-form';

interface SelectIdReloadProps {
    name: string;
}

const SelectIdReload: React.FC<SelectIdReloadProps> = ({ name }) => {
    const { setValue } = useFormContext();
    const selected = useWatch({ name });
    return (
        <>
            {!!selected?.length && (
                <button
                    className={StyleSelectId.refresh}
                    onClick={(event) => {
                        event.preventDefault();
                        setValue(name, []);
                    }}
                >
                    <Svg symbol='blue_refresh_arrow' />
                </button>
            )}
        </>
    );
};

export default SelectIdReload;
