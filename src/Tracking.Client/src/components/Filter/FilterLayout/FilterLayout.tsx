import Svg from '@/components/Elements/Svg/Svg';
import FilterForm from '@/components/Form/FilterForm/FilterForm';
import { OpenHandler } from '@/store/states/OpenHandler';
import { StyleFilter } from '@/style/layout/filter/filter';
import { motion } from 'framer-motion';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import ApplyFilter from '../Buttons/ApplyFilter';
import ResetFilter from '../Buttons/ResetFilter';
import HardCheckbox from '../Fields/HardCheckbox';
import { useAnimationMountDebounce } from './useAnimationMountDebounce';

interface FilterLayoutProps {
    children?: React.ReactNode;
    openHandler: OpenHandler;
    filterOpen: OpenHandler;
}

const FilterLayout: React.FC<FilterLayoutProps> = ({
    children,
    openHandler,
    filterOpen,
}) => {
    const s = useAnimationMountDebounce();

    useEffect(() => {
        return () => {
            filterOpen.close();
        };
    }, []);

    return (
        <>
            <motion.div
                animate={{
                    height: !filterOpen.isOpen ? 0 : 'max-content',
                    overflow: !filterOpen.isOpen ? 'hidden' : 'visible',
                }}
                {...s}
                className={StyleFilter.wrapper}
                transition={{ duration: 0.4, easings: ['easeIn'] }}
            >
                <FilterForm>
                    <div className={StyleFilter.table}>{children}</div>
                    <HardCheckbox />
                    <div className={StyleFilter.buttons}>
                        <ApplyFilter />

                        <button
                            className={StyleFilter.settingsButton}
                            onClick={(event) => {
                                event.preventDefault();
                                openHandler.open();
                            }}
                        >
                            <Svg symbol='grey_settings' />
                        </button>

                        <ResetFilter />
                    </div>
                </FilterForm>
            </motion.div>
        </>
    );
};

export default observer(FilterLayout);
