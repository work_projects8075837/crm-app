import { useBooleanState } from '@/hooks/useBooleanState';
import { useEffect } from 'react';

export const useAnimationMountDebounce = (property = 'flex') => {
    const [isHide, { off }] = useBooleanState(true);
    useEffect(() => {
        setTimeout(() => off(), 600);
    }, []);

    return { style: { display: isHide ? 'none' : property } };
};
