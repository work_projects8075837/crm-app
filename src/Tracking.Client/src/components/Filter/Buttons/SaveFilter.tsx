import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import { FilterModalStyle } from '@/style/elements/filter_modal';
import React from 'react';

const SaveFilter: React.FC = () => {
    const { onSubmit } = useSubmit();
    return (
        <>
            <SaveButton
                onClick={onSubmit}
                className={FilterModalStyle.saveButton}
            >
                Сохранить
            </SaveButton>
        </>
    );
};

export default SaveFilter;
