import WhiteButton from '@/components/Elements/WhiteButton/WhiteButton';
import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import { StyleFilter } from '@/style/layout/filter/filter';
import React from 'react';

const ApplyFilter: React.FC = () => {
    const { onSubmit } = useSubmit();

    return (
        <>
            <WhiteButton
                onClick={onSubmit}
                type='submit'
                className={StyleFilter.button}
            >
                Применить
            </WhiteButton>
        </>
    );
};

export default ApplyFilter;
