import { FilterModalStyle } from '@/style/elements/filter_modal';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { IFilterModalHandler } from '../FilterModalForm/FilterModalForm';

interface ResetSelectedFieldsProps {
    filterHandler: IFilterModalHandler;
}

const ResetSelectedFields: React.FC<ResetSelectedFieldsProps> = ({
    filterHandler,
}) => {
    const { setValue } = useFormContext();
    return (
        <>
            <button
                className={FilterModalStyle.link}
                onClick={() => {
                    setValue(
                        'selectedFields',
                        filterHandler.fields.filter((f) =>
                            filterHandler.defaultSelect.includes(f.key),
                        ),
                    );
                }}
            >
                Сбросить
            </button>
        </>
    );
};

export default ResetSelectedFields;
