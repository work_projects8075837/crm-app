import Svg from '@/components/Elements/Svg/Svg';
import { OpenHandler } from '@/store/states/OpenHandler';
import { StyleFilter } from '@/style/layout/filter/filter';
import { observer } from 'mobx-react-lite';
import React from 'react';

interface FilterTogglerProps {
    text?: React.ReactNode;
    openHandler: OpenHandler;
}

const FilterToggler: React.FC<FilterTogglerProps> = ({
    text = 'Развернуть фильтр',
    openHandler,
}) => {
    return (
        <button
            className={StyleFilter.toggler}
            onClick={(e) => {
                e.preventDefault();

                openHandler.toggle();
            }}
        >
            <span>{text}</span>
            <Svg
                symbol={
                    openHandler.isOpen ? 'blue_arrow_up' : 'blue_arrow_down'
                }
            />
        </button>
    );
};

export default observer(FilterToggler);
