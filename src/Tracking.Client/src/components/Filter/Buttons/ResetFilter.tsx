import { FilterModalStyle } from '@/style/elements/filter_modal';
import React from 'react';
import { useFormContext } from 'react-hook-form';

const ResetFilter: React.FC = () => {
    const { resetField, getValues } = useFormContext();
    return (
        <>
            <button
                className={FilterModalStyle.link}
                onClick={(event) => {
                    event.preventDefault();

                    Object.keys(getValues()).forEach((name) => {
                        resetField(name);
                    });
                }}
            >
                Сбросить фильтр
            </button>
        </>
    );
};

export default ResetFilter;
