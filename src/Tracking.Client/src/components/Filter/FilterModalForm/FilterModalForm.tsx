import SubmitProvider from '@/components/Form/BaseForm/SubmitProvider';
import { useBaseForm } from '@/components/Form/hooks/useBaseForm';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';
import { FormProvider } from 'react-hook-form';

export interface ISimpleField {
    name: string;
    key: string;
}

export interface IFilterModalHandler {
    fields: ISimpleField[];
    selectedFields: string[];
    defaultSelect: string[];
    setSelectedFields: (fields: string[]) => void;
}

interface FilterModalFormProps {
    children: React.ReactNode;
    filterHandler: IFilterModalHandler;
    openHandler: OpenHandler;
}

interface IFilterModalForm {
    fields: ISimpleField[];
    selectedFields: ISimpleField[];
}

const FilterModalForm: React.FC<FilterModalFormProps> = ({
    children,
    filterHandler,
    openHandler,
}) => {
    const { form } = useBaseForm<IFilterModalForm>({
        defaultValues: {
            fields: filterHandler.fields,
            selectedFields: filterHandler.selectedFields.map((key) =>
                filterHandler.fields.find((f) => f.key === key),
            ),
        },
    });
    const onSubmit = form.handleSubmit((data) => {
        filterHandler.setSelectedFields(data.selectedFields.map((f) => f.key));
        openHandler.close();
    });
    return (
        <>
            <FormProvider {...form}>
                <SubmitProvider {...{ onSubmit }}>{children}</SubmitProvider>
            </FormProvider>
        </>
    );
};

export default FilterModalForm;
