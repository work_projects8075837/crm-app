import { IFilterField } from '@/store/states/FilterHandler';
import { FilterModalStyle } from '@/style/elements/filter_modal';
import { Reorder } from 'framer-motion';
import React from 'react';
import { useFormContext, useWatch } from 'react-hook-form';
import OrderItem from './OrderItem';

const Order: React.FC = () => {
    const selectedFields: IFilterField[] = useWatch({ name: 'selectedFields' });
    const { setValue } = useFormContext();

    return (
        <>
            <div className={FilterModalStyle.order}>
                <Reorder.Group
                    axis='y'
                    values={selectedFields}
                    onReorder={(newOrder) =>
                        setValue('selectedFields', newOrder)
                    }
                >
                    {selectedFields.map((f) => (
                        <OrderItem field={f} key={f.key} />
                    ))}
                </Reorder.Group>
            </div>
        </>
    );
};

export default Order;
