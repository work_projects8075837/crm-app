import { OpenHandler } from '@/store/states/OpenHandler';
import { FilterModalStyle } from '@/style/elements/filter_modal';
import React from 'react';
import ResetSelectedFields from '../Buttons/ResetSelectedFields';
import SaveFilter from '../Buttons/SaveFilter';
import FilterModalForm, {
    IFilterModalHandler,
} from '../FilterModalForm/FilterModalForm';
import FilterModalLayout from './FilterModalLayout';
import Order from './Order';
import SelectFields from './SelectFields';

interface FilterModalProps {
    openHandler: OpenHandler;
    filterHandler: IFilterModalHandler;
}

const FilterModal: React.FC<FilterModalProps> = ({
    openHandler,
    filterHandler,
}) => {
    return (
        <>
            <FilterModalLayout openHandler={openHandler}>
                <FilterModalForm
                    filterHandler={filterHandler}
                    openHandler={openHandler}
                >
                    <Order />
                    <div className={FilterModalStyle.select}>
                        <div className={FilterModalStyle.table}>
                            <SelectFields />
                        </div>
                    </div>
                    <div className={FilterModalStyle.bottom}>
                        <SaveFilter />
                        <ResetSelectedFields {...{ filterHandler }} />
                    </div>
                </FilterModalForm>
            </FilterModalLayout>
        </>
    );
};

export default FilterModal;
