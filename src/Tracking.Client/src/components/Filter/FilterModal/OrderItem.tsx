import Svg from '@/components/Elements/Svg/Svg';
import { IFilterField } from '@/store/states/FilterHandler';
import { FilterModalStyle } from '@/style/elements/filter_modal';
import { Reorder } from 'framer-motion';
import React from 'react';
import { useFormContext } from 'react-hook-form';

interface OrderItemProps {
    field: IFilterField;
}

const OrderItem: React.FC<OrderItemProps> = ({ field }) => {
    const { getValues, setValue } = useFormContext();
    return (
        <>
            <Reorder.Item value={field} id={field.key}>
                <div className={FilterModalStyle.orderItem}>
                    <div className={FilterModalStyle.orderContent}>
                        <Svg
                            symbol='dots'
                            className={FilterModalStyle.dotsSvg}
                        />
                        <span className={FilterModalStyle.text}>
                            {field.name}
                        </span>
                    </div>

                    <div
                        className={FilterModalStyle.bag}
                        onClick={() => {
                            const sf: IFilterField[] =
                                getValues('selectedFields');

                            setValue(
                                'selectedFields',
                                sf.filter((f) => f.key !== field.key),
                            );
                        }}
                    >
                        <Svg symbol='delete_filter' />
                    </div>
                </div>
            </Reorder.Item>
        </>
    );
};

export default OrderItem;
