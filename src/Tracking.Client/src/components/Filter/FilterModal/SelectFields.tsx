import { IFilterField } from '@/store/states/FilterHandler';
import { FilterModalStyle } from '@/style/elements/filter_modal';
import React, { Fragment } from 'react';
import { useWatch } from 'react-hook-form';
import CheckFieldButton from './CheckFieldButton';

const SelectFields: React.FC = () => {
    const fields: IFilterField[] = useWatch({ name: 'fields' });

    const existLetters = [
        ...new Set(fields.map((f) => f.name[0].toUpperCase())),
    ];

    const sortedFields = existLetters.map((letter) => {
        return [
            letter,
            fields.filter((f) => f.name[0].toUpperCase() === letter),
        ] as const;
    });

    return (
        <>
            <div className={FilterModalStyle.table}>
                {sortedFields.map(([letter, fields]) => {
                    return (
                        <Fragment key={letter}>
                            <div className={FilterModalStyle.letter}>
                                {letter}
                            </div>
                            <div className={FilterModalStyle.checkboxes}>
                                {fields.map((f) => (
                                    <CheckFieldButton field={f} key={f.key} />
                                ))}
                            </div>
                        </Fragment>
                    );
                })}
            </div>
        </>
    );
};

export default SelectFields;
