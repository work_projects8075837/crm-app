import Checkbox from '@/components/Elements/Checkbox/Checkbox';
import { IFilterField } from '@/store/states/FilterHandler';
import { FilterModalStyle } from '@/style/elements/filter_modal';
import React from 'react';
import { useFormContext, useWatch } from 'react-hook-form';

interface CheckFieldButtonProps {
    field: IFilterField;
}

const CheckFieldButton: React.FC<CheckFieldButtonProps> = ({
    field: { key, name, InputElem },
}) => {
    const selectedFields: IFilterField[] = useWatch({ name: 'selectedFields' });
    const { getValues, setValue } = useFormContext();

    const isChecked = selectedFields.some((f) => f.key === key);

    return (
        <>
            <div
                className={FilterModalStyle.checkbox}
                onClick={() => {
                    const sf: IFilterField[] = getValues('selectedFields');

                    if (sf.some((f) => f.key === key)) {
                        setValue(
                            'selectedFields',
                            sf.filter((f) => f.key !== key),
                        );
                    } else {
                        setValue('selectedFields', [
                            ...sf,
                            { key, name, InputElem },
                        ]);
                    }
                }}
            >
                <Checkbox isChecked={isChecked} />

                <span>{name}</span>
            </div>
        </>
    );
};

export default CheckFieldButton;
