import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import { OpenHandler } from '@/store/states/OpenHandler';
import { FilterModalStyle } from '@/style/elements/filter_modal';
import React from 'react';

interface FilterModalLayoutProps {
    children?: React.ReactNode;
    openHandler: OpenHandler;
}

const FilterModalLayout: React.FC<FilterModalLayoutProps> = ({
    children,
    openHandler,
}) => {
    return (
        <>
            <ModalWindow
                {...{
                    openHandler,
                    title: 'Фильтры',
                    layoutClass: FilterModalStyle.filterWidth,
                    contentClass: FilterModalStyle.wrapper,
                    headerClass: FilterModalStyle.filterHeader,
                    withoutModalContent: true,
                }}
            >
                {/* <div className={FilterModalStyle.wrapper}>{children}</div> */}
                {children}
            </ModalWindow>
        </>
    );
};

export default FilterModalLayout;
