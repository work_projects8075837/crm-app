import React from 'react';
import style from '../../../style/elements/select_option.module.scss';
import Svg from '@/components/Elements/Svg/Svg';

const CardGrateIcon: React.FC = () => {
    return (
        <>
            <Svg symbol='grate' className={style.optionItemSvg} />
        </>
    );
};

export default CardGrateIcon;
