import { performExistCallback } from '@/components/utils/performExistCallback';
import React from 'react';
import style from '../../../style/elements/checkbox.module.scss';
import Svg from '../Svg/Svg';

interface CheckboxProps {
    className?: string;
    isChecked: boolean;
    toggle?: () => void;
}

const Checkbox: React.FC<CheckboxProps> = ({
    isChecked,
    toggle,
    className = '',
}) => {
    return (
        <>
            <div
                onClick={(event) => {
                    event.preventDefault();
                    performExistCallback(toggle);
                }}
                className={`${style.button} ${className}`}
            >
                <Svg
                    symbol={
                        isChecked
                            ? 'dark_checkbox_checked'
                            : 'dark_checkbox_unchecked'
                    }
                />
            </div>
        </>
    );
};

export default Checkbox;
