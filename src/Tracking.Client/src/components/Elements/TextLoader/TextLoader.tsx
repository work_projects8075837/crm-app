import React from 'react';
import style from '../../../style/elements/loader_text.module.scss';

interface TextLoaderProps {
    text?: string;
    className?: string;
}

const TextLoader: React.FC<TextLoaderProps> = ({
    text = 'Загрузка...',
    className = '',
}) => {
    return (
        <>
            <div className={`${style.loaderText} ${className}`}>{text}</div>
        </>
    );
};

export default TextLoader;
