import React from 'react';
import style from '../../../style/elements/blue_button.module.scss';
import { Link, LinkProps } from 'react-router-dom';

interface BlueLinkProps extends LinkProps {
    className?: string;
}

const BlueLink: React.FC<BlueLinkProps> = ({
    children,
    className = '',
    ...linkProps
}) => {
    return (
        <>
            <Link {...linkProps} className={`${style.button} ${className}`}>
                {children}
            </Link>
        </>
    );
};

export default BlueLink;
