import { createRoot } from 'react-dom/client';
import WarnWindow from './WarnWindow';

const confirmRootElem = document.createElement('div');
const body = document.querySelector('body');
body?.appendChild(confirmRootElem);

export const warnMessage = (props: {
    text: string;
    title?: string;
    zIndex?: number;
    isHtml?: boolean;
}): Promise<void> =>
    new Promise((res) => {
        const confirmRoot = createRoot(confirmRootElem);
        const close = () => {
            confirmRoot.unmount();
            res();
        };

        confirmRoot.render(<WarnWindow {...{ ...props, close }} />);
    });
