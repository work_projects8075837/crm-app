import Svg from '@/components/Elements/Svg/Svg';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import React from 'react';
import style from '../../../style/elements/confirm_window.module.scss';
import { viewImage } from '../ImageViewModal/viewImage';

interface WarnWindowProps {
    title?: string;
    isHtml?: boolean;
    zIndex?: number;

    text: string;
    close: () => void;
}

const WarnWindow: React.FC<WarnWindowProps> = ({
    title = 'Предупреждение',
    isHtml = false,
    zIndex,
    text,
    close,
}) => {
    return (
        <>
            <div className={style.modalShadow} style={{ zIndex }} />
            <div
                className={style.confirmWindowLayout}
                style={{ zIndex: zIndex && zIndex + 1 }}
            >
                <div className={style.layoutHeader}>
                    <div className={style.headerContent}>
                        <h3 className={style.modalTitle}>{title}</h3>
                    </div>
                    <button className={style.close} onClick={() => close()}>
                        <Svg symbol='cross' className={style.closeSvg} />
                    </button>
                </div>
                <div className={style.fullModalContent}>
                    <div className={style.modalVertGrid}>
                        {isHtml ? (
                            <div
                                className={style.confirmText}
                                dangerouslySetInnerHTML={{ __html: text }}
                                onClick={async (event) => {
                                    event.stopPropagation();
                                    if (
                                        event.target instanceof HTMLImageElement
                                    ) {
                                        await viewImage(event.target.src, 10);
                                    }
                                }}
                            />
                        ) : (
                            <div className={style.confirmText}>{text}</div>
                        )}
                        <div className={style.buttons}>
                            <SaveButton onClick={() => close()}>
                                Окей
                            </SaveButton>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default WarnWindow;
