import React from 'react';
import '../../../style/elements/switcher.scss';

export interface SwitcherProps {
    defaultChecked?: boolean;
    onChange?: () => void;
}

const Switcher: React.FC<SwitcherProps> = ({
    onChange,
    defaultChecked = false,
}) => {
    return (
        <>
            <input
                type='checkbox'
                defaultChecked={defaultChecked}
                onChange={onChange}
                className={'switcherInput'}
            />
            <div className={'switcher'}>
                <span className={'switcherDot'} />
            </div>
        </>
    );
};

export default Switcher;
