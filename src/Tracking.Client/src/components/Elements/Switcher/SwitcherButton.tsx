import React from 'react';
import Switcher, { SwitcherProps } from './Switcher';

interface SwitcherButtonProps extends SwitcherProps {
    text: string;
}

const SwitcherButton: React.FC<SwitcherButtonProps> = ({ text, ...props }) => {
    return (
        <>
            <label className='switcherField'>
                <Switcher {...props} />

                <span>{text}</span>
            </label>
        </>
    );
};

export default SwitcherButton;
