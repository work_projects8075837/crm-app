import Svg from '@/components/Elements/Svg/Svg';
import { performExistCallback } from '@/components/utils/performExistCallback';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import style from '../../../style/layout/modal_window.module.scss';

interface ModalWindowProps {
    openHandler: OpenHandler;
    title: string | React.ReactNode;
    children: React.ReactNode;
    headerChildren?: React.ReactNode;
    layoutClass?: string;
    contentClass?: string;
    headerClass?: string;
    hidden?: boolean;
    onClose?: () => void;
    withoutModalContent?: boolean;
}

const ModalWindow: React.FC<ModalWindowProps> = ({
    title,
    openHandler,
    children,
    layoutClass = '',
    contentClass = '',
    headerClass = '',
    hidden = false,
    withoutModalContent = false,
    onClose,
    headerChildren,
}) => {
    useEffect(() => {
        return () => openHandler.close();
    }, []);
    return (
        <>
            {openHandler.isOpen && (
                <>
                    <div
                        hidden={hidden}
                        className={style.modalShadow}
                        onClick={() => {
                            openHandler.close();
                            performExistCallback(onClose);
                        }}
                    />

                    <div
                        hidden={hidden}
                        className={`${style.modalLayout} ${layoutClass}`}
                    >
                        <div className={`${style.layoutHeader} ${headerClass}`}>
                            <div className={style.headerContent}>
                                <h3 className={style.modalTitle}>{title}</h3>
                            </div>
                            {headerChildren}
                            <button
                                className={style.close}
                                onClick={() => {
                                    openHandler.close();
                                    performExistCallback(onClose);
                                }}
                            >
                                <Svg
                                    symbol='cross'
                                    className={style.closeSvg}
                                />
                            </button>
                        </div>
                        {withoutModalContent ? (
                            children
                        ) : (
                            <div
                                className={`${style.modalContent} ${contentClass}`}
                            >
                                {children}
                            </div>
                        )}
                    </div>
                </>
            )}
        </>
    );
};

export default observer(ModalWindow);
