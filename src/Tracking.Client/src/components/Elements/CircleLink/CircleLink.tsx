import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/elements/tab.module.scss';

interface CircleLinkProps {
    to: string;
    children: React.ReactNode;
}

const CircleLink: React.FC<CircleLinkProps> = ({ to, children }) => {
    return (
        <Link to={to} className={style.tab} target='_blank'>
            {children}
        </Link>
    );
};

export default CircleLink;
