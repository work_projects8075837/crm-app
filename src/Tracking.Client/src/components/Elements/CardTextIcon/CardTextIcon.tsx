import React from 'react';
import style from '../../../style/elements/icon_text.module.scss';

interface CardTextIconProps {
    text: string;
}

const CardTextIcon: React.FC<CardTextIconProps> = ({ text }) => {
    return (
        <>
            <span className={style.text}>{text}</span>
        </>
    );
};

export default CardTextIcon;
