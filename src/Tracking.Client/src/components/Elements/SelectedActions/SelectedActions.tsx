import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';
import SelectedActionsBottomLayout from './SelectedActionsBottom/SelectedActionsBottomLayout';

interface SelectedActionsProps {
    checkHandler: CheckEntitiesHandler;
    children?: React.ReactNode;
}

const SelectedActions: React.FC<SelectedActionsProps> = ({
    checkHandler,
    children,
}) => {
    return (
        <>
            {!!checkHandler.checkedEntities.length && (
                <SelectedActionsBottomLayout
                    count={checkHandler.checkedEntities.length}
                >
                    {children}
                </SelectedActionsBottomLayout>
            )}
        </>
    );
};

export default observer(SelectedActions);
