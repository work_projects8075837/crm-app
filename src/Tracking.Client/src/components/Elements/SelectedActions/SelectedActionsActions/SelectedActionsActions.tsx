import React from 'react';
import popupStyle from '../../../../style/elements/popup.module.scss';

import ModalMenu from '../../ModalMenu/ModalMenu';
import DeleteSelectedItemsAction, {
    DeleteSelectedItemsActionProps,
} from '../DeleteSelectedItems/DeleteSelectedItemsAction';
interface SelectedActionsActionsProps extends DeleteSelectedItemsActionProps {
    children?: React.ReactNode;
}

const SelectedActionsActions: React.FC<SelectedActionsActionsProps> = ({
    children,
    ...deleteActionProps
}) => {
    return (
        <>
            {/* <div className={style.mediumWidth}> */}
            <ModalMenu
                className='rowCenter'
                popupClass={popupStyle.popupTopClear}
                side='top'
            >
                {children}
                <DeleteSelectedItemsAction {...deleteActionProps} />
            </ModalMenu>
            {/* </div> */}
        </>
    );
};

export default SelectedActionsActions;
