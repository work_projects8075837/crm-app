import { ADMIN_ROLE } from '@/components/Providers/AuthGuard/AuthProvider';
import { useAuthQuery } from '@/components/Providers/AuthGuard/useAuthQuery';
import ActionsMenuButton from '@/components/Tickets/TicketActionsMenu/ActionsMenuButton';
import { invalidateKey } from '@/store/queries/actions/base';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useMutation } from '@tanstack/react-query';
import { AxiosResponse } from 'axios';
import { observer } from 'mobx-react-lite';
import React from 'react';
import toast from 'react-hot-toast';
import { confirmAction } from '../../ConfirmDialogWindow/confirmWindow';
import { globalLoader } from '../../GlobalLoader/GlobalLoader';

export interface DeleteSelectedItemsActionProps {
    service: { hideArray: (d: string[]) => Promise<AxiosResponse> };
    keys: (string | symbol)[];
    checkHandler: CheckEntitiesHandler;
    refetch?: () => Promise<unknown>;
}

const DeleteSelectedItemsAction: React.FC<DeleteSelectedItemsActionProps> = ({
    checkHandler,
    keys,
    service,
    refetch,
}) => {
    const { checkRoles } = useAuthQuery();

    const { mutate, isLoading } = useMutation({
        mutationFn: async (ids: string[]) => {
            if (!checkRoles([ADMIN_ROLE])) {
                toast.error('У вас нет прав на удаления');
                return;
            }
            globalLoader.open();
            return await service.hideArray(ids);
        },
        onSuccess: () => {
            for (const key of keys) {
                invalidateKey(key);
            }
        },
        onSettled: async () => {
            await refetch?.();
            checkHandler.uncheckAll();
            globalLoader.close();
        },
    });
    return (
        <>
            <ActionsMenuButton
                symbol='delete_bag'
                text='Удалить'
                disabled={isLoading}
                onClick={async () => {
                    if (
                        await confirmAction({
                            title: 'Подтвердите удаление',
                            text: 'Вы действительно хотите продолжить и совершить удаление?',
                        })
                    ) {
                        mutate(
                            checkHandler.checkedEntities.map(
                                (entity) => entity.id,
                            ),
                        );
                    }
                }}
            />
        </>
    );
};

export default observer(DeleteSelectedItemsAction);
