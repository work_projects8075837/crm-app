import React, { ButtonHTMLAttributes } from 'react';
import WhiteButton from '../../WhiteButton/WhiteButton';
import style from '../../../../style/layout/selected_bottom.module.scss';

type SelectedActionsButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;

const SelectedActionsButton: React.FC<SelectedActionsButtonProps> = ({
    ...buttonProps
}) => {
    return (
        <>
            <WhiteButton className={style.button} {...buttonProps}>
                Действия
            </WhiteButton>
        </>
    );
};

export default SelectedActionsButton;
