import React from 'react';
import SelectedActions from '../SelectedActions';
import SelectedActionsActions from '../SelectedActionsActions/SelectedActionsActions';
import { DeleteSelectedItemsActionProps } from './../DeleteSelectedItems/DeleteSelectedItemsAction';

interface EntitySelectedActionsProps extends DeleteSelectedItemsActionProps {
    children?: React.ReactNode;
}

const EntitySelectedActions: React.FC<EntitySelectedActionsProps> = ({
    children,
    ...deleteProps
}) => {
    return (
        <>
            <SelectedActions checkHandler={deleteProps.checkHandler}>
                <SelectedActionsActions {...deleteProps}>
                    {children}
                </SelectedActionsActions>
            </SelectedActions>
        </>
    );
};

export default EntitySelectedActions;
