import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import React from 'react';
import popupStyle from '../../../../style/elements/popup.module.scss';
import ModalMenu from '../../ModalMenu/ModalMenu';
import SelectedActions from '../SelectedActions';

interface BaseSelectedActionsProps {
    children: React.ReactNode;
    checkHandler: CheckEntitiesHandler;
}

const BaseSelectedActions: React.FC<BaseSelectedActionsProps> = ({
    checkHandler,
    children,
}) => {
    return (
        <>
            <SelectedActions checkHandler={checkHandler}>
                <ModalMenu
                    className='rowCenter'
                    popupClass={popupStyle.popupTopClear}
                    side='top'
                >
                    {children}
                </ModalMenu>
            </SelectedActions>
        </>
    );
};

export default BaseSelectedActions;
