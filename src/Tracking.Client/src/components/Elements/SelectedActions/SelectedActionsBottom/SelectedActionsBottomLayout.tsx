import React from 'react';
import style from '../../../../style/layout/selected_bottom.module.scss';
import SelectedActionsButton from '../SelectedActionsButton/SelectedActionsButton';

interface SelectedActionsBottomLayoutProps {
    count: number;
    children?: React.ReactNode;
}

const SelectedActionsBottomLayout: React.FC<
    SelectedActionsBottomLayoutProps
> = ({ children, count }) => {
    return (
        <>
            <div className={style.actionsBottom}>
                <div className={style.buttons}>
                    {children}
                    <span className={style.selected}>
                        Выбрано элементов: {count}
                    </span>
                </div>
            </div>
        </>
    );
};

export default SelectedActionsBottomLayout;
