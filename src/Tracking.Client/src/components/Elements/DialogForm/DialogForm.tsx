import SaveButton, {
    SaveButtonProps,
} from '@/components/Fields/Buttons/SaveButton';
import BaseForm from '@/components/Form/BaseForm/BaseForm';
import { useBaseForm } from '@/components/Form/hooks/useBaseForm';
import React from 'react';
import { UseFormProps } from 'react-hook-form';
import style from '../../../style/elements/confirm_window.module.scss';
import Svg from '../Svg/Svg';

export interface DialogFormFields {
    [key: string]: string | number;
}

interface DialogFormProps<T extends DialogFormFields> {
    children: React.ReactNode;
    title: string;
    giveResult: (result?: T) => void;
    formProps?: UseFormProps<T>;
    buttonProps?: SaveButtonProps;
}

const DialogForm = <T extends DialogFormFields>({
    title,
    children,
    giveResult,
    formProps,
    buttonProps,
}: DialogFormProps<T>) => {
    const { form } = useBaseForm<T>(formProps);
    const onSubmit = form.handleSubmit((data) => {
        giveResult(data);
    });
    return (
        <>
            <div className={style.modalShadow} onClick={() => giveResult()} />
            <div className={style.confirmWindowLayout}>
                <div className={style.layoutHeader}>
                    <div className={style.headerContent}>
                        <h3 className={style.modalTitle}>{title}</h3>
                    </div>
                    <button
                        className={style.close}
                        onClick={() => giveResult()}
                    >
                        <Svg symbol='cross' className={style.closeSvg} />
                    </button>
                </div>

                <div className={style.modalContent}>
                    <BaseForm
                        {...form}
                        htmlFormProps={{
                            className: style.modalVertAround,
                            onSubmit,
                        }}
                    >
                        {children}
                        <SaveButton {...buttonProps}>Сохранить</SaveButton>
                    </BaseForm>
                </div>
            </div>
        </>
    );
};

export default DialogForm;
