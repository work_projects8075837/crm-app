import { SaveButtonProps } from '@/components/Fields/Buttons/SaveButton';
import { RefObject } from 'react';
import { createRoot } from 'react-dom/client';
import { UseFormProps } from 'react-hook-form';
import DialogForm, { DialogFormFields } from './DialogForm';

const RootElem = document.createElement('div');
const body = document.querySelector('body');
body?.appendChild(RootElem);

interface Props<T extends DialogFormFields, R> {
    title: string;
    fields: React.ReactNode;
    formProps?: UseFormProps<T>;
    buttonProps?: SaveButtonProps;
    answerFn?: (data?: T) => Promise<R | void>;
}

export const questionForm = <T extends DialogFormFields, R = T>(
    { title, fields, formProps, buttonProps, answerFn }: Props<T, R>,
    ref?: RefObject<HTMLElement>,
): Promise<T | R | void> =>
    new Promise((res) => {
        const root = createRoot(ref?.current || RootElem);
        const giveResult = (result?: T) => {
            if (answerFn) {
                answerFn(result).then((data) => {
                    root.unmount();
                    res(data);
                });
            } else {
                res(result);
                root.unmount();
            }
        };

        root.render(
            <DialogForm
                title={title}
                giveResult={giveResult}
                formProps={formProps}
                buttonProps={buttonProps}
            >
                {fields}
            </DialogForm>,
        );
    });
