import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/elements/dash.module.scss';

export interface DashProps {
    className?: string;
    height?: number;
    width?: number;
}

const Dash: React.FC<DashProps> = ({
    height = 2,
    width = 13,
    className = '',
}) => {
    return (
        <>
            <Svg
                symbol='dash'
                className={`${style.dash} ${className}`}
                style={{ height, width }}
            />
        </>
    );
};

export default Dash;
