import React, { ButtonHTMLAttributes } from 'react';
import style from '../../../style/layout/ticket.module.scss';

interface WhiteButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    children: React.ReactNode;
    className?: string;
    isLoading?: boolean;
    loadingText?: string;
}

const WhiteButton: React.FC<WhiteButtonProps> = ({
    children,
    className = '',
    loadingText,
    isLoading,
    ...buttonProps
}) => {
    return (
        <>
            <button
                {...buttonProps}
                className={`${style.ticketSubmitButton} ${className}`}
                disabled={isLoading}
            >
                {isLoading ? (
                    loadingText
                ) : (
                    <>
                        <span>{children}</span>
                    </>
                )}
            </button>
        </>
    );
};

export default WhiteButton;
