import { TabHandler } from '@/store/states/TabHandler.ts';
import React from 'react';
import style from '../../../style/elements/tab.module.scss';
import { observer } from 'mobx-react-lite';

interface CircleTabBtnProps {
    tabHandler: TabHandler;
    toTab: string;
    text: string;
}

const CircleTabBtn: React.FC<CircleTabBtnProps> = ({
    toTab,
    tabHandler,
    text,
}) => {
    return (
        <>
            <button
                onClick={(event) => {
                    event.preventDefault();
                    tabHandler.setTab(toTab);
                }}
                className={
                    tabHandler.tab === toTab ? style.tabCurrent : style.tab
                }
            >
                {text}
            </button>
        </>
    );
};

export default observer(CircleTabBtn);
