import React from 'react';
import style from '../../../style/elements/img_view.module.scss';
import Svg from '../Svg/Svg';

interface ImageViewModalProps {
    src: string;
    zIndex?: number;
    close: () => void;
}

const ImageViewModal: React.FC<ImageViewModalProps> = ({
    src,
    close,
    zIndex,
}) => {
    return (
        <>
            <div
                className={style.modalShadow}
                onClick={close}
                style={{ zIndex }}
            />
            <div
                className={style.wrapper}
                style={{ zIndex: zIndex && zIndex + 1 }}
            >
                <img src={src} className={style.img} />
                <button onClick={close} className={style.button}>
                    <Svg symbol='add_plus' />
                </button>
            </div>
        </>
    );
};

export default ImageViewModal;
