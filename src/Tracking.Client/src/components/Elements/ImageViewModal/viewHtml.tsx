import { createRoot } from 'react-dom/client';

const confirmRootElem = document.createElement('div');
const body = document.querySelector('body');
body?.appendChild(confirmRootElem);

const viewImage = (jsx: string): Promise<void> =>
    new Promise((res) => {
        const confirmRoot = createRoot(confirmRootElem);
        const close = () => {
            confirmRoot.unmount();
            res();
        };

        confirmRoot.render(jsx);
    });
