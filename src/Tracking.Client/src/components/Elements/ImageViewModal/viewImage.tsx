import { createRoot } from 'react-dom/client';
import ImageViewModal from './ImageViewModal';

const confirmRootElem = document.createElement('div');
const body = document.querySelector('body');
body?.appendChild(confirmRootElem);

export const viewImage = (src: string, zIndex?: number): Promise<void> =>
    new Promise((res) => {
        const confirmRoot = createRoot(confirmRootElem);
        const close = () => {
            confirmRoot.unmount();
            res();
        };

        confirmRoot.render(<ImageViewModal {...{ close, src, zIndex }} />);
    });
