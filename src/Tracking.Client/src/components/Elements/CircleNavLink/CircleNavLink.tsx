import React from 'react';
import { NavLink } from 'react-router-dom';
import style from '../../../style/elements/tab.module.scss';

interface CircleNavLinkProps {
    text: string | React.ReactNode;
    to: string;
}

const CircleNavLink: React.FC<CircleNavLinkProps> = ({ to, text }) => {
    return (
        <>
            <NavLink
                to={to}
                className={({ isActive }) =>
                    isActive ? style.tabCurrent : style.tab
                }
            >
                {text}
            </NavLink>
        </>
    );
};

export default CircleNavLink;
