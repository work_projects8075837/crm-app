import React, { HTMLAttributes } from 'react';
import style from '../../../style/elements/tab.module.scss';

interface CircleButtonProps
    extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string;
    isActive?: boolean;
    text: string;
}

const CircleButton: React.FC<CircleButtonProps> = ({
    className = '',
    isActive = false,
    text,
    ...buttonProps
}) => {
    return (
        <>
            <button
                {...buttonProps}
                className={`${
                    isActive ? style.tabCurrent : style.tab
                } ${className}`}
            >
                {text}
            </button>
        </>
    );
};

export default CircleButton;
