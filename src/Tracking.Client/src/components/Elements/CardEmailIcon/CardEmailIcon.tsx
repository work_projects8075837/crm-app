import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

const CardEmailIcon: React.FC = () => {
    return (
        <>
            <Svg symbol='email' className={style.optionEmailSvg} />
        </>
    );
};

export default CardEmailIcon;
