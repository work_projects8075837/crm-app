import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

const CardClockIcon: React.FC = () => {
    return (
        <>
            <Svg symbol='dark_clock' className={style.optionItemSvg} />
        </>
    );
};

export default CardClockIcon;
