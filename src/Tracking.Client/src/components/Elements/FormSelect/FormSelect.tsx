import React from 'react';
import style from '../../../style/elements/form_select.module.scss';
import ArrowOpen from './ArrowOpen';
import { useBooleanState } from '@/hooks/useBooleanState';

interface FormSelectProps {
    children: React.ReactNode;
    childrenClass?: string;
    title: string;
}

const FormSelect: React.FC<FormSelectProps> = ({
    children,
    childrenClass = '',
    title,
}) => {
    // const { trackRefElem, toggle, isOpen } = useFormSelectHandler();
    const [isOpen, { toggle }] = useBooleanState(true);

    const borderBottomRadius = isOpen ? 0 : 5;

    return (
        <div className={style.select}>
            <button
                className={style.selectHeader}
                style={{
                    borderBottomLeftRadius: borderBottomRadius,
                    borderBottomRightRadius: borderBottomRadius,
                }}
                onClick={(event) => {
                    event.preventDefault();
                    toggle();
                }}
            >
                <span>{title}</span>

                <ArrowOpen className={style.selectHeaderSvg} isOpen={isOpen} />
            </button>
            {isOpen && (
                <>
                    <div
                        className={style.selectTrack}
                        // ref={trackRefElem}
                    >
                        <div
                            className={`${style.selectChildren} ${childrenClass}`}
                        >
                            {children}
                        </div>
                    </div>
                </>
            )}
        </div>
    );
};

export default FormSelect;
