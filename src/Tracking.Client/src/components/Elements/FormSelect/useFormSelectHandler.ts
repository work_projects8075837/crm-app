import { useSelectorHandler } from '@/hooks/useSelectHandler';

export const useFormSelectHandler = () => {
    const { trackRefElem, toggle, isOpen } = useSelectorHandler<HTMLDivElement>(
        {
            onOpen: () => {
                if (trackRefElem.current) {
                    trackRefElem.current.style.borderColor = '#dae3eb';
                }
            },
            afterOpen: () => {
                if (trackRefElem.current) {
                    trackRefElem.current.style.overflow = 'visible';
                }
            },
            onClose: () => {
                if (trackRefElem.current) {
                    trackRefElem.current.style.overflow = 'hidden';
                }
            },
            afterClose: () => {
                if (trackRefElem.current) {
                    trackRefElem.current.style.borderColor = 'transparent';
                }
            },
        },
    );

    return { trackRefElem, toggle, isOpen };
};
