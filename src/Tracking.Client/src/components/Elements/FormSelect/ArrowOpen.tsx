import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

interface ArrowOpenProps {
    isOpen?: boolean;
    className?: string;
    fill?: string;
}

const ArrowOpen: React.FC<ArrowOpenProps> = ({
    isOpen = false,
    className = style.selectFieldSvg,
    fill = '#1A202C',
}) => {
    return (
        <>
            <Svg
                symbol={isOpen ? 'up_arrow' : 'down_arrow'}
                className={className}
                style={{ fill }}
            />
        </>
    );
};

export default ArrowOpen;
