import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/elements/breadcrumb.module.scss';

interface CharacterProblemsLinkProps {
    text?: string;
}

const CharacterProblemsLink: React.FC<CharacterProblemsLinkProps> = ({
    text = 'Характеры проблемы',
}) => {
    return (
        <>
            <Link to='/character_problem/' className={style.link}>
                {text}
            </Link>
        </>
    );
};

export default CharacterProblemsLink;
