import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/elements/breadcrumb.module.scss';

interface EquipmentLinkProps {
    text?: string;
}

const EquipmentLink: React.FC<EquipmentLinkProps> = ({
    text = 'Оборудование',
}) => {
    return (
        <>
            <Link to='/inventory/' className={style.link}>
                {text}
            </Link>
        </>
    );
};

export default EquipmentLink;
