import React from 'react';
import style from '../../../style/elements/breadcrumb.module.scss';
import { Link } from 'react-router-dom';

interface DirectionsLinkProps {
    text?: string;
}

const DirectionsLink: React.FC<DirectionsLinkProps> = ({
    text = 'Направления',
}) => {
    return (
        <>
            <Link to='/direction/' className={style.link}>
                {text}
            </Link>
        </>
    );
};

export default DirectionsLink;
