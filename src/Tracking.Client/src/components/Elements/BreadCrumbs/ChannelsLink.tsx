import React from 'react';
import style from '../../../style/elements/breadcrumb.module.scss';
import { Link } from 'react-router-dom';

interface ChannelsLinkProps {
    text?: string;
}

const ChannelsLink: React.FC<ChannelsLinkProps> = ({
    text = 'Каналы связи',
}) => {
    return (
        <>
            <Link to='/direction/' className={style.link}>
                {text}
            </Link>
        </>
    );
};

export default ChannelsLink;
