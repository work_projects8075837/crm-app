import React from 'react'
import { Link, useParams } from 'react-router-dom'
import style from '../../../style/elements/breadcrumb.module.scss'

interface CurrentSalePointInventoryLinkProps {
    text?: string;
}

const CurrentSalePointInventoryLink: React.FC<
    CurrentSalePointInventoryLinkProps
> = ({ text = 'Оборудование' }) => {
    const { salePointId } = useParams();
    return (
        <>
            <Link
                to={`/sale_point/single/${salePointId}/inventory/`}
                className={style.link}
            >
                {text}
            </Link>
        </>
    );
};

export default CurrentSalePointInventoryLink;
