import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/elements/breadcrumb.module.scss';

interface CounterPartiesLinkProps {
    text?: string;
}

const CounterPartiesLink: React.FC<CounterPartiesLinkProps> = ({
    text = 'Контрагенты',
}) => {
    return (
        <>
            <Link to='/counterparty/' className={style.link}>
                {text}
            </Link>
        </>
    );
};

export default CounterPartiesLink;
