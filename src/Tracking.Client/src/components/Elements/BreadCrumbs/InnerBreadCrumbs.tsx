import { IChildEntity } from '@/store/queries/stores/base/useDirectoryBreadCrumbs';
import { Fragment } from 'react';
import BreadCrumbLink from './BreadCrumbLink';

interface InnerBreadCrumbsProps {
    links?: IChildEntity[];
    route: string;
}

const InnerBreadCrumbs = ({ links, route }: InnerBreadCrumbsProps) => {
    return (
        <>
            {links?.map(({ id, name }, index) => (
                <Fragment key={id}>
                    {' '}
                    /{' '}
                    <BreadCrumbLink to={`/${route}/${id}/`}>
                        {name}
                    </BreadCrumbLink>
                </Fragment>
            ))}
        </>
    );
};

export default InnerBreadCrumbs;
