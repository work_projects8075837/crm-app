import React from 'react';
import BreadCrumbLink from './BreadCrumbLink';

interface JobsLinkProps {
    text?: string;
}

const JobsLink: React.FC<JobsLinkProps> = ({ text = 'Работы' }) => {
    return (
        <>
            <BreadCrumbLink to={'/jobs/'}>{text}</BreadCrumbLink>
        </>
    );
};

export default JobsLink;
