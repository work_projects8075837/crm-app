import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/elements/breadcrumb.module.scss';

interface SalePointsLinkProps {
    text?: string;
}

const SalePointsLink: React.FC<SalePointsLinkProps> = ({
    text = 'Заведения',
}) => {
    return (
        <>
            <Link to='/sale_point/' className={style.link}>
                {text}
            </Link>
        </>
    );
};

export default SalePointsLink;
