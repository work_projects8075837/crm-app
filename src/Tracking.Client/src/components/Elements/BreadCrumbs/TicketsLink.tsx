import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/elements/breadcrumb.module.scss';

interface TicketsLinkProps {
    text?: string;
}

const TicketsLink: React.FC<TicketsLinkProps> = ({ text = 'Главная' }) => {
    return (
        <>
            <Link to='/' className={style.link}>
                {text}
            </Link>
        </>
    );
};

export default TicketsLink;
