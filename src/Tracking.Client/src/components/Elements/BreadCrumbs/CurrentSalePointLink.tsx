import { useSalePointByParams } from '@/store/queries/stores/sale_point/useSalePointByParams';
import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/elements/breadcrumb.module.scss';

const CurrentSalePointLink: React.FC = () => {
    const { data } = useSalePointByParams();
    return (
        <>
            <Link to={`/sale_point/single/${data?.id}/`} className={style.link}>
                {data?.name}
            </Link>
        </>
    );
};

export default CurrentSalePointLink;
