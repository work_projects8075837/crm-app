import React from 'react';
import { Link, LinkProps } from 'react-router-dom';
import style from '../../../style/elements/breadcrumb.module.scss';

const BreadCrumbLink: React.FC<LinkProps> = ({ children, ...linkProps }) => {
    return (
        <>
            <Link {...linkProps} className={style.link}>
                {children}
            </Link>
        </>
    );
};

export default BreadCrumbLink;
