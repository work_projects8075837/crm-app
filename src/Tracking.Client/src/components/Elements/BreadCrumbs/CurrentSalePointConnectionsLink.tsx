import React from 'react';
import { Link, useParams } from 'react-router-dom';
import style from '../../../style/elements/breadcrumb.module.scss';

interface CurrentSalePointConnectionsLinkProps {
    text?: string;
}

const CurrentSalePointConnectionsLink: React.FC<
    CurrentSalePointConnectionsLinkProps
> = ({ text = 'Подключения' }) => {
    const { salePointId } = useParams();
    return (
        <>
            <Link
                to={`/sale_point/single/${salePointId}/connections/`}
                className={style.link}
            >
                {text}
            </Link>
        </>
    );
};

export default CurrentSalePointConnectionsLink;
