import { RefObject } from 'react';
import { createRoot } from 'react-dom/client';
import TooltipConfirm from './TooltipConfirm';

const confirmRootElem = document.createElement('div');
const body = document.querySelector('body');
body?.appendChild(confirmRootElem);

export const tooltipConfirm = (
    {
        text,
        title,
    }: {
        text: string;
        title?: string;
    },
    ref?: RefObject<HTMLElement>,
): Promise<boolean> =>
    new Promise((res) => {
        const confirmRoot = createRoot(ref?.current || confirmRootElem);
        const giveAnswer = (a: boolean) => {
            res(a);
            confirmRoot.unmount();
        };

        confirmRoot.render(<TooltipConfirm {...{ title, giveAnswer, text }} />);
    });
