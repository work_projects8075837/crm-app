import React from 'react';
import style from '../../../style/elements/tooltip_confirm.module.scss';
import Svg from '../Svg/Svg';

interface TooltipConfirmProps {
    title?: string;
    text: string;
    giveAnswer: (a: boolean) => void;
}

const TooltipConfirm: React.FC<TooltipConfirmProps> = ({
    title = 'Подтверждение',
    text,
    giveAnswer,
}) => {
    return (
        <div className={style.tooltip}>
            <div className={style.header}>
                <h4>{title}</h4>

                <button
                    className={style.close}
                    onClick={() => giveAnswer(false)}
                >
                    <Svg symbol='cross' />
                </button>
            </div>
            <div className={style.content}>
                <span>{text}</span>
            </div>
            <div className={style.bottom}>
                <button className={style.button}>Нет</button>
                <button className={style.buttonPrimary}>Да</button>
            </div>
        </div>
    );
};

export default TooltipConfirm;
