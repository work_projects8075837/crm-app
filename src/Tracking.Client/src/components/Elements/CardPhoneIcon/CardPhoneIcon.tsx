import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

const CardPhoneIcon: React.FC = () => {
    return (
        <>
            <Svg symbol='phone' className={style.optionPhoneSvg} />
        </>
    );
};

export default CardPhoneIcon;
