import React from 'react';

interface EmptyTasksProps {
    text?: string;
}

const EmptyTasks: React.FC<EmptyTasksProps> = ({
    text = 'Нет заявок в системе',
}) => {
    return (
        <>
            <div className={'task-nav--container'}>
                <div className={'none--task--container'}>
                    <img src='/none--task.svg' alt='' />
                    <div className={'none-task--text'}>{text}</div>
                </div>
            </div>
        </>
    );
};

export default EmptyTasks;
