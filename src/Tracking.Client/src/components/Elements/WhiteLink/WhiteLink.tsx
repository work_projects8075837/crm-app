import { Link, LinkProps } from 'react-router-dom';
import style from '../../../style/elements/blue_button.module.scss';

import React from 'react';

interface WhiteLinkProps extends LinkProps {
    className?: string;
}

const WhiteLink: React.FC<WhiteLinkProps> = ({
    className,
    children,
    ...linkProps
}) => {
    return (
        <Link {...linkProps} className={`${style.whiteButton} ${className}`}>
            {children}
        </Link>
    );
};

export default WhiteLink;
