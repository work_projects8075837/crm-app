import React from 'react';
import style from '../../../style/layout/character_problem.module.scss';
import Svg from '../Svg/Svg';

const DirectoryIcon: React.FC = () => {
    return (
        <>
            <Svg symbol='directory_in' className={style.directoryArrow} />
        </>
    );
};

export default DirectoryIcon;
