import React from 'react';
import NullEntities from '../NullEntities/NullEntities';
import EntityLoaderCircle from '../LoaderCircle/EntityLoaderCircle';

interface EntitiesLoadingProps {
    children: React.ReactNode;
    isLoading?: boolean;
    isEmpty: boolean;
    emptyText: string;
}

const EntitiesLoading: React.FC<EntitiesLoadingProps> = ({
    children,
    isLoading,
    isEmpty,
    emptyText,
}) => {
    return (
        <>
            {!isLoading ? (
                <>{!isEmpty ? children : <NullEntities text={emptyText} />}</>
            ) : (
                <EntityLoaderCircle />
            )}
        </>
    );
};

export default EntitiesLoading;
