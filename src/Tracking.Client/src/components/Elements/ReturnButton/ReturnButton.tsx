import React, { ButtonHTMLAttributes } from 'react';
import style from '../../../style/elements/return_button.module.scss';
import Svg from '../Svg/Svg';

type ReturnButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;

const ReturnButton: React.FC<ReturnButtonProps> = ({
    children,
    className = '',
    ...buttonProps
}) => {
    return (
        <>
            <button {...buttonProps} className={`${style.button} ${className}`}>
                <Svg symbol='return_arrow' />
                <span>{children}</span>
            </button>
        </>
    );
};

export default ReturnButton;
