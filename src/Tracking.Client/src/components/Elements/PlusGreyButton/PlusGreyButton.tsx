import Svg from '@/components/Elements/Svg/Svg';
import React, { HTMLAttributes } from 'react';
import style from '../../../style/elements/plus.module.scss';

interface PlusGreyButtonProps extends HTMLAttributes<HTMLButtonElement> {
    className?: string;
}

const PlusGreyButton: React.FC<PlusGreyButtonProps> = ({
    className = '',
    ...buttonProps
}) => {
    return (
        <>
            <button
                {...buttonProps}
                className={`${style.plusButton} ${className}`}
            >
                <Svg symbol='add_plus' />
            </button>
        </>
    );
};

export default PlusGreyButton;
