import { OpenHandler } from '@/store/states/OpenHandler';
import { StyleLoader } from '@/style/elements/elements';
import { observer } from 'mobx-react-lite';
import React from 'react';
import LoaderCircle from '../LoaderCircle/LoaderCircle';

export const globalLoader = new OpenHandler();

const GlobalLoader: React.FC = () => {
    return (
        <>
            {globalLoader.isOpen && (
                <div className={StyleLoader.globalLoaderPlace}>
                    <LoaderCircle />
                </div>
            )}
        </>
    );
};

export default observer(GlobalLoader);
