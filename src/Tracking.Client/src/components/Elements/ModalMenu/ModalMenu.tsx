import ActionsMenu from '@/components/Tickets/TicketActionsMenu/ActionsMenu';
import TicketOptionButton from '@/components/Tickets/TicketOptions/TicketOptionButton';
import { OpenHandler } from '@/store/states/OpenHandler';
import React, { ButtonHTMLAttributes, useState } from 'react';
import style from '../../../style/elements/popup.module.scss';

interface ModalMenuProps {
    symbol?: string;
    text?: string;
    children?: React.ReactNode;
    side?: 'top' | 'bottom';
    OpenButton?: React.FC<ButtonHTMLAttributes<HTMLButtonElement>>;
    popupClass?: string;
    className?: string;
}

const ModalMenu: React.FC<ModalMenuProps> = ({
    children,
    OpenButton,
    className = '',
    popupClass = '',
    symbol = 'ticket_edit',
    text = 'Действия',
    side = 'bottom',
}) => {
    const [open] = useState(() => new OpenHandler(false));

    return (
        <>
            <div
                className={`rowStartEnd ${className}`}
                style={{
                    alignItems: side === 'top' ? 'flex-start' : 'flex-end',
                }}
            >
                {side === 'top' && (
                    <ActionsMenu
                        openHandler={open}
                        className={`${style.popupTop} ${popupClass}`}
                    >
                        {children}
                    </ActionsMenu>
                )}
                {OpenButton ? (
                    <OpenButton
                        onClick={(event) => {
                            event.stopPropagation();
                            open.toggle();
                        }}
                    />
                ) : (
                    <TicketOptionButton
                        onClick={() => {
                            open.toggle();
                        }}
                        symbol={symbol}
                        text={text}
                    />
                )}

                {side === 'bottom' && (
                    <ActionsMenu openHandler={open} className={popupClass}>
                        {children}
                    </ActionsMenu>
                )}
            </div>
        </>
    );
};

export default ModalMenu;
