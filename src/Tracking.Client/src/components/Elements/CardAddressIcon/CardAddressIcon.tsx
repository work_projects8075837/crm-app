import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

const CardAddressIcon: React.FC = () => {
    return (
        <>
            <Svg symbol='location' className={style.optionLocationSvg} />
        </>
    );
};

export default CardAddressIcon;
