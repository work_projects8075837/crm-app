import React from 'react';
import LoaderCircle from './LoaderCircle';
import style from '../../../style/elements/loader_text.module.scss';

const EntityLoaderCircle: React.FC = () => {
    return (
        <>
            <LoaderCircle size={100} className={style.loaderPlaceFull} />
        </>
    );
};

export default EntityLoaderCircle;
