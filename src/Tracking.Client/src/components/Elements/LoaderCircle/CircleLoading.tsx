import React from 'react';

interface CircleLoadingProps {
    size: number;
    borderWidth?: number;
    color?: string;
}

const CircleLoading: React.FC<CircleLoadingProps> = ({
    size,
    borderWidth = 2,
    color = '#1e293b',
}) => {
    return (
        <>
            <span
                className='loader'
                style={{
                    width: size,
                    height: size,
                    borderWidth,
                    borderColor: color,
                    borderBottomColor: 'transparent',
                }}
            ></span>
        </>
    );
};

export default CircleLoading;
