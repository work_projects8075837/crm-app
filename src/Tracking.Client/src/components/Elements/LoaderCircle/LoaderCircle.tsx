import React from 'react';
import style from '../../../style/elements/loader_text.module.scss';
import CircleLoading from './CircleLoading';

export interface LoaderCircleProps {
    size?: number;
    color?: string;
    borderWidth?: number;
    className?: string;
    cover?: boolean;
}

const LoaderCircle: React.FC<LoaderCircleProps> = ({
    size = 160,
    borderWidth = 5,
    color = '#1e293b',
    cover = false,
    className = '',
}) => {
    return (
        <div
            className={`${
                cover ? style.loaderPlaceCover : style.loaderPlace
            } ${className}`}
        >
            <CircleLoading {...{ size, color, borderWidth }} />
        </div>
    );
};

export default LoaderCircle;
