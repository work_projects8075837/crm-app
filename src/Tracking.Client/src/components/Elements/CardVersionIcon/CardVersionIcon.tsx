import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

const CardVersionIcon: React.FC = () => {
    return (
        <>
            <Svg symbol='version' className={style.optionPhoneSvg} />
        </>
    );
};

export default CardVersionIcon;
