import React from 'react';
import Dash, { DashProps } from '../Dash/Dash';

interface ValueDashProps extends DashProps {
    children?: React.ReactNode;
}

const ValueDash: React.FC<ValueDashProps> = ({ children, ...dashProps }) => {
    return <>{children || <Dash {...dashProps} />}</>;
};

export default ValueDash;
