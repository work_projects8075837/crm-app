import Svg from '@/components/Elements/Svg/Svg';
import React, { ButtonHTMLAttributes } from 'react';
import '../../../style/elements/checkboxes.scss';

interface GreenCheckboxProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    isChecked: boolean;
}

const GreenCheckbox: React.FC<GreenCheckboxProps> = ({
    isChecked,
    ...buttonProps
}) => {
    return (
        <>
            <button {...buttonProps} className={'checkbox'}>
                {isChecked ? (
                    <Svg
                        symbol='green_checkbox_checked'
                        className='svgChecked'
                    />
                ) : (
                    <Svg
                        symbol='green_checkbox_empty'
                        className='svgUnchecked'
                    />
                )}
            </button>
        </>
    );
};

export default GreenCheckbox;
