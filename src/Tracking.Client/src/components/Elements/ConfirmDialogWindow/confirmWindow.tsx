import { createRoot } from 'react-dom/client';
import ConfirmDialogWindow from './ConfirmDialogWindow';

const confirmRootElem = document.createElement('div');
const body = document.querySelector('body');
body?.appendChild(confirmRootElem);

interface Props {
    title?: string;
    zIndex?: number;
    text?: string | React.ReactNode;
    options?: {
        falseButtonText?: string;
        trueButtonText?: string;
    };
}

export const confirmAction = ({
    text,
    title,
    options,
    zIndex,
}: Props): Promise<boolean> =>
    new Promise((res) => {
        const confirmRoot = createRoot(confirmRootElem);
        const giveAnswer = (answer: boolean) => {
            confirmRoot.unmount();
            res(answer);
        };

        confirmRoot.render(
            <ConfirmDialogWindow
                title={title}
                text={text}
                giveAnswer={giveAnswer}
                options={options}
                zIndex={zIndex}
            />,
        );
    });
