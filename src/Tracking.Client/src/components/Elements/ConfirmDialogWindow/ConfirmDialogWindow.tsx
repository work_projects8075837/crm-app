import Svg from '@/components/Elements/Svg/Svg';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import React from 'react';
import style from '../../../style/elements/confirm_window.module.scss';
import WhiteButton from '../WhiteButton/WhiteButton';

interface ConfirmDialogWindowProps {
    title?: string;
    zIndex?: number;

    text?: string | React.ReactNode;
    options?: {
        falseButtonText?: string;
        trueButtonText?: string;
    };
    giveAnswer: (answer: boolean) => void;
}

const ConfirmDialogWindow: React.FC<ConfirmDialogWindowProps> = ({
    title = 'Подтвердите',
    text = '',
    giveAnswer,
    options,
    zIndex,
}) => {
    return (
        <>
            <div
                className={style.modalShadow}
                style={{ zIndex }}
                onClick={() => giveAnswer(false)}
            />
            <div
                className={style.confirmWindowLayout}
                style={{ zIndex: zIndex && zIndex + 1 }}
            >
                <div className={style.layoutHeader}>
                    <div className={style.headerContent}>
                        <h3 className={style.modalTitle}>{title}</h3>
                    </div>
                    <button
                        className={style.close}
                        onClick={() => giveAnswer(false)}
                    >
                        <Svg symbol='cross' className={style.closeSvg} />
                    </button>
                </div>
                <div className={style.fullModalContent}>
                    <div className={style.modalVertGrid}>
                        <div className={style.confirmText}>{text}</div>
                    </div>

                    <div className={style.buttons}>
                        <SaveButton onClick={() => giveAnswer(true)}>
                            {options?.trueButtonText || 'Да'}
                        </SaveButton>
                        <WhiteButton onClick={() => giveAnswer(false)}>
                            {options?.falseButtonText || 'Нет'}
                        </WhiteButton>
                    </div>
                </div>
            </div>
        </>
    );
};

export default ConfirmDialogWindow;
