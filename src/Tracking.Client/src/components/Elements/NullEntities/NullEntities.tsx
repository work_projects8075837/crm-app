import React from 'react';

interface NullEntitiesProps {
    text?: string;
}

const NullEntities: React.FC<NullEntitiesProps> = ({ text = 'Нет заявок' }) => {
    return (
        <>
            <div className={'task-nav--container'}>
                <div className={'none--task--container'}>
                    <img src='/none--task.svg' alt='' />
                    <div className={'none-task--text'}>{text}</div>
                </div>
            </div>
        </>
    );
};

export default NullEntities;
