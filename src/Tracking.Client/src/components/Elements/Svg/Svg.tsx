import React, { CSSProperties } from 'react';

interface SvgProps {
    symbol: string;
    className?: string;
    style?: CSSProperties;
}

const Svg: React.FC<SvgProps> = ({ symbol, className = '', ...svgProps }) => {
    return (
        <>
            <svg {...svgProps} className={className}>
                <use href={`/sprite.svg#${symbol}`}></use>
            </svg>
        </>
    );
};

export default Svg;
