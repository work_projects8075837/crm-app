import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/layout/table.module.scss';
import PaginationOffsetButton from './PaginationOffsetButton';

interface PaginationProps {
    paginationHandler: PaginationHandler;
    pages: React.ReactNode;
}

const Pagination: React.FC<PaginationProps> = ({
    paginationHandler,
    pages,
}) => {
    return (
        <>
            <div className={style.tablePagination}>
                <div className={style.tablePaginationOffset}>
                    <span>Показать:</span>
                    <div className={style.offsetButtons}>
                        <PaginationOffsetButton
                            paginationHandler={paginationHandler}
                            offset={20}
                        />
                        <span>/</span>

                        <PaginationOffsetButton
                            paginationHandler={paginationHandler}
                            offset={50}
                        />
                        <span>/</span>

                        <PaginationOffsetButton
                            paginationHandler={paginationHandler}
                            offset={100}
                        />
                    </div>
                </div>

                <div className={style.tablePaginationPages}>{pages}</div>
            </div>
        </>
    );
};

export default observer(Pagination);
