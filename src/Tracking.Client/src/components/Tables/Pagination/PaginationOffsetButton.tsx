import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import style from '../../../style/layout/table.module.scss';

import { observer } from 'mobx-react-lite';
import React from 'react';

interface PaginationOffsetButtonProps {
    offset: number;
    paginationHandler: PaginationHandler;
}

const PaginationOffsetButton: React.FC<PaginationOffsetButtonProps> = ({
    offset,
    paginationHandler,
}) => {
    return (
        <>
            <button
                className={
                    offset === paginationHandler.offset
                        ? style.offsetButtonCurrent
                        : style.offsetButton
                }
                onClick={() => {
                    paginationHandler.setOffset(offset);
                }}
            >
                по {offset}
            </button>
        </>
    );
};

export default observer(PaginationOffsetButton);
