import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import React from 'react';
import style from '../../../style/layout/table.module.scss';
import { observer } from 'mobx-react-lite';

interface PageButtonProps {
    pagonationHandler: PaginationHandler;
    page: number;
}

const PageButton: React.FC<PageButtonProps> = ({ pagonationHandler, page }) => {
    return (
        <>
            <button
                className={
                    pagonationHandler.page === page
                        ? style.tablePaginationPageCurrent
                        : style.tablePaginationPage
                }
                onClick={() => {
                    pagonationHandler.setPage(page);
                }}
            >
                {page}
            </button>
        </>
    );
};

export default observer(PageButton);
