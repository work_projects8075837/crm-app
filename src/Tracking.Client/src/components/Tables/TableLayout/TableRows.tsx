import { StyleTable } from '@/style/layout/layout';
import classNames from 'classnames';
import React, { HTMLAttributes } from 'react';

export type TableRowsProps = HTMLAttributes<HTMLInputElement>;

const TableRows: React.FC<TableRowsProps> = ({
    className,
    children,
    ...divProps
}) => {
    return (
        <div
            {...divProps}
            className={classNames(StyleTable.rowsWrapper, className)}
        >
            {children}
        </div>
    );
};

export default TableRows;
