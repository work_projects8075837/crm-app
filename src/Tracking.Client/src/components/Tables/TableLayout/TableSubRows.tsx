import { StyleTable } from '@/style/layout/layout';
import classNames from 'classnames';
import React from 'react';
import { TableRowsProps } from './TableRows';

const TableSubRows: React.FC<TableRowsProps> = ({
    className,
    children,
    ...divProps
}) => {
    return (
        <div
            {...divProps}
            className={classNames(StyleTable.subRows, className)}
        >
            {children}
        </div>
    );
};

export default TableSubRows;
