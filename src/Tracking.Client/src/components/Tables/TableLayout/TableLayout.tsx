import Svg from '@/components/Elements/Svg/Svg';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';
import style from '../../../style/layout/table.module.scss';
import TableRows from './TableRows';

interface TableLayoutProps {
    header: React.ReactNode;
    pagination: React.ReactNode;
    children: React.ReactNode;
    extraChild?: React.ReactNode;
    layoutClass: string;
    settingsOpenHandler?: OpenHandler;
}

const TableLayout: React.FC<TableLayoutProps> = ({
    pagination,
    header,
    extraChild,
    children,
    layoutClass = '',
    settingsOpenHandler,
}) => {
    return (
        <>
            <div className={`${style.tableLayout} ${layoutClass}`}>
                {settingsOpenHandler && (
                    <button
                        className={style.settingsButton}
                        onClick={() => {
                            settingsOpenHandler.open();
                        }}
                    >
                        <Svg symbol='white_settings' />
                    </button>
                )}

                {extraChild}
                <div className={style.tableContent}>
                    {header}
                    <TableRows>{children}</TableRows>
                </div>
                {pagination}
            </div>
        </>
    );
};

export default TableLayout;
