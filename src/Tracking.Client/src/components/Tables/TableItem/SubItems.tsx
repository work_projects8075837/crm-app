import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import { StyleLoader } from '@/style/elements/elements';
import { useQuery } from '@tanstack/react-query';
import TableStepItem, {
    IStepEntity,
    TableStepItemProps,
} from './TableStepItem';

const SubItems = <T extends IStepEntity, F>({
    parentFilterKey,
    parentKey,
    getAll,
    entity,
    paddingLeft,
    mainQueryKey,
    step,
    ...tableItemProps
}: Omit<TableStepItemProps<T, F>, 'step'> & {
    paddingLeft: number;
    step: number;
}) => {
    const { data } = useQuery({
        queryKey: [mainQueryKey, { [parentFilterKey]: entity.id }],
        queryFn: async () => {
            return await getAll({
                [parentFilterKey]: entity.id,
            } as F).then((res) => res.data.data);
        },
    });

    return (
        <>
            {data ? (
                data.map((ent) => (
                    <TableStepItem
                        {...{
                            ...tableItemProps,
                            mainQueryKey,
                            parentFilterKey,
                            getAll,
                            parentKey,
                        }}
                        entity={ent}
                        isParent={ent.is_parent}
                        paddingLeft={paddingLeft + 12}
                        step={step + 1}
                        key={ent.id}
                    />
                ))
            ) : (
                <LoaderCircle
                    size={60}
                    className={StyleLoader.directoryLoading}
                />
            )}
        </>
    );
};

export default SubItems;
