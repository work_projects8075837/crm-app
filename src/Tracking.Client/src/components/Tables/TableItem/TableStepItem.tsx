import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import { useBooleanState } from '@/hooks/useBooleanState';
import { PaginatedData } from '@/services/types';
import { FieldsHandler } from '@/store/states/FieldsHandler';
import { AxiosResponse } from 'axios';
import SubItems from './SubItems';
import TableItem, { TableItemProps } from './TableItem';

export interface IStepEntity extends Entity {
    is_parent: boolean;
}

export interface TableStepItemProps<TEntity extends IStepEntity, TFilter>
    extends TableItemProps<TEntity> {
    parentKey: keyof TEntity;
    parentFilterKey: keyof TFilter;
    getAll: (
        query?: TFilter,
    ) => Promise<AxiosResponse<PaginatedData<TEntity[]>>>;
    paddingLeft?: number;
    mainQueryKey: string;
    step?: number;
    fieldsHandler: FieldsHandler<TEntity>;
}

type TableStepItemT = <T extends IStepEntity, F>(
    props: TableStepItemProps<T, F>,
) => React.ReactNode;

const TableStepItem: TableStepItemT = ({
    paddingLeft = 0,
    step = 0,
    fieldsHandler,
    parentFilterKey,
    parentKey,
    getAll,
    mainQueryKey,
    entity,
    ...tableItemProps
}) => {
    const [isOpen, handler] = useBooleanState();

    return (
        <>
            <TableItem
                {...{
                    ...tableItemProps,
                    entity,
                    isOpen,
                    paddingLeft,
                    fieldsHandler,
                }}
                onClick={
                    entity.is_parent
                        ? () => {
                              fieldsHandler.setStep(step + 1);
                              handler.toggle();
                          }
                        : tableItemProps.onClick
                }
            />
            {isOpen && entity.is_parent && (
                <SubItems
                    {...{
                        ...tableItemProps,
                        parentFilterKey,
                        parentKey,
                        getAll,
                        mainQueryKey,
                        entity,
                        fieldsHandler,
                        step,
                    }}
                    paddingLeft={paddingLeft}
                />
            )}
        </>
    );
};

export default TableStepItem;
