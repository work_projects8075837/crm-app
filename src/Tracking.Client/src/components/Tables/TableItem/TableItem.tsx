import Dash from '@/components/Elements/Dash/Dash';
import ArrowOpen from '@/components/Elements/FormSelect/ArrowOpen';
import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { FieldsHandler } from '@/store/states/FieldsHandler';
import { StyleTable } from '@/style/layout/layout';
import classNames from 'classnames';
import { observer } from 'mobx-react-lite';
import { HTMLAttributes } from 'react';
import { IStepEntity } from './TableStepItem';

export interface TableItemProps<T>
    extends Omit<HTMLAttributes<HTMLDivElement>, 'onDoubleClick'> {
    entity: T;
    fieldClass?: string;
    checkHandler: CheckEntitiesHandler;
    isParent?: boolean;
    onDoubleClick?: (entity: T) => void;
    isOpen?: boolean;
    paddingLeft?: number;
    fieldsHandler: FieldsHandler<T>;
}

const TableItem = <T extends IStepEntity>({
    entity,
    className,
    checkHandler,
    isParent = false,
    onDoubleClick,
    isOpen = false,
    paddingLeft,
    fieldsHandler,
    ...divProps
}: TableItemProps<T>) => {
    return (
        <div
            onDoubleClick={() => onDoubleClick?.(entity)}
            className={classNames(StyleTable.tableItem, className)}
            {...divProps}
            style={{
                ...divProps.style,
                ...fieldsHandler.getGridTemplateStyle(),
            }}
        >
            <div style={{ paddingLeft }} className={StyleTable.tableItemName}>
                <CheckItem {...{ checkHandler, entity }} />
                {isParent && (
                    <ArrowOpen
                        {...{
                            isOpen,
                            className: StyleTable.tableItemArrow,
                        }}
                    />
                )}
            </div>

            {fieldsHandler.getSelectedFields().map((field, i) => {
                const value = field.getValue(entity);

                return (
                    <div
                        key={field.key}
                        className={classNames(StyleTable.tableItemName)}
                    >
                        {value ? (
                            <>
                                {typeof value !== 'object' ? (
                                    <>
                                        <span>{value}</span>
                                    </>
                                ) : (
                                    <>
                                        <div
                                            className={
                                                StyleTable.tableItemValue
                                            }
                                        >
                                            {value}
                                        </div>
                                    </>
                                )}
                                {/* {isParent && i === 0 && (
                                    <ArrowOpen
                                        {...{
                                            isOpen,
                                            className:
                                                StyleTable.tableItemArrow,
                                        }}
                                    />
                                )} */}
                            </>
                        ) : (
                            <Dash />
                        )}
                    </div>
                );
            })}
        </div>
    );
};

export default observer(TableItem);
