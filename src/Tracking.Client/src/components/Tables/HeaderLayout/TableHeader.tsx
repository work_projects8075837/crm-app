import CheckAll from '@/components/Fields/CheckItem/CheckAll';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { StyleTable } from '@/style/layout/layout';
import React from 'react';

interface TableHeaderProps {
    fields: { name: string }[];
    layoutClass: string;
    fieldClass?: string;
    checkHandler?: CheckEntitiesHandler;
    children?: React.ReactNode;
    withCheck?: boolean;
    style?: React.CSSProperties;
}

const TableHeader: React.FC<TableHeaderProps> = ({
    checkHandler,
    layoutClass,
    fields,
    fieldClass,
    children,
    style,
    withCheck = true,
}) => {
    return (
        <>
            <div
                className={`${StyleTable.tableHeader} ${layoutClass}`}
                style={style}
            >
                {withCheck && (
                    <div className={StyleTable.tableItemField}>
                        <CheckAll checkHandler={checkHandler} />
                    </div>
                )}
                {fields.map(({ name }, index) => {
                    return (
                        <label
                            key={index}
                            className={`${StyleTable.tableHeaderField} ${fieldClass}`}
                        >
                            <span>{name}</span>
                        </label>
                    );
                })}
                {children}
            </div>
        </>
    );
};

export default TableHeader;
