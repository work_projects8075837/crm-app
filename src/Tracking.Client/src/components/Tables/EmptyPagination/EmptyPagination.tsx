import React from 'react';
import style from '../../../style/layout/table.module.scss';

const EmptyPagination: React.FC = () => {
    return (
        <>
            <div className={style.tablePagination}></div>
        </>
    );
};

export default EmptyPagination;
