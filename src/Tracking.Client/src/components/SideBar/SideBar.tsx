import UserNavBar from './components/UserNavBar';
import NavBarService from './components/NavBarService';
import styles from '../../style/layout/layout.module.scss';
import React from 'react';
import { NavLink } from 'react-router-dom';
import UserMenuLink from './Links/UserMenuLink/UserMenuLink';

const SideBar = () => (
    <div className={styles.sidebar}>
        <div className={styles.sidebarLogo}>
            <NavLink to='/' className='logo'>
                <img src='/logo.svg' alt='Флагман' />
            </NavLink>
        </div>

        <div className={styles.sidebarChildren}>
            <NavBarService />
            <UserNavBar />
        </div>

        <div className={styles.sidebarUser}>
            <UserMenuLink />
        </div>
    </div>
);

export default React.memo(SideBar);
