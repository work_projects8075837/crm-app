import { useAuthQuery } from '@/components/Providers/AuthGuard/useAuthQuery';
import SideMenuLayout from '@/components/SideMenu/SideMenuLayout';
import { useMenuCasesList } from '@/components/Tickets/CaseMenu/useMenuCasesList';
import { useUserReadStatusesList } from '@/store/queries/stores/read_status/useUserReadStatusesList';
import { TabHandler } from '@/store/states/TabHandler.ts';
import { MenuHandlerState } from '@/store/states/menuHandler.ts';
import styles from '@/style/layout/side_menu.module.scss';
import { observer } from 'mobx-react-lite';
import React from 'react';
import NotificationsList from '../Notification/NotificationsList';
import Tasks, { viewCaseOpen } from '../Task/Tasks';
import MenuTabLayout from './MenuTabLayout';
import { useConfirmNotifications } from './useConfirmNotifications';

export const notificationsMenu = new MenuHandlerState<HTMLDivElement>();
const notificationsTabs = new TabHandler('notifications');

const NotificationsMenu: React.FC = () => {
    const unreadStatuses = useUserReadStatusesList({
        read_status_is_read: false,
    });
    const currentUserQuery = useAuthQuery();
    const casesQ = useMenuCasesList(
        {
            manager_user_id: currentUserQuery.data?.userId,
        },
        { refetchOnMount: true },
    );
    const { mutate } = useConfirmNotifications();
    return (
        <>
            <SideMenuLayout
                className={styles.wideLayout}
                ref={notificationsMenu.ref}
                close={() => {
                    notificationsMenu.close();
                    viewCaseOpen.close();
                }}
                isOpen={notificationsMenu.isOpen}
                header={
                    <>
                        <div className={styles.tabs}>
                            <MenuTabLayout
                                onClick={mutate}
                                className={styles.firstTab}
                                title='Уведомления'
                                svgSymbol='notification'
                                tabName='notifications'
                                tabHandler={notificationsTabs}
                                count={unreadStatuses.data?.data.length}
                            />
                            <MenuTabLayout
                                title='Дела'
                                svgSymbol='task'
                                tabName='tasks'
                                tabHandler={notificationsTabs}
                                count={
                                    casesQ.data?.data.filter(
                                        (c) => !c.is_completed,
                                    ).length
                                }
                            />
                        </div>
                    </>
                }
            >
                <div className={styles.menuContentScrollable}>
                    {notificationsTabs.tab === 'notifications' ? (
                        <NotificationsList />
                    ) : (
                        <Tasks />
                    )}
                </div>
            </SideMenuLayout>
        </>
    );
};

export default observer(NotificationsMenu);
