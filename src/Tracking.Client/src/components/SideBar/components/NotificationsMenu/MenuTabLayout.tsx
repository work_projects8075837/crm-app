import Svg from '@/components/Elements/Svg/Svg';
import { performExistCallback } from '@/components/utils/performExistCallback';
import { TabHandler } from '@/store/states/TabHandler.ts';
import styles from '@/style/layout/side_menu.module.scss';
import { observer } from 'mobx-react-lite';
import React from 'react';

interface MenuTabLayoutProps {
    title: string;
    svgSymbol: string;
    tabName: string;
    tabHandler: TabHandler;
    className?: string;
    count?: React.ReactNode;
    onClick?: () => void | Promise<void>;
}

const MenuTabLayout: React.FC<MenuTabLayoutProps> = ({
    title,
    svgSymbol,
    tabName,
    tabHandler,
    className = '',
    count,
    onClick,
}) => {
    return (
        <>
            <button
                onClick={() => {
                    performExistCallback(onClick);
                    tabHandler.setTab(tabName);
                }}
                className={`${styles.tab} ${className} ${
                    tabName === tabHandler.tab ? styles.tabBottom : ''
                }`}
            >
                <Svg symbol={svgSymbol} className={styles.tabSvg} />
                <span>{title}</span>
                {!!count && <span className={styles.counter}>{count}</span>}
            </button>
        </>
    );
};

export default observer(MenuTabLayout);
