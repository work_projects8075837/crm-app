import { readStatusApi } from '@/services/read_status/read_status';
import { invalidateKey } from '@/store/queries/actions/base';
import { getUserId } from '@/store/queries/actions/user';
import {
    GET_ALL_NOTIFICATIONS_KEY,
    GET_ALL_READ_STATUSES_KEY,
} from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';

export const useConfirmNotifications = () => {
    return useMutation({
        mutationFn: async () => {
            const userId = getUserId();

            const updatableReadStatuses = await readStatusApi
                .getAll({
                    read_status_user_id: userId,
                    read_status_is_read: false,
                })
                .then((res) => res.data.data.filter((rs) => !rs.read));

            return await readStatusApi.confirm(
                updatableReadStatuses.map((rs) => rs.id),
            );
        },
        onSuccess: () => {
            invalidateKey(GET_ALL_NOTIFICATIONS_KEY);
            invalidateKey(GET_ALL_READ_STATUSES_KEY);
        },
    });
};
