import { caseApi, caseHooks } from '@/services/case/case';
import { CaseFilter } from '@/services/case/types';
import { invalidateKey } from '@/store/queries/actions/base';
import { updateCases } from '@/store/queries/actions/case';
import {
    GET_ALL_CASES_KEY,
    GET_ONE_CASE_BY_ID_KEY,
} from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

interface CompleteTaskMutate {
    taskId: string;
    is_completed: boolean;
}

export const useCompleteTask = (filter: CaseFilter) => {
    const { ticketId } = useParams();
    const casesQuery = caseHooks.useList(filter);

    const m = useMutation({
        mutationFn: async ({ taskId, is_completed }: CompleteTaskMutate) => {
            if (casesQuery.data && ticketId) {
                const newCases = casesQuery.data?.data.map((c) => {
                    if (c.id === taskId) {
                        return { ...c, is_completed };
                    }
                    return c;
                });

                updateCases(newCases, filter);
            }

            return await caseApi.update(taskId, { is_completed });
        },
        onSuccess: () => {
            invalidateKey(GET_ALL_CASES_KEY);
            invalidateKey(GET_ONE_CASE_BY_ID_KEY);
        },
    });
    return m;
};
