import GreenCheckbox from '@/components/Elements/GreenCheckbox/GreenCheckbox';
import Svg from '@/components/Elements/Svg/Svg';
import { caseId } from '@/components/Tickets/CaseDetail/CaseDetail';
import { formatTimeString } from '@/components/utils/formatTimeString';
import { CaseFilter, ICase } from '@/services/case/types';
import { OpenHandler } from '@/store/states/OpenHandler';
import styles from '@/style/notifications/message.module.scss';
import React from 'react';
import { useCompleteTask } from './useCompleteTask';

interface TaskProps extends ICase {
    openHandler: OpenHandler;
    filter: CaseFilter;
}

const Task: React.FC<TaskProps> = ({
    id,
    name,
    manager,
    is_completed,
    finish_date,
    finish_time,
    openHandler,
    filter,
}) => {
    const { mutate, isLoading } = useCompleteTask(filter);
    return (
        <>
            <div className={styles.notificationWrapper}>
                <div
                    className={styles.task}
                    onClick={() => {
                        caseId.setValue(id);
                        openHandler.open();
                    }}
                >
                    <div className={styles.taskContent}>{name}</div>
                    <div className={styles.taskAuthor}>
                        <Svg symbol='micro_user' className={styles.authorSvg} />
                        <span>{manager?.user.name}</span>
                    </div>
                </div>
                <div className={styles.afterMessage}>
                    <span className={styles.timeText}>
                        {formatTimeString(finish_time)}
                    </span>
                    <GreenCheckbox
                        onClick={() => {
                            mutate({ taskId: id, is_completed: !is_completed });
                        }}
                        disabled={isLoading}
                        isChecked={is_completed}
                    />
                </div>
            </div>
        </>
    );
};

export default Task;
