import Loader from '@/Loader/Loader';
import SwitcherButton from '@/components/Elements/Switcher/SwitcherButton';
import TextLoader from '@/components/Elements/TextLoader/TextLoader';
import MenuSubHeaderLayout from '@/components/SideMenu/MenuSubHeaderLayout';
import CaseDetail, { caseId } from '@/components/Tickets/CaseDetail/CaseDetail';
import { useMenuCasesList } from '@/components/Tickets/CaseMenu/useMenuCasesList';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { OpenHandler } from '@/store/states/OpenHandler';
import { observer } from 'mobx-react-lite';
import React, { Fragment } from 'react';
import style from '../../../../style/notifications/message.module.scss';
import Task from './Task';

export const viewCaseOpen = new OpenHandler(false);

const Tasks: React.FC = () => {
    const currentUserQuery = useCurrentUser();
    const { isHideCompleted, handler, datesCases, isLoading } =
        useMenuCasesList({
            manager_user_id: currentUserQuery.data?.id,
        });
    return (
        <>
            {viewCaseOpen.isOpen && caseId.value ? (
                <CaseDetail openHandler={viewCaseOpen} />
            ) : (
                <>
                    <MenuSubHeaderLayout>
                        <div></div>

                        <SwitcherButton
                            text='Скрыть выполненые'
                            defaultChecked={isHideCompleted}
                            onChange={() => handler.toggle()}
                        />
                    </MenuSubHeaderLayout>
                    <div className={style.tasksTrack}>
                        {isLoading ? (
                            <Loader size={100} />
                        ) : (
                            <>
                                {datesCases?.length ? (
                                    datesCases?.map(([date, casesByDate]) => {
                                        const cases = casesByDate?.filter(
                                            (c) =>
                                                c.manager?.user.id ===
                                                currentUserQuery.data?.id,
                                        );
                                        return (
                                            <Fragment key={date}>
                                                {!!cases?.length && (
                                                    <div
                                                        className={
                                                            style.dayTime
                                                        }
                                                    >
                                                        {date}
                                                    </div>
                                                )}

                                                {cases?.map((c) => (
                                                    <Task
                                                        filter={{
                                                            manager_user_id:
                                                                currentUserQuery
                                                                    .data?.id,
                                                        }}
                                                        openHandler={
                                                            viewCaseOpen
                                                        }
                                                        key={c.id}
                                                        {...c}
                                                    />
                                                ))}
                                            </Fragment>
                                        );
                                    })
                                ) : (
                                    <TextLoader
                                        text='Нет задач'
                                        className='mt15'
                                    />
                                )}
                            </>
                        )}
                    </div>
                </>
            )}
        </>
    );
};

export default observer(Tasks);
