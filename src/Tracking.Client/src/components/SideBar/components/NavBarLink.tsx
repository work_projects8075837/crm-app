import { FC } from 'react';

export interface NavBarLinkProps {
    onClick: () => void;
    path: string;
}
const NavBarLink: FC<NavBarLinkProps> = ({ onClick, path }) => {
    return (
        <nav className={'navbar__link logo--activate br'}>
            <img src={path} alt='Notification' onClick={onClick} />
        </nav>
    );
};

export default NavBarLink;
