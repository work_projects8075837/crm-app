import { adminSidebarOpen } from '@/components/NavbarLayout/Layout';
import AdminGuard from '@/components/Providers/RolesGuard/AdminGuard';
import { useUserReadStatusesList } from '@/store/queries/stores/read_status/useUserReadStatusesList';
import { observer } from 'mobx-react-lite';
import React, { useCallback } from 'react';
import NavbarButton from '../Links/NavbarButton/NavbarButton';
import NavbarLink from '../Links/NavbarLink/NavbarLink';
import NotificationsMenu, {
    notificationsMenu,
} from './NotificationsMenu/NotificationsMenu';
import { useConfirmNotifications } from './NotificationsMenu/useConfirmNotifications';
import UserMenu from './UserMenu/UserMenu';

const UserNavBar: React.FC = () => {
    const openNotificationsMenu = useCallback(
        () => notificationsMenu.open(),
        [],
    );

    const closeAdminSideBar = () => adminSidebarOpen.close();

    const unreadStatuses = useUserReadStatusesList(
        { read_status_is_read: false },
        { refetchOnMount: true },
    );

    const { mutate } = useConfirmNotifications();

    return (
        <div className='navbar--container'>
            <AdminGuard>
                <NavbarButton
                    alt='Админ-панель'
                    isSelected={adminSidebarOpen.isOpen}
                    onClick={() => adminSidebarOpen.toggle()}
                    src='/user--admin.svg'
                />
            </AdminGuard>

            <NavbarButton
                alt='Уведомления'
                onClick={() => {
                    mutate();
                    openNotificationsMenu();
                }}
                src={
                    unreadStatuses.data?.data.length
                        ? '/notification_unread.svg'
                        : '/notification.svg'
                }
            />
            <NavbarLink
                onClick={closeAdminSideBar}
                to='/settings/'
                src='/settings--profile.svg'
                alt='Профиль'
            />

            <UserMenu />
            <NotificationsMenu />

            {/* <NavBarContainer
                blur={blurNavBarProfile}
                navBar={profileNavBar}
                handleClick={() => handleToggle('profile')}
                handleClickTask={() => handleToggle('task')}
                handleClickMessage={() => handleToggle('message')}
                defaultHeader={true}
            >
                <ProfileInfo />
                <ProfileStatus />
                <ProfileSettingNav />
            </NavBarContainer>
            <NavBarContainer
                blur={blurNavBarNotification}
                navBar={notificationNavBar}
                handleClick={() => handleToggle('notification')}
                handleClickTask={() => handleToggle('task')}
                handleClickMessage={() => handleToggle('message')}
                defaultHeader={false}
            >
                <Messages isClicked={isClicked} />
                <NotificationAppMiddle isClicked={isClicked} />
            </NavBarContainer> */}
        </div>
    );
};

export default observer(UserNavBar);
