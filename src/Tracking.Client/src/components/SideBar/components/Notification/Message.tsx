import {FC} from 'react';
import clsx from "clsx";

interface MessageProps {
    read: boolean
}

const Message: FC<MessageProps> = ({read}) => (
    <div className="message">
        <div className="message--date">04.08.23</div>
        <div className="message--df">
            <div className={clsx("message--block", read && "message--block--read")}>
                <div className="message--block--text">Добавлен комментарий к заявке 43С</div>
            </div>
            <div className="message--block--time">17:25</div>
        </div>
    </div>
);

export default Message;