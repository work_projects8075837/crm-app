import styles from '@/style/notifications/message.module.scss';
import React from 'react';
import { Link } from 'react-router-dom';

interface NotificationProps {
    text: React.ReactNode;
    time: string;
    link_id?: string;
    model: string;
}

const Notification: React.ForwardRefRenderFunction<
    HTMLAnchorElement,
    NotificationProps
> = ({ text, time, model, link_id }, ref) => {
    return (
        <Link
            to={`/${model}/${model === 'ticket' ? '' : 'single/'}${link_id}`}
            className={styles.notificationWrapper}
            ref={ref}
            target='_blank'
        >
            <div className={styles.notification}>{text}</div>
            <div className={styles.messageTime}>{time}</div>
        </Link>
    );
};

export default React.forwardRef(Notification);
