import Message from "./Message";
import {FC} from "react";

export interface MessagesProps {
    isClicked: boolean
}
const Messages: FC<MessagesProps> = ({isClicked}) => {
    return (
        <div style={{display: isClicked ? 'none' : 'block'}} className="container--message">
            {Array(10).fill(null).map((_, index) => (
                <Message key={index} read={index > 0}/>
            ))}
        </div>
    );
};

export default Messages;