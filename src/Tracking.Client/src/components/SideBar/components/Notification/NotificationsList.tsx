import Loader from '@/Loader/Loader';
import TextLoader from '@/components/Elements/TextLoader/TextLoader';
import { sortItemsByDate } from '@/components/Tickets/Comments/sortItemsByDate';
import { useUserNotifications } from '@/store/queries/stores/notification/useUserNotifications';
import React, { Fragment, useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import style from '../../../../style/notifications/message.module.scss';
import Notification from './Notification';

const NotificationsList: React.FC = () => {
    const { isLoading, fetchNextPage, items } = useUserNotifications({
        refetchOnMount: true,
    });

    const datesNotes =
        items &&
        sortItemsByDate(
            items.filter((i) => !!i.created_at),
            'created_at',
        );

    const { ref, inView } = useInView();

    useEffect(() => {
        if (inView) {
            fetchNextPage();
        }
    }, [inView]);

    return (
        <>
            <div className={style.tasksTrack}>
                {isLoading ? (
                    <Loader size={100} />
                ) : (
                    <>
                        {datesNotes?.length ? (
                            datesNotes?.map(([date, notes], dateIndex) => {
                                return (
                                    <Fragment key={date}>
                                        {!!notes?.length && (
                                            <div className={style.dayTime}>
                                                {date}
                                            </div>
                                        )}

                                        {notes?.map((note, noteIndex) => (
                                            <Notification
                                                {...note}
                                                ref={
                                                    dateIndex ===
                                                        datesNotes.length - 1 &&
                                                    noteIndex ===
                                                        notes.length - 2
                                                        ? ref
                                                        : undefined
                                                }
                                                text={note.name}
                                                time={note.time}
                                                key={note.id}
                                            />
                                        ))}
                                    </Fragment>
                                );
                            })
                        ) : (
                            <TextLoader
                                text='Нет уведомлений'
                                className='mt15'
                            />
                        )}
                    </>
                )}
            </div>
            {/* {datesCases.map(([date, notes]) => {
                return <Fragment key={date}></Fragment>;
            })} */}
        </>
    );
};

export default React.memo(NotificationsList);
