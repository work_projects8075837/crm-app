import SelectDirection from '@/components/Fields/SelectDirection/SelectDirection';
import { useBaseForm } from '@/components/Form/hooks/useBaseForm';
import { authService } from '@/services/auth/auth';
import { IDirection } from '@/services/direction/types';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_CURRENT_USER_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import { FormProvider } from 'react-hook-form';

interface UserDirectionProps {
    direction: IDirection | null;
}

const UserDirection: React.FC<UserDirectionProps> = ({ direction }) => {
    const { form } = useBaseForm({ defaultValues: { direction } });

    return (
        <>
            <div className={'mt15'}>
                <FormProvider {...form}>
                    <SelectDirection
                        placeholder='Выбрать направление'
                        withLabel={false}
                        onChangeAction={(direction) => {
                            authService
                                .update({ direction_id: direction.id })
                                .then(() => {
                                    invalidateKey(GET_CURRENT_USER_KEY);
                                });
                        }}
                        onCloseClick={() => {
                            authService
                                .update({ direction_id: null })
                                .then(() => {
                                    invalidateKey(GET_CURRENT_USER_KEY);
                                });
                        }}
                    />
                </FormProvider>
            </div>
        </>
    );
};

export default UserDirection;
