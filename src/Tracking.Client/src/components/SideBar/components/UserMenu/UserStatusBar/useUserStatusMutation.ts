import { authService } from '@/services/auth/auth';
import { ISetUserStatus } from '@/services/auth/types.ts';

import { updateUser } from '@/store/queries/actions/user';
import { queryClient } from '@/store/queries/client/client';
import { GET_CURRENT_USER_KEY } from '@/store/queries/keys/keys';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { useMutation } from '@tanstack/react-query';
import { IUser } from './../../../../../services/user/types';

export const useUserStatusMutation = () => {
    const { data } = useCurrentUser();
    const mutation = useMutation({
        mutationFn: async ({ status }: ISetUserStatus) => {
            return await authService.setUserStatus({ status });
        },
        onMutate: ({ status }: ISetUserStatus) => {
            const currentUser = queryClient.getQueryData<IUser>([
                GET_CURRENT_USER_KEY,
                { userId: data?.id },
            ]);

            const lastStatus = currentUser?.status;

            updateUser({ status }, data?.id);
            return { lastStatus };
        },
        onSuccess: (userData) => {
            return updateUser(userData.data, data?.id);
        },
        onError: (_, __, context) => {
            if (context?.lastStatus)
                updateUser({ status: context?.lastStatus }, data?.id);
        },
    });

    return mutation;
};
