import styles from '@/style/user/user_menu.module.scss';
import React from 'react';
import { useUserStatusMutation } from './useUserStatusMutation';

interface UserStatusProps {
    color: string;
    text: string;
    value: 'free' | 'work' | 'rest' | 'out';
    isChecked: boolean;
}

const UserStatus: React.FC<UserStatusProps> = ({
    color,
    text,
    value,
    isChecked,
}) => {
    const statusMutation = useUserStatusMutation();

    const onStatusChange = () => {
        statusMutation.mutate({ status: value });
    };

    return (
        <button
            onClick={onStatusChange}
            className={styles.statusButton}
            style={{ backgroundColor: isChecked ? '#fff' : 'transparent' }}
        >
            <div
                className={styles.statusDot}
                style={{ backgroundColor: color }}
            ></div>
            <span>{text}</span>
        </button>
    );
};

export default React.memo(UserStatus);
