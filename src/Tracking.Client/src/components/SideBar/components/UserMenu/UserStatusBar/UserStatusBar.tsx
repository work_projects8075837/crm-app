import styles from '@/style/user/user_menu.module.scss';
import React from 'react';
import { useCurrentUser } from '../../../../../store/queries/stores/user/useCurrentUser';
import UserStatus from './UserStatus';

interface UserStatusAttributes {
    color: string;
    text: string;
    value: 'free' | 'work' | 'rest' | 'out';
}

const statuses: UserStatusAttributes[] = [
    { color: '#00FF57', text: 'Свободен', value: 'free' },
    { color: '#FFE978', text: 'Занят', value: 'work' },
    { color: '#FF8A00', text: 'Перерыв', value: 'rest' },
    { color: '#FF1F00', text: 'Нет на месте', value: 'out' },
];

const UserStatusBar: React.FC = () => {
    const { data } = useCurrentUser();

    return (
        <>
            <div className={styles.statusBar}>
                {statuses.map((status) => (
                    <UserStatus
                        key={status.color}
                        {...status}
                        isChecked={data?.status === status.value}
                    />
                ))}
            </div>
        </>
    );
};

export default React.memo(UserStatusBar);
