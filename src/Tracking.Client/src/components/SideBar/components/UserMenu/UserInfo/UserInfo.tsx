import React from 'react';
import { useCurrentUser } from '../../../../../store/queries/stores/user/useCurrentUser';
import { observer } from 'mobx-react-lite';
import UserAvatar from './UserAvatar';

const UserInfo: React.FC = () => {
    const { data } = useCurrentUser();
    return (
        <>
            <div className={'profile--info'}>
                <UserAvatar />
                <div className={'profile--text'}>
                    <div className={'profile--text--name'}>{data?.name}</div>
                    <div className={'profile--text--email'}>{data?.email}</div>
                </div>
            </div>
        </>
    );
};

export default observer(UserInfo);
