import React from 'react';
import { useCurrentUser } from '../../../../../store/queries/stores/user/useCurrentUser';
import UserPlaceholder from './UserPlaceholder';

interface UserAvatarProps {
    className?: string;
}

const UserAvatar: React.FC<UserAvatarProps> = ({ className = '' }) => {
    const { data } = useCurrentUser();

    return (
        <>
            {data?.file ? (
                <img src={data.file} alt={data.name} />
            ) : (
                <UserPlaceholder name={data?.name} className={className} />
            )}
        </>
    );
};

export default React.memo(UserAvatar);
