import styles from '@/style/user/user_menu.module.scss';
import React from 'react';

interface UserPlaceholderProps {
    name?: string;
    className?: string;
}

const UserPlaceholder: React.FC<UserPlaceholderProps> = ({
    name,
    className = '',
}) => {
    return (
        <>
            <div className={`${styles.avatarPlaceholder} ${className}`}>
                <span>
                    {name
                        ?.split(' ')
                        .splice(0, 2)
                        .map((word) => word[0])
                        .join('')}
                </span>
            </div>
        </>
    );
};

export default React.memo(UserPlaceholder);
