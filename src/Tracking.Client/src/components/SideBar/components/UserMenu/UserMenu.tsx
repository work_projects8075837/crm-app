import Svg from '@/components/Elements/Svg/Svg';
import SideMenuLayout from '@/components/SideMenu/SideMenuLayout';
import { serviceManager } from '@/services/utils/instance/ServiceManager';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { MenuHandlerState } from '@/store/states/menuHandler.ts';
import styles from '@/style/layout/side_menu.module.scss';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Link } from 'react-router-dom';
import UserChannel from './UserChannel/UserChannel';
import UserDirection from './UserDirection/UserDirection';
import UserInfo from './UserInfo/UserInfo';
import UserStatusBar from './UserStatusBar/UserStatusBar';

export const userMenu = new MenuHandlerState<HTMLDivElement>();

const UserMenu: React.FC = () => {
    const { data } = useCurrentUser();
    return (
        <>
            <SideMenuLayout
                ref={userMenu.ref}
                close={() => userMenu.close()}
                isOpen={userMenu.isOpen}
                header={
                    <>
                        <h3 className={styles.headerTitle}>
                            Профиль пользователя
                        </h3>
                    </>
                }
            >
                <div className={styles.betweenWrapper}>
                    <div className={styles.sideMenuContainerMarginTop}>
                        <UserInfo />
                        <UserStatusBar />

                        {data && <UserChannel channel={data.channel} />}
                        {data && <UserDirection direction={data.direction} />}
                    </div>
                    <div className={styles.sideMenuBottom}>
                        <div className={styles.sideMenuContainerRowBetween}>
                            <Link className={styles.link} to={'/settings/'}>
                                <Svg
                                    className={styles.linkSvg}
                                    symbol='settings'
                                />
                                <span>Настройки профиля</span>
                            </Link>

                            <button
                                className={styles.link}
                                onClick={() => serviceManager.unauthorize()}
                            >
                                <Svg className={styles.linkSvg} symbol='exit' />
                                <span>Выход</span>
                            </button>
                        </div>
                    </div>
                </div>
            </SideMenuLayout>
        </>
    );
};

export default observer(UserMenu);
