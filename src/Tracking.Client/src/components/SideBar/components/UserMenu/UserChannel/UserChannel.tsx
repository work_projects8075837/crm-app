import SelectChannel from '@/components/Fields/SelectChannel/SelectChannel';
import { useBaseForm } from '@/components/Form/hooks/useBaseForm';
import { authService } from '@/services/auth/auth';
import { IChannel } from '@/services/channel/types';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_CURRENT_USER_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import { FormProvider } from 'react-hook-form';

interface UserChannelProps {
    channel?: IChannel;
}

const UserChannel: React.FC<UserChannelProps> = ({ channel }) => {
    const { form } = useBaseForm({ defaultValues: { channel } });
    return (
        <>
            <div className={'mt15'}>
                <FormProvider {...form}>
                    <SelectChannel
                        placeholder='Выбрать канал связи'
                        withLabel={false}
                        onChangeAction={(channel) => {
                            authService
                                .update({ channel_id: channel.id })
                                .then(() => {
                                    invalidateKey(GET_CURRENT_USER_KEY);
                                });
                        }}
                        onCloseClick={() => {
                            authService
                                .update({ channel_id: null })
                                .then(() => {
                                    invalidateKey(GET_CURRENT_USER_KEY);
                                });
                        }}
                    />
                </FormProvider>
            </div>
        </>
    );
};

export default UserChannel;
