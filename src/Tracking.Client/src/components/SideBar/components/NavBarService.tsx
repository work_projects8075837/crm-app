import { adminSidebarOpen } from '@/components/NavbarLayout/Layout';
import { CookiesStorage } from '@/services/utils/CookieStorage';
import { generateQuery } from '@/services/utils/generateQuery/generateQuery';
import NavbarLink from '../Links/NavbarLink/NavbarLink';

const NavBarService: React.FC = () => {
    const closeAdminSideBar = () => adminSidebarOpen.close();

    return (
        <div className='logo--container'>
            <NavbarLink
                to='/'
                src='/task.svg'
                alt='Заявки'
                onClick={closeAdminSideBar}
            />

            <NavbarLink
                onClick={closeAdminSideBar}
                to='/counterparty'
                src='/clients.svg'
                alt='Контрагент'
            />

            <NavbarLink
                onClick={closeAdminSideBar}
                to='/sale_point'
                src='/sale_point.svg'
                alt='Объекты'
            />

            <NavbarLink
                onClick={closeAdminSideBar}
                target='_blank'
                to={`/reports/${generateQuery({
                    access: CookiesStorage.getItem('access'),
                })}`}
                src='/report.svg'
                alt='Отчеты'
            />

            <NavbarLink
                onClick={closeAdminSideBar}
                to='/contact'
                src='/contacts.svg'
                alt='Контакты'
            />
        </div>
    );
};

export default NavBarService;
