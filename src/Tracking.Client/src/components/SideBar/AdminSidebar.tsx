import React from 'react';
import style from '../../style/layout/layout.module.scss';
import NavbarLink from './Links/NavbarLink/NavbarLink';

const AdminSidebar: React.FC = () => {
    return (
        <div className={style.adminSidebar}>
            <NavbarLink
                alt='Характеры проблемы'
                to='/character_problem'
                src='/character_problem.svg'
            />
            <NavbarLink
                alt='Направления'
                to='/direction'
                src='/direction.svg'
            />
            <NavbarLink alt='Каналы связи' to='/channel' src='/channel.svg' />
            <NavbarLink alt='Работы' to='/jobs' src='/job.svg' />
            <NavbarLink
                alt='Оборудование'
                to='/inventory'
                src='/equipment.svg'
            />
            <NavbarLink alt='Модули' to='/module' src='/modules.svg' />
        </div>
    );
};

export default React.memo(AdminSidebar);
