import React, { ButtonHTMLAttributes } from 'react';
import styles from '../../../../style/links/navbar_link.module.scss';
import LinkTooltip from '../LinkTooltip/LinkTooltip';

interface NavbarButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    alt: string;
    src: string;
    isSelected?: boolean;
    className?: string;
    onClick: () => void;
}

const NavbarButton: React.FC<NavbarButtonProps> = ({
    alt,
    src,
    className = '',
    isSelected = false,
    onClick,
    ...buttonProps
}) => {
    return (
        <button
            data-tooltip-id={`link-tooltip_${alt}`}
            data-tooltip-content={alt}
            {...buttonProps}
            onClick={onClick}
            className={`${styles.link} ${className} ${
                isSelected ? styles.active : ''
            }`}
        >
            <img src={src} alt={alt} className={styles.linkImg} />
            <LinkTooltip alt={alt} />
        </button>
    );
};

export default React.memo(NavbarButton);
