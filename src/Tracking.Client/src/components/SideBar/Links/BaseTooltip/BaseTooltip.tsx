import classNames from 'classnames';
import React from 'react';
import { ITooltip, Tooltip } from 'react-tooltip';
import styles from '../../../../style/links/navbar_link.module.scss';

type BaseTooltipProps = ITooltip;

const BaseTooltip: React.FC<BaseTooltipProps> = ({
    style,
    className,
    delayShow = 500,
    place = 'right',
    children,
    ...tooltipProps
}) => {
    return (
        <>
            <Tooltip
                {...tooltipProps}
                className={classNames(styles.tooltip, className)}
                delayShow={delayShow}
                place={place}
                style={{
                    backgroundColor: '#222222',
                    fontSize: 12,
                    fontWeight: 400,
                    zIndex: 2,
                    ...style,
                }}
            >
                {children}
            </Tooltip>
        </>
    );
};

export default BaseTooltip;
