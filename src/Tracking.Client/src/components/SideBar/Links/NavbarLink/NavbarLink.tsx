import React from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';
import styles from '../../../../style/links/navbar_link.module.scss';
import { Tooltip } from 'react-tooltip';
import LinkTooltip from '../LinkTooltip/LinkTooltip';

interface NavbarLinkProps extends NavLinkProps {
    alt: string;
    onClick?: () => void;
    className?: string;
    to: string;
    src: string;
}

const NavbarLink: React.FC<NavbarLinkProps> = ({
    className = '',
    onClick,
    to,
    src,
    alt,
    ...navLinkProps
}) => {
    return (
        <>
            <NavLink
                data-tooltip-id={`link-tooltip_${alt}`}
                data-tooltip-content={alt}
                onClick={onClick}
                {...navLinkProps}
                to={to}
                className={({ isActive }) =>
                    `${styles.link} ${className} ${
                        isActive ? styles.active : ''
                    }`
                }
            >
                <img src={src} alt={alt} className={styles.linkImg} />
                <LinkTooltip alt={alt} />
            </NavLink>
        </>
    );
};

export default React.memo(NavbarLink);
