import React from 'react';
import { PlacesType } from 'react-tooltip';
import BaseTooltip from '../BaseTooltip/BaseTooltip';

interface LinkTooltipProps {
    alt: string;
    place?: PlacesType;
}

const LinkTooltip: React.FC<LinkTooltipProps> = ({ alt, place = 'right' }) => {
    return (
        <>
            <BaseTooltip id={`link-tooltip_${alt}`} />
        </>
    );
};

export default LinkTooltip;
