import React, { useCallback } from 'react';
import { useCurrentUser } from '../../../../store/queries/stores/user/useCurrentUser';
import style from '../../../../style/links/navbar_link.module.scss';
import UserAvatar from '../../components/UserMenu/UserInfo/UserAvatar';
import { userMenu } from '../../components/UserMenu/UserMenu';

const UserMenuLink: React.FC = () => {
    const openUserMenu = useCallback(() => userMenu.open(), []);
    const { data } = useCurrentUser({ refetchOnMount: true });

    return (
        <>
            <button onClick={openUserMenu} className={`${style.link}`}>
                <UserAvatar className={style.userLinkPlaceholder} />
            </button>
        </>
    );
};

export default React.memo(UserMenuLink);
