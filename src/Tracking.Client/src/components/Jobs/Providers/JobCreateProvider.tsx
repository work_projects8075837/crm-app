import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import JobCreateForm from '@/components/Form/JobCreateForm/JobCreateForm';
import { useJobByParams } from '@/store/queries/stores/job/useJobByParams';
import { ValueHandler } from '@/store/states/SearchHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { jobCreateFromDirectoryModalOpen } from '../JobCreateFromDirectoryModal/JobCreateFromDirectoryModal';

export const updatableJobId = new ValueHandler<string>();

const JobCreateProvider: React.FC = () => {
    const { data } = useJobByParams({ refetchOnMount: true });
    return (
        <>
            {data ? (
                <JobCreateForm
                    openHandler={jobCreateFromDirectoryModalOpen}
                    defaultValues={{ job: data }}
                />
            ) : (
                <LoaderCircle />
            )}
        </>
    );
};

export default observer(JobCreateProvider);
