import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import UpdateJobForm from '@/components/Form/UpdateJobForm/UpdateJobForm';
import { jobHooks } from '@/services/job/job';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { jobUpdateModal } from '../JobUpdateModal/JobUpdateModal';
import { updatableJobId } from './JobCreateProvider';

interface JobEditProviderProps {
    children: React.ReactNode;
}

const JobEditProvider: React.FC<JobEditProviderProps> = ({ children }) => {
    const { data } = jobHooks.useOne(updatableJobId.value || undefined, {
        refetchOnMount: true,
    });
    return (
        <>
            {data ? (
                <UpdateJobForm
                    openHandler={jobUpdateModal}
                    defaultValues={data}
                >
                    {children}
                </UpdateJobForm>
            ) : (
                <LoaderCircle />
            )}
        </>
    );
};

export default observer(JobEditProvider);
