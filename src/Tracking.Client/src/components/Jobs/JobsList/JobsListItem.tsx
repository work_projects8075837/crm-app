import DurationText from '@/components/CharacterProblems/JobsList/DurationText';
import DirectoryIcon from '@/components/Elements/DirectoryIcon/DirectoryIcon';
import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import { countHours } from '@/components/utils/countHours';
import { IJob } from '@/services/job/types';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/jobs.module.scss';
import { jobUpdateModal } from '../JobUpdateModal/JobUpdateModal';
import { updatableJobId } from '../Providers/JobCreateProvider';
import { jobsCheck } from './JobsList';

type JobsListItemProps = IJob;

const JobsListItem: React.FC<JobsListItemProps> = ({
    id,
    name,
    duration,
    is_parent,
}) => {
    const navigate = useNavigate();
    return (
        <div
            className={style.tableItem}
            onDoubleClick={() => {
                if (is_parent) {
                    navigate(`/jobs/${id}`);
                } else {
                    updatableJobId.setValue(id);
                    jobUpdateModal.open();
                    // navigate(`/jobs/single/`);
                }
            }}
        >
            <CheckItem
                checkHandler={jobsCheck}
                entity={{ id, name, duration }}
            />
            <label className={style.tableItemName}>
                <span>{name}</span>

                {is_parent && <DirectoryIcon />}
            </label>

            <div className={style.tableItemField}>{id.split('-')[0]}</div>

            <div className={style.tableItemField}>
                <DurationText {...countHours(duration)} />
            </div>
        </div>
    );
};

export default JobsListItem;
