import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import React from 'react';
import style from '../../../style/layout/jobs.module.scss';
import { jobsCheck } from './JobsList';
import JobsPagination from './JobsPagination';

interface JobsTableLayoutProps {
    children: React.ReactNode;
}

const JobsTableLayout: React.FC<JobsTableLayoutProps> = ({ children }) => {
    return (
        <>
            <TableLayout
                header={
                    <TableHeader
                        checkHandler={jobsCheck}
                        fields={[
                            { name: 'Назвние' },
                            { name: 'Номер' },
                            { name: 'Время' },
                        ]}
                        layoutClass={style.rowLayout}
                        fieldClass={style.tableHeaderField}
                    />
                }
                layoutClass={style.entityPaginatedTable}
                pagination={<JobsPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default JobsTableLayout;
