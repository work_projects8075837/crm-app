import Pagination from '@/components/Tables/Pagination/Pagination';
import PaginationButtonsList from '@/components/Tickets/TiketsPaginationButtonsList/PaginationButtonsList';
import { useJobsTable } from '@/store/queries/stores/job/useJobsTable';
import { PaginationHandler } from '@/store/states/PaginationHandler';
import React from 'react';

const JOBS_PAGINATION = 'JOBS_PAGINATION';
export const jobsPagination = new PaginationHandler(JOBS_PAGINATION);

const JobsPagination: React.FC = () => {
    const { data } = useJobsTable(jobsPagination);
    return (
        <>
            <Pagination
                paginationHandler={jobsPagination}
                pages={
                    <PaginationButtonsList
                        paginationHandler={jobsPagination}
                        data={data}
                    />
                }
            />
        </>
    );
};

export default JobsPagination;
