import EntitiesLoading from '@/components/Elements/EntitiesLoading/EntitiesLoading';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { jobApi } from '@/services/job/job';
import {
    GET_ALL_JOBS_KEY,
    GET_SEARCH_JOBS_KEY,
} from '@/store/queries/keys/keys';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import React from 'react';
import { useJobsTable } from '../../../store/queries/stores/job/useJobsTable';
import JobCreateFromDirectoryModal from '../JobCreateFromDirectoryModal/JobCreateFromDirectoryModal';
import JobCreateModal from '../JobCreateModal/JobCreateModal';
import JobUpdateModal from '../JobUpdateModal/JobUpdateModal';
import JobsListItem from './JobsListItem';
import { jobsPagination } from './JobsPagination';
import JobsTableLayout from './JobsTableLayout';

export const jobsCheck = new CheckEntitiesHandler();

const JobsList: React.FC = () => {
    const { data, refetch } = useJobsTable(jobsPagination, {
        refetchOnMount: true,
    });
    useSetCheckableEntities(jobsCheck, data?.data);
    return (
        <>
            <EntitiesLoading
                emptyText='Нет работ'
                isEmpty={!data?.data.length}
                isLoading={!data}
            >
                <JobsTableLayout>
                    {data?.data.map((j) => <JobsListItem key={j.id} {...j} />)}
                </JobsTableLayout>
            </EntitiesLoading>
            <JobCreateModal />
            <JobCreateFromDirectoryModal />
            <JobUpdateModal />
            <EntitySelectedActions
                keys={[GET_ALL_JOBS_KEY, GET_SEARCH_JOBS_KEY]}
                checkHandler={jobsCheck}
                service={jobApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default JobsList;
