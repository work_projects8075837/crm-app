import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';
import JobCreateProvider from '../Providers/JobCreateProvider';

export const jobCreateFromDirectoryModalOpen = new OpenHandler();

const JobCreateFromDirectoryModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                title='Новая работа'
                openHandler={jobCreateFromDirectoryModalOpen}
            >
                <JobCreateProvider />
            </ModalWindow>
        </>
    );
};

export default JobCreateFromDirectoryModal;
