import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import DurationInput from '@/components/Fields/DurationInput/DurationInput';
import {
    JOB_DURATION_NAME,
    JOB_NAME_NAME,
} from '@/components/Fields/EntitiesFields/job';
import SelectJob from '@/components/Fields/SelectJob/SelectJob';
import ParentEntityExceptProvider from '@/components/SalePoints/Providers/ParentEntityExceptProvider/ParentEntityExceptProvider';
import { OpenHandler } from '@/store/states/OpenHandler';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { updatableJobId } from '../Providers/JobCreateProvider';
import JobEditProvider from '../Providers/JobEditProvider';
import JobUpdateTitle from './JobUpdateTitle';

export const jobUpdateModal = new OpenHandler();

const JobUpdateModal: React.FC = () => {
    useEffect(() => {
        return () => updatableJobId.setValue(null);
    }, []);
    return (
        <>
            <ModalWindow
                title={<JobUpdateTitle />}
                openHandler={jobUpdateModal}
            >
                <JobEditProvider>
                    <FormInput name={JOB_NAME_NAME} placeholder='Название' />

                    <SelectJob onlyDirectory={true} />

                    <ParentEntityExceptProvider>
                        <DurationInput
                            required={false}
                            name={JOB_DURATION_NAME}
                        />
                    </ParentEntityExceptProvider>
                </JobEditProvider>
            </ModalWindow>
        </>
    );
};

export default observer(JobUpdateModal);
