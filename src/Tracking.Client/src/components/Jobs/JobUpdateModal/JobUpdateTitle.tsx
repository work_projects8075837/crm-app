import { jobHooks } from '@/services/job/job';
import React from 'react';
import { updatableJobId } from '../Providers/JobCreateProvider';

const JobUpdateTitle: React.FC = () => {
    const { data } = jobHooks.useOne(updatableJobId.value || undefined);
    return <>{data?.name}</>;
};

export default JobUpdateTitle;
