import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import JobCreateForm from '@/components/Form/JobCreateForm/JobCreateForm';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';

export const jobCreateOpen = new OpenHandler();

const JobCreateModal: React.FC = () => {
    return (
        <>
            <ModalWindow openHandler={jobCreateOpen} title='Новая работа'>
                <JobCreateForm openHandler={jobCreateOpen} />
            </ModalWindow>
        </>
    );
};

export default JobCreateModal;
