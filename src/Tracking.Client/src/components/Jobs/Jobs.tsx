import React from 'react';
import HeaderLayout from '../Layouts/HeaderLayout/HeaderLayout';
import Layout from '../NavbarLayout/Layout';
import DirectoryHeaderButtons from '../SalePoints/SalePointsHeader/DirectoryHeaderButtons';
import { jobCreateOpen } from './JobCreateModal/JobCreateModal';
import JobsList from './JobsList/JobsList';

const Jobs: React.FC = () => {
    return (
        <>
            <Layout>
                <HeaderLayout title='Справочник работ'>
                    <DirectoryHeaderButtons
                        entityText='Новая работа'
                        directoryText='Новая папка работ'
                        link={'/jobs/'}
                        onDirectoryClick={() => {
                            jobCreateOpen.open();
                        }}
                        onEntityClick={() => {
                            jobCreateOpen.open();
                        }}
                    />
                </HeaderLayout>
                <JobsList />
            </Layout>
        </>
    );
};

export default Jobs;
