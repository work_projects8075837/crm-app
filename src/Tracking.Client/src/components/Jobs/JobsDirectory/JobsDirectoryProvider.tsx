import Loader from '@/Loader/Loader';
import { useJobBreadCrumbs } from '@/store/queries/stores/job/useJobBreadCrumbs';
import React from 'react';

interface JobsDirectoryProviderProps {
    children: React.ReactNode;
}

const JobsDirectoryProvider: React.FC<JobsDirectoryProviderProps> = ({
    children,
}) => {
    const { isLoading } = useJobBreadCrumbs({ refetchOnMount: true });
    return <>{isLoading ? <Loader /> : children}</>;
};

export default JobsDirectoryProvider;
