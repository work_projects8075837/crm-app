import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import JobsLink from '@/components/Elements/BreadCrumbs/JobsLink';
import BaseDirectoryHeader from '@/components/SalePoints/SalePointsHeader/BaseDirectoryHeader';
import { StyleBreadcrumb } from '@/style/elements/elements';
import React from 'react';
import { jobCreateFromDirectoryModalOpen } from '../JobCreateFromDirectoryModal/JobCreateFromDirectoryModal';
import { jobUpdateModal } from '../JobUpdateModal/JobUpdateModal';
import { updatableJobId } from '../Providers/JobCreateProvider';
import { useJobBreadCrumbs } from './../../../store/queries/stores/job/useJobBreadCrumbs';

const JobsDirectoryHeader: React.FC = () => {
    const { data, current } = useJobBreadCrumbs();
    return (
        <>
            <BaseDirectoryHeader
                breadcrumbs={
                    <>
                        <JobsLink />
                        <InnerBreadCrumbs links={data} route={'jobs'} />
                    </>
                }
                title={
                    <button
                        className={StyleBreadcrumb.link}
                        onClick={() => {
                            updatableJobId.setValue(current?.id || null);
                            jobUpdateModal.open();
                        }}
                    >
                        {current?.name}
                    </button>
                }
                onDirectoryClick={() => {
                    jobCreateFromDirectoryModalOpen.open();
                }}
                onEntityClick={() => {
                    jobCreateFromDirectoryModalOpen.open();
                }}
                link={`/jobs/${current?.id}/`}
                directoryText='Новая папка работ'
                entityText='Новая работа'
            />
        </>
    );
};

export default JobsDirectoryHeader;
