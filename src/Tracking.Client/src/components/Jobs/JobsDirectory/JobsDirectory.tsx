import Layout from '@/components/NavbarLayout/Layout';
import React from 'react';
import JobsList from '../JobsList/JobsList';
import JobsDirectoryHeader from './JobsDirectoryHeader';
import JobsDirectoryProvider from './JobsDirectoryProvider';

const JobsDirectory: React.FC = () => {
    return (
        <>
            <Layout>
                <JobsDirectoryProvider>
                    <JobsDirectoryHeader />
                    <JobsList />
                </JobsDirectoryProvider>
            </Layout>
        </>
    );
};

export default JobsDirectory;
