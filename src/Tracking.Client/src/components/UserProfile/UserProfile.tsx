import React from 'react';
import UserGeneral from './UserGeneral';
import UserProfileFormProvider from './UserProfileFormProvider';
import UserProfileHeader from './UserProfileHeader';

const UserProfile: React.FC = () => {
    return (
        <>
            <UserProfileFormProvider>
                <UserProfileHeader />
                <UserGeneral />
            </UserProfileFormProvider>
        </>
    );
};

export default UserProfile;
