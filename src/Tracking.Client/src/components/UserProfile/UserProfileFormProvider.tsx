import Loader from '@/Loader/Loader';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import React from 'react';
import style from '../../style/layout/layout.module.scss';
import UpdateUserForm from '../Form/UpdateUserForm/UpdateUserForm';
import Layout from '../NavbarLayout/Layout';

interface UserProfileFormProviderProps {
    children: React.ReactNode;
}

const UserProfileFormProvider: React.FC<UserProfileFormProviderProps> = ({
    children,
}) => {
    const { data } = useCurrentUser({ refetchOnMount: true });
    return (
        <>
            {data ? (
                <Layout childrenClass={style.formLayout}>
                    <UpdateUserForm defaultValues={data}>
                        {children}
                    </UpdateUserForm>
                </Layout>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default UserProfileFormProvider;
