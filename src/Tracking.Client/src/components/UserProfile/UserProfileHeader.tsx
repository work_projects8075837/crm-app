import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import React from 'react';
import TicketsLink from '../Elements/BreadCrumbs/TicketsLink';
import HeaderLayout from '../Layouts/HeaderLayout/HeaderLayout';

const UserProfileHeader: React.FC = () => {
    const { data } = useCurrentUser();
    return (
        <>
            <HeaderLayout
                breadcrumbs={
                    <>
                        <TicketsLink /> / Пользователи
                    </>
                }
                title={`Профиль пользователя ${data?.name}`}
            ></HeaderLayout>
        </>
    );
};

export default UserProfileHeader;
