import React from 'react';
import style from '../../style/layout/layout.module.scss';
import FormSelect from '../Elements/FormSelect/FormSelect';
import EmailShortInput from '../Fields/EmailShortInput/EmailShortInput';
import FormShortInput from '../Fields/FormShortInput/FormShortInput';
import EntityFieldLayout from '../Fields/Layouts/EntityFieldLayout';
import PhoneShortInput from '../Fields/PhoneShortInput/PhoneShortInput';
import SelectChannel from '../Fields/SelectChannel/SelectChannel';
import SelectDirection from '../Fields/SelectDirection/SelectDirection';
import DoubleColumnsLayout from '../Layouts/DoubleColumnsLayout/DoubleColumnsLayout';

const UserGeneral: React.FC = () => {
    return (
        <>
            <DoubleColumnsLayout
                className={style.afterTabsMargin}
                left={
                    <>
                        <FormSelect title='Основное'>
                            <EntityFieldLayout title='ФИО'>
                                <FormShortInput
                                    name='name'
                                    placeholder='Ваше ФИО'
                                />
                            </EntityFieldLayout>
                            <EntityFieldLayout title='E-mail'>
                                <EmailShortInput name='email' />
                            </EntityFieldLayout>
                            <EntityFieldLayout title='Телефон'>
                                <PhoneShortInput
                                    name='phone'
                                    placeholder='+7 900 000 00-00'
                                    required={false}
                                />
                            </EntityFieldLayout>
                        </FormSelect>
                    </>
                }
                right={
                    <>
                        <FormSelect title='Настройки'>
                            <SelectChannel />
                            <SelectDirection />
                        </FormSelect>
                    </>
                }
            />
        </>
    );
};

export default UserGeneral;
