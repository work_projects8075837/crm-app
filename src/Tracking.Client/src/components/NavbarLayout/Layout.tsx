import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';
import GlobalLoader from '../Elements/GlobalLoader/GlobalLoader';
import AdminLayout from './AdminLayout';
import SimpleLayout from './SimpleLayout';

interface LayoutProps {
    children: React.ReactNode;
    childrenClass?: string;
}

export const adminSidebarOpen = new OpenHandler(false);

const Layout: React.FC<LayoutProps> = ({ children, childrenClass = '' }) => {
    return (
        <>
            {adminSidebarOpen.isOpen ? (
                <AdminLayout childrenClass={childrenClass}>
                    {children}
                </AdminLayout>
            ) : (
                <SimpleLayout childrenClass={childrenClass}>
                    {children}
                </SimpleLayout>
            )}
            <GlobalLoader />
        </>
    );
};

export default observer(Layout);
