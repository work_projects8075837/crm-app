import React from 'react';
import styles from '../../style/layout/layout.module.scss';
import AdminSidebar from '../SideBar/AdminSidebar';
import SimpleLayout from './SimpleLayout';

interface AdminLayoutProps {
    children: React.ReactNode;
    childrenClass?: string;
}

const AdminLayout: React.FC<AdminLayoutProps> = ({
    children,
    childrenClass = '',
}) => {
    return (
        <>
            <SimpleLayout childrenClass={styles.emptyPadding}>
                <div className={styles.window}>
                    <AdminSidebar />
                    <div className={`${styles.children} ${childrenClass}`}>
                        {children}
                    </div>
                </div>
            </SimpleLayout>
        </>
    );
};

export default AdminLayout;
