import React from 'react';
import SideBar from '../SideBar/SideBar';
import styles from '../../style/layout/layout.module.scss';

interface SimpleLayoutProps {
    children: React.ReactNode;
    childrenClass?: string;
}

const SimpleLayout: React.FC<SimpleLayoutProps> = ({
    children,
    childrenClass = '',
}) => {
    return (
        <>
            <div className={styles.window}>
                <SideBar />
                <div className={`${styles.children} ${childrenClass}`}>
                    {children}
                </div>
            </div>
        </>
    );
};

export default SimpleLayout;
