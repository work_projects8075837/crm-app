import React from 'react';
import SalePointEditLayout from '../../SalePointEdit/SalePointEditLayout';
import InventoryDirectoryBreadCrumbs from '../../SalePointEditInventory/InventoryDirectory/InventoryDirectoryHeader';
import InventoryDirectoryHeaderButtons from '../../SalePointEditInventory/InventoryDirectory/InventoryDirectoryHeaderButtons';
import InventoryDirectoryProvider from '../../SalePointEditInventory/InventoryDirectory/InventoryDirectoryProvider';
import SalePointConnectionsList from '../SalePointConnectionsList';

const ConnectionsDirectory: React.FC = () => {
    return (
        <>
            <InventoryDirectoryProvider>
                <SalePointEditLayout
                    breadcrumbs={<InventoryDirectoryBreadCrumbs />}
                    headerButton={
                        <>
                            <InventoryDirectoryHeaderButtons />
                        </>
                    }
                >
                    <SalePointConnectionsList />
                </SalePointEditLayout>
            </InventoryDirectoryProvider>
        </>
    );
};

export default ConnectionsDirectory;
