import React from 'react';
import { useParams } from 'react-router-dom';
import SalePointEditLayout from '../SalePointEdit/SalePointEditLayout';
import DirectoryHeaderButtons from '../SalePointsHeader/DirectoryHeaderButtons';
import SalePointConnectionsList from './SalePointConnectionsList';

const SalePointConnections: React.FC = () => {
    const { salePointId } = useParams();
    return (
        <>
            <SalePointEditLayout
                headerButton={
                    <>
                        <DirectoryHeaderButtons
                            link={`/sale_point/single/${salePointId}/inventory/create/?is_connection=1`}
                            entityText='Добавить подключение'
                        />
                    </>
                }
            >
                <SalePointConnectionsList />
            </SalePointEditLayout>
        </>
    );
};

export default SalePointConnections;
