import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import NullEntities from '@/components/Elements/NullEntities/NullEntities';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { inventoryApi } from '@/services/inventory/inventory';
import { TInventoryFilter } from '@/services/inventory/types';
import {
    GET_ALL_INVENTORIES_KEY,
    GET_SEARCH_INVENTORIES_KEY,
} from '@/store/queries/keys/keys';
import { useInventoryTable } from '@/store/queries/stores/inventory/useInventoryBySalePointTable';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { PaginationHandler } from '@/store/states/PaginationHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import React from 'react';
import SalePointInventoryItem from '../SalePointEditInventory/SalePointInventoryList/SalePointInventoryItem';
import SalePointInventoryTableLayout from '../SalePointEditInventory/SalePointInventoryList/SalePointInventoryTableLayout';

const CONNECTIONS_PAGINATION = 'CONNECTIONS_PAGINATION';
export const inventoryPagination = new PaginationHandler(
    CONNECTIONS_PAGINATION,
);

const connectionsCheck = new CheckEntitiesHandler();

const filters: TInventoryFilter = {
    inventory_is_connection: true,
};

const SalePointConnectionsList: React.FC = () => {
    const { data, refetch } = useInventoryTable(
        { ...inventoryPagination, ...filters },
        {
            refetchOnMount: true,
        },
    );
    const onlyConnections = data?.data;
    useSetCheckableEntities(connectionsCheck, onlyConnections);
    return (
        <>
            {onlyConnections ? (
                <>
                    {onlyConnections.length ? (
                        <SalePointInventoryTableLayout
                            checkHandler={connectionsCheck}
                            withConnections={true}
                            {...{ filters }}
                        >
                            {onlyConnections.map((inventory) => (
                                <SalePointInventoryItem
                                    checkHandler={connectionsCheck}
                                    key={inventory.id}
                                    {...inventory}
                                    version={inventory?.software[0]?.version}
                                    withConnections={true}
                                />
                            ))}
                        </SalePointInventoryTableLayout>
                    ) : (
                        <NullEntities text='У данного заведения нет подключенний' />
                    )}
                </>
            ) : (
                <LoaderCircle size={60} />
            )}
            <EntitySelectedActions
                keys={[GET_ALL_INVENTORIES_KEY, GET_SEARCH_INVENTORIES_KEY]}
                checkHandler={connectionsCheck}
                service={inventoryApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default SalePointConnectionsList;
