import React from 'react';
import { useWatch } from 'react-hook-form';

interface ParentEntityExceptProviderProps {
    children: React.ReactNode;
}

const ParentEntityExceptProvider: React.FC<ParentEntityExceptProviderProps> = ({
    children,
}) => {
    const is_parent = useWatch({ name: 'is_parent' });

    return <>{!is_parent && children}</>;
};

export default ParentEntityExceptProvider;
