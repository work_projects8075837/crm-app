import Loader from '@/Loader/Loader';
import SalePointCreateForm from '@/components/Form/SalePointCreateForm/SalePointCreateForm';
import Layout from '@/components/NavbarLayout/Layout';
import AddTagsModal from '@/components/Tickets/TicketCreate/AddTagsModal/AddTagsModal';
import { useSalePointBreadCrumbs } from '@/store/queries/stores/sale_point/useSalePointBreadCrumbs';
import { useSalePointByParams } from '@/store/queries/stores/sale_point/useSalePointByParams';
import React from 'react';
import style from '../../../../style/layout/layout.module.scss';
import ChangeContactModal from '../../ChangeContactModal/ChangeContactModal';
import CounterPartySelectModal from '../../CounterPartySelectModal/CounterPartySelectModal';
import CreateShortCounterPartyModal from '../../CreateShortCounterPartyModal/CreateShortCounterPartyModal';
import { salePointsFilesOpen } from '../../SalePointsCreate/SalePointsCreateOptions/SalePointsCreateOptions';
import SalePointsFilesModal from '../../SalePointsFilesModal/SalePointsFilesModal';
import CreateFormContactModal from '../../SelectContactModal/CreateFormContactModal/CreateFormContactModal';
import SelectContactModal from '../../SelectContactModal/SelectContactModal';

interface SalePointsCreateFormProviderProps {
    children: React.ReactNode;
}

const SalePointsCreateFormProvider: React.FC<
    SalePointsCreateFormProviderProps
> = ({ children }) => {
    const { data } = useSalePointByParams();
    const { isLoading } = useSalePointBreadCrumbs({ refetchOnMount: true });
    return (
        <>
            {data && !isLoading ? (
                <Layout childrenClass={style.formLayout}>
                    <SalePointCreateForm
                        parentSalePoint={data}
                        modals={
                            <>
                                <ChangeContactModal />
                                <SelectContactModal />
                                <CreateFormContactModal />
                                <SalePointsFilesModal
                                    openHandler={salePointsFilesOpen}
                                />
                                <CounterPartySelectModal />
                                <CreateShortCounterPartyModal />
                                <AddTagsModal />
                            </>
                        }
                    >
                        {children}
                    </SalePointCreateForm>
                </Layout>
            ) : (
                <>
                    <Loader />
                </>
            )}
        </>
    );
};

export default SalePointsCreateFormProvider;
