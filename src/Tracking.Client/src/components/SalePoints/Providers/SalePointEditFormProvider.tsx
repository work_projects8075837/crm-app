import Loader from '@/Loader/Loader';
import SalePointUpdateForm from '@/components/Form/SalePointUpdateForm/SalePointUpdateForm';
import Layout from '@/components/NavbarLayout/Layout';
import AddTagsModal from '@/components/Tickets/TicketCreate/AddTagsModal/AddTagsModal';
import { useSalePointBreadCrumbs } from '@/store/queries/stores/sale_point/useSalePointBreadCrumbs';
import { useSalePointByParams } from '@/store/queries/stores/sale_point/useSalePointByParams';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import ChangeContactModal from '../ChangeContactModal/ChangeContactModal';
import CounterPartySelectModal from '../CounterPartySelectModal/CounterPartySelectModal';
import CreateShortCounterPartyModal from '../CreateShortCounterPartyModal/CreateShortCounterPartyModal';
import { salePointsFilesOpen } from '../SalePointsCreate/SalePointsCreateOptions/SalePointsCreateOptions';
import SalePointsFilesModal from '../SalePointsFilesModal/SalePointsFilesModal';
import CreateFormContactModal from '../SelectContactModal/CreateFormContactModal/CreateFormContactModal';
import SelectContactModal from '../SelectContactModal/SelectContactModal';
import SelectPhoneNumberModal from '../SelectPhoneNumberModal/SelectPhoneNumberModal';

interface SalePointEditFormProviderProps {
    children: React.ReactNode;
}

const SalePointEditFormProvider: React.FC<SalePointEditFormProviderProps> = ({
    children,
}) => {
    const { data } = useSalePointByParams({ refetchOnMount: true });
    const { isLoading } = useSalePointBreadCrumbs({ refetchOnMount: true });

    return (
        <>
            {data && !isLoading ? (
                <Layout childrenClass={style.formLayout}>
                    <SalePointUpdateForm
                        modals={
                            <>
                                <ChangeContactModal />
                                <ChangeContactModal />
                                <SelectContactModal />
                                <CreateFormContactModal />
                                <SalePointsFilesModal
                                    openHandler={salePointsFilesOpen}
                                />
                                <CounterPartySelectModal />
                                <CreateShortCounterPartyModal />
                                <AddTagsModal />
                                <SelectPhoneNumberModal />
                            </>
                        }
                        salePointFrom={{
                            ...data,

                            position: data.position?.map((c) => ({
                                ...c,
                                ...c.contact,
                                position_name: c.position,
                                id: c.id,
                                contact_id: c.contact?.id,
                            })),
                            sale_point: data.sale_point,
                            description: data.description,
                            regulations: data.regulations,
                            status: { id: data.status, name: data.status },
                        }}
                    >
                        {children}
                    </SalePointUpdateForm>
                </Layout>
            ) : (
                <>
                    <Loader />
                </>
            )}
        </>
    );
};

export default SalePointEditFormProvider;
