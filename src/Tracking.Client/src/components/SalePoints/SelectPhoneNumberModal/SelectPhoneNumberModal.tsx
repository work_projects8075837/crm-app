import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { SALE_POINT_PHONE_NUMBER_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import SearchSelectPhoneNumber from '@/components/Fields/SearchSelectPhoneNumber/SearchSelectPhoneNumber';
import SelectEntityForm from '@/components/Form/SelectEntityForm/SelectEntityForm';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';

export const phoneNumberSelect = new OpenHandler();

const SelectPhoneNumberModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                title='Выбрать телефон'
                openHandler={phoneNumberSelect}
            >
                <SelectEntityForm
                    name={SALE_POINT_PHONE_NUMBER_NAME}
                    openHandler={phoneNumberSelect}
                >
                    <SearchSelectPhoneNumber />
                    <SaveButton>Сохранить</SaveButton>
                </SelectEntityForm>
            </ModalWindow>
        </>
    );
};

export default SelectPhoneNumberModal;
