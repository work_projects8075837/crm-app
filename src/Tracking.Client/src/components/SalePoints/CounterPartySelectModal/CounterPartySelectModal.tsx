import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import CounterPartySearchCreate from '@/components/Fields/CounterPartySearchCreate/CounterPartySearchCreate';
import { SALE_POINT_COUNTER_PARTIES_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import SelectEntityForm from '@/components/Form/SelectEntityForm/SelectEntityForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';

export const counterPartySelectModal = new OpenHandler(false);

const CounterPartySelectModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                openHandler={counterPartySelectModal}
                title='Выбрать контрагента'
            >
                <div className={style.modalVert}>
                    <SelectEntityForm
                        openHandler={counterPartySelectModal}
                        name={SALE_POINT_COUNTER_PARTIES_NAME}
                    >
                        <CounterPartySearchCreate
                            openHandler={counterPartySelectModal}
                        />
                        <SaveButton>Сохранить</SaveButton>
                    </SelectEntityForm>
                </div>
            </ModalWindow>
        </>
    );
};

export default CounterPartySelectModal;
