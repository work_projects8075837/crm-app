import { SALE_POINT_FILES_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import FilesModal from '@/components/Fields/FilesModal/FilesModal';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';

interface SalePointsFilesModalProps {
    openHandler: OpenHandler;
}

const SalePointsFilesModal: React.FC<SalePointsFilesModalProps> = ({
    openHandler,
}) => {
    return (
        <>
            <FilesModal
                name={SALE_POINT_FILES_NAME}
                openHandler={openHandler}
            />
        </>
    );
};

export default SalePointsFilesModal;
