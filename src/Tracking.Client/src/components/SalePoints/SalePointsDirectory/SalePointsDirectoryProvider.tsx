import Loader from '@/Loader/Loader';
import { useSalePointBreadCrumbs } from '@/store/queries/stores/sale_point/useSalePointBreadCrumbs';
import React from 'react';

interface SalePointsDirectoryProviderProps {
    children: React.ReactNode;
}

const SalePointsDirectoryProvider: React.FC<
    SalePointsDirectoryProviderProps
> = ({ children }) => {
    const { data } = useSalePointBreadCrumbs({ refetchOnMount: true });

    return <>{data ? children : <Loader />}</>;
};

export default SalePointsDirectoryProvider;
