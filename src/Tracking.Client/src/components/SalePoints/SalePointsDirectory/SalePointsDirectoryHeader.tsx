import BreadCrumbLink from '@/components/Elements/BreadCrumbs/BreadCrumbLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import SalePointsLink from '@/components/Elements/BreadCrumbs/SalePointsLink';
import { useSalePointBreadCrumbs } from '@/store/queries/stores/sale_point/useSalePointBreadCrumbs';
import React from 'react';
import BaseDirectoryHeader from '../SalePointsHeader/BaseDirectoryHeader';

const SalePointsDirectoryHeader: React.FC = () => {
    const { data, current } = useSalePointBreadCrumbs();

    return (
        <>
            <BaseDirectoryHeader
                breadcrumbs={
                    <>
                        <SalePointsLink />
                        <InnerBreadCrumbs links={data} route={'sale_point'} />
                    </>
                }
                title={
                    <BreadCrumbLink to={`/sale_point/single/${current?.id}`}>
                        {current?.name}
                    </BreadCrumbLink>
                }
                link={`/sale_point/create/${current?.id}`}
                directoryText='Новая папка заведений'
                entityText='Новое заведение'
            />
        </>
    );
};

export default SalePointsDirectoryHeader;
