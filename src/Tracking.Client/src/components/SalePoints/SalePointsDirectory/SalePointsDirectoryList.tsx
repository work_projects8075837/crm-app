import EntitiesLoading from '@/components/Elements/EntitiesLoading/EntitiesLoading';
import React from 'react';
import SalePointsTableLayout from '../SalePointsList/SalePointsTableLayout/SalePointsTableLayout';
import SalePointsListItem from '../SalePointsList/SalePointsListItem';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import { useSalePointsTable } from '@/store/queries/stores/sale_point/useSalePointsTable';
import { salePointPaginationHandler } from '../SalePointsList/SalePointPagination/SalePointPagination';
import { salePointCheckHandler } from '../SalePointsList/SalePointsList';

const SalePointsDirectoryList: React.FC = () => {
    const { data } = useSalePointsTable(salePointPaginationHandler, {
        refetchOnMount: true,
    });

    useSetCheckableEntities(salePointCheckHandler, data?.data);

    return (
        <>
            <EntitiesLoading
                isLoading={!data}
                isEmpty={!data?.data.length}
                emptyText='Нет заведений'
            >
                <SalePointsTableLayout>
                    {data?.data.map((salePoint) => (
                        <SalePointsListItem key={salePoint.id} {...salePoint} />
                    ))}
                </SalePointsTableLayout>
            </EntitiesLoading>
        </>
    );
};

export default SalePointsDirectoryList;
