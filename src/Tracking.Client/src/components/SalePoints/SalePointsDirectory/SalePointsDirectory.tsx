import Layout from '@/components/NavbarLayout/Layout';
import React from 'react';
import SalePointsDirectoryHeader from './SalePointsDirectoryHeader';
import SalePointsDirectoryProvider from './SalePointsDirectoryProvider';
import SalePointsList from '../SalePointsList/SalePointsList';

const SalePointsDirectory: React.FC = () => {
    return (
        <>
            <Layout>
                <SalePointsDirectoryProvider>
                    <SalePointsDirectoryHeader />
                    <SalePointsList />
                </SalePointsDirectoryProvider>
            </Layout>
        </>
    );
};

export default SalePointsDirectory;
