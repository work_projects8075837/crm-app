import { SALE_POINT_PHONE_NUMBER_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import EntitiesFormCardsLayout from '@/components/Layouts/EntitiesFormCardsLayout/EntitiesFormCardsLayout';
import React from 'react';
import { phoneNumberSelect } from '../SelectPhoneNumberModal/SelectPhoneNumberModal';
import SalePointPhoneNumberItem from './SalePointPhoneNumberItem';

const SalePointPhoneNumberList: React.FC = () => {
    const { entities } = useFormEntityValueArray(SALE_POINT_PHONE_NUMBER_NAME);

    return (
        <EntitiesFormCardsLayout onPlusClick={() => phoneNumberSelect.open()}>
            {entities?.map((phoneNumber) => (
                <SalePointPhoneNumberItem
                    key={phoneNumber.id}
                    {...phoneNumber}
                />
            ))}
        </EntitiesFormCardsLayout>
    );
};

export default SalePointPhoneNumberList;
