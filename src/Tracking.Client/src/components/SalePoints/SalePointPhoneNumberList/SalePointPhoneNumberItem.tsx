import { SALE_POINT_PHONE_NUMBER_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import SelectedEntityCardLayout from '@/components/Fields/SelectedEntityCard/SelectedEntityCardLayout';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import React from 'react';

interface SalePointPhoneNumberItemProps {
    id: string;
    name?: string;
}

const SalePointPhoneNumberItem: React.FC<SalePointPhoneNumberItemProps> = ({
    id,
    name,
}) => {
    const { remove } = useFormEntityValueArray(SALE_POINT_PHONE_NUMBER_NAME);
    return (
        <>
            <SelectedEntityCardLayout
                id={id}
                valueName={name}
                title='Телефон'
                closeBtn={
                    <CloseCrossButton
                        onClick={(event) => {
                            event.stopPropagation();
                            remove(id);
                        }}
                    />
                }
            ></SelectedEntityCardLayout>
        </>
    );
};

export default SalePointPhoneNumberItem;
