import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import CreateShortCounterPartyForm from '@/components/Form/CreateShortCounterPartyForm/CreateShortCounterPartyForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import {
    COUNTER_PARTY_MRN_NAME,
    COUNTER_PARTY_NAME_NAME,
    COUNTER_PARTY_TIN_NAME,
} from './../../Fields/EntitiesFields/counter_party';

export const createShortCounterPartyModal = new OpenHandler(false);

const CreateShortCounterPartyModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                openHandler={createShortCounterPartyModal}
                title='Добавить контрагента'
            >
                <CreateShortCounterPartyForm
                    openHandler={createShortCounterPartyModal}
                >
                    <FormInput
                        name={COUNTER_PARTY_NAME_NAME}
                        placeholder='Название'
                    />
                    <FormInput
                        name={COUNTER_PARTY_TIN_NAME}
                        placeholder='ИНН'
                    />
                    <FormInput
                        name={COUNTER_PARTY_MRN_NAME}
                        placeholder='ОГРН'
                    />
                </CreateShortCounterPartyForm>
            </ModalWindow>
        </>
    );
};

export default CreateShortCounterPartyModal;
