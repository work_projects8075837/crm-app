import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import { IContact } from '@/services/contact/types';
import { OpenHandler } from '@/store/states/OpenHandler';
import { ValueHandler } from '@/store/states/SearchHandler';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import ChangeContactForm from './ChangeContactForm';

export interface IEditableContact {
    id: string;
    is_responsible: boolean;
    contact: IContact;
    position_name: string;
}

export const changeContactOpen = new OpenHandler();
export const editableContact = new ValueHandler<IEditableContact>();

const ChangeContactModal: React.FC = () => {
    useEffect(() => {
        return () => editableContact.setValue(null);
    }, []);

    return (
        <>
            <ModalWindow
                title={editableContact.value?.contact.name}
                openHandler={changeContactOpen}
            >
                {editableContact.value && (
                    <ChangeContactForm defaultValues={editableContact.value} />
                )}
            </ModalWindow>
        </>
    );
};

export default observer(ChangeContactModal);
