import CheckboxInput from '@/components/Fields/BaseFields/CheckboxInput';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import {
    ISalePointFormContact,
    SALE_POINT_CONTACTS_NAME,
} from '@/components/Fields/EntitiesFields/sale_point';
import SearchSelectContact from '@/components/Fields/SearchSelectContact/SearchSelectContact';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import BaseForm from '@/components/Form/BaseForm/BaseForm';
import { useBaseForm } from '@/components/Form/hooks/useBaseForm';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import { formContactModalOpen } from '../SelectContactModal/CreateFormContactModal/CreateFormContactModal';
import { selectContactModalOpen } from '../SelectContactModal/SelectContactModal';
import { IEditableContact, changeContactOpen } from './ChangeContactModal';

interface ChangeContactFormProps {
    defaultValues: IEditableContact;
}

const ChangeContactForm: React.FC<ChangeContactFormProps> = ({
    defaultValues,
}) => {
    const { update } = useFormEntityValueArray<ISalePointFormContact>(
        SALE_POINT_CONTACTS_NAME,
    );

    const { form } = useBaseForm<IEditableContact>({
        defaultValues,
    });

    const onSubmit = form.handleSubmit((data) => {
        update(data.id, {
            ...data,
            ...data.contact,
            contact_id: data.contact.id,
            id: data.id,
        });
        changeContactOpen.close();
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{
                    onSubmit,
                    className: style.modalVertAround,
                }}
            >
                <FormInput name='position_name' placeholder='Должность' />

                <SearchSelectContact
                    name='contact'
                    onCreateClick={() => {
                        selectContactModalOpen.close();
                        formContactModalOpen.open();
                    }}
                />
                <CheckboxInput
                    name='is_responsible'
                    label='Сделать старшим по точке'
                />
                <SaveButton>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default ChangeContactForm;
