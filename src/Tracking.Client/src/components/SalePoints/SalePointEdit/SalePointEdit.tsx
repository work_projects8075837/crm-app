import React from 'react';
import SalePointGeneral from '../SalePointGeneral/SalePointGeneral';
import SalePointEditLayout from './SalePointEditLayout';

const SalePointEdit: React.FC = () => {
    return (
        <>
            <SalePointEditLayout>
                <SalePointGeneral />
            </SalePointEditLayout>
        </>
    );
};

export default SalePointEdit;
