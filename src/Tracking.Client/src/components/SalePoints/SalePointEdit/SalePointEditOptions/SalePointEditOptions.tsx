import ModalMenu from '@/components/Elements/ModalMenu/ModalMenu';
import ActionsMenuDeleteButton from '@/components/Fields/ActionsMenuDeleteButton/ActionsMenuDeleteButton';
import EntityOptionsLayout from '@/components/Tickets/TicketOptions/EntityOptionsLayout';
import TicketOptionButton from '@/components/Tickets/TicketOptions/TicketOptionButton';
import { salePointApi } from '@/services/sale_point/sale_point';
import { GET_ALL_SALE_POINTS_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import { salePointsFilesOpen } from '../../SalePointsCreate/SalePointsCreateOptions/SalePointsCreateOptions';

const SalePointEditOptions: React.FC = () => {
    return (
        <>
            <EntityOptionsLayout>
                <ModalMenu symbol='ticket_edit' text='Действия'>
                    <ActionsMenuDeleteButton
                        queryKey={GET_ALL_SALE_POINTS_KEY}
                        service={salePointApi}
                        idName='salePointId'
                        navigateTo='/sale_point/'
                    />
                </ModalMenu>
                <TicketOptionButton
                    onClick={() => salePointsFilesOpen.open()}
                    symbol='ticket_file'
                    text='Файлы'
                    svgSize={{ width: 9, height: 13 }}
                />
            </EntityOptionsLayout>
        </>
    );
};

export default SalePointEditOptions;
