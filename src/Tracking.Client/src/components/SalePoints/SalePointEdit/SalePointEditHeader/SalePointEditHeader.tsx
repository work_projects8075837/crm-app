import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import SalePointsLink from '@/components/Elements/BreadCrumbs/SalePointsLink';
import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import { useSalePointBreadCrumbs } from '@/store/queries/stores/sale_point/useSalePointBreadCrumbs';
import React from 'react';
import OneEntityBaseHeader from '../../SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';

interface SalePointEditHeaderProps {
    children?: React.ReactNode;
    breadcrumbs?: React.ReactNode;
}

const SalePointEditHeader: React.FC<SalePointEditHeaderProps> = ({
    children,
    breadcrumbs,
}) => {
    const { data, current } = useSalePointBreadCrumbs();
    const inventoryBreadcrumbs = useInventoryBreadCrumbs();
    return (
        <>
            <OneEntityBaseHeader
                title={`${current?.name} ${
                    inventoryBreadcrumbs.current
                        ? `/ ${inventoryBreadcrumbs.current.name}`
                        : ''
                }`}
                breadcrumbs={
                    <>
                        <SalePointsLink />
                        <InnerBreadCrumbs
                            links={data}
                            route={'sale_point'}
                        />{' '}
                        {breadcrumbs}
                    </>
                }
            >
                {children}
            </OneEntityBaseHeader>
        </>
    );
};

export default SalePointEditHeader;
