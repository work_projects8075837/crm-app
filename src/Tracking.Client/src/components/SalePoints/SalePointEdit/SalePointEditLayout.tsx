import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import SalePointEditFormProvider from '../Providers/SalePointEditFormProvider';
import SalePointEditHeader from './SalePointEditHeader/SalePointEditHeader';
import SalePointEditOptions from './SalePointEditOptions/SalePointEditOptions';
import SalePointEditTabs from './SalePointEditTabs/SalePointEditTabs';

interface SalePointEditLayoutProps {
    children: React.ReactNode;
    headerButton?: React.ReactNode;
    breadcrumbs?: React.ReactNode;
}

const SalePointEditLayout: React.FC<SalePointEditLayoutProps> = ({
    children,
    headerButton,
    breadcrumbs,
}) => {
    return (
        <>
            <SalePointEditFormProvider>
                <SalePointEditHeader breadcrumbs={breadcrumbs}>
                    {headerButton}
                </SalePointEditHeader>
                <SalePointEditOptions />
                <div className={style.formTabsButtons}>
                    <SalePointEditTabs />
                </div>
                <div className={style.afterTabsMargin}>{children}</div>
            </SalePointEditFormProvider>
        </>
    );
};

export default SalePointEditLayout;
