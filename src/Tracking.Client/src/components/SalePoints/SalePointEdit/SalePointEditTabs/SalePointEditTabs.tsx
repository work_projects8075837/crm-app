import CircleNavLink from '@/components/Elements/CircleNavLink/CircleNavLink';
import { useSalePointByParams } from '@/store/queries/stores/sale_point/useSalePointByParams';
import React from 'react';

const SalePointEditTabs: React.FC = () => {
    const { data } = useSalePointByParams();
    return (
        <>
            <CircleNavLink
                text='Общая информация'
                to={`/sale_point/single/${data?.id}/`}
            />

            <CircleNavLink
                text='Оборудование'
                to={`/sale_point/single/${data?.id}/inventory/`}
            />

            <CircleNavLink
                text='Подключения'
                to={`/sale_point/single/${data?.id}/connections/`}
            />

            <CircleNavLink
                text='Регламент'
                to={`/sale_point/single/${data?.id}/regulations/`}
            />
        </>
    );
};

export default SalePointEditTabs;
