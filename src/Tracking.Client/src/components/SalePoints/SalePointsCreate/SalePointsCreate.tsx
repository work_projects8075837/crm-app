import Layout from '@/components/NavbarLayout/Layout';
import React from 'react';

import SalePointCreateForm from '@/components/Form/SalePointCreateForm/SalePointCreateForm';
import AddTagsModal from '@/components/Tickets/TicketCreate/AddTagsModal/AddTagsModal';
import style from '../../../style/layout/sale_point.module.scss';
import ChangeContactModal from '../ChangeContactModal/ChangeContactModal';
import CounterPartySelectModal from '../CounterPartySelectModal/CounterPartySelectModal';
import CreateShortCounterPartyModal from '../CreateShortCounterPartyModal/CreateShortCounterPartyModal';
import SalePointsFilesModal from '../SalePointsFilesModal/SalePointsFilesModal';
import CreateFormContactModal from '../SelectContactModal/CreateFormContactModal/CreateFormContactModal';
import SelectContactModal from '../SelectContactModal/SelectContactModal';
import SelectPhoneNumberModal from '../SelectPhoneNumberModal/SelectPhoneNumberModal';
import SalePointsCreateElements from './SalePointsCreateElements';
import { salePointsFilesOpen } from './SalePointsCreateOptions/SalePointsCreateOptions';

const SalePointsCreate: React.FC = () => {
    return (
        <>
            <Layout childrenClass={style.formLayout}>
                <SalePointCreateForm
                    modals={
                        <>
                            <ChangeContactModal />
                            <SelectContactModal />
                            <CreateFormContactModal />
                            <SalePointsFilesModal
                                openHandler={salePointsFilesOpen}
                            />
                            <CounterPartySelectModal />
                            <CreateShortCounterPartyModal />
                            <AddTagsModal />
                            <SelectPhoneNumberModal />
                        </>
                    }
                >
                    <SalePointsCreateElements />
                </SalePointCreateForm>
            </Layout>
        </>
    );
};

export default SalePointsCreate;
