import EntityOptionsLayout from '@/components/Tickets/TicketOptions/EntityOptionsLayout';
import TicketOptionButton from '@/components/Tickets/TicketOptions/TicketOptionButton';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import ParentEntityExceptProvider from '../../Providers/ParentEntityExceptProvider/ParentEntityExceptProvider';

export const salePointsFilesOpen = new OpenHandler(false);

const SalePointsCreateOptions: React.FC = () => {
    return (
        <>
            <EntityOptionsLayout>
                <ParentEntityExceptProvider>
                    <TicketOptionButton
                        onClick={() => salePointsFilesOpen.open()}
                        symbol='ticket_file'
                        text='Файлы'
                        svgSize={{ width: 9, height: 13 }}
                    />
                </ParentEntityExceptProvider>
            </EntityOptionsLayout>
        </>
    );
};

export default SalePointsCreateOptions;
