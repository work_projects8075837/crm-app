import React from 'react';
import SalePointsCreateFormProvider from '../Providers/SalePointsCreateFormProvider/SalePointsCreateFormProvider';

import SalePointsCreateElements from './SalePointsCreateElements';

const SalePointsCreateFromDirectory: React.FC = () => {
    return (
        <>
            <SalePointsCreateFormProvider>
                <SalePointsCreateElements />
            </SalePointsCreateFormProvider>
        </>
    );
};

export default SalePointsCreateFromDirectory;
