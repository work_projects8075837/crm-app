import React from 'react';
import SalePointsCreateHeader from './SalePointsCreateHeader/SalePointsCreateHeader';
import SalePointsCreateOptions from './SalePointsCreateOptions/SalePointsCreateOptions';
import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import SalePointGeneral from '../SalePointGeneral/SalePointGeneral';
import style from '../../../style/layout/sale_point.module.scss';

const SalePointsCreateElements: React.FC = () => {
    return (
        <>
            <div className={style.formChildren}>
                <SalePointsCreateHeader />
                <SalePointsCreateOptions />
                <div className={style.formTabsButtons}>
                    <CircleButton isActive={true} text='Общая информация' />
                </div>
                <div className={style.afterTabsMargin}>
                    <SalePointGeneral />
                </div>
            </div>
        </>
    );
};

export default SalePointsCreateElements;
