import React from 'react';
import OneEntityBaseHeader from './OneEntityBaseHeader';
import SalePointsLink from '@/components/Elements/BreadCrumbs/SalePointsLink';
import { useSalePointBreadCrumbs } from '@/store/queries/stores/sale_point/useSalePointBreadCrumbs';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';

const SalePointsCreateHeader: React.FC = () => {
    const { data } = useSalePointBreadCrumbs();
    return (
        <>
            <OneEntityBaseHeader
                title='Новое заведение'
                breadcrumbs={
                    <>
                        <SalePointsLink />{' '}
                        <InnerBreadCrumbs links={data} route='sale_point' /> /
                        Новая заведение
                    </>
                }
            />
        </>
    );
};

export default SalePointsCreateHeader;
