import React from 'react';
import style from '../../../../style/layout/header.module.scss';
import HeaderLayout from '@/components/Layouts/HeaderLayout/HeaderLayout';
import TicketsLink from '@/components/Elements/BreadCrumbs/TicketsLink';

interface OneEntityBaseHeaderProps {
    children?: React.ReactNode;
    breadcrumbs?: string | React.ReactNode;
    title?: string;
}

const OneEntityBaseHeader: React.FC<OneEntityBaseHeaderProps> = ({
    children,
    title,
    breadcrumbs,
}) => {
    return (
        <>
            <HeaderLayout
                className={style.smallHeaderMargin}
                title={title}
                breadcrumbs={<span>{breadcrumbs}</span>}
            >
                {children}
            </HeaderLayout>
        </>
    );
};

export default OneEntityBaseHeader;
