import { SALE_POINT_COUNTER_PARTIES_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import EntitiesFormCardsLayout from '@/components/Layouts/EntitiesFormCardsLayout/EntitiesFormCardsLayout';
import { ICounterparty } from '@/services/counterparty/types';
import React from 'react';
import style from '../../../style/layout/sale_point_table.module.scss';
import { counterPartySelectModal } from '../CounterPartySelectModal/CounterPartySelectModal';
import SalePointCounterPartyItem from './SalePointCounterPartyItem';

const SalePointCounterPartiesList: React.FC = () => {
    const { entities } = useFormEntityValueArray<ICounterparty>(
        SALE_POINT_COUNTER_PARTIES_NAME,
    );
    return (
        <>
            <EntitiesFormCardsLayout
                childrenClass={style.oneFieldEntityCardHeight}
                onPlusClick={() => counterPartySelectModal.open()}
            >
                {entities?.map((c) => (
                    <SalePointCounterPartyItem key={c.id} {...c} />
                ))}
            </EntitiesFormCardsLayout>
        </>
    );
};

export default SalePointCounterPartiesList;
