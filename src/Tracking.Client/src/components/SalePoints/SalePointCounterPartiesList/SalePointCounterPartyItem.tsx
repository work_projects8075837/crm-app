import CardTextIcon from '@/components/Elements/CardTextIcon/CardTextIcon';
import { SALE_POINT_COUNTER_PARTIES_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import SelectedEntityCardLayout from '@/components/Fields/SelectedEntityCard/SelectedEntityCardLayout';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import React from 'react';

interface SalePointCounterPartyItemProps {
    id: string;
    name: string;
    tin: string;
    tag: string;
}

const SalePointCounterPartyItem: React.FC<SalePointCounterPartyItemProps> = ({
    name,
    tin,
    tag,
    id,
}) => {
    const { remove } = useFormEntityValueArray(SALE_POINT_COUNTER_PARTIES_NAME);
    return (
        <>
            <SelectedEntityCardLayout
                id={id}
                linkName='counterparty'
                title='Контрагент'
                valueName={name}
                closeBtn={
                    <CloseCrossButton
                        onClick={(event) => {
                            event.stopPropagation();
                            remove(id);
                        }}
                    />
                }
            >
                <FullExtraCardFields
                    first={{
                        icon: <CardTextIcon text='ИНН:' />,
                        text: tin,
                    }}
                    second={{
                        icon: <CardTextIcon text='ОГРН:' />,
                        text: tag,
                    }}
                />
            </SelectedEntityCardLayout>
        </>
    );
};

export default SalePointCounterPartyItem;
