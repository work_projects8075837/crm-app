import React from 'react';
import SalePointFieldIcon from '../SalePointFieldIcon/SalePointFieldIcon';
import { useWatch } from 'react-hook-form';
import { SALE_POINT_EMAIL_NAME } from '@/components/Fields/EntitiesFields/sale_point';

const SalePointEmailIcon: React.FC = () => {
    const email = useWatch({ name: SALE_POINT_EMAIL_NAME });
    return (
        <>
            <SalePointFieldIcon
                href={`mailto:${email}`}
                symbol='blue_message'
            />
        </>
    );
};

export default SalePointEmailIcon;
