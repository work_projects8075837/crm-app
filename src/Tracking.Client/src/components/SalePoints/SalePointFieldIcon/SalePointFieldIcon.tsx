import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/forms/field.module.scss';

interface SalePointFieldIconProps
    extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
    symbol: string;
}

const SalePointFieldIcon: React.FC<SalePointFieldIconProps> = ({
    symbol,
    ...linkProps
}) => {
    return (
        <>
            <a {...linkProps}>
                <Svg symbol={symbol} className={style.fieldEntityIcon} />
            </a>
        </>
    );
};

export default SalePointFieldIcon;
