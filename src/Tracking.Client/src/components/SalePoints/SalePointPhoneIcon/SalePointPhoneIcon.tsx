import React from 'react';
import SalePointFieldIcon from '../SalePointFieldIcon/SalePointFieldIcon';
import { useWatch } from 'react-hook-form';
import { SALE_POINT_PHONE_NAME } from '@/components/Fields/EntitiesFields/sale_point';

const SalePointPhoneIcon: React.FC = () => {
    const phone = useWatch({ name: SALE_POINT_PHONE_NAME });
    return (
        <>
            <SalePointFieldIcon
                href={`tel:${phone?.replace(/[^0-9]/g, '')}`}
                symbol='blue_phone'
            />
        </>
    );
};

export default SalePointPhoneIcon;
