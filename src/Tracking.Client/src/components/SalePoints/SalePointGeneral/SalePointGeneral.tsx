import FormSelect from '@/components/Elements/FormSelect/FormSelect';
import AnyEmailShortInput from '@/components/Fields/AnyEmailShortInput/AnyEmailShortInput';
import CommentField from '@/components/Fields/CommentField/CommentField';
import {
    SALE_POINT_ADDRESS_NAME,
    SALE_POINT_EMAIL_NAME,
    SALE_POINT_NAME_NAME,
    SALE_POINT_PHONE_NAME,
} from '@/components/Fields/EntitiesFields/sale_point';
import FormShortInput from '@/components/Fields/FormShortInput/FormShortInput';
import EntityFieldLayout from '@/components/Fields/Layouts/EntityFieldLayout';
import PhoneShortInput from '@/components/Fields/PhoneShortInput/PhoneShortInput';
import SalePointSelect from '@/components/Fields/SalePointSelect/SalePointSelect';
import SelectSalePointManager from '@/components/Fields/SelectManager/SelectManager';
import SelectServiceStatus from '@/components/Fields/SelectServiceStatus/SelectServiceStatus';
import ServiceFinishInput from '@/components/Fields/ServiceFinishInput/ServiceFinishInput';
import DoubleColumnsLayout from '@/components/Layouts/DoubleColumnsLayout/DoubleColumnsLayout';
import TagsList from '@/components/Tickets/TagsList/TagsList';
import React from 'react';
import ParentEntityExceptProvider from '../Providers/ParentEntityExceptProvider/ParentEntityExceptProvider';
import SalePointContactsList from '../SalePointContactsList/SalePointContactsList';
import SalePointCounterPartiesList from '../SalePointCounterPartiesList/SalePointCounterPartiesList';
import SalePointEmailIcon from '../SalePointEmailIcon/SalePointEmailIcon';
import SalePointPhoneIcon from '../SalePointPhoneIcon/SalePointPhoneIcon';
import SalePointPhoneNumberList from '../SalePointPhoneNumberList/SalePointPhoneNumberList';

// interface SalePointGeneralProps {}

const SalePointGeneral: React.FC = () => {
    return (
        <>
            <DoubleColumnsLayout
                left={
                    <>
                        <FormSelect title='Основное'>
                            <EntityFieldLayout title='Название'>
                                <FormShortInput
                                    name={SALE_POINT_NAME_NAME}
                                    placeholder='Название'
                                />
                            </EntityFieldLayout>

                            <SalePointSelect
                                onlyDirectory={true}
                                title='Папка заведений (сеть)'
                            />

                            <ParentEntityExceptProvider>
                                <EntityFieldLayout title='Телефон заведения'>
                                    <PhoneShortInput
                                        name={SALE_POINT_PHONE_NAME}
                                        placeholder='Телефон'
                                    />
                                    <SalePointPhoneIcon />
                                </EntityFieldLayout>

                                <EntityFieldLayout title='Электронная почта'>
                                    <AnyEmailShortInput
                                        name={SALE_POINT_EMAIL_NAME}
                                        placeholder='Электронная почта'
                                        required={false}
                                    />
                                    <SalePointEmailIcon />
                                </EntityFieldLayout>

                                <EntityFieldLayout title='Адрес'>
                                    <FormShortInput
                                        name={SALE_POINT_ADDRESS_NAME}
                                        placeholder='Адрес'
                                    />
                                </EntityFieldLayout>

                                <SelectSalePointManager />

                                <SelectServiceStatus />

                                <EntityFieldLayout title='Конец обслуживания'>
                                    <ServiceFinishInput />
                                </EntityFieldLayout>
                            </ParentEntityExceptProvider>
                        </FormSelect>

                        <ParentEntityExceptProvider>
                            <FormSelect title='Контрагенты'>
                                <SalePointCounterPartiesList />
                            </FormSelect>

                            <FormSelect title='Модули'>
                                <TagsList />
                            </FormSelect>
                        </ParentEntityExceptProvider>
                    </>
                }
                right={
                    <>
                        <ParentEntityExceptProvider>
                            <FormSelect title='Телефоны филиала'>
                                <SalePointPhoneNumberList />
                            </FormSelect>

                            <FormSelect title='Контакты филиала'>
                                <SalePointContactsList />
                            </FormSelect>

                            <FormSelect title='Комментарий к филиалу'>
                                <CommentField />
                            </FormSelect>
                        </ParentEntityExceptProvider>
                    </>
                }
            />
        </>
    );
};

export default SalePointGeneral;
