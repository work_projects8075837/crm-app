import {
    ISalePointFormContact,
    SALE_POINT_CONTACTS_NAME,
} from '@/components/Fields/EntitiesFields/sale_point';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import EntitiesFormCardsLayout from '@/components/Layouts/EntitiesFormCardsLayout/EntitiesFormCardsLayout';
import React from 'react';
import { selectContactModalOpen } from '../SelectContactModal/SelectContactModal';
import SalePointContactsItem from './SalePointContactsItem';

const SalePointContactsList: React.FC = () => {
    const { entities } = useFormEntityValueArray<ISalePointFormContact>(
        SALE_POINT_CONTACTS_NAME,
    );

    return (
        <EntitiesFormCardsLayout
            onPlusClick={() => selectContactModalOpen.open()}
        >
            {entities?.map((contact) => (
                <SalePointContactsItem key={contact.id} {...contact} />
            ))}
        </EntitiesFormCardsLayout>
    );
};

export default SalePointContactsList;
