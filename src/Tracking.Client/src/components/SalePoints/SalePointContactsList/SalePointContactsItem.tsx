import CardEmailIcon from '@/components/Elements/CardEmailIcon/CardEmailIcon';
import CardPhoneIcon from '@/components/Elements/CardPhoneIcon/CardPhoneIcon';
import {
    ISalePointFormContact,
    SALE_POINT_CONTACTS_NAME,
} from '@/components/Fields/EntitiesFields/sale_point';
import CardTitleValue from '@/components/Fields/SelectedEntityCard/CardTitleValue';
import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import SelectedEntityCardLayout from '@/components/Fields/SelectedEntityCard/SelectedEntityCardLayout';
import SelectedEntityNavbarLayout from '@/components/Fields/SelectedEntityCard/SelectedEntityNavbar/SelectedEntityNavbarLayout';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import { formatPhoneLink } from '@/components/utils/formatPhoneLink';
import React from 'react';
import {
    changeContactOpen,
    editableContact,
} from '../ChangeContactModal/ChangeContactModal';
import { useDeleteContactPosition } from './useDeleteContactPosition';

type SalePointContactsItemProps = ISalePointFormContact;

const SalePointContactsItem: React.FC<SalePointContactsItemProps> = ({
    contact_id,
    phone_number,
    position_name,
    name,
    email,
    position,
    id,
    is_responsible,
}) => {
    const { remove, update } = useFormEntityValueArray<ISalePointFormContact>(
        SALE_POINT_CONTACTS_NAME,
    );

    const { mutate } = useDeleteContactPosition();

    return (
        <>
            <SelectedEntityCardLayout
                onClick={() => {
                    editableContact.setValue({
                        id,
                        position_name,
                        is_responsible,
                        contact: {
                            id: contact_id,
                            email,
                            name,
                            phone_number,
                            position,
                        },
                    });
                    changeContactOpen.open();
                }}
                id={id}
                isSelected={is_responsible}
                title={is_responsible ? 'Старший по точке' : 'Контакт'}
                valueName={name}
                closeBtn={
                    <CloseCrossButton
                        onClick={(event) => {
                            event.stopPropagation();
                            remove(id);
                            if (!id.includes('create')) {
                                mutate(id);
                            }
                        }}
                    />
                }
                navbar={
                    <SelectedEntityNavbarLayout>
                        <CardTitleValue
                            title={'Должность'}
                            value={position_name}
                        />
                    </SelectedEntityNavbarLayout>
                }
            >
                <FullExtraCardFields
                    first={{
                        icon: <CardPhoneIcon />,
                        text: (
                            <>
                                {phone_number?.map((p, index) => {
                                    return (
                                        <a
                                            key={p.id}
                                            target='_blank'
                                            href={`tel:${formatPhoneLink(
                                                p.name,
                                            )}`}
                                            rel='noreferrer'
                                        >
                                            {p.name}
                                            {!(
                                                index ===
                                                phone_number.length - 1
                                            ) && ','}
                                        </a>
                                    );
                                })}
                            </>
                        ),
                    }}
                    third={{ icon: <CardEmailIcon />, text: email }}
                />
            </SelectedEntityCardLayout>
        </>
    );
};

export default SalePointContactsItem;
