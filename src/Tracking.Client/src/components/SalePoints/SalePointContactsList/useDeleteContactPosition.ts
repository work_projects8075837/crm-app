import { contactPositionApi } from '@/services/contact_position/contact_position';
import { useMutation } from '@tanstack/react-query';

export const useDeleteContactPosition = () => {
    const m = useMutation({
        mutationFn: async (id: string) => {
            return await contactPositionApi.delete(id);
        },
    });

    return m;
};
