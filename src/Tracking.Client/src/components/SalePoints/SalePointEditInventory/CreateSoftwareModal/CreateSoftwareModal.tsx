import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import {
    SOFTWARE_NAME_NAME,
    SOFTWARE_VERSION_NAME,
} from '@/components/Fields/EntitiesFields/software';
import CreateSoftwareForm from '@/components/Form/CreateSoftwareForm/CreateSoftwareForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';

export const softwareModalOpen = new OpenHandler(false);

const CreateSoftwareModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                openHandler={softwareModalOpen}
                title='Добавть обеспечение'
            >
                <CreateSoftwareForm openHandler={softwareModalOpen}>
                    <FormInput
                        name={SOFTWARE_NAME_NAME}
                        placeholder='Название'
                    />

                    <FormInput
                        name={SOFTWARE_VERSION_NAME}
                        placeholder='Версия'
                    />
                </CreateSoftwareForm>
            </ModalWindow>
        </>
    );
};

export default CreateSoftwareModal;
