import CurrentSalePointInventoryLink from '@/components/Elements/BreadCrumbs/CurrentSalePointInventoryLink';
import CurrentSalePointLink from '@/components/Elements/BreadCrumbs/CurrentSalePointLink';
import EquipmentLink from '@/components/Elements/BreadCrumbs/EquipmentLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import SalePointsLink from '@/components/Elements/BreadCrumbs/SalePointsLink';
import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import React from 'react';
import { useParams } from 'react-router-dom';
import OneEntityBaseHeader from '../../SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';

const InventoryEditHeader: React.FC = () => {
    const { data, current } = useInventoryBreadCrumbs();
    const { salePointId } = useParams();
    return (
        <>
            <OneEntityBaseHeader
                title={current?.name}
                breadcrumbs={
                    <>
                        {salePointId && (
                            <>
                                <SalePointsLink /> / <CurrentSalePointLink /> /{' '}
                            </>
                        )}
                        {salePointId ? (
                            <CurrentSalePointInventoryLink />
                        ) : (
                            <EquipmentLink />
                        )}{' '}
                        <InnerBreadCrumbs
                            links={data?.slice(0, -1)}
                            route={`${
                                salePointId
                                    ? `sale_point/single/${salePointId}/`
                                    : ''
                            }inventory`}
                        />
                        / {current?.name}
                    </>
                }
            />
        </>
    );
};

export default InventoryEditHeader;
