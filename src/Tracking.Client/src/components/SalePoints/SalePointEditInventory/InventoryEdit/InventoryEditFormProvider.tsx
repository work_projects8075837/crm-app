import Loader from '@/Loader/Loader';
import Layout from '@/components/NavbarLayout/Layout';
import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import { useInventoryByParams } from '@/store/queries/stores/inventory/useInventoryByParams';
import React from 'react';
import style from '../../../../style/layout/layout.module.scss';
import CreateConnectionModal from '../CreateConnectionModal/CreateConnectionModal';
import CreateSoftwareModal from '../CreateSoftwareModal/CreateSoftwareModal';
import SelectSoftwareModal from '../SelectSoftwareModal/SelectSoftwareModal';
import UpdateInventoryForm from './../../../Form/UpdateInventoryForm/UpdateInventoryForm';

interface InventoryEditFormProviderProps {
    children: React.ReactNode;
}

const InventoryEditFormProvider: React.FC<InventoryEditFormProviderProps> = ({
    children,
}) => {
    const { data } = useInventoryByParams();
    const { isLoading } = useInventoryBreadCrumbs({ refetchOnMount: true });
    return (
        <>
            {data && !isLoading ? (
                <Layout childrenClass={style.formLayout}>
                    <UpdateInventoryForm
                        defaultValues={data}
                        modals={
                            <>
                                <CreateSoftwareModal />
                                <CreateConnectionModal />
                                <SelectSoftwareModal />
                            </>
                        }
                    >
                        {children}
                    </UpdateInventoryForm>
                </Layout>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default InventoryEditFormProvider;
