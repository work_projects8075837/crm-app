import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import React from 'react';
import style from '../../../../style/layout/layout.module.scss';
import InventoryGeneral from '../InventoryGeneral/InventoryGeneral';
import InventoryEditFormProvider from './InventoryEditFormProvider';
import InventoryEditHeader from './InventoryEditHeader';

const InventoryEdit: React.FC = () => {
    return (
        <>
            <InventoryEditFormProvider>
                <InventoryEditHeader />

                <div className={style.formTabsButtons}>
                    <CircleButton isActive={true} text='Общая информация' />
                </div>
                <div className={style.afterTabsMargin}>
                    <InventoryGeneral />
                </div>
            </InventoryEditFormProvider>
        </>
    );
};

export default InventoryEdit;
