import CurrentSalePointConnectionsLink from '@/components/Elements/BreadCrumbs/CurrentSalePointConnectionsLink';
import CurrentSalePointInventoryLink from '@/components/Elements/BreadCrumbs/CurrentSalePointInventoryLink';
import CurrentSalePointLink from '@/components/Elements/BreadCrumbs/CurrentSalePointLink';
import EquipmentLink from '@/components/Elements/BreadCrumbs/EquipmentLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import SalePointsLink from '@/components/Elements/BreadCrumbs/SalePointsLink';
import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import { useQueryParams } from '@/hooks/useQueryParams';
import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import React from 'react';
import { useParams } from 'react-router-dom';
import style from '../../../../style/layout/layout.module.scss';
import OneEntityBaseHeader from '../../SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';
import InventoryGeneral from '../InventoryGeneral/InventoryGeneral';

const InventoryCreateElements: React.FC = () => {
    const { data } = useInventoryBreadCrumbs();
    const { salePointId } = useParams();
    const { is_connection } = useQueryParams();
    return (
        <>
            <OneEntityBaseHeader
                title='Новое оборудование'
                breadcrumbs={
                    <>
                        {salePointId && (
                            <>
                                {' '}
                                <SalePointsLink /> / <CurrentSalePointLink /> /{' '}
                            </>
                        )}
                        {salePointId ? (
                            <>
                                {is_connection ? (
                                    <CurrentSalePointConnectionsLink />
                                ) : (
                                    <CurrentSalePointInventoryLink />
                                )}{' '}
                            </>
                        ) : (
                            <>
                                <EquipmentLink />
                            </>
                        )}
                        <InnerBreadCrumbs
                            links={data}
                            route={`sale_point/single/${salePointId}/inventory`}
                        />{' '}
                        / Новое оборудование
                    </>
                }
            />

            <div className={style.formTabsButtons}>
                <CircleButton isActive={true} text='Общая информация' />
            </div>
            <div className={style.afterTabsMargin}>
                <InventoryGeneral />
            </div>
        </>
    );
};

export default InventoryCreateElements;
