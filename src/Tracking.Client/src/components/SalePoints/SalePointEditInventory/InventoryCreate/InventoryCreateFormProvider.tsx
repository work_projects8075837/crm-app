import Loader from '@/Loader/Loader';
import CreateInventoryToSalePointForm from '@/components/Form/CreateInventoryToSalePointForm/CreateInventoryToSalePointForm';
import Layout from '@/components/NavbarLayout/Layout';
import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import { useInventoryByParams } from '@/store/queries/stores/inventory/useInventoryByParams';
import React from 'react';
import style from '../../../../style/layout/layout.module.scss';
import CreateConnectionModal from '../CreateConnectionModal/CreateConnectionModal';
import CreateSoftwareModal from '../CreateSoftwareModal/CreateSoftwareModal';
import SelectSoftwareModal from '../SelectSoftwareModal/SelectSoftwareModal';

interface InventoryCreateFormProviderProps {
    children: React.ReactNode;
}

const InventoryCreateFormProvider: React.FC<
    InventoryCreateFormProviderProps
> = ({ children }) => {
    const { data } = useInventoryByParams();
    const { isLoading } = useInventoryBreadCrumbs({ refetchOnMount: true });
    return (
        <>
            {data && !isLoading ? (
                <Layout childrenClass={style.formLayout}>
                    <CreateInventoryToSalePointForm
                        parentInventory={data}
                        modals={
                            <>
                                <CreateSoftwareModal />
                                <CreateConnectionModal />
                                <SelectSoftwareModal />
                            </>
                        }
                    >
                        {children}
                    </CreateInventoryToSalePointForm>
                </Layout>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default InventoryCreateFormProvider;
