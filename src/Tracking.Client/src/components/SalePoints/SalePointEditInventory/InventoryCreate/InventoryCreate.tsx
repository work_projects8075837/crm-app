import CreateInventoryToSalePointForm from '@/components/Form/CreateInventoryToSalePointForm/CreateInventoryToSalePointForm';
import Layout from '@/components/NavbarLayout/Layout';
import React from 'react';
import style from '../../../../style/layout/layout.module.scss';
import CreateConnectionModal from '../CreateConnectionModal/CreateConnectionModal';
import CreateSoftwareModal from '../CreateSoftwareModal/CreateSoftwareModal';
import SelectSoftwareModal from '../SelectSoftwareModal/SelectSoftwareModal';
import InventoryCreateElements from './InventoryCreateElements';

const InventoryCreate: React.FC = () => {
    return (
        <>
            <Layout childrenClass={style.formLayout}>
                <CreateInventoryToSalePointForm
                    modals={
                        <>
                            <CreateSoftwareModal />
                            <CreateConnectionModal />
                            <SelectSoftwareModal />
                        </>
                    }
                >
                    <InventoryCreateElements />
                </CreateInventoryToSalePointForm>
            </Layout>
        </>
    );
};

export default InventoryCreate;
