import React from 'react';
import InventoryCreateElements from './InventoryCreateElements';
import InventoryCreateFormProvider from './InventoryCreateFormProvider';

const InventoryDirectoryCreate: React.FC = () => {
    return (
        <>
            <InventoryCreateFormProvider>
                <InventoryCreateElements />
            </InventoryCreateFormProvider>
        </>
    );
};

export default InventoryDirectoryCreate;
