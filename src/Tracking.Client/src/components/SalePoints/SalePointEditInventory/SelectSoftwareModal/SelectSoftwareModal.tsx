import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import SoftwareSearchSelect from '@/components/Fields/SoftwareSearchSelect/SoftwareSearchSelect';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../../style/layout/modal_window.module.scss';

export const selectSoftwareModal = new OpenHandler(false);

const SelectSoftwareModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                title='Выбрать версию'
                openHandler={selectSoftwareModal}
            >
                <div className={style.modalVert}>
                    <SoftwareSearchSelect />
                </div>
            </ModalWindow>
        </>
    );
};

export default SelectSoftwareModal;
