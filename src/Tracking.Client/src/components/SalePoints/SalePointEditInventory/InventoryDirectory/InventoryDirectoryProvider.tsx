import Loader from '@/Loader/Loader';
import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import React from 'react';

interface InventoryDirectoryProviderProps {
    children: React.ReactNode;
}

const InventoryDirectoryProvider: React.FC<InventoryDirectoryProviderProps> = ({
    children,
}) => {
    const { isLoading } = useInventoryBreadCrumbs({ refetchOnMount: true });
    return <>{isLoading ? <Loader /> : children}</>;
};

export default InventoryDirectoryProvider;
