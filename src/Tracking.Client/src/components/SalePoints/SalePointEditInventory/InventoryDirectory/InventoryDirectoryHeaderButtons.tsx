import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import React from 'react';
import { useParams } from 'react-router-dom';
import DirectoryHeaderButtons from '../../SalePointsHeader/DirectoryHeaderButtons';

const InventoryDirectoryHeaderButtons: React.FC = () => {
    const { salePointId } = useParams();
    const { current } = useInventoryBreadCrumbs();
    return (
        <>
            <DirectoryHeaderButtons
                link={`/sale_point/single/${salePointId}/inventory/create/${current?.id}`}
                entityText='Добавить инвентарь'
                directoryText='Добавить папку инвентаря'
            />
        </>
    );
};

export default InventoryDirectoryHeaderButtons;
