import React from 'react';
import SalePointEditLayout from '../../SalePointEdit/SalePointEditLayout';
import SalePointInventoryList from '../SalePointInventoryList/SalePointInventoryList';
import InventoryDirectoryBreadCrumbs from './InventoryDirectoryHeader';
import InventoryDirectoryHeaderButtons from './InventoryDirectoryHeaderButtons';
import InventoryDirectoryProvider from './InventoryDirectoryProvider';

const InventoryDirectory: React.FC = () => {
    return (
        <>
            <InventoryDirectoryProvider>
                <SalePointEditLayout
                    breadcrumbs={<InventoryDirectoryBreadCrumbs />}
                    headerButton={
                        <>
                            <InventoryDirectoryHeaderButtons />
                        </>
                    }
                >
                    <SalePointInventoryList />
                </SalePointEditLayout>
            </InventoryDirectoryProvider>
        </>
    );
};

export default InventoryDirectory;
