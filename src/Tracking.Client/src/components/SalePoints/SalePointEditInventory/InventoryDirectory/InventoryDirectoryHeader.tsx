import CurrentSalePointInventoryLink from '@/components/Elements/BreadCrumbs/CurrentSalePointInventoryLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import React from 'react';
import { useParams } from 'react-router-dom';

const InventoryDirectoryBreadCrumbs: React.FC = () => {
    const { data } = useInventoryBreadCrumbs();
    const { salePointId } = useParams();
    return (
        <>
            / <CurrentSalePointInventoryLink />{' '}
            <InnerBreadCrumbs
                links={data}
                route={`sale_point/single/${salePointId}/inventory`}
            />
        </>
    );
};

export default InventoryDirectoryBreadCrumbs;
