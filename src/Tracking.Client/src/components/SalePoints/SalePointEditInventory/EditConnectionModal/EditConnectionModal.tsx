import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import SelectConnectionType, {
    connectionTypes,
} from '@/components/Fields/SelectConnectionType/SelectConnectionType';
import UpdateConnectionForm, {
    updatableConnection,
} from '@/components/Form/UpdateConnectionForm/UpdateConnectionForm';
import { OpenHandler } from '@/store/states/OpenHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';
import ConnectionDataFields from '../CreateConnectionModal/ConnectionDataFields';

export const editConnectionOpen = new OpenHandler();

const EditConnectionModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                openHandler={editConnectionOpen}
                title={updatableConnection.value?.type}
            >
                {updatableConnection.value && (
                    <UpdateConnectionForm
                        openHandler={editConnectionOpen}
                        defaultValues={{
                            type: connectionTypes.find(
                                (t) =>
                                    t.type === updatableConnection.value?.type,
                            ),
                            ...updatableConnection.value.configuration[0],
                        }}
                    >
                        <SelectConnectionType />
                        <ConnectionDataFields />
                    </UpdateConnectionForm>
                )}
            </ModalWindow>
        </>
    );
};

export default observer(EditConnectionModal);
