import React from 'react';
import { useParams } from 'react-router-dom';
import SalePointEditLayout from '../SalePointEdit/SalePointEditLayout';
import DirectoryHeaderButtons from '../SalePointsHeader/DirectoryHeaderButtons';
import SalePointInventoryList from './SalePointInventoryList/SalePointInventoryList';

const SalePointEditInventory: React.FC = () => {
    const { salePointId } = useParams();
    return (
        <>
            <SalePointEditLayout
                headerButton={
                    <>
                        <DirectoryHeaderButtons
                            link={`/sale_point/single/${salePointId}/inventory/create/`}
                            entityText='Добавить оборудование'
                            directoryText='Добавить папку оборудования'
                        />
                    </>
                }
            >
                <SalePointInventoryList />
            </SalePointEditLayout>
        </>
    );
};

export default SalePointEditInventory;
