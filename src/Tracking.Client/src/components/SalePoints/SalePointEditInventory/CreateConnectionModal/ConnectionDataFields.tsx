import FormInput from '@/components/Fields/BaseFields/FormInput';
import Textarea from '@/components/Fields/BaseFields/Textarea';
import {
    ANY_DESK,
    CONNECTION_LOGIN_NAME,
    CONNECTION_OTHER_NAME,
    CONNECTION_PASSWORD_NAME,
    CONNECTION_TYPE_NAME,
    SUPREME,
    TEAM_VIEWER,
} from '@/components/Fields/EntitiesFields/connection';
import PasswordInput from '@/components/Fields/PasswordInput/PasswordInput';
import { IConnectionType } from '@/components/Fields/SelectConnectionType/SelectConnectionType';
import { useFormEntityValue } from '@/components/Fields/hooks/useFormEntityValue';
import React from 'react';

const ConnectionDataFields: React.FC = () => {
    const { currentEntity } =
        useFormEntityValue<IConnectionType>(CONNECTION_TYPE_NAME);
    return (
        <>
            {(currentEntity?.type === ANY_DESK ||
                currentEntity?.type === TEAM_VIEWER) && (
                <>
                    <FormInput
                        name={CONNECTION_LOGIN_NAME}
                        placeholder='Логин'
                    />
                    <PasswordInput
                        hard={false}
                        name={CONNECTION_PASSWORD_NAME}
                        placeholder='Пароль'
                    />
                </>
            )}

            {currentEntity?.type === SUPREME && (
                <>
                    <Textarea name={CONNECTION_OTHER_NAME} />
                </>
            )}
        </>
    );
};

export default ConnectionDataFields;
