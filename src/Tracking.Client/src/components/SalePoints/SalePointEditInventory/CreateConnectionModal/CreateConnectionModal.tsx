import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import SelectConnectionType from '@/components/Fields/SelectConnectionType/SelectConnectionType';
import CreateConnectionForm from '@/components/Form/CreateConnectionForm/CreateConnectionForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import ConnectionDataFields from './ConnectionDataFields';

export const connectionModalOpen = new OpenHandler(false);

const CreateConnectionModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                openHandler={connectionModalOpen}
                title='Добавить подключение'
            >
                <CreateConnectionForm openHandler={connectionModalOpen}>
                    <SelectConnectionType />
                    <ConnectionDataFields />
                </CreateConnectionForm>
            </ModalWindow>
        </>
    );
};

export default CreateConnectionModal;
