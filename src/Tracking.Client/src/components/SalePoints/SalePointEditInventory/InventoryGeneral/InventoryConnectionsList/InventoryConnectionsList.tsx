import { INVENTORY_CONNECTIONS_NAME } from '@/components/Fields/EntitiesFields/inventory';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import EntitiesFormCardsLayout from '@/components/Layouts/EntitiesFormCardsLayout/EntitiesFormCardsLayout';
import { IConnection } from '@/services/connection/types';
import React from 'react';
import style from '../../../../../style/layout/inventory_table.module.scss';
import { connectionModalOpen } from '../../CreateConnectionModal/CreateConnectionModal';
import InventoryConnection from './InventoryConnection';

const InventoryConnectionsList: React.FC = () => {
    const { entities } = useFormEntityValueArray<IConnection>(
        INVENTORY_CONNECTIONS_NAME,
    );
    return (
        <>
            <EntitiesFormCardsLayout
                childrenClass={style.oneFieldEntityCardHeight}
                onPlusClick={() => connectionModalOpen.open()}
            >
                {entities?.map((connection) => (
                    <InventoryConnection key={connection.id} {...connection} />
                ))}
            </EntitiesFormCardsLayout>
        </>
    );
};

export default InventoryConnectionsList;
