import CardGrateIcon from '@/components/Elements/CardGrateIcon/CardGrateIcon';
import {
    ANY_DESK,
    SUPREME,
    TEAM_VIEWER,
} from '@/components/Fields/EntitiesFields/connection';
import { INVENTORY_CONNECTIONS_NAME } from '@/components/Fields/EntitiesFields/inventory';
import { connectionTypes } from '@/components/Fields/SelectConnectionType/SelectConnectionType';
import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import SelectedEntityCardLayout from '@/components/Fields/SelectedEntityCard/SelectedEntityCardLayout';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import { connectionApi } from '@/services/connection/connection';
import { IConnection } from '@/services/connection/types';
import React from 'react';

type InventoryConnectionProps = IConnection;

const InventoryConnection: React.FC<InventoryConnectionProps> = ({
    type,
    id,
    configuration,
}) => {
    const { remove } = useFormEntityValueArray(INVENTORY_CONNECTIONS_NAME);
    return (
        <>
            <SelectedEntityCardLayout
                id={id}
                title='Подключение'
                valueName={connectionTypes.find((t) => t.type === type)?.name}
                closeBtn={
                    <CloseCrossButton
                        onClick={() => {
                            connectionApi.delete(id);
                            remove(id);
                        }}
                    />
                }
            >
                {(type === ANY_DESK || type === TEAM_VIEWER) && (
                    <FullExtraCardFields
                        first={{
                            icon: <CardGrateIcon />,
                            text: configuration[0]?.login,
                        }}
                    />
                )}
                {type === SUPREME && (
                    <FullExtraCardFields
                        first={{
                            text: configuration[0]?.other,
                        }}
                    />
                )}
            </SelectedEntityCardLayout>
        </>
    );
};

export default InventoryConnection;
