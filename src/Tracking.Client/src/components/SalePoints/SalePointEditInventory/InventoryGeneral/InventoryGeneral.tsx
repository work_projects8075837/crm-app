import FormSelect from '@/components/Elements/FormSelect/FormSelect';
import {
    INVENTORY_NAME_NAME,
    INVENTORY_SERIAL_NUMBER_NAME,
} from '@/components/Fields/EntitiesFields/inventory';
import FormShortInput from '@/components/Fields/FormShortInput/FormShortInput';
import EntityFieldLayout from '@/components/Fields/Layouts/EntityFieldLayout';
import SelectInventory from '@/components/Fields/SelectInventory/SelectInventory';
import DoubleColumnsLayout from '@/components/Layouts/DoubleColumnsLayout/DoubleColumnsLayout';
import { useQueryParams } from '@/hooks/useQueryParams';
import React from 'react';
import ParentEntityExceptProvider from '../../Providers/ParentEntityExceptProvider/ParentEntityExceptProvider';
import InventoryConnectionsList from './InventoryConnectionsList/InventoryConnectionsList';
import InventorySoftwareList from './InventorySoftwareList/InventorySoftwareList';

const InventoryGeneral: React.FC = () => {
    const { is_connection } = useQueryParams();
    return (
        <>
            <DoubleColumnsLayout
                left={
                    <>
                        <FormSelect title='Основное'>
                            <EntityFieldLayout title='Название'>
                                <FormShortInput
                                    name={INVENTORY_NAME_NAME}
                                    placeholder='Название'
                                />
                            </EntityFieldLayout>

                            {!is_connection && (
                                <SelectInventory onlyDirectory={true} />
                            )}

                            <ParentEntityExceptProvider>
                                <EntityFieldLayout title='Серийный номер'>
                                    <FormShortInput
                                        name={INVENTORY_SERIAL_NUMBER_NAME}
                                        placeholder='Серийный номер'
                                        required={false}
                                    />
                                </EntityFieldLayout>
                            </ParentEntityExceptProvider>
                        </FormSelect>
                        <ParentEntityExceptProvider>
                            {!!is_connection && (
                                <FormSelect title='Обеспечение'>
                                    <InventorySoftwareList />
                                </FormSelect>
                            )}
                        </ParentEntityExceptProvider>
                    </>
                }
                right={
                    <>
                        <ParentEntityExceptProvider>
                            {!!is_connection && (
                                <FormSelect title='Подключения'>
                                    <InventoryConnectionsList />
                                </FormSelect>
                            )}
                            {!is_connection && (
                                <FormSelect title='Обеспечение'>
                                    <InventorySoftwareList />
                                </FormSelect>
                            )}
                        </ParentEntityExceptProvider>
                    </>
                }
            />
        </>
    );
};

export default InventoryGeneral;
