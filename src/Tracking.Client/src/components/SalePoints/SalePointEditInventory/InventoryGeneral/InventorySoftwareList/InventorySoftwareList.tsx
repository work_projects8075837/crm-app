import { INVENTORY_SOFTWARE_NAME } from '@/components/Fields/EntitiesFields/inventory';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import EntitiesFormCardsLayout from '@/components/Layouts/EntitiesFormCardsLayout/EntitiesFormCardsLayout';
import { ISoftware } from '@/services/software/types';
import React from 'react';
import style from '../../../../../style/layout/inventory_table.module.scss';
import { selectSoftwareModal } from '../../SelectSoftwareModal/SelectSoftwareModal';
import InventorySoftware from './InventorySoftware';

const InventorySoftwareList: React.FC = () => {
    const { entities } = useFormEntityValueArray<ISoftware>(
        INVENTORY_SOFTWARE_NAME,
    );
    return (
        <>
            <EntitiesFormCardsLayout
                childrenClass={style.oneFieldEntityCardHeight}
                onPlusClick={() => {
                    selectSoftwareModal.open();
                }}
            >
                {entities?.map((software) => (
                    <InventorySoftware key={software.id} {...software} />
                ))}
            </EntitiesFormCardsLayout>
        </>
    );
};

export default InventorySoftwareList;
