import CardVersionIcon from '@/components/Elements/CardVersionIcon/CardVersionIcon';
import { INVENTORY_SOFTWARE_NAME } from '@/components/Fields/EntitiesFields/inventory';
import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import SelectedEntityCardLayout from '@/components/Fields/SelectedEntityCard/SelectedEntityCardLayout';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import React from 'react';

interface InventorySoftwareProps {
    name: string;
    version: string;
    id: string;
}

const InventorySoftware: React.FC<InventorySoftwareProps> = ({
    id,
    name,
    version,
}) => {
    const { remove } = useFormEntityValueArray(INVENTORY_SOFTWARE_NAME);
    return (
        <>
            <SelectedEntityCardLayout
                id={id}
                title='Подключение'
                valueName={name}
                closeBtn={<CloseCrossButton onClick={() => remove(id)} />}
            >
                <FullExtraCardFields
                    first={{ icon: <CardVersionIcon />, text: version }}
                />
            </SelectedEntityCardLayout>
        </>
    );
};

export default InventorySoftware;
