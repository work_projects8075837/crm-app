import Pagination from '@/components/Tables/Pagination/Pagination';
import PaginationButtonsList from '@/components/Tickets/TiketsPaginationButtonsList/PaginationButtonsList';
import { TInventoryFilter } from '@/services/inventory/types';
import { useInventoryTable } from '@/store/queries/stores/inventory/useInventoryBySalePointTable';
import React from 'react';
import { inventoryPagination } from './SalePointInventoryList';

interface InventoryPaginationProps {
    filters?: TInventoryFilter;
}

const InventoryPagination: React.FC<InventoryPaginationProps> = ({
    filters,
}) => {
    const { data } = useInventoryTable({ ...inventoryPagination, ...filters });
    return (
        <>
            <Pagination
                paginationHandler={inventoryPagination}
                pages={
                    <PaginationButtonsList
                        data={data}
                        paginationHandler={inventoryPagination}
                    />
                }
            />
        </>
    );
};

export default InventoryPagination;
