import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import NullEntities from '@/components/Elements/NullEntities/NullEntities';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { inventoryApi } from '@/services/inventory/inventory';
import { TInventoryFilter } from '@/services/inventory/types';
import {
    GET_ALL_INVENTORIES_KEY,
    GET_SEARCH_INVENTORIES_KEY,
} from '@/store/queries/keys/keys';
import { useInventoryTable } from '@/store/queries/stores/inventory/useInventoryBySalePointTable';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { PaginationHandler } from '@/store/states/PaginationHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import { observer } from 'mobx-react-lite';
import React from 'react';
import SalePointInventoryItem from './SalePointInventoryItem';
import SalePointInventoryTableLayout from './SalePointInventoryTableLayout';

const INVENTORY_PAGINATION = 'INVENTORY_PAGINATION';
export const inventoryPagination = new PaginationHandler(INVENTORY_PAGINATION);

export const inventoryCheck = new CheckEntitiesHandler();

export const inventoryFilters: TInventoryFilter = {
    inventory_is_connection: false,
};

interface SalePointInventoryListProps {
    emptyText?: string;
}

const SalePointInventoryList: React.FC<SalePointInventoryListProps> = ({
    emptyText = 'У данного заведения нет инвентаря',
}) => {
    const { data, refetch } = useInventoryTable(
        { ...inventoryPagination, ...inventoryFilters },
        {
            refetchOnMount: true,
        },
    );
    const onlySoft = data?.data;

    useSetCheckableEntities(inventoryCheck, onlySoft);

    return (
        <>
            {onlySoft ? (
                <>
                    {onlySoft.length ? (
                        <SalePointInventoryTableLayout
                            checkHandler={inventoryCheck}
                            filters={inventoryFilters}
                        >
                            {onlySoft.map((inventory) => (
                                <SalePointInventoryItem
                                    checkHandler={inventoryCheck}
                                    key={inventory.id}
                                    {...inventory}
                                    version={inventory?.software[0]?.version}
                                />
                            ))}
                        </SalePointInventoryTableLayout>
                    ) : (
                        <NullEntities text={emptyText} />
                    )}
                </>
            ) : (
                <LoaderCircle size={60} />
            )}
            <EntitySelectedActions
                keys={[GET_ALL_INVENTORIES_KEY, GET_SEARCH_INVENTORIES_KEY]}
                checkHandler={inventoryCheck}
                service={inventoryApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default observer(SalePointInventoryList);
