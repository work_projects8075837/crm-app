import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import { TInventoryFilter } from '@/services/inventory/types';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import React from 'react';
import style from '../../../../style/layout/inventory_table.module.scss';
import InventoryPagination from './InventoryPagination';

interface SalePointInventoryTableLayoutProps {
    children: React.ReactNode;
}

const inventoryFields = [
    { name: 'Наименование' },
    { name: 'Серийный номер' },
    { name: 'Версия ПО' },
    { name: 'Подключения' },
];

interface SalePointInventoryTableLayoutProps {
    withConnections?: boolean;
    checkHandler: CheckEntitiesHandler;
    filters?: TInventoryFilter;
}

const SalePointInventoryTableLayout: React.FC<
    SalePointInventoryTableLayoutProps
> = ({ children, checkHandler, filters, withConnections = false }) => {
    return (
        <>
            <TableLayout
                layoutClass={style.entityPaginatedTable}
                header={
                    <TableHeader
                        checkHandler={checkHandler}
                        fields={
                            withConnections
                                ? inventoryFields
                                : inventoryFields.slice(0, -1)
                        }
                        layoutClass={style.rowLayout}
                        fieldClass={style.headerField}
                    />
                }
                pagination={<InventoryPagination {...{ filters }} />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default SalePointInventoryTableLayout;
