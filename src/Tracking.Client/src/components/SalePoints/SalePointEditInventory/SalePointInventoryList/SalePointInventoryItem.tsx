import Svg from '@/components/Elements/Svg/Svg';
import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import ServerLink from '@/components/Tickets/InventoryList/ServerLink';
import { IConnection } from '@/services/connection/types';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import style from '../../../../style/layout/inventory_table.module.scss';

interface SalePointInventoryItemProps {
    id: string;
    withConnections?: boolean;
    name: string;
    serial_number?: string;
    version?: string;
    connection: IConnection[];
    is_parent: boolean;
    checkHandler: CheckEntitiesHandler;
}

const SalePointInventoryItem: React.FC<SalePointInventoryItemProps> = ({
    id,
    name,
    serial_number,
    version,
    connection,
    withConnections = false,
    is_parent,
    checkHandler,
}) => {
    const navigate = useNavigate();
    const { salePointId } = useParams();
    return (
        <>
            <div
                className={style.tableItem}
                onDoubleClick={() => {
                    if (withConnections) {
                        navigate(
                            `/sale_point/single/${salePointId}/inventory/single/${id}/?is_connection=1`,
                        );
                    } else {
                        if (salePointId) {
                            navigate(
                                `/sale_point/single/${salePointId}/inventory${
                                    is_parent ? '' : '/single'
                                }/${id}/`,
                            );
                        } else {
                            navigate(
                                `/inventory${
                                    is_parent ? '' : '/single'
                                }/${id}/`,
                            );
                        }
                    }
                }}
            >
                <CheckItem checkHandler={checkHandler} entity={{ id, name }} />
                <div className={style.tableItemName}>
                    <span>{name}</span>
                    {is_parent && (
                        <Svg
                            symbol='directory_in'
                            className={style.directoryArrow}
                        />
                    )}
                </div>

                <div className={style.tableItemField}>
                    <span>{serial_number}</span>
                </div>
                <div className={style.tableItemField}>
                    <span>{version}</span>
                </div>
                {withConnections && (
                    <div className={style.tableItemName}>
                        {connection.map((c) => (
                            <ServerLink
                                key={c.id}
                                connection={c}
                                className={style.connection}
                            />
                        ))}
                    </div>
                )}
            </div>
        </>
    );
};

export default SalePointInventoryItem;
