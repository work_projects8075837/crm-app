import React from 'react';
import SalePointsList from './SalePointsList/SalePointsList';
import Layout from '../NavbarLayout/Layout';
import SalePointsHeader from './SalePointsHeader/SalePointsHeader';

const SalePoints: React.FC = () => {
    return (
        <>
            <Layout>
                <SalePointsHeader />
                <SalePointsList />
            </Layout>
        </>
    );
};

export default SalePoints;
