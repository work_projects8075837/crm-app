import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import { IContact } from '@/services/contact/types';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { formContactModalOpen } from './CreateFormContactModal';

interface SaveSalePointFormContactButtonProps {
    add: <T extends Entity>(entity: T) => void;
}

const SaveSalePointFormContactButton: React.FC<
    SaveSalePointFormContactButtonProps
> = ({ add }) => {
    const { onSubmit, isDisabled } = useSubmit();
    const { getValues } = useFormContext();
    return (
        <>
            <SaveButton
                isLoading={isDisabled}
                onClick={() => {
                    if (onSubmit) {
                        onSubmit((newContact: IContact) => {
                            add({
                                ...newContact,
                                id: `create_${newContact.id}`,
                                contact_id: newContact.id,
                                position_name: getValues('position_name'),
                                is_responsible: getValues('is_responsible'),
                            });
                            formContactModalOpen.close();
                        });
                    }
                }}
            >
                Создать контакт
            </SaveButton>
        </>
    );
};

export default SaveSalePointFormContactButton;
