import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import CheckboxInput from '@/components/Fields/BaseFields/CheckboxInput';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import { SALE_POINT_CONTACTS_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import ContactFields from '@/components/Form/CreateOnlyContactForm/ContactFields';
import CreateOnlyContactForm from '@/components/Form/CreateOnlyContactForm/CreateOnlyContactForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import SaveSalePointFormContactButton from './SaveSalePointFormContactButton';

export const formContactModalOpen = new OpenHandler(false);

const CreateFormContactModal: React.FC = () => {
    const { add } = useFormEntityValueArray(SALE_POINT_CONTACTS_NAME);

    return (
        <>
            <ModalWindow
                title='Добавить контакт'
                openHandler={formContactModalOpen}
            >
                <CreateOnlyContactForm>
                    <FormInput name='position_name' placeholder='Должность' />
                    <ContactFields />
                    <CheckboxInput
                        name='is_responsible'
                        label='Сделать старшим по точке'
                    />
                    <SaveSalePointFormContactButton add={add} />
                </CreateOnlyContactForm>
            </ModalWindow>
        </>
    );
};

export default CreateFormContactModal;
