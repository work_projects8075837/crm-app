import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import CheckboxInput from '@/components/Fields/BaseFields/CheckboxInput';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { SALE_POINT_CONTACTS_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import SearchSelectContact from '@/components/Fields/SearchSelectContact/SearchSelectContact';
import SelectEntityForm from '@/components/Form/SelectEntityForm/SelectEntityForm';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { ISalePointContact } from '@/services/sale_point/types';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import { formContactModalOpen } from './CreateFormContactModal/CreateFormContactModal';

export const selectContactModalOpen = new OpenHandler(false);

export interface TSetContactForm
    extends Omit<Omit<ISalePointContact, 'id'>, 'is_responsible'> {
    id?: string;
}

const SelectContactModal: React.FC = () => {
    const model = useSearchInputModel();
    return (
        <>
            <ModalWindow
                layoutClass={style.wideLayout}
                title='Выбрать контакт'
                openHandler={selectContactModalOpen}
            >
                <SelectEntityForm
                    addMode={true}
                    converter={(data: TSetContactForm) => {
                        return {
                            ...data,
                            ...data.contact,
                            id: data.id || `create_${data.contact.id}`,
                            contact_id: data.contact.id,
                        };
                    }}
                    name={SALE_POINT_CONTACTS_NAME}
                    openHandler={selectContactModalOpen}
                >
                    <FormInput name='position_name' placeholder='Должность' />

                    <SearchSelectContact
                        name='contact'
                        onCreateClick={() => {
                            selectContactModalOpen.close();
                            formContactModalOpen.open();
                        }}
                    />
                    <CheckboxInput
                        name='is_responsible'
                        label='Сделать старшим по точке'
                    />
                    <SaveButton>Сохранить</SaveButton>
                </SelectEntityForm>
            </ModalWindow>
        </>
    );
};

export default SelectContactModal;
