import { SALE_POINT_REGULATIONS_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import LargeEditor from '@/components/Fields/LargeEditor/LargeEditor';
import React from 'react';
import SalePointEditLayout from '../SalePointEdit/SalePointEditLayout';

const SalePointRegulations: React.FC = () => {
    return (
        <>
            <SalePointEditLayout>
                <LargeEditor name={SALE_POINT_REGULATIONS_NAME} />
            </SalePointEditLayout>
        </>
    );
};

export default SalePointRegulations;
