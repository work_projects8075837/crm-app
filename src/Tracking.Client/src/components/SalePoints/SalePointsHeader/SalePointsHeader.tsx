import React from 'react';
import BaseDirectoryHeader from './BaseDirectoryHeader';

const SalePointsHeader: React.FC = () => {
    return (
        <>
            <BaseDirectoryHeader
                title='Список заведений'
                link='/sale_point/create/'
                directoryText='Новая папка заведений'
                entityText='Новое заведение'
            />
        </>
    );
};

export default SalePointsHeader;
