import BlueLink from '@/components/Elements/BlueLink/BlueLink';
import WhiteLink from '@/components/Elements/WhiteLink/WhiteLink';
import React from 'react';
import style from '../../../style/elements/blue_button.module.scss';
import headerStyle from '../../../style/layout/header.module.scss';

interface DirectoryHeaderButtonsProps {
    link: string;
    onEntityClick?: () => void;
    onDirectoryClick?: () => void;
    entityText: string;
    directoryText?: string;
}

const DirectoryHeaderButtons: React.FC<DirectoryHeaderButtonsProps> = ({
    directoryText,
    entityText,
    link,
    onDirectoryClick,
    onEntityClick,
}) => {
    return (
        <>
            <div className={headerStyle.headerChildrenContainerEnd}>
                <div></div>

                <div className={headerStyle.buttons}>
                    <BlueLink
                        className={style.blockStandardWidth}
                        to={link}
                        onClick={onEntityClick}
                    >
                        {entityText}
                    </BlueLink>
                    {directoryText && (
                        <WhiteLink
                            onClick={onDirectoryClick}
                            className={style.blockStandardWidth}
                            to={`${link}?is_parent=1`}
                        >
                            {directoryText}
                        </WhiteLink>
                    )}
                </div>
            </div>
        </>
    );
};

export default DirectoryHeaderButtons;
