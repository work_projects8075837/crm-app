import BlueLink from '@/components/Elements/BlueLink/BlueLink';
import HeaderLayout from '@/components/Layouts/HeaderLayout/HeaderLayout';
import React from 'react';
import style from '../../../style/elements/blue_button.module.scss';
import headerStyle from '../../../style/layout/header.module.scss';

interface BaseEntityHeaderProps {
    title?: string | React.ReactNode;
    link?: string;
    linkText?: string;
    headerButton?: React.ReactNode;
    breadcrumbs?: string | React.ReactNode;
    children?: React.ReactNode;
}

const BaseEntityHeader: React.FC<BaseEntityHeaderProps> = ({
    title,
    link,
    linkText = 'Новый',
    headerButton,
    breadcrumbs,
    children,
}) => {
    return (
        <>
            <HeaderLayout title={title} breadcrumbs={breadcrumbs}>
                <div className={headerStyle.headerChildrenContainerEnd}>
                    <div>{children}</div>
                    <div className={style.blockStandardWidth}>
                        {headerButton ? (
                            <>{headerButton}</>
                        ) : (
                            <>
                                {link && (
                                    <BlueLink to={link}>{linkText}</BlueLink>
                                )}
                            </>
                        )}
                    </div>
                </div>
            </HeaderLayout>
        </>
    );
};

export default BaseEntityHeader;
