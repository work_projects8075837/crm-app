import BlueLink from '@/components/Elements/BlueLink/BlueLink';
import { StyleBlueButton } from '@/style/elements/elements';
import React from 'react';
import headerStyle from '../../../style/layout/header.module.scss';

interface EntityCreateBlueButtonProps {
    link: string;
    linkText?: string;
}

const EntityCreateBlueButton: React.FC<EntityCreateBlueButtonProps> = ({
    link,
    linkText = 'Новый',
}) => {
    return (
        <>
            <div className={headerStyle.headerChildrenContainerEnd}>
                <div className={StyleBlueButton.blockStandardWidth}>
                    <BlueLink to={link}>{linkText}</BlueLink>
                </div>
            </div>
        </>
    );
};

export default EntityCreateBlueButton;
