import HeaderLayout from '@/components/Layouts/HeaderLayout/HeaderLayout';
import React from 'react';
import DirectoryHeaderButtons from './DirectoryHeaderButtons';

interface BaseDirectoryHeaderProps {
    title?: string | React.ReactNode;
    link: string;
    entityText: string;
    directoryText: string;
    breadcrumbs?: string | React.ReactNode;
    onEntityClick?: () => void;
    onDirectoryClick?: () => void;
}

const BaseDirectoryHeader: React.FC<BaseDirectoryHeaderProps> = ({
    title,
    breadcrumbs,
    ...directoryButtonsProps
}) => {
    return (
        <>
            <HeaderLayout title={title} breadcrumbs={breadcrumbs}>
                <DirectoryHeaderButtons {...directoryButtonsProps} />
            </HeaderLayout>
        </>
    );
};

export default BaseDirectoryHeader;
