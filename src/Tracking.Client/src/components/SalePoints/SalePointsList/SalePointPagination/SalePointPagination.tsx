import Pagination from '@/components/Tables/Pagination/Pagination';
import PaginationButtonsList from '@/components/Tickets/TiketsPaginationButtonsList/PaginationButtonsList';
import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import React from 'react';
import { useSalePointsTable } from '@/store/queries/stores/sale_point/useSalePointsTable';
import { observer } from 'mobx-react-lite';

const SALE_POINT_PAGINATION = 'SALE_POINT_PAGINATION';
export const salePointPaginationHandler = new PaginationHandler(
    SALE_POINT_PAGINATION,
);

const SalePointPagination: React.FC = () => {
    const { data } = useSalePointsTable(salePointPaginationHandler);

    return (
        <>
            <Pagination
                paginationHandler={salePointPaginationHandler}
                pages={
                    <PaginationButtonsList
                        paginationHandler={salePointPaginationHandler}
                        data={data}
                    />
                }
            />
        </>
    );
};

export default observer(SalePointPagination);
