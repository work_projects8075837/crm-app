import React from 'react';
import style from '../../../../style/layout/sale_point_table.module.scss';
import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import SalePointPagination from '../SalePointPagination/SalePointPagination';
import { salePointCheckHandler } from '../SalePointsList';

interface SalePointsTableLayoutProps {
    children: React.ReactNode;
}

const salePointTableFields = [{ name: 'Название' }];

const SalePointsTableLayout: React.FC<SalePointsTableLayoutProps> = ({
    children,
}) => {
    return (
        <>
            <TableLayout
                layoutClass={style.salePointTable}
                header={
                    <TableHeader
                        checkHandler={salePointCheckHandler}
                        fields={salePointTableFields}
                        layoutClass={style.salePointTableHeader}
                        fieldClass={style.salePointTableHeaderField}
                    />
                }
                pagination={<SalePointPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default SalePointsTableLayout;
