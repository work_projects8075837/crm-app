import EntitiesLoading from '@/components/Elements/EntitiesLoading/EntitiesLoading';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { salePointApi } from '@/services/sale_point/sale_point';
import {
    GET_ALL_SALE_POINTS_KEY,
    GET_SEARCH_SALE_POINTS_KEY,
} from '@/store/queries/keys/keys';
import { useSalePointsTable } from '@/store/queries/stores/sale_point/useSalePointsTable';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { salePointPaginationHandler } from './SalePointPagination/SalePointPagination';
import SalePointsListItem from './SalePointsListItem';
import SalePointsTableLayout from './SalePointsTableLayout/SalePointsTableLayout';

export const salePointCheckHandler = new CheckEntitiesHandler();

const SalePointsList: React.FC = () => {
    const { data, refetch } = useSalePointsTable(salePointPaginationHandler, {
        refetchOnMount: true,
    });

    useSetCheckableEntities(salePointCheckHandler, data?.data);

    return (
        <>
            <EntitiesLoading
                isLoading={!data}
                isEmpty={!data?.data.length}
                emptyText='Нет заведений'
            >
                <SalePointsTableLayout>
                    {data?.data.map((salePoint) => (
                        <SalePointsListItem key={salePoint.id} {...salePoint} />
                    ))}
                </SalePointsTableLayout>
            </EntitiesLoading>
            <EntitySelectedActions
                keys={[GET_ALL_SALE_POINTS_KEY, GET_SEARCH_SALE_POINTS_KEY]}
                checkHandler={salePointCheckHandler}
                service={salePointApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default observer(SalePointsList);
