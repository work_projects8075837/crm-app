import DirectoryIcon from '@/components/Elements/DirectoryIcon/DirectoryIcon';
import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/sale_point_table.module.scss';
import { salePointCheckHandler } from './SalePointsList';

interface SalePointsListItemProps {
    id: string;
    name: string;
    is_parent: boolean;
}

const SalePointsListItem: React.FC<SalePointsListItemProps> = ({
    name,
    id,
    is_parent,
}) => {
    const navigate = useNavigate();
    return (
        <div
            className={style.salePointTableItem}
            onDoubleClick={() => {
                navigate(
                    is_parent
                        ? `/sale_point/${id}/`
                        : `/sale_point/single/${id}/`,
                );
            }}
        >
            <CheckItem
                checkHandler={salePointCheckHandler}
                entity={{ id, name }}
            />
            <div className={style.salePointTableItemName}>
                <span>{name}</span>
                {is_parent && <DirectoryIcon />}
            </div>
        </div>
    );
};

export default SalePointsListItem;
