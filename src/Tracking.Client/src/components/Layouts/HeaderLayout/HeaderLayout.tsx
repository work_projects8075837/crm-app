import React from 'react';
import style from '../../../style/layout/header.module.scss';

interface HeaderLayoutProps {
    title?: string | React.ReactNode;
    children?: React.ReactNode;
    breadcrumbs?: React.ReactNode;
    className?: string;
}

const HeaderLayout: React.FC<HeaderLayoutProps> = ({
    title,
    breadcrumbs,
    children,
    className = '',
}) => {
    return (
        <header className={`${style.header} ${className}`}>
            <div className={style.headerTitleSide}>
                <div className={style.breadcrumbs}>{breadcrumbs}</div>
                <h1 className={style.headerMainTitle}>{title}</h1>
            </div>
            {children}
        </header>
    );
};

export default HeaderLayout;
