import TicketSaveButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveButton';
import TicketSaveCloseButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveCloseButton';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';

interface FormBottomProps {
    isLoading?: boolean;
    isSaveDisabled?: boolean;
    isSaveCloseDisabled?: boolean;
    onSaveClick?: () => void;
    onSaveCloseClick?: () => void;
}

const FormBottom: React.FC<FormBottomProps> = ({
    isLoading = false,
    isSaveDisabled = false,
    isSaveCloseDisabled = false,
    onSaveClick,
    onSaveCloseClick,
}) => {
    return (
        <>
            <div className={style.formSubmitBottom}>
                {isLoading ? (
                    <>Сохранение...</>
                ) : (
                    <>
                        <TicketSaveButton
                            isDisabled={isSaveDisabled}
                            onClick={onSaveClick}
                            className={style.formSubmitLimitedButton}
                        />
                        <TicketSaveCloseButton
                            isDisabled={isSaveCloseDisabled}
                            onClick={onSaveCloseClick}
                            className={style.formSubmitLimitedButton}
                        />
                    </>
                )}
            </div>
        </>
    );
};

export default FormBottom;
