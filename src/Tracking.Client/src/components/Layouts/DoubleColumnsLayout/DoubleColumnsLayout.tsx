import React from 'react';
import style from '../../../style/layout/layout.module.scss';
interface DoubleColumnsLayoutProps {
    right?: React.ReactNode;
    left?: React.ReactNode;
    className?: string;
}

const DoubleColumnsLayout: React.FC<DoubleColumnsLayoutProps> = ({
    left,
    right,
    className = '',
}) => {
    return (
        <div className={`${style.doubleColumns} ${className}`}>
            <div className={style.side}>{left}</div>

            <div className={style.side}>{right}</div>
        </div>
    );
};

export default DoubleColumnsLayout;
