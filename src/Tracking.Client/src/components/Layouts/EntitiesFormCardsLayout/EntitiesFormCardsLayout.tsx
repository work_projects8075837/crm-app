import React from 'react';
import style from '../../../style/layout/sale_point.module.scss';
import PlusGreyButton from '@/components/Elements/PlusGreyButton/PlusGreyButton';

interface EntitiesFormCardsLayoutProps {
    children: React.ReactNode;
    childrenClass?: string;
    onPlusClick: () => void;
}

const EntitiesFormCardsLayout: React.FC<EntitiesFormCardsLayoutProps> = ({
    children,
    childrenClass = '',
    onPlusClick,
}) => {
    return (
        <>
            <div className={`${style.contactsList}`}>
                <div className={`${style.contacts} ${childrenClass}`}>
                    {children}
                </div>
                <PlusGreyButton
                    onClick={onPlusClick}
                    className={style.addContactButton}
                />
            </div>
        </>
    );
};

export default EntitiesFormCardsLayout;
