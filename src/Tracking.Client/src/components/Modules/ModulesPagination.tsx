import { tagHooks } from '@/services/tag/tag';
import { PaginationHandler } from '@/store/states/PaginationHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';
import Pagination from '../Tables/Pagination/Pagination';
import PaginationButtonsList from '../Tickets/TiketsPaginationButtonsList/PaginationButtonsList';

const TAGS_PAGINATION = 'TAGS_PAGINATION';
export const tagsPaginationHandler = new PaginationHandler(TAGS_PAGINATION);

const ModulesPagination: React.FC = () => {
    const { data } = tagHooks.useList(tagsPaginationHandler);
    return (
        <>
            <Pagination
                paginationHandler={tagsPaginationHandler}
                pages={
                    <PaginationButtonsList
                        {...{ data, paginationHandler: tagsPaginationHandler }}
                    />
                }
            />
        </>
    );
};

export default observer(ModulesPagination);
