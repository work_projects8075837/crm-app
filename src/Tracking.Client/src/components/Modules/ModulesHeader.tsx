import { invalidateKey } from '@/store/queries/actions/base';
import { GET_ALL_TAGS_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import { questionForm } from '../Elements/DialogForm/questionForm';
import FormInput from '../Fields/BaseFields/FormInput';
import BlueButton from '../Fields/Buttons/BlueButton';
import { useTagCreateMutation } from '../Form/TagCreateForm/useTagCreateMutation';
import BaseEntityHeader from '../SalePoints/SalePointsHeader/BaseEntityHeader';
import { moduleCreateRef } from './Modules';

const ModulesHeader: React.FC = () => {
    const { mutateAsync, isLoading } = useTagCreateMutation({});

    return (
        <>
            <div ref={moduleCreateRef}></div>
            <BaseEntityHeader
                title={'Список модулей'}
                headerButton={
                    <>
                        <BlueButton
                            disabled={isLoading}
                            onClick={async () => {
                                const answer = await questionForm<{
                                    name: string;
                                }>(
                                    {
                                        title: 'Новый модуль',
                                        fields: (
                                            <>
                                                <FormInput
                                                    name='name'
                                                    placeholder='Название'
                                                />
                                            </>
                                        ),
                                        buttonProps: { isLoading },
                                    },
                                    moduleCreateRef,
                                );

                                if (!answer) {
                                    return;
                                }

                                await mutateAsync(answer, {
                                    onSuccess: () => {
                                        invalidateKey(GET_ALL_TAGS_KEY);
                                    },
                                });
                            }}
                        >
                            {isLoading ? 'Загрузка...' : 'Добавить модуль'}
                        </BlueButton>
                    </>
                }
            />
        </>
    );
};

export default ModulesHeader;
