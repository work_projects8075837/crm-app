import { tagApi } from '@/services/tag/tag';
import { TagCreate } from '@/services/tag/types';
import { useMutation } from '@tanstack/react-query';

export const useTagUpdate = () => {
    return useMutation({
        mutationFn: async (data: TagCreate & { id: string }) => {
            return await tagApi.update(data.id, data);
        },
    });
};
