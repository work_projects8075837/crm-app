import React, { createRef } from 'react';
import Layout from '../NavbarLayout/Layout';
import ModulesHeader from './ModulesHeader';
import ModulesList from './ModulesList';

export const moduleCreateRef = createRef<HTMLDivElement>();

const Modules: React.FC = () => {
    return (
        <>
            <Layout>
                <ModulesHeader />
                <ModulesList />
            </Layout>
        </>
    );
};

export default Modules;
