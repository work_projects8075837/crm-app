import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import React from 'react';
import style from '../../style/layout/tags.module.scss';
import TableHeader from '../Tables/HeaderLayout/TableHeader';
import TableLayout from '../Tables/TableLayout/TableLayout';
import ModulesPagination from './ModulesPagination';
interface ModulesTableLayoutProps {
    children: React.ReactNode;
    checkHandler: CheckEntitiesHandler;
}

const ModulesTableLayout: React.FC<ModulesTableLayoutProps> = ({
    checkHandler,
    children,
}) => {
    return (
        <>
            <TableLayout
                layoutClass={style.entityPaginatedTable}
                header={
                    <TableHeader
                        checkHandler={checkHandler}
                        fields={[{ name: 'Назвние' }]}
                        layoutClass={style.rowLayout}
                        fieldClass={style.headerField}
                    />
                }
                pagination={<ModulesPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default ModulesTableLayout;
