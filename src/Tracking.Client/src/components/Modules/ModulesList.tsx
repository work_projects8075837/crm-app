import { tagApi, tagHooks } from '@/services/tag/tag';
import {
    GET_ALL_TAGS_KEY,
    GET_SEARCH_TAGS_KEY,
} from '@/store/queries/keys/keys';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import { observer } from 'mobx-react-lite';
import React from 'react';
import EntitiesLoading from '../Elements/EntitiesLoading/EntitiesLoading';
import EntitySelectedActions from '../Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import ModuleItem from './ModuleItem';
import { tagsPaginationHandler } from './ModulesPagination';
import ModulesTableLayout from './ModulesTableLayout';

export const modulesCheck = new CheckEntitiesHandler();

const ModulesList: React.FC = () => {
    const { data, refetch } = tagHooks.useList(tagsPaginationHandler, {
        refetchOnMount: true,
    });

    useSetCheckableEntities(modulesCheck, data?.data);
    return (
        <>
            <EntitiesLoading
                isEmpty={!data?.data.length}
                isLoading={!data}
                emptyText='Нет модулей'
            >
                <ModulesTableLayout checkHandler={modulesCheck}>
                    {data?.data.map((t) => <ModuleItem key={t.id} {...t} />)}
                </ModulesTableLayout>
            </EntitiesLoading>
            <EntitySelectedActions
                keys={[GET_ALL_TAGS_KEY, GET_SEARCH_TAGS_KEY]}
                checkHandler={modulesCheck}
                service={tagApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default observer(ModulesList);
