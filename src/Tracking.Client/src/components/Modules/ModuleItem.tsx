import { ITag } from '@/services/tag/types';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_ALL_TAGS_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import style from '../../style/layout/tags.module.scss';
import { questionForm } from '../Elements/DialogForm/questionForm';
import FormInput from '../Fields/BaseFields/FormInput';
import CheckItem from '../Fields/CheckItem/CheckItem';
import { moduleCreateRef } from './Modules';
import { modulesCheck } from './ModulesList';
import { useTagUpdate } from './useTagUpdate';

type ModuleItemProps = ITag;

const ModuleItem: React.FC<ModuleItemProps> = ({ id, name }) => {
    const { mutateAsync } = useTagUpdate();
    return (
        <div
            className={style.tableItem}
            onDoubleClick={async () => {
                const answer = await questionForm<{ name: string }>(
                    {
                        title: name,
                        fields: (
                            <>
                                <FormInput name='name' placeholder='Название' />
                            </>
                        ),
                        formProps: { defaultValues: { name } },
                    },
                    moduleCreateRef,
                );

                if (!answer) {
                    return;
                }

                await mutateAsync(
                    { ...answer, id },
                    {
                        onSuccess: () => {
                            invalidateKey(GET_ALL_TAGS_KEY);
                        },
                    },
                );
            }}
        >
            <CheckItem checkHandler={modulesCheck} entity={{ id, name }} />

            <div className={style.tableItemName}>
                <span>{name}</span>
            </div>
        </div>
    );
};

export default ModuleItem;
