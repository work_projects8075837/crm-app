import CharacterProblemsLink from '@/components/Elements/BreadCrumbs/CharacterProblemsLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import OneEntityBaseHeader from '@/components/SalePoints/SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';
import { useCharacterProblemBreadCrumbs } from '@/store/queries/stores/character_problem/useCharacterProblemBreadCrumbs';
import { useCharacterProblemByParams } from '@/store/queries/stores/character_problem/useCharacterProblemByParams';
import React from 'react';

interface CharacterProblemEditHeader {
    children?: React.ReactNode;
}

const CharacterProblemEditHeader: React.FC<CharacterProblemEditHeader> = ({
    children,
}) => {
    const { data, current } = useCharacterProblemBreadCrumbs();
    return (
        <>
            <OneEntityBaseHeader
                title={current?.name}
                breadcrumbs={
                    <>
                        <CharacterProblemsLink />{' '}
                        <InnerBreadCrumbs
                            links={data}
                            route={'character_problem'}
                        />{' '}
                    </>
                }
            >
                {children}
            </OneEntityBaseHeader>
        </>
    );
};

export default CharacterProblemEditHeader;
