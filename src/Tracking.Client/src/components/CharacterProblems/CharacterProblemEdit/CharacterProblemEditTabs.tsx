import CircleNavLink from '@/components/Elements/CircleNavLink/CircleNavLink';
import { useCharacterProblemByParams } from '@/store/queries/stores/character_problem/useCharacterProblemByParams';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';

const CharacterProblemEditTabs: React.FC = () => {
    const { data } = useCharacterProblemByParams();
    return (
        <>
            <div className={style.formTabsButtons}>
                <CircleNavLink
                    to={`/character_problem/single/${data?.id}/`}
                    text='Общая информация'
                />
                <CircleNavLink
                    to={`/character_problem/single/${data?.id}/jobs`}
                    text='Работы'
                />
            </div>
        </>
    );
};

export default CharacterProblemEditTabs;
