import ModalMenu from '@/components/Elements/ModalMenu/ModalMenu';
import ActionsMenuDeleteButton from '@/components/Fields/ActionsMenuDeleteButton/ActionsMenuDeleteButton';
import EntityOptionsLayout from '@/components/Tickets/TicketOptions/EntityOptionsLayout';
import { characterProblemApi } from '@/services/character_problem/character_problem';
import { GET_ALL_CHARACTER_PROBLEMS_KEY } from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import CharacterProblemEditFormProvider from '../Providers/CharacterProblemEditFormProvider';
import CharacterProblemEditHeader from './CharacterProblemEditHeader';
import CharacterProblemEditTabs from './CharacterProblemEditTabs';

interface CharacterProblemEditLayoutProps {
    children: React.ReactNode;
    headerButton?: React.ReactNode;
}

export const characterProblemActions = new OpenHandler(false);

const CharacterProblemEditLayout: React.FC<CharacterProblemEditLayoutProps> = ({
    children,
    headerButton,
}) => {
    return (
        <>
            <CharacterProblemEditFormProvider>
                <CharacterProblemEditHeader>
                    {headerButton}
                </CharacterProblemEditHeader>
                <EntityOptionsLayout>
                    <ModalMenu symbol='ticket_edit' text='Действия'>
                        <ActionsMenuDeleteButton
                            queryKey={GET_ALL_CHARACTER_PROBLEMS_KEY}
                            service={characterProblemApi}
                            idName='characterProblemId'
                            navigateTo='/character_problem/'
                        />
                    </ModalMenu>
                </EntityOptionsLayout>
                <CharacterProblemEditTabs />
                <div className={style.afterTabsMargin}>{children}</div>
            </CharacterProblemEditFormProvider>
        </>
    );
};

export default CharacterProblemEditLayout;
