import React from 'react';
import CharacterProblemGeneral from '../CharacterProblemGeneral/CharacterProblemGeneral';
import CharacterProblemEditLayout from './CharacterProblemEditLayout';

const CharacterProblemEdit: React.FC = () => {
    return (
        <>
            <CharacterProblemEditLayout>
                <CharacterProblemGeneral />
            </CharacterProblemEditLayout>
        </>
    );
};

export default CharacterProblemEdit;
