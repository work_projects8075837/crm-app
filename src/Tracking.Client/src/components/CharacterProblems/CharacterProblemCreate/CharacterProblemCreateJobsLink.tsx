import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import { ICharacterProblem } from '@/services/character_problem/types';
import React from 'react';
import { useNavigate } from 'react-router-dom';

const CharacterProblemCreateJobsLink: React.FC = () => {
    const { onSubmit, isDisabled } = useSubmit();
    const navigate = useNavigate();
    return (
        <>
            <CircleButton
                text={'Работы'}
                disabled={isDisabled}
                onClick={() => {
                    if (onSubmit) {
                        onSubmit((data: ICharacterProblem) => {
                            navigate(
                                `/character_problem/single/${data.id}/jobs`,
                            );
                        });
                    }
                }}
            />
        </>
    );
};

export default CharacterProblemCreateJobsLink;
