import CharacterProblemsLink from '@/components/Elements/BreadCrumbs/CharacterProblemsLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import OneEntityBaseHeader from '@/components/SalePoints/SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';
import { useCharacterProblemBreadCrumbs } from '@/store/queries/stores/character_problem/useCharacterProblemBreadCrumbs';
import React from 'react';
import style from '../../../style/layout/sale_point.module.scss';
import CharacterProblemGeneral from '../CharacterProblemGeneral/CharacterProblemGeneral';
import CharacterProblemCreateJobsLink from './CharacterProblemCreateJobsLink';

const CharacterProblemCreateElements: React.FC = () => {
    const { data } = useCharacterProblemBreadCrumbs();
    return (
        <>
            <div className={style.formChildren}>
                <OneEntityBaseHeader
                    title='Новый характер проблемы'
                    breadcrumbs={
                        <>
                            <CharacterProblemsLink />{' '}
                            <InnerBreadCrumbs
                                links={data}
                                route='character_problem'
                            />{' '}
                            / Новый характер проблемы
                        </>
                    }
                />
                <div className={style.formTabsButtons}>
                    <CircleButton isActive={true} text='Общая информация' />

                    <CharacterProblemCreateJobsLink />
                </div>
                <div className={style.afterTabsMargin}>
                    <CharacterProblemGeneral />
                </div>
            </div>
        </>
    );
};

export default CharacterProblemCreateElements;
