import Layout from '@/components/NavbarLayout/Layout';
import React from 'react';
import CharacterProblemCreateForm from './../../Form/CharacterProblemCreateForm/CharacterProblemCreateForm';
import style from '../../../style/layout/sale_point.module.scss';

import CreateJobModal from '../CreateJobModal/CreateJobModal';
import SelectJobsModal from '../SelectJobsModal/SelectJobsModal';
import AddTagsModal from '@/components/Tickets/TicketCreate/AddTagsModal/AddTagsModal';
import CharacterProblemCreateElements from './CharacterProblemCreateElements';

const CharacterProblemCreate: React.FC = () => {
    return (
        <>
            <Layout childrenClass={style.formLayout}>
                <CharacterProblemCreateForm
                    modals={
                        <>
                            <AddTagsModal />
                            <SelectJobsModal />
                            <CreateJobModal />
                        </>
                    }
                >
                    <CharacterProblemCreateElements />
                </CharacterProblemCreateForm>
            </Layout>
        </>
    );
};

export default CharacterProblemCreate;
