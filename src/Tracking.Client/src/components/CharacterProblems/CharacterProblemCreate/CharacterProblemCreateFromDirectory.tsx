import React from 'react';
import CharacterProblemCreateElements from './CharacterProblemCreateElements';
import CharacterProblemCreateFormProvider from '../Providers/CharacterProblemCreateFormProvider';

const CharacterProblemCreateFromDirectory: React.FC = () => {
    return (
        <>
            <CharacterProblemCreateFormProvider>
                <CharacterProblemCreateElements />
            </CharacterProblemCreateFormProvider>
        </>
    );
};

export default CharacterProblemCreateFromDirectory;
