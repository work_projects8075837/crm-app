import BreadCrumbLink from '@/components/Elements/BreadCrumbs/BreadCrumbLink';
import CharacterProblemsLink from '@/components/Elements/BreadCrumbs/CharacterProblemsLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import BaseDirectoryHeader from '@/components/SalePoints/SalePointsHeader/BaseDirectoryHeader';
import { useCharacterProblemBreadCrumbs } from '@/store/queries/stores/character_problem/useCharacterProblemBreadCrumbs';
import React from 'react';

const CharacterProblemDirectoryHeader: React.FC = () => {
    const { data, current } = useCharacterProblemBreadCrumbs();
    return (
        <>
            <BaseDirectoryHeader
                breadcrumbs={
                    <>
                        <CharacterProblemsLink />
                        <InnerBreadCrumbs
                            links={data}
                            route='character_problem'
                        />
                    </>
                }
                title={
                    <BreadCrumbLink
                        to={`/character_problem/single/${current?.id}`}
                    >
                        {current?.name}
                    </BreadCrumbLink>
                }
                link={`/character_problem/create/${current?.id}`}
                directoryText='Новая папка характеров'
                entityText='Новый характер проблемы'
            />
        </>
    );
};

export default CharacterProblemDirectoryHeader;
