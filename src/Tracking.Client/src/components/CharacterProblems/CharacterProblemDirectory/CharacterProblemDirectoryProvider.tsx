import React from 'react';
import Loader from '@/Loader/Loader';
import { useCharacterProblemBreadCrumbs } from '@/store/queries/stores/character_problem/useCharacterProblemBreadCrumbs';

interface CharacterProblemDirectoryProviderProps {
    children: React.ReactNode;
}

const CharacterProblemDirectoryProvider: React.FC<
    CharacterProblemDirectoryProviderProps
> = ({ children }) => {
    const { isLoading } = useCharacterProblemBreadCrumbs({
        refetchOnMount: true,
    });
    return <>{isLoading ? <Loader /> : children}</>;
};

export default CharacterProblemDirectoryProvider;
