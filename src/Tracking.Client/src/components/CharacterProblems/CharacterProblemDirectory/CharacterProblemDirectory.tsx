import Layout from '@/components/NavbarLayout/Layout';
import React from 'react';
import CharacterProblemDirectoryProvider from './CharacterProblemDirectoryProvider';
import CharacterProblemDirectoryHeader from './CharacterProblemDirectoryHeader';
import CharacterProblemList from '../CharacterProblemList/CharacterProblemList';

const CharacterProblemDirectory: React.FC = () => {
    return (
        <>
            <Layout>
                <CharacterProblemDirectoryProvider>
                    <CharacterProblemDirectoryHeader />
                    <CharacterProblemList />
                </CharacterProblemDirectoryProvider>
            </Layout>
        </>
    );
};

export default CharacterProblemDirectory;
