import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import Layout from '@/components/NavbarLayout/Layout';
import CharacterProblemCreateForm from '@/components/Form/CharacterProblemCreateForm/CharacterProblemCreateForm';
import AddTagsModal from '@/components/Tickets/TicketCreate/AddTagsModal/AddTagsModal';
import SelectJobsModal from '../SelectJobsModal/SelectJobsModal';
import CreateJobModal from '../CreateJobModal/CreateJobModal';
import { useCharacterProblemByParams } from '@/store/queries/stores/character_problem/useCharacterProblemByParams';
import { useCharacterProblemBreadCrumbs } from '@/store/queries/stores/character_problem/useCharacterProblemBreadCrumbs';
import Loader from '@/Loader/Loader';
interface CharacterProblemCreateFormProviderProps {
    children: React.ReactNode;
}

const CharacterProblemCreateFormProvider: React.FC<
    CharacterProblemCreateFormProviderProps
> = ({ children }) => {
    const { data } = useCharacterProblemByParams({ refetchOnMount: true });
    const { isLoading } = useCharacterProblemBreadCrumbs({
        refetchOnMount: true,
    });
    return (
        <>
            {data && !isLoading ? (
                <Layout childrenClass={style.formLayout}>
                    <CharacterProblemCreateForm
                        parentCharacterProblem={data}
                        modals={
                            <>
                                <AddTagsModal />
                                <SelectJobsModal />
                                <CreateJobModal />
                            </>
                        }
                    >
                        {children}
                    </CharacterProblemCreateForm>
                </Layout>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default CharacterProblemCreateFormProvider;
