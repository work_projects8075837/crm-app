import Loader from '@/Loader/Loader';
import UpdateCharacterProblemForm from '@/components/Form/UpdateCharacterProblemForm/UpdateCharacterProblemForm';
import Layout from '@/components/NavbarLayout/Layout';
import AddTagsModal from '@/components/Tickets/TicketCreate/AddTagsModal/AddTagsModal';
import { useCharacterProblemBreadCrumbs } from '@/store/queries/stores/character_problem/useCharacterProblemBreadCrumbs';
import { useCharacterProblemByParams } from '@/store/queries/stores/character_problem/useCharacterProblemByParams';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import CreateJobModal from '../CreateJobModal/CreateJobModal';
import SelectJobsModal from '../SelectJobsModal/SelectJobsModal';

interface CharacterProblemEditFormProviderProps {
    children: React.ReactNode;
}

const CharacterProblemEditFormProvider: React.FC<
    CharacterProblemEditFormProviderProps
> = ({ children }) => {
    const { data } = useCharacterProblemByParams({ refetchOnMount: true });
    const { isLoading } = useCharacterProblemBreadCrumbs({
        refetchOnMount: true,
    });
    return (
        <>
            {data && !isLoading ? (
                <Layout childrenClass={style.formLayout}>
                    <UpdateCharacterProblemForm
                        modals={
                            <>
                                <AddTagsModal />
                                <SelectJobsModal />
                                <CreateJobModal />
                            </>
                        }
                        defaultValues={data}
                    >
                        {children}
                    </UpdateCharacterProblemForm>
                </Layout>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default CharacterProblemEditFormProvider;
