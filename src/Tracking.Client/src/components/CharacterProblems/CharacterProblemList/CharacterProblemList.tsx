import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import NullEntities from '@/components/Elements/NullEntities/NullEntities';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { characterProblemApi } from '@/services/character_problem/character_problem';
import {
    GET_ALL_CHARACTER_PROBLEMS_KEY,
    GET_SEARCH_CHARACTER_PROBLEMS_KEY,
} from '@/store/queries/keys/keys';
import { useCharacterProblemsTable } from '@/store/queries/stores/character_problem/useCharacterProblemsTable';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/elements/loader_text.module.scss';
import CharacterProblemListItem from './CharacterProblemListItem';
import { characterProblemPagination } from './CharacterProblemPagination';
import CharacterProblemTableLayout from './CharacterProblemTableLayout';

export const characterProblemCheck = new CheckEntitiesHandler();

const CharacterProblemList: React.FC = () => {
    const { data, refetch } = useCharacterProblemsTable(
        characterProblemPagination,
        {
            refetchOnMount: true,
        },
    );
    useSetCheckableEntities(characterProblemCheck, data?.data);
    return (
        <>
            {data ? (
                <>
                    {data.data.length ? (
                        <>
                            <CharacterProblemTableLayout>
                                {data.data.map((characterProblem) => (
                                    <CharacterProblemListItem
                                        key={characterProblem.id}
                                        {...characterProblem}
                                    />
                                ))}
                            </CharacterProblemTableLayout>
                        </>
                    ) : (
                        <NullEntities text='Нет характеров проблемы' />
                    )}
                </>
            ) : (
                <LoaderCircle size={100} className={style.loaderPlaceFull} />
            )}
            <EntitySelectedActions
                keys={[
                    GET_ALL_CHARACTER_PROBLEMS_KEY,
                    GET_SEARCH_CHARACTER_PROBLEMS_KEY,
                ]}
                checkHandler={characterProblemCheck}
                service={characterProblemApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default observer(CharacterProblemList);
