import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import React from 'react';
import style from '../../../style/layout/character_problem.module.scss';
import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import CharacterProblemPagination from './CharacterProblemPagination';
import { characterProblemCheck } from './CharacterProblemList';

interface CharacterProblemTableLayoutProps {
    children: React.ReactNode;
}

const fields = [
    { name: 'Название' },
    { name: 'Длительность' },
    { name: 'Ссылка' },
];

const CharacterProblemTableLayout: React.FC<
    CharacterProblemTableLayoutProps
> = ({ children }) => {
    return (
        <>
            <TableLayout
                layoutClass={style.entityPaginatedTable}
                header={
                    <TableHeader
                        checkHandler={characterProblemCheck}
                        fields={fields}
                        layoutClass={style.header}
                        fieldClass={style.headerField}
                    />
                }
                pagination={<CharacterProblemPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default CharacterProblemTableLayout;
