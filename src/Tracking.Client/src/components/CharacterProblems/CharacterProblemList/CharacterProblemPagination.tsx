import Pagination from '@/components/Tables/Pagination/Pagination';
import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';
import PaginationButtonsList from '@/components/Tickets/TiketsPaginationButtonsList/PaginationButtonsList';
import { useCharacterProblemsTable } from '@/store/queries/stores/character_problem/useCharacterProblemsTable';

const CHARACTER_PROBLEM_PAGINATION = 'CHARACTER_PROBLEM_PAGINATION';
export const characterProblemPagination = new PaginationHandler(
    CHARACTER_PROBLEM_PAGINATION,
);

const CharacterProblemPagination: React.FC = () => {
    const { data } = useCharacterProblemsTable(characterProblemPagination);
    return (
        <>
            <Pagination
                paginationHandler={characterProblemPagination}
                pages={
                    <PaginationButtonsList
                        paginationHandler={characterProblemPagination}
                        data={data}
                    />
                }
            />
        </>
    );
};

export default observer(CharacterProblemPagination);
