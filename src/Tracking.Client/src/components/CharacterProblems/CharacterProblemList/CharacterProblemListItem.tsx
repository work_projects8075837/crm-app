import DirectoryIcon from '@/components/Elements/DirectoryIcon/DirectoryIcon';
import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import { countHours } from '@/components/utils/countHours';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/character_problem.module.scss';
import { characterProblemCheck } from './CharacterProblemList';

interface CharacterProblemListItemProps {
    id: string;
    name: string;
    sla: number;
    url: string;
    is_parent: boolean;
}

const CharacterProblemListItem: React.FC<CharacterProblemListItemProps> = ({
    id,
    name,
    url,
    sla,
    is_parent,
}) => {
    const navigate = useNavigate();
    const { hours, minutes } = countHours(sla);

    return (
        <div
            className={style.tableItem}
            onDoubleClick={() => {
                if (is_parent) {
                    navigate(`/character_problem/${id}/`);
                } else {
                    navigate(`/character_problem/single/${id}/`);
                }
            }}
        >
            <CheckItem
                checkHandler={characterProblemCheck}
                entity={{ id, name, url, sla }}
            />
            <div className={style.tableItemName}>
                <span>{name}</span>
                {is_parent && <DirectoryIcon />}
            </div>

            <div className={style.tableItemField}>
                <span>
                    {!!hours && `${hours}ч`} {!!minutes && `${minutes}мин`}
                </span>
            </div>

            <div className={style.tableItemField}>
                <a href={url} target='_blank' rel='noreferrer'>
                    {url}
                </a>
            </div>
        </div>
    );
};

export default CharacterProblemListItem;
