import Layout from '@/components/NavbarLayout/Layout';
import BaseDirectoryHeader from '@/components/SalePoints/SalePointsHeader/BaseDirectoryHeader';
import React from 'react';
import CharacterProblemList from '../CharacterProblemList/CharacterProblemList';

const CharacterProblemsPage: React.FC = () => {
    return (
        <>
            <Layout>
                <BaseDirectoryHeader
                    title='Список характеров проблемы'
                    link={'/character_problem/create/'}
                    directoryText='Новая папка характеров'
                    entityText='Новый характер проблемы'
                />
                <CharacterProblemList />
            </Layout>
        </>
    );
};

export default CharacterProblemsPage;
