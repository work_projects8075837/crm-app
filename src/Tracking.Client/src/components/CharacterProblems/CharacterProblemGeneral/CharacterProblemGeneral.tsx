import FormSelect from '@/components/Elements/FormSelect/FormSelect';
import CharacterProblemSelect from '@/components/Fields/CharacterProblemSelect/CharacterProblemSelect';
import DurationShortInput from '@/components/Fields/DurationShortInput/DurationShortInput';
import {
    CHARACTER_PROBLEM_NAME_NAME,
    CHARACTER_PROBLEM_SLA_NAME,
    CHARACTER_PROBLEM_URL_NAME,
} from '@/components/Fields/EntitiesFields/character_problem';
import FormShortInput from '@/components/Fields/FormShortInput/FormShortInput';
import EntityFieldLayout from '@/components/Fields/Layouts/EntityFieldLayout';
import DoubleColumnsLayout from '@/components/Layouts/DoubleColumnsLayout/DoubleColumnsLayout';
import ParentEntityExceptProvider from '@/components/SalePoints/Providers/ParentEntityExceptProvider/ParentEntityExceptProvider';
import TagsList from '@/components/Tickets/TagsList/TagsList';
import React from 'react';

const CharacterProblemGeneral: React.FC = () => {
    return (
        <>
            <DoubleColumnsLayout
                left={
                    <>
                        <FormSelect title='Основное'>
                            <EntityFieldLayout title='Название'>
                                <FormShortInput
                                    placeholder='Название'
                                    name={CHARACTER_PROBLEM_NAME_NAME}
                                />
                            </EntityFieldLayout>

                            <CharacterProblemSelect
                                title='Папка характеров проблемы'
                                onlyDirectory={true}
                                isRequired={false}
                            />

                            <ParentEntityExceptProvider>
                                <EntityFieldLayout title='Время решения'>
                                    <DurationShortInput
                                        required={true}
                                        name={CHARACTER_PROBLEM_SLA_NAME}
                                    />
                                </EntityFieldLayout>
                                <EntityFieldLayout title='Ссылка'>
                                    <FormShortInput
                                        placeholder='Ссылка'
                                        name={CHARACTER_PROBLEM_URL_NAME}
                                        required={false}
                                    />
                                </EntityFieldLayout>
                            </ParentEntityExceptProvider>
                        </FormSelect>
                    </>
                }
                right={
                    <>
                        <ParentEntityExceptProvider>
                            <FormSelect title='Модули'>
                                <TagsList />
                            </FormSelect>
                        </ParentEntityExceptProvider>
                    </>
                }
            />
        </>
    );
};

export default CharacterProblemGeneral;
