import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import { CHARACTER_PROBLEM_JOBS_NAME } from '@/components/Fields/EntitiesFields/character_problem';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import JobCreateForm from '@/components/Form/JobCreateForm/JobCreateForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';

export const createJobToCharacterProblemModalOpen = new OpenHandler(false);

const CreateJobModal: React.FC = () => {
    const { add } = useFormEntityValueArray(CHARACTER_PROBLEM_JOBS_NAME);

    return (
        <>
            <ModalWindow
                title='Добавить работу'
                openHandler={createJobToCharacterProblemModalOpen}
            >
                <JobCreateForm
                    openHandler={createJobToCharacterProblemModalOpen}
                    add={add}
                />
            </ModalWindow>
        </>
    );
};

export default CreateJobModal;
