import React from 'react';

interface DurationTextProps {
    hours: number;
    minutes: number;
    seconds?: number;
}

const DurationText: React.FC<DurationTextProps> = ({
    hours,
    minutes,
    seconds,
}) => {
    return (
        <>
            {hours !== 0 && `${hours}ч`} {minutes !== 0 && `${minutes}мин`}{' '}
            {!!seconds && seconds !== 0 && `${seconds}сек`}
        </>
    );
};

export default DurationText;
