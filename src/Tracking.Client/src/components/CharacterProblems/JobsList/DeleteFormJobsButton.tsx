import { CHARACTER_PROBLEM_JOBS_NAME } from '@/components/Fields/EntitiesFields/character_problem';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import ActionsMenuButton from '@/components/Tickets/TicketActionsMenu/ActionsMenuButton';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';

interface DeleteFormJobsButtonProps {
    checkHandler: CheckEntitiesHandler;
}

const DeleteFormJobsButton: React.FC<DeleteFormJobsButtonProps> = ({
    checkHandler,
}) => {
    const { remove } = useFormEntityValueArray(CHARACTER_PROBLEM_JOBS_NAME);
    return (
        <>
            <ActionsMenuButton
                symbol='delete_bag'
                text='Удалить'
                onClick={() => {
                    checkHandler.checkedEntities.map(({ id }) => remove(id));
                    checkHandler.uncheckAll();
                }}
            />
        </>
    );
};

export default observer(DeleteFormJobsButton);
