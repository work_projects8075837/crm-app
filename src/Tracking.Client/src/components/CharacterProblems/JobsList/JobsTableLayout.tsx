import EmptyPagination from '@/components/Tables/EmptyPagination/EmptyPagination';
import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import React from 'react';
import style from '../../../style/layout/character_problem_jobs.module.scss';
import { characterProblemJobCheckHandler } from './JobsList';

interface JobsTableLayoutProps {
    children: React.ReactNode;
}

const JobsTableLayout: React.FC<JobsTableLayoutProps> = ({ children }) => {
    return (
        <>
            <TableLayout
                layoutClass={style.entityPaginatedTable}
                header={
                    <TableHeader
                        checkHandler={characterProblemJobCheckHandler}
                        fields={[{ name: 'Название' }, { name: 'Время' }]}
                        layoutClass={style.rowLayout}
                        fieldClass={style.headerField}
                    />
                }
                pagination={<EmptyPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default JobsTableLayout;
