import BaseSelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/BaseSelectedActions';
import TextLoader from '@/components/Elements/TextLoader/TextLoader';
import { CHARACTER_PROBLEM_JOBS_NAME } from '@/components/Fields/EntitiesFields/character_problem';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { IJob } from '@/services/job/types';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import React from 'react';
import DeleteFormJobsButton from './DeleteFormJobsButton';
import JobListItem from './JobListItem';
import JobsTableLayout from './JobsTableLayout';

export const characterProblemJobCheckHandler = new CheckEntitiesHandler();

const JobsList: React.FC = () => {
    const { entities } = useFormEntityValueArray<IJob>(
        CHARACTER_PROBLEM_JOBS_NAME,
    );
    useSetCheckableEntities(characterProblemJobCheckHandler, entities);
    return (
        <>
            {entities?.length ? (
                <JobsTableLayout>
                    {entities.map((j) => (
                        <JobListItem key={j.id} {...j} />
                    ))}
                </JobsTableLayout>
            ) : (
                <TextLoader text='У этого характера нет работ' />
            )}

            <BaseSelectedActions checkHandler={characterProblemJobCheckHandler}>
                <DeleteFormJobsButton
                    checkHandler={characterProblemJobCheckHandler}
                />
            </BaseSelectedActions>
        </>
    );
};

export default JobsList;
