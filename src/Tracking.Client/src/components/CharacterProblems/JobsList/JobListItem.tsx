import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import { countHours } from '@/components/utils/countHours';
import React from 'react';
import style from '../../../style/layout/character_problem_jobs.module.scss';
import DurationText from './DurationText';
import { characterProblemJobCheckHandler } from './JobsList';
interface JobListItemProps {
    id: string;
    name: string;
    duration?: number;
}

const JobListItem: React.FC<JobListItemProps> = ({ id, name, duration }) => {
    const { hours, minutes } = countHours(duration);
    return (
        <div className={style.tableItem}>
            <CheckItem
                checkHandler={characterProblemJobCheckHandler}
                entity={{ id, name }}
            />
            <label className={style.tableItemName}>
                <span>{name}</span>
            </label>

            <label className={style.tableItemName}>
                <DurationText {...{ hours, minutes }} />
            </label>
        </div>
    );
};

export default JobListItem;
