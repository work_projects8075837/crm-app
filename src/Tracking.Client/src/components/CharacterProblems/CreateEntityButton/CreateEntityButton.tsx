import React from 'react';
import style from '../../../style/elements/blue_button.module.scss';
import headerStyle from '../../../style/layout/header.module.scss';
import BlueButton, {
    BlueButtonProps,
} from '@/components/Fields/Buttons/BlueButton';

const CreateEntityButton: React.FC<BlueButtonProps> = ({
    children,
    ...blueButtonProps
}) => {
    return (
        <>
            <div className={headerStyle.headerChildrenContainerEnd}>
                <div className={style.blockStandardWidth}>
                    <BlueButton {...blueButtonProps}>{children}</BlueButton>
                </div>
            </div>
        </>
    );
};

export default CreateEntityButton;
