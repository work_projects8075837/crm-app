import { createJobToCharacterProblemModalOpen } from '@/components/CharacterProblems/CreateJobModal/CreateJobModal';
import { CHARACTER_PROBLEM_JOBS_NAME } from '@/components/Fields/EntitiesFields/character_problem';
import { countHours } from '@/components/utils/countHours';
import { jobHooks } from '@/services/job/job';
import { DirectoryHandler } from '@/store/states/DirectoryHandler';
import { ValueHandler } from '@/store/states/SearchHandler';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import HeaderOption from '../InnerOptions/HeaderOption';
import InnerOptions from '../InnerOptions/InnerOptions';
import { selectJobsModalOpen } from '../SelectJobsModal';

export const jobDirectory = new DirectoryHandler();
export const searchJobs = new ValueHandler('');

const InnerJobs: React.FC = () => {
    const { data } = jobHooks.useSearch(searchJobs.value, {
        job_job_id: jobDirectory.id || null,
        is_hard: true,
    });

    useEffect(() => {
        return () => {
            jobDirectory.setDirectory(null);
        };
    }, []);

    return (
        <>
            <div>
                <HeaderOption
                    fields={[
                        { name: 'Название' },
                        { name: 'Номер' },
                        { name: 'Время' },
                    ]}
                />
                <InnerOptions
                    entitiesData={data?.data.map((j) => {
                        const { hours, minutes } = countHours(j.duration);
                        return [
                            { text: j.id.split('-')[0] },
                            {
                                text: `${hours ? `${hours}ч` : ''} ${
                                    minutes ? `${minutes}мин` : ''
                                }`,
                            },
                        ];
                    })}
                    name={CHARACTER_PROBLEM_JOBS_NAME}
                    entities={data?.data}
                    directoryHandler={jobDirectory}
                    onAddClick={() => {
                        selectJobsModalOpen.close();
                        createJobToCharacterProblemModalOpen.open();
                    }}
                />
            </div>
        </>
    );
};

export default observer(InnerJobs);
