import Svg from '@/components/Elements/Svg/Svg';
import React, { HTMLAttributes } from 'react';
import style from '../../../../style/elements/modal_search.module.scss';

interface ModalSearchInputProps extends HTMLAttributes<HTMLInputElement> {
    value: string;
}

const ModalSearchInput: React.FC<ModalSearchInputProps> = ({
    value,
    ...inputProps
}) => {
    return (
        <>
            <div className={style.field}>
                <div className={style.search}>
                    <Svg symbol='modal_search' />
                </div>
                <input
                    value={value}
                    type='text'
                    placeholder='Поиск...'
                    {...inputProps}
                    className={style.input}
                />
            </div>
        </>
    );
};

export default ModalSearchInput;
