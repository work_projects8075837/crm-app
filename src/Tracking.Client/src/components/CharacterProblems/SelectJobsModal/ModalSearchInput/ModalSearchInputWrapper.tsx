import React from 'react';
import style from '../../../../style/elements/modal_search.module.scss';

interface ModalSearchInputWrapperProps {
    children: React.ReactNode;
}

const ModalSearchInputWrapper: React.FC<ModalSearchInputWrapperProps> = ({
    children,
}) => {
    return <div className={style.filedWrapper}>{children}</div>;
};

export default ModalSearchInputWrapper;
