import { observer } from 'mobx-react-lite';
import React from 'react';
import { searchJobs } from '../InnerJobs/InnerJobs';
import ModalSearchInput from '../ModalSearchInput/ModalSearchInput';
import ModalSearchInputWrapper from '../ModalSearchInput/ModalSearchInputWrapper';

const ModalSearchJobs: React.FC = () => {
    return (
        <>
            <ModalSearchInputWrapper>
                <ModalSearchInput
                    value={searchJobs.value || ''}
                    onChange={(event) =>
                        searchJobs.setValue(event.currentTarget.value)
                    }
                />
            </ModalSearchInputWrapper>
        </>
    );
};

export default observer(ModalSearchJobs);
