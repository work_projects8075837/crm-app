import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import ReturnButton from '@/components/Elements/ReturnButton/ReturnButton';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { CHARACTER_PROBLEM_JOBS_NAME } from '@/components/Fields/EntitiesFields/character_problem';
import SelectEntityForm from '@/components/Form/SelectEntityForm/SelectEntityForm';
import { jobHooks } from '@/services/job/job';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';
import jobsStyle from '../../../style/layout/character_problem_jobs.module.scss';
import style from '../../../style/layout/modal_window.module.scss';
import InnerJobs, { jobDirectory } from './InnerJobs/InnerJobs';
import JobsModalTitle from './JobsModalTitle/JobsModalTitle';
import ModalSearchJobs from './ModalSearchJobs/ModalSearchJobs';

export const selectJobsModalOpen = new OpenHandler(false);

const SelectJobsModal: React.FC = () => {
    const { data } = jobHooks.useOne(jobDirectory.id || undefined);

    return (
        <>
            <ModalWindow
                layoutClass={style.modalLarge}
                headerClass={jobsStyle.modalHeaderLayout}
                title={<JobsModalTitle />}
                openHandler={selectJobsModalOpen}
                headerChildren={<ModalSearchJobs />}
            >
                <SelectEntityForm
                    name={CHARACTER_PROBLEM_JOBS_NAME}
                    openHandler={selectJobsModalOpen}
                >
                    {/* <SearchSelectCharacterProblemJobs /> */}
                    <InnerJobs />

                    <div className={jobsStyle.modalButtons}>
                        <SaveButton>Сохранить</SaveButton>
                        <ReturnButton
                            disabled={!jobDirectory.id}
                            onClick={(event) => {
                                event.preventDefault();
                                jobDirectory.setDirectory(data?.job_id || null);
                            }}
                        >
                            Назад
                        </ReturnButton>
                    </div>
                </SelectEntityForm>
            </ModalWindow>
        </>
    );
};

export default observer(SelectJobsModal);
