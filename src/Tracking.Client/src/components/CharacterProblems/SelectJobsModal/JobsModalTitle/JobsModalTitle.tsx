import { jobHooks } from '@/services/job/job';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { jobDirectory } from '../InnerJobs/InnerJobs';

const JobsModalTitle: React.FC = () => {
    const { data } = jobHooks.useOne(jobDirectory.id || undefined);
    return (
        <>
            {jobDirectory.id
                ? data
                    ? data.name
                    : 'Загрузка ...'
                : 'Выбрать работы'}
        </>
    );
};

export default observer(JobsModalTitle);
