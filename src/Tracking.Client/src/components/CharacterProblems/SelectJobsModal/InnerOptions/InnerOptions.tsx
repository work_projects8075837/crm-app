import DirectoryIcon from '@/components/Elements/DirectoryIcon/DirectoryIcon';
import {
    EntitiesData,
    Entity,
} from '@/components/Fields/SelectEntity/SelectEntity';
import CreateEntityButton from '@/components/Fields/SelectEntity/SelectEntityOption/CreateEntityButton';
import EmptyOptionEntity from '@/components/Fields/SelectEntity/SelectEntityOption/EmptyOptionEntity';
import OptionButtonLayout from '@/components/Fields/SelectEntity/SelectEntityOption/OptionButtonLayout';
import OptionEntityButton from '@/components/Fields/SelectEntity/SelectEntityOption/OptionEntityButton';
import OptionEntityButtonContent from '@/components/Fields/SelectEntity/SelectEntityOption/OptionEntityButtonContent';
import OptionLoader from '@/components/Fields/SelectEntity/SelectEntityOption/OptionLoader';
import { DirectoryHandler } from '@/store/states/DirectoryHandler';
import React from 'react';
import style from '../../../../style/elements/select_option.module.scss';

interface IEntityInner extends Entity {
    is_parent: boolean;
}

interface InnerOptionsProps {
    onAddClick?: () => void;
    entities?: IEntityInner[];
    isCheckbox?: boolean;
    name: string;
    directoryHandler: DirectoryHandler;
    entitiesData?: EntitiesData;
}

const InnerOptions: React.FC<InnerOptionsProps> = ({
    onAddClick,
    entities,
    isCheckbox = true,
    name,
    directoryHandler,
    entitiesData,
}) => {
    return (
        <>
            <div className={style.borderedSelectList}>
                <div className={style.searchSelectChildren}>
                    {entities ? (
                        <>
                            {entities.length ? (
                                <>
                                    {entities.map((entity, i) => {
                                        return entity.is_parent ? (
                                            <OptionButtonLayout
                                                key={entity.id}
                                                onClick={(event) => {
                                                    event.preventDefault();
                                                    directoryHandler.setDirectory(
                                                        entity.id,
                                                    );
                                                }}
                                            >
                                                <OptionEntityButtonContent
                                                    fields={entitiesData?.[i]}
                                                    nameText={
                                                        <>
                                                            <span>
                                                                {entity.name}
                                                            </span>
                                                            <DirectoryIcon />
                                                        </>
                                                    }
                                                />
                                            </OptionButtonLayout>
                                        ) : (
                                            <OptionEntityButton
                                                fields={entitiesData?.[i]}
                                                key={entity.id}
                                                name={name}
                                                nameText={entity.name}
                                                value={entity}
                                                isCheckbox={isCheckbox}
                                            />
                                        );
                                    })}
                                </>
                            ) : (
                                <>
                                    <EmptyOptionEntity text='Нет подходящих вариантов' />
                                </>
                            )}
                        </>
                    ) : (
                        <>
                            <OptionLoader />
                        </>
                    )}
                </div>
                <CreateEntityButton onClick={onAddClick} />
            </div>
        </>
    );
};

export default InnerOptions;
