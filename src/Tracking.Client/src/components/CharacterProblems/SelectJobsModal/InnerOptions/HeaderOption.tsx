import React from 'react';
import style from '../../../../style/elements/select_option.module.scss';

interface HeaderOptionProps {
    fields: { name: string }[];
}

const HeaderOption: React.FC<HeaderOptionProps> = ({ fields }) => {
    return (
        <div className={style.headerOption}>
            {fields.map(({ name }) => (
                <div className={style.optionItem} key={name}>
                    {name}
                </div>
            ))}
        </div>
    );
};

export default HeaderOption;
