import DirectoryHeaderButtons from '@/components/SalePoints/SalePointsHeader/DirectoryHeaderButtons';
import React from 'react';
import CharacterProblemEditLayout from '../CharacterProblemEdit/CharacterProblemEditLayout';
import { createJobToCharacterProblemModalOpen } from '../CreateJobModal/CreateJobModal';
import JobsList from '../JobsList/JobsList';
import { selectJobsModalOpen } from '../SelectJobsModal/SelectJobsModal';

const CharacterProblemJobsPage: React.FC = () => {
    return (
        <>
            <CharacterProblemEditLayout
                headerButton={
                    <DirectoryHeaderButtons
                        link='./'
                        entityText='Добавить работу'
                        directoryText='Добавить папку работ'
                        onEntityClick={() => selectJobsModalOpen.open()}
                        onDirectoryClick={() =>
                            createJobToCharacterProblemModalOpen.open()
                        }
                    />
                    // <CreateEntityButton
                    //     onClick={() => selectJobsModalOpen.open()}
                    // >
                    //     Добавить работу
                    // </CreateEntityButton>
                }
            >
                <JobsList />
            </CharacterProblemEditLayout>
        </>
    );
};

export default CharacterProblemJobsPage;
