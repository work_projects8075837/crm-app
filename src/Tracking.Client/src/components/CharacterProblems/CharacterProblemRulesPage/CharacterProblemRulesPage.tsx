import LargeEditor from '@/components/Fields/LargeEditor/LargeEditor';
import React from 'react';
import CharacterProblemEditLayout from '../CharacterProblemEdit/CharacterProblemEditLayout';

const CharacterProblemRulesPage: React.FC = () => {
    return (
        <>
            <CharacterProblemEditLayout>
                <LargeEditor name='regulations' />
            </CharacterProblemEditLayout>
        </>
    );
};

export default CharacterProblemRulesPage;
