import { CHARACTER_PROBLEM_JOBS_NAME } from '@/components/Fields/EntitiesFields/character_problem';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import EntitiesFormCardsLayout from '@/components/Layouts/EntitiesFormCardsLayout/EntitiesFormCardsLayout';
import { IJob } from '@/services/job/types';
import React from 'react';
import style from '../../../style/layout/character_problem.module.scss';
import { selectJobsModalOpen } from '../SelectJobsModal/SelectJobsModal';
import CharacterProblemJob from './CharacterProblemJob';

const CharacterProblemJobs: React.FC = () => {
    const { entities } = useFormEntityValueArray<IJob>(
        CHARACTER_PROBLEM_JOBS_NAME,
    );

    return (
        <>
            <EntitiesFormCardsLayout
                childrenClass={style.jobsList}
                onPlusClick={() => selectJobsModalOpen.open()}
            >
                {entities?.map((job) => (
                    <CharacterProblemJob key={job.id} {...job} />
                ))}
            </EntitiesFormCardsLayout>
        </>
    );
};

export default CharacterProblemJobs;
