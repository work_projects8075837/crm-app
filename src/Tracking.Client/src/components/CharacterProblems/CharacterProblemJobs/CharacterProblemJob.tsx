import CardClockIcon from '@/components/Elements/CardClockIcon/CardClockIcon';
import { CHARACTER_PROBLEM_JOBS_NAME } from '@/components/Fields/EntitiesFields/character_problem';
import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import SelectedEntityCardLayout from '@/components/Fields/SelectedEntityCard/SelectedEntityCardLayout';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import { countHours } from '@/components/utils/countHours';
import React from 'react';

interface CharacterProblemJobProps {
    id: string;
    name: string;
    duration?: number;
}

const CharacterProblemJob: React.FC<CharacterProblemJobProps> = ({
    id,
    name,
    duration,
}) => {
    const { hours, minutes } = countHours(duration);
    const { remove } = useFormEntityValueArray(CHARACTER_PROBLEM_JOBS_NAME);

    return (
        <>
            <SelectedEntityCardLayout
                id={id}
                linkName='character_problem'
                title='Работа'
                valueName={name}
                closeBtn={<CloseCrossButton onClick={() => remove(id)} />}
            >
                <FullExtraCardFields
                    first={{
                        icon: <CardClockIcon />,
                        text: (
                            <>
                                {hours || minutes ? (
                                    <>
                                        {!!hours && `${hours}ч`}{' '}
                                        {!!minutes && `${minutes}мин`}
                                    </>
                                ) : (
                                    'Не определно'
                                )}
                            </>
                        ),
                    }}
                />
            </SelectedEntityCardLayout>
        </>
    );
};

export default CharacterProblemJob;
