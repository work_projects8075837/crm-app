import React from 'react';
import style from '../../../style/forms/field.module.scss';
import FormInput, { FormInputProps } from '../BaseFields/FormInput';

export interface FormShortInputProps extends FormInputProps {
    className?: string;
    required?: boolean;
}

const FormShortInput: React.FC<FormShortInputProps> = ({
    className = '',
    required = true,
    ...formInputProps
}) => {
    return (
        <>
            <FormInput
                wrapperClass={style.shortInputWrapper}
                {...formInputProps}
                options={{ ...formInputProps.options, required }}
                className={`${style.shortInput} ${className}`}
            />
        </>
    );
};

export default FormShortInput;
