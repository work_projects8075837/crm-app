import { softwareModalOpen } from '@/components/SalePoints/SalePointEditInventory/CreateSoftwareModal/CreateSoftwareModal';
import { selectSoftwareModal } from '@/components/SalePoints/SalePointEditInventory/SelectSoftwareModal/SelectSoftwareModal';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { softwareHooks } from '@/services/software/software';
import React from 'react';
import { INVENTORY_SOFTWARE_NAME } from '../EntitiesFields/inventory';
import SearchCreateSelect from '../SearchCreateSelect/SearchCreateSelect';
import CreateEntityButton from '../SelectEntity/SelectEntityOption/CreateEntityButton';

const SoftwareSearchSelect: React.FC = () => {
    const model = useSearchInputModel();
    const { data } = softwareHooks.useSearch(model.value);
    return (
        <>
            <SearchCreateSelect
                name={INVENTORY_SOFTWARE_NAME}
                searchModelState={model}
                entities={data?.data}
                addButton={
                    <CreateEntityButton
                        onClick={() => {
                            selectSoftwareModal.close();
                            softwareModalOpen.open();
                        }}
                    />
                }
            />
        </>
    );
};

export default React.memo(SoftwareSearchSelect);
