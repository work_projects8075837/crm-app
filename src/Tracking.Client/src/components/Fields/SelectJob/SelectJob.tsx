import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { jobHooks } from '@/services/job/job';
import React from 'react';
import { JOB_JOB_NAME } from '../EntitiesFields/job';
import SelectEntity from '../SelectEntity/SelectEntity';

interface SelectJobProps {
    onlyDirectory?: boolean;
}

const SelectJob: React.FC<SelectJobProps> = ({ onlyDirectory = false }) => {
    const model = useSearchInputModel();
    const { data } = jobHooks.useSearch(model.value);
    const jobs = data?.data.filter((j) => j.is_parent === onlyDirectory);

    return (
        <>
            <SelectEntity
                placeholder='Выбрать папку работ'
                name={JOB_JOB_NAME}
                entities={jobs}
                searchInputModel={model}
            />
        </>
    );
};

export default SelectJob;
