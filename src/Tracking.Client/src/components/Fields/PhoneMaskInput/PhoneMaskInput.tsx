import { useTextInputModel } from '@/hooks/useTextInputModel';
import { StylePhoneInput } from '@/style/elements/phone_input';
import { InputMask } from '@react-input/mask';
import classNames from 'classnames';
import React, { InputHTMLAttributes, useRef, useState } from 'react';

interface PhoneMaskInputProps
    extends Omit<InputHTMLAttributes<HTMLInputElement>, 'placeholder'> {
    className?: string;
}

const PhoneMaskInput: React.ForwardRefRenderFunction<
    HTMLInputElement,
    PhoneMaskInputProps
> = ({ className, value, onChange, ...inputProps }, ref) => {
    const code_number = value ? `${value}`?.split('(') : '';
    const defaultCode = code_number[0] ? code_number[0] : '+7';
    const defaultNumber = code_number[1]
        ? `(${code_number[1]}`
        : '(___)___-__-__';

    const [code, _, setCode] = useTextInputModel(defaultCode, /^\+[\d]+$/);
    const [number, setNumber] = useState(defaultNumber);

    const codeElemRef = useRef<HTMLInputElement>(null);

    return (
        <>
            <div className={classNames(StylePhoneInput.inputs, className)}>
                <InputMask
                    ref={codeElemRef}
                    {...inputProps}
                    style={
                        {
                            // width: code.replaceAll(' ', '')?.length * 10 + 10 && ,
                        }
                    }
                    {...{
                        mask: '+   ',
                        replacement: { ' ': /\d/ },
                        showMask: true,
                    }}
                    value={code}
                    className={classNames(StylePhoneInput.code)}
                    onChange={(event) => {
                        const v = event.currentTarget.value;
                        setCode(v);
                        event.currentTarget.value = v + number;
                        if (onChange) onChange(event);
                    }}
                />
                <InputMask
                    ref={ref}
                    {...inputProps}
                    {...{
                        mask: '(___)___-__-__',
                        replacement: { _: /\d/ },
                        showMask: true,
                    }}
                    value={number}
                    className={classNames(StylePhoneInput.number)}
                    onChange={(event) => {
                        const v = event.currentTarget.value;

                        setNumber(v);
                        event.currentTarget.value = code + v;
                        if (onChange) onChange(event);
                    }}
                />
            </div>
        </>
    );
};

export default React.memo(React.forwardRef(PhoneMaskInput));
