import React from 'react';
import FormInput, { FormInputProps } from '../BaseFields/FormInput';

interface RepeatPasswordProps extends FormInputProps {
    passwordName?: string;
}

const RepeatPassword: React.FC<RepeatPasswordProps> = ({
    options,
    passwordName = 'password',
    ...formInputProps
}) => {
    return (
        <>
            <FormInput
                type='password'
                {...formInputProps}
                options={{
                    ...options,
                    validate: {
                        password_compare: (value, values) => {
                            return (
                                (values[passwordName] &&
                                    value === values[passwordName]) ||
                                'Пароли не совпадают'
                            );
                        },
                    },
                }}
            />
        </>
    );
};

export default RepeatPassword;
