import React from 'react';
import { Entity, EntityDataField } from '../SelectEntity';
import OptionEntityButtonCheckbox from './OptionEntityButtonCheckbox';
import OptionEntityButtonContent from './OptionEntityButtonContent';
import OptionEntityButtonRadio from './OptionEntityButtonRadio';

interface OptionEntityButtonProps {
    nameText?: string;
    name: string;
    value: Entity;
    fields?: EntityDataField[];
    isCheckbox: boolean;
    onClose?: () => void;
    onChangeAction?: (v: any) => void;
}

const OptionEntityButton: React.FC<OptionEntityButtonProps> = ({
    nameText,
    name,
    value,
    fields,
    isCheckbox,
    onClose,
    onChangeAction,
}) => {
    return (
        <>
            {isCheckbox ? (
                <>
                    <OptionEntityButtonCheckbox
                        name={name}
                        value={value}
                        onChangeAction={onChangeAction}
                    >
                        <OptionEntityButtonContent
                            nameText={nameText}
                            fields={fields}
                        />
                    </OptionEntityButtonCheckbox>
                </>
            ) : (
                <>
                    <OptionEntityButtonRadio
                        name={name}
                        value={value}
                        onClose={onClose}
                        onChangeAction={onChangeAction}
                    >
                        <OptionEntityButtonContent
                            nameText={nameText}
                            fields={fields}
                        />
                    </OptionEntityButtonRadio>
                </>
            )}
        </>
    );
};

export default OptionEntityButton;
