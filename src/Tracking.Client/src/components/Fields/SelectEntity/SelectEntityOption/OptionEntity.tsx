import React from 'react';
import style from '../../../style/elements/select_option.module.scss';
import '../../../style/elements/entity.scss';
import { EntityDataField } from '../SelectEntity';
import Svg from '@/components/Elements/Svg/Svg';

interface OptionEntityProps extends React.HTMLAttributes<HTMLInputElement> {
    nameText: string;
    value: string;
    fields?: EntityDataField[];
    isCheckbox: boolean;
}

const OptionEntity: React.ForwardRefRenderFunction<
    HTMLInputElement,
    OptionEntityProps
> = ({ nameText, value, fields, isCheckbox, ...inputProps }, ref) => {
    return (
        <label className={style.optionLabel}>
            <input
                type={isCheckbox ? 'checkbox' : 'radio'}
                className='entityInput'
                {...inputProps}
                ref={ref}
                value={value}
            />
            <div className={`${style.option} entityContent`}>
                <span className={style.optionName}>
                    {nameText || 'Без имени'}
                </span>
                {fields?.map((field) => (
                    <span className={style.optionItem} key={field.text}>
                        {!!field.symbol && (
                            <Svg
                                className={style.optionItemSvg}
                                symbol={field.symbol}
                            />
                        )}
                        <span>{field.text}</span>
                    </span>
                ))}
            </div>
        </label>
    );
};

export default React.forwardRef(OptionEntity);
