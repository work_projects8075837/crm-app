import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import classNames from 'classnames';
import React from 'react';
import style from '../../../../style/elements/loader_text.module.scss';

interface OptionLoaderProps {
    size?: number;
    className?: string;
}

const OptionLoader: React.FC<OptionLoaderProps> = ({
    size = 20,
    className,
}) => {
    return (
        <>
            <LoaderCircle
                size={size}
                borderWidth={1}
                className={classNames(style.optionLoader, className)}
            />
        </>
    );
};

export default OptionLoader;
