import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../../style/elements/select_option.module.scss';
import { EntityDataField } from '../SelectEntity';

interface OptionEntityButtonContentProps {
    nameText?: string | React.ReactNode;
    fields?: EntityDataField[];
}

const OptionEntityButtonContent: React.FC<OptionEntityButtonContentProps> = ({
    fields,
    nameText,
}) => {
    return (
        <>
            <span className={style.optionName}>{nameText || 'Без имени'}</span>
            {fields?.map((field, index) => (
                <span
                    className={style.optionItem}
                    key={index}
                    style={{ width: field.width }}
                >
                    {field.text && (
                        <>
                            {!!field.symbol && (
                                <Svg
                                    className={style.optionItemSvg}
                                    symbol={field.symbol}
                                />
                            )}
                            <span>{field.text}</span>
                        </>
                    )}
                </span>
            ))}
        </>
    );
};

export default OptionEntityButtonContent;
