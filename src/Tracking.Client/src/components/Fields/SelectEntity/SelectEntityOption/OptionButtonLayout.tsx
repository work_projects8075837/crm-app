import React, { HTMLAttributes } from 'react';
import style from '../../../../style/elements/select_option.module.scss';

interface OptionButtonLayoutProps extends HTMLAttributes<HTMLDivElement> {
    isSelected?: boolean;
    disabled?: boolean;
}

const OptionButtonLayout: React.FC<OptionButtonLayoutProps> = ({
    isSelected = false,
    disabled = false,
    onClick,
    children,
    ...buttonProps
}) => {
    return (
        <>
            <div
                onClick={disabled ? undefined : onClick}
                {...buttonProps}
                className={`${style.optionButton} ${
                    isSelected ? style.optionSelected : ''
                }`}
            >
                {children}
            </div>
        </>
    );
};

export default OptionButtonLayout;
