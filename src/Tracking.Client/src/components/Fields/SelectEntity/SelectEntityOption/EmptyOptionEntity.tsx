import React from 'react';
import style from '../../../../style/elements/select_option.module.scss';

interface EmptyOptionEntityProps {
    text?: string;
    onClick?: () => void;
}

const EmptyOptionEntity: React.FC<EmptyOptionEntityProps> = ({
    text = 'Нет вариантов',
    onClick,
}) => {
    return (
        <>
            <div className={style.optionLabel}>
                <div className={`${style.option}`} onClick={onClick}>
                    <span className={style.optionName}>{text}</span>
                </div>
            </div>
        </>
    );
};

export default EmptyOptionEntity;
