import React, { RefObject } from 'react';
import { useFormContext } from 'react-hook-form';
import { useFormEntityValueArray } from '../../hooks/useFormEntityValueArray';
import { Entity } from '../SelectEntity';
import OptionButtonLayout from './OptionButtonLayout';

interface OptionEntityButtonCheckboxProps {
    children: React.ReactNode;
    name: string;
    value: Entity;
    onChangeAction?: (v: any) => void;
    focusInput?: RefObject<HTMLInputElement | null>;
}

const OptionEntityButtonCheckbox: React.FC<OptionEntityButtonCheckboxProps> = ({
    children,
    name,
    value,
    onChangeAction,
    focusInput,
}) => {
    const { add, remove, entities } = useFormEntityValueArray(name);
    const { clearErrors } = useFormContext();

    const isSelected = entities?.some(({ id }) => id === value.id);

    return (
        <>
            <OptionButtonLayout
                isSelected={isSelected}
                onClick={(event) => {
                    event.preventDefault();
                    if (isSelected) {
                        remove(value.id);
                    } else {
                        add(value);
                        onChangeAction?.(value);
                    }
                    clearErrors(name);
                    focusInput?.current?.focus();
                }}
            >
                {children}
            </OptionButtonLayout>
        </>
    );
};

export default OptionEntityButtonCheckbox;
