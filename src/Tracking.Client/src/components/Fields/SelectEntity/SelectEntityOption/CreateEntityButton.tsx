import React from 'react';
import style from '../../../../style/elements/select_option.module.scss';
import Svg from '@/components/Elements/Svg/Svg';
import { performExistCallback } from '@/components/utils/performExistCallback';

interface CreateEntityButtonProps {
    className?: string;
    onClick?: () => void;
}

const CreateEntityButton: React.FC<CreateEntityButtonProps> = ({
    onClick,
    className = '',
}) => {
    return (
        <button
            className={`${style.createButton} ${className}`}
            onClick={(event) => {
                event.preventDefault();
                performExistCallback(onClick);
            }}
        >
            <Svg symbol='add_plus' className={style.createButtonIcon} />
            <span>Добавить</span>
        </button>
    );
};

export default CreateEntityButton;
