import React, { RefObject } from 'react';
import { useFormContext } from 'react-hook-form';
import { useFormEntityValue } from '../../hooks/useFormEntityValue';
import { Entity } from '../SelectEntity';
import OptionButtonLayout from './OptionButtonLayout';

interface OptionEntityButtonRadioProps {
    children: React.ReactNode;
    name: string;
    value: Entity;
    onClose?: () => void;
    onChangeAction?: (v: any) => void;
    focusInput?: RefObject<HTMLInputElement | null>;
}

const OptionEntityButtonRadio: React.FC<OptionEntityButtonRadioProps> = ({
    children,
    name,
    value,
    onClose,
    onChangeAction,
    focusInput,
}) => {
    const { set, clear, currentEntity } = useFormEntityValue(name);

    const { clearErrors } = useFormContext();

    const isSelected = currentEntity?.id === value.id;
    return (
        <>
            <OptionButtonLayout
                isSelected={isSelected}
                onClick={(event) => {
                    event.preventDefault();
                    if (!isSelected) {
                        set(value);
                        if (onChangeAction) onChangeAction(value);
                    }

                    onClose?.();

                    clearErrors(name);
                    focusInput?.current?.focus();
                }}
            >
                {children}
            </OptionButtonLayout>
        </>
    );
};

export default OptionEntityButtonRadio;
