import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import React, { useDeferredValue } from 'react';
import style from '../../../../style/elements/loader_text.module.scss';
import { EntitiesData, Entity } from '../SelectEntity';
import EmptyOptionEntity from '../SelectEntityOption/EmptyOptionEntity';
import OptionEntityButton from '../SelectEntityOption/OptionEntityButton';

interface SelectEntityOptionsProps {
    name: string;
    entities?: Entity[];
    searchIndexing: boolean;
    isCheckbox: boolean;
    search: string;
    entitiesData?: EntitiesData;
    setSearch: React.Dispatch<React.SetStateAction<string>>;
    onChangeAction?: (v?: any) => void;
    close: () => void;
}

const SelectEntityOptions: React.FC<SelectEntityOptionsProps> = ({
    name,
    entities,
    searchIndexing,
    isCheckbox,
    search,
    entitiesData,
    setSearch,
    onChangeAction,
    close,
}) => {
    const filteredEntities = useDeferredValue(
        searchIndexing
            ? entities?.filter((entity) => {
                  return entity.name
                      ? entity.name
                            .toLocaleLowerCase()
                            .includes(search.toLocaleLowerCase())
                      : true;
              })
            : entities,
    );

    const closeClear = () => {
        setSearch('');
        close();
    };

    return (
        <>
            {filteredEntities ? (
                <>
                    {filteredEntities.length ? (
                        <>
                            {filteredEntities.map((entity, index) => (
                                <OptionEntityButton
                                    isCheckbox={isCheckbox}
                                    key={entity.id}
                                    nameText={entity.name || ''}
                                    value={entity}
                                    name={name}
                                    fields={entitiesData?.[index]}
                                    onClose={closeClear}
                                    onChangeAction={onChangeAction}
                                />
                            ))}
                        </>
                    ) : (
                        <>
                            <EmptyOptionEntity onClick={closeClear} />
                        </>
                    )}
                </>
            ) : (
                <>
                    <LoaderCircle
                        size={20}
                        borderWidth={1}
                        className={style.optionLoader}
                    />
                </>
            )}
        </>
    );
};

export default SelectEntityOptions;
