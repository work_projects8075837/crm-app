import React from 'react';
import style from '../../../../style/elements/select_option.module.scss';

interface SelectEntityDropDownProps {
    children: React.ReactNode;
    isOpen: boolean;
    bottomButton?: React.ReactNode;
    focus?: () => void;
}

const SelectEntityDropDown: React.FC<SelectEntityDropDownProps> = ({
    isOpen,
    children,
    bottomButton,
    focus,
}) => {
    return (
        <>
            {isOpen && (
                <>
                    <div className={style.selectTrack} onClick={focus}>
                        <div className={style.selectChildren}>{children}</div>
                        {bottomButton}
                    </div>
                </>
            )}
        </>
    );
};

export default SelectEntityDropDown;
