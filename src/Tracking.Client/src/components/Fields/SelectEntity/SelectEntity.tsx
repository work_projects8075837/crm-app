import { useBooleanState } from '@/hooks/useBooleanState';
import { useOutsideClose } from '@/hooks/useOutsideClose';
import { ITextInputModel, useTextInputModel } from '@/hooks/useTextInputModel';
import React, { useEffect } from 'react';
import { useFormContext } from 'react-hook-form';
import style from '../../../style/elements/select_option.module.scss';
import { useCustomFucus } from '../hooks/useCustomFucus';
import SelectEntityHeader from './SelectEntityHeader/SelectEntityHeader';
import CreateEntityButton from './SelectEntityOption/CreateEntityButton';
import SelectEntityDropDown from './SelectEntityOptions/SelectEntityDropDown';
import SelectEntityOptions from './SelectEntityOptions/SelectEntityOptions';

export interface SearchInputModelState {
    value: string;
    model: ITextInputModel;
    setSearch: React.Dispatch<React.SetStateAction<string>>;
}

export interface Entity {
    id: string;
    name?: string;
}

export interface EntityDataField {
    symbol?: string;
    text: string;
    width?: string | number;
}

export type EntitiesData = EntityDataField[][];

interface SelectEntityProps {
    entities: Entity[] | undefined;
    entitiesData?: EntitiesData;
    name: string;
    isDisabled?: boolean;
    isRequired?: boolean;
    placeholder?: string;
    onChangeAction?: (v: any) => void;
    isCheckbox?: boolean;
    searchInputModel?: SearchInputModelState;
    onCreateClick?: () => void;
    fieldClass?: string;
    setValueAs?: <T extends Entity, R>(v: T) => R;
}

const SelectEntity = ({
    isCheckbox = false,
    isDisabled = false,
    isRequired = false,
    placeholder = 'Выбрать',
    fieldClass = '',
    name,
    entities,
    entitiesData,
    onChangeAction,
    searchInputModel,
    onCreateClick,
}: SelectEntityProps) => {
    const [isOpen, handler] = useBooleanState();
    const [search, searchModel, setSearch] = useTextInputModel();
    const { register } = useFormContext();

    const ref = useOutsideClose(handler.off);

    useEffect(() => {
        register(name, {
            required: isRequired ? 'Это поле обязательно' : false,
        });
    }, []);

    const { inputRef, focus } = useCustomFucus();

    return (
        <>
            <div
                className={style.select}
                onClick={(event) => {
                    event.stopPropagation();
                }}
                ref={ref}
            >
                <input className='opacityInput' ref={inputRef} />
                <SelectEntityHeader
                    {...{
                        ...handler,
                        open: () => handler.on(),
                        name,
                        isOpen,
                        isDisabled,
                        isCheckbox,
                        placeholder,
                        searchModel: searchInputModel?.model || searchModel,
                        searchIndexing: !searchInputModel,
                        fieldClass,
                    }}
                />
                <SelectEntityDropDown
                    {...{ isOpen, focus }}
                    bottomButton={
                        <>
                            {!!onCreateClick && (
                                <CreateEntityButton
                                    onClick={() => {
                                        if (onCreateClick) onCreateClick();
                                        handler.off();
                                    }}
                                />
                            )}
                        </>
                    }
                >
                    <SelectEntityOptions
                        {...{
                            close: () => handler.off(),
                            isCheckbox,
                            entities,
                            entitiesData,
                            name,
                            search,
                            searchIndexing: !searchInputModel,
                            setSearch: searchInputModel?.setSearch || setSearch,
                            onChangeAction,
                        }}
                    />
                </SelectEntityDropDown>
            </div>
        </>
    );
};

export default React.memo(SelectEntity);
