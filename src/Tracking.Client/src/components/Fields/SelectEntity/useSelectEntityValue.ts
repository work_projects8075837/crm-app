import { useWatch } from 'react-hook-form';
import { Entity } from './SelectEntity';

export const useSelectEntityValue = <T extends Entity = Entity>(
    name: string,
    entities?: T[],
) => {
    // const { setValue, getValues } = useFormContext();
    const currentEntityId = useWatch({ name });
    // useEffect(() => {
    //     if (entities[0]?.id && !getValues(name)) setValue(name, entities[0].id);
    // }, []);

    const selectValue = entities?.find(
        (entity) => currentEntityId && entity.id === currentEntityId,
    );

    return selectValue;
};
