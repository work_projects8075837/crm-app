import ArrowOpen from '@/components/Elements/FormSelect/ArrowOpen';
import { ITextInputModel } from '@/hooks/useTextInputModel';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import style from '../../../../style/elements/select_option.module.scss';
import SelectedEntityName from './SelectedEntityName';

interface SelectEntityHeaderProps {
    isOpen: boolean;
    placeholder: string;
    searchModel: ITextInputModel;
    toggle: () => void;
    open: () => void;
    isDisabled: boolean;
    isCheckbox: boolean;
    name: string;
    fieldClass: string;
}

const focusInput = (inputElem: HTMLInputElement) => {
    inputElem?.focus();
};

const SelectEntityHeader: React.FC<SelectEntityHeaderProps> = ({
    isOpen,
    placeholder,
    searchModel,
    toggle,
    open,
    isDisabled,
    isCheckbox,
    name,
    fieldClass,
}) => {
    const { formState, getFieldState, clearErrors } = useFormContext();
    const { error } = getFieldState(name, formState);

    const onOpen = () => {
        open();
        clearErrors(name);
    };

    return (
        <>
            <div
                className={`${style.selectHeader} 
                ${fieldClass}
                ${isOpen ? style.selectHeaderOpen : ''}
                ${isDisabled ? style.selectHeaderDisabled : ''}
                ${error ? style.selectHeaderError : ''}

                `}
            >
                {isOpen ? (
                    <>
                        <input
                            placeholder={placeholder}
                            className={style.selectHeaderInput}
                            type='text'
                            name={`${name}__search`}
                            {...searchModel}
                            ref={focusInput}
                        />
                    </>
                ) : (
                    <>
                        <div
                            className={style.selectHeaderButton}
                            onClick={
                                isDisabled
                                    ? undefined
                                    : (event) => {
                                          event.preventDefault();
                                          onOpen();
                                      }
                            }
                        >
                            {isCheckbox ? (
                                placeholder
                            ) : (
                                <SelectedEntityName
                                    name={name}
                                    placeholder={placeholder}
                                />
                            )}
                        </div>
                    </>
                )}

                <div
                    className={style.selectHeaderSvgButton}
                    onClick={
                        isDisabled
                            ? undefined
                            : (event) => {
                                  event.preventDefault();
                                  toggle();
                                  clearErrors(name);
                              }
                    }
                >
                    <ArrowOpen isOpen={isOpen} />
                </div>
            </div>
        </>
    );
};

export default React.memo(SelectEntityHeader);
