import React from 'react';
import { useFormEntityValue } from '../../hooks/useFormEntityValue';

interface SelectedEntityNameProps {
    name: string;
    placeholder: string;
}

const SelectedEntityName: React.FC<SelectedEntityNameProps> = ({
    name,
    placeholder,
}) => {
    const { currentEntity } = useFormEntityValue(name);

    return <>{currentEntity?.name || placeholder}</>;
};

export default SelectedEntityName;
