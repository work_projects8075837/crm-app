import { useCallback } from 'react';
import { useFormContext, useWatch } from 'react-hook-form';
import { Entity } from '../SelectEntity/SelectEntity';

export const useFormEntityValue = <T extends Entity>(name: string) => {
    const { setValue } = useFormContext();
    const currentEntity: T | undefined = useWatch({ name });

    const set = useCallback((entity: T) => {
        setValue(name, entity);
    }, []);

    const clear = useCallback(() => {
        setValue(name, undefined);
    }, []);

    return { currentEntity, set, clear };
};
