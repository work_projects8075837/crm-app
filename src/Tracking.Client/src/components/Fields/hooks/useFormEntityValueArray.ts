import { useCallback } from 'react';
import { useFormContext, useWatch } from 'react-hook-form';
import { Entity } from '../SelectEntity/SelectEntity';

export const useFormEntityValueArray = <T extends Entity>(name: string) => {
    const { setValue, getValues } = useFormContext();
    const entities: T[] | undefined = useWatch({ name });

    const remove = useCallback((entityId: string) => {
        const oldEntities: T[] | undefined = getValues(name);
        if (!oldEntities) return;

        const mewEntities = oldEntities.filter(
            (entity) => entity.id !== entityId,
        );
        setValue(name, mewEntities);
    }, []);

    const update = useCallback((entityId: string, data: Partial<T>) => {
        const oldEntities: T[] | undefined = getValues(name);
        if (!oldEntities) return;

        const mewEntities = oldEntities.map((entity) => {
            return entity.id === entityId ? { ...entity, ...data } : entity;
        });

        setValue(name, mewEntities);
    }, []);

    const add = useCallback((entity: T) => {
        const oldEntities: T[] | undefined = getValues(name);

        setValue(name, [entity, ...(oldEntities || [])]);
    }, []);

    const addArray = useCallback((newEntities: T[]) => {
        const oldEntities: T[] | undefined = getValues(name);
        if (oldEntities) {
            setValue(name, [...oldEntities, ...newEntities]);
        } else {
            setValue(name, [...newEntities]);
        }
    }, []);

    const setArray = useCallback((newEntities: T[]) => {
        setValue(name, [...newEntities]);
    }, []);

    return { entities, remove, add, addArray, setArray, update };
};
