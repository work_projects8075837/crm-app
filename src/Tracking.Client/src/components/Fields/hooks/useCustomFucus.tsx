import { useRef } from 'react';

export const useCustomFucus = () => {
    const inputRef = useRef<HTMLInputElement>(null);

    const focus = () => inputRef.current?.focus();

    return { inputRef, focus };
};
