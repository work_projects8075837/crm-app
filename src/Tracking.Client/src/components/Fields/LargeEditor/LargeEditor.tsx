import React from 'react';
import EditorInput from '../EditorInput/EditorInput';

interface LargeEditorProps {
    name: string;
}

const LargeEditor: React.FC<LargeEditorProps> = ({ name }) => {
    return (
        <>
            <EditorInput
                name={name}
                className='largeEditor'
                wrapperClass='largeEditorWrapper'
            />
        </>
    );
};

export default LargeEditor;
