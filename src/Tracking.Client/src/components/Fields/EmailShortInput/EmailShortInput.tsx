import React from 'react';
import { FormInputProps } from '../BaseFields/FormInput';
import FormShortInput from '../FormShortInput/FormShortInput';

const EmailShortInput: React.FC<FormInputProps> = (props) => {
    return (
        <>
            <FormShortInput
                {...props}
                options={{
                    pattern: {
                        value: /^[\w-\.]+@flagman-it.ru$/,
                        // value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
                        message: 'Неверный формат E-mail',
                    },
                }}
            />
        </>
    );
};

export default EmailShortInput;
