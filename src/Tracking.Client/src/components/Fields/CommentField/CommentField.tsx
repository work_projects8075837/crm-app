import React from 'react';
import EditorInput from '../EditorInput/EditorInput';
import { SALE_POINT_COMMENT_NAME } from '../EntitiesFields/sale_point';

const CommentField: React.FC = () => {
    return (
        <>
            <EditorInput name={SALE_POINT_COMMENT_NAME} />
        </>
    );
};

export default CommentField;
