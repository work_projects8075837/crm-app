import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { SALE_POINT_SERVICE_FINISH_NAME } from '../EntitiesFields/sale_point';
import SelectEntity, { Entity } from '../SelectEntity/SelectEntity';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';

interface SelectServiceStatusProps {
    name?: string;
    title?: string;
}

export const SERVICE_STATUSES = {
    subscriber: 'Абонент',
    not_subscriber: 'Не абонент',
} as const;

const SelectServiceStatus: React.FC<SelectServiceStatusProps> = ({
    name = 'status',
    title = 'Статус обслуживания',
}) => {
    const { setValue, watch } = useFormContext();

    return (
        <>
            <SelectedEntityCard
                name={name}
                title={title}
                onCloseClick={() => {
                    setValue(SALE_POINT_SERVICE_FINISH_NAME, undefined);
                }}
            >
                <EntitySelectFieldLayout label={title}>
                    <SelectEntity
                        entities={[
                            {
                                id: SERVICE_STATUSES.subscriber,
                                name: SERVICE_STATUSES.subscriber,
                            },
                            {
                                id: SERVICE_STATUSES.not_subscriber,
                                name: SERVICE_STATUSES.not_subscriber,
                            },
                        ]}
                        name={name}
                        onChangeAction={(status: Entity) => {
                            if (status.id === SERVICE_STATUSES.not_subscriber) {
                                setValue(
                                    SALE_POINT_SERVICE_FINISH_NAME,
                                    undefined,
                                );
                            }
                        }}
                    />
                </EntitySelectFieldLayout>
            </SelectedEntityCard>
        </>
    );
};

export default SelectServiceStatus;
