import { Input } from '@chakra-ui/react';
import React from 'react';
import { RegisterOptions, useFormContext } from 'react-hook-form';
import style from '../../../style/forms/field.module.scss';

interface TimeInputProps {
    name: string;
    options?: RegisterOptions;
    wrapperClass?: string;
}

const TimeInput: React.FC<TimeInputProps> = ({
    name,
    options,
    wrapperClass = '',
}) => {
    const { formState, getFieldState, register } = useFormContext();
    const { error, isTouched } = getFieldState(name, formState);

    const isError = !!error && isTouched;

    return (
        <>
            <div className={wrapperClass}>
                <Input
                    height={38}
                    className={style.input}
                    type='time'
                    {...register(name, {
                        required: 'Это обязательное поле',
                        ...options,
                    })}
                />
                {isError && (
                    <span className={style.errorMessage}>{error.message}</span>
                )}
            </div>
        </>
    );
};

export default TimeInput;
