import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { contactHooks } from '@/services/contact/contact';
import React from 'react';
import SearchCreateSelect from '../SearchCreateSelect/SearchCreateSelect';
import CreateEntityButton from '../SelectEntity/SelectEntityOption/CreateEntityButton';

interface SearchSelectContactProps {
    name: string;
    onCreateClick?: (search: string) => void;
    isCheckbox?: boolean;
}

const SearchSelectContact: React.FC<SearchSelectContactProps> = ({
    name,
    onCreateClick,
    isCheckbox = false,
}) => {
    const model = useSearchInputModel();
    const { data } = contactHooks.useSearch(model.value);

    const contactsData = data?.data.map((contact) => {
        return [
            {
                symbol: 'phone',
                text: contact.phone_number.map((p) => p.name).join(', '),
                width: '300%',
            },
        ];
    });

    return (
        <>
            <SearchCreateSelect
                placeholder='Поиск контакта...'
                name={name}
                entities={data?.data}
                entitiesData={contactsData}
                searchModelState={model}
                isCheckbox={isCheckbox}
                addButton={
                    <CreateEntityButton
                        onClick={() => {
                            if (onCreateClick) onCreateClick(model.value);
                        }}
                    />
                }
            />
        </>
    );
};

export default SearchSelectContact;
