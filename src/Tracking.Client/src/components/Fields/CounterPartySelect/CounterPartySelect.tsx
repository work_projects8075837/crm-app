import CardTextIcon from '@/components/Elements/CardTextIcon/CardTextIcon';
import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import ExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import { ICounterparty } from '@/services/counterparty/types';
import { useCounterPartiesBySalePoint } from '@/store/queries/stores/counterparty/useCounterPartyBySalePoint';
import React from 'react';
import {
    TICKET_COUNTER_PARTY_NAME,
    TICKET_SALE_POINT_NAME,
} from '../EntitiesFields/ticket';
import SelectEntity from '../SelectEntity/SelectEntity';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';
import { useFormEntityValue } from '../hooks/useFormEntityValue';

interface CounterPartySelectProps {
    onlyDirectory?: boolean;
}

const CounterPartySelect: React.FC<CounterPartySelectProps> = () => {
    const formSalePoint = useFormEntityValue(TICKET_SALE_POINT_NAME);
    const salePointId = formSalePoint.currentEntity?.id;

    const { data } = useCounterPartiesBySalePoint({ salePointId });

    const formCounterParty = useFormEntityValue<ICounterparty>(
        TICKET_COUNTER_PARTY_NAME,
    );
    const currentCounterParty = formCounterParty.currentEntity;

    return (
        <>
            <SelectedEntityCard
                linkName='counterparty'
                name={TICKET_COUNTER_PARTY_NAME}
                title='Контрагент'
                extraFields={
                    <ExtraCardFields
                        first={{
                            icon: <CardTextIcon text='ИНН:' />,
                            text: currentCounterParty?.tin,
                        }}
                        second={{
                            icon: <CardTextIcon text='ОГРН:' />,
                            text: currentCounterParty?.tag,
                        }}
                    />
                }
            >
                <EntitySelectFieldLayout label='Контрагент'>
                    <SelectEntity
                        name={TICKET_COUNTER_PARTY_NAME}
                        entities={data}
                        isDisabled={!salePointId}
                    />
                </EntitySelectFieldLayout>
            </SelectedEntityCard>
        </>
    );
};

export default React.memo(CounterPartySelect);
