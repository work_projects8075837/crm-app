import CardTextIcon from '@/components/Elements/CardTextIcon/CardTextIcon';
import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { counterPartyHooks } from '@/services/counterparty/counterparty';
import { ICounterparty } from '@/services/counterparty/types';
import React, { useDeferredValue } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { TICKET_COUNTER_PARTY_NAME } from '../EntitiesFields/ticket';
import SelectEntity from '../SelectEntity/SelectEntity';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';
import { useFormEntityValue } from '../hooks/useFormEntityValue';

interface CounterPartyFullSelectProps {
    onlyDirectory?: boolean;
    title?: string;
}

const CounterPartyFullSelect: React.FC<CounterPartyFullSelectProps> = ({
    onlyDirectory = false,
    title = 'Контрагент',
}) => {
    const model = useSearchInputModel();
    const { data } = counterPartyHooks.useSearch(model.value);
    const { counterPartyId } = useParams();
    const { pathname } = useLocation();

    const counterParties = useDeferredValue(
        data?.data.filter(
            (c) =>
                c.is_parent === onlyDirectory &&
                (c.id !== counterPartyId || pathname.includes('create')),
        ),
    );

    const formCounterParty = useFormEntityValue<ICounterparty>(
        TICKET_COUNTER_PARTY_NAME,
    );
    const currentCounterParty = formCounterParty.currentEntity;

    return (
        <>
            <SelectedEntityCard
                linkName='counterparty'
                name={TICKET_COUNTER_PARTY_NAME}
                title={title}
                extraFields={
                    <>
                        {!onlyDirectory && (
                            <FullExtraCardFields
                                first={{
                                    icon: <CardTextIcon text='ИНН:' />,
                                    text: currentCounterParty?.tin,
                                }}
                                second={{
                                    icon: <CardTextIcon text='ОГРН:' />,
                                    text: currentCounterParty?.tag,
                                }}
                            />
                        )}
                    </>
                }
            >
                <EntitySelectFieldLayout label={title}>
                    <SelectEntity
                        searchInputModel={model}
                        name={TICKET_COUNTER_PARTY_NAME}
                        entities={counterParties}
                    />
                </EntitySelectFieldLayout>
            </SelectedEntityCard>
        </>
    );
};

export default CounterPartyFullSelect;
