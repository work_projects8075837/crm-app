import { StylePhoneInput } from '@/style/elements/phone_input';
import React from 'react';
import { FormShortInputProps } from '../FormShortInput/FormShortInput';
import PhoneInput from '../PhoneInput/PhoneInput';

const PhoneShortInput: React.FC<FormShortInputProps> = ({
    className = '',
    required = true,
    ...formInputProps
}) => {
    return (
        <>
            <PhoneInput
                wrapperClass={StylePhoneInput.shortInputWrapper}
                {...formInputProps}
                options={{
                    ...formInputProps.options,
                    required: required ? 'Это поле обязательно' : false,
                }}
                className={`${StylePhoneInput.shortInput} ${className}`}
            />
        </>
    );
};

export default PhoneShortInput;
