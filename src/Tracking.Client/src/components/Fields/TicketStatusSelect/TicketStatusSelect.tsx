import React from 'react';
import style from '../../../style/elements/status.module.scss';

const TicketStatusSelect: React.FC = () => {
    return (
        <div>
            <button
                className={style.statusOptionSelect}
                style={{ backgroundColor: '#FFE978', color: '#E1980C' }}
            >
                <span className={style.statusOptionName}>
                    <img
                        src={'/status/new.svg'}
                        alt={'Новая'}
                        className={style.statusOptionIcon}
                    />

                    <span>Новая</span>
                </span>
            </button>
        </div>
    );
};

export default React.memo(TicketStatusSelect);
