import React from 'react';
import { FormInputProps } from '../BaseFields/FormInput';
import FormShortInput from '../FormShortInput/FormShortInput';

const AnyEmailShortInput: React.FC<FormInputProps> = ({
    ...formInputProps
}) => {
    return (
        <>
            <FormShortInput
                {...formInputProps}
                options={{
                    pattern: {
                        value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
                        message: 'Неверный формат E-mail',
                    },
                }}
            />
        </>
    );
};

export default AnyEmailShortInput;
