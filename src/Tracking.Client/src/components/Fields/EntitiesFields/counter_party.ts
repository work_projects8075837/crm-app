import { ICounterparty } from '@/services/counterparty/types';
import { IFile } from '@/services/file/types';

export const COUNTER_PARTY_NAME_NAME = 'name';
export const COUNTER_PARTY_COMMENT_NAME = 'comment';
export const COUNTER_PARTY_FILES_NAME = 'file';
export const COUNTER_PARTY_TIN_NAME = 'tin';
export const COUNTER_PARTY_MRN_NAME = 'tag';

export interface ICounterPartyForm {
    name: string;
    comment?: string;
    file?: IFile[];
    tag: string;
    tin: string;
    is_parent: boolean;
    counterparty?: ICounterparty | null;
}

export interface IShortCounterPartyForm {
    name: string;
    tag: string;
    tin: string;
}
