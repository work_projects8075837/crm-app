import { IConnectionType } from '../SelectConnectionType/SelectConnectionType';

export const CONNECTION_TYPE_NAME = 'type';
export const CONNECTION_RECEIVED_NAME = 'received';
export const CONNECTION_LOGIN_NAME = 'login';
export const CONNECTION_PASSWORD_NAME = 'password';
export const CONNECTION_OTHER_NAME = 'other';

export interface IConnectionForm {
    type?: IConnectionType;
    login?: string;
    password?: string;
    other?: string;
}

export const ANY_DESK = 'AnyDesk';
export const TEAM_VIEWER = 'TeamViewer';
export const SUPREME = 'Supreme';
