import { IJob } from '@/services/job/types';

export const JOB_NAME_NAME = 'name';
export const JOB_JOB_NAME = 'job';
export const JOB_DURATION_NAME = 'duration';

export interface IJobForm {
    name: string;
    is_parent: boolean;
    job?: IJob | null;
    duration?: number;
}
