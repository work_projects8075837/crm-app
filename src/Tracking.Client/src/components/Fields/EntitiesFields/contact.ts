import { IPhoneNumber } from '@/services/phone_number/types';

export const CONTACT_EMAIL_NAME = 'email';
export const CONTACT_NAME_NAME = 'name';
export const CONTACT_PHONE_NUMBERS_NAME = 'phone_number';

export interface IContactForm {
    email: string;
    name: string;
    phone_number?: IPhoneNumber[];
}
