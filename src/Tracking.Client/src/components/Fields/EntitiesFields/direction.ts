import { IStatus } from '@/services/status/types';

export const DIRECTION_NAME_NAME = 'name';
export const DIRECTION_STATUSES_NAME = 'status';

export interface IDirectionForm {
    name: string;
    status?: IStatus[];
}
