import { IShortMember } from '@/components/Tickets/hooks/useFormMembers';
import { IChannel } from '@/services/channel/types';
import { ICharacterProblem } from '@/services/character_problem/types';
import { IContact } from '@/services/contact/types';
import { ICounterparty } from '@/services/counterparty/types';
import { IDirection } from '@/services/direction/types';
import { IFile } from '@/services/file/types';
import { ISalePoint } from '@/services/sale_point/types';
import { ITag } from '@/services/tag/types';

export const TICKET_SALE_POINT_NAME = 'sale_point';
export const TICKET_CHARACTER_PROBLEM_NAME = 'character_problem';
export const TICKET_DIRECTION_NAME = 'direction';
export const TICKET_CHANNEL_NAME = 'channel';
export const TICKET_COUNTER_PARTY_NAME = 'counterparty';
export const TICKET_CONTACT_NAME = 'contact';
export const TICKET_STATUS_NAME = 'status_id';
export const TICKET_DESCRIPTION_NAME = 'description';
export const TICKET_DECISION_NAME = 'decision';
export const TICKET_MEMBERS_NAME = 'manager';
export const TICKET_TAGS_NAME = 'tag';
export const TICKET_FILES_NAME = 'file';
export const TICKET_TICKET_NAME = 'ticket';

export interface IShortTicket {
    id: string;
    number: number;
    character_problem: ICharacterProblem;
}

export interface ITicketForm {
    channel?: IChannel;
    character_problem?: ICharacterProblem;
    contact?: IContact;
    description?: string;
    decision?: string;
    direction?: IDirection;
    sale_point?: ISalePoint;
    counterparty?: ICounterparty;
    tag?: ITag[];
    manager?: IShortMember[];
    file?: IFile[];
    is_parent?: boolean;
    ticket?: IShortTicket | null;
    status_id?: string;
}

export const TICKET_MEMBERS_TYPES = {
    author: 'Создатель',
    responsible: 'Ответственный',
    involved: 'Вовлеченный',
};

export const TICKET_CONNECTIONS_TYPES = {
    teamViewer: 'TeamViewer',
    anyDesk: 'AnyDesk',
    ssh: 'ssh',
} as const;
