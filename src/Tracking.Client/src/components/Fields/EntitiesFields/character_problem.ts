import { ICharacterProblem } from '@/services/character_problem/types';
import { IJob } from '@/services/job/types';
import { ITag } from '@/services/tag/types';

export const CHARACTER_PROBLEM_NAME_NAME = 'name';
export const CHARACTER_PROBLEM_URL_NAME = 'url';
export const CHARACTER_PROBLEM_SLA_NAME = 'sla';
export const CHARACTER_PROBLEM_JOBS_NAME = 'job';
export const CHARACTER_PROBLEM_TAGS_NAME = 'tag';

export interface ICharacterProblemForm {
    name: string;
    url: string;
    sla: number;
    job: IJob[];
    tag: ITag[];
    is_parent: boolean;
    character_problem?: Omit<ICharacterProblem, 'character_problem_id'> | null;
}
