export const CHANNEL_NAME_NAME = 'name';
export const CHANNEL_MULTIPLE_NAME = 'multiple';

export interface IChannelForm {
    name: string;
    multiple: number;
}
