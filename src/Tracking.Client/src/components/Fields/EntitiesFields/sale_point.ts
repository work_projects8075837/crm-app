import { IContact } from '@/services/contact/types';
import { ICounterparty } from '@/services/counterparty/types';
import { IFile } from '@/services/file/types';
import { ISalePoint } from '@/services/sale_point/types';
import { ITag } from '@/services/tag/types';
import { IUser } from '@/services/user/types';
import { Entity } from '../SelectEntity/SelectEntity';

export const SALE_POINT_FILES_NAME = 'file';
export const SALE_POINT_NAME_NAME = 'name';
export const SALE_POINT_ADDRESS_NAME = 'address';
export const SALE_POINT_PHONE_NAME = 'phone';
export const SALE_POINT_EMAIL_NAME = 'email';
export const SALE_POINT_MANAGER_CONTACT_NAME = 'director_contact';
export const SALE_POINT_STATUS_NAME = 'sale_point_status';
export const SALE_POINT_CONTACTS_NAME = 'position';
export const SALE_POINT_COMMENT_NAME = 'description';
export const SALE_POINT_COUNTER_PARTIES_NAME = 'counterparty';
export const SALE_POINT_SALE_POINT_NAME = 'sale_point';
export const SALE_POINT_TAGS_NAME = 'tag';
export const SALE_POINT_MANAGER_NAME = 'user';
export const SALE_POINT_REGULATIONS_NAME = 'regulations';
export const SALE_POINT_SERVICE_FINISH_NAME = 'service_finish';
export const SALE_POINT_PHONE_NUMBER_NAME = 'phone_number';

export interface ISalePointFormContact extends Omit<IContact, 'id'> {
    id: string;
    contact_id: string;
    position_name: string;
    is_responsible: boolean;
}

export interface ISalePointForm {
    file?: IFile[];
    name: string;
    address: string;
    phone: string;
    email: string;
    director_contact?: IContact;
    // sale_point_status:
    position?: ISalePointFormContact[];
    counterparty?: ICounterparty[];
    description?: string;
    sale_point?: ISalePoint | null;
    tag: ITag[];
    is_parent: boolean;
    user?: IUser;
    regulations?: string;
    status?: Entity;
    service_finish?: string;
    phone_number?: Entity[];
}
