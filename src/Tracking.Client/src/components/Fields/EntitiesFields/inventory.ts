import { IConnection } from '@/services/connection/types';
import { IInventory } from '@/services/inventory/types';
import { ISoftware } from '@/services/software/types';

export const INVENTORY_NAME_NAME = 'name';
export const INVENTORY_SERIAL_NUMBER_NAME = 'serial_number';
export const INVENTORY_CONNECTIONS_NAME = 'connection';
export const INVENTORY_SOFTWARE_NAME = 'software';
export const INVENTORY_INVENTORY_NAME = 'inventory';

export interface IInventoryForm {
    name: string;
    serial_number?: string;
    connection: IConnection[];
    software: ISoftware[];
    inventory: IInventory | null;
    is_parent?: boolean;
}
