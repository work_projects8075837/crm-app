export const STATUS_NAME_NAME = 'name';
export const STATUS_BACKGROUND_NAME = 'background';
export const STATUS_COLOR_NAME = 'color';
export const STATUS_PREVIEW_LINK_NAME = 'preview_link';
export const STATUS_SLA_NAME = 'sla';

export interface IStatusForm {
    name: string;
    background: string;
    color: string;
    preview_link: string;
    sla: boolean;
}
