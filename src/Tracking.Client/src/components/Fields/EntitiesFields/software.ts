export const SOFTWARE_NAME_NAME = 'name';
export const SOFTWARE_VERSION_NAME = 'version';

export interface ISoftwareForm {
    name: string;
    version: string;
}
