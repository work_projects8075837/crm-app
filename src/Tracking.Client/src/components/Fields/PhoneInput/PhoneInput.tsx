import { StylePhoneInput } from '@/style/elements/phone_input';
import classNames from 'classnames';
import React, { useEffect } from 'react';
import { useFormContext } from 'react-hook-form';
import { FormInputProps } from '../BaseFields/FormInput';
import PhoneMaskInput from '../PhoneMaskInput/PhoneMaskInput';

const PhoneInput: React.FC<FormInputProps> = ({
    name,
    options,
    errorClass = StylePhoneInput.errorInput,
    wrapperClass = '',
    className,
    required = true,
    ...inputProps
}) => {
    const { formState, getFieldState, register, clearErrors, setValue, watch } =
        useFormContext();
    const { error, isTouched } = getFieldState(name, formState);

    const isError = !!error;

    useEffect(() => {
        register(name, {
            pattern: /^[^_]*$/,
            required: required ? 'Это обязательное поле' : false,
            validate: {
                phone: (v: string) => {
                    return v.split('(')[1].replace(/[\s\(\)_]/, '')
                        ? true
                        : 'Это обязательное поле';
                },
            },
        });
    }, []);

    const v = watch(name);

    return (
        <>
            <div className={wrapperClass}>
                <PhoneMaskInput
                    type='text'
                    {...{
                        ...inputProps,
                        name,
                        className: classNames(className, {
                            [errorClass]: isError,
                        }),
                    }}
                    value={v}
                    onChange={(event) => {
                        setValue(name, event.currentTarget.value);
                        if (isError) clearErrors(name);
                    }}
                />
                {isError && (
                    <span className={StylePhoneInput.errorMessage}>
                        {error.message}
                    </span>
                )}
            </div>
        </>
    );
};

export default PhoneInput;
