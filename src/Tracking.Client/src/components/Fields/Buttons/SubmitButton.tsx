import React, { ButtonHTMLAttributes } from 'react';

interface SubmitButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    text: string;
    isLoading?: boolean;
    loadingText?: string;
}

const SubmitButton: React.FC<SubmitButtonProps> = ({
    text,
    isLoading = false,
    loadingText = 'Загрузка...',
    ...buttonProps
}) => {
    return (
        <>
            <button
                className='btn'
                type='submit'
                disabled={isLoading}
                {...buttonProps}
            >
                {isLoading ? loadingText : text}
            </button>
        </>
    );
};

export default React.memo(SubmitButton);
