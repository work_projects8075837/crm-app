import React, { ButtonHTMLAttributes, HTMLAttributes } from 'react';
import style from '../../../style/elements/blue_button.module.scss';

export interface BlueButtonProps
    extends ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string;
}

const BlueButton: React.FC<BlueButtonProps> = ({
    children,
    className = '',
    ...buttonProps
}) => {
    return (
        <>
            <button {...buttonProps} className={`${style.button} ${className}`}>
                {children}
            </button>
        </>
    );
};

export default BlueButton;
