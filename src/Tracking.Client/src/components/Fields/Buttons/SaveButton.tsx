import Svg from '@/components/Elements/Svg/Svg';
import React, { ButtonHTMLAttributes, HTMLAttributes } from 'react';
import style from '../../../style/layout/ticket.module.scss';

export interface SaveButtonProps
    extends ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string;
    isLoading?: boolean;
    loadingText?: string;
}

const SaveButton: React.FC<SaveButtonProps> = ({
    children,
    className = '',
    isLoading = false,
    loadingText = 'Загрузка...',
    ...buttonProps
}) => {
    return (
        <>
            <button
                {...buttonProps}
                className={`${style.ticketSubmitButtonPrimary} ${className}`}
                disabled={isLoading}
            >
                {isLoading ? (
                    loadingText
                ) : (
                    <>
                        <Svg
                            symbol='check'
                            className={style.ticketSubmitButtonSvg}
                        />
                        <span>{children}</span>
                    </>
                )}
            </button>
        </>
    );
};

export default React.memo(SaveButton);
