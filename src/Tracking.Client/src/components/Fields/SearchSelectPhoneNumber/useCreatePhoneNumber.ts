import { phoneNumberApi } from '@/services/phone_number/phone_number';
import { ICreatePhoneNumber } from '@/services/phone_number/types';
import { useMutation } from '@tanstack/react-query';

export const useCreatePhoneNumber = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreatePhoneNumber) => {
            return await phoneNumberApi.create(data).then((res) => res.data);
        },
    });
    return mutation;
};
