export const formatSearchPhoneNumber = (phone?: string) => {
    const clearPhone = phone?.replaceAll(/[\_\+\-]/g, '');

    const code_number = clearPhone?.split('(');

    const clearPhoneNumber = code_number?.[1];

    const dashPhoneNumber =
        clearPhoneNumber &&
        Array.from(clearPhoneNumber)
            .map((char, i) => {
                if (i === 7 || i === 9) {
                    return `-${char}`;
                }
                return char;
            })
            .join('');

    const dashPhone = `${code_number?.[0] || ''}${
        dashPhoneNumber ? '(' + dashPhoneNumber : ''
    }`;

    const bracketPhone =
        dashPhoneNumber && dashPhoneNumber.length < 4
            ? dashPhone.replace(')', '')
            : dashPhone;

    return bracketPhone;
};
