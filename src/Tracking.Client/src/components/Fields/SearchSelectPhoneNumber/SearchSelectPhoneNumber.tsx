import { formatPhoneLink } from '@/components/utils/formatPhoneLink';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { phoneNumberHooks } from '@/services/phone_number/phone_number';
import { totalClearKey } from '@/store/queries/actions/base';
import { GET_SEARCH_PHONE_NUMBERS_KEY } from '@/store/queries/keys/keys';
import { StylePhoneInput } from '@/style/elements/phone_input';
import React from 'react';
import { CONTACT_PHONE_NUMBERS_NAME } from '../EntitiesFields/contact';
import PhoneMaskInput from '../PhoneMaskInput/PhoneMaskInput';
import SearchCreateButton from '../SearchCreateSelect/SearchCreateButton';
import SearchCreateSelect from '../SearchCreateSelect/SearchCreateSelect';
import { useCustomFucus } from '../hooks/useCustomFucus';
import { useFormEntityValueArray } from '../hooks/useFormEntityValueArray';
import { formatSearchPhoneNumber } from './formatSearchPhoneNumber';
import { useCreatePhoneNumber } from './useCreatePhoneNumber';

interface SearchSelectPhoneNumberProps {
    defaultSearch?: string;
}

const SearchSelectPhoneNumber: React.FC<SearchSelectPhoneNumberProps> = ({
    defaultSearch,
}) => {
    const { add } = useFormEntityValueArray(CONTACT_PHONE_NUMBERS_NAME);
    const model = useSearchInputModel(defaultSearch);

    const { data, isLoading } = phoneNumberHooks.useSearch(
        formatSearchPhoneNumber(model.value),
    );
    const mutation = useCreatePhoneNumber();

    const isCreateDisabled =
        (formatPhoneLink(model.value)?.length || 0) < 11 ||
        data?.data.some((phone) => phone.name === model.value);

    const { inputRef, focus } = useCustomFucus();

    return (
        <>
            <SearchCreateSelect
                options={{ required: true }}
                searchInput={
                    <PhoneMaskInput
                        onChange={model.model.onChange}
                        disabled={mutation.isLoading}
                        className={StylePhoneInput.inputsSearch}
                        ref={inputRef}
                    />
                }
                createButton={
                    <SearchCreateButton
                        isLoading={isLoading || mutation.isLoading}
                        onClick={() =>
                            mutation.mutate(
                                { name: model.value },
                                {
                                    onSuccess: (newPhone) => {
                                        add(newPhone);
                                        totalClearKey(
                                            GET_SEARCH_PHONE_NUMBERS_KEY,
                                        );
                                    },
                                },
                            )
                        }
                        isDisabled={isCreateDisabled}
                    />
                }
                name={CONTACT_PHONE_NUMBERS_NAME}
                entities={data?.data}
                searchModelState={model}
                focus={focus}
            />
        </>
    );
};

export default SearchSelectPhoneNumber;
