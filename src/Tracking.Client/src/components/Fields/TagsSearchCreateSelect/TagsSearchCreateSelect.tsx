import { useTagCreateMutation } from '@/components/Form/TagCreateForm/useTagCreateMutation';
import { useTextInputModel } from '@/hooks/useTextInputModel';
import { tagHooks } from '@/services/tag/tag';
import { totalClearKey } from '@/store/queries/actions/base';
import { GET_SEARCH_TAGS_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import { TICKET_TAGS_NAME } from '../EntitiesFields/ticket';
import SearchCreateButton from '../SearchCreateSelect/SearchCreateButton';
import SearchCreateSelect from '../SearchCreateSelect/SearchCreateSelect';
import { useFormEntityValueArray } from '../hooks/useFormEntityValueArray';

const tags = [
    { id: '1', name: 'Тег 1' },
    { id: '2', name: 'Тег 2' },
    { id: '3', name: 'Тег 3' },
    { id: '4', name: 'Тег 4' },
    { id: '5', name: 'Тег 5' },
];

const TagsSearchCreateSelect: React.FC = () => {
    const { add } = useFormEntityValueArray(TICKET_TAGS_NAME);
    const [value, model, setSearch] = useTextInputModel();
    const { data, isLoading } = tagHooks.useSearch(value);

    const mutation = useTagCreateMutation({
        onSuccess: (newTag) => {
            add(newTag);
            totalClearKey(GET_SEARCH_TAGS_KEY);
        },
    });

    const isCreateDisabled =
        isLoading ||
        mutation.isLoading ||
        !value.replaceAll(' ', '') ||
        data?.data.some((tag) => tag.name === value);

    return (
        <>
            <SearchCreateSelect
                isDisabled={mutation.isLoading}
                name={TICKET_TAGS_NAME}
                createButton={
                    <>
                        <SearchCreateButton
                            onClick={() => mutation.mutate({ name: value })}
                            isDisabled={isCreateDisabled}
                        />
                    </>
                }
                entities={data?.data}
                searchModelState={{ value, model, setSearch }}
            />
        </>
    );
};

export default React.memo(TagsSearchCreateSelect);
