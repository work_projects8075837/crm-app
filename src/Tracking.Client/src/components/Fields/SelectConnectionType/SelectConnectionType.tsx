import React from 'react';
import { useFormContext } from 'react-hook-form';
import {
    ANY_DESK,
    CONNECTION_LOGIN_NAME,
    CONNECTION_OTHER_NAME,
    CONNECTION_PASSWORD_NAME,
    CONNECTION_TYPE_NAME,
    SUPREME,
    TEAM_VIEWER,
} from '../EntitiesFields/connection';
import SelectEntity from '../SelectEntity/SelectEntity';

export const connectionTypes = [
    { id: '1', type: ANY_DESK, name: 'AnyDesk' },
    { id: '2', type: TEAM_VIEWER, name: 'TeamViewer' },
    { id: '3', type: SUPREME, name: 'Иное подключение' },
];

export type IConnectionType = (typeof connectionTypes)[0];

const SelectConnectionType: React.FC = () => {
    const { setValue } = useFormContext();
    return (
        <>
            <SelectEntity
                isRequired={true}
                name={CONNECTION_TYPE_NAME}
                entities={connectionTypes}
                onChangeAction={() => {
                    setValue(CONNECTION_LOGIN_NAME, undefined);
                    setValue(CONNECTION_PASSWORD_NAME, undefined);
                    setValue(CONNECTION_OTHER_NAME, undefined);
                }}
            />
        </>
    );
};

export default SelectConnectionType;
