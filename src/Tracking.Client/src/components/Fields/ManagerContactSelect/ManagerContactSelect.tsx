import React from 'react';
import {
    SALE_POINT_CONTACTS_NAME,
    SALE_POINT_MANAGER_CONTACT_NAME,
} from '../EntitiesFields/sale_point';
import SelectEntity from '../SelectEntity/SelectEntity';
import { useFormEntityValueArray } from '../hooks/useFormEntityValueArray';

const ManagerContactSelect: React.FC = () => {
    const { entities } = useFormEntityValueArray(SALE_POINT_CONTACTS_NAME);

    return (
        <>
            <SelectEntity
                name={SALE_POINT_MANAGER_CONTACT_NAME}
                entities={entities || []}
            />
        </>
    );
};

export default ManagerContactSelect;
