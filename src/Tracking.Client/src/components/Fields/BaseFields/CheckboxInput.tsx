import Checkbox from '@/components/Elements/Checkbox/Checkbox';
import React, { InputHTMLAttributes } from 'react';
import { useFormContext, useWatch } from 'react-hook-form';
import styles from '../../../style/forms/field.module.scss';

interface CheckboxInputProps extends InputHTMLAttributes<HTMLInputElement> {
    label: string;
    name: string;
    labelClassName?: string;
}

const CheckboxInput: React.FC<CheckboxInputProps> = ({
    label,
    name,
    labelClassName = '',
    ...inputProps
}) => {
    const { setValue } = useFormContext();
    const isChecked = useWatch({ name });
    return (
        <>
            <label
                className={`${styles.checkboxLabel} ${labelClassName}`}
                onClick={() => setValue(name, !isChecked)}
            >
                <Checkbox className={styles.checkbox} isChecked={isChecked} />
                <span>{label}</span>
            </label>
        </>
    );
};

export default React.memo(CheckboxInput);
