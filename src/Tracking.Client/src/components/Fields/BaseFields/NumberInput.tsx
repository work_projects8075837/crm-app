import React from 'react';
import { RegisterOptions, useFormContext } from 'react-hook-form';
import FormInput, { FormInputProps } from './FormInput';

type NumberInputProps = FormInputProps;

const NumberInput: React.FC<NumberInputProps> = ({
    options,
    ...inputProps
}) => {
    const { setValue } = useFormContext();
    const inputOptions: RegisterOptions = { ...options };
    inputOptions.setValueAs = (v?: string) => {
        const n = typeof v === 'number' ? v : Number(v?.replace(',', '.'));
        return n;
    };
    inputOptions.validate = {
        notNan: (v) => {
            return !isNaN(v) || 'Неверный формат числа';
        },
    };
    // inputOptions.pattern = {
    //     value: /^[0-9\.\,]$/g,
    //     message: 'Неверный формат числа',
    // };
    return (
        <>
            <FormInput options={inputOptions} {...inputProps} />
        </>
    );
};

export default NumberInput;
