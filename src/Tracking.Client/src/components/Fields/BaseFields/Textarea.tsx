import { SERVER_FORM_ERROR_NAME } from '@/components/Form/Types/Types';
import React from 'react';
import { RegisterOptions, useFormContext } from 'react-hook-form';
import styles from '../../../style/forms/field.module.scss';

interface TextareaProps extends React.HTMLAttributes<HTMLTextAreaElement> {
    name: string;
    options?: RegisterOptions;
    errorClass?: string;
    wrapperClass?: string;
}

const Textarea: React.FC<TextareaProps> = ({
    name,
    options,
    className,
    errorClass = styles.errorInput,
    wrapperClass = '',
    ...inputProps
}) => {
    const { formState, getFieldState, register, clearErrors } =
        useFormContext();
    const { error, isTouched } = getFieldState(name, formState);
    const serverErrorState = getFieldState(SERVER_FORM_ERROR_NAME, formState);

    const isError = !!error && isTouched;

    return (
        <>
            <div className={wrapperClass}>
                <textarea
                    {...{
                        ...inputProps,
                        name,
                        className:
                            `${styles.textarea} ${className || ''}` +
                            (isError ? ` ${errorClass}` : ''),
                    }}
                    {...register(name, {
                        required: 'Это обязательное поле',
                        ...options,
                        onChange: (event) => {
                            if (options?.onChange) options?.onChange(event);
                            if (serverErrorState.error) {
                                clearErrors(SERVER_FORM_ERROR_NAME);
                            }
                        },
                    })}
                ></textarea>
                {isError && (
                    <span className={styles.errorMessage}>{error.message}</span>
                )}
            </div>
        </>
    );
};

export default Textarea;
