import React, { InputHTMLAttributes } from 'react';
import { RegisterOptions, useFormContext } from 'react-hook-form';
// import styles from '@/style/forms/field.module.scss';
import { SERVER_FORM_ERROR_NAME } from '@/components/Form/Types/Types';
import styles from '../../../style/forms/field.module.scss';

export interface FormInputProps extends InputHTMLAttributes<HTMLInputElement> {
    name: string;
    options?: RegisterOptions;
    errorClass?: string;
    wrapperClass?: string;
}

const FormInput: React.FC<FormInputProps> = ({
    name,
    options,
    errorClass = styles.errorInput,
    wrapperClass = '',
    ...inputProps
}) => {
    const { formState, getFieldState, register, clearErrors } =
        useFormContext();
    const { error, isTouched } = getFieldState(name, formState);
    const serverErrorState = getFieldState(SERVER_FORM_ERROR_NAME, formState);

    const isError = !!error && isTouched;

    return (
        <>
            <div className={wrapperClass}>
                <input
                    type='text'
                    {...{
                        ...inputProps,
                        name,
                        className:
                            `${styles.input} ${inputProps.className || ''}` +
                            (isError ? ` ${errorClass}` : ''),
                    }}
                    {...register(name, {
                        required: 'Это обязательное поле',
                        ...options,
                        onChange: (event) => {
                            if (options?.onChange) options?.onChange(event);
                            if (serverErrorState.error) {
                                clearErrors(SERVER_FORM_ERROR_NAME);
                            }
                        },
                    })}
                />
                {isError && (
                    <span className={styles.errorMessage}>{error.message}</span>
                )}
            </div>
        </>
    );
};

export default React.memo(FormInput);
