import React from 'react';
import style from '../../../style/forms/add_member.module.scss';

interface AddMemberSelectLayoutProps {
    children: React.ReactNode;
}

const AddMemberSelectLayout: React.FC<AddMemberSelectLayoutProps> = ({
    children,
}) => {
    return (
        <>
            <div className={style.addMemberSearch}>{children}</div>
        </>
    );
};

export default AddMemberSelectLayout;
