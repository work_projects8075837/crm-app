import React from 'react';
import { RegisterOptions } from 'react-hook-form';
import FormInput, { FormInputProps } from '../BaseFields/FormInput';

const AnyEmailInput: React.FC<FormInputProps> = ({
    options,
    ...formInputProps
}) => {
    const propsOptions: RegisterOptions = { ...options };
    propsOptions.pattern = {
        value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
        message: 'Неверный формат E-mail',
    };

    return (
        <>
            <FormInput {...formInputProps} options={propsOptions} />
        </>
    );
};

export default AnyEmailInput;
