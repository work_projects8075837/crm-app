import React from 'react';
import FormInput, { FormInputProps } from '../BaseFields/FormInput';

// interface EmailInputProps extends FormInputProps {}

const EmailInput: React.FC<FormInputProps> = ({
    options,
    ...formInputProps
}) => {
    return (
        <>
            <FormInput
                {...formInputProps}
                options={{
                    pattern: {
                        value: /^[\w-\.]+@flagman-it.ru$/,
                        // value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
                        message: 'Неверный формат E-mail',
                    },
                }}
            />
        </>
    );
};

export default React.memo(EmailInput);
