import { Input, InputProps } from '@chakra-ui/react';
import React from 'react';
import { RegisterOptions, useFormContext } from 'react-hook-form';
import style from '../../../style/forms/field.module.scss';

interface DateInputProps extends InputProps {
    name: string;
    options?: RegisterOptions;
    wrapperClass?: string;
    errorClass?: string;
}

const DateInput: React.FC<DateInputProps> = ({
    name,
    options,
    errorClass = style.errorInput,
    className = '',
    wrapperClass = '',
    ...inputProps
}) => {
    const { formState, getFieldState, register } = useFormContext();
    const { error, isTouched } = getFieldState(name, formState);

    const isError = !!error && isTouched;

    return (
        <>
            <div className={wrapperClass}>
                <Input
                    {...inputProps}
                    className={`${style.input} ${className} ${
                        isError ? ` ${errorClass}` : ''
                    }`}
                    type='date'
                    {...register(name, {
                        required: 'Это обязательное поле',
                        ...options,
                    })}
                />
                {isError && (
                    <span className={style.errorMessage}>{error.message}</span>
                )}
            </div>
        </>
    );
};

export default DateInput;
