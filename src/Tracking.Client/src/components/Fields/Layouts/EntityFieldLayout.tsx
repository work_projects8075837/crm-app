import React from 'react';
import style from '../../../style/forms/field.module.scss';

interface EntityFieldLayoutProps {
    children: React.ReactNode;
    title: string;
}

const EntityFieldLayout: React.FC<EntityFieldLayoutProps> = ({
    children,
    title,
}) => {
    return (
        <>
            <div className={style.fieldEntity}>
                <div className={style.fieldEntityLabel}>{title}</div>

                <div className={style.fieldEntityChildren}>{children}</div>
            </div>
        </>
    );
};

export default EntityFieldLayout;
