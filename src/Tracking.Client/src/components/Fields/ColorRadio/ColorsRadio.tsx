import React, { useEffect } from 'react';
import '../../../style/elements/radio_option.scss';
import { useFormContext } from 'react-hook-form';
import ColorOption from '../ColorOption/ColorOption';

interface ColorsRadioProps {
    name: string;
    colors: string[];
}

const ColorsRadio: React.FC<ColorsRadioProps> = ({ colors, name }) => {
    const { register, setValue } = useFormContext();

    useEffect(() => {
        setValue(name, colors[0]);
    }, []);
    return (
        <>
            <div className='colorsOptions'>
                {colors.map((color) => (
                    <ColorOption
                        key={color}
                        {...register(name)}
                        value={color}
                    />
                ))}
            </div>
        </>
    );
};

export default ColorsRadio;
