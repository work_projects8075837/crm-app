import React, { HTMLAttributes } from 'react';

interface ImageLinkOptionProps extends HTMLAttributes<HTMLInputElement> {
    value: string;
}

const ImageLinkOption: React.ForwardRefRenderFunction<
    HTMLInputElement,
    ImageLinkOptionProps
> = ({ value, ...inputProps }, ref) => {
    return (
        <>
            <label className='colorLabel'>
                <input
                    {...inputProps}
                    type='radio'
                    className='colorInput'
                    value={value}
                    ref={ref}
                />
                <div className='colorValue'></div>
            </label>
        </>
    );
};

export default ImageLinkOption;
