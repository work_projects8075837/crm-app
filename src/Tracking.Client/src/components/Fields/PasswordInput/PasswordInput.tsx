import React from 'react';
import FormInput, { FormInputProps } from '../BaseFields/FormInput';

interface PasswordInputProps extends FormInputProps {
    passwordRepeatName?: string;
    hard?: boolean;
}

const PasswordInput: React.FC<PasswordInputProps> = ({
    passwordRepeatName = 'password_repeat',
    hard = true,
    ...formInputProps
}) => {
    return (
        <>
            <FormInput
                type='password'
                {...formInputProps}
                options={{
                    pattern: hard
                        ? {
                              value: /^(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?=.*\d)[\w\W]{8,}$/,
                              message:
                                  'Пароль должен содержать как минимум одну прописную и заглавную букву (А-Я или A-Z) и цифру (0-9)',
                          }
                        : undefined,
                    minLength: hard
                        ? {
                              value: 8,
                              message:
                                  'Пароль должен содержать не менее 8 символов',
                          }
                        : undefined,
                    deps: [passwordRepeatName],
                }}
            />
        </>
    );
};

export default React.memo(PasswordInput);
