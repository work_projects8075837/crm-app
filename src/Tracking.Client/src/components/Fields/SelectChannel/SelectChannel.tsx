import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout'
import { useSearchInputModel } from '@/hooks/useTextInputModel'
import { channelHooks } from '@/services/channel/channel'
import { IChannel } from '@/services/channel/types'
import React from 'react'
import { TICKET_CHANNEL_NAME } from '../EntitiesFields/ticket'
import SelectEntity from '../SelectEntity/SelectEntity'
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard'

interface SelectChannelProps {
    onChangeAction?: (data: IChannel) => void;
    withLabel?: boolean;
    onCloseClick?: () => void;
    required?: boolean;
    placeholder?: string;
}

const SelectChannel: React.FC<SelectChannelProps> = ({
    onChangeAction,
    withLabel = true,
    required = false,
    onCloseClick,
    placeholder,
}) => {
    const searchInputModel = useSearchInputModel();
    const { data } = channelHooks.useSearch(searchInputModel.value);
    const title = 'Канал связи';

    return (
        <>
            <SelectedEntityCard
                title={title}
                name={TICKET_CHANNEL_NAME}
                onCloseClick={onCloseClick}
            >
                {withLabel ? (
                    <EntitySelectFieldLayout label={title}>
                        <SelectEntity
                            placeholder={placeholder}
                            isRequired={required}
                            name={TICKET_CHANNEL_NAME}
                            searchInputModel={searchInputModel}
                            entities={data?.data}
                            onChangeAction={onChangeAction}
                        />
                    </EntitySelectFieldLayout>
                ) : (
                    <SelectEntity
                        placeholder={placeholder}
                        isRequired={required}
                        name={TICKET_CHANNEL_NAME}
                        searchInputModel={searchInputModel}
                        entities={data?.data}
                        onChangeAction={onChangeAction}
                    />
                )}
            </SelectedEntityCard>
        </>
    );
};

export default React.memo(SelectChannel);
