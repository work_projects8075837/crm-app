import { performExistCallback } from '@/components/utils/performExistCallback';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useFormEntityValue } from '../hooks/useFormEntityValue';
import CloseCrossButton from './CloseCrossButton';
import SelectedEntityCardLayout from './SelectedEntityCardLayout';

interface SelectedEntityCardProps {
    name: string;
    title: string;
    children: React.ReactNode;
    extraFields?: React.ReactNode;
    navbar?: React.ReactNode;
    linkName?: string;
    onCloseClick?: () => void;
}

const SelectedEntityCard: React.FC<SelectedEntityCardProps> = ({
    title,
    name,
    children,
    extraFields,
    navbar,
    linkName,
    onCloseClick,
}) => {
    const { currentEntity } = useFormEntityValue(name);
    const { setValue } = useFormContext();
    return (
        <>
            {currentEntity ? (
                <>
                    <SelectedEntityCardLayout
                        linkName={linkName}
                        id={currentEntity.id}
                        title={title}
                        valueName={currentEntity.name}
                        closeBtn={
                            <CloseCrossButton
                                onClick={() => {
                                    setValue(name, null);
                                    performExistCallback(onCloseClick);
                                }}
                            />
                        }
                        navbar={navbar}
                    >
                        {extraFields}
                    </SelectedEntityCardLayout>
                </>
            ) : (
                <>{children}</>
            )}
        </>
    );
};

export default SelectedEntityCard;
