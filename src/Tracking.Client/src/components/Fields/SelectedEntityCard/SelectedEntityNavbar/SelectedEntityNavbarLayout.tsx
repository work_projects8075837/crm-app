import React from 'react';
import style from '../../../../style/elements/select_option.module.scss';

interface SelectedEntityNavbarLayoutProps {
    children: React.ReactNode;
}

const SelectedEntityNavbarLayout: React.FC<SelectedEntityNavbarLayoutProps> = ({
    children,
}) => {
    return <div className={style.selectedEntityCardNavBar}>{children}</div>;
};

export default SelectedEntityNavbarLayout;
