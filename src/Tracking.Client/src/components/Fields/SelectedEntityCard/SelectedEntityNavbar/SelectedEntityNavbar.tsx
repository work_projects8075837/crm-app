import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../../style/elements/select_option.module.scss';
import SelectedEntityNavbarLayout from './SelectedEntityNavbarLayout';

interface SelectedEntityNavbarProps {
    phone?: string;
    email?: string;
    children?: React.ReactNode;
}

const SelectedEntityNavbar: React.FC<SelectedEntityNavbarProps> = ({
    phone,
    email,
    children,
}) => {
    return (
        <SelectedEntityNavbarLayout>
            {children}

            <div className={style.selectedEntityNavbarList}>
                <a
                    href={`tel:${phone?.replace(/[^0-9]/g, '')}`}
                    className={style.selectedEntityNavbarLink}
                >
                    <Svg
                        symbol='blue_user_card'
                        className={style.selectedEntityNavbarSvgSmall}
                    />
                </a>

                <a
                    href={`tel:${phone?.replace(/[^0-9]/g, '')}`}
                    className={style.selectedEntityNavbarLink}
                >
                    <Svg
                        symbol='blue_phone'
                        className={style.selectedEntityNavbarSvg}
                    />
                </a>

                <a
                    href={`mailto:${email}`}
                    className={style.selectedEntityNavbarLink}
                >
                    <Svg
                        symbol='blue_message'
                        className={style.selectedEntityNavbarSvg}
                    />
                </a>
            </div>
        </SelectedEntityNavbarLayout>
    );
};

export default SelectedEntityNavbar;
