import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/elements/select_option.module.scss';

interface CardTitleValueProps {
    id?: string;
    title?: string | React.ReactNode;
    value?: string | React.ReactNode;
    linkName?: string;
}

const CardTitleValue: React.FC<CardTitleValueProps> = ({
    id,
    title,
    value,
    linkName,
}) => {
    return (
        <>
            <div className={style.titleValue}>
                <span>{title}</span>

                {linkName ? (
                    <Link
                        target='_blank'
                        className={style.selectedEntityCardMainTitle}
                        to={`/${linkName}/single/${id}/`}
                    >
                        {value}
                    </Link>
                ) : (
                    <span className={style.selectedEntityCardMainTitle}>
                        {value}
                    </span>
                )}
            </div>
        </>
    );
};

export default CardTitleValue;
