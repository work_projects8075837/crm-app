import Svg from '@/components/Elements/Svg/Svg';
import React, { ButtonHTMLAttributes } from 'react';
import style from '../../../style/elements/select_option.module.scss';

interface CloseCrossButtonProps
    extends ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string;
}

const CloseCrossButton: React.FC<CloseCrossButtonProps> = ({
    onClick,
    className = '',
}) => {
    return (
        <button
            className={`${style.closeButton} ${className}`}
            onClick={onClick}
        >
            <Svg symbol='add_plus' />
        </button>
    );
};

export default CloseCrossButton;
