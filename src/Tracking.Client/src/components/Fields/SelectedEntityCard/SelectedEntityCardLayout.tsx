import React from 'react';
import style from '../../../style/elements/select_option.module.scss';
import CardTitleValue from './CardTitleValue';

interface SelectedEntityCardLayoutProps {
    id: string;
    children?: React.ReactNode;
    navbar?: React.ReactNode;
    closeBtn?: React.ReactNode;
    title: string;
    valueName?: string;
    linkName?: string;
    onDoubleClick?: () => void;
    onClick?: () => void;
    cardClassName?: string;
    isSelected?: boolean;
}

const SelectedEntityCardLayout: React.FC<SelectedEntityCardLayoutProps> = ({
    id,
    children,
    title,
    valueName,
    navbar,
    closeBtn,
    linkName,
    onClick,
    cardClassName = '',
    isSelected = false,
}) => {
    return (
        <>
            <div
                className={`${style.selectedEntityCard} ${
                    onClick ? style.selectedEntityCardHovering : ''
                } ${
                    isSelected ? style.selectedEntityCardSelected : ''
                } ${cardClassName}`}
                onClick={onClick}
                onDoubleClick={(e) => e.preventDefault()}
            >
                {closeBtn}
                <div className={style.selectedEntityCardHeader}>
                    <CardTitleValue
                        id={id}
                        linkName={linkName}
                        title={title}
                        value={valueName}
                    />
                    {navbar}
                </div>
                {children}
            </div>
        </>
    );
};

export default SelectedEntityCardLayout;
