import { counterPartyHooks } from '@/services/counterparty/counterparty';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import { SALE_POINT_COUNTER_PARTIES_NAME } from '../EntitiesFields/sale_point';
import SearchCreateSelect from '../SearchCreateSelect/SearchCreateSelect';
import CreateEntityButton from '../SelectEntity/SelectEntityOption/CreateEntityButton';
import { createShortCounterPartyModal } from './../../SalePoints/CreateShortCounterPartyModal/CreateShortCounterPartyModal';

interface CounterPartySearchCreateProps {
    openHandler: OpenHandler;
}

const CounterPartySearchCreate: React.FC<CounterPartySearchCreateProps> = ({
    openHandler,
}) => {
    const { data } = counterPartyHooks.useList();

    return (
        <>
            <SearchCreateSelect
                name={SALE_POINT_COUNTER_PARTIES_NAME}
                entities={data?.data}
                addButton={
                    <CreateEntityButton
                        onClick={() => {
                            openHandler.close();
                            createShortCounterPartyModal.open();
                        }}
                    />
                }
            />
        </>
    );
};

export default CounterPartySearchCreate;
