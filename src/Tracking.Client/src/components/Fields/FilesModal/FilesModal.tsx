import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import FilesTable from '@/components/Tickets/TicketFilesModal/FilesTable/FilesTable';
import FilesDragOnDrop from '../FilesDragOnDrop/FilesDragOnDrop';
import { useFilesPasteHandler } from '../EditorInput/useFilesPasteHandler';

interface FilesModalProps {
    openHandler: OpenHandler;
    name: string;
}

const FilesModal: React.FC<FilesModalProps> = ({ openHandler, name }) => {
    useFilesPasteHandler(openHandler, name);

    return (
        <>
            <ModalWindow
                layoutClass={style.modalLarge}
                contentClass={style.emptyModalContent}
                title='Файлы'
                openHandler={openHandler}
            >
                <div className={style.modalVert}>
                    <FilesTable />
                    <FilesDragOnDrop name={name} />
                </div>
            </ModalWindow>
        </>
    );
};

export default FilesModal;
