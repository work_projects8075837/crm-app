import React from 'react';
import { useFormContext, useWatch } from 'react-hook-form';
import {
    STATUS_BACKGROUND_NAME,
    STATUS_COLOR_NAME,
    STATUS_PREVIEW_LINK_NAME,
} from '../EntitiesFields/status';
import { IStatusStyle } from './StatusStyleSelect';

interface StatusStyleOptionProps {
    value: IStatusStyle;
}

const StatusStyleOption: React.FC<StatusStyleOptionProps> = ({ value }) => {
    const currentColor = useWatch({ name: STATUS_COLOR_NAME });
    const { setValue } = useFormContext();

    return (
        <>
            <button
                onClick={(event) => {
                    event.preventDefault();
                    setValue(STATUS_COLOR_NAME, value.color);
                    setValue(STATUS_BACKGROUND_NAME, value.background);
                    setValue(STATUS_PREVIEW_LINK_NAME, value.preview_link);
                }}
                className={
                    'colorValue' +
                    (currentColor === value.color ? ' colorValueSelected' : '')
                }
                style={{ backgroundColor: value.background }}
            >
                <img src={value.preview_link} />
            </button>
        </>
    );
};

export default StatusStyleOption;
