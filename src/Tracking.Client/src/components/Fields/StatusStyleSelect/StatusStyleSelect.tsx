import React, { useEffect } from 'react';
import '../../../style/elements/radio_option.scss';
import StatusStyleOption from './StatusStyleOption';
import { useFormContext } from 'react-hook-form';
import {
    STATUS_BACKGROUND_NAME,
    STATUS_COLOR_NAME,
    STATUS_PREVIEW_LINK_NAME,
} from '../EntitiesFields/status';

export interface IStatusStyle {
    color: string;
    background: string;
    preview_link: string;
}

interface StatusStyleSelectProps {
    styles: IStatusStyle[];
}

const StatusStyleSelect: React.FC<StatusStyleSelectProps> = ({ styles }) => {
    const { setValue } = useFormContext();
    useEffect(() => {
        setValue(STATUS_COLOR_NAME, styles[0].color);
        setValue(STATUS_BACKGROUND_NAME, styles[0].background);
        setValue(STATUS_PREVIEW_LINK_NAME, styles[0].preview_link);
    }, []);
    return (
        <div className='colorsOptions'>
            {styles.map((s) => (
                <StatusStyleOption key={s.color} value={s} />
            ))}
        </div>
    );
};

export default StatusStyleSelect;
