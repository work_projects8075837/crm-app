import { useFormContext } from 'react-hook-form';
import { useFileCreateMutation } from '../FilesDragOnDrop/useFileCreateMutation';

const allowedExt = [
    'svg',
    'webp',
    'png',
    'gif',
    'jfif',
    'pjpeg',
    'jpeg',
    'pjp',
    'jpg',
    'bmp',
    'ico',
];

export const useHandleEditorFiles = (name: string) => {
    const { setValue, getValues } = useFormContext();
    const { mutateAsync, isLoading } = useFileCreateMutation();

    const handleFiles = async (files?: FileList | null) => {
        if (!files) return;

        const filesArr = [...files];

        const filesUploadCallbacks = filesArr.map((file) => {
            return (async () => {
                const ext = file.name.split('.').pop()?.toLocaleLowerCase();

                const loadedFile = await mutateAsync([file]).then(
                    (files) => files[0],
                );
                const pastedFileSrc = loadedFile.path;

                if (pastedFileSrc) {
                    if (ext && allowedExt.includes(ext)) {
                        setValue(
                            name,
                            (getValues(name) || '') +
                                `<img loading="eager" src="${pastedFileSrc}">`,
                        );
                    } else {
                        setValue(
                            name,
                            (getValues(name) || '') +
                                `<a href="${pastedFileSrc}">${loadedFile.name}</a>`,
                        );
                    }
                }
            })();
        });

        await Promise.allSettled(filesUploadCallbacks);
    };

    return { isLoading, handleFiles };
};
