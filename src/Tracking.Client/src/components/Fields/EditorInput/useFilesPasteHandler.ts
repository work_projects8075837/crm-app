import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { useEffect } from 'react';
import { useFileCreateMutation } from '../FilesDragOnDrop/useFileCreateMutation';
import { useFormEntityValueArray } from '../hooks/useFormEntityValueArray';

export const useFilesPasteHandler = (
    openHandler: OpenHandler,
    name: string,
) => {
    const { addArray } = useFormEntityValueArray(name);
    const fileMutation = useFileCreateMutation();
    useEffect(() => {
        const onPaste = async (event: ClipboardEvent) => {
            if (!event.clipboardData) return;

            if (openHandler.isOpen) {
                try {
                    const files: File[] = [];
                    for (let i = 0; i < event.clipboardData.items.length; i++) {
                        const dataTransferFile =
                            event.clipboardData.items[i].getAsFile();

                        if (dataTransferFile) {
                            files.push(dataTransferFile);
                        }
                    }
                    const loadedFiles = await fileMutation.mutateAsync(files);
                    addArray(loadedFiles);
                    return;
                } catch (error) {
                    return;
                }
            }
        };

        document.addEventListener('paste', onPaste);

        return () => document.removeEventListener('paste', onPaste);
    }, []);
};
