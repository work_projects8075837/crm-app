export const readFile = (file: File | null): Promise<string> => {
    return new Promise((resolve, reject) => {
        if (!file) {
            return null;
        }

        const fileReader = new FileReader();
        fileReader.readAsDataURL(file);

        fileReader.onload = (event) => {
            resolve(event.target?.result as string);
        };

        fileReader.onerror = (error) => {
            reject(error);
        };
    });
};
