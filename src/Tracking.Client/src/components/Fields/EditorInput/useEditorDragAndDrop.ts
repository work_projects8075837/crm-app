import { useBooleanState } from '@/hooks/useBooleanState';
import { useHandleEditorFiles } from './useHandleEditorFiles';

export const useEditorDragAndDrop = (name: string) => {
    const { handleFiles, isLoading } = useHandleEditorFiles(name);
    const [isDragging, { on, off }] = useBooleanState(false);

    const enableDragging = (event: React.DragEvent) => {
        event.preventDefault();
        on();
    };

    const disableDragging = (event: React.DragEvent) => {
        event.preventDefault();
        off();
    };

    const handleDroppedFiles = async (files?: FileList | null) => {
        off();
        await handleFiles(files);
    };

    return {
        isDragging,
        enableDragging,
        disableDragging,
        handleDroppedFiles,
        isDropLoading: isLoading,
    };
};
