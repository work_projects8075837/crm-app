import { RefObject, useEffect } from 'react';
import ReactQuill from 'react-quill';
import { useHandleEditorFiles } from './useHandleEditorFiles';

export const useListenEditorPaste = (
    editorRefElem: RefObject<ReactQuill>,
    name: string,
) => {
    const { handleFiles, isLoading } = useHandleEditorFiles(name);
    useEffect(() => {
        const onPaste = async (event: ClipboardEvent) => {
            event.stopImmediatePropagation();

            editorRefElem.current?.editor?.disable();

            await handleFiles(event.clipboardData?.files);

            editorRefElem.current?.editor?.enable();

            editorRefElem.current?.editor?.root.scrollHeight;

            setTimeout(() => {
                console.log(editorRefElem.current?.editor?.root.scrollHeight);
                if (editorRefElem.current?.editor) {
                    editorRefElem.current?.editor?.root.scrollTo({
                        top: editorRefElem.current?.editor?.root.scrollHeight,
                    });
                }
            }, 500);
        };

        editorRefElem.current?.editor?.root.addEventListener('paste', onPaste);

        return () => {
            editorRefElem.current?.editor?.root.removeEventListener(
                'paste',
                onPaste,
            );
        };
    }, []);

    return { isClipboardLoading: isLoading };
};
