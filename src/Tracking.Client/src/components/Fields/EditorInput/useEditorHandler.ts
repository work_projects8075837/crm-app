import { useRef } from 'react';
import { useFormContext } from 'react-hook-form';
import ReactQuill from 'react-quill';

export const dataURItoBlob = (dataURI: string) => {
    const byteString = atob(dataURI.split(',')[1]);

    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab as BlobPart], { type: mimeString });
};

export const useEditorHandler = (name: string) => {
    const { setValue } = useFormContext();

    const editorRefElem = useRef<ReactQuill>(null);

    const onEditorChange = async (newState: string) => {
        if (newState.includes('data:image')) {
            if (editorRefElem.current) {
                editorRefElem.current.editor?.disable();

                const editorPlace = document.createElement('div');
                editorPlace.innerHTML = newState;

                const images = editorPlace.getElementsByTagName('img');

                for await (const image of images) {
                    if (image.src.includes('data:image')) {
                        // const blobImg = dataURItoBlob(image.src);
                        // const files = await mutateAsync([
                        //     new File(
                        //         [blobImg],
                        //         `image_${blobImg.size}.${
                        //             blobImg.type.split('/')[1]
                        //         }`,
                        //         { type: blobImg.type },
                        //     ),
                        // ]);
                        // image.src = files[0].path;
                        image.remove();
                    }
                }

                editorRefElem.current.value = editorPlace.innerHTML;
                if (editorRefElem.current.editor) {
                    editorRefElem.current.editor.root.innerHTML =
                        editorPlace.innerHTML;
                }

                setValue(name, editorPlace.innerHTML);
                editorRefElem.current.editor?.enable();

                return;
            }
        }
        setValue(name, newState);
    };

    return { onEditorChange, editorRefElem };
};
