import { viewImage } from '@/components/Elements/ImageViewModal/viewImage';
import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useFormContext, useWatch } from 'react-hook-form';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import '../../../style/elements/editor.scss';
import { useEditorDragAndDrop } from './useEditorDragAndDrop';
import { useEditorHandler } from './useEditorHandler';

export const dataURItoBlob = (dataURI: string) => {
    const byteString = atob(dataURI.split(',')[1]);

    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab as BlobPart], { type: mimeString });
};

export const EMPTY_EDITOR_LN = '<p></p>';

interface EditorInputProps {
    name: string;
    className?: string;
    wrapperClass?: string;
}
// Quill.import();
import { useListenEditorPaste } from './useListenEditorPaste';

const EditorInput: React.FC<EditorInputProps> = ({
    name,
    className = '',
    wrapperClass = '',
}) => {
    const { register } = useFormContext();
    const currentValue = useWatch({ name });

    const { onEditorChange, editorRefElem } = useEditorHandler(name);

    const { isClipboardLoading } = useListenEditorPaste(editorRefElem, name);

    const {
        disableDragging,
        enableDragging,
        handleDroppedFiles,
        isDragging,
        isDropLoading,
    } = useEditorDragAndDrop(name);

    const isLoading = isDropLoading || isClipboardLoading;

    return (
        <>
            <div
                className={`editorWrapper ${wrapperClass} ${
                    isDragging ? 'dragEditor' : ''
                }`}
                onDragEnter={enableDragging}
                onDragOver={enableDragging}
                onDragLeave={disableDragging}
                onDrop={(event) => {
                    event.preventDefault();
                    handleDroppedFiles(event.dataTransfer.files);
                }}
                onClick={async (event) => {
                    event.stopPropagation();
                    if (event.target instanceof HTMLImageElement) {
                        await viewImage(event.target.src);
                    }
                }}
            >
                {isLoading && (
                    <div className={'editorLoaderWrapper'}>
                        <LoaderCircle size={100} />
                    </div>
                )}
                <input
                    type='text'
                    {...register(name)}
                    className='hiddenInput'
                />
                <ReactQuill
                    ref={editorRefElem}
                    theme='snow'
                    value={currentValue}
                    onChange={onEditorChange}
                    modules={{
                        toolbar: [
                            [{ size: ['small', false, 'large', 'huge'] }],

                            [{ header: 1 }, { header: 2 }],
                            ['bold', 'italic', 'underline', 'strike'],

                            ['code-block'],

                            [{ list: 'ordered' }, { list: 'bullet' }],

                            [{ color: [] }, { background: [] }],
                        ],
                    }}
                    className={`editor ${className}`}
                />
            </div>

            {/* <button onClick={() => console.log(getValues(name))}>
                Значение
            </button> */}
        </>
    );
};

export default observer(EditorInput);
