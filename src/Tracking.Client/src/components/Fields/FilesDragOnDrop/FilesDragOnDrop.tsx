import React from 'react';
import style from '../../../style/layout/files_modal.module.scss';
import { useBooleanState } from '@/hooks/useBooleanState';
import { useFormContext } from 'react-hook-form';
import { useFormEntityValueArray } from '../hooks/useFormEntityValueArray';
import { convertFiles } from './convertFiles';
import { useFileCreateMutation } from './useFileCreateMutation';
import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import { useFilesPasteHandler } from '../EditorInput/useFilesPasteHandler';

interface FilesDragOnDropProps {
    name: string;
}

const FilesDragOnDrop: React.FC<FilesDragOnDropProps> = ({ name }) => {
    const { addArray } = useFormEntityValueArray(name);
    const [isDragging, { on, off }] = useBooleanState(false);
    const { setValue } = useFormContext();
    const { mutateAsync, isLoading } = useFileCreateMutation();

    const enableDragging = (event: React.DragEvent) => {
        event.preventDefault();
        on();
    };

    const disableDragging = (event: React.DragEvent) => {
        event.preventDefault();
        off();
    };

    const handleFiles = async (files?: FileList | null) => {
        if (!files) return;

        const loadedFiles = await mutateAsync(files);

        addArray(loadedFiles);
        // setValue(TICKET_FILES_NAME, files);
    };

    return (
        <div className={style.filesInputWrapper}>
            <label
                className={`${style.filesInput} ${
                    isDragging ? style.filesInputDrag : ''
                }`}
                onDragEnter={enableDragging}
                onDragOver={enableDragging}
                onDragLeave={disableDragging}
                onDrop={async (event) => {
                    event.preventDefault();
                    if (!isLoading) await handleFiles(event.dataTransfer.files);
                    off();
                }}
            >
                <input
                    disabled={isLoading}
                    type='file'
                    className='hiddenInput'
                    onChange={(event) => {
                        event.preventDefault();
                        handleFiles(event.currentTarget.files);
                    }}
                    id={`file__input__${name}`}
                    multiple={true}
                />
                {isDragging ? (
                    <>Загрузить файл</>
                ) : (
                    <>
                        {isLoading ? (
                            <>
                                <LoaderCircle size={25} borderWidth={1} />
                            </>
                        ) : (
                            <>
                                <label
                                    className={style.filesInputButton}
                                    htmlFor={`file__input__${name}`}
                                >
                                    Выберите файл
                                </label>
                            </>
                        )}
                    </>
                )}
            </label>
        </div>
    );
};

export default FilesDragOnDrop;
