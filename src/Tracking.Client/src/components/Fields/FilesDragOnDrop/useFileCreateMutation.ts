import { fileService } from '@/services/file/file';
import { useMutation } from '@tanstack/react-query';

export const useFileCreateMutation = () => {
    const mutation = useMutation({
        mutationFn: async (files: FileList | File[]) => {
            return await fileService.create(files).then((res) => res.data);
        },
    });

    return mutation;
};
