import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { characterProblemHooks } from '@/services/character_problem/character_problem';
import { ICharacterProblem } from '@/services/character_problem/types';
import React, { useDeferredValue } from 'react';
import { Link, useParams } from 'react-router-dom';
import style from '../../../style/layout/character_problem.module.scss';
import { TICKET_CHARACTER_PROBLEM_NAME } from '../EntitiesFields/ticket';
import SelectEntity from '../SelectEntity/SelectEntity';
import CardTitleValue from '../SelectedEntityCard/CardTitleValue';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';
import SelectedEntityNavbarLayout from '../SelectedEntityCard/SelectedEntityNavbar/SelectedEntityNavbarLayout';
import { useFormEntityValue } from '../hooks/useFormEntityValue';
interface CharacterProblemSelectProps {
    onlyDirectory?: boolean;
    isRequired?: boolean;
    title?: string;
}

const CharacterProblemSelect: React.FC<CharacterProblemSelectProps> = ({
    onlyDirectory = false,
    isRequired = true,
    title = 'Характер проблемы',
}) => {
    const searchInputModel = useSearchInputModel();
    const { data } = characterProblemHooks.useSearch(searchInputModel.value);

    const { characterProblemId } = useParams();

    const characterProblems = useDeferredValue(
        data?.data.filter(
            (c) => c.is_parent === onlyDirectory && c.id !== characterProblemId,
        ),
    );

    const { currentEntity } = useFormEntityValue<ICharacterProblem>(
        TICKET_CHARACTER_PROBLEM_NAME,
    );

    return (
        <>
            <SelectedEntityCard
                linkName='character_problem'
                name={TICKET_CHARACTER_PROBLEM_NAME}
                title={title}
                navbar={
                    <>
                        {currentEntity?.url && (
                            <SelectedEntityNavbarLayout>
                                <CardTitleValue
                                    title='Ссылка на базу знаний'
                                    value={
                                        <Link
                                            to={currentEntity?.url || ''}
                                            target='_blank'
                                            className={style.link}
                                        >
                                            {currentEntity?.url}
                                        </Link>
                                    }
                                />
                            </SelectedEntityNavbarLayout>
                        )}
                    </>
                }
            >
                <EntitySelectFieldLayout label={title}>
                    <SelectEntity
                        searchInputModel={searchInputModel}
                        isRequired={isRequired}
                        name={TICKET_CHARACTER_PROBLEM_NAME}
                        entities={characterProblems}
                    />
                </EntitySelectFieldLayout>
            </SelectedEntityCard>
        </>
    );
};

export default React.memo(CharacterProblemSelect);
