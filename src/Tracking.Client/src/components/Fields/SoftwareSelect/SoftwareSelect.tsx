import CardVersionIcon from '@/components/Elements/CardVersionIcon/CardVersionIcon';
import { softwareModalOpen } from '@/components/SalePoints/SalePointEditInventory/CreateSoftwareModal/CreateSoftwareModal';
import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { softwareHooks } from '@/services/software/software';
import { ISoftware } from '@/services/software/types';
import React from 'react';
import { INVENTORY_SOFTWARE_NAME } from '../EntitiesFields/inventory';
import SelectEntity from '../SelectEntity/SelectEntity';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';
import { useFormEntityValue } from '../hooks/useFormEntityValue';

const SoftwareSelect: React.FC = () => {
    const model = useSearchInputModel();
    const { data } = softwareHooks.useSearch(model.value);
    const softwareData = data?.data.map((s) => [
        { symbol: 'version', text: s.version },
        { text: '' },
    ]);
    const { currentEntity } = useFormEntityValue<ISoftware>(
        INVENTORY_SOFTWARE_NAME,
    );

    return (
        <>
            {!!data && (
                <SelectedEntityCard
                    name={INVENTORY_SOFTWARE_NAME}
                    title='Программное обеспечение'
                    extraFields={
                        <FullExtraCardFields
                            first={{
                                icon: <CardVersionIcon />,
                                text: currentEntity?.version,
                            }}
                        />
                    }
                >
                    <EntitySelectFieldLayout label='Программное обеспечение'>
                        <SelectEntity
                            name={INVENTORY_SOFTWARE_NAME}
                            searchInputModel={model}
                            entities={data.data}
                            entitiesData={softwareData}
                            onCreateClick={() => softwareModalOpen.open()}
                        />
                    </EntitySelectFieldLayout>
                </SelectedEntityCard>
            )}
        </>
    );
};

export default SoftwareSelect;
