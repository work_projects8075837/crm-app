import Checkbox from '@/components/Elements/Checkbox/Checkbox';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';

interface CheckAllProps {
    checkHandler?: CheckEntitiesHandler;
}

const CheckAll: React.FC<CheckAllProps> = ({ checkHandler }) => {
    return (
        <>
            <Checkbox
                isChecked={checkHandler?.isAllChecked || false}
                toggle={() => checkHandler?.toggleAll()}
            />
        </>
    );
};

export default observer(CheckAll);
