import Checkbox from '@/components/Elements/Checkbox/Checkbox';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { computed } from 'mobx';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Entity } from '../SelectEntity/SelectEntity';

interface CheckItemProps<T extends Entity> {
    entity: T;
    checkHandler: CheckEntitiesHandler;
}

function CheckItem<T extends Entity>({
    checkHandler,
    entity,
}: CheckItemProps<T>) {
    const isChecked = computed(() => checkHandler.isChecked(entity.id)).get();
    return (
        <>
            <Checkbox
                isChecked={isChecked}
                toggle={() => checkHandler.toggle(entity)}
            />
        </>
    );
}

export default observer(CheckItem);
