import { addStatusModalOpen } from '@/components/Directions/AddStatusModal/AddStatusModal';
import StatusItem from '@/components/Tickets/StatusItem/StatusItem';
import { useTextInputModel } from '@/hooks/useTextInputModel';
import { useStatusSearch } from '@/services/status/status';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/elements/status_select.module.scss';
import CreateEntityButton from '../SelectEntity/SelectEntityOption/CreateEntityButton';
import SelectStatusButton from './SelectStatusButton';

interface SearchSelectStatusProps {
    openHandler: OpenHandler;
}

const SearchSelectStatus: React.FC<SearchSelectStatusProps> = ({
    openHandler,
}) => {
    const [search, searchModel] = useTextInputModel();

    const { data } = useStatusSearch(search);

    return (
        <>
            <div>
                <input
                    type='text'
                    className={style.input}
                    {...searchModel}
                    placeholder='Поиск...'
                />

                <div className={style.statuses}>
                    {data?.data.map((status) => (
                        <SelectStatusButton key={status.id} status={status}>
                            <StatusItem {...status} />
                        </SelectStatusButton>
                    ))}
                </div>

                <CreateEntityButton
                    className={style.createButton}
                    onClick={() => {
                        openHandler.close();
                        addStatusModalOpen.open();
                    }}
                />
            </div>
        </>
    );
};

export default SearchSelectStatus;
