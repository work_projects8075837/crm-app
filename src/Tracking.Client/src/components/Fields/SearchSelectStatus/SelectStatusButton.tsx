import { IStatus } from '@/services/status/types';
import React from 'react';
import style from '../../../style/elements/status_select.module.scss';
import { DIRECTION_STATUSES_NAME } from '../EntitiesFields/direction';
import { useFormEntityValueArray } from '../hooks/useFormEntityValueArray';

interface SelectStatusButtonProps {
    status: IStatus;
    children: React.ReactNode;
}

const SelectStatusButton: React.FC<SelectStatusButtonProps> = ({
    status,
    children,
}) => {
    const { add, remove, entities } = useFormEntityValueArray(
        DIRECTION_STATUSES_NAME,
    );
    const isSelected = entities?.some((s) => s.id === status.id);
    return (
        <>
            <button
                className={`${style.statusBtn} ${
                    isSelected ? style.statusBtnSelected : ''
                }`}
                onClick={(event) => {
                    event.preventDefault();
                    if (!isSelected) {
                        add(status);
                    } else {
                        remove(status.id);
                    }
                }}
            >
                {children}
            </button>
        </>
    );
};

export default SelectStatusButton;
