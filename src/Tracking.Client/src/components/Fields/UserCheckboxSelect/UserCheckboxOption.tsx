import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

interface UserCheckboxOptionProps {
    value: string;
    name: string;
}

const UserCheckboxOption: React.FC<UserCheckboxOptionProps> = () => {
    return <div className={style.option}></div>;
};

export default UserCheckboxOption;
