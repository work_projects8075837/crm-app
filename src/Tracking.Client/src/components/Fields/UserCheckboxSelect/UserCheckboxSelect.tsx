import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import { useTextInputModel } from '@/hooks/useTextInputModel';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { useSearchUsers } from '@/store/queries/stores/user/useSearchUsers';
import React from 'react';
import style from '../../../style/elements/select_option.module.scss';
import EmptyOptionEntity from '../SelectEntity/SelectEntityOption/EmptyOptionEntity';
import UserCheckboxButton from './UserCheckboxButton';
import UserSearchInput from './UserSearchInput';

// const users = [
//     { id: '1', name: 'Петр Петрович', phone: '+7 900 999 00-00' },
//     { id: '2', name: 'Нина Павловна', phone: '+7 900 999 00-00' },
//     { id: '3', name: 'Игорь Агапьев', phone: '+7 900 999 00-00' },
//     { id: '4', name: 'Артур Федотов', phone: '+7 900 999 00-00' },
//     { id: '5', name: 'Павел Немчинский', phone: '+7 900 999 00-00' },
//     { id: '6', name: 'Саул Рахжатов', phone: '+7 900 999 00-00' },
// ];

const UserCheckboxSelect: React.FC = () => {
    const [search, searchModel] = useTextInputModel();
    const currentUser = useCurrentUser();

    const { data } = useSearchUsers({ search });

    const users = data?.data.filter((u) => u.id !== currentUser.data?.id);

    return (
        <>
            <div className={style.flexSelect}>
                <UserSearchInput searchModel={searchModel} />
                {users ? (
                    <>
                        <div className={style.borderedSelectChildren}>
                            {users.length ? (
                                <>
                                    {users.map((entity) => {
                                        return (
                                            <UserCheckboxButton
                                                key={entity.id}
                                                value={entity}
                                            />
                                        );
                                    })}
                                </>
                            ) : (
                                <>
                                    <EmptyOptionEntity text='Нет подходящих сотрудников' />
                                </>
                            )}
                        </div>
                    </>
                ) : (
                    <>
                        <LoaderCircle size={20} borderWidth={1} />
                    </>
                )}
            </div>
        </>
    );
};

export default React.memo(UserCheckboxSelect);
