import { ITextInputModel } from '@/hooks/useTextInputModel';
import React, { HTMLAttributes } from 'react';
import style from '../../../style/elements/small_input.module.scss';

interface UserSearchInputProps extends HTMLAttributes<HTMLInputElement> {
    searchModel: ITextInputModel;
}

const UserSearchInput: React.FC<UserSearchInputProps> = ({
    searchModel,
    ...inputProps
}) => {
    return (
        <>
            <input
                {...inputProps}
                {...searchModel}
                type='text'
                className={style.input}
                style={{ borderRadius: '2px 2px 0 0' }}
                placeholder='Введите ФИО'
            />
        </>
    );
};

export default UserSearchInput;
