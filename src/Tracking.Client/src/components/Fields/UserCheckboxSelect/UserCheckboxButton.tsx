import { useFormMembers } from '@/components/Tickets/hooks/useFormMembers';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import style from '../../../style/elements/select_option.module.scss';

interface UserCheckboxButtonProps {
    value: {
        id: string;
        name: string;
        phone?: string;
    };
}

const UserCheckboxButton: React.FC<UserCheckboxButtonProps> = ({ value }) => {
    const { setValue } = useFormContext();
    const { members, remove, add } = useFormMembers();

    const isSelected = members?.some((member) => member.user.id === value.id);
    // const isAuthor =
    //     members?.find((member) => member.type === TICKET_MEMBERS_TYPES.author)
    //         ?.user.id === value.id;
    return (
        <>
            <div className={style.optionLabel}>
                <button
                    // disabled={isAuthor}
                    className={`${style.option} ${
                        isSelected ? style.optionSelected : ''
                    }`}
                    onClick={(event) => {
                        event.preventDefault();

                        if (isSelected) {
                            remove(value.id);
                        } else {
                            add(value);
                        }
                    }}
                >
                    <span className={style.optionName}>{value.name}</span>
                </button>
            </div>
        </>
    );
};

export default UserCheckboxButton;
