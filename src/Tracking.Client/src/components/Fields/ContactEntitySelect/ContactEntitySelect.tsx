import CardEmailIcon from '@/components/Elements/CardEmailIcon/CardEmailIcon';
import CardPhoneIcon from '@/components/Elements/CardPhoneIcon/CardPhoneIcon';
import {
    addContactModalOpenHandler,
    providedContactValue,
} from '@/components/Tickets/AddContactModal/AddContactModal';
import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import { formatPhoneLink } from '@/components/utils/formatPhoneLink';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { contactHooks } from '@/services/contact/contact';
import { IContact } from '@/services/contact/types';
import { ISalePoint } from '@/services/sale_point/types';
import React from 'react';
import {
    TICKET_CONTACT_NAME,
    TICKET_SALE_POINT_NAME,
} from '../EntitiesFields/ticket';
import SelectEntity from '../SelectEntity/SelectEntity';
import CardTitleValue from '../SelectedEntityCard/CardTitleValue';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';
import SelectedEntityNavbarLayout from '../SelectedEntityCard/SelectedEntityNavbar/SelectedEntityNavbarLayout';
import { useFormEntityValue } from '../hooks/useFormEntityValue';

interface ContactEntitySelectProps {
    required?: boolean;
}

const ContactEntitySelect: React.FC<ContactEntitySelectProps> = ({
    required = false,
}) => {
    const model = useSearchInputModel();
    const { data } = contactHooks.useSearch(model.value);

    const formContact = useFormEntityValue<IContact>(TICKET_CONTACT_NAME);
    const currentContact = formContact.currentEntity;

    const fieldName = 'Контакт';

    const formSalePoint = useFormEntityValue<ISalePoint>(
        TICKET_SALE_POINT_NAME,
    );
    const currentSalePoint = formSalePoint.currentEntity;

    const positionBySalePointPosition = currentContact?.position.find(
        (p) => p.sale_point.id === currentSalePoint?.id,
    )?.position;

    return (
        <>
            <SelectedEntityCard
                name={TICKET_CONTACT_NAME}
                title={fieldName}
                extraFields={
                    <FullExtraCardFields
                        first={{
                            icon: <CardPhoneIcon />,
                            text: (
                                <>
                                    {currentContact?.phone_number.map((p) => (
                                        <a
                                            key={p.id}
                                            href={`tel:${formatPhoneLink(
                                                p.name,
                                            )}`}
                                        >
                                            {p.name}
                                        </a>
                                    ))}
                                </>
                            ),
                        }}
                        third={{
                            icon: <CardEmailIcon />,
                            text: currentContact?.email,
                        }}
                    />
                }
                navbar={
                    <SelectedEntityNavbarLayout>
                        <CardTitleValue
                            title={'Должность'}
                            value={
                                positionBySalePointPosition || 'Указать позже'
                            }
                        />
                    </SelectedEntityNavbarLayout>
                }
            >
                <EntitySelectFieldLayout label={fieldName}>
                    <SelectEntity
                        isRequired={required}
                        name={TICKET_CONTACT_NAME}
                        entities={data?.data}
                        searchInputModel={model}
                        onCreateClick={() => {
                            providedContactValue.setValue(model.value);

                            addContactModalOpenHandler.open();
                        }}
                    />
                </EntitySelectFieldLayout>
            </SelectedEntityCard>
        </>
    );
};

export default React.memo(ContactEntitySelect);
