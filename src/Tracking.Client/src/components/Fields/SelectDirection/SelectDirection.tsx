import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { directionHooks } from '@/services/direction/direction';
import { IDirection } from '@/services/direction/types';
import React from 'react';
import { TICKET_DIRECTION_NAME } from '../EntitiesFields/ticket';
import SelectEntity from '../SelectEntity/SelectEntity';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';

interface SelectDirectionProps {
    onChangeAction?: (data: IDirection) => void;
    withLabel?: boolean;
    onCloseClick?: () => void;
    required?: boolean;
    placeholder?: string;
}

const SelectDirection: React.FC<SelectDirectionProps> = ({
    onChangeAction,
    withLabel = true,
    required = false,
    onCloseClick,
    placeholder,
}) => {
    const model = useSearchInputModel();
    const { data } = directionHooks.useSearch(model.value);

    return (
        <>
            <SelectedEntityCard
                linkName='direction'
                name={TICKET_DIRECTION_NAME}
                title='Направление'
                onCloseClick={onCloseClick}
            >
                {withLabel ? (
                    <EntitySelectFieldLayout label='Направление'>
                        <SelectEntity
                            placeholder={placeholder}
                            isRequired={required}
                            name={TICKET_DIRECTION_NAME}
                            searchInputModel={model}
                            entities={data?.data}
                            onChangeAction={onChangeAction}
                        />
                    </EntitySelectFieldLayout>
                ) : (
                    <SelectEntity
                        placeholder={placeholder}
                        isRequired={required}
                        name={TICKET_DIRECTION_NAME}
                        searchInputModel={model}
                        entities={data?.data}
                        onChangeAction={onChangeAction}
                    />
                )}
            </SelectedEntityCard>
        </>
    );
};

export default React.memo(SelectDirection);
