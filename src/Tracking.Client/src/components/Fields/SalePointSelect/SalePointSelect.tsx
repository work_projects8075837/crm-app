import CardAddressIcon from '@/components/Elements/CardAddressIcon/CardAddressIcon';
import CardEmailIcon from '@/components/Elements/CardEmailIcon/CardEmailIcon';
import CardPhoneIcon from '@/components/Elements/CardPhoneIcon/CardPhoneIcon';
import { warnMessage } from '@/components/Elements/WarnWindow/warnMessage';
import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import {
    formatDate,
    formatDateWithoutTime,
} from '@/components/utils/formatDate';
import { formatPhoneLink } from '@/components/utils/formatPhoneLink';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { salePointHooks } from '@/services/sale_point/sale_point';
import { ISalePoint } from '@/services/sale_point/types';
import React, { useDeferredValue } from 'react';
import { Link, useParams } from 'react-router-dom';
import style from '../../../style/layout/sale_point.module.scss';
import { EMPTY_EDITOR_LN } from '../EditorInput/EditorInput';
import { TICKET_SALE_POINT_NAME } from '../EntitiesFields/ticket';
import SelectEntity from '../SelectEntity/SelectEntity';
import { SERVICE_STATUSES } from '../SelectServiceStatus/SelectServiceStatus';
import CardTitleValue from '../SelectedEntityCard/CardTitleValue';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';
import SelectedEntityNavbarLayout from '../SelectedEntityCard/SelectedEntityNavbar/SelectedEntityNavbarLayout';
import { useFormEntityValue } from '../hooks/useFormEntityValue';
interface SalePointSelectProps {
    onlyDirectory?: boolean;
    name?: string;
    title?: string;
    required?: boolean;
}

export const isEditorContentExist = (content?: string | null) => {
    const clearContent = content
        ?.replaceAll(' ', '')
        .replaceAll('<br>', '')
        .replaceAll('<p>', '')
        .replaceAll('</p>', '');
    return !!(clearContent && clearContent !== EMPTY_EDITOR_LN);
};

const SalePointSelect: React.FC<SalePointSelectProps> = ({
    onlyDirectory = false,
    name = TICKET_SALE_POINT_NAME,
    title = 'Заведение',
    required = false,
}) => {
    const { salePointId } = useParams();
    const searchInputModel = useSearchInputModel();
    const { data } = salePointHooks.useSearch(searchInputModel.value, {
        is_hard: onlyDirectory,
        is_parent: onlyDirectory,
    });

    const formCounterParty = useFormEntityValue<ISalePoint>(name);
    const currentSalePoint = formCounterParty.currentEntity;

    const salePointData = onlyDirectory
        ? undefined
        : data?.data.map((salePoint) => {
              return [
                  { symbol: 'phone', text: salePoint.phone },
                  { symbol: 'email', text: salePoint.email },
              ];
          });

    const salePoints = useDeferredValue(
        data?.data.filter(
            (s) => s.is_parent === onlyDirectory && s.id !== salePointId,
        ),
    );

    const withRegulations = isEditorContentExist(currentSalePoint?.regulations);
    const finishDate = formatDate(currentSalePoint?.service_finish);

    return (
        <>
            <SelectedEntityCard
                linkName='sale_point'
                title={title}
                name={name}
                extraFields={
                    <>
                        {!onlyDirectory && (
                            <FullExtraCardFields
                                first={{
                                    icon: <CardPhoneIcon />,
                                    text: (
                                        <Link
                                            to={`tel:${formatPhoneLink(
                                                currentSalePoint?.phone,
                                            )}`}
                                        >
                                            {currentSalePoint?.phone}
                                        </Link>
                                    ),
                                }}
                                second={{
                                    icon: <CardEmailIcon />,
                                    text: (
                                        <Link
                                            to={`mailto:${currentSalePoint?.email}`}
                                        >
                                            {currentSalePoint?.email}
                                        </Link>
                                    ),
                                }}
                                third={
                                    onlyDirectory
                                        ? undefined
                                        : {
                                              icon: <CardAddressIcon />,
                                              text: currentSalePoint?.address,
                                          }
                                }
                            />
                        )}
                    </>
                }
                navbar={
                    <>
                        <SelectedEntityNavbarLayout>
                            <CardTitleValue
                                title={
                                    !currentSalePoint?.is_parent &&
                                    'Статус обслуживания'
                                }
                                value={
                                    !currentSalePoint?.is_parent && (
                                        <>
                                            {currentSalePoint?.status ===
                                            SERVICE_STATUSES.not_subscriber ? (
                                                <span
                                                    className={style.redValue}
                                                >
                                                    {currentSalePoint?.status}
                                                </span>
                                            ) : (
                                                <span
                                                    className={style.greenValue}
                                                >
                                                    {currentSalePoint?.status}
                                                    {finishDate && (
                                                        <>
                                                            {' '}
                                                            до{' '}
                                                            {formatDateWithoutTime(
                                                                currentSalePoint?.service_finish,
                                                            )}
                                                        </>
                                                    )}
                                                </span>
                                            )}
                                        </>
                                    )
                                }
                            />
                            {withRegulations && (
                                <CardTitleValue
                                    title='Регламент'
                                    value={
                                        <Link
                                            to={`/sale_point/single/${currentSalePoint?.id}/regulations/`}
                                            target='_blank'
                                            className={style.rulesLink}
                                        >
                                            Регламент заведения
                                        </Link>
                                    }
                                />
                            )}
                        </SelectedEntityNavbarLayout>
                    </>
                }
            >
                <EntitySelectFieldLayout label={title}>
                    <SelectEntity
                        isRequired={required}
                        onChangeAction={(salePoint: ISalePoint) => {
                            if (isEditorContentExist(salePoint.regulations)) {
                                warnMessage({
                                    text: 'Выбранное вами заведение имеет регламент!!!',
                                });
                            }
                        }}
                        searchInputModel={searchInputModel}
                        name={name}
                        entities={salePoints}
                        entitiesData={salePointData}
                    />
                </EntitySelectFieldLayout>
            </SelectedEntityCard>
        </>
    );
};

export default React.memo(SalePointSelect);
