import { createJobToCharacterProblemModalOpen } from '@/components/CharacterProblems/CreateJobModal/CreateJobModal';
import { selectJobsModalOpen } from '@/components/CharacterProblems/SelectJobsModal/SelectJobsModal';
import { useTextInputModel } from '@/hooks/useTextInputModel';
import { jobHooks } from '@/services/job/job';
import React from 'react';
import { CHARACTER_PROBLEM_JOBS_NAME } from '../EntitiesFields/character_problem';
import SearchCreateSelect from '../SearchCreateSelect/SearchCreateSelect';
import CreateEntityButton from '../SelectEntity/SelectEntityOption/CreateEntityButton';

const SearchSelectCharacterProblemJobs: React.FC = () => {
    const [value, model, setSearch] = useTextInputModel();
    const { data } = jobHooks.useSearch(value);
    return (
        <>
            <SearchCreateSelect
                entities={data?.data}
                name={CHARACTER_PROBLEM_JOBS_NAME}
                searchModelState={{ value, model, setSearch }}
                addButton={
                    <CreateEntityButton
                        onClick={() => {
                            selectJobsModalOpen.close();
                            createJobToCharacterProblemModalOpen.open();
                        }}
                    />
                }
            />
        </>
    );
};

export default SearchSelectCharacterProblemJobs;
