import CardEmailIcon from '@/components/Elements/CardEmailIcon/CardEmailIcon';
import CardPhoneIcon from '@/components/Elements/CardPhoneIcon/CardPhoneIcon';
import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import FullExtraCardFields from '@/components/Tickets/ExtraCardFields/FullExtraCardFields';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { IUser } from '@/services/user/types';
import { useSearchUsers } from '@/store/queries/stores/user/useSearchUsers';
import React from 'react';
import { SALE_POINT_MANAGER_NAME } from '../EntitiesFields/sale_point';
import SelectEntity from '../SelectEntity/SelectEntity';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';
import { useFormEntityValue } from '../hooks/useFormEntityValue';

interface SelectManagerProps {
    title?: string;
    name?: string;
    onlySelect?: boolean;
    fieldClass?: string;
    required?: boolean;
}

const SelectManager: React.FC<SelectManagerProps> = ({
    title = 'Менеджер по точке',
    name = SALE_POINT_MANAGER_NAME,
    required = false,
    onlySelect = false,
    fieldClass,
}) => {
    const model = useSearchInputModel();
    const { data } = useSearchUsers({ search: model.value });
    const { currentEntity } = useFormEntityValue<IUser>(name);
    return (
        <>
            {onlySelect ? (
                <SelectEntity
                    entities={data?.data}
                    name={name}
                    searchInputModel={model}
                    isRequired={required}
                />
            ) : (
                <SelectedEntityCard
                    name={name}
                    title={title}
                    extraFields={
                        <FullExtraCardFields
                            first={{
                                icon: <CardPhoneIcon />,
                                text: currentEntity?.phone,
                            }}
                            second={{
                                icon: <CardEmailIcon />,
                                text: currentEntity?.email,
                            }}
                        />
                    }
                >
                    <EntitySelectFieldLayout label={title}>
                        <SelectEntity
                            fieldClass={fieldClass}
                            entities={data?.data}
                            name={name}
                            searchInputModel={model}
                            isRequired={required}
                        />
                    </EntitySelectFieldLayout>
                </SelectedEntityCard>
            )}
        </>
    );
};

export default SelectManager;
