import { ITextInputModel } from '@/hooks/useTextInputModel';
import React from 'react';
import style from '../../../style/forms/field.module.scss';

interface SearchCreateInputProps {
    inputModel: ITextInputModel;
    isDisabled?: boolean;
    placeholder?: string;
}

const SearchCreateInput: React.ForwardRefRenderFunction<
    HTMLInputElement,
    SearchCreateInputProps
> = ({ inputModel, isDisabled = false, placeholder }, ref) => {
    return (
        <>
            <input
                type='text'
                {...inputModel}
                className={style.emptyInput}
                placeholder={placeholder || 'Поиск...'}
                disabled={isDisabled}
                ref={ref}
            />
        </>
    );
};

export default React.forwardRef(SearchCreateInput);
