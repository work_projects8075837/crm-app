import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import Svg from '@/components/Elements/Svg/Svg';
import { performExistCallback } from '@/components/utils/performExistCallback';
import classNames from 'classnames';
import React from 'react';
import style from '../../../style/elements/input_button.module.scss';

interface SearchCreateButtonProps {
    onClick?: () => void;
    isDisabled?: boolean;
    isLoading?: boolean;
}

const SearchCreateButton: React.FC<SearchCreateButtonProps> = ({
    onClick,
    isDisabled = false,
    isLoading = false,
}) => {
    const disabled = isLoading || isDisabled;
    return (
        <>
            <div
                className={classNames(style.button, {
                    [style.disabledButton]: disabled,
                })}
                onClick={
                    disabled
                        ? undefined
                        : (event) => {
                              event.preventDefault();
                              performExistCallback(onClick);
                          }
                }
            >
                {isLoading ? (
                    <LoaderCircle color='#fff' size={14} borderWidth={1} />
                ) : (
                    <Svg symbol='white_plus' />
                )}
            </div>
        </>
    );
};

export default SearchCreateButton;
