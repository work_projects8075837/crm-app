import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import { ITextInputModel, useTextInputModel } from '@/hooks/useTextInputModel';
import classNames from 'classnames';
import React, { useDeferredValue, useEffect } from 'react';
import { RegisterOptions, useFormContext } from 'react-hook-form';
import style from '../../../style/elements/select_option.module.scss';
import {
    EntitiesData,
    Entity,
    SearchInputModelState,
} from '../SelectEntity/SelectEntity';
import EmptyOptionEntity from '../SelectEntity/SelectEntityOption/EmptyOptionEntity';
import OptionEntityButton from '../SelectEntity/SelectEntityOption/OptionEntityButton';
import { useCustomFucus } from '../hooks/useCustomFucus';
import SearchCreateInput from './SearchCreateInput';
import SearchCreateInputLayout from './SearchCreateSelectLayout';

export interface SearchInputProps {
    inputModel: ITextInputModel;
    placeholder?: string;
    isDisabled?: boolean;
}

interface SearchCreateSelectProps {
    createButton?: React.ReactNode;
    addButton?: React.ReactNode;
    searchModelState?: SearchInputModelState;
    entities?: Entity[];
    name: string;
    isDisabled?: boolean;
    placeholder?: string;
    isCheckbox?: boolean;
    pattern?: RegExp;
    entitiesData?: EntitiesData;
    searchInput?: React.ReactNode;
    options?: RegisterOptions;
    focus?: () => void;
}

const SearchCreateSelect: React.FC<SearchCreateSelectProps> = ({
    searchModelState,
    entities,
    name,
    createButton,
    addButton,
    isDisabled = false,
    placeholder,
    isCheckbox = true,
    pattern,
    entitiesData,
    searchInput,
    options,
    focus,
}) => {
    const { register, getFieldState, formState } = useFormContext();
    const [search, searchModel] = useTextInputModel('', pattern);

    const filteredEntities = useDeferredValue(
        searchModelState
            ? entities
            : entities?.filter((tag) => {
                  return (
                      tag.name &&
                      tag.name
                          .toLocaleLowerCase()
                          .includes(search.toLocaleLowerCase())
                  );
              }),
    );

    useEffect(() => {
        register(name, options);
    }, []);

    const { error } = getFieldState(name, formState);

    const { inputRef, ...inputFocus } = useCustomFucus();

    return (
        <div className={style.flexSelect}>
            <SearchCreateInputLayout
                className={classNames({
                    [style.errorBorder]: !!error,
                })}
            >
                <>
                    <>{createButton}</>
                </>
                {searchInput || (
                    <SearchCreateInput
                        placeholder={placeholder}
                        inputModel={searchModelState?.model || searchModel}
                        isDisabled={isDisabled}
                        ref={inputRef}
                    />
                )}
            </SearchCreateInputLayout>
            {filteredEntities ? (
                <>
                    <div
                        className={classNames(style.borderedSelectList, {
                            [style.errorBorder]: !!error,
                        })}
                        onClick={focus || inputFocus.focus}
                    >
                        <div className={style.searchSelectChildren}>
                            {filteredEntities.length ? (
                                <>
                                    {filteredEntities.map((entity, index) => {
                                        return (
                                            <OptionEntityButton
                                                fields={entitiesData?.[index]}
                                                key={entity.id}
                                                name={name}
                                                nameText={entity.name}
                                                value={entity}
                                                isCheckbox={isCheckbox}
                                            />
                                        );
                                    })}
                                </>
                            ) : (
                                <>
                                    <EmptyOptionEntity text='Нет подходящих вариантов' />
                                </>
                            )}
                        </div>
                        {addButton}
                    </div>
                </>
            ) : (
                <>
                    <LoaderCircle size={20} borderWidth={1} />
                </>
            )}
        </div>
    );
};

export default SearchCreateSelect;
