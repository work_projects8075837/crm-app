import classNames from 'classnames';
import React from 'react';
import style from '../../../style/forms/field.module.scss';

interface SearchCreateSelectLayoutProps {
    children: React.ReactNode;
    className?: string;
}

const SearchCreateSelectLayout: React.FC<SearchCreateSelectLayoutProps> = ({
    children,
    className,
}) => {
    return (
        <>
            <div
                className={classNames(
                    style.inputEmptyLayoutFlatBottom,
                    className,
                )}
            >
                {children}
            </div>
        </>
    );
};

export default SearchCreateSelectLayout;
