import Switcher from '@/components/Elements/Switcher/Switcher';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useFormContext } from 'react-hook-form';

const ParentEntitySwitcher: React.FC = () => {
    const { clearErrors, setValue, getValues } = useFormContext();
    const toggle = () => {
        setValue('is_parent', !getValues('is_parent'));
    };
    return (
        <>
            <label className='switcherField'>
                <Switcher
                    defaultChecked={!!getValues('is_parent')}
                    onChange={() => {
                        clearErrors();
                        toggle();
                    }}
                />

                <span>Сделать папкой</span>
            </label>
        </>
    );
};

export default observer(ParentEntitySwitcher);
