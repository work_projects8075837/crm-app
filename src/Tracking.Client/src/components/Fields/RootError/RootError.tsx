import React from 'react';
import styles from '@/style/forms/field.module.scss';
import { useFormContext } from 'react-hook-form';
import { SERVER_FORM_ERROR_NAME } from '@/components/Form/Types/Types';

const RootError: React.FC = () => {
    const { getFieldState, formState } = useFormContext();
    const { error } = getFieldState(SERVER_FORM_ERROR_NAME, formState);

    return (
        <div className={styles.errorMessage}>{!!error && error.message}</div>
    );
};

export default React.memo(RootError);
