import React from 'react';
import DurationInput from '../DurationInput/DurationInput';
import style from '../../../style/forms/field.module.scss';

interface DurationShortInputProps {
    name: string;
    required?: boolean;
}

const DurationShortInput: React.FC<DurationShortInputProps> = ({
    name,
    required,
}) => {
    return (
        <>
            <DurationInput
                name={name}
                required={required}
                labelsClass='form-short-group'
                inputsClass={style.shortInput}
            />
        </>
    );
};

export default DurationShortInput;
