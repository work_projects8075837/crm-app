import { confirmAction } from '@/components/Elements/ConfirmDialogWindow/confirmWindow';
import { globalLoader } from '@/components/Elements/GlobalLoader/GlobalLoader';
import { ADMIN_ROLE } from '@/components/Providers/AuthGuard/AuthProvider';
import { useAuthQuery } from '@/components/Providers/AuthGuard/useAuthQuery';
import ActionsMenuButton from '@/components/Tickets/TicketActionsMenu/ActionsMenuButton';
import { performExistCallback } from '@/components/utils/performExistCallback';
import { totalClearKey } from '@/store/queries/actions/base';
import { useMutation } from '@tanstack/react-query';
import React from 'react';
import toast from 'react-hot-toast';
import { useNavigate, useParams } from 'react-router-dom';

interface IServiceDelete {
    hide: (id: string) => Promise<unknown>;
}

interface ActionsMenuDeleteButtonProps {
    service: IServiceDelete;
    id?: string | null;
    onNavigate?: () => void;
    idName?: string;
    queryKey: string | symbol;
    navigateTo?: string;
}

const ActionsMenuDeleteButton: React.FC<ActionsMenuDeleteButtonProps> = ({
    service,
    idName,
    queryKey,
    navigateTo,
    id,
    onNavigate,
}) => {
    const { checkRoles } = useAuthQuery();
    const params = useParams();
    const navigate = useNavigate();
    const { mutate, isLoading } = useMutation({
        mutationFn: async () => {
            const entityId = idName ? params[idName] : id;
            if (!entityId) {
                throw Error();
            }

            globalLoader.open();

            if (!checkRoles([ADMIN_ROLE])) {
                toast.error('У вас нет прав на удаление');
                throw Error();
            }

            return await service.hide(entityId);
        },
        onSuccess: () => {
            totalClearKey(queryKey);
            if (navigateTo) navigate(navigateTo);
            performExistCallback(onNavigate);
        },
        onSettled: () => {
            globalLoader.close();
        },
    });

    return (
        <>
            <ActionsMenuButton
                symbol='delete_bag'
                text='Удалить'
                disabled={isLoading}
                onClick={async () => {
                    if (
                        await confirmAction({
                            title: 'Подтвердите удаление',
                            text: 'Вы действительно хотите продолжить и совершить удаление?',
                        })
                    ) {
                        mutate();
                    }
                }}
            />
        </>
    );
};

export default ActionsMenuDeleteButton;
