import { countHours } from '@/components/utils/countHours';
import { useTextInputModel } from '@/hooks/useTextInputModel';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import style from '../../../style/forms/field.module.scss';

const calcTimestamp = (days: number, hours: number, minutes: number) => {
    return (
        days * 28_800 * 1000 + hours * 3_600 * 1000 + minutes * 60 * 1000 ||
        undefined
    );
};

interface DurationInputProps {
    name: string;
    withDays?: boolean;
    labelsClass?: string;
    inputsClass?: string;
    fieldClass?: string;
    required?: boolean;
}

const DurationInput: React.FC<DurationInputProps> = ({
    name,
    required = true,
    withDays = false,
    labelsClass = '',
    inputsClass = '',
    fieldClass = '',
}) => {
    const {
        setValue,
        register,
        getFieldState,
        formState,
        clearErrors,
        getValues,
    } = useFormContext();
    const { error } = getFieldState(name, formState);

    const defaultValues = countHours(getValues(name));

    const [days, daysModel, setDays] = useTextInputModel();
    const [hours, hoursModel, setHours] = useTextInputModel(
        `${defaultValues.hours || ''}`,
    );
    const [minutes, minutesModel, setMinutes] = useTextInputModel(
        `${defaultValues.minutes || ''}`,
    );

    return (
        <>
            <input
                type='number'
                {...register(name, {
                    required: required ? 'Это поле обязательно' : false,
                    valueAsNumber: true,
                })}
                className='hiddenInput'
            />
            <div className={`${style.fieldsRow} ${fieldClass}`}>
                {withDays && (
                    <>
                        <input
                            placeholder='Рабочие дни'
                            type='number'
                            min='0'
                            {...daysModel}
                            className={`${style.input} ${inputsClass} ${
                                error ? style.errorInput : ''
                            }`}
                            onChange={(event) => {
                                const eventValue = event.currentTarget.value;
                                setDays(eventValue);
                                clearErrors(name);
                                setValue(
                                    name,
                                    calcTimestamp(
                                        Number(eventValue),
                                        Number(hours),
                                        Number(minutes),
                                    ),
                                );
                            }}
                        />
                    </>
                )}
                <input
                    placeholder='Часы'
                    type='number'
                    min='0'
                    {...hoursModel}
                    className={`${style.input} ${inputsClass} ${
                        error ? style.errorInput : ''
                    }`}
                    onChange={(event) => {
                        const eventValue = event.currentTarget.value;
                        setHours(eventValue);
                        clearErrors(name);

                        setValue(
                            name,
                            calcTimestamp(
                                Number(days),
                                Number(eventValue),
                                Number(minutes),
                            ),
                        );
                    }}
                />

                <input
                    placeholder='Минуты'
                    type='number'
                    min='0'
                    {...minutesModel}
                    className={`${style.input} ${inputsClass} ${
                        error ? style.errorInput : ''
                    }`}
                    onChange={(event) => {
                        const eventValue = event.currentTarget.value;
                        setMinutes(eventValue);
                        clearErrors(name);

                        setValue(
                            name,
                            calcTimestamp(
                                Number(days),
                                Number(hours),
                                Number(eventValue),
                            ),
                        );
                    }}
                />
            </div>
        </>
    );
};

export default DurationInput;
