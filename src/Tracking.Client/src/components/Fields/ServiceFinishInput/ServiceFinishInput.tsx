import React from 'react';
import { useWatch } from 'react-hook-form';
import style from '../../../style/forms/field.module.scss';
import DateInput from '../DateInput/DateInput';
import { SALE_POINT_SERVICE_FINISH_NAME } from '../EntitiesFields/sale_point';
import { Entity } from '../SelectEntity/SelectEntity';
import { SERVICE_STATUSES } from '../SelectServiceStatus/SelectServiceStatus';

const ServiceFinishInput: React.FC = () => {
    const serviceStatus: Entity = useWatch({ name: 'status' });
    return (
        <>
            <DateInput
                name={SALE_POINT_SERVICE_FINISH_NAME}
                isDisabled={
                    !serviceStatus ||
                    serviceStatus.name === SERVICE_STATUSES.not_subscriber
                }
                className={style.shortInput}
                wrapperClass={style.shortInputWrapper}
                options={{ required: false }}
            />
        </>
    );
};

export default ServiceFinishInput;
