import React, { HTMLAttributes, Ref, RefObject } from 'react';
import '../../../style/elements/radio_option.scss';

interface ColorOptionProps extends HTMLAttributes<HTMLInputElement> {
    value: string;
}

const ColorOption: React.ForwardRefRenderFunction<
    HTMLInputElement,
    ColorOptionProps
> = ({ value, ...inputProps }, ref) => {
    return (
        <>
            <label className='colorLabel'>
                <input
                    {...inputProps}
                    type='radio'
                    className='colorInput'
                    value={value}
                    ref={ref}
                />
                <div
                    className='colorValue'
                    style={{ backgroundColor: value }}
                />
            </label>
        </>
    );
};

export default React.forwardRef(ColorOption);
