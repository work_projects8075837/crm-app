import EntitySelectFieldLayout from '@/components/Tickets/EntitySelectFieldLayout/EntitySelectFieldLayout';
import { useSearchInputModel } from '@/hooks/useTextInputModel';
import { useSearchInventory } from '@/store/queries/stores/inventory/useSearchInventory';
import React from 'react';
import { INVENTORY_INVENTORY_NAME } from '../EntitiesFields/inventory';
import SelectEntity from '../SelectEntity/SelectEntity';
import SelectedEntityCard from '../SelectedEntityCard/SelectedEntityCard';

interface SelectInventoryProps {
    onlyDirectory?: boolean;
    bySalePoint?: boolean;
}

const SelectInventory: React.FC<SelectInventoryProps> = ({
    onlyDirectory = false,
    bySalePoint,
}) => {
    const model = useSearchInputModel();
    const { data } = useSearchInventory(model.value, bySalePoint);

    const inventories = data?.data.filter(
        (inventory) => inventory.is_parent === onlyDirectory,
    );
    return (
        <>
            <SelectedEntityCard
                name={INVENTORY_INVENTORY_NAME}
                title='Папка оборудования'
            >
                <EntitySelectFieldLayout label='Папка оборудования'>
                    <SelectEntity
                        name={INVENTORY_INVENTORY_NAME}
                        entities={inventories}
                        searchInputModel={model}
                    />
                </EntitySelectFieldLayout>
            </SelectedEntityCard>
        </>
    );
};

export default SelectInventory;
