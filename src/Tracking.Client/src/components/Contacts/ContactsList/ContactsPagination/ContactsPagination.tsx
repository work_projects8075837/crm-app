import Pagination from '@/components/Tables/Pagination/Pagination';
import PaginationButtonsList from '@/components/Tickets/TiketsPaginationButtonsList/PaginationButtonsList';
import { contactHooks } from '@/services/contact/contact';
import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';

const CONTACTS_PAGINATION = 'CONTACTS_PAGINATION';
export const contactsPaginationHandler = new PaginationHandler(
    CONTACTS_PAGINATION,
);

const ContactsPagination: React.FC = () => {
    const { data } = contactHooks.useList(contactsPaginationHandler);

    return (
        <>
            <Pagination
                paginationHandler={contactsPaginationHandler}
                pages={
                    <PaginationButtonsList
                        data={data}
                        paginationHandler={contactsPaginationHandler}
                    />
                }
            />
        </>
    );
};

export default observer(ContactsPagination);
