import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import { IContact } from '@/services/contact/types';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/contacts.module.scss';
import { contactCheckHandler } from './ContactsList';

type ContactItemProps = IContact;

const ContactItem: React.FC<ContactItemProps> = ({
    id,
    name,
    phone_number,
    email,
}) => {
    const navigate = useNavigate();
    return (
        <div
            className={style.contactsTableItem}
            onDoubleClick={() => {
                navigate(`/contact/${id}`);
            }}
        >
            <CheckItem
                checkHandler={contactCheckHandler}
                entity={{ id, name, phone_number, email }}
            />
            <div className={style.contactsTableItemField}>
                <span>{name}</span>
            </div>
            <div className={style.contactsTableItemField}>
                {phone_number.map(({ name }, index) => (
                    <span key={name}>
                        {name} {index === phone_number.length - 1 ? '' : ','}
                    </span>
                ))}
            </div>
            <div className={style.contactsTableItemField}>
                <span>{email}</span>
            </div>
        </div>
    );
};

export default ContactItem;
