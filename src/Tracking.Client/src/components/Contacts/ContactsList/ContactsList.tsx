import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import NullEntities from '@/components/Elements/NullEntities/NullEntities';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { contactApi, contactHooks } from '@/services/contact/contact';
import { GET_ALL_CONTACTS_KEY } from '@/store/queries/keys/keys';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/elements/loader_text.module.scss';
import ContactItem from './ContactItem';
import { contactsPaginationHandler } from './ContactsPagination/ContactsPagination';
import ContactsTableLayout from './ContactsTableLayout/ContactsTableLayout';

export const contactCheckHandler = new CheckEntitiesHandler();

const ContactsList: React.FC = () => {
    const { data, refetch } = contactHooks.useList(contactsPaginationHandler, {
        refetchOnMount: true,
    });

    useSetCheckableEntities(contactCheckHandler, data?.data);

    return (
        <>
            {data ? (
                <>
                    {data.data.length ? (
                        <>
                            <ContactsTableLayout>
                                {data.data.map((contact) => (
                                    <ContactItem
                                        key={contact.id}
                                        {...contact}
                                    />
                                ))}
                            </ContactsTableLayout>
                        </>
                    ) : (
                        <NullEntities text='Нет контактов' />
                    )}
                </>
            ) : (
                <LoaderCircle size={100} className={style.loaderPlaceFull} />
            )}
            <EntitySelectedActions
                keys={[GET_ALL_CONTACTS_KEY]}
                checkHandler={contactCheckHandler}
                service={contactApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default observer(ContactsList);
