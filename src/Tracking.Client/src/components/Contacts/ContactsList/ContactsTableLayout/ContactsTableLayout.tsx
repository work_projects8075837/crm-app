import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import React from 'react';
import style from '../../../../style/layout/contacts.module.scss';
import { contactCheckHandler } from '../ContactsList';
import ContactsPagination from '../ContactsPagination/ContactsPagination';

interface ContactsTableLayoutProps {
    children: React.ReactNode;
}

const contactsTableFields = [
    { name: 'ФИО' },
    { name: 'Номер телефона' },
    { name: 'Электронная почта' },
];

const ContactsTableLayout: React.FC<ContactsTableLayoutProps> = ({
    children,
}) => {
    return (
        <>
            <TableLayout
                layoutClass={style.contactsTable}
                header={
                    <TableHeader
                        checkHandler={contactCheckHandler}
                        fields={contactsTableFields}
                        layoutClass={style.rowLayout}
                    />
                }
                pagination={<ContactsPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default ContactsTableLayout;
