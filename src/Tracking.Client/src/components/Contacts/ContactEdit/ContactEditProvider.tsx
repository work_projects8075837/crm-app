import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import UpdateContactForm from '@/components/Form/UpdateContactForm/UpdateContactForm';
import { useContactByParams } from '@/store/queries/stores/contact/useContactByParams';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';

interface ContactEditProviderProps {
    children: React.ReactNode;
    openHandler: OpenHandler;
}

const ContactEditProvider: React.FC<ContactEditProviderProps> = ({
    children,
    openHandler,
}) => {
    const { data } = useContactByParams();
    return (
        <>
            {data ? (
                <UpdateContactForm
                    defaultValues={data}
                    openHandler={openHandler}
                >
                    {children}
                </UpdateContactForm>
            ) : (
                <LoaderCircle />
            )}
        </>
    );
};

export default ContactEditProvider;
