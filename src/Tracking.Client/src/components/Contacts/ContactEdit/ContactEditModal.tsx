import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import ContactFields from '@/components/Form/CreateOnlyContactForm/ContactFields';
import { OpenHandler } from '@/store/states/OpenHandler';
import React, { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import ContactSalePoints from '../ContactSalePoints/ContactSalePoints';
import ContactEditProvider from './ContactEditProvider';

export const contactsEditOpen = new OpenHandler(false);

const ContactEditModal: React.FC = () => {
    const navigate = useNavigate();
    const { contactId } = useParams();
    useEffect(() => {
        if (contactId) contactsEditOpen.open();
    }, [contactId]);
    return (
        <>
            <ModalWindow
                onClose={() => navigate('/contact/')}
                title='Редатировать контакт'
                openHandler={contactsEditOpen}
            >
                <ContactEditProvider openHandler={contactsEditOpen}>
                    <ContactFields />
                    <ContactSalePoints />
                </ContactEditProvider>
            </ModalWindow>
        </>
    );
};

export default ContactEditModal;
