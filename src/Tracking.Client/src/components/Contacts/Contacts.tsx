import React from 'react';
import Layout from '../NavbarLayout/Layout';
import ContactEditModal from './ContactEdit/ContactEditModal';
import ContactsCreateModal from './ContactsCreateModal/ContactsCreateModal';
import ContactsHeader from './ContactsHeader/ContactsHeader';
import ContactsList from './ContactsList/ContactsList';

const Contacts: React.FC = () => {
    return (
        <>
            <Layout>
                <ContactsHeader />
                <ContactsList />
                <ContactsCreateModal />
                <ContactEditModal />
            </Layout>
        </>
    );
};

export default Contacts;
