import SelectedEntityCardLayout from '@/components/Fields/SelectedEntityCard/SelectedEntityCardLayout';
import { IPosition } from '@/services/contact_position/types';
import { useContactByParams } from '@/store/queries/stores/contact/useContactByParams';
import React from 'react';

const ContactSalePoints: React.FC = () => {
    const { data } = useContactByParams();
    const uniquePositions =
        data &&
        [...new Set(data.position.map((p) => p.sale_point.id))].map(
            (sale_point_id) =>
                data.position.find(
                    (p) => p.sale_point.id === sale_point_id,
                ) as IPosition,
        );
    return (
        <>
            {uniquePositions?.map(({ id, sale_point }) => (
                <SelectedEntityCardLayout
                    key={id}
                    id={sale_point.id}
                    title='Заведение'
                    valueName={sale_point.name}
                    linkName='sale_point'
                ></SelectedEntityCardLayout>
            ))}
        </>
    );
};

export default ContactSalePoints;
