import BlueButton from '@/components/Fields/Buttons/BlueButton';
import HeaderLayout from '@/components/Layouts/HeaderLayout/HeaderLayout';
import React from 'react';
import style from '../../../style/elements/blue_button.module.scss';
import headerStyle from '../../../style/layout/header.module.scss';
import { contactsCreateModalHandler } from '../ContactsCreateModal/ContactsCreateModal';

const ContactsHeader: React.FC = () => {
    return (
        <>
            <HeaderLayout title='Список контактов'>
                <div className={headerStyle.headerChildrenContainerEnd}>
                    <div></div>
                    <div className={style.blockStandardWidth}>
                        <BlueButton
                            onClick={() => contactsCreateModalHandler.open()}
                        >
                            Новый
                        </BlueButton>
                    </div>
                </div>
            </HeaderLayout>
        </>
    );
};

export default ContactsHeader;
