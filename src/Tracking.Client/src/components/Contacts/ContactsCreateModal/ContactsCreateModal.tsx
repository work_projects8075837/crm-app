import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import ContactFields from '@/components/Form/CreateOnlyContactForm/ContactFields';
import CreateOnlyContactForm from '@/components/Form/CreateOnlyContactForm/CreateOnlyContactForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import ContactsCreatePlainButton from './ContactsCreatePlainButton/ContactsCreatePlainButton';

export const contactsCreateModalHandler = new OpenHandler(false);

const ContactsCreateModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                title='Новый контакт'
                openHandler={contactsCreateModalHandler}
            >
                <CreateOnlyContactForm openHandler={contactsCreateModalHandler}>
                    <ContactFields />

                    <ContactsCreatePlainButton />
                </CreateOnlyContactForm>
            </ModalWindow>
        </>
    );
};

export default ContactsCreateModal;
