import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import React from 'react';

const ContactsCreatePlainButton: React.FC = () => {
    const { onSubmit, isDisabled } = useSubmit();
    return (
        <>
            <SaveButton isLoading={isDisabled}>Создать контакт</SaveButton>
        </>
    );
};

export default ContactsCreatePlainButton;
