import React from 'react';
import styles from '@/style/layout/auth.module.scss';
import Svg from '@/components/Elements/Svg/Svg';

interface AuthLayoutProps {
    children: React.ReactNode;
    title: string;
    subTitle: string;
    afterChildren?: React.ReactNode;
}

const AuthLayout: React.FC<AuthLayoutProps> = ({
    children,
    title,
    subTitle,
    afterChildren,
}) => {
    return (
        <>
            <div className={styles.window}>
                <div className={styles.cover}>
                    <Svg symbol='white_logo' className={styles.logo} />

                    <span className={styles.copyright}>
                        © ООО «ФЛАГМАН» 2023
                    </span>
                </div>

                <div className={styles.content}>
                    <div className='container'>
                        <div className='welcome--auth'>{title}</div>
                        <div className='welcome--auth--text'>{subTitle}</div>
                        {children}
                        <div className='welcome--auth--singup'>
                            {afterChildren}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default AuthLayout;
