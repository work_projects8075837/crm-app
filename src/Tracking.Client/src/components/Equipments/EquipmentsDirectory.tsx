import React from 'react';
import Layout from '../NavbarLayout/Layout';
import SalePointInventoryList from '../SalePoints/SalePointEditInventory/SalePointInventoryList/SalePointInventoryList';
import InventoryDirectoryProvider from './../SalePoints/SalePointEditInventory/InventoryDirectory/InventoryDirectoryProvider';
import EquipmentsDirectoryHeader from './EquipmentsDirectoryHeader';

const EquipmentsDirectory: React.FC = () => {
    return (
        <>
            <Layout>
                <InventoryDirectoryProvider>
                    <EquipmentsDirectoryHeader />
                    <SalePointInventoryList emptyText='Нет инвентаря в текущей папке' />
                </InventoryDirectoryProvider>
            </Layout>
        </>
    );
};

export default EquipmentsDirectory;
