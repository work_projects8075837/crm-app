import { useInventoryBreadCrumbs } from '@/store/queries/stores/inventory/useInventoryBreadCrumbs';
import React from 'react';
import { Link } from 'react-router-dom';
import EquipmentLink from '../Elements/BreadCrumbs/EquipmentLink';
import InnerBreadCrumbs from '../Elements/BreadCrumbs/InnerBreadCrumbs';
import BaseDirectoryHeader from '../SalePoints/SalePointsHeader/BaseDirectoryHeader';

const EquipmentsDirectoryHeader: React.FC = () => {
    const { data, current } = useInventoryBreadCrumbs();
    return (
        <>
            <BaseDirectoryHeader
                title={
                    <Link to={`/inventory/single/${current?.id}`}>
                        {current?.name}
                    </Link>
                }
                breadcrumbs={
                    <>
                        <EquipmentLink />{' '}
                        <InnerBreadCrumbs links={data} route='inventory' />
                    </>
                }
                link='/inventory/create/'
                directoryText='Новая папка оборудования'
                entityText='Новое оборудование'
            />
        </>
    );
};

export default EquipmentsDirectoryHeader;
