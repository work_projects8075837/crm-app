import React from 'react';
import Layout from '../NavbarLayout/Layout';
import SalePointInventoryList from '../SalePoints/SalePointEditInventory/SalePointInventoryList/SalePointInventoryList';
import BaseDirectoryHeader from '../SalePoints/SalePointsHeader/BaseDirectoryHeader';

const Equipments: React.FC = () => {
    return (
        <>
            <Layout>
                <BaseDirectoryHeader
                    title='Список оборудования'
                    link='/inventory/create/'
                    directoryText='Новая папка оборудования'
                    entityText='Новое оборудование'
                />
                <SalePointInventoryList emptyText='Список инвентаря пуст' />
            </Layout>
        </>
    );
};

export default Equipments;
