import React from 'react';
import style from '../../style/layout/side_menu.module.scss';

interface MenuSubHeaderLayoutProps {
    children?: React.ReactNode;
}

const MenuSubHeaderLayout: React.FC<MenuSubHeaderLayoutProps> = ({
    children,
}) => {
    return (
        <>
            <div className={style.subHeader}>{children}</div>
        </>
    );
};

export default MenuSubHeaderLayout;
