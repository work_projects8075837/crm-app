import Svg from '@/components/Elements/Svg/Svg';
import React, { useLayoutEffect } from 'react';
import styles from '../../style/layout/side_menu.module.scss';

interface SideMenuLayoutProps {
    children?: React.ReactNode;
    header?: React.ReactNode;
    close: () => void;
    isOpen: boolean;
    className?: string;
}

const SideMenuLayout: React.ForwardRefRenderFunction<
    HTMLDivElement,
    SideMenuLayoutProps
> = ({ children, header, isOpen, close, className = '' }, ref) => {
    useLayoutEffect(() => {
        return () => close();
    }, []);
    return (
        <>
            <div className={`${styles.layout} ${className}`} ref={ref}>
                <div className={styles.layoutHeader}>
                    <div className={styles.headerContent}>{header}</div>
                    <button onClick={close} className={styles.close}>
                        <Svg symbol='cross' className={styles.closeSvg} />
                    </button>
                </div>
                {children}
            </div>

            {isOpen && <div onClick={close} className={styles.backDark} />}
        </>
    );
};

export default React.forwardRef(SideMenuLayout);
