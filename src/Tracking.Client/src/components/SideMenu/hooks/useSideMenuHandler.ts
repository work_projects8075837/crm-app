import { createRef, useCallback, useState } from 'react';

const ref = createRef<HTMLDivElement>();

export const useSideMenuHandler = () => {
    const [isOpen, setOpen] = useState<boolean>(false);
    // const ref = useRef<HTMLButtonElement>();

    const open = useCallback(() => {
        if (ref.current) {
            ref.current.style.transform = 'translateX(0)';
            setOpen(true);
        }
    }, [ref.current]);

    const close = useCallback(() => {
        if (ref.current) {
            ref.current.style.transform = 'translateX(-100%)';
            setOpen(false);
        }
    }, [ref.current]);

    return { isOpen, ref, open, close };
};
