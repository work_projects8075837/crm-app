import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import styles from '../../../style/layout/side_menu.module.scss';

const CloseButton: React.FC = () => {
    return (
        <>
            <button className={styles.close}>
                <Svg symbol='cross' className={styles.closeSvg} />
            </button>
        </>
    );
};

export default React.memo(CloseButton);
