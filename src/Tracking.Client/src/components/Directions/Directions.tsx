import React from 'react';
import Layout from '../NavbarLayout/Layout';
import DirectionsHeader from './DirectionsHeader/DirectionsHeader';
import DirectionsList from './DirectionsList/DirectionsList';

const Directions: React.FC = () => {
    return (
        <>
            <Layout>
                <DirectionsHeader />
                <DirectionsList />
            </Layout>
        </>
    );
};

export default Directions;
