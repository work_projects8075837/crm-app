import { DIRECTION_STATUSES_NAME } from '@/components/Fields/EntitiesFields/direction';
import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import StatusItem from '@/components/Tickets/StatusItem/StatusItem';
import { IStatus } from '@/services/status/types';
import React from 'react';
import style from '../../../style/layout/direction.module.scss';
import { selectStatusModal } from '../SelectStatusModal/SelectStatusModal';
import AddStatusButton from './AddStatusButton';

const StatusesFormList: React.FC = () => {
    const { entities, remove } = useFormEntityValueArray<IStatus>(
        DIRECTION_STATUSES_NAME,
    );
    return (
        <>
            <div className={style.statuses}>
                {entities?.map((status) => (
                    <div
                        key={status.id}
                        className={`${style.status} removableWrapper`}
                    >
                        <StatusItem {...status} />
                        <CloseCrossButton
                            className={`${style.statusRemove} removeBtn`}
                            onClick={() => {
                                remove(status.id);
                            }}
                        />
                    </div>
                ))}
                <AddStatusButton openHandler={selectStatusModal} />
            </div>
        </>
    );
};

export default StatusesFormList;
