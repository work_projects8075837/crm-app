import { statusApi } from '@/services/status/status';
import { useMutation } from '@tanstack/react-query';

export const useDeleteStatusMutation = () => {
    const mutation = useMutation({
        mutationFn: async (statusId: string) => {
            return await statusApi.delete(statusId);
        },
    });

    return mutation;
};
