import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/direction.module.scss';
import Svg from '@/components/Elements/Svg/Svg';

interface AddStatusButtonProps {
    openHandler: OpenHandler;
}

const AddStatusButton: React.FC<AddStatusButtonProps> = ({ openHandler }) => {
    return (
        <>
            <button
                className={style.addStatus}
                onClick={() => openHandler.open()}
            >
                <Svg symbol='plus' />
            </button>
        </>
    );
};

export default AddStatusButton;
