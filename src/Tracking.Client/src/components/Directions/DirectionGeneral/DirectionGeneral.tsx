import FormSelect from '@/components/Elements/FormSelect/FormSelect';
import { DIRECTION_NAME_NAME } from '@/components/Fields/EntitiesFields/direction';
import FormShortInput from '@/components/Fields/FormShortInput/FormShortInput';
import EntityFieldLayout from '@/components/Fields/Layouts/EntityFieldLayout';
import DoubleColumnsLayout from '@/components/Layouts/DoubleColumnsLayout/DoubleColumnsLayout';
import React from 'react';
import StatusesFormList from '../StatusesFormList/StatusesFormList';

// interface DirectionGeneralProps {

// }

const DirectionGeneral: React.FC = () => {
    return (
        <>
            <DoubleColumnsLayout
                left={
                    <>
                        <FormSelect title='Основное'>
                            <EntityFieldLayout title='Название'>
                                <FormShortInput
                                    name={DIRECTION_NAME_NAME}
                                    placeholder='Название'
                                />
                            </EntityFieldLayout>
                        </FormSelect>

                        <FormSelect title='Статусы'>
                            <StatusesFormList />
                        </FormSelect>
                    </>
                }
            />
        </>
    );
};

export default DirectionGeneral;
