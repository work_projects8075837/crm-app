import Pagination from '@/components/Tables/Pagination/Pagination';
import PaginationButtonsList from '@/components/Tickets/TiketsPaginationButtonsList/PaginationButtonsList';
import { directionHooks } from '@/services/direction/direction';
import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';

const DIRECTIONS_PAGINATION = 'DIRECTIONS_PAGINATION';
export const directionsPaginationHandler = new PaginationHandler(
    DIRECTIONS_PAGINATION,
);

const DirectionsPagination: React.FC = () => {
    const { data } = directionHooks.useList(directionsPaginationHandler);
    return (
        <>
            <Pagination
                paginationHandler={directionsPaginationHandler}
                pages={
                    <PaginationButtonsList
                        paginationHandler={directionsPaginationHandler}
                        data={data}
                    />
                }
            />
        </>
    );
};

export default observer(DirectionsPagination);
