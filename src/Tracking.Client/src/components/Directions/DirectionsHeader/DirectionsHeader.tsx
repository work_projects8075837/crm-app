import BaseEntityHeader from '@/components/SalePoints/SalePointsHeader/BaseEntityHeader';
import React from 'react';

const DirectionsHeader: React.FC = () => {
    return (
        <>
            <BaseEntityHeader
                title='Список направлений'
                link='/direction/create'
            />
        </>
    );
};

export default DirectionsHeader;
