import DirectionsLink from '@/components/Elements/BreadCrumbs/DirectionsLink';
import OneEntityBaseHeader from '@/components/SalePoints/SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';
import React from 'react';

const DirectionCreateHeader: React.FC = () => {
    return (
        <>
            <OneEntityBaseHeader
                title='Новое направление'
                breadcrumbs={
                    <>
                        <DirectionsLink /> / Новое направление
                    </>
                }
            />
        </>
    );
};

export default DirectionCreateHeader;
