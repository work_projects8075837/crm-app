import React from 'react';
import DirectionCreateHeader from './DirectionCreateHeader';
import Layout from '@/components/NavbarLayout/Layout';
import style from '../../../style/layout/direction.module.scss';
import DirectionCreateForm from '@/components/Form/DirectionCreateForm/DirectionCreateForm';
import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import DirectionGeneral from '../DirectionGeneral/DirectionGeneral';
import AddStatusModal from '../AddStatusModal/AddStatusModal';
import SelectStatusModal from '../SelectStatusModal/SelectStatusModal';

const DirectionCreate: React.FC = () => {
    return (
        <>
            <Layout childrenClass={style.formLayout}>
                <DirectionCreateForm
                    modals={
                        <>
                            <SelectStatusModal />
                            <AddStatusModal />
                        </>
                    }
                >
                    <DirectionCreateHeader />
                    <div className={style.formTabsButtons}>
                        <CircleButton isActive={true} text='Общая информация' />
                    </div>
                    <div className={style.afterTabsMargin}>
                        <DirectionGeneral />
                    </div>
                </DirectionCreateForm>
            </Layout>
        </>
    );
};

export default DirectionCreate;
