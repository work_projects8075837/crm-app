import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import NullEntities from '@/components/Elements/NullEntities/NullEntities';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { directionApi, directionHooks } from '@/services/direction/direction';
import {
    GET_ALL_DIRECTIONS_KEY,
    GET_SEARCH_DIRECTIONS_KEY,
} from '@/store/queries/keys/keys';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/elements/loader_text.module.scss';
import { directionsPaginationHandler } from '../DirectionsPagination/DirectionsPagination';
import DirectionItem from './DirectionItem';
import DirectionsTableLayout from './DirectionsTableLayout';

export const directionCheckHandler = new CheckEntitiesHandler();

const DirectionsList: React.FC = () => {
    const { data, refetch } = directionHooks.useList(
        directionsPaginationHandler,
        {
            refetchOnMount: true,
        },
    );

    useSetCheckableEntities(directionCheckHandler, data?.data);

    return (
        <>
            {data ? (
                <>
                    {data.data.length ? (
                        <>
                            <DirectionsTableLayout>
                                {data.data.map((direction) => (
                                    <DirectionItem
                                        key={direction.id}
                                        {...direction}
                                    />
                                ))}
                            </DirectionsTableLayout>
                        </>
                    ) : (
                        <NullEntities text='Нет направлнний' />
                    )}
                </>
            ) : (
                <LoaderCircle size={100} className={style.loaderPlaceFull} />
            )}
            <EntitySelectedActions
                keys={[GET_ALL_DIRECTIONS_KEY, GET_SEARCH_DIRECTIONS_KEY]}
                checkHandler={directionCheckHandler}
                service={directionApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default observer(DirectionsList);
