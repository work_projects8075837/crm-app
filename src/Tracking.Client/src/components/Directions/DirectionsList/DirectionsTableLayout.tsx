import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import React from 'react';
import style from '../../../style/layout/direction.module.scss';
import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import DirectionsPagination from '../DirectionsPagination/DirectionsPagination';
import { directionCheckHandler } from './DirectionsList';

interface DirectionsTableLayoutProps {
    children: React.ReactNode;
}

const fields = [{ name: 'Название' }];

const DirectionsTableLayout: React.FC<DirectionsTableLayoutProps> = ({
    children,
}) => {
    return (
        <>
            <TableLayout
                layoutClass={style.entityPaginatedTable}
                header={
                    <TableHeader
                        checkHandler={directionCheckHandler}
                        fields={fields}
                        layoutClass={style.header}
                        fieldClass={style.headerField}
                    />
                }
                pagination={<DirectionsPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default DirectionsTableLayout;
