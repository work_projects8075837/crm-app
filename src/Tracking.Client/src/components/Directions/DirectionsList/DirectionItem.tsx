import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/direction.module.scss';
import { directionCheckHandler } from './DirectionsList';

interface DirectionItemProps {
    id: string;
    name: string;
}

const DirectionItem: React.FC<DirectionItemProps> = ({ name, id }) => {
    const navigate = useNavigate();
    return (
        <div
            className={style.tableItem}
            onDoubleClick={() => {
                navigate(`/direction/single/${id}/`);
            }}
        >
            <CheckItem
                checkHandler={directionCheckHandler}
                entity={{ id, name }}
            />
            <span className={style.tableItemName}>
                <span>{name}</span>
            </span>
        </div>
    );
};

export default DirectionItem;
