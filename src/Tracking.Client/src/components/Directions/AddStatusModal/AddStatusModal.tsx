import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import AddStatusForm from '@/components/Form/AddStatusForm/AddStatusForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';

export const addStatusModalOpen = new OpenHandler(false);

const AddStatusModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                title='Добавить статус'
                openHandler={addStatusModalOpen}
            >
                <AddStatusForm openHandler={addStatusModalOpen} />
            </ModalWindow>
        </>
    );
};

export default AddStatusModal;
