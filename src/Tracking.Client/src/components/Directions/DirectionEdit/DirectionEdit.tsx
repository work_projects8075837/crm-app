import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import ModalMenu from '@/components/Elements/ModalMenu/ModalMenu';
import ActionsMenuDeleteButton from '@/components/Fields/ActionsMenuDeleteButton/ActionsMenuDeleteButton';
import EntityOptionsLayout from '@/components/Tickets/TicketOptions/EntityOptionsLayout';
import { directionApi } from '@/services/direction/direction';
import { GET_ALL_DIRECTIONS_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import style from '../../../style/layout/direction.module.scss';
import DirectionGeneral from '../DirectionGeneral/DirectionGeneral';
import DirectionEditFormProvider from '../Providers/DirectionEditFormProvider';
import DirectionEditHeader from './DirectionEditHeader';

const DirectionEdit: React.FC = () => {
    return (
        <>
            <DirectionEditFormProvider>
                <DirectionEditHeader />
                <EntityOptionsLayout>
                    <ModalMenu symbol='ticket_edit' text='Действия'>
                        <ActionsMenuDeleteButton
                            queryKey={GET_ALL_DIRECTIONS_KEY}
                            service={directionApi}
                            idName='directionId'
                            navigateTo='/direction/'
                        />
                    </ModalMenu>
                </EntityOptionsLayout>
                <div className={style.formTabsButtons}>
                    <CircleButton isActive={true} text='Общая информация' />
                </div>
                <div className={style.afterTabsMargin}>
                    <DirectionGeneral />
                </div>
            </DirectionEditFormProvider>
        </>
    );
};

export default DirectionEdit;
