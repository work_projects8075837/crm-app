import DirectionsLink from '@/components/Elements/BreadCrumbs/DirectionsLink';
import OneEntityBaseHeader from '@/components/SalePoints/SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';
import { useDirectionByParams } from '@/store/queries/stores/direction/useDirectionByParams';
import React from 'react';

const DirectionEditHeader: React.FC = () => {
    const { data } = useDirectionByParams();
    return (
        <>
            <OneEntityBaseHeader
                title={data?.name}
                breadcrumbs={
                    <>
                        <DirectionsLink /> / {data?.name}
                    </>
                }
            />
        </>
    );
};

export default DirectionEditHeader;
