import Layout from '@/components/NavbarLayout/Layout';
import { useDirectionByParams } from '@/store/queries/stores/direction/useDirectionByParams';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import UpdateDirectionForm from '@/components/Form/UpdateDirectionForm/UpdateDirectionForm';
import Loader from '@/Loader/Loader';
import SelectStatusModal from '../SelectStatusModal/SelectStatusModal';
import AddStatusModal from '../AddStatusModal/AddStatusModal';
interface DirectionEditFormProviderProps {
    children: React.ReactNode;
}

const DirectionEditFormProvider: React.FC<DirectionEditFormProviderProps> = ({
    children,
}) => {
    const { data } = useDirectionByParams({ refetchOnMount: true });
    return (
        <>
            {data ? (
                <Layout childrenClass={style.formLayout}>
                    <UpdateDirectionForm
                        defaultValues={data}
                        modals={
                            <>
                                <SelectStatusModal />
                                <AddStatusModal />
                            </>
                        }
                    >
                        {children}
                    </UpdateDirectionForm>
                </Layout>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default DirectionEditFormProvider;
