import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { DIRECTION_STATUSES_NAME } from '@/components/Fields/EntitiesFields/direction';
import SearchSelectStatus from '@/components/Fields/SearchSelectStatus/SearchSelectStatus';
import SelectEntityForm from '@/components/Form/SelectEntityForm/SelectEntityForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';

export const selectStatusModal = new OpenHandler(false);

const SelectStatusModal: React.FC = () => {
    return (
        <>
            <ModalWindow title='Выбрать статус' openHandler={selectStatusModal}>
                <SelectEntityForm
                    openHandler={selectStatusModal}
                    name={DIRECTION_STATUSES_NAME}
                >
                    <SearchSelectStatus openHandler={selectStatusModal} />
                    <SaveButton>Сохранить</SaveButton>
                </SelectEntityForm>
            </ModalWindow>
        </>
    );
};

export default SelectStatusModal;
