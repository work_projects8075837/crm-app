import FilterLayout from '@/components/Filter/FilterLayout/FilterLayout';
import FilterTable from '@/components/Filter/FilterTable/FilterTable';
import { ticketFilterOpen, ticketFilterOpener } from '@/pages/task/Tickets';
import React from 'react';
import { ticketFilter } from './filter';

const TicketFilter: React.FC = () => {
    return (
        <>
            <FilterLayout
                openHandler={ticketFilterOpen}
                filterOpen={ticketFilterOpener}
            >
                <FilterTable filterHandler={ticketFilter} />
            </FilterLayout>
        </>
    );
};

export default TicketFilter;
