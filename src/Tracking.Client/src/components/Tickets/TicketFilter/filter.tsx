import DateDuration from '@/components/Filter/Fields/DateDuration';
import SelectCharacterProblemIds from '@/components/Filter/Fields/Selects/SelectCharacterProblemIds';
import SelectSalePointId from '@/components/Filter/Fields/Selects/SelectSalePointId';
import StringField from '@/components/Filter/Fields/StringField';
import { ITicketFilter } from '@/services/ticket/types';
import { FilterHandler } from '@/store/states/FilterHandler';

export const ticketFilter = new FilterHandler<keyof ITicketFilter>(
    [
        {
            key: 'ticket_number',
            name: 'Номер заявки',
            InputElem: StringField,
        },
        {
            key: 'channel_name',
            name: 'Канал связи',
            InputElem: StringField,
        },
        {
            key: 'character_problem_id',
            name: 'Выбрать характеры проблемы',
            InputElem: SelectCharacterProblemIds,
        },
        {
            key: 'character_problem_name',
            name: 'Характер проблемы',
            InputElem: StringField,
        },

        {
            key: 'contact_name',
            name: 'Контакт',
            InputElem: StringField,
        },
        {
            key: 'contact_email',
            name: 'Email контакта',
            InputElem: StringField,
        },

        {
            key: 'phone_number_name',
            name: 'Номер телефона',
            InputElem: StringField,
        },

        {
            key: 'direction_name',
            name: 'Направление',
            InputElem: StringField,
        },

        {
            key: 'sale_point_id',
            name: 'Выбрать заведение',
            InputElem: SelectSalePointId,
        },
        {
            key: 'sale_point_name',
            name: 'Заведение',
            InputElem: StringField,
        },
        {
            key: 'sale_point_email',
            name: 'Email заведения',
            InputElem: StringField,
        },
        {
            key: 'sale_point_phone',
            name: 'Телефон заведения',
            InputElem: StringField,
        },
        {
            key: 'sale_point_address',
            name: 'Адрес заведения',
            InputElem: StringField,
        },

        {
            key: 'status_name',
            name: 'Статус',
            InputElem: StringField,
        },

        {
            key: 'ticket_created_at_from-ticket_created_at_to',
            name: 'Дата создания',
            InputElem: DateDuration,
        },
    ],
    [
        'ticket_number',
        'sale_point_name',
        'character_problem_name',
        'channel_name',
        'contact_name',
        'direction_name',
        'status_name',
        'phone_number_name',
    ],
    'TICKET_FILTER',
);
