import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import {
    formatDate,
    formatDateWithoutTime,
} from '@/components/utils/formatDate';

type DT<T> = readonly [string | undefined, T[]];

export const sortItemsByDate = <
    // T extends { [key: string]: string | number | null | undefined | Entity },
    T extends Entity,
>(
    items: T[],
    dateKey: keyof T,
): DT<T & { date?: string; date_n: number; time: string }>[] => {
    const itemsWithDates = items
        .filter((i) => !!i[dateKey])
        .map((item) => {
            const dateValue = item[dateKey];
            if (!dateValue || typeof dateValue !== 'string') {
                throw Error('Invalid date Key');
            }

            const localDate = new Date(dateValue).toLocaleDateString('en-US');
            return {
                ...item,
                date: formatDateWithoutTime(dateValue),
                date_n: new Date(localDate).getTime(),
                time: formatDate(dateValue)?.split(' ')?.[1] || '',
            };
        });

    const dates = [...new Set(itemsWithDates?.map((c) => c.date_n))]
        .sort((d1, d2) => d2 - d1)
        .map((d) => formatDateWithoutTime(d));

    const dateItems = dates.map(
        (d) =>
            [
                d,
                itemsWithDates
                    ?.filter((c) => c.date === d)
                    .sort((c1, c2) => c2.date_n - c1.date_n),
            ] as const,
    );

    return dateItems;
};
