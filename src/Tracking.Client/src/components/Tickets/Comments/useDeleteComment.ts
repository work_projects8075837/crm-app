import { commentApi } from '@/services/comment/comment';
import { invalidateKey } from '@/store/queries/actions/base';
import { removeComment } from '@/store/queries/actions/comment';
import { GET_ALL_COMMENTS_KEY } from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useDeleteComment = () => {
    const { ticketId } = useParams();
    return useMutation({
        mutationFn: async (id: string) => {
            removeComment(id, ticketId || '');
            await commentApi.delete(id);
        },
        onSuccess: () => {
            invalidateKey(GET_ALL_COMMENTS_KEY);
        },
    });
};
