import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import NullEntities from '@/components/Elements/NullEntities/NullEntities';
import { commentHooks } from '@/services/comment/comment';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import React, { Fragment } from 'react';
import { useParams } from 'react-router-dom';
import style from '../../../style/notifications/message.module.scss';
import Comment from './Comment';
import { sortItemsByDate } from './sortItemsByDate';

const CommentsList: React.FC = () => {
    const userQ = useCurrentUser();
    const { ticketId } = useParams();
    const { data } = commentHooks.useList({ ticket_id: ticketId });

    const dateComments =
        data?.data && sortItemsByDate(data.data, 'date_update');

    return (
        <>
            {dateComments ? (
                <>
                    {dateComments.length ? (
                        dateComments.map(([date, comments], i) => {
                            return (
                                <Fragment key={i}>
                                    <div className={style.dayTime}>{date}</div>
                                    {comments?.map((c) => (
                                        <Comment
                                            key={c.id}
                                            {...c}
                                            isSelf={
                                                userQ.data?.id === c.user.id
                                            }
                                        />
                                    ))}
                                </Fragment>
                            );
                        })
                    ) : (
                        <NullEntities text='Вы можете добавить первый комментарий' />
                    )}
                </>
            ) : (
                <LoaderCircle size={100} cover={true} />
            )}
        </>
    );
};

export default CommentsList;
