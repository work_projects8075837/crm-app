import { viewImage } from '@/components/Elements/ImageViewModal/viewImage';
import Svg from '@/components/Elements/Svg/Svg';
import { updatableComment } from '@/components/Form/UpdateCommentForm/UpdateCommentProvider';
import { IComment } from '@/services/comment/types';
import React from 'react';
import style from '../../../style/layout/comment.module.scss';
import { useDeleteComment } from './useDeleteComment';

interface CommentProps extends IComment {
    isSelf: boolean;
    date?: string;
    time?: string;
}

const Comment: React.FC<CommentProps> = ({
    id,
    date_create,
    date_update,
    description,
    user,
    isSelf,
    time,
}) => {
    const { mutate } = useDeleteComment();
    return (
        <>
            <div
                className={style.wrapper}
                style={{ justifyContent: isSelf ? 'flex-end' : undefined }}
            >
                <div className={`${style.comment}`}>
                    {isSelf && (
                        <div className={style.commentButtons}>
                            <button
                                className={style.editButton}
                                onClick={async () => {
                                    updatableComment.setValue(null);

                                    mutate(id);
                                }}
                            >
                                <Svg symbol='delete_bag' />
                            </button>
                            <button
                                className={style.editButton}
                                onClick={() => {
                                    updatableComment.setValue({
                                        id,
                                        description,
                                    });
                                }}
                            >
                                <Svg symbol='ticket_edit' />
                            </button>
                        </div>
                    )}

                    <div className={style.commentHeader}>
                        <h2 className={style.selectedEntityCardMainTitle}>
                            {user.name}
                        </h2>
                        <span>{time}</span>
                    </div>

                    <div
                        onClick={async (event) => {
                            event.stopPropagation();
                            if (event.target instanceof HTMLImageElement) {
                                await viewImage(event.target.src);
                            }
                        }}
                        className={`${style.content} mt15`}
                        dangerouslySetInnerHTML={{ __html: description }}
                    ></div>
                </div>
            </div>
        </>
    );
};

export default Comment;
