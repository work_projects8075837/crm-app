import UpdateCommentProvider from '@/components/Form/UpdateCommentForm/UpdateCommentProvider';
import React from 'react';
import TicketsEditPageLayout from '../TicketEdit/TicketsEditPageLayout/TicketsEditPageLayout';
import CommentsList from './CommentsList';

const TicketEditComments: React.FC = () => {
    return (
        <>
            <TicketsEditPageLayout
                formBottom={
                    <>
                        <UpdateCommentProvider />
                    </>
                }
            >
                <CommentsList />
            </TicketsEditPageLayout>
        </>
    );
};

export default TicketEditComments;
