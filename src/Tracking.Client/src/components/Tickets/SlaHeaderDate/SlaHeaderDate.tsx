import React from 'react';
import { useFormCharacterProblem } from '../hooks/useFormCharacterProblem';
import TicketHeaderLabelLayout from '../TicketHeaderLabelLayout/TicketHeaderLabelLayout';
import { calculateSla } from '@/components/utils/calculateSla';
import Dash from '@/components/Elements/Dash/Dash';
import ValueDash from '@/components/Elements/ValueDash/ValueDash';

interface SlaHeaderDateProps {
    fromDate?: Date;
}

const SlaHeaderDate: React.FC<SlaHeaderDateProps> = ({
    fromDate = new Date(),
}) => {
    const characterProblem = useFormCharacterProblem();
    return (
        <>
            <TicketHeaderLabelLayout title='Планируемое завершение'>
                {characterProblem ? (
                    <ValueDash>
                        {calculateSla(characterProblem.sla, fromDate.getTime())}
                    </ValueDash>
                ) : (
                    <Dash />
                )}
            </TicketHeaderLabelLayout>
        </>
    );
};

export default SlaHeaderDate;
