import DurationText from '@/components/CharacterProblems/JobsList/DurationText';
import LinkTooltip from '@/components/SideBar/Links/LinkTooltip/LinkTooltip';
import { countHours } from '@/components/utils/countHours';
import { IJob } from '@/services/job/types';
import { IJobResult } from '@/services/result_job/types';
import React from 'react';
import style from '../../../style/layout/job_result.module.scss';
import { useChangeJobResultTime } from '../TicketJobs/TicketJobItem/hooks/useChangeJobResultTime';
import { useChangeResultsCount } from '../TicketJobs/TicketJobItem/hooks/useChangeResultsCount';

interface TicketJobResultProps {
    name?: string;
    count?: number;
    duration?: number;
    job?: IJob | null;
    jobResults?: IJobResult[];
}

const TicketJobResult: React.FC<TicketJobResultProps> = ({
    count,
    duration,
    name,
    job,
    jobResults,
}) => {
    const changeResultCount = useChangeResultsCount(
        job,
        jobResults?.[0].manager?.id,
    );

    const changeJobResultTime = useChangeJobResultTime(jobResults?.[0]);

    return (
        <div className={style.tableItem}>
            <div className={style.tableItemField}>
                <span>{name}</span>
            </div>
            <div className={style.tableItemField}>
                {duration ? (
                    changeResultCount.isLoading ? (
                        <span
                            className='loader'
                            style={{
                                width: 20,
                                height: 20,
                                borderWidth: 2,
                            }}
                        />
                    ) : (
                        <button
                            disabled={changeResultCount.isLoading}
                            className={style.jobResultUserCount}
                            data-tooltip-id={`link-tooltip_${'Изменить количество'}`}
                            data-tooltip-content={'Изменить количество'}
                            onClick={() => {
                                if (name) changeResultCount.change(name);
                            }}
                        >
                            {count}

                            <LinkTooltip alt={'Изменить количество'} />
                        </button>
                    )
                ) : (
                    <span>{count}</span>
                )}
            </div>
            <div className={style.tableItemField}>
                {job?.duration ? (
                    <span>
                        <DurationText {...countHours(duration)} />
                    </span>
                ) : (
                    <>
                        {changeJobResultTime.isLoading ? (
                            <span
                                className='loader'
                                style={{
                                    width: 20,
                                    height: 20,
                                    borderWidth: 2,
                                }}
                            />
                        ) : (
                            <button
                                disabled={changeJobResultTime.isLoading}
                                className={style.jobResultUserCount}
                                data-tooltip-id={`link-tooltip_${'Изменить время'}`}
                                data-tooltip-content={'Изменить время'}
                                onClick={() => {
                                    changeJobResultTime.change();
                                }}
                            >
                                <DurationText {...countHours(duration)} />
                                <LinkTooltip alt={'Изменить время'} />
                            </button>
                        )}
                    </>
                )}
            </div>
        </div>
    );
};

export default TicketJobResult;
