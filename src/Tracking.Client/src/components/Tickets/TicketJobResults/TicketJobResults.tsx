import React from 'react';
import TicketsEditPageLayout from '../TicketEdit/TicketsEditPageLayout/TicketsEditPageLayout';
import TicketJobResultsList from './TicketJobResultsList';

const TicketJobResults: React.FC = () => {
    return (
        <>
            <TicketsEditPageLayout>
                <TicketJobResultsList />
            </TicketsEditPageLayout>
        </>
    );
};

export default TicketJobResults;
