import EmptyPagination from '@/components/Tables/EmptyPagination/EmptyPagination';
import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import React from 'react';
import style from '../../../style/layout/job_result.module.scss';

interface TicketJobResultsTableLayoutProps {
    children: React.ReactNode;
}

const TicketJobResultsTableLayout: React.FC<
    TicketJobResultsTableLayoutProps
> = ({ children }) => {
    return (
        <>
            <TableLayout
                header={
                    <TableHeader
                        fields={[
                            { name: 'Название работы' },
                            { name: 'Сделано работ' },
                            { name: 'Общее время' },
                        ]}
                        layoutClass={style.rowLayout}
                    />
                }
                layoutClass={style.entityPaginatedTable}
                pagination={<EmptyPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default TicketJobResultsTableLayout;
