import TextLoader from '@/components/Elements/TextLoader/TextLoader';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import React, { Fragment } from 'react';
import style from '../../../style/layout/job_result.module.scss';
import TicketJobResult from './TicketJobResult';
import TicketJobResultsTableLayout from './TicketJobResultsTableLayout';

const TicketJobResultsList: React.FC = () => {
    const { data } = useTicketByParams();

    const existedManagers = [
        ...new Set(
            data?.job_result
                .map((jr) => jr.manager?.id)
                .filter((managerId) => managerId),
        ),
    ].map(
        (id) =>
            data?.job_result.map((jb) => jb.manager).find((m) => m?.id === id),
    );

    const managersResults = existedManagers.map((manager) => {
        const managerJobResults = data?.job_result.filter(
            (jr) => jr.manager?.id === manager?.id && jr.job !== null,
        );

        const existedJobsId = [
            ...new Set(managerJobResults?.map((jr) => jr.job?.id)),
        ].map((id) => managerJobResults?.find((jr) => jr.job?.id === id)?.job);

        return [manager, existedJobsId] as const;
    });

    return (
        <>
            {managersResults.length ? (
                managersResults.map(([manager, jobs]) => {
                    return (
                        <Fragment key={manager?.id}>
                            <h3 className={style.title}>
                                {manager?.user.name}
                            </h3>

                            <TicketJobResultsTableLayout>
                                {jobs?.map((job) => {
                                    const currentJobResults =
                                        data?.job_result.filter(
                                            (jr) =>
                                                jr.job?.id === job?.id &&
                                                jr.manager &&
                                                jr.manager.id === manager?.id,
                                        );
                                    return (
                                        <TicketJobResult
                                            key={job?.id}
                                            job={job}
                                            name={job?.name}
                                            jobResults={currentJobResults}
                                            count={currentJobResults?.length}
                                            duration={currentJobResults?.reduce(
                                                (sum, jr) => (sum += jr.time),
                                                0,
                                            )}
                                        />
                                    );
                                })}
                            </TicketJobResultsTableLayout>
                        </Fragment>
                    );
                })
            ) : (
                <TextLoader text='По этой заявке ещё не было выполнено работ' />
            )}
        </>
    );
};

export default TicketJobResultsList;
