import { TICKET_FILES_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { formatDate } from '@/components/utils/formatDate';
import { IFile } from '@/services/file/types';
import React from 'react';
import style from '../../../../style/layout/modal_table.module.scss';

type FileTableItemProps = IFile;

const FileTableItem: React.FC<FileTableItemProps> = ({
    id,
    name,
    path,
    size,
    user,
    created_at,
}) => {
    const { remove } = useFormEntityValueArray(TICKET_FILES_NAME);
    const onRemove = () => {
        remove(id);
    };
    return (
        <div className={style.modalTableItem}>
            <div className={style.modalTableItemField}>{name}</div>
            <div className={style.modalTableItemField}>
                {Math.round((size / 1024 / 1024) * 10) / 10} mb
            </div>
            <div className={style.modalTableItemFieldBlue}>
                {user?.name?.split(' ').slice(0, 2).join(' ')}
            </div>
            <div className={style.modalTableItemField}>
                {formatDate(created_at)}
            </div>
            <div className={style.modalTableItemFieldBlue}>
                <a href={path} target='_blank' download={true} rel='noreferrer'>
                    Скачать
                </a>
                <button onClick={onRemove}>Удалить</button>
            </div>
        </div>
    );
};

export default FileTableItem;
