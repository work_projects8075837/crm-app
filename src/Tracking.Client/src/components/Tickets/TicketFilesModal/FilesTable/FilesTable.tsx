import { TICKET_FILES_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import React from 'react';
import FilesTableLayout from './FilesTableLayout';
import { IFile } from '@/services/file/types';
import FileTableItem from './FileTableItem';

const FilesTable: React.FC = () => {
    const { entities } = useFormEntityValueArray<IFile>(TICKET_FILES_NAME);
    return (
        <>
            {!!entities?.length && (
                <FilesTableLayout>
                    {entities.map((file) => (
                        <FileTableItem key={file.id} {...file} />
                    ))}
                </FilesTableLayout>
            )}
        </>
    );
};

export default FilesTable;
