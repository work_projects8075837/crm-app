import React from 'react';
import style from '../../../../style/layout/modal_table.module.scss';

interface FilesTableLayoutProps {
    children: React.ReactNode;
}

const FilesTableLayout: React.FC<FilesTableLayoutProps> = ({ children }) => {
    return (
        <div className={style.modalTable}>
            <div className={style.modalTableHeader}>
                <div className={style.modalTableHeaderField}>Имя</div>
                <div className={style.modalTableHeaderField}>Размер</div>
                <div className={style.modalTableHeaderField}>Кем добавлено</div>
                <div className={style.modalTableHeaderField}>Дата загрузки</div>
                <div className={style.modalTableHeaderField}>Действия</div>
            </div>
            <div className={style.modalTableContent}>{children}</div>
        </div>
    );
};

export default FilesTableLayout;
