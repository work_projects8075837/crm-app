import React from 'react';
import { TICKET_FILES_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { ticketFilesOpenHandler } from '../TicketOptions/TicketOptions';
import FilesModal from '@/components/Fields/FilesModal/FilesModal';

const TicketFilesModal: React.FC = () => {
    return (
        <>
            <FilesModal
                openHandler={ticketFilesOpenHandler}
                name={TICKET_FILES_NAME}
            />
        </>
    );
};

export default TicketFilesModal;
