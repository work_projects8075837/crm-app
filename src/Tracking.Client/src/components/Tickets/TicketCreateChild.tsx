import React from 'react';
import style from '../../style/layout/ticket.module.scss';
import CircleTabBtn from '../Elements/CircleTabBtn/CircleTabBtn';
import TicketCreateChildProvider from './Providers/TicketCreateChildProvider/TicketCreateChildProvider';
import GeneralInfo from './TicketCreate/GeneralInfo/GeneralInfo';
import MembersFormList from './TicketCreate/MembersFormList/MembersFormList';
import { ticketTabsHandler } from './TicketCreate/TicketCreate';
import TicketCreateHeader from './TicketCreate/TicketCreateHeader/TicketCreateHeader';
import TicketJobsCreateButton from './TicketJobsCreateButton/TicketJobsCreateButton';
import TicketCreateOptions from './TicketOptions/TicketCreateOptions';
import TicketTabsRouter, {
    ticketTabsNames,
} from './TicketTabsRouter/TicketTabsRouter';

const TicketCreateChild: React.FC = () => {
    return (
        <>
            <TicketCreateChildProvider>
                <div className={style.ticketCreateChildren}>
                    <TicketCreateHeader />
                    <TicketCreateOptions />
                    <div className={style.ticketTabsButtons}>
                        <CircleTabBtn
                            tabHandler={ticketTabsHandler}
                            toTab={ticketTabsNames.general_info}
                            text='Общая информация'
                        />

                        <TicketJobsCreateButton />
                    </div>
                    <TicketTabsRouter
                        tabHandler={ticketTabsHandler}
                        className={style.afterTabsMargin}
                        generalInfo={
                            <GeneralInfo membersList={<MembersFormList />} />
                        }
                    />
                </div>
            </TicketCreateChildProvider>
        </>
    );
};

export default TicketCreateChild;
