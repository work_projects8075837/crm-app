import { useBooleanState } from '@/hooks/useBooleanState';
import { caseHooks } from '@/services/case/case';
import { CaseFilter } from '@/services/case/types';
import { IOptionsProps } from '@/store/queries/stores/base/useAuthorizedQuery';
import { sortItemsByDate } from '../Comments/sortItemsByDate';

export const useMenuCasesList = (
    filter: CaseFilter,
    options?: IOptionsProps,
) => {
    const { data, isLoading } = caseHooks.useList(filter, options);
    const [isHideCompleted, handler] = useBooleanState(false);
    const cases = data?.data.filter((c) => !c.is_completed || !isHideCompleted);

    const datesCases =
        cases &&
        sortItemsByDate(
            cases.filter((i) => !!i.finish_date),
            'finish_date',
        )
            .map(([date, cases]) => {
                return [
                    date,
                    cases.sort((c1, c2) => {
                        return (
                            new Date(
                                c1.finish_date + ' ' + c1.finish_time,
                            ).getTime() -
                            new Date(
                                c2.finish_date + ' ' + c2.finish_time,
                            ).getTime()
                        );
                    }),
                ] as const;
            })
            .reverse();

    return { isHideCompleted, handler, data, datesCases, isLoading };
};
