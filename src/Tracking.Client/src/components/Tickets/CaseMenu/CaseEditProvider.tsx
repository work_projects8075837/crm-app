import Loader from '@/Loader/Loader';
import UpdateCaseForm from '@/components/Form/UpdateCaseForm/UpdateCaseForm';
import { caseHooks } from '@/services/case/case';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { caseId } from '../CaseDetail/CaseDetail';
import { updateCaseOpen } from './CaseMenu';

interface CaseEditProviderProps {
    children: React.ReactNode;
}

const CaseEditProvider: React.FC<CaseEditProviderProps> = ({ children }) => {
    const { data } = caseHooks.useOne(caseId.value || undefined);

    return (
        <>
            {data ? (
                <UpdateCaseForm
                    defaultValues={{
                        ...data,
                        user: data.manager?.user,
                    }}
                    openHandler={updateCaseOpen}
                >
                    {children}
                </UpdateCaseForm>
            ) : (
                <Loader size={100} />
            )}
        </>
    );
};

export default observer(CaseEditProvider);
