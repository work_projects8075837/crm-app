import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import React from 'react';
import style from '../../../style/layout/side_menu.module.scss';

const AddCaseTitle: React.FC = () => {
    const { data } = useTicketByParams();
    return (
        <>
            <h3 className={style.headerTitle}>
                Новое дело к заявке {data?.number}
            </h3>
        </>
    );
};

export default AddCaseTitle;
