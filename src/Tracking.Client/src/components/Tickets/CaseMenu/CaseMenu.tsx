import Loader from '@/Loader/Loader';
import ModalMenu from '@/components/Elements/ModalMenu/ModalMenu';
import SwitcherButton from '@/components/Elements/Switcher/SwitcherButton';
import ActionsMenuDeleteButton from '@/components/Fields/ActionsMenuDeleteButton/ActionsMenuDeleteButton';
import CreateCaseForm from '@/components/Form/CreateCaseForm/CreateCaseForm';
import Task from '@/components/SideBar/components/Task/Task';
import MenuSubHeaderLayout from '@/components/SideMenu/MenuSubHeaderLayout';
import SideMenuLayout from '@/components/SideMenu/SideMenuLayout';
import { caseApi } from '@/services/case/case';
import { GET_ALL_CASES_KEY } from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler';
import { MenuHandlerState } from '@/store/states/menuHandler';
import { observer } from 'mobx-react-lite';
import React, { Fragment } from 'react';
import { useParams } from 'react-router-dom';
import style from '../../../style/layout/side_menu.module.scss';
import notesStyle from '../../../style/notifications/message.module.scss';
import CaseDetail, { caseId } from '../CaseDetail/CaseDetail';
import CaseDetailTitle from '../CaseDetail/CaseDetailTitle';
import AddCaseButton from './AddCaseButton';
import AddCaseTitle from './AddCaseTitle';
import CaseEditProvider from './CaseEditProvider';
import CaseEditTitle from './CaseEditTitle';
import CaseFields from './CaseFields';
import CaseMenuTitle from './CaseMenuTitle';
import { useMenuCasesList } from './useMenuCasesList';

export const ticketCaseMenu = new MenuHandlerState<HTMLDivElement>('right');
export const createCaseOpen = new OpenHandler(false);
export const updateCaseOpen = new OpenHandler(false);

export const detailCaseOpen = new OpenHandler(false);

const CaseMenu: React.FC = () => {
    const { ticketId } = useParams();
    const { isHideCompleted, handler, datesCases, isLoading } =
        useMenuCasesList({
            case_ticket_id: ticketId,
        });

    return (
        <>
            <SideMenuLayout
                className={`${style.rightLayout} ${style.wideLayout}`}
                ref={ticketCaseMenu.ref}
                close={() => {
                    ticketCaseMenu.close();
                    createCaseOpen.close();
                    updateCaseOpen.close();
                    detailCaseOpen.close();
                }}
                isOpen={ticketCaseMenu.isOpen}
                header={
                    <>
                        {createCaseOpen.isOpen ||
                        detailCaseOpen.isOpen ||
                        updateCaseOpen.isOpen ? (
                            <>
                                {createCaseOpen.isOpen && <AddCaseTitle />}
                                {detailCaseOpen.isOpen && <CaseDetailTitle />}
                                {updateCaseOpen.isOpen && <CaseEditTitle />}
                            </>
                        ) : (
                            <CaseMenuTitle />
                        )}
                    </>
                }
            >
                {createCaseOpen.isOpen ||
                detailCaseOpen.isOpen ||
                updateCaseOpen.isOpen ? (
                    <>
                        {createCaseOpen.isOpen && (
                            <CreateCaseForm openHandler={createCaseOpen}>
                                <CaseFields openHandler={createCaseOpen} />
                            </CreateCaseForm>
                        )}

                        {detailCaseOpen.isOpen && caseId.value && (
                            <CaseDetail openHandler={detailCaseOpen} />
                        )}

                        {updateCaseOpen.isOpen && caseId.value && (
                            <CaseEditProvider>
                                <CaseFields
                                    openHandler={updateCaseOpen}
                                    bottomChildren={
                                        <ModalMenu side='top'>
                                            <ActionsMenuDeleteButton
                                                service={caseApi}
                                                id={caseId.value}
                                                queryKey={GET_ALL_CASES_KEY}
                                                onNavigate={() => [
                                                    updateCaseOpen.close(),
                                                ]}
                                            />
                                        </ModalMenu>
                                    }
                                />
                            </CaseEditProvider>
                        )}
                    </>
                ) : (
                    <>
                        <MenuSubHeaderLayout>
                            <AddCaseButton />

                            <SwitcherButton
                                text='Скрыть выполненые'
                                defaultChecked={isHideCompleted}
                                onChange={() => handler.toggle()}
                            />
                        </MenuSubHeaderLayout>

                        <div className={notesStyle.tasksTrack}>
                            {isLoading ? (
                                <Loader size={100} />
                            ) : (
                                <>
                                    {datesCases?.map(([date, casesByDate]) => {
                                        return (
                                            <Fragment key={date}>
                                                <div
                                                    className={
                                                        notesStyle.dayTime
                                                    }
                                                >
                                                    {date}
                                                </div>
                                                {casesByDate?.map((c) => (
                                                    <Task
                                                        filter={{
                                                            case_ticket_id:
                                                                ticketId,
                                                        }}
                                                        openHandler={
                                                            updateCaseOpen
                                                        }
                                                        key={c.id}
                                                        {...c}
                                                    />
                                                ))}
                                            </Fragment>
                                        );
                                    })}
                                </>
                            )}
                        </div>
                    </>
                )}
            </SideMenuLayout>
        </>
    );
};

export default observer(CaseMenu);
