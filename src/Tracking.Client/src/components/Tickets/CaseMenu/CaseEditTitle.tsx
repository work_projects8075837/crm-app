import { caseHooks } from '@/services/case/case';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/layout/side_menu.module.scss';
import { caseId } from '../CaseDetail/CaseDetail';

const CaseEditTitle: React.FC = () => {
    const { data } = caseHooks.useOne(caseId.value || undefined);
    return (
        <>
            <h3 className={style.headerTitle}>{data?.name}</h3>
        </>
    );
};

export default observer(CaseEditTitle);
