import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import React from 'react';
import style from '../../../style/layout/side_menu.module.scss';

const CaseMenuTitle: React.FC = () => {
    const { data } = useTicketByParams();
    return (
        <>
            <h3 className={style.headerTitle}>Дела к заявке {data?.number}</h3>
        </>
    );
};

export default CaseMenuTitle;
