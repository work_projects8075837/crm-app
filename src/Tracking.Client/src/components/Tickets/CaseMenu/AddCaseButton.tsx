import Svg from '@/components/Elements/Svg/Svg';
import BlueButton from '@/components/Fields/Buttons/BlueButton';
import React from 'react';
import style from '../../../style/elements/blue_button.module.scss';
import { createCaseOpen } from './CaseMenu';

const AddCaseButton: React.FC = () => {
    return (
        <BlueButton
            className={style.smallWidth}
            onClick={() => {
                createCaseOpen.open();
            }}
        >
            <Svg symbol='circle_plus' />
            <span>Создать</span>
        </BlueButton>
    );
};

export default AddCaseButton;
