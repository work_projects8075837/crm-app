import FormInput from '@/components/Fields/BaseFields/FormInput';
import BlueButton from '@/components/Fields/Buttons/BlueButton';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import DateInput from '@/components/Fields/DateInput/DateInput';
import EditorInput from '@/components/Fields/EditorInput/EditorInput';
import SelectManager from '@/components/Fields/SelectManager/SelectManager';
import TimeInput from '@/components/Fields/TimeInput/TimeInput';
import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import LabelFieldLayout from '@/components/Form/LoginForm/layouts/LabelFieldLayout';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';
import style from '../../../style/layout/side_menu.module.scss';
import ticketStyle from '../../../style/layout/ticket.module.scss';

interface CaseFieldsProps {
    openHandler: OpenHandler;
    bottomChildren?: React.ReactNode;
}

const CaseFields: React.FC<CaseFieldsProps> = ({
    openHandler,
    bottomChildren,
}) => {
    const { isDisabled } = useSubmit();
    return (
        <>
            <div className={style.sideMenuContainerMarginTop}>
                <LabelFieldLayout label='Что сделать:'>
                    <FormInput name='name' />
                </LabelFieldLayout>
                <LabelFieldLayout label='Срок исполнения'>
                    <div className={ticketStyle.caseDateTimeField}>
                        <DateInput name='finish_date' />
                        <TimeInput name='finish_time' />
                    </div>
                </LabelFieldLayout>

                <LabelFieldLayout label='Комментарий'>
                    <EditorInput name='comment' wrapperClass='mt2' />
                </LabelFieldLayout>

                <LabelFieldLayout label='Ответсвтвенный'>
                    <SelectManager
                        required={true}
                        fieldClass={ticketStyle.caseManagerField}
                        name='user'
                        onlySelect={true}
                    />
                </LabelFieldLayout>
            </div>
            <div className={style.bottomButtons}>
                <SaveButton
                    isLoading={isDisabled}
                    className={style.caseBlueButton}
                >
                    Сохранить
                </SaveButton>
                <BlueButton
                    onClick={(event) => {
                        event.preventDefault();
                        openHandler.close();
                    }}
                >
                    Закрыть
                </BlueButton>
                {bottomChildren}
            </div>
        </>
    );
};

export default CaseFields;
