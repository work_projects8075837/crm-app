import { historyKeys } from '@/services/history/history';
import { ticketApi } from '@/services/ticket/ticket';
import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import { updateTicket } from '@/store/queries/actions/ticket';
import {
    GET_All_TICKETS_KEY,
    GET_ONE_TICKET_BY_ID_KEY,
} from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import { useTicketByParams } from './../../../store/queries/stores/ticket/useTicketByParams';

export const useFinishTicket = () => {
    const { ticketId } = useParams();
    const { data } = useTicketByParams();

    return useMutation({
        mutationFn: async ({
            finished_at = null,
            statusId,
            start_at,
        }: {
            finished_at?: string | null;
            statusId: string;
            start_at?: string;
        }) => {
            if (!ticketId || !data) {
                throw Error();
            }
            const jobs_time = finished_at
                ? new Date(finished_at).getTime() -
                  new Date(data.created_at).getTime()
                : 0;

            updateTicket(
                start_at
                    ? {
                          finished_at,
                          jobs_time,
                          status_id: statusId,
                          start_at,
                      }
                    : {
                          finished_at,
                          jobs_time,
                          status_id: statusId,
                      },
                ticketId,
            );

            const t = await ticketApi
                .update(ticketId, {
                    finished_at,
                    jobs_time,
                    status_id: statusId,
                    start_at,
                })
                .then((res) => res.data);

            return t;
        },
        onSuccess: () => {
            invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
            totalClearKey(GET_All_TICKETS_KEY);
            totalClearKey(historyKeys.GET_ALL_KEY);
        },
    });
};
