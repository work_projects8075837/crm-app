import ArrowOpen from '@/components/Elements/FormSelect/ArrowOpen';
import { IStatus } from '@/services/status/types';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/elements/status.module.scss';
import StatusItem from '../StatusItem/StatusItem';

interface StatusSelectLayoutProps {
    children: React.ReactNode;
    currentStatus?: Omit<IStatus, 'calculate_sla'>;
    openHandler: OpenHandler;
    isDisable?: boolean;
}

const StatusSelectLayout: React.FC<StatusSelectLayoutProps> = ({
    children,
    currentStatus,
    openHandler,
    isDisable = false,
}) => {
    const defaultColor = '#1E293B';
    const defaultBackground = '#DAE3EB';

    return (
        <>
            <div className={style.statusSelect}>
                <>
                    <button
                        disabled={isDisable}
                        className={style.statusSelectButton}
                        onClick={() => openHandler.toggle()}
                    >
                        <StatusItem
                            name={currentStatus?.name || 'Выберите статус'}
                            color={currentStatus?.color || defaultColor}
                            background={
                                currentStatus?.background || defaultBackground
                            }
                            preview_link={currentStatus?.preview_link}
                        />

                        <ArrowOpen
                            isOpen={openHandler.isOpen}
                            className={style.statusSelectButtonSvg}
                            fill={currentStatus?.color}
                        />
                    </button>
                </>

                {openHandler.isOpen && (
                    <>
                        <div className={style.statusSelectTrack}>
                            {children}
                        </div>
                    </>
                )}
            </div>
        </>
    );
};

export default observer(StatusSelectLayout);
