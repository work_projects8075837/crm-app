import { ticketApi } from '@/services/ticket/ticket';
import { updateTicket } from '@/store/queries/actions/ticket';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useUpdateStartTicketDate = () => {
    const { ticketId } = useParams();
    return useMutation({
        mutationFn: async (date: string) => {
            if (ticketId) {
                updateTicket(
                    {
                        start_at: date,
                    },
                    ticketId,
                );
                return await ticketApi.update(ticketId, {
                    start_at: date,
                });
            }
        },
    });
};
