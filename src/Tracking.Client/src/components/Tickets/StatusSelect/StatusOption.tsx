import { performExistCallback } from '@/components/utils/performExistCallback';
import { IStatus } from '@/services/status/types';
import classNames from 'classnames';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import toast from 'react-hot-toast';
import style from '../../../style/elements/status.module.scss';
import StatusItem from '../StatusItem/StatusItem';
import { useStatusOptionDisable } from './useStatusOptionDisable';

interface StatusOptionProps extends React.HtmlHTMLAttributes<HTMLInputElement> {
    name: string;
    status: Omit<IStatus, 'calculate_sla'>;
    onClick?: () => void;
}

const StatusOption: React.FC<StatusOptionProps> = ({
    status,
    onClick,
    name,
}) => {
    const {
        isDisabled,
        isDisabledForChildrenTickets,
        isDisabledForDecision,
        isDisabledForResultJobs,
        isLoading,
    } = useStatusOptionDisable(status);

    const { setValue } = useFormContext();

    return (
        <>
            <button
                disabled={isLoading}
                className={classNames(style.statusLabel, {
                    [style.statusLabelDisable]: isDisabled,
                })}
                onClick={() => {
                    if (isDisabledForResultJobs) {
                        return toast.error('Ни одна из работ не выполнена');
                    }
                    if (isDisabledForDecision) {
                        return toast.error(
                            'Нельзя закрыть заявку без заполненного решения',
                        );
                    }
                    if (isDisabledForChildrenTickets) {
                        return toast.error(
                            'Нельзя закрыть заявку с незакрытыми подзаявками',
                        );
                    }
                    performExistCallback(onClick);
                    setValue(name, status.id);
                }}
            >
                <StatusItem {...status} />
            </button>
        </>
    );
};

export default StatusOption;
