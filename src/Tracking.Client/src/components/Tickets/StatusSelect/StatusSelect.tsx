import { TICKET_STATUS_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import StatusOption from './StatusOption';
import StatusSelectLayout from './StatusSelectLayoutLayuot';
import { useHandleStatus } from './useHandleStatus';
import { useSetDefaultStatus } from './useSetDefaultStatus';

const statuses = [
    { id: '1', name: 'Новое', color: '#E1980C', background: '#FFE978' },
    { id: '2', name: 'Выполнено', color: '#65A300', background: '#C2FF5F' },
    { id: '3', name: 'Новое', color: '#D50000', background: '#FF7F7F' },
];

const statusSelectOpenHandler = new OpenHandler(false);

const StatusSelect: React.FC = () => {
    const { handle, ...finishTicket } = useHandleStatus();

    const { currentStatus, directionId, statuses } = useSetDefaultStatus();

    return (
        <>
            {directionId && statuses && (
                <>
                    <StatusSelectLayout
                        isDisable={finishTicket.isLoading}
                        openHandler={statusSelectOpenHandler}
                        currentStatus={currentStatus}
                    >
                        {statuses?.map((status) => {
                            return (
                                <StatusOption
                                    name={TICKET_STATUS_NAME}
                                    key={status.id}
                                    status={status}
                                    onClick={() => {
                                        statusSelectOpenHandler.close();

                                        handle(status);
                                    }}
                                />
                            );
                        })}
                    </StatusSelectLayout>
                </>
            )}
        </>
    );
};

export default React.memo(StatusSelect);
