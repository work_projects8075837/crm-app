import { TICKET_DECISION_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { isEditorContentExist } from '@/components/Fields/SalePointSelect/SalePointSelect';
import { IStatus } from '@/services/status/types';
import { useTicketList } from '@/services/ticket/ticket';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useWatch } from 'react-hook-form';

export const useStatusOptionDisable = (status: IStatus) => {
    const ticketQuery = useTicketByParams();
    const childrenTickets = useTicketList({
        ticket_ticket_id: ticketQuery.data?.id,
    });
    const decision = useWatch({ name: TICKET_DECISION_NAME });

    const isDisabledForChildrenTickets =
        status.is_finish &&
        childrenTickets.data?.data.some((t) => !t.status?.is_finish);

    const isDisabledForResultJobs =
        status.is_finish &&
        ticketQuery.data?.job_result.length === 0 &&
        ticketQuery.data.character_problem?.job.length !== 0;

    const isDisabledForDecision =
        !isEditorContentExist(decision) && status.is_finish;

    const isDisabled =
        isDisabledForChildrenTickets ||
        isDisabledForResultJobs ||
        isDisabledForDecision;

    return {
        isDisabled,
        isDisabledForChildrenTickets,
        isDisabledForDecision,
        isDisabledForResultJobs,
        isLoading: ticketQuery.isLoading || childrenTickets.isLoading,
    };
};
