import { IStatus } from '@/services/status/types';
import { useFinishTicket } from './useFinishTicket';

export const useHandleStatus = () => {
    const finishTicket = useFinishTicket();

    const handle = async (status: IStatus) => {
        return await finishTicket.mutateAsync({
            statusId: status.id,
            finished_at: status.is_finish ? new Date().toISOString() : null,
            start_at:
                status.name === 'Возобновлена'
                    ? new Date().toISOString()
                    : undefined,
        });
    };
    return { handle, ...finishTicket };
};
