import {
    TICKET_DIRECTION_NAME,
    TICKET_STATUS_NAME,
} from '@/components/Fields/EntitiesFields/ticket';
import { useSelectEntityValue } from '@/components/Fields/SelectEntity/useSelectEntityValue';
import { useFormEntityValue } from '@/components/Fields/hooks/useFormEntityValue';
import { useStatusesByDirectionId } from '@/store/queries/stores/status/useStatusesByDirectionId';
import { useEffect } from 'react';
import { useFormContext } from 'react-hook-form';
import { useParams } from 'react-router-dom';

export const useSetDefaultStatus = () => {
    const { setValue } = useFormContext();

    const formDirection = useFormEntityValue(TICKET_DIRECTION_NAME);
    const directionId = formDirection.currentEntity?.id;

    const { data, isLoading } = useStatusesByDirectionId(directionId);

    const { ticketId } = useParams();
    const currentStatus = useSelectEntityValue(TICKET_STATUS_NAME, data);

    useEffect(() => {
        if (
            directionId &&
            data &&
            (!ticketId || (!currentStatus && ticketId))
        ) {
            const foundStatus = data.find((s) => s.name === 'Новая');

            if (foundStatus) setValue(TICKET_STATUS_NAME, foundStatus.id);
        }
    }, [directionId, isLoading, currentStatus]);

    return {
        formDirection,
        directionId,
        statuses: data,
        isStatusesLoading: isLoading,
        ticketId,
        currentStatus,
    };
};
