import React from 'react';
import style from '../../../style/elements/select_option.module.scss';
import Svg from '@/components/Elements/Svg/Svg';

interface ShortExtraCardFieldsProps {
    phone?: string;
    address?: string;
}

const ShortExtraCardFields: React.FC<ShortExtraCardFieldsProps> = ({
    phone,
    address,
}) => {
    return (
        <>
            <div className={style.extraFields}>
                <div className={style.fieldsRow}>
                    <div className={style.fieldFull}>
                        <Svg symbol='phone' className={style.optionPhoneSvg} />
                        <span>{phone}</span>
                    </div>
                </div>
                <div className={style.fieldsRow}>
                    <div className={style.fieldFull}>
                        <Svg
                            symbol='location'
                            className={style.optionLocationSvg}
                        />
                        <span>{address}</span>
                    </div>
                </div>
            </div>
        </>
    );
};

export default ShortExtraCardFields;
