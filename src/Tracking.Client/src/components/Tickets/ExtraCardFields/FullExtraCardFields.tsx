import ValueDash from '@/components/Elements/ValueDash/ValueDash';
import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

interface FullExtraCardFieldsProps {
    first?: { icon?: React.ReactNode; text?: string | React.ReactNode };
    second?: { icon?: React.ReactNode; text?: string | React.ReactNode };
    third?: { icon?: React.ReactNode; text?: string | React.ReactNode };
}

const FullExtraCardFields: React.FC<FullExtraCardFieldsProps> = ({
    first,
    second,
    third,
}) => {
    return (
        <>
            <div className={style.extraFields}>
                {first && (
                    <>
                        <div className={style.fieldsRow}>
                            <div className={style.fieldFull}>
                                {first.icon}
                                <span>
                                    <ValueDash className={style.fieldDash}>
                                        {first.text}
                                    </ValueDash>
                                </span>
                            </div>
                        </div>
                    </>
                )}
                {second && (
                    <>
                        <div className={style.fieldsRow}>
                            <div className={style.fieldFull}>
                                {second.icon}
                                <span>
                                    <ValueDash className={style.fieldDash}>
                                        {second.text}
                                    </ValueDash>
                                </span>
                            </div>
                        </div>
                    </>
                )}
                {third && (
                    <>
                        <div className={style.fieldsRow}>
                            <div className={style.fieldFull}>
                                {third.icon}
                                <span>
                                    <ValueDash className={style.fieldDash}>
                                        {third.text}
                                    </ValueDash>
                                </span>
                            </div>
                        </div>
                    </>
                )}
            </div>
        </>
    );
};

export default FullExtraCardFields;
