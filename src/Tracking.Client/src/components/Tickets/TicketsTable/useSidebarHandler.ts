import { useRef, useState } from 'react';

const transitionTime = 300;

export const useSidebarHandler = () => {
    const [isOpen, setOpen] = useState<boolean>(false);
    const sidebarElemRef = useRef<HTMLDivElement>(null);

    const open = () => {
        setOpen(true);
        if (sidebarElemRef.current) {
            sidebarElemRef.current.style.width = '100px';
        }
    };

    const close = () => {
        if (sidebarElemRef.current) {
            sidebarElemRef.current.style.width = '20px';
        }
        setTimeout(() => setOpen(false), transitionTime / 2);
    };

    return { isOpen, open, close, sidebarElemRef, transitionTime } as const;
};
