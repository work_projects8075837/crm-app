import { useArrayQueryParameter } from '@/hooks/useQueryParams';
import { IStatus } from '@/services/status/types';
import { generateQuery } from '@/services/utils/generateQuery/generateQuery';
import classNames from 'classnames';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/table.module.scss';

type SidebarStatusProps = IStatus;

const SidebarStatus: React.FC<SidebarStatusProps> = ({ name, id }) => {
    const paramName = 'status_id';
    const statusIds = useArrayQueryParameter(paramName);

    const navigate = useNavigate();

    const isSelected = statusIds?.includes(id);
    return (
        <>
            <button
                className={classNames(style.ticketsSidebarItem, {
                    [style.ticketsSidebarItemSelected]: isSelected,
                })}
                onClick={() => {
                    if (isSelected) {
                        navigate(
                            `./${generateQuery({
                                [paramName]: statusIds?.filter(
                                    (statusId) => statusId !== id,
                                ),
                            })}`,
                        );
                    } else {
                        navigate(
                            `./${generateQuery({
                                [paramName]: [...(statusIds || []), id],
                            })}`,
                        );
                    }
                }}
            >
                {name}
            </button>
        </>
    );
};

export default SidebarStatus;
