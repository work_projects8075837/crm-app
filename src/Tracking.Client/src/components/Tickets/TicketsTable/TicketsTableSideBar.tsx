import { useStatusList } from '@/services/status/status';
import React from 'react';
import SidebarStatus from './SidebarStatus';
import TicketsTableSideBarLayout from './TicketsTableSideBarLayout';

interface TicketsTableSideBarProps {
    text?: string;
}

const sidebarItems = [
    {
        name: 'Все',
    },
    {
        name: 'Новая',
    },
    {
        name: 'В работе',
    },
    {
        name: 'Передано менеджеру',
    },
    {
        name: 'Требуется выезд',
    },
    {
        name: 'Недозвон',
    },
    {
        name: 'Выполнено',
    },
];

const TicketsTableSideBar: React.FC<TicketsTableSideBarProps> = () => {
    const { data } = useStatusList({ offset: 200 }, { refetchOnMount: true });
    return (
        <>
            <TicketsTableSideBarLayout>
                {data?.data.map((s) => <SidebarStatus key={s.id} {...s} />)}
            </TicketsTableSideBarLayout>
        </>
    );
};

export default React.memo(TicketsTableSideBar);
