import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/layout/table.module.scss';
import { useSidebarHandler } from './useSidebarHandler';

interface TicketsTableSideBarLayoutProps {
    text?: string;
    children?: React.ReactNode;
}

const TicketsTableSideBarLayout: React.FC<TicketsTableSideBarLayoutProps> = ({
    children,
    text = 'Статусы',
}) => {
    const { isOpen, open, close, sidebarElemRef, transitionTime } =
        useSidebarHandler();

    return (
        <>
            <div
                className={style.tableSidebar}
                style={{ transition: `width ${transitionTime}ms` }}
                ref={sidebarElemRef}
            >
                {isOpen ? (
                    <>
                        <div className={style.tableOpenedSidebar}>
                            <button
                                onClick={close}
                                className={style.tableOpenedSidebarName}
                            >
                                <Svg
                                    symbol='vert_arrow'
                                    className={style.vertArrowSvg}
                                />

                                <span>Свернуть</span>
                            </button>
                            <div className={style.tableOpenedSideBarContent}>
                                {children}
                            </div>
                        </div>
                    </>
                ) : (
                    <>
                        <button
                            className={style.tableSidebarBurger}
                            onClick={open}
                        >
                            <Svg
                                symbol='little_plates'
                                className={style.platesSvg}
                            />
                            <span className={style.tableSidebarText}>
                                {text}
                            </span>
                        </button>
                    </>
                )}
            </div>
        </>
    );
};

export default TicketsTableSideBarLayout;
