import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/elements/tab.module.scss';
import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import { ITicket } from '@/services/ticket/types';
import { useNavigate } from 'react-router-dom';

const TicketJobsCreateButton: React.FC = () => {
    const { onSubmit, isDisabled } = useSubmit();
    const navigate = useNavigate();
    return (
        <div>
            <button
                disabled={isDisabled}
                onClick={() => {
                    if (onSubmit) {
                        onSubmit((newTicket: ITicket) => {
                            navigate(`/ticket/${newTicket.id}/jobs`);
                        });
                    }
                }}
                className={style.tab}
            >
                Учёт работ
            </button>
        </div>
    );
};

export default observer(TicketJobsCreateButton);
