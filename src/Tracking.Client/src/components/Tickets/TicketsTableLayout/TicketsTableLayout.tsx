import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import { ticketFieldsOpen } from '@/pages/task/Tickets';
import { ITicket } from '@/services/ticket/types';
import { IField } from '@/store/states/FieldsHandler';
import { StyleTable } from '@/style/layout/layout';
import React from 'react';
import { ticketCheckHandler } from '../TicketsList/TicketsList';
import TicketsPagination from '../TicketsPagination/TicketsPagination';
import TicketsTableSideBar from '../TicketsTable/TicketsTableSideBar';

interface TicketsTableLayoutProps {
    children: React.ReactNode;
    style: React.CSSProperties;
    fields: IField<ITicket>[];
}

const ticketsTableFields = [
    { name: 'Номер' },
    { name: 'Дата и время создания' },
    { name: 'Клиент' },
    { name: 'Контактный телефон' },
    { name: 'Характер проблемы' },
    { name: 'Ответственный' },
    { name: 'Дата и время завершения SLA' },
    { name: 'Статус заявки' },
];

const TicketsTableLayout: React.FC<TicketsTableLayoutProps> = ({
    children,
    style,
    fields,
}) => {
    return (
        <>
            <TableLayout
                settingsOpenHandler={ticketFieldsOpen}
                layoutClass={StyleTable.ticketsTable}
                header={
                    <TableHeader
                        checkHandler={ticketCheckHandler}
                        fields={fields}
                        fieldClass={StyleTable.ticketHeaderField}
                        layoutClass={StyleTable.ticketsRowLayout}
                        style={style}
                    />
                }
                pagination={<TicketsPagination />}
                extraChild={<TicketsTableSideBar />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default TicketsTableLayout;
