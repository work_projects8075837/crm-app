import React from 'react';
import style from '../../../style/layout/small_table.module.scss';

interface SmallTicketTableLayoutProps {
    children: React.ReactNode;
    layoutClass?: string;
    fields: { name: string }[];
}

const SmallTicketTableLayout: React.FC<SmallTicketTableLayoutProps> = ({
    children,
    fields,
    layoutClass = style.rowLayout,
}) => {
    return (
        <div className={`${style.table}`}>
            <div className={`${style.tableRow} ${layoutClass}`}>
                {fields.map(({ name }) => (
                    <span key={name} className={style.tableRowField}>
                        {name}
                    </span>
                ))}
            </div>
            {children}
        </div>
    );
};

export default SmallTicketTableLayout;
