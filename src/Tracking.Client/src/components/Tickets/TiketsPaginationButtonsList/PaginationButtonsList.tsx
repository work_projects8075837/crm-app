import React from 'react';
import PageButton from '@/components/Tables/Pagination/PageButton';
import { observer } from 'mobx-react-lite';
import { IdEntity, PaginatedData } from '../../../services/types';
import { PaginationHandler } from '@/store/states/PaginationHandler.ts';

interface PaginationButtonsListProps {
    data?: PaginatedData<IdEntity[]>;
    paginationHandler: PaginationHandler;
}

const PaginationButtonsList: React.FC<PaginationButtonsListProps> = ({
    data,
    paginationHandler,
}) => {
    return (
        <>
            {data && (
                <>
                    {paginationHandler.page !== 1 && (
                        <PageButton
                            page={paginationHandler.page - 1}
                            pagonationHandler={paginationHandler}
                        />
                    )}
                    <PageButton
                        page={paginationHandler.page}
                        pagonationHandler={paginationHandler}
                    />
                    {paginationHandler.page + 1 < data.pages && (
                        <PageButton
                            page={paginationHandler.page + 1}
                            pagonationHandler={paginationHandler}
                        />
                    )}

                    {paginationHandler.page + 2 < data.pages && (
                        <PageButton
                            page={paginationHandler.page + 2}
                            pagonationHandler={paginationHandler}
                        />
                    )}

                    {data.pages > 3 && (
                        <>
                            <span>...</span>
                            <PageButton
                                page={data.pages || 1}
                                pagonationHandler={paginationHandler}
                            />
                        </>
                    )}
                </>
            )}
        </>
    );
};

export default observer(PaginationButtonsList);
