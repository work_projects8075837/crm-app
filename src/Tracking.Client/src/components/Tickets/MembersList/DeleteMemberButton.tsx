import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import React from 'react';
import '../../../style/layout/small_table.scss';
import { useFormMembers } from '../hooks/useFormMembers';

interface DeleteMemberButtonProps {
    id?: string;
    userId: string;
    onDeleteMember?: (managerId?: string) => void | Promise<void>;
}

const DeleteMemberButton: React.FC<DeleteMemberButtonProps> = ({
    id,
    userId,
    onDeleteMember,
}) => {
    const { remove } = useFormMembers();
    return (
        <>
            <CloseCrossButton
                onClick={() => {
                    if (onDeleteMember) {
                        onDeleteMember(id);
                    }
                    remove(userId);
                }}
                className='deleteRowButton'
            />
        </>
    );
};

export default React.memo(DeleteMemberButton);
