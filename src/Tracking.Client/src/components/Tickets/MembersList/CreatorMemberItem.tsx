import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import React from 'react';
import MemberItem from './MemberItem';
import { TICKET_MEMBERS_TYPES } from '@/components/Fields/EntitiesFields/ticket';

const CreatorMemberItem: React.FC = () => {
    const { data } = useCurrentUser();
    return (
        <>
            {!!data && (
                <MemberItem type={TICKET_MEMBERS_TYPES.author} user={data} />
            )}
        </>
    );
};

export default CreatorMemberItem;
