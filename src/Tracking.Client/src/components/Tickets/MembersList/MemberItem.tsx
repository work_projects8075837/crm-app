import React from 'react';
import style from '../../../style/layout/small_table.module.scss';
import ValueDash from '@/components/Elements/ValueDash/ValueDash';
import { IShortMember } from '../hooks/useFormMembers';
import '../../../style/layout/small_table.scss';
import { TICKET_MEMBERS_TYPES } from '@/components/Fields/EntitiesFields/ticket';

interface MemberItemProps extends IShortMember {
    buttonChild?: React.ReactNode;
    onTypeClick?: () => void;
}

const MemberItem: React.FC<MemberItemProps> = ({
    type,
    user,
    buttonChild,
    onTypeClick,
}) => {
    const isClickable = onTypeClick && type !== TICKET_MEMBERS_TYPES.author;
    return (
        <div className={`${style.defaultTableRow} tableRow`}>
            <span className={style.tableRowField}>
                <ValueDash height={1}>{user.name}</ValueDash>
            </span>
            <button
                className={
                    isClickable
                        ? style.tableRowFieldClickable
                        : style.tableRowField
                }
                onClick={isClickable ? onTypeClick : undefined}
                style={
                    isClickable ? { cursor: 'pointer' } : { cursor: 'default' }
                }
            >
                <ValueDash height={1}>
                    <span>{type}</span>
                </ValueDash>
            </button>
            <span className={style.tableRowField}>
                <ValueDash height={1}>{user.phone}</ValueDash>
            </span>
            {isClickable && buttonChild}
        </div>
    );
};

export default MemberItem;
