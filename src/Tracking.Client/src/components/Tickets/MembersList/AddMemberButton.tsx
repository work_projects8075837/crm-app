import React from 'react';
import style from '../../../style/layout/small_table.module.scss';
import Svg from '@/components/Elements/Svg/Svg';
import { membersAddModalOpenHandler } from '../MembersAddModal/MembersAddModal';

const AddMemberButton: React.FC = () => {
    return (
        <>
            <button
                className={style.tableRowButton}
                onClick={() => membersAddModalOpenHandler.open()}
            >
                <Svg symbol='add_plus' />
                <span>Добавить</span>
            </button>
        </>
    );
};

export default React.memo(AddMemberButton);
