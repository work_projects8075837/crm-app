import { TICKET_MEMBERS_TYPES } from '@/components/Fields/EntitiesFields/ticket';
import React from 'react';
import SmallTicketTableLayout from '../SmallTicketTableLayout/SmallTicketTableLayout';
import { useFormMembers } from './../hooks/useFormMembers';
import DeleteMemberButton from './DeleteMemberButton';
import MemberItem from './MemberItem';

const membersFields = [
    { name: 'Имя' },
    { name: 'Должность' },
    { name: 'Номер телефона' },
];

// const members = [
//     {
//         id: '6f86afc0-42d3-4132-8c92-85b8cd9d0405',
//         user: {
//             name: 'Тестище Тест',
//             phone: '+7(999)999-99-99',
//         },
//         type: 'Ответсвенный',
//     },
//     {
//         id: '2f71cga3-42d3-4132-8c92-85b8cd9d0405',
//         user: {
//             name: 'Тестов Тест',
//             phone: '+7(999)999-99-99',
//         },

//         type: 'Вовлеченный',
//     },
// ];

interface MembersListProps {
    creatorMember?: React.ReactNode;
    addMemberButton?: React.ReactNode;
    onDeleteMember?: (managerId?: string) => void | Promise<void>;
    onTypeChangeClick?: (data: {
        managerId?: string;
        type: string;
    }) => void | Promise<void>;
}

const MembersList: React.FC<MembersListProps> = ({
    creatorMember,
    addMemberButton,
    onDeleteMember,
    onTypeChangeClick,
}) => {
    const { members, toggleType } = useFormMembers();
    return (
        <SmallTicketTableLayout fields={membersFields}>
            {creatorMember}
            {members
                ?.sort((o1, o2) =>
                    o1.type === TICKET_MEMBERS_TYPES.author ? -1 : 1,
                )
                ?.map((member) => (
                    <MemberItem
                        key={`${member.user.id}_${member.id}`}
                        {...member}
                        onTypeClick={() => {
                            if (onTypeChangeClick)
                                onTypeChangeClick({
                                    managerId: member.id,
                                    type:
                                        member.type ===
                                        TICKET_MEMBERS_TYPES.involved
                                            ? TICKET_MEMBERS_TYPES.responsible
                                            : TICKET_MEMBERS_TYPES.involved,
                                });
                            toggleType(member.user.id);
                        }}
                        buttonChild={
                            <DeleteMemberButton
                                id={member.id}
                                onDeleteMember={onDeleteMember}
                                userId={member.user.id}
                            />
                        }
                    />
                ))}
            {addMemberButton}
        </SmallTicketTableLayout>
    );
};

export default React.memo(MembersList);
