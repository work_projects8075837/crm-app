import React from 'react';
import style from '../../../style/elements/status.module.scss';

type StatusItemProps = {
    background: string;
    color: string;
    name: string;
    preview_link?: string;
    onClick?: () => void;
};
const StatusItem: React.FC<StatusItemProps> = ({
    background,
    color,
    name,
    preview_link,
    onClick,
}) => {
    return (
        <>
            <div
                onClick={onClick}
                className={style.statusOption}
                style={{ backgroundColor: background, color }}
            >
                <span className={style.statusOptionName}>
                    {!!preview_link && (
                        <>
                            <img
                                src={preview_link}
                                alt={name}
                                className={style.statusOptionIcon}
                            />
                        </>
                    )}

                    <span>{name}</span>
                </span>
            </div>
        </>
    );
};

export default StatusItem;
