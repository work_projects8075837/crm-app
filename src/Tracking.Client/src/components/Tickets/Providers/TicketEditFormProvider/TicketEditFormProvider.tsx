import Loader from '@/Loader/Loader';
import UpdateTicketForm from '@/components/Form/UpdateTicketForm/UpdateTicketForm';
import Layout from '@/components/NavbarLayout/Layout';
import { useCounterPartyBreadCrumbs } from '@/store/queries/stores/counterparty/useCounterPartyBreadCrumbs';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';
import { useSelectQueriesLoading } from '../../hooks/useSelectQueriesLoading';
import { formatTicketFormDefaultValues } from './formatTicketFormDefaultValues';

interface TicketEditFormProviderProps {
    children: React.ReactNode;
    modals?: React.ReactNode;
    formBottom?: React.ReactNode;
}

const TicketEditFormProvider: React.FC<TicketEditFormProviderProps> = ({
    children,
    modals,
    formBottom,
}) => {
    const { isLoading, data } = useTicketByParams();
    const breadcrumbs = useCounterPartyBreadCrumbs({ refetchOnMount: true });
    const selectsLoading = useSelectQueriesLoading();

    return (
        <>
            {isLoading || breadcrumbs.isLoading || selectsLoading || !data ? (
                <Loader />
            ) : (
                <>
                    <Layout childrenClass={style.ticketCreate}>
                        <UpdateTicketForm
                            modals={modals}
                            formBottom={formBottom}
                            ticketForm={formatTicketFormDefaultValues(data)}
                        >
                            {children}
                        </UpdateTicketForm>
                    </Layout>
                </>
            )}
        </>
    );
};

export default TicketEditFormProvider;
