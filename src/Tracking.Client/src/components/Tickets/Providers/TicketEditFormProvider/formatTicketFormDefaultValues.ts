import { ITicket } from '@/services/ticket/types';

export const formatTicketFormDefaultValues = (data: ITicket) => {
    const {
        channel,
        direction,
        character_problem,
        sale_point,
        contact,
        manager,
        tag,
        description,
        decision,
        status,
        counterparty,
        file,
    } = data;

    return {
        channel: channel,
        direction: direction,
        character_problem: character_problem,
        sale_point: sale_point,
        contact: contact,
        status_id: status?.id,
        description: description,
        decision: decision,
        manager: manager.map((member) => ({
            id: member.id,
            type: member.type,
            user: {
                name: member.user.name,
                id: member.user.id,
                phone: member.user.phone,
            },
        })),
        tag: tag,
        counterparty: counterparty,
        file: file,
    };
};
