import React from 'react';
import { useSelectQueriesLoading } from '../../hooks/useSelectQueriesLoading';
import Loader from '@/Loader/Loader';

interface TicketCreateProviderProps {
    children: React.ReactNode;
}

const TicketCreateProvider: React.FC<TicketCreateProviderProps> = ({
    children,
}) => {
    const isLoading = useSelectQueriesLoading();
    return <>{isLoading ? <Loader /> : children}</>;
};

export default TicketCreateProvider;
