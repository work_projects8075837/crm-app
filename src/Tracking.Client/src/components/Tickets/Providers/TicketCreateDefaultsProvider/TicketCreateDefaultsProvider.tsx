import TicketCreateForm from '@/components/Form/TicketCreateForm/TicketCreateForm';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import React from 'react';
import AddContactModal from '../../AddContactModal/AddContactModal';
import MembersAddModal from '../../MembersAddModal/MembersAddModal';
import CreatorMemberItem from '../../MembersList/CreatorMemberItem';
import AddTagsModal from '../../TicketCreate/AddTagsModal/AddTagsModal';
import TicketFilesModal from '../../TicketFilesModal/TicketFilesModal';

interface TicketCreateDefaultsProviderProps {
    children: React.ReactNode;
}

const TicketCreateDefaultsProvider: React.FC<
    TicketCreateDefaultsProviderProps
> = ({ children }) => {
    const { data } = useCurrentUser();
    return (
        <>
            {data && (
                <TicketCreateForm
                    channel={data.channel}
                    direction={data.direction}
                    modals={
                        <>
                            <TicketFilesModal />
                            <AddContactModal />
                            <AddTagsModal />
                            <MembersAddModal
                                creatorMember={<CreatorMemberItem />}
                            />
                        </>
                    }
                >
                    {children}
                </TicketCreateForm>
            )}
        </>
    );
};

export default TicketCreateDefaultsProvider;
