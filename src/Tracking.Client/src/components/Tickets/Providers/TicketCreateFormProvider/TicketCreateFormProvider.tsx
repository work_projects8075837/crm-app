import React from 'react';

interface TicketCreateFormProviderProps {
    children: React.ReactNode;
}

const TicketCreateFormProvider: React.FC<
    TicketCreateFormProviderProps
> = () => {
    return <></>;
};

export default TicketCreateFormProvider;
