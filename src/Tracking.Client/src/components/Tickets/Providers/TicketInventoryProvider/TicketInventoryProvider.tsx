import React from 'react';
import { useFormInventory } from '../../hooks/useFormInventory';

interface TicketInventoryProviderProps {
    children: React.ReactNode;
}

const TicketInventoryProvider: React.FC<TicketInventoryProviderProps> = ({
    children,
}) => {
    const { data } = useFormInventory();
    return <>{!!data && children}</>;
};

export default TicketInventoryProvider;
