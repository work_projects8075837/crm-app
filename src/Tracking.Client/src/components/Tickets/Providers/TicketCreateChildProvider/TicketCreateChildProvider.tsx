import Loader from '@/Loader/Loader';
import TicketCreateForm from '@/components/Form/TicketCreateForm/TicketCreateForm';
import Layout from '@/components/NavbarLayout/Layout';
import { useTicketBreadCrumbs } from '@/store/queries/stores/ticket/useTicketBreadCrumbs';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';
import AddContactModal from '../../AddContactModal/AddContactModal';
import MembersAddModal from '../../MembersAddModal/MembersAddModal';
import CreatorMemberItem from '../../MembersList/CreatorMemberItem';
import AddTagsModal from '../../TicketCreate/AddTagsModal/AddTagsModal';
import TicketFilesModal from '../../TicketFilesModal/TicketFilesModal';
import TicketCreateProvider from '../TicketCreateProvider/TicketCreateProvider';

interface TicketCreateChildProviderProps {
    children: React.ReactNode;
}

const TicketCreateChildProvider: React.FC<TicketCreateChildProviderProps> = ({
    children,
}) => {
    const { data } = useTicketByParams();
    const { isLoading } = useTicketBreadCrumbs({ refetchOnMount: true });
    const userQuery = useCurrentUser();

    return (
        <>
            {data && !isLoading && userQuery.data ? (
                <TicketCreateProvider>
                    <Layout childrenClass={style.ticketCreate}>
                        <TicketCreateForm
                            parentTicket={data}
                            channel={userQuery.data.channel}
                            direction={userQuery.data.direction}
                            modals={
                                <>
                                    <TicketFilesModal />
                                    <AddContactModal />
                                    <AddTagsModal />
                                    <MembersAddModal
                                        creatorMember={<CreatorMemberItem />}
                                    />
                                </>
                            }
                        >
                            {children}
                        </TicketCreateForm>
                    </Layout>
                </TicketCreateProvider>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default TicketCreateChildProvider;
