import { TICKET_MEMBERS_TYPES } from '@/components/Fields/EntitiesFields/ticket';
import { calculateSla } from '@/components/utils/calculateSla';
import { formatDate } from '@/components/utils/formatDate';
import { formatPhoneLink } from '@/components/utils/formatPhoneLink';
import { ITicket } from '@/services/ticket/types';
import { FieldsHandler } from '@/store/states/FieldsHandler';
import StatusItem from '../StatusItem/StatusItem';

export const ticketFields = new FieldsHandler<ITicket>(
    [
        {
            key: 'number',
            name: 'Номер',
            getValue: (t) => t.number,
            size: '92px',
        },
        {
            key: 'created_at',
            name: 'Дата и время создания',
            getValue: (t) => formatDate(t.created_at),
            size: 'minmax(110px, 1fr)',
        },
        {
            key: 'sale_point',
            name: 'Клиент',
            getValue: (t) => t.sale_point?.name,
            size: 'minmax(140px, 1fr)',
        },
        {
            key: 'phone_number',
            name: 'Контактный телефон',
            getValue: (t) =>
                t.contact?.phone_number.map((p) => (
                    <a
                        href={`tel:${formatPhoneLink(p.name)}`}
                        key={p.id}
                        onClick={(e) => e.stopPropagation()}
                    >
                        {p.name}
                    </a>
                )),
            size: 'minmax(130px, 1fr)',
        },
        {
            key: 'character_problem',
            name: 'Характер проблемы',
            getValue: (t) => t.character_problem?.name,
            size: 'minmax(170px, 1fr)',
        },
        {
            key: 'manager',
            name: 'Ответственный',
            getValue: (t) => {
                return (
                    t.manager.find(
                        (m) => m.type === TICKET_MEMBERS_TYPES.responsible,
                    )?.user.name || t.manager[0]?.user.name
                );
            },
            size: 'minmax(100px, 1fr)',
        },
        {
            key: 'sla',
            name: 'Дата и время завершения SLA',
            getValue: (t) => calculateSla(t.character_problem?.sla),
            size: 'minmax(110px, 1fr)',
        },
        {
            key: 'status',
            name: 'Статус заявки',
            getValue: (t) => t.status && <StatusItem {...t.status} />,
            size: '166px',
        },
    ],
    [
        'number',
        'created_at',
        'sale_point',
        'phone_number',
        'character_problem',
        'manager',
        'sla',
        'status',
    ],
    'TICKET_FIELDS',
);
