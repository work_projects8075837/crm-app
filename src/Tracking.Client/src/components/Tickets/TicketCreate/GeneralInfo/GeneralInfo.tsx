import FormSelect from '@/components/Elements/FormSelect/FormSelect';
import CharacterProblemSelect from '@/components/Fields/CharacterProblemSelect/CharacterProblemSelect';
import ContactEntitySelect from '@/components/Fields/ContactEntitySelect/ContactEntitySelect';
import SalePointSelect from '@/components/Fields/SalePointSelect/SalePointSelect';
import SelectChannel from '@/components/Fields/SelectChannel/SelectChannel';
import SelectDirection from '@/components/Fields/SelectDirection/SelectDirection';
import DoubleColumnsLayout from '@/components/Layouts/DoubleColumnsLayout/DoubleColumnsLayout';
import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';
import DescriptionEditor from '../../DescriptionEditor/DescriptionEditor';
import InventoryTabs from '../../InventoryList/InventoryTabs';
import TicketInventoryProvider from '../../Providers/TicketInventoryProvider/TicketInventoryProvider';

interface GeneralInfoProps {
    membersList?: React.ReactNode;
    decisionSection?: React.ReactNode;
}

const GeneralInfo: React.FC<GeneralInfoProps> = ({
    membersList,
    decisionSection,
}) => {
    return (
        <>
            <DoubleColumnsLayout
                left={
                    <>
                        <FormSelect
                            title={'Основное'}
                            childrenClass={style.IndexReverse}
                        >
                            <CharacterProblemSelect />

                            <SelectDirection />

                            <SelectChannel required={true} />
                        </FormSelect>

                        <FormSelect title='Описание'>
                            <DescriptionEditor />
                        </FormSelect>

                        {decisionSection}

                        {/* <FormSelect title='Модули'>
                            <TagsList />
                        </FormSelect> */}
                    </>
                }
                right={
                    <>
                        <FormSelect
                            title={'Клиент'}
                            childrenClass={style.IndexReverse}
                        >
                            <ContactEntitySelect required={true} />

                            <SalePointSelect required={true} />
                        </FormSelect>

                        <TicketInventoryProvider>
                            <InventoryTabs />
                        </TicketInventoryProvider>

                        <FormSelect title='Вовлеченные сотрудники'>
                            {membersList}
                        </FormSelect>
                    </>
                }
            />
        </>
    );
};

export default GeneralInfo;
