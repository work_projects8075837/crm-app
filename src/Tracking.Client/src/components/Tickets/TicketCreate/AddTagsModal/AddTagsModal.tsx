import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import TagCreateForm from '@/components/Form/TagCreateForm/TagCreateForm';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import style from '../../../../style/layout/modal_window.module.scss';
import { OpenHandler } from '@/store/states/OpenHandler.ts';

export const tagsModalHandler = new OpenHandler(false);

const AddTagsModal: React.FC = () => {
    const { setValue } = useFormContext();

    return (
        <>
            <ModalWindow
                openHandler={tagsModalHandler}
                title='Добавить модуль'
                layoutClass={style.modalWide}
            >
                <TagCreateForm close={() => tagsModalHandler.close()} />
            </ModalWindow>
        </>
    );
};

export default React.memo(AddTagsModal);
