import React from 'react';
import SmallTicketTableLayout from '../../SmallTicketTableLayout/SmallTicketTableLayout';
import { useFormMembers } from '../../hooks/useFormMembers';
import MemberItem from '../../MembersList/MemberItem';
import MembersList from '../../MembersList/MembersList';
import CreatorMemberItem from '../../MembersList/CreatorMemberItem';
import AddMemberButton from '../../MembersList/AddMemberButton';

const membersFields = [
    { name: 'Имя' },
    { name: 'Должность' },
    { name: 'Номер телефона' },
];

const MembersFormList: React.FC = () => {
    return (
        <>
            <MembersList
                creatorMember={<CreatorMemberItem />}
                addMemberButton={<AddMemberButton />}
            />
        </>
    );
};

export default React.memo(MembersFormList);
