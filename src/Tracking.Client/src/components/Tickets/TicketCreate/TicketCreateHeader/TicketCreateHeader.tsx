import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import TicketsLink from '@/components/Elements/BreadCrumbs/TicketsLink';
import Dash from '@/components/Elements/Dash/Dash';
import HeaderLayout from '@/components/Layouts/HeaderLayout/HeaderLayout';
import { formatDate } from '@/components/utils/formatDate';
import { useTicketBreadCrumbs } from '@/store/queries/stores/ticket/useTicketBreadCrumbs';
import React from 'react';
import style from '../../../../style/layout/header.module.scss';
import SlaHeaderDate from '../../SlaHeaderDate/SlaHeaderDate';
import TicketHeaderLabelLayout from '../../TicketHeaderLabelLayout/TicketHeaderLabelLayout';

const TicketCreateHeader: React.FC = () => {
    const { data } = useTicketBreadCrumbs();
    return (
        <>
            <HeaderLayout
                className={style.smallHeaderMargin}
                title='Новая заявка'
                breadcrumbs={
                    <>
                        <span>
                            <TicketsLink />{' '}
                            <InnerBreadCrumbs links={data} route='ticket' /> /
                            Новая заявка
                        </span>
                    </>
                }
            >
                <div className={style.headerChildrenContainer}>
                    <TicketHeaderLabelLayout title='Дата и время создания'>
                        {formatDate(new Date().toString())}
                    </TicketHeaderLabelLayout>

                    <SlaHeaderDate />

                    <TicketHeaderLabelLayout
                        title='Остаток времени'
                        isBlocked={true}
                    >
                        <Dash />
                    </TicketHeaderLabelLayout>

                    <TicketHeaderLabelLayout
                        title='Фактическое завершение'
                        isBlocked={true}
                    >
                        <Dash />
                    </TicketHeaderLabelLayout>

                    <TicketHeaderLabelLayout
                        title='Эффективное время'
                        isBlocked={true}
                    >
                        <Dash />
                    </TicketHeaderLabelLayout>
                </div>
            </HeaderLayout>
        </>
    );
};

export default TicketCreateHeader;
