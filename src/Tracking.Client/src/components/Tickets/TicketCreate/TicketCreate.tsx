import CircleTabBtn from '@/components/Elements/CircleTabBtn/CircleTabBtn';
import Layout from '@/components/NavbarLayout/Layout';
import { TabHandler } from '@/store/states/TabHandler.ts';
import React from 'react';
import style from '../../../style/layout/ticket.module.scss';
import TicketCreateDefaultsProvider from '../Providers/TicketCreateDefaultsProvider/TicketCreateDefaultsProvider';
import TicketCreateProvider from '../Providers/TicketCreateProvider/TicketCreateProvider';
import {} from '../TicketEdit/TicketEdit';
import TicketJobsCreateButton from '../TicketJobsCreateButton/TicketJobsCreateButton';
import TicketCreateOptions from '../TicketOptions/TicketCreateOptions';
import TicketTabsRouter from '../TicketTabsRouter/TicketTabsRouter';
import { ticketTabsNames } from './../TicketTabsRouter/TicketTabsRouter';
import GeneralInfo from './GeneralInfo/GeneralInfo';
import MembersFormList from './MembersFormList/MembersFormList';
import TicketCreateHeader from './TicketCreateHeader/TicketCreateHeader';

export const ticketTabsHandler = new TabHandler(ticketTabsNames.general_info);

const TicketCreate: React.FC = () => {
    return (
        <>
            <TicketCreateProvider>
                <Layout childrenClass={style.ticketCreate}>
                    <TicketCreateDefaultsProvider>
                        <div className={style.ticketCreateChildren}>
                            <TicketCreateHeader />
                            <TicketCreateOptions />
                            <div className={style.ticketTabsButtons}>
                                <CircleTabBtn
                                    tabHandler={ticketTabsHandler}
                                    toTab={ticketTabsNames.general_info}
                                    text='Общая информация'
                                />

                                {/* <CircleLink to='./'>Внешние заявки</CircleLink>

                                <CircleTabBtn
                                    tabHandler={ticketTabsHandler}
                                    toTab={ticketTabsNames.sub_tickets}
                                    text='Подзаявки'
                                /> */}

                                <TicketJobsCreateButton />
                            </div>
                            <TicketTabsRouter
                                tabHandler={ticketTabsHandler}
                                className={style.afterTabsMargin}
                                generalInfo={
                                    <GeneralInfo
                                        membersList={<MembersFormList />}
                                    />
                                }
                            />
                        </div>
                    </TicketCreateDefaultsProvider>
                </Layout>
            </TicketCreateProvider>
        </>
    );
};

export default TicketCreate;
