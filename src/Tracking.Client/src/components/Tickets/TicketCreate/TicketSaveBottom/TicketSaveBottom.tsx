import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';
import TicketSaveButton from './TicketSaveButton';
import TicketSaveCloseButton from './TicketSaveCloseButton';

const TicketSaveBottom: React.FC = () => {
    return (
        <>
            <div className={style.ticketSubmitBottom}>
                {/* <TicketSaveButton /> */}
                <TicketSaveCloseButton />
            </div>
        </>
    );
};

export default TicketSaveBottom;
