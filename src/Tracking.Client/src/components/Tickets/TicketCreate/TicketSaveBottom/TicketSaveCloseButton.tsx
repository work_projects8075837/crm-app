import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';

interface TicketSaveCloseButtonProps {
    className?: string;
    onClick?: () => void;
    isDisabled?: boolean;
}

const TicketSaveCloseButton: React.FC<TicketSaveCloseButtonProps> = ({
    isDisabled = false,
    className = '',
    onClick,
}) => {
    return (
        <button
            disabled={isDisabled}
            onClick={onClick}
            className={`${style.ticketSubmitButton} ${className}`}
        >
            Сохранить и выйти
        </button>
    );
};

export default React.memo(TicketSaveCloseButton);
