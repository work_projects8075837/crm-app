import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';
import Svg from '@/components/Elements/Svg/Svg';

interface TicketSaveButtonProps {
    onClick?: () => void;
    className?: string;
    isLoading?: boolean;
    loadingText?: string;
    isDisabled?: boolean;
}

const TicketSaveButton: React.FC<TicketSaveButtonProps> = ({
    onClick,
    className = '',
    isLoading = false,
    isDisabled = false,
    loadingText = 'Сохранение...',
}) => {
    return (
        <button
            disabled={isDisabled}
            onClick={onClick}
            className={`${style.ticketSubmitButtonPrimary} ${className}`}
        >
            {isLoading ? (
                <>{loadingText}</>
            ) : (
                <>
                    <Svg
                        symbol='check'
                        className={style.ticketSubmitButtonSvg}
                    />
                    <span>Сохранить</span>
                </>
            )}
        </button>
    );
};

export default React.memo(TicketSaveButton);
