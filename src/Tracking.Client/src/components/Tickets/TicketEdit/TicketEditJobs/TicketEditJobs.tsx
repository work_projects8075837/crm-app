import React from 'react';
import TicketJobs from '../../TicketJobs/TicketJobs';
import TicketsEditPageLayout from '../TicketsEditPageLayout/TicketsEditPageLayout';
import TicketJobTimeModal from '../../TicketJobs/TicketJobTimeModal/TicketJobTimeModal';

const TicketEditJobs: React.FC = () => {
    return (
        <>
            <TicketsEditPageLayout>
                <TicketJobs />
                <TicketJobTimeModal />
            </TicketsEditPageLayout>
        </>
    );
};

export default TicketEditJobs;
