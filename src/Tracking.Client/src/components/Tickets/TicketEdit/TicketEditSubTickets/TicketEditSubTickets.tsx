import BlueLink from '@/components/Elements/BlueLink/BlueLink';
import { StyleBlueButton } from '@/style/elements/elements';
import React from 'react';
import { useParams } from 'react-router-dom';
import { StyleHeader } from '../../../../style/layout/layout';
import TicketsList from '../../TicketsList/TicketsList';
import TicketsEditPageLayout from '../TicketsEditPageLayout/TicketsEditPageLayout';

const TicketEditSubTickets: React.FC = () => {
    const { ticketId } = useParams();
    return (
        <>
            <TicketsEditPageLayout
                headerButton={
                    <>
                        <div className={StyleHeader.headerChildrenContainerEnd}>
                            <div></div>
                            <div className={StyleBlueButton.blockStandardWidth}>
                                <BlueLink
                                    to={`/ticket/${ticketId}/sub_tickets/create/`}
                                >
                                    Добавить подзаявку
                                </BlueLink>
                            </div>
                        </div>
                    </>
                }
            >
                <TicketsList />
            </TicketsEditPageLayout>
        </>
    );
};

export default TicketEditSubTickets;
