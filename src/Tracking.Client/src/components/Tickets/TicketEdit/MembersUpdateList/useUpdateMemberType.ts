import { memberApi } from '@/services/member/member';
import { updateTicket } from '@/store/queries/actions/ticket';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useUpdateMemberType = () => {
    const { ticketId } = useParams();
    return useMutation({
        mutationFn: async (data: { managerId?: string; type: string }) => {
            updateTicket((ticket) => {
                return {
                    ...ticket,
                    manager: ticket.manager.map((m) => {
                        if (m.id === data.managerId) {
                            return { ...m, type: data.type };
                        }
                        return m;
                    }),
                };
            }, ticketId);
            if (data.managerId) {
                return await memberApi.update(data.managerId, { ...data });
            }
        },
    });
};
