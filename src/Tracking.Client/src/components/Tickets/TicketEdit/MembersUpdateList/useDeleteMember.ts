import { memberApi } from '@/services/member/member';
import { invalidateKey } from '@/store/queries/actions/base';
import { updateTicket } from '@/store/queries/actions/ticket';
import { GET_ONE_TICKET_BY_ID_KEY } from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useDeleteMember = () => {
    const { ticketId } = useParams();
    return useMutation({
        mutationFn: async (managerId?: string) => {
            if (managerId) {
                updateTicket((currentTicket) => {
                    return {
                        ...currentTicket,
                        manager: [
                            ...currentTicket.manager.filter(
                                (m) => m.id !== managerId,
                            ),
                        ],
                    };
                }, ticketId);
                return await memberApi.delete(managerId);
            }
        },
        onSuccess: () => {
            invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
        },
    });
};
