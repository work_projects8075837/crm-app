import React from 'react';
import AddMemberButton from '../../MembersList/AddMemberButton';
import MembersList from '../../MembersList/MembersList';
import { useDeleteMember } from './useDeleteMember';
import { useUpdateMemberType } from './useUpdateMemberType';

const MembersUpdateList: React.FC = () => {
    const deleteMember = useDeleteMember();
    const updateMemberType = useUpdateMemberType();
    return (
        <>
            <MembersList
                onDeleteMember={deleteMember.mutate}
                onTypeChangeClick={updateMemberType.mutate}
                addMemberButton={<AddMemberButton />}
            />
        </>
    );
};

export default MembersUpdateList;
