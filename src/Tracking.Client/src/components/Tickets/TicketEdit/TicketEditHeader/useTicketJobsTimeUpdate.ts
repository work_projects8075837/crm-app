import { ticketApi } from '@/services/ticket/ticket';
import { invalidateKey } from '@/store/queries/actions/base';
import { updateTicket } from '@/store/queries/actions/ticket';
import { GET_ONE_TICKET_BY_ID_KEY } from '@/store/queries/keys/keys';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useTicketJobsTimeUpdate = () => {
    const userQ = useCurrentUser();
    const { ticketId } = useParams();
    return useMutation({
        mutationFn: async (jobs_time: number | null) => {
            if (!ticketId) {
                throw Error();
            }
            updateTicket({ jobs_time }, ticketId);
            return ticketApi.update(ticketId, { jobs_time });
        },
        onSuccess: () => {
            invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
        },
    });
};
