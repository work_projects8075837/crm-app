import DurationText from '@/components/CharacterProblems/JobsList/DurationText';
import Dash from '@/components/Elements/Dash/Dash';
import { TICKET_CHARACTER_PROBLEM_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { useFormEntityValue } from '@/components/Fields/hooks/useFormEntityValue';
import { countHours } from '@/components/utils/countHours';
import { ICharacterProblem } from '@/services/character_problem/types';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { TimerState } from '@/store/states/TimerState';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import style from '../../../../style/layout/header.module.scss';
import TicketHeaderLabelLayout from '../../TicketHeaderLabelLayout/TicketHeaderLabelLayout';

export const ticketTimer = new TimerState();

const TicketTimer: React.FC = () => {
    const currentTicket = useTicketByParams();
    const characterProblem = useFormEntityValue<ICharacterProblem>(
        TICKET_CHARACTER_PROBLEM_NAME,
    ).currentEntity;

    useEffect(() => {
        if (!currentTicket.data?.finished_at) ticketTimer.start();
        () => ticketTimer.clear();
    }, []);

    const dateSla =
        characterProblem && currentTicket.data
            ? characterProblem?.sla +
              new Date(
                  currentTicket.data.start_at || currentTicket.data.created_at,
              ).getTime()
            : ticketTimer.nowDate;

    const staleDuration = new Date(dateSla - ticketTimer.nowDate).getTime();

    const isPositive = staleDuration >= 0;

    const staleTime = isPositive
        ? countHours(staleDuration, true)
        : countHours(-staleDuration, true);

    return (
        <>
            <TicketHeaderLabelLayout
                title={isPositive ? 'Остаток времени' : 'Просроченное время'}
            >
                <div className={style.ticketTimer}>
                    {characterProblem ? (
                        <>
                            {!isPositive ? '-' : ''}{' '}
                            <DurationText {...staleTime} />
                        </>
                    ) : (
                        <Dash />
                    )}
                </div>
            </TicketHeaderLabelLayout>
        </>
    );
};

export default observer(TicketTimer);
