import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import TicketsLink from '@/components/Elements/BreadCrumbs/TicketsLink';
import ValueDash from '@/components/Elements/ValueDash/ValueDash';
import HeaderLayout from '@/components/Layouts/HeaderLayout/HeaderLayout';
import AdminGuard from '@/components/Providers/RolesGuard/AdminGuard';
import { formatDate } from '@/components/utils/formatDate';
import { useTicketBreadCrumbs } from '@/store/queries/stores/ticket/useTicketBreadCrumbs';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import React from 'react';
import style from '../../../../style/layout/header.module.scss';
import SlaHeaderDate from '../../SlaHeaderDate/SlaHeaderDate';
import TicketHeaderLabelLayout from '../../TicketHeaderLabelLayout/TicketHeaderLabelLayout';
import TicketDuration from './TicketDuration';
import TicketJobResultsTime from './TicketJobResultsTime';
import TicketTimer from './TicketTimer';
import TicketWorkTime from './TicketWorkTime';

interface TicketEditHeaderProps {
    headerButton?: React.ReactNode;
}

const TicketEditHeader: React.FC<TicketEditHeaderProps> = ({
    headerButton,
}) => {
    const { data } = useTicketBreadCrumbs();
    const currentTicket = useTicketByParams();

    return (
        <HeaderLayout
            className={style.smallHeaderMargin}
            title={`Заявка ${currentTicket.data?.number}`}
            breadcrumbs={
                <>
                    <span>
                        <TicketsLink />{' '}
                        <InnerBreadCrumbs links={data} route='ticket' />
                    </span>
                </>
            }
        >
            <div className={style.headerChildrenContainer}>
                <TicketHeaderLabelLayout title='Дата и время создания'>
                    {formatDate(currentTicket.data?.start_at)}
                </TicketHeaderLabelLayout>

                <SlaHeaderDate
                    fromDate={
                        currentTicket.data?.start_at
                            ? new Date(currentTicket.data.start_at)
                            : undefined
                    }
                />

                {currentTicket.data && !currentTicket.data.finished_at && (
                    <TicketTimer />
                )}

                <TicketHeaderLabelLayout title='Фактическое завершение'>
                    <ValueDash>
                        {formatDate(currentTicket.data?.finished_at)}
                    </ValueDash>
                </TicketHeaderLabelLayout>

                <TicketHeaderLabelLayout title='Эффективное время'>
                    <TicketWorkTime />
                </TicketHeaderLabelLayout>

                <TicketHeaderLabelLayout title='Длительность заявки'>
                    <TicketDuration />
                </TicketHeaderLabelLayout>
                <AdminGuard>
                    <TicketHeaderLabelLayout title='Время по работам'>
                        <TicketJobResultsTime />
                    </TicketHeaderLabelLayout>
                </AdminGuard>
            </div>
            {headerButton}
        </HeaderLayout>
    );
};

export default TicketEditHeader;
