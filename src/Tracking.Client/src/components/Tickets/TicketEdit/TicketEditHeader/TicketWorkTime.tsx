import DurationText from '@/components/CharacterProblems/JobsList/DurationText';
import Dash from '@/components/Elements/Dash/Dash';
import { questionForm } from '@/components/Elements/DialogForm/questionForm';
import DurationInput from '@/components/Fields/DurationInput/DurationInput';
import { countHours } from '@/components/utils/countHours';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import React from 'react';
import style from '../../../../style/layout/header.module.scss';
import { useTicketJobsTimeUpdate } from './useTicketJobsTimeUpdate';

const TicketWorkTime: React.FC = () => {
    const currentTicket = useTicketByParams();
    const { mutateAsync, isLoading } = useTicketJobsTimeUpdate();
    return (
        <>
            {currentTicket.data?.jobs_time ? (
                <button
                    disabled={isLoading}
                    className={style.ticketWorkTime}
                    onClick={async () => {
                        const answer = await questionForm<{
                            jobs_time: number;
                        }>({
                            title: 'Эффективное время',
                            fields: (
                                <>
                                    <DurationInput name={'jobs_time'} />
                                </>
                            ),
                            formProps: {
                                defaultValues: {
                                    jobs_time:
                                        currentTicket.data?.jobs_time || 0,
                                },
                            },
                        });

                        if (answer) {
                            await mutateAsync(answer.jobs_time);
                        }
                    }}
                >
                    <DurationText
                        {...countHours(currentTicket.data?.jobs_time)}
                    />
                </button>
            ) : (
                <Dash />
            )}
        </>
    );
};

export default TicketWorkTime;
