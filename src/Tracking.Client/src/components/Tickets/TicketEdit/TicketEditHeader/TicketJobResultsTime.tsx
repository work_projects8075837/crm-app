import DurationText from '@/components/CharacterProblems/JobsList/DurationText';
import Dash from '@/components/Elements/Dash/Dash';
import { countHours } from '@/components/utils/countHours';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import React from 'react';

const TicketJobResultsTime: React.FC = () => {
    const currentTicket = useTicketByParams();

    const jobResultsTime = currentTicket.data?.job_result.reduce(
        (time: number, jobResult) => (time += jobResult.time),
        0,
    );
    return (
        <>
            {jobResultsTime ? (
                <DurationText {...countHours(jobResultsTime)} />
            ) : (
                <Dash />
            )}
        </>
    );
};

export default TicketJobResultsTime;
