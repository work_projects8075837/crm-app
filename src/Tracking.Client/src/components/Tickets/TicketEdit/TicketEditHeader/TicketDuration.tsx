import DurationText from '@/components/CharacterProblems/JobsList/DurationText';
import Dash from '@/components/Elements/Dash/Dash';
import React from 'react';
import { useTicketByParams } from './../../../../store/queries/stores/ticket/useTicketByParams';
import { countHours } from './../../../utils/countHours';

const TicketDuration: React.FC = () => {
    const currentTicket = useTicketByParams();

    const durationTicket = countHours(
        new Date(currentTicket.data?.finished_at || 0).getTime() -
            new Date(currentTicket.data?.created_at || 0).getTime(),
    );

    return (
        <>
            {currentTicket.data?.finished_at &&
            (durationTicket.hours || durationTicket.minutes) ? (
                <DurationText {...durationTicket} />
            ) : (
                <Dash />
            )}
        </>
    );
};

export default TicketDuration;
