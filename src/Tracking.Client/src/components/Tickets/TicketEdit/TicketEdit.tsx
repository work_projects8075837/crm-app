import FormSelect from '@/components/Elements/FormSelect/FormSelect';
import EditorInput from '@/components/Fields/EditorInput/EditorInput';
import { TICKET_DECISION_NAME } from '@/components/Fields/EntitiesFields/ticket';
import React from 'react';
import GeneralInfo from '../TicketCreate/GeneralInfo/GeneralInfo';
import MembersUpdateList from './MembersUpdateList/MembersUpdateList';
import TicketsEditPageLayout from './TicketsEditPageLayout/TicketsEditPageLayout';

const TicketEdit: React.FC = () => {
    return (
        <>
            <TicketsEditPageLayout>
                <GeneralInfo
                    membersList={<MembersUpdateList />}
                    decisionSection={
                        <FormSelect title='Решение'>
                            <EditorInput name={TICKET_DECISION_NAME} />
                        </FormSelect>
                    }
                />
            </TicketsEditPageLayout>
        </>
    );
};

export default TicketEdit;
