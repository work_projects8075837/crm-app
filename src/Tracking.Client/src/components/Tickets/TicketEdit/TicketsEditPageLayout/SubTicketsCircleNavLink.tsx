import CircleNavLink from '@/components/Elements/CircleNavLink/CircleNavLink';
import { useTicketList } from '@/services/ticket/ticket';
import { StyleTab } from '@/style/elements/elements';
import React from 'react';
import { useParams } from 'react-router-dom';

const SubTicketsCircleNavLink: React.FC = () => {
    const { ticketId } = useParams();
    const { data } = useTicketList({ ticket_ticket_id: ticketId });
    return (
        <>
            <CircleNavLink
                to={`/ticket/${ticketId}/sub_tickets`}
                text={
                    <>
                        <span>Подзаявки</span>
                        {!!data?.data.filter((t) => !t.status?.is_finish)
                            .length && <span className={StyleTab.circle} />}
                    </>
                }
            />
        </>
    );
};

export default SubTicketsCircleNavLink;
