import CircleLink from '@/components/Elements/CircleLink/CircleLink';
import CircleNavLink from '@/components/Elements/CircleNavLink/CircleNavLink';
import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';
import TicketEditFormProvider from '../../Providers/TicketEditFormProvider/TicketEditFormProvider';
import TicketOptions from '../../TicketOptions/TicketOptions';
import TicketEditHeader from '../TicketEditHeader/TicketEditHeader';

import SelectedActions from '@/components/Elements/SelectedActions/SelectedActions';
import AdminGuard from '@/components/Providers/RolesGuard/AdminGuard';
import { useParams } from 'react-router-dom';
import AddContactModal from '../../AddContactModal/AddContactModal';
import CaseMenu from '../../CaseMenu/CaseMenu';
import TicketHistoryModal from '../../History/TicketHistoryModal/TicketHistoryModal';
import MembersAddModal from '../../MembersAddModal/MembersAddModal';
import AddTagsModal from '../../TicketCreate/AddTagsModal/AddTagsModal';
import TicketFilesModal from '../../TicketFilesModal/TicketFilesModal';
import PerformSelectedJobsButton from '../../TicketJobs/TicketJobItem/PerformSelectedJobsButton';
import { jobCheckHandler } from '../../TicketJobs/TicketJobs';
import SubTicketsCircleNavLink from './SubTicketsCircleNavLink';

interface TicketsEditPageLayoutProps {
    children: React.ReactNode;
    headerButton?: React.ReactNode;
    formBottom?: React.ReactNode;
}

const TicketsEditPageLayout: React.FC<TicketsEditPageLayoutProps> = ({
    children,
    headerButton,
    formBottom,
}) => {
    const { ticketId } = useParams();
    return (
        <div>
            <TicketEditFormProvider
                formBottom={formBottom}
                modals={
                    <>
                        <TicketFilesModal />
                        <AddContactModal />
                        <AddTagsModal />
                        <MembersAddModal />

                        <SelectedActions checkHandler={jobCheckHandler}>
                            <PerformSelectedJobsButton />
                        </SelectedActions>
                        <TicketHistoryModal />
                        <CaseMenu />
                    </>
                }
            >
                <div className={style.ticketCreateChildren}>
                    <TicketEditHeader headerButton={headerButton} />
                    <TicketOptions />
                    <div className={style.ticketTabsButtons}>
                        <CircleNavLink
                            to={`/ticket/${ticketId}/`}
                            text='Общая информация'
                        />

                        <CircleNavLink
                            to={`/ticket/${ticketId}/comments`}
                            text='Комментарии'
                        />

                        <CircleLink to='https://flagman-it.bitrix24.ru/'>
                            Внешние заявки
                        </CircleLink>

                        <SubTicketsCircleNavLink />

                        <CircleNavLink
                            to={`/ticket/${ticketId}/jobs`}
                            text='Учёт работ'
                        />
                        <AdminGuard>
                            <CircleNavLink
                                to={`/ticket/${ticketId}/results`}
                                text='Статистика работ'
                            />
                        </AdminGuard>
                    </div>
                    <div className={style.afterTabsMargin}>{children}</div>
                </div>
            </TicketEditFormProvider>
        </div>
    );
};

export default TicketsEditPageLayout;
