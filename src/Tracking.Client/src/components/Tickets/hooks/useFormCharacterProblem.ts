import { TICKET_CHARACTER_PROBLEM_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { useFormEntityValue } from '@/components/Fields/hooks/useFormEntityValue';
import { ICharacterProblem } from '@/services/character_problem/types';

export const useFormCharacterProblem = () => {
    const formCharacterProblem = useFormEntityValue<ICharacterProblem>(
        TICKET_CHARACTER_PROBLEM_NAME,
    );
    return formCharacterProblem.currentEntity;
};
