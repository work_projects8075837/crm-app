import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';

export const useMemberByCurrentUser = () => {
    const currentUser = useCurrentUser();
    const currentTicket = useTicketByParams();

    const memberByCurrentUser = currentTicket.data?.manager.find(
        (member) => member.user.id === currentUser.data?.id,
    );

    return memberByCurrentUser;
};
