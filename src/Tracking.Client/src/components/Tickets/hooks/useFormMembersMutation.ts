import { IMember } from '@/services/member/types';
import { useFormContext } from 'react-hook-form';
import { useFormMembers } from './useFormMembers';

export const useFormMembersMutation = () => {
    const members = useFormMembers();
    const { setValue } = useFormContext();

    const addMember = (member: IMember) => {};
};
