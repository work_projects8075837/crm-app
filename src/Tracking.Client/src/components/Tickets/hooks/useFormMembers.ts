import {
    TICKET_MEMBERS_NAME,
    TICKET_MEMBERS_TYPES,
} from '@/components/Fields/EntitiesFields/ticket';
import { useCallback } from 'react';
import { useFormContext, useWatch } from 'react-hook-form';
import { toast } from 'react-hot-toast';

export interface IMemberUser {
    id: string;
    name: string;
    phone?: string;
}

export interface IShortMember {
    id?: string;
    type: string;
    user: IMemberUser;
}

export const useFormMembers = () => {
    const { setValue, getValues } = useFormContext();
    // const membersJsonArray: string[] = useWatch({ name: TICKET_MEMBERS_NAME });
    const members: IShortMember[] | undefined = useWatch({
        name: TICKET_MEMBERS_NAME,
    });

    const remove = useCallback((userId: string) => {
        const oldMembers: IShortMember[] | undefined =
            getValues(TICKET_MEMBERS_NAME);
        if (!oldMembers) return;
        const newMembers = oldMembers.filter(
            (member) => member.user.id !== userId,
        );
        setValue(TICKET_MEMBERS_NAME, newMembers);
    }, []);

    const add = useCallback((user: IMemberUser) => {
        const oldMembers: IShortMember[] | undefined =
            getValues(TICKET_MEMBERS_NAME);

        setValue(TICKET_MEMBERS_NAME, [
            ...(oldMembers || []),
            {
                type: TICKET_MEMBERS_TYPES.responsible,
                user,
            },
        ]);
    }, []);

    const toggleType = useCallback(
        (userId: string) => {
            const oldMembers: IShortMember[] | undefined =
                getValues(TICKET_MEMBERS_NAME);
            if (!oldMembers) return;

            const newMembers = oldMembers.map((member) => {
                if (member.user.id === userId) {
                    const newType =
                        member.type === TICKET_MEMBERS_TYPES.involved
                            ? TICKET_MEMBERS_TYPES.responsible
                            : TICKET_MEMBERS_TYPES.involved;

                    if (
                        newType === TICKET_MEMBERS_TYPES.involved &&
                        !oldMembers.some(
                            (member) =>
                                member.type ===
                                TICKET_MEMBERS_TYPES.responsible,
                        )
                    ) {
                        toast.error(
                            'В заявке должен быть миниму один ответственный!',
                        );
                        return member;
                    }
                    return {
                        ...member,
                        type: newType,
                    };
                } else {
                    return member;
                }
            });
            const newMembersJsonArray = newMembers.map((member) =>
                JSON.stringify(member),
            );
            setValue(TICKET_MEMBERS_NAME, newMembers);
        },
        [members],
    );

    return { members, remove, add, toggleType };
};
