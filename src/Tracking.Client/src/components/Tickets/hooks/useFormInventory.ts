import { TICKET_SALE_POINT_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { useFormEntityValue } from '@/components/Fields/hooks/useFormEntityValue';
import { useInventoriesBySalePointId } from '@/store/queries/stores/inventory/useInventoriesBySalePointId';
import { useMutation } from '@tanstack/react-query';
import { useWatch } from 'react-hook-form';

export const useFormInventory = () => {
    const formSalePoint = useFormEntityValue(TICKET_SALE_POINT_NAME);
    const salePointId = formSalePoint.currentEntity?.id;
    const query = useInventoriesBySalePointId(salePointId);

    return query;
};
