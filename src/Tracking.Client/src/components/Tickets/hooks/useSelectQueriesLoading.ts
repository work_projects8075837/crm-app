import { channelHooks } from '@/services/channel/channel';
import { characterProblemHooks } from '@/services/character_problem/character_problem';
import { contactHooks } from '@/services/contact/contact';
import { directionHooks } from '@/services/direction/direction';
import { salePointHooks } from '@/services/sale_point/sale_point';
import { tagHooks } from '@/services/tag/tag';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { useSearchUsers } from '@/store/queries/stores/user/useSearchUsers';

export const useSelectQueriesLoading = () => {
    const channels = channelHooks.useList(
        { page: 1, offset: 100 },
        { refetchOnMount: true },
    );
    const directions = directionHooks.useList(
        { page: 1, offset: 100 },
        { refetchOnMount: true },
    );
    const characterProblems = characterProblemHooks.useList(
        { page: 1, offset: 100 },
        { refetchOnMount: true },
    );
    const salePoints = salePointHooks.useList(
        { page: 1, offset: 100 },
        {
            refetchOnMount: true,
        },
    );
    const contact = contactHooks.useList({}, { refetchOnMount: true });
    const tags = tagHooks.useSearch();
    const users = useSearchUsers({ search: '' });
    const currentUser = useCurrentUser();

    return (
        channels.isLoading ||
        directions.isLoading ||
        characterProblems.isLoading ||
        salePoints.isLoading ||
        tags.isLoading ||
        users.isLoading ||
        contact.isLoading ||
        currentUser.isLoading
    );
};
