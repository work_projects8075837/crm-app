import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import { resultJobModalState } from '@/store/states/ResultJobModalState.ts';
import React, { useEffect } from 'react';
import style from '../../../../style/layout/modal_window.module.scss';
import CreateJobResultForm from '@/components/Form/CreateJobResultForm/CreateJobResultForm';
import { observer } from 'mobx-react-lite';
const TicketJobTimeModal: React.FC = () => {
    useEffect(() => {
        return () => {
            resultJobModalState.clear();
        };
    }, []);
    return (
        <>
            <ModalWindow
                layoutClass={style.wideLayout}
                title={`"${resultJobModalState.jobName}" время`}
                openHandler={resultJobModalState.openHandler}
            >
                <CreateJobResultForm />
            </ModalWindow>
        </>
    );
};

export default observer(TicketJobTimeModal);
