import TextLoader from '@/components/Elements/TextLoader/TextLoader';
import { IJob } from '@/services/job/types';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import React from 'react';
import TicketJobItem from './TicketJobItem/TicketJobItem';
import TicketJobsLayout from './TicketJobsLayout';

export type TJobPerform = Omit<Omit<IJob, 'character_problem'>, 'confirm'>;

export const jobCheckHandler = new CheckEntitiesHandler<TJobPerform>();

const TicketJobs: React.FC = () => {
    const { data } = useTicketByParams();

    useSetCheckableEntities(jobCheckHandler, data?.character_problem?.job);

    return (
        <>
            {data?.character_problem?.job.length ? (
                <TicketJobsLayout checkHandler={jobCheckHandler}>
                    {data?.character_problem.job.map((job) => {
                        return (
                            <TicketJobItem
                                checkHandler={jobCheckHandler}
                                key={job.id}
                                id={job.id}
                                duration={job.duration}
                                name={job.name}
                            />
                        );
                    })}
                </TicketJobsLayout>
            ) : (
                <TextLoader text='По данной заявке нет работ' />
            )}
        </>
    );
};

export default TicketJobs;
