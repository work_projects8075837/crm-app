import DurationText from '@/components/CharacterProblems/JobsList/DurationText';
import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import LinkTooltip from '@/components/SideBar/Links/LinkTooltip/LinkTooltip';
import { countHours } from '@/components/utils/countHours';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import React from 'react';
import style from '../../../../style/layout/table.module.scss';
import { useMemberByCurrentUser } from '../../hooks/useMemberByCurrentUser';
import TicketJobPerformButton from './TicketJobPerformButton';
import { useChangeJobResultTime } from './hooks/useChangeJobResultTime';
import { useChangeResultsCount } from './hooks/useChangeResultsCount';

interface TicketJobItemProps {
    checkHandler: CheckEntitiesHandler;
    id: string;
    name: string;
    duration?: number;
}

const TicketJobItem: React.FC<TicketJobItemProps> = ({
    name,
    id,
    duration,
    checkHandler,
}) => {
    const manager = useMemberByCurrentUser();

    const {
        myJobResultsCount,
        isLoading,
        currentJobResults,
        ...changeResultCount
    } = useChangeResultsCount({ id, duration }, manager?.id);

    const currentJobResult = currentJobResults?.find(
        (jr) => jr.job?.id === id && jr.manager?.id === manager?.id,
    );

    const changeJobResultTime = useChangeJobResultTime(currentJobResult);

    const myJobResultsCountText = 'Изменить количество';

    return (
        <div className={style.ticketJobItem}>
            <CheckItem
                checkHandler={checkHandler}
                entity={{ id, name, duration }}
            />
            <div className={style.ticketJobItemName}>
                <span>{name}</span>
            </div>
            <div className={style.ticketJobItemName}>
                {duration ? (
                    <span>
                        <DurationText {...countHours(duration)} />
                    </span>
                ) : (
                    <>
                        {currentJobResult && (
                            <button
                                disabled={changeJobResultTime.isLoading}
                                className={style.jobResultUserCount}
                                data-tooltip-id={`link-tooltip_${'Изменить время'}`}
                                data-tooltip-content={'Изменить время'}
                                onClick={() => {
                                    changeJobResultTime.change();
                                }}
                            >
                                <DurationText
                                    {...countHours(currentJobResult?.time)}
                                />
                                <LinkTooltip alt={'Изменить время'} />
                            </button>
                        )}
                    </>
                )}
            </div>
            <div className={style.ticketJobItemName}>
                <span>{currentJobResults?.length}</span>
            </div>
            <div className={style.ticketJobItemName}>
                {duration ? (
                    <button
                        disabled={isLoading}
                        className={style.jobResultUserCount}
                        data-tooltip-id={`link-tooltip_${myJobResultsCountText}`}
                        data-tooltip-content={myJobResultsCountText}
                        onClick={() => {
                            changeResultCount.change(name);
                        }}
                    >
                        {myJobResultsCount}
                        <LinkTooltip alt={myJobResultsCountText} />
                    </button>
                ) : (
                    <span>{myJobResultsCount}</span>
                )}
            </div>
            <div className={style.ticketJobItemField}>
                <TicketJobPerformButton
                    countLoading={isLoading || changeJobResultTime.isLoading}
                    jobId={id}
                    time={duration}
                    name={name}
                />
            </div>
        </div>
    );
};

export default TicketJobItem;
