import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';
import { jobCheckHandler } from '../TicketJobs';
import JobButton from './JobButton';
import { usePerformSelectedJobs } from './hooks/usePerformSelectedJobs';

const PerformSelectedJobsButton: React.FC = () => {
    const { onPerform, isLoading } = usePerformSelectedJobs();
    return (
        <>
            <JobButton
                className={style.performSelectedJobsButton}
                disabled={!jobCheckHandler.checkedEntities.length || isLoading}
                onClick={onPerform}
            >
                {isLoading ? (
                    <LoaderCircle
                        size={17}
                        borderWidth={1}
                        className='p0'
                        color='#fff'
                    />
                ) : (
                    'Выполнить выбранные'
                )}
            </JobButton>
        </>
    );
};

export default observer(PerformSelectedJobsButton);
