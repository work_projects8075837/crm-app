import React, { ButtonHTMLAttributes } from 'react';
import style from '../../../../style/layout/table.module.scss';

interface JobButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    isPerformed?: boolean;
}

const JobButton: React.FC<JobButtonProps> = ({
    isPerformed,
    children,
    className = '',
    ...buttonProps
}) => {
    return (
        <>
            <button
                className={`${style.ticketJobItemButton} ${
                    isPerformed ? style.ticketJobItemButtonSelected : ''
                } ${className}`}
                {...buttonProps}
            >
                {children}
            </button>
        </>
    );
};

export default JobButton;
