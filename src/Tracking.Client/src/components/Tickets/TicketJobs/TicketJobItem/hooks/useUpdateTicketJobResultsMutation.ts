import { IJobResult } from '@/services/result_job/types';
import { ticketApi } from '@/services/ticket/ticket';
import { updateTicket } from '@/store/queries/actions/ticket';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useMutation } from '@tanstack/react-query';

export const useUpdateTicketJobResultsMutation = () => {
    const currentTicket = useTicketByParams();

    const mutation = useMutation({
        mutationFn: async (mutationData: { job_results: IJobResult[] }) => {
            if (!currentTicket.data) {
                throw Error();
            }

            const newJobResults = [
                ...currentTicket.data.job_result,
                ...mutationData.job_results,
            ];

            const newTicketData = await ticketApi
                .update(currentTicket.data.id, {
                    job_result: newJobResults,
                })
                .then((res) => res.data);

            updateTicket({ job_result: newJobResults }, currentTicket.data?.id);

            return newTicketData;
        },
    });

    return mutation;
};
