import { jobResultApi } from '@/services/result_job/result_job';
import { useMutation } from '@tanstack/react-query';
import { JobResultMutationData } from './useJobResultToTicketMutation';

interface CreateJobMutationData extends JobResultMutationData {
    manager_id: string;
}

export const useCreateJobResult = () => {
    const mutation = useMutation({
        mutationFn: async (data: CreateJobMutationData) => {
            const newResultJob = await jobResultApi
                .create(data)
                .then((res) => res.data);

            return newResultJob;
        },
    });

    return mutation;
};
