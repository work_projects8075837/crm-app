import { confirmAction } from '@/components/Elements/ConfirmDialogWindow/confirmWindow';
import { questionForm } from '@/components/Elements/DialogForm/questionForm';
import DurationInput from '@/components/Fields/DurationInput/DurationInput';
import { useMemberByCurrentUser } from '@/components/Tickets/hooks/useMemberByCurrentUser';
import { IJobResult } from '@/services/result_job/types';
import { queryClient } from '@/store/queries/client/client';
import { GET_ONE_TICKET_BY_ID_KEY } from '@/store/queries/keys/keys';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { toast } from 'react-hot-toast';
import { jobCheckHandler } from '../../TicketJobs';
import { useCreateJobResult } from './useCreateJobResult';
import { getMemberId } from './useJobResultToTicketMutation';
import { useUpdateTicketJobResultsMutation } from './useUpdateTicketJobResultsMutation';

export const usePerformSelectedJobs = () => {
    const currentMember = useMemberByCurrentUser();
    const currentUser = useCurrentUser();
    const { mutateAsync, isLoading } = useCreateJobResult();
    const { data } = useTicketByParams();
    const ticketJobResultMutation = useUpdateTicketJobResultsMutation();

    const isSomeCompleted = data?.job_result.some((jobResult) =>
        jobCheckHandler.checkedEntities.some(
            (job) => job.id === jobResult.job?.id,
        ),
    );
    const onPerform = async () => {
        const newJobResults: IJobResult[] = [];

        let isAllow = true;
        if (isSomeCompleted) {
            isAllow = await confirmAction({
                text: 'Среди выбранных задач есть те что уже выполнены. Вы хотите выполнить их повторно?',
            });
        }

        if (isAllow) {
            const manager_id = await getMemberId(
                currentMember,
                data,
                currentUser.data,
            );

            for await (const job of jobCheckHandler.checkedEntities) {
                if (job.duration) {
                    const data = await mutateAsync({
                        job_id: job.id,
                        time: job.duration,
                        manager_id,
                    });
                    newJobResults.push(data);
                    toast.success(`Задача "${job.name}" выполнена`);
                } else {
                    if (!data?.job_result.some((jr) => jr.job?.id === job.id)) {
                        const timeForm = await questionForm<{ time: number }>({
                            title: `"${job.name}" время`,
                            fields: (
                                <>
                                    <DurationInput name='time' />
                                </>
                            ),
                        });
                        if (timeForm) {
                            const data = await mutateAsync({
                                job_id: job.id,
                                time: timeForm.time,
                                manager_id,
                            });
                            newJobResults.push(data);
                            toast.success(`Задача "${job.name}" выполнена`);
                        } else {
                            toast.error(
                                `Выполнение задачи "${job.name}" отклонено`,
                            );
                        }
                    } else {
                        toast.error(
                            `Задачу с редактированием времени можно выполнить только один  раз!`,
                        );
                    }
                }
            }

            ticketJobResultMutation.mutate(
                { job_results: newJobResults },
                {
                    onSuccess: () => {
                        queryClient.invalidateQueries({
                            queryKey: [GET_ONE_TICKET_BY_ID_KEY],
                        });
                    },
                },
            );
        }
    };

    return { onPerform, isLoading };
};
