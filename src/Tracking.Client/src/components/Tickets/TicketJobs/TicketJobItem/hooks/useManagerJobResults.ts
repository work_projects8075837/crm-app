import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';

export const useManagerJobResults = (jobId?: string, managerId?: string) => {
    const ticketQuery = useTicketByParams();
    const ticketData = ticketQuery.data;
    const currentJobResults = ticketData?.job_result.filter(
        (jr) => jr.job?.id === jobId,
    );

    const myJobResults = currentJobResults?.filter(
        (jr) => managerId && jr.manager?.id === managerId,
    );
    const myJobResultsCount = myJobResults?.length;

    return { ticketData, currentJobResults, myJobResults, myJobResultsCount };
};
