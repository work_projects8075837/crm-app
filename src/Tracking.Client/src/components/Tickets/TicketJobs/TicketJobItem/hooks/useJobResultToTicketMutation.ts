import { TICKET_MEMBERS_TYPES } from '@/components/Fields/EntitiesFields/ticket';
import { memberApi } from '@/services/member/member';
import { IMember } from '@/services/member/types';
import { jobResultApi } from '@/services/result_job/result_job';
import { ticketApi } from '@/services/ticket/ticket';
import { ITicket } from '@/services/ticket/types';
import { IUser } from '@/services/user/types';
import { updateTicket } from '@/store/queries/actions/ticket';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { useMutation } from '@tanstack/react-query';
import { useMemberByCurrentUser } from '../../../hooks/useMemberByCurrentUser';

export interface JobResultMutationData {
    time: number;
    job_id: string;
}

export const getMemberId = async (
    currentMember?: IMember,
    currentTicketData?: ITicket,
    currentUserData?: IUser,
) => {
    if (!currentUserData || !currentTicketData) {
        throw Error();
    }
    const memberId = currentMember?.id
        ? currentMember?.id
        : await memberApi
              .create({
                  type: TICKET_MEMBERS_TYPES.involved,
                  user_id: currentUserData?.id,
                  ticket_id: currentTicketData.id,
              })
              .then(async ({ data }) => {
                  return data.id;
              });

    return memberId;
};

export const useJobResultToTicketMutation = () => {
    const currentTicket = useTicketByParams();
    const currentMember = useMemberByCurrentUser();
    const currentUser = useCurrentUser();

    const mutation = useMutation({
        mutationFn: async (data: JobResultMutationData) => {
            if (!currentTicket.data) {
                throw Error();
            }

            const memberId = await getMemberId(
                currentMember,
                currentTicket.data,
                currentUser.data,
            );

            const newResultJob = await jobResultApi.create({
                manager_id: memberId,
                ...data,
            });

            const newTicketData = await ticketApi
                .update(currentTicket.data.id, {
                    job_result: [
                        ...currentTicket.data.job_result,
                        newResultJob.data,
                    ],
                })
                .then((res) => res.data);

            updateTicket(
                { job_result: newTicketData.job_result },
                currentTicket.data?.id,
            );

            return newTicketData;
        },
    });

    return mutation;
};
