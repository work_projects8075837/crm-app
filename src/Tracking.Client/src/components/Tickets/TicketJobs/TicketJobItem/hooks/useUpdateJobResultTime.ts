import { jobResultApi } from '@/services/result_job/result_job';
import { useMutation } from '@tanstack/react-query';

interface UseUpdateJobResultTimeMutation {
    id: string;
    time: number;
}

export const useUpdateJobResultTime = () => {
    return useMutation({
        mutationFn: async ({ id, time }: UseUpdateJobResultTimeMutation) => {
            return jobResultApi.update(id, { time }).then((res) => res.data);
        },
    });
};
