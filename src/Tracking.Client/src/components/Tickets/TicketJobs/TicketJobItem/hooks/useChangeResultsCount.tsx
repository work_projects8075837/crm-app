import { questionForm } from '@/components/Elements/DialogForm/questionForm';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import { IJob } from '@/services/job/types';
import { jobResultApi } from '@/services/result_job/result_job';
import { IJobResult } from '@/services/result_job/types';
import { invalidateKey } from '@/store/queries/actions/base';
import { updateTicket } from '@/store/queries/actions/ticket';
import { GET_ONE_TICKET_BY_ID_KEY } from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';
import { useManagerJobResults } from './useManagerJobResults';
import { useUpdateTicketJobResultsMutation } from './useUpdateTicketJobResultsMutation';

export const useChangeResultsCount = (
    job?: Pick<IJob, 'id' | 'duration'> | null,
    managerId?: string,
) => {
    const jobId = job?.id;

    const { myJobResults, myJobResultsCount, ticketData, currentJobResults } =
        useManagerJobResults(jobId, managerId);

    const updateTicketJobResult = useUpdateTicketJobResultsMutation();
    const m = useMutation({
        mutationFn: async (countData?: { count: number }) => {
            if (
                countData &&
                myJobResults &&
                myJobResultsCount &&
                ticketData &&
                managerId &&
                jobId
            ) {
                const { count } = countData;

                const changeCount = myJobResultsCount - count;

                if (changeCount < 0) {
                    const time = job?.duration;
                    if (time) {
                        const newJobResults: IJobResult[] = [];

                        await Promise.allSettled(
                            Array.from(Array(changeCount * -1).keys()).map(
                                async () => {
                                    return await jobResultApi
                                        .create({
                                            time,
                                            job_id: jobId,
                                            manager_id: managerId,
                                        })
                                        .then((res) => res.data);
                                },
                            ),
                        ).then((results) => {
                            results.forEach((result) => {
                                if (result.status === 'fulfilled') {
                                    newJobResults.push(result.value);
                                }
                            });
                        });

                        await updateTicketJobResult.mutateAsync(
                            {
                                job_results: newJobResults,
                            },
                            {
                                onSuccess: () => {
                                    invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
                                },
                            },
                        );
                    }

                    return;
                } else {
                    const deleteJobResults = myJobResults?.slice(
                        0,
                        changeCount,
                    );

                    await jobResultApi.deleteArray(
                        deleteJobResults.map((j) => j.id),
                    );

                    updateTicket(
                        {
                            job_result: ticketData.job_result.filter(
                                (jb) =>
                                    !deleteJobResults.some(
                                        (delJb) => delJb.id === jb.id,
                                    ),
                            ),
                        },
                        ticketData.id,
                    );
                }

                return { count: 0 };
            }
        },
        onSuccess: () => {
            invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
        },
    });

    const change = async (name: string) => {
        await questionForm<{ count: number }>({
            title: `Количество выполнений работы ${name}`,
            fields: (
                <>
                    <FormInput
                        name={'count'}
                        placeholder='Количество выполнений'
                        options={{ valueAsNumber: true }}
                        type='number'
                    />
                </>
            ),
            formProps: { defaultValues: { count: myJobResultsCount } },
            answerFn: async (v) => m.mutate(v),
        });
    };

    return { myJobResultsCount, currentJobResults, ticketData, change, ...m };
};
