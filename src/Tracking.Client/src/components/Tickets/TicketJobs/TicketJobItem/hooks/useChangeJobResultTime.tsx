import { questionForm } from '@/components/Elements/DialogForm/questionForm';
import DurationInput from '@/components/Fields/DurationInput/DurationInput';
import { IJobResult } from '@/services/result_job/types';
import { invalidateKey } from '@/store/queries/actions/base';
import { updateTicket } from '@/store/queries/actions/ticket';
import { GET_ONE_TICKET_BY_ID_KEY } from '@/store/queries/keys/keys';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useUpdateJobResultTime } from './useUpdateJobResultTime';

export const useChangeJobResultTime = (jobResult?: IJobResult) => {
    const ticketData = useTicketByParams().data;
    const { mutate, isLoading } = useUpdateJobResultTime();

    const change = async () => {
        const timeForm = await questionForm<{ time: number }>({
            title: 'Новое время',
            fields: (
                <>
                    <DurationInput name='time' required={true} />
                </>
            ),
            formProps: { defaultValues: { time: jobResult?.time } },
        });

        if (timeForm && jobResult?.id) {
            const { time } = timeForm;
            mutate(
                { id: jobResult.id, time },
                {
                    onSuccess: () => {
                        updateTicket(
                            (ticket) => {
                                return {
                                    ...ticket,
                                    job_result: ticket.job_result.map((jr) => {
                                        if (jr.id === jobResult.id) {
                                            return { ...jr, time };
                                        }
                                        return jr;
                                    }),
                                };
                            },
                            ticketData?.id,
                        );
                        invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
                    },
                },
            );
        }
    };

    return { ticketData, change, isLoading };
};
