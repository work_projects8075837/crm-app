import { confirmAction } from '@/components/Elements/ConfirmDialogWindow/confirmWindow';
import { ITicketForm } from '@/components/Fields/EntitiesFields/ticket';
import { useJobResultToTicketMutation } from '@/components/Tickets/TicketJobs/TicketJobItem/hooks/useJobResultToTicketMutation';
import { queryClient } from '@/store/queries/client/client';
import { GET_ONE_TICKET_BY_ID_KEY } from '@/store/queries/keys/keys';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { resultJobModalState } from '@/store/states/ResultJobModalState.ts';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { toast } from 'react-hot-toast';
import { formatTicketFormDefaultValues } from '../../Providers/TicketEditFormProvider/formatTicketFormDefaultValues';
import JobButton from './JobButton';

interface TicketJobPerformButtonProps {
    jobId: string;
    time?: number;
    name: string;
    countLoading?: boolean;
}

const TicketJobPerformButton: React.FC<TicketJobPerformButtonProps> = ({
    jobId,
    time,
    name,
    countLoading = false,
}) => {
    const currentTicket = useTicketByParams();
    const isPerformed = currentTicket.data?.job_result.some(
        (jobResult) => jobResult.job?.id === jobId,
    );

    const { mutate, isLoading } = useJobResultToTicketMutation();

    const { reset } = useFormContext<ITicketForm>();

    return (
        <>
            <div className=''></div>
            <JobButton
                isPerformed={isPerformed}
                disabled={isLoading || countLoading}
                onClick={async () => {
                    let isContinue = true;
                    if (isPerformed) {
                        if (!time) {
                            toast.error(
                                'Задачу с редактированием времени можно выполнить только один  раз!',
                            );
                            return;
                        }

                        isContinue = await confirmAction({
                            text: 'Это задача уже была выполнена. Вы хотите выполнить её повторно?',
                        });
                    }
                    if (!isContinue) return;
                    if (!time) {
                        resultJobModalState.setTicketJob(jobId, name);
                        resultJobModalState.openHandler.open();
                    } else {
                        mutate(
                            { job_id: jobId, time },
                            {
                                onSuccess: (data) => {
                                    queryClient.invalidateQueries({
                                        queryKey: [GET_ONE_TICKET_BY_ID_KEY],
                                    });
                                    reset(formatTicketFormDefaultValues(data));

                                    toast.success(`Задача "${name}" выполнена`);
                                },
                            },
                        );
                    }
                }}
            >
                {isLoading || countLoading ? 'Загрузка...' : 'Выполнить'}
            </JobButton>
        </>
    );
};

export default TicketJobPerformButton;
