import EmptyPagination from '@/components/Tables/EmptyPagination/EmptyPagination';
import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import React from 'react';
import style from '../../../style/layout/table.module.scss';

interface TicketJobsLayoutProps {
    children: React.ReactNode;
    checkHandler: CheckEntitiesHandler;
}

const TicketJobsLayout: React.FC<TicketJobsLayoutProps> = ({
    children,
    checkHandler,
}) => {
    return (
        <>
            <TableLayout
                layoutClass={style.ticketJobsTableLayout}
                header={
                    <TableHeader
                        checkHandler={checkHandler}
                        fields={[
                            { name: 'Название' },
                            { name: 'Время' },
                            { name: 'Всего выполнено' },
                            { name: 'Я выполнил' },
                        ]}
                        layoutClass={`${style.ticketJobsTableRowLayout}`}
                        fieldClass={style.ticketJobHeaderField}
                    ></TableHeader>
                }
                pagination={<EmptyPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default TicketJobsLayout;
