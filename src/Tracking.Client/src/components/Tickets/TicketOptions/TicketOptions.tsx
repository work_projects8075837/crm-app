import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import { ticketCaseMenu } from '../CaseMenu/CaseMenu';
import { ticketHistoryModalHandler } from '../History/TicketHistoryModal/TicketHistoryModal';
import StatusSelect from '../StatusSelect/StatusSelect';
import TicketUpdateActionsMenu, {
    ticketUpdateActionsHandler,
} from '../TicketActionsMenu/TicketUpdateActionsMenu';
import TicketOptionsLayout from './EntityOptionsLayout';
import TicketOptionButton from './TicketOptionButton';

export const ticketFilesOpenHandler = new OpenHandler(false);

const TicketOptions: React.FC = () => {
    return (
        <>
            <TicketOptionsLayout otherChildren={<StatusSelect />}>
                <div className='rowStartEnd'>
                    <TicketOptionButton
                        onClick={() => ticketUpdateActionsHandler.toggle()}
                        symbol='ticket_edit'
                        text='Действия'
                    />
                    <TicketUpdateActionsMenu />
                </div>

                <TicketOptionButton
                    onClick={() => print()}
                    symbol='ticket_print'
                    text='Печать'
                />

                <TicketOptionButton
                    onClick={() => ticketCaseMenu.open()}
                    symbol='ticket_task'
                    text='Дела'
                />

                <TicketOptionButton
                    onClick={() => ticketFilesOpenHandler.open()}
                    symbol='ticket_file'
                    text='Файлы'
                    svgSize={{ width: 9, height: 13 }}
                />

                <TicketOptionButton
                    onClick={() => ticketHistoryModalHandler.open()}
                    symbol='ticket_history'
                    text='История'
                    svgSize={{ width: 13, height: 13 }}
                />
            </TicketOptionsLayout>
        </>
    );
};

export default React.memo(TicketOptions);
