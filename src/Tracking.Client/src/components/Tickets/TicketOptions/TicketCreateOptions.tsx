import React from 'react';
import TicketOptionsLayout from './EntityOptionsLayout';
import TicketOptionButton from './TicketOptionButton';
import { ticketFilesOpenHandler } from './TicketOptions';
import TicketCreateActionsMenu, {
    ticketCreateActionsHandler,
} from '../TicketActionsMenu/TicketCreateActionsMenu';
import StatusSelect from '../StatusSelect/StatusSelect';

const TicketCreateOptions: React.FC = () => {
    return (
        <>
            <TicketOptionsLayout otherChildren={<StatusSelect />}>
                <div className='rowStartEnd'>
                    <TicketOptionButton
                        onClick={() => ticketCreateActionsHandler.open()}
                        symbol='ticket_edit'
                        text='Действия'
                    />
                    <TicketCreateActionsMenu />
                </div>

                <TicketOptionButton
                    onClick={() => ticketFilesOpenHandler.open()}
                    symbol='ticket_file'
                    text='Файлы'
                    svgSize={{ width: 9, height: 13 }}
                />
            </TicketOptionsLayout>
        </>
    );
};

export default TicketCreateOptions;
