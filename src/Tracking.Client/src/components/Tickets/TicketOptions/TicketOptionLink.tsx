import React from 'react';
import { Link } from 'react-router-dom';
import style from '../../../style/layout/ticket.module.scss';
import Svg from '@/components/Elements/Svg/Svg';

interface TicketOptionLinkProps {
    to: string;
    symbol: string;
    text: string;
    svgSize?: {
        width: number;
        height: number;
    };
}

const TicketOptionLink: React.FC<TicketOptionLinkProps> = ({
    to,
    text,
    symbol,
    svgSize = {
        width: 12,
        height: 13,
    },
}) => {
    return (
        <Link to={to} className={style.ticketOptionLink}>
            <Svg symbol={symbol} style={{ ...svgSize }} />
            <span>{text}</span>
        </Link>
    );
};

export default TicketOptionLink;
