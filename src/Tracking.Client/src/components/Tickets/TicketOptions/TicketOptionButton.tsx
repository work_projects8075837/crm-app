import Svg from '@/components/Elements/Svg/Svg';
import React from 'react';
import style from '../../../style/layout/ticket.module.scss';

interface TicketOptionButtonProps {
    symbol: string;
    text: string;
    onClick: () => void;
    svgSize?: {
        width: number;
        height: number;
    };
}

const TicketOptionButton: React.FC<TicketOptionButtonProps> = ({
    symbol,
    text,
    onClick,
    svgSize = {
        width: 12,
        height: 13,
    },
}) => {
    return (
        <button
            onClick={(event) => {
                event.preventDefault();
                event.stopPropagation();
                onClick();
            }}
            className={style.ticketOptionLink}
        >
            <Svg symbol={symbol} style={{ ...svgSize }} />
            <span>{text}</span>
        </button>
    );
};

export default TicketOptionButton;
