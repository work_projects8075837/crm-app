import React from 'react';
import style from '../../../style/layout/ticket.module.scss';

interface EntityOptionsLayoutProps {
    children?: React.ReactNode;
    otherChildren?: React.ReactNode;
}

const EntityOptionsLayout: React.FC<EntityOptionsLayoutProps> = ({
    children,
    otherChildren,
}) => {
    return (
        <>
            <div className={style.ticketOptionsBar}>
                <nav className={style.ticketOptions}>{children}</nav>

                {otherChildren}
            </div>
        </>
    );
};

export default EntityOptionsLayout;
