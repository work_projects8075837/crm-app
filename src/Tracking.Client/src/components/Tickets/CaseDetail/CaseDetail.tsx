import BlueButton from '@/components/Fields/Buttons/BlueButton';
import { formatDateWithoutTime } from '@/components/utils/formatDate';
import { formatTimeString } from '@/components/utils/formatTimeString';
import { caseHooks } from '@/services/case/case';
import { OpenHandler } from '@/store/states/OpenHandler';
import { ValueHandler } from '@/store/states/SearchHandler';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import style from '../../../style/layout/side_menu.module.scss';

export const caseId = new ValueHandler();

interface CaseDetailProps {
    openHandler: OpenHandler;
}

const CaseDetail: React.FC<CaseDetailProps> = ({ openHandler }) => {
    const { data } = caseHooks.useOne(caseId.value || undefined);

    useEffect(() => {
        return () => openHandler.close();
    }, []);

    return (
        <>
            <div className={style.sideBarFormWithBottom}>
                <div className={style.caseDetailWrapper}>
                    <h2>{data?.name}</h2>

                    <div
                        className={style.caseDetailContent}
                        dangerouslySetInnerHTML={{
                            __html: data?.comment || '',
                        }}
                    ></div>
                </div>
                <div className={style.caseDetailBottom}>
                    <div className={style.sideMenuContainerRow}>
                        <div>
                            <span className={style.textLabel}>
                                Срок выполнения
                            </span>
                            <p className={style.textBold}>
                                {formatDateWithoutTime(data?.finish_date)}{' '}
                                {formatTimeString(data?.finish_time)}
                            </p>
                        </div>

                        <div>
                            <span className={style.textLabel}>
                                Ответственный
                            </span>
                            <p className={style.textBold}>
                                {data?.manager?.user.name}
                            </p>
                        </div>
                    </div>

                    <div className={style.bottomButtons}>
                        <BlueButton
                            onClick={(event) => {
                                event.preventDefault();
                                openHandler.close();
                            }}
                        >
                            Закрыть
                        </BlueButton>
                    </div>
                </div>
            </div>
        </>
    );
};

export default observer(CaseDetail);
