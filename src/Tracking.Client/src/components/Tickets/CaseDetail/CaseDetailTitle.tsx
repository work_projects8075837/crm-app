import { caseHooks } from '@/services/case/case';
import React from 'react';
import style from '../../../style/layout/side_menu.module.scss';
import { caseId } from './CaseDetail';

const CaseDetailTitle: React.FC = () => {
    const { data } = caseHooks.useOne(caseId.value || undefined);

    return (
        <>
            <h3 className={style.headerTitle}>{data?.name}</h3>
        </>
    );
};

export default CaseDetailTitle;
