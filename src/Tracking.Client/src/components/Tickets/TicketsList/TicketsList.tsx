import EntityLoaderCircle from '@/components/Elements/LoaderCircle/EntityLoaderCircle';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import TextLoader from '@/components/Elements/TextLoader/TextLoader';
import { useQueryParams } from '@/hooks/useQueryParams';
import { ticketApi } from '@/services/ticket/ticket';
import {
    GET_All_TICKETS_KEY,
    GET_SEARCH_TICKETS_KEY,
} from '@/store/queries/keys/keys';
import { useTicketsTable } from '@/store/queries/stores/ticket/useTicketsTable';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/layout/ticket.module.scss';
import { ticketFields } from '../TicketFields/TicketFields';
import TicketItem from '../TicketItem/TicketItem';
import { ticketsPaginationHandler } from '../TicketsPagination/TicketsPagination';
import TicketsTableLayout from '../TicketsTableLayout/TicketsTableLayout';

export const ticketCheckHandler = new CheckEntitiesHandler();

const TicketsList: React.FC = () => {
    const q = useQueryParams();

    const { data, refetch } = useTicketsTable(
        {
            page: ticketsPaginationHandler.page,
            offset: ticketsPaginationHandler.offset,
            ...q,
        },
        { refetchOnMount: true },
    );

    useSetCheckableEntities(ticketCheckHandler, data?.data);

    return (
        <>
            <TicketsTableLayout
                style={ticketFields.getGridTemplateStyle()}
                fields={ticketFields.getSelectedFields()}
            >
                {data ? (
                    <>
                        {data.data.length ? (
                            <>
                                {data.data.map((ticket) => {
                                    return (
                                        <TicketItem
                                            ticket={ticket}
                                            key={ticket.id}
                                        />
                                    );
                                })}
                            </>
                        ) : (
                            <>
                                <TextLoader
                                    text='Нет заявок'
                                    className={style.loaderText}
                                />
                            </>
                        )}
                    </>
                ) : (
                    <>
                        <EntityLoaderCircle />
                    </>
                )}
            </TicketsTableLayout>
            <EntitySelectedActions
                keys={[GET_All_TICKETS_KEY, GET_SEARCH_TICKETS_KEY]}
                checkHandler={ticketCheckHandler}
                service={ticketApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default observer(TicketsList);
