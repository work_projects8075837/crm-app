import UserCheckboxSelect from '@/components/Fields/UserCheckboxSelect/UserCheckboxSelect';
import AddMembersForm from '@/components/Form/AddMembersForm/AddMembersForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import MembersList from '../MembersList/MembersList';
import { useFormMembers } from '../hooks/useFormMembers';
import style from '../../../style/layout/modal_window.module.scss';
import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';

export const membersAddModalOpenHandler = new OpenHandler(false);

interface MembersAddModalProps {
    creatorMember?: React.ReactNode;
}

const MembersAddModal: React.FC<MembersAddModalProps> = ({ creatorMember }) => {
    const { members } = useFormMembers();
    return (
        <>
            <ModalWindow
                openHandler={membersAddModalOpenHandler}
                title='Добавить сотрудника'
                layoutClass={style.modalWide}
            >
                <AddMembersForm openHandler={membersAddModalOpenHandler}>
                    <MembersList creatorMember={creatorMember} />
                    <UserCheckboxSelect />
                </AddMembersForm>
            </ModalWindow>
        </>
    );
};

export default MembersAddModal;
