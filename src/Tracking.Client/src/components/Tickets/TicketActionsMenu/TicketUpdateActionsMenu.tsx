import { confirmAction } from '@/components/Elements/ConfirmDialogWindow/confirmWindow';
import {
    ITicketForm,
    TICKET_MEMBERS_TYPES,
} from '@/components/Fields/EntitiesFields/ticket';
import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import { queryClient } from '@/store/queries/client/client';
import { GET_All_TICKETS_KEY } from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { duplicateTicketState } from '@/store/states/duplicateTicketState.ts';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import ActionsMenu from './ActionsMenu';
import ActionsMenuButton from './ActionsMenuButton';
import { useDeleteCurrentTicketMutation } from './useDeleteCurrentTicketMutation';

export const ticketUpdateActionsHandler = new OpenHandler(false);

const TicketUpdateActionsMenu: React.FC = () => {
    const navigate = useNavigate();
    const { onSubmit } = useSubmit();
    const { getValues } = useFormContext<ITicketForm>();
    const deleteTicket = useDeleteCurrentTicketMutation();

    return (
        <>
            <ActionsMenu openHandler={ticketUpdateActionsHandler}>
                <ActionsMenuButton
                    symbol='duplicate'
                    text='Дублировать'
                    onClick={() => {
                        ticketUpdateActionsHandler.close();
                        if (onSubmit) {
                            onSubmit(() => {
                                const ticketForm = getValues();
                                ticketForm.manager = ticketForm.manager?.filter(
                                    (member) =>
                                        member.type !==
                                        TICKET_MEMBERS_TYPES.author,
                                );
                                duplicateTicketState.setTicketForm(ticketForm);
                                navigate('/ticket/create/');
                            });
                        }
                    }}
                />
                <ActionsMenuButton
                    symbol='delete_bag'
                    text='Удалить'
                    onClick={async () => {
                        if (
                            await confirmAction({
                                title: 'Подтвердите удаление',
                                text: 'Вы действительно хотите продолжить и совершить удаление?',
                            })
                        ) {
                            deleteTicket.mutate(undefined, {
                                onSuccess() {
                                    navigate('/');
                                    queryClient.invalidateQueries({
                                        queryKey: [GET_All_TICKETS_KEY],
                                    });
                                    queryClient.removeQueries({
                                        queryKey: [GET_All_TICKETS_KEY],
                                    });
                                },
                            });
                        }

                        ticketUpdateActionsHandler.close();
                    }}
                />
            </ActionsMenu>
        </>
    );
};

export default TicketUpdateActionsMenu;
