import React, { ButtonHTMLAttributes } from 'react';
import style from '../../../style/elements/popup.module.scss';
import Svg from '@/components/Elements/Svg/Svg';

interface ActionsMenuButtonProps
    extends ButtonHTMLAttributes<HTMLButtonElement> {
    text: string;
    symbol: string;
}

const ActionsMenuButton: React.FC<ActionsMenuButtonProps> = ({
    text,
    symbol,
    ...buttonProps
}) => {
    return (
        <button className={style.popupOption} {...buttonProps}>
            <Svg symbol={symbol} />
            <span>{text}</span>
        </button>
    );
};

export default ActionsMenuButton;
