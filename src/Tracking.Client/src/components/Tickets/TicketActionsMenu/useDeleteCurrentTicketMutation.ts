import { globalLoader } from '@/components/Elements/GlobalLoader/GlobalLoader';
import { ticketApi } from '@/services/ticket/ticket';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useDeleteCurrentTicketMutation = () => {
    const { ticketId } = useParams();
    const mutation = useMutation({
        mutationFn: async () => {
            if (!ticketId) {
                throw Error();
            }
            globalLoader.open();

            return await ticketApi.hide(ticketId);
        },
        onSettled: () => {
            globalLoader.close();
        },
    });

    return mutation;
};
