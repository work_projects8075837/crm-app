import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import style from '../../../style/elements/popup.module.scss';

interface ActionsMenuProps {
    openHandler: OpenHandler;
    className?: string;
    children: React.ReactNode;
}

const ActionsMenu: React.FC<ActionsMenuProps> = ({
    openHandler,
    children,
    className = '',
}) => {
    useEffect(() => {
        const onOutsideClick = () => {
            openHandler.close();
        };
        document.addEventListener('click', onOutsideClick);
        return () => {
            document.removeEventListener('click', onOutsideClick);
            openHandler.close();
        };
    }, []);
    return (
        <>
            {openHandler.isOpen && (
                <>
                    <div className={`${style.popup} ${className}`}>
                        {children}
                    </div>
                </>
            )}
        </>
    );
};

export default observer(ActionsMenu);
