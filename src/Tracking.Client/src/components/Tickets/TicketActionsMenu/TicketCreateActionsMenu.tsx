import { useSubmit } from '@/components/Form/BaseForm/useSubmit';
import { ITicket } from '@/services/ticket/types';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import { toast } from 'react-hot-toast';
import ActionsMenu from './ActionsMenu';
import ActionsMenuButton from './ActionsMenuButton';

export const ticketCreateActionsHandler = new OpenHandler(false);

const TicketCreateActionsMenu: React.FC = () => {
    const { onSubmit } = useSubmit();

    return (
        <>
            <ActionsMenu openHandler={ticketCreateActionsHandler}>
                <ActionsMenuButton
                    symbol='duplicate'
                    text='Дублировать'
                    onClick={() => {
                        ticketCreateActionsHandler.close();
                        if (onSubmit) {
                            onSubmit((newTicket: ITicket) => {
                                toast.success(
                                    `Заявка сохранена под номером ${newTicket?.number}`,
                                );
                            });
                        }
                    }}
                />
            </ActionsMenu>
        </>
    );
};

export default TicketCreateActionsMenu;
