import React from 'react';
import style from '../../../style/layout/ticket.module.scss';
import TicketsTag from './TicketsTag';
import AddTicketTagButton from './AddTicketTagButton/AddTicketTagButton';
import { TICKET_TAGS_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { observer } from 'mobx-react-lite';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import '../../../style/elements/tag_item.scss';
import { tagsModalHandler } from '../TicketCreate/AddTagsModal/AddTagsModal';

const tags = [
    { id: '1', name: 'ЕГАИС' },
    { id: '2', name: 'МАРКИРОВКА' },
    { id: '3', name: 'ЭВОТОР' },
];

const TagsList: React.FC = () => {
    const { entities } = useFormEntityValueArray(TICKET_TAGS_NAME);

    return (
        <div className={style.tags}>
            {entities?.map((tag) => <TicketsTag key={tag.name} {...tag} />)}
            <AddTicketTagButton openHandler={tagsModalHandler} />
        </div>
    );
};

export default observer(TagsList);
