import CloseCrossButton from '@/components/Fields/SelectedEntityCard/CloseCrossButton';
import React from 'react';
import style from '../../../style/layout/ticket.module.scss';

interface RemoveTagButtonProps {
    onClick: () => void;
}

const RemoveTagButton: React.FC<RemoveTagButtonProps> = ({ onClick }) => {
    return (
        <>
            <CloseCrossButton
                className={`${style.tagRemoveButton} tagButton`}
                onClick={onClick}
            />
        </>
    );
};

export default RemoveTagButton;
