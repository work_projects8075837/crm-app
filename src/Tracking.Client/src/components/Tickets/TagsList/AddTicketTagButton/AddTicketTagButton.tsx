import React from 'react';
import style from '../../../../style/layout/ticket.module.scss';
import Svg from '@/components/Elements/Svg/Svg';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { observer } from 'mobx-react-lite';

interface AddTicketTagButtonProps {
    openHandler: OpenHandler;
}

const AddTicketTagButton: React.FC<AddTicketTagButtonProps> = ({
    openHandler,
}) => {
    return (
        <>
            <button
                className={style.tagAddButton}
                onClick={() => openHandler.open()}
            >
                <Svg symbol='plus' className={style.tagPlusSvg} />
            </button>
        </>
    );
};

export default observer(AddTicketTagButton);
