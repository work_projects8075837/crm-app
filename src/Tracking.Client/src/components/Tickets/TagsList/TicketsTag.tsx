import React from 'react';
import style from '../../../style/layout/ticket.module.scss';
import RemoveTagButton from './RemoveTagButton';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { TICKET_TAGS_NAME } from '@/components/Fields/EntitiesFields/ticket';

interface TicketsTagProps {
    name?: string;
    id: string;
}

const TicketsTag: React.FC<TicketsTagProps> = ({ name, id }) => {
    const { remove } = useFormEntityValueArray(TICKET_TAGS_NAME);
    return (
        <div className={`${style.tag} tag`}>
            {name}
            <RemoveTagButton onClick={() => remove(id)} />
        </div>
    );
};

export default TicketsTag;
