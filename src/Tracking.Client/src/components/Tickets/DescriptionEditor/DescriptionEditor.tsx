import EditorInput from '@/components/Fields/EditorInput/EditorInput';
import { TICKET_DESCRIPTION_NAME } from '@/components/Fields/EntitiesFields/ticket';
import React from 'react';
import 'react-quill/dist/quill.snow.css';
import '../../../style/elements/editor.scss';

const DescriptionEditor: React.FC = () => {
    return (
        <>
            <EditorInput name={TICKET_DESCRIPTION_NAME} />
        </>
    );
};

export default DescriptionEditor;
