import CircleLoading from '@/components/Elements/LoaderCircle/CircleLoading';
import { useTicketList } from '@/services/ticket/ticket';
import { StyleTicket } from '@/style/layout/layout';
import React from 'react';
import { Link } from 'react-router-dom';

interface DirectorySubTicketsProps {
    ticketId: string;
}

const DirectorySubTickets: React.FC<DirectorySubTicketsProps> = ({
    ticketId,
}) => {
    const { data } = useTicketList({ ticket_ticket_id: ticketId });

    return (
        <div className={StyleTicket.directoryLinks}>
            {data ? (
                data?.data.map((t) => (
                    <Link
                        key={t.id}
                        to={`/ticket/${t.id}`}
                        className={StyleTicket.directoryLink}
                    >
                        Заявка номер {t.number}
                    </Link>
                ))
            ) : (
                <CircleLoading size={20} color='#fff' />
            )}
        </div>
    );
};

export default DirectorySubTickets;
