import TableStepItem from '@/components/Tables/TableItem/TableStepItem';
import { ticketApi, ticketKeys } from '@/services/ticket/ticket';
import { ITicket, ITicketFilter } from '@/services/ticket/types';
import { StyleTicket } from '@/style/layout/layout';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { ticketFields } from '../TicketFields/TicketFields';
import { ticketCheckHandler } from '../TicketsList/TicketsList';

interface TicketItemProps {
    ticket: ITicket;
}

const TicketItem: React.FC<TicketItemProps> = ({ ticket }) => {
    const navigate = useNavigate();
    return (
        <TableStepItem<ITicket, ITicketFilter>
            mainQueryKey={ticketKeys.GET_ALL_KEY}
            isParent={ticket.is_parent}
            parentFilterKey='ticket_ticket_id'
            parentKey='ticket_id'
            getAll={ticketApi.getAll}
            entity={ticket}
            checkHandler={ticketCheckHandler}
            key={ticket.id}
            className={StyleTicket.ticketTableItem}
            onDoubleClick={(t) => navigate(`/ticket/${t.id}/`)}
            fieldsHandler={ticketFields}
        />
    );
};

export default TicketItem;
