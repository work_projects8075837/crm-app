import ValueDash from '@/components/Elements/ValueDash/ValueDash';
import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import { TICKET_MEMBERS_TYPES } from '@/components/Fields/EntitiesFields/ticket';
import { calculateSla } from '@/components/utils/calculateSla';
import { formatDate } from '@/components/utils/formatDate';
import { formatPhoneLink } from '@/components/utils/formatPhoneLink';
import { ITicket } from '@/services/ticket/types';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/table.module.scss';
import StatusItem from '../StatusItem/StatusItem';
import DirectoryWarn from './DirectoryWarn';

export interface IStatus {
    img: string;
    text: string;
    color: string;
    backgroundColor: string;
}

interface TicketItemProps {
    checkHandler: CheckEntitiesHandler;
    ticket: ITicket;
}

const TicketItem: React.FC<TicketItemProps> = ({ ticket, checkHandler }) => {
    const navigate = useNavigate();

    return (
        <>
            <div
                className={style.rowItem}
                onDoubleClick={() => {
                    navigate(`/ticket/${ticket.id}/`);
                }}
            >
                <CheckItem checkHandler={checkHandler} entity={ticket} />

                <div className={style.rowItemField}>
                    <span>{ticket.number}</span>

                    <DirectoryWarn ticket={ticket} />
                </div>

                <div className={style.rowItemField}>
                    <span>{formatDate(ticket.created_at)}</span>
                </div>

                <div className={style.rowItemField}>
                    <span>
                        <ValueDash>{ticket.sale_point?.name}</ValueDash>
                    </span>
                </div>

                <div className={style.rowItemField}>
                    <ValueDash>
                        {!!ticket.contact?.phone_number.length &&
                            ticket.contact?.phone_number.map((p) => (
                                <a
                                    href={`tel:${formatPhoneLink(p.name)}`}
                                    key={p.id}
                                >
                                    {p.name}
                                </a>
                            ))}
                    </ValueDash>
                </div>

                <div className={style.rowItemField}>
                    <span>
                        <ValueDash>{ticket.character_problem?.name}</ValueDash>
                    </span>
                </div>

                <div className={style.rowItemField}>
                    <span>
                        <ValueDash>
                            {ticket.manager.find(
                                (m) =>
                                    m.type === TICKET_MEMBERS_TYPES.responsible,
                            )?.user.name || ticket.manager[0]?.user.name}
                        </ValueDash>
                    </span>
                </div>

                <div className={style.rowItemField}>
                    <span>
                        <ValueDash>
                            {calculateSla(ticket.character_problem?.sla)}
                        </ValueDash>
                    </span>
                </div>

                <div className={style.rowItemField}>
                    {!!ticket.status && <StatusItem {...ticket.status} />}
                </div>
            </div>
        </>
    );
};

export default React.memo(TicketItem);
