import DirectoryIcon from '@/components/Elements/DirectoryIcon/DirectoryIcon';
import BaseTooltip from '@/components/SideBar/Links/BaseTooltip/BaseTooltip';
import { ITicket } from '@/services/ticket/types';
import React from 'react';
import DirectorySubTickets from './DirectorySubTickets';

interface DirectoryWarnProps {
    ticket: ITicket;
}

const DirectoryWarn: React.FC<DirectoryWarnProps> = ({ ticket }) => {
    return (
        <>
            {ticket.is_parent && (
                <button data-tooltip-id={`ticket-tooltip_${ticket.id}`}>
                    <DirectoryIcon />
                </button>
            )}
            <BaseTooltip
                id={`ticket-tooltip_${ticket.id}`}
                delayShow={0}
                openOnClick={true}
                clickable={true}
            >
                <DirectorySubTickets ticketId={ticket.id} />
            </BaseTooltip>
        </>
    );
};

export default DirectoryWarn;
