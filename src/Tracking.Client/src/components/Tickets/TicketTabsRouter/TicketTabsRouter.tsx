import React from 'react';
import { TabHandler } from '@/store/states/TabHandler.ts';
import { observer } from 'mobx-react-lite';
import GeneralInfo from '../TicketCreate/GeneralInfo/GeneralInfo';

export const ticketTabsNames = {
    general_info: 'general_info',
    sub_tickets: 'sub_tickets',
    ticket_jobs: 'ticket_jobs',
};

interface TicketTabsRouterProps {
    className?: string;
    tabHandler: TabHandler;
    generalInfo?: React.ReactNode;
    subTickets?: React.ReactNode;
    ticketJobs?: React.ReactNode;
}

const TicketTabsRouter: React.FC<TicketTabsRouterProps> = ({
    className = '',
    tabHandler,
    generalInfo,
    subTickets,
    ticketJobs,
}) => {
    return (
        <div className={className}>
            {tabHandler.tab === ticketTabsNames.general_info && generalInfo}

            {tabHandler.tab === ticketTabsNames.sub_tickets && subTickets}

            {tabHandler.tab === ticketTabsNames.ticket_jobs && ticketJobs}
        </div>
    );
};

export default observer(TicketTabsRouter);
