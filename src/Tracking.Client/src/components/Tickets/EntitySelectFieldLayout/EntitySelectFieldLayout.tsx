import React from 'react';
import style from '../../../style/elements/select_option.module.scss';

interface EntitySelectFieldLayoutProps {
    label: string;
    children: React.ReactNode;
}

const EntitySelectFieldLayout: React.FC<EntitySelectFieldLayoutProps> = ({
    children,
    label,
}) => {
    return (
        <div className={style.selectLayout}>
            <span className={style.selectLabelText}>{label}</span>
            {children}
        </div>
    );
};

export default EntitySelectFieldLayout;
