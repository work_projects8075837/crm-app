import { useCallback } from 'react';

export const useOpenCreateContact = () => {
    const open = useCallback((value: string) => {
        const isPhone = value.match(/^\+?[0-9\s\-\(\)]+$/g);
    }, []);
};
