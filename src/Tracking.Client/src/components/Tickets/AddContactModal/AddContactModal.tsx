import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import CreateContactForm from '@/components/Form/CreateContactForm/CreateContactForm';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import { ValueHandler } from '@/store/states/SearchHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';

export const addContactModalOpenHandler = new OpenHandler(false);

export const providedContactValue = new ValueHandler();

const AddContactModal: React.FC = () => {
    const isPhone = providedContactValue.value?.match(/^\+?[0-9\s\-\(\)]+$/g);
    return (
        <>
            <ModalWindow
                openHandler={addContactModalOpenHandler}
                title='Создать контакт'
            >
                <CreateContactForm
                    defaultName={
                        isPhone ? undefined : providedContactValue.value || ''
                    }
                    openHandler={addContactModalOpenHandler}
                />
            </ModalWindow>
        </>
    );
};

export default observer(AddContactModal);
