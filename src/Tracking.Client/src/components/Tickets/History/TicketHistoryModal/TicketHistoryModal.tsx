import ModalWindow from '@/components/Elements/ModalWindow/ModalWindow';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../../style/layout/modal_window.module.scss';
import HistoryList from '../HistoryTable/HistoryList';

export const ticketHistoryModalHandler = new OpenHandler(false);

const TicketHistoryModal: React.FC = () => {
    return (
        <>
            <ModalWindow
                layoutClass={style.modalLargeHeight}
                contentClass={style.emptyModalContent}
                title='Изменения заявки'
                openHandler={ticketHistoryModalHandler}
            >
                <HistoryList />
            </ModalWindow>
        </>
    );
};

export default React.memo(TicketHistoryModal);
