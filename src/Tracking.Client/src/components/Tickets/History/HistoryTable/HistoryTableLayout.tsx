import { StyleModalTable } from '@/style/layout/layout';
import classNames from 'classnames';
import React from 'react';
import style from '../../../../style/layout/modal_table.module.scss';

interface HistoryTableLayoutProps {
    children: React.ReactNode;
}

const HistoryTableLayout: React.FC<HistoryTableLayoutProps> = ({
    children,
}) => {
    return (
        <>
            <div className={classNames(style.modalTable)}>
                <div
                    className={classNames(style.modalTableHeader, style.noGap)}
                >
                    <div className={style.modalTableHeaderField}>
                        Изменённый параметр
                    </div>
                    <div className={style.modalTableHeaderField}>
                        Старое значение
                    </div>
                    <div className={style.modalTableHeaderField}>
                        Новое значение
                    </div>
                    <div className={style.modalTableHeaderField}>
                        Кем изменено
                    </div>
                    <div className={style.modalTableHeaderField}>
                        Когда изменено
                    </div>
                </div>
                <div
                    className={classNames(
                        style.modalTableContent,
                        StyleModalTable.noGap,
                    )}
                >
                    {children}
                </div>
            </div>
        </>
    );
};

export default HistoryTableLayout;
