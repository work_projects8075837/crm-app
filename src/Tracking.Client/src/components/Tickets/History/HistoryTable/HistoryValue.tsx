import Dash from '@/components/Elements/Dash/Dash';
import { warnMessage } from '@/components/Elements/WarnWindow/warnMessage';
import { isEditorContentExist } from '@/components/Fields/SalePointSelect/SalePointSelect';
import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import { formatDate } from '@/components/utils/formatDate';
import {
    IHistoryItem,
    fieldsDictionary,
} from '@/components/utils/formatHistory';
import { IStatus } from '@/services/status/types';
import { StyleModalTable } from '@/style/layout/layout';
import classNames from 'classnames';
import React from 'react';
import StatusItem from '../../StatusItem/StatusItem';

{
    /* <div>
    {historyItem.old && historyItem.current?.length < historyItem.old?.length
        ? `Уменьшение на ${
              historyItem.old?.length - historyItem.current?.length
          }`
        : `Увеличение ${
              historyItem.current?.length - (historyItem.old?.length || 0)
          }`}
</div>; */
}

type HistoryValue =
    | {
          type: 'string';
          value: string;
      }
    | {
          type: 'entity';
          value: Entity | null;
      }
    | {
          type: 'entities';
          value: Entity[] | null;
      }
    | {
          type: 'status';
          value: IStatus | null;
      };

interface HistoryValueProps {
    historyItem: IHistoryItem;
    state: 'old' | 'current';
}

const HistoryValue: React.FC<HistoryValueProps> = ({ historyItem, state }) => {
    <span className={StyleModalTable.emptyValue}>Нет значения</span>;
    if (historyItem.type === 'string')
        console.log(historyItem[state], historyItem.field);

    return (
        <>
            {historyItem.type === 'string' && (
                <>
                    {historyItem[state] ? (
                        historyItem.field?.includes('at') ? (
                            <span>{formatDate(historyItem[state])}</span>
                        ) : (
                            historyItem[state]
                        )
                    ) : (
                        <Dash className={StyleModalTable.dashMargin} />
                    )}
                </>
            )}
            {historyItem.type === 'editor' &&
                (historyItem[state] &&
                isEditorContentExist(historyItem[state]) ? (
                    <span
                        className={StyleModalTable.link}
                        onClick={() => {
                            warnMessage({
                                text: historyItem[state] || '',
                                title: fieldsDictionary[historyItem.field],
                                isHtml: true,
                                zIndex: 9,
                            });
                        }}
                    >
                        Подробнее
                    </span>
                ) : (
                    <Dash className={StyleModalTable.dashMargin} />
                ))}
            {historyItem.type === 'entity' &&
                (historyItem[state]?.name ? (
                    <div className={StyleModalTable.entityValue}>
                        {historyItem[state]?.name}
                    </div>
                ) : (
                    <Dash className={StyleModalTable.dashMargin} />
                ))}
            {historyItem.type === 'status' && (
                <>
                    {' '}
                    {historyItem[state] ? (
                        <StatusItem {...(historyItem[state] as IStatus)} />
                    ) : (
                        <Dash className={StyleModalTable.dashMargin} />
                    )}
                </>
            )}
            {historyItem.type === 'entities' && (
                <>
                    {state === 'old' ? (
                        historyItem.old?.map(({ id, name }) => (
                            <div
                                key={id}
                                className={classNames({
                                    [StyleModalTable.redValue]:
                                        !historyItem.current.some(
                                            (ent) => ent.id === id,
                                        ),
                                })}
                            >
                                {name}
                            </div>
                        ))
                    ) : (
                        <>
                            {historyItem.current?.map(({ id, name }) => (
                                <div
                                    key={id}
                                    className={classNames({
                                        [StyleModalTable.greenValue]:
                                            !historyItem.old?.some(
                                                (ent) => ent.id === id,
                                            ),
                                    })}
                                >
                                    {name}
                                </div>
                            ))}
                        </>
                    )}
                </>
            )}
        </>
    );
};

export default HistoryValue;
