import { confirmAction } from '@/components/Elements/ConfirmDialogWindow/confirmWindow';
import { globalLoader } from '@/components/Elements/GlobalLoader/GlobalLoader';
import Svg from '@/components/Elements/Svg/Svg';
import { ITicketForm } from '@/components/Fields/EntitiesFields/ticket';
import {
    IHistoryItem,
    fieldsDictionary,
} from '@/components/utils/formatHistory';
import { ITicket } from '@/services/ticket/types';
import { StyleModalTable } from '@/style/layout/layout';
import { useFormContext } from 'react-hook-form';
import { formatTicketFormDefaultValues } from '../../Providers/TicketEditFormProvider/formatTicketFormDefaultValues';
import { useHandleStatus } from '../../StatusSelect/useHandleStatus';
import { ticketHistoryModalHandler } from '../TicketHistoryModal/TicketHistoryModal';
import HistoryValue from './HistoryValue';
import { useTicketRecovery } from './useTicketRecovery';

export const useHandleHistoryItem = () => {
    const { mutateAsync, isLoading } = useTicketRecovery();
    const { reset } = useFormContext<ITicketForm>();
    const statusHandler = useHandleStatus();

    const handle = async (historyItem: IHistoryItem) => {
        if (!historyItem.old || isLoading) {
            return;
        }
        const isConfirm = await confirmAction({
            text: (
                <>
                    <h3 className={StyleModalTable.title}>
                        Подтвертдите восстановление параметра{' '}
                        {`"${fieldsDictionary[historyItem.field]}"`}
                    </h3>

                    <div className={StyleModalTable.historyItem}>
                        <HistoryValue
                            state='current'
                            historyItem={historyItem}
                        />

                        <Svg symbol='history_arrow' />
                        <HistoryValue state='old' historyItem={historyItem} />
                    </div>
                </>
            ),
        });

        if (isConfirm) {
            globalLoader.open();

            let newTicket: ITicket | null = null;

            if (historyItem.type === 'status') {
                newTicket = await statusHandler.handle(historyItem.old);
            } else {
                newTicket = await mutateAsync({
                    historyItem,
                });
            }

            reset(formatTicketFormDefaultValues(newTicket));

            globalLoader.close();
            ticketHistoryModalHandler.close();
        }
    };

    return { handle, isLoading: isLoading || statusHandler.isLoading };
};
