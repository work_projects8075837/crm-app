import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import TextLoader from '@/components/Elements/TextLoader/TextLoader';
import { formatHistory } from '@/components/utils/formatHistory';
import { historyHooks } from '@/services/history/history';
import { StyleModalTable } from '@/style/layout/layout';
import React from 'react';
import { useParams } from 'react-router-dom';
import HistoryTableItem from './HistoryTableItem';
import HistoryTableLayout from './HistoryTableLayout';

const HistoryList: React.FC = () => {
    const { ticketId } = useParams();
    const { data } = historyHooks.useList({
        ticket_history_ticket_id: ticketId,
        offset: 1000,
    });

    return (
        <>
            {data ? (
                <HistoryTableLayout>
                    {data.data.length ? (
                        formatHistory(data.data).map((historyItem, i) => (
                            <HistoryTableItem key={i} {...historyItem} />
                        ))
                    ) : (
                        <TextLoader text='История пуста' className='mt15' />
                    )}
                </HistoryTableLayout>
            ) : (
                <div className={StyleModalTable.loader}>
                    <LoaderCircle size={60} />
                </div>
            )}
        </>
    );
};

export default HistoryList;
