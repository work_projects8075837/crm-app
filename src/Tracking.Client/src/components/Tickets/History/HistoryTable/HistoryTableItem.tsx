import Svg from '@/components/Elements/Svg/Svg';
import {
    IHistoryItem,
    fieldsDictionary,
} from '@/components/utils/formatHistory';
import classNames from 'classnames';
import React from 'react';
import style from '../../../../style/layout/modal_table.module.scss';
import { formatDate } from './../../../utils/formatDate';
import HistoryValue from './HistoryValue';
import { useHandleHistoryItem } from './useHandleHistoryItem';

type HistoryTableItemProps = IHistoryItem;

const HistoryTableItem: React.FC<HistoryTableItemProps> = (props) => {
    const { handle, isLoading } = useHandleHistoryItem();
    return (
        <>
            <div
                className={classNames(style.modalTableItem, style.noGap, {
                    [style.pointer]: !!props.old && !isLoading,
                })}
                onDoubleClick={() => {
                    handle(props);
                }}
            >
                <div className={style.modalTableItemGapField}>
                    {fieldsDictionary[props.field]}
                </div>
                <div className={style.modalTableItemArrowField}>
                    <HistoryValue state='old' historyItem={props} />
                    <Svg symbol='history_arrow' />
                </div>
                <div className={style.modalTableItemGapField}>
                    <HistoryValue state='current' historyItem={props} />
                </div>
                <div
                    className={classNames(
                        style.modalTableItemGapField,
                        style.modalTableItemFieldBlue,
                    )}
                >
                    {props.user.name.split(' ').slice(0, 2).join(' ')}
                </div>
                <div className={style.modalTableItemGapField}>
                    {formatDate(props.created_at)}
                </div>
            </div>
        </>
    );
};

export default HistoryTableItem;
