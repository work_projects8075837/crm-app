import { IHistoryItem } from '@/components/utils/formatHistory';
import { historyKeys } from '@/services/history/history';
import { ticketApi, ticketKeys } from '@/services/ticket/ticket';
import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import { updateTicket } from '@/store/queries/actions/ticket';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

interface TicketRecoveryMutation {
    historyItem: IHistoryItem;
}

export const useTicketRecovery = () => {
    const { ticketId } = useParams();
    return useMutation({
        mutationFn: async ({ historyItem }: TicketRecoveryMutation) => {
            if (!ticketId) {
                throw Error();
            }
            switch (historyItem.type) {
                case 'editor':
                    return await ticketApi
                        .update(ticketId, {
                            [historyItem.field]: historyItem.old,
                        })
                        .then((res) => res.data);
                case 'string':
                    return await ticketApi
                        .update(ticketId, {
                            [historyItem.field]: historyItem.old,
                        })
                        .then((res) => res.data);

                case 'entities':
                    return await ticketApi
                        .update(ticketId, {
                            [historyItem.field]: historyItem.old,
                        })
                        .then((res) => res.data);
                case 'status':
                    return await ticketApi
                        .update(ticketId, {
                            [`${historyItem.field}_id`]: historyItem.old?.id,
                        })
                        .then((res) => res.data);
                default:
                    return await ticketApi
                        .update(ticketId, {
                            [`${historyItem.field}_id`]: historyItem.old?.id,
                        })
                        .then((res) => res.data);
            }
        },
        onSuccess: (data) => {
            updateTicket(data, ticketId);
            totalClearKey(ticketKeys.GET_ALL_KEY);
            invalidateKey(ticketKeys.GET_ONE_KEY);
            invalidateKey(historyKeys.GET_ALL_KEY);
        },
    });
};
