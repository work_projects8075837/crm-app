import TextLoader from '@/components/Elements/TextLoader/TextLoader';
import React from 'react';
import SmallTicketTableLayout from '../SmallTicketTableLayout/SmallTicketTableLayout';
import { useFormInventory } from '../hooks/useFormInventory';
import InventoryItem from './InventoryItem';

const inventoryFields = [{ name: 'Название' }, { name: 'Серийный номер' }];

const inventories = [
    {
        id: '6f86afc0-42d3-4132-8c92-85b8cd9d0405',
        name: 'Сервер',
        serial_number: '12vg44cd',
        connections: [1],
    },
    {
        id: '2f71cga3-42d3-4132-8c92-85b8cd9d0405',
        name: 'Моноблок',
        serial_number: '12vg44cd',
        connections: [],
    },
];

/**
 * TODO: Сделать лоадер если завки грузятся, сделать сообщение об отсутствии инвентаря если массив пустой
 *
 */

interface InventoryListProps {
    onlyConnections?: boolean;
}

const InventoryList: React.FC<InventoryListProps> = ({ onlyConnections }) => {
    const { data } = useFormInventory();
    const items = data?.filter((i) =>
        onlyConnections !== undefined
            ? onlyConnections
                ? !!i.connection.length
                : !i.connection.length
            : true,
    );
    return (
        <>
            {items ? (
                <>
                    {items?.length ? (
                        <>
                            <SmallTicketTableLayout fields={inventoryFields}>
                                {items
                                    .filter((i) => !i.is_parent)
                                    .map((inventory) => (
                                        <InventoryItem
                                            key={inventory.id}
                                            {...inventory}
                                        />
                                    ))}
                            </SmallTicketTableLayout>
                        </>
                    ) : (
                        <TextLoader
                            text={`У заведения нет ${
                                onlyConnections !== undefined
                                    ? onlyConnections
                                        ? 'подключений'
                                        : 'оборудования'
                                    : 'инвентаря'
                            }`}
                        />
                    )}
                </>
            ) : (
                <TextLoader />
            )}
        </>
    );
};

export default React.memo(InventoryList);
