import CircleLoading from '@/components/Elements/LoaderCircle/CircleLoading';
import BaseTooltip from '@/components/SideBar/Links/BaseTooltip/BaseTooltip';
import { IInventory } from '@/services/inventory/types';
import { StyleTicket } from '@/style/layout/layout';
import React from 'react';
import { useUpdateTicketProblemInventory } from './useUpdateTicketProblemInventory';

interface ProblemButtonProps {
    inventory: IInventory;
}

const ProblemButton: React.FC<ProblemButtonProps> = ({ inventory }) => {
    const { mutate, isProblem, isLoading } =
        useUpdateTicketProblemInventory(inventory);
    return (
        <>
            <BaseTooltip
                id={`inventory-tooltip_${inventory.id}`}
                delayShow={0}
                openOnClick={true}
                clickable={true}
                place='left'
            >
                {isLoading ? (
                    <CircleLoading size={20} color='#fff' />
                ) : (
                    <button
                        className={StyleTicket.inventoryButton}
                        onClick={() => {
                            mutate();
                        }}
                    >
                        {isProblem
                            ? 'Удалить из проблемного'
                            : 'Сделать проблемным'}
                    </button>
                )}
            </BaseTooltip>
        </>
    );
};

export default ProblemButton;
