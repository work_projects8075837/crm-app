import Svg from '@/components/Elements/Svg/Svg';
import { warnMessage } from '@/components/Elements/WarnWindow/warnMessage';
import {
    ANY_DESK,
    SUPREME,
    TEAM_VIEWER,
} from '@/components/Fields/EntitiesFields/connection';
import { IConnection } from '@/services/connection/types';
import React from 'react';
import { toast } from 'react-hot-toast';
import { Link } from 'react-router-dom';
import style from '../../../style/layout/small_table.module.scss';

interface ServerLinkProps {
    connection: IConnection;
    className?: string;
}

const ServerLink: React.FC<ServerLinkProps> = ({
    connection,
    className = '',
}) => {
    const isAnyDesk = connection.type === ANY_DESK;
    const isTeamViewer = connection.type === TEAM_VIEWER;
    const isSupreme = connection.type === SUPREME;

    const login = connection.configuration[0].login?.replaceAll(' ', '');
    const linkPrefix = isAnyDesk
        ? `anydesk:${login}`
        : isTeamViewer
        ? `https://start.teamviewer.com/${login}`
        : '';

    return (
        <>
            <Link
                className={style.serverLink}
                to={`${linkPrefix}`}
                rel='noreferrer'
                onClick={async (event) => {
                    event.stopPropagation();
                    if (isSupreme) {
                        await warnMessage({
                            text: connection.configuration[0].other || '',
                            title: 'Инструкция',
                        });
                    }
                    if (connection.configuration[0].password) {
                        navigator.clipboard.writeText(
                            connection.configuration[0].password,
                        );
                        toast.success('Пароль скопирован в буфер обмена!');
                    }
                }}
            >
                <Svg
                    className={`${style.smallTableIcon} ${className}`}
                    symbol={connection.type}
                />
            </Link>
        </>
    );
};

export default ServerLink;
