import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';

export const useIsProblemTicketInventory = (inventoryId: string) => {
    const ticketData = useTicketByParams().data;
    const isProblem = ticketData?.inventory.some((i) => i.id === inventoryId);
    return { isProblem, ticketData };
};
