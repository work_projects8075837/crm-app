import { IInventory } from '@/services/inventory/types';
import { ticketApi } from '@/services/ticket/ticket';
import { invalidateKey } from '@/store/queries/actions/base';
import { updateTicket } from '@/store/queries/actions/ticket';
import {
    GET_ALL_INVENTORIES_KEY,
    GET_All_TICKETS_KEY,
    GET_INVENTORIES_BY_SALE_POINT_KEY,
    GET_ONE_TICKET_BY_ID_KEY,
} from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';
import { useIsProblemTicketInventory } from './useIsProblemTicketInventory';

export const useUpdateTicketProblemInventory = (inventory: IInventory) => {
    const { isProblem, ticketData } = useIsProblemTicketInventory(inventory.id);
    const m = useMutation({
        mutationFn: async () => {
            if (!ticketData) {
                throw Error();
            }

            if (isProblem) {
                const newTicket = await ticketApi
                    .update(ticketData.id, {
                        inventory: ticketData.inventory.filter(
                            (i) => i.id !== inventory.id,
                        ),
                    })
                    .then((res) => res.data);

                updateTicket(newTicket, ticketData.id);

                return newTicket;
            }

            const newTicket = await ticketApi
                .update(ticketData.id, {
                    inventory: [...ticketData.inventory, inventory],
                })
                .then((res) => res.data);

            updateTicket(newTicket, ticketData.id);

            return newTicket;
        },
        onSuccess: () => {
            invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
            invalidateKey(GET_All_TICKETS_KEY);
            invalidateKey(GET_ALL_INVENTORIES_KEY);
            invalidateKey(GET_INVENTORIES_BY_SALE_POINT_KEY);
        },
    });

    return { ...m, isProblem };
};
