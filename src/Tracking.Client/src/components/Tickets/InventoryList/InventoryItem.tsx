import { TICKET_SALE_POINT_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { IInventory } from '@/services/inventory/types';
import classNames from 'classnames';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import style from '../../../style/layout/small_table.module.scss';
import ProblemButton from './ProblemButton';
import ServerLink from './ServerLink';
import { useIsProblemTicketInventory } from './useIsProblemTicketInventory';

type InventoryItemProps = IInventory;

const InventoryItem: React.FC<InventoryItemProps> = ({
    id,
    name,
    connection,
    serial_number,
    ...inventoryProps
}) => {
    const { getValues } = useFormContext();
    const navigate = useNavigate();
    const isServer = !!connection.length;

    const { isProblem } = useIsProblemTicketInventory(id);

    const { ticketId } = useParams();

    return (
        <>
            <div
                className={classNames(style.pointerTableRow, {
                    [style.problemItem]: isProblem,
                })}
                onDoubleClick={() => {
                    const salePointId = getValues(TICKET_SALE_POINT_NAME).id;
                    window.open(
                        `/sale_point/single/${salePointId}/inventory/single/${id}/${
                            isServer ? '?is_connection=1' : ''
                        }`,
                        '_blank',
                    );
                }}
                data-tooltip-id={`inventory-tooltip_${id}`}
            >
                <span className={style.tableRowField}>{name}</span>
                <span className={style.tableRowField}>
                    <span>{serial_number}</span>
                </span>
                <div className={style.betweenTableRowField}>
                    <div></div>
                    {isServer && (
                        <>
                            <div className={style.wrapper}>
                                {connection.map((connection) => (
                                    <ServerLink
                                        key={connection.id}
                                        connection={connection}
                                    />
                                ))}
                            </div>
                        </>
                    )}
                </div>
            </div>
            {ticketId && (
                <ProblemButton
                    inventory={{
                        id,
                        name,
                        connection,
                        serial_number,
                        ...inventoryProps,
                    }}
                />
            )}
        </>
    );
};

export default InventoryItem;
