import FormSelect from '@/components/Elements/FormSelect/FormSelect';
import { TabHandler } from '@/store/states/TabHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/elements/inventory_tabs.module.scss';
import InventoryList from './InventoryList';

const inventoryListTab = new TabHandler('connections');

const InventoryTabs: React.FC = () => {
    return (
        <>
            <FormSelect title='Инвентарь' childrenClass={style.list}>
                <div className={style.tabs}>
                    <button
                        className={
                            inventoryListTab.tab === 'connections'
                                ? style.tabActive
                                : style.tab
                        }
                        onClick={() => inventoryListTab.setTab('connections')}
                    >
                        Подключения
                    </button>

                    <button
                        className={
                            inventoryListTab.tab === 'equipment'
                                ? style.tabActive
                                : style.tab
                        }
                        onClick={() => inventoryListTab.setTab('equipment')}
                    >
                        Оборудование
                    </button>
                </div>
                {inventoryListTab.tab === 'connections' ? (
                    <InventoryList onlyConnections={true} />
                ) : (
                    <InventoryList onlyConnections={false} />
                )}
            </FormSelect>
        </>
    );
};

export default observer(InventoryTabs);
