import Pagination from '@/components/Tables/Pagination/Pagination';
import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import React from 'react';
import PaginationButtonsList from '../TiketsPaginationButtonsList/PaginationButtonsList';
import { observer } from 'mobx-react-lite';
import { useTicketsTable } from '@/store/queries/stores/ticket/useTicketsTable';

export const TICKET_PAGINATION = 'TICKET_PAGINATION';
export const ticketsPaginationHandler = new PaginationHandler(
    TICKET_PAGINATION,
);

const TicketsPagination: React.FC = () => {
    const { data } = useTicketsTable(ticketsPaginationHandler);
    return (
        <>
            <Pagination
                paginationHandler={ticketsPaginationHandler}
                pages={
                    <PaginationButtonsList
                        data={data}
                        paginationHandler={ticketsPaginationHandler}
                    />
                }
            />
        </>
    );
};

export default observer(TicketsPagination);
