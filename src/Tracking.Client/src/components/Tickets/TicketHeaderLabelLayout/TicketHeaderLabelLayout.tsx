import React from 'react';
import style from '../../../style/layout/header.module.scss';

interface TicketHeaderLabelLayoutProps {
    title: string;
    children: React.ReactNode;
    isBlocked?: boolean;
}

const TicketHeaderLabelLayout: React.FC<TicketHeaderLabelLayoutProps> = ({
    isBlocked = false,
    title,
    children,
}) => {
    return (
        <div
            className={`${style.ticketHeaderLabel} ${
                isBlocked ? style.ticketHeaderLabelBlocked : ''
            }`}
        >
            <span>{title}</span>
            <span className={style.ticketHeaderValue}>{children}</span>
        </div>
    );
};

export default TicketHeaderLabelLayout;
