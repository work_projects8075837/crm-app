import Loader from '@/Loader/Loader';
import CreateCounterPartyForm from '@/components/Form/CreateCounterPartyForm/CreateCounterPartyForm';
import Layout from '@/components/NavbarLayout/Layout';
import { useCounterPartyBreadCrumbs } from '@/store/queries/stores/counterparty/useCounterPartyBreadCrumbs';
import { useCounterPartyByParams } from '@/store/queries/stores/counterparty/useCounterPartyByParams';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import CounterPartyFiles from '../CounterPartyFiles/CounterPartyFiles';

interface CounterPartyCreateProviderProps {
    children: React.ReactNode;
}

const CounterPartyCreateProvider: React.FC<CounterPartyCreateProviderProps> = ({
    children,
}) => {
    const { data } = useCounterPartyByParams();
    const { isLoading } = useCounterPartyBreadCrumbs({ refetchOnMount: true });
    return (
        <>
            {data && !isLoading ? (
                <Layout childrenClass={style.formLayout}>
                    <CreateCounterPartyForm
                        modals={
                            <>
                                <CounterPartyFiles />
                            </>
                        }
                        parentCounterParty={data}
                    >
                        {children}
                    </CreateCounterPartyForm>
                </Layout>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default CounterPartyCreateProvider;
