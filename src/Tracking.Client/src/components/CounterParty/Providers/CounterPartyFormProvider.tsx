import Loader from '@/Loader/Loader';
import UpdateCounterPartyForm from '@/components/Form/UpdateCounterPartyForm/UpdateCounterPartyForm';
import Layout from '@/components/NavbarLayout/Layout';
import { useCounterPartyBreadCrumbs } from '@/store/queries/stores/counterparty/useCounterPartyBreadCrumbs';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import CounterPartyFiles from '../CounterPartyFiles/CounterPartyFiles';
import { useCounterPartyByParams } from './../../../store/queries/stores/counterparty/useCounterPartyByParams';

interface CounterPartyFormProviderProps {
    children: React.ReactNode;
}

const CounterPartyFormProvider: React.FC<CounterPartyFormProviderProps> = ({
    children,
}) => {
    const { data } = useCounterPartyByParams({ refetchOnMount: true });
    const { isLoading } = useCounterPartyBreadCrumbs({ refetchOnMount: true });

    return (
        <>
            {data && !isLoading ? (
                <Layout childrenClass={style.formLayout}>
                    <UpdateCounterPartyForm
                        modals={
                            <>
                                <CounterPartyFiles />
                            </>
                        }
                        defaultValues={{
                            ...data,
                        }}
                    >
                        {children}
                    </UpdateCounterPartyForm>
                </Layout>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default CounterPartyFormProvider;
