import FormSelect from '@/components/Elements/FormSelect/FormSelect';
import CounterPartyFullSelect from '@/components/Fields/CounterPartySelect/CounterPartyFullSelect';
import EditorInput from '@/components/Fields/EditorInput/EditorInput';
import {
    COUNTER_PARTY_COMMENT_NAME,
    COUNTER_PARTY_MRN_NAME,
    COUNTER_PARTY_NAME_NAME,
    COUNTER_PARTY_TIN_NAME,
} from '@/components/Fields/EntitiesFields/counter_party';
import FormShortInput from '@/components/Fields/FormShortInput/FormShortInput';
import EntityFieldLayout from '@/components/Fields/Layouts/EntityFieldLayout';
import DoubleColumnsLayout from '@/components/Layouts/DoubleColumnsLayout/DoubleColumnsLayout';
import ParentEntityExceptProvider from '@/components/SalePoints/Providers/ParentEntityExceptProvider/ParentEntityExceptProvider';
import React from 'react';

const CounterPartyGeneral: React.FC = () => {
    return (
        <>
            <DoubleColumnsLayout
                left={
                    <>
                        <FormSelect title='Основное'>
                            <EntityFieldLayout title='Название'>
                                <FormShortInput
                                    name={COUNTER_PARTY_NAME_NAME}
                                    placeholder='Название'
                                />
                            </EntityFieldLayout>

                            <CounterPartyFullSelect
                                onlyDirectory={true}
                                title='Папка контрагентов'
                            />

                            <ParentEntityExceptProvider>
                                <EntityFieldLayout title='ИНН'>
                                    <FormShortInput
                                        name={COUNTER_PARTY_TIN_NAME}
                                        type='number'
                                        placeholder='ИНН'
                                    />
                                </EntityFieldLayout>

                                <EntityFieldLayout title='ОГРН'>
                                    <FormShortInput
                                        name={COUNTER_PARTY_MRN_NAME}
                                        type='number'
                                        placeholder='ОГРН'
                                    />
                                </EntityFieldLayout>
                            </ParentEntityExceptProvider>
                        </FormSelect>
                    </>
                }
                right={
                    <>
                        <ParentEntityExceptProvider>
                            <FormSelect title='Комментарий'>
                                <EditorInput
                                    name={COUNTER_PARTY_COMMENT_NAME}
                                />
                            </FormSelect>
                        </ParentEntityExceptProvider>
                    </>
                }
            />
        </>
    );
};

export default CounterPartyGeneral;
