import FilesModal from '@/components/Fields/FilesModal/FilesModal';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';

export const counterPartyFilesOpen = new OpenHandler(false);

const CounterPartyFiles: React.FC = () => {
    return (
        <>
            <FilesModal openHandler={counterPartyFilesOpen} name='file' />
        </>
    );
};

export default CounterPartyFiles;
