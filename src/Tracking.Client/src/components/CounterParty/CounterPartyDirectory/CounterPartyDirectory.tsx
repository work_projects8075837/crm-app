import Layout from '@/components/NavbarLayout/Layout';
import React from 'react';
import CounterPartyDirectoryProvider from './CounterPartyDirectoryProvider';
import CounterPartyDirectoryHeader from './CounterPartyDirectoryHeader';
import CounterPartyList from '../CounterPartyList/CounterPartyList';

const CounterPartyDirectory: React.FC = () => {
    return (
        <>
            <Layout>
                <CounterPartyDirectoryProvider>
                    <CounterPartyDirectoryHeader />
                    <CounterPartyList />
                </CounterPartyDirectoryProvider>
            </Layout>
        </>
    );
};

export default CounterPartyDirectory;
