import Loader from '@/Loader/Loader';
import { useCounterPartyBreadCrumbs } from '@/store/queries/stores/counterparty/useCounterPartyBreadCrumbs';
import React from 'react';

interface CounterPartyDirectoryProviderProps {
    children: React.ReactNode;
}

const CounterPartyDirectoryProvider: React.FC<
    CounterPartyDirectoryProviderProps
> = ({ children }) => {
    const { isLoading } = useCounterPartyBreadCrumbs({ refetchOnMount: true });

    return <>{isLoading ? <Loader /> : children}</>;
};

export default CounterPartyDirectoryProvider;
