import BreadCrumbLink from '@/components/Elements/BreadCrumbs/BreadCrumbLink';
import CounterPartiesLink from '@/components/Elements/BreadCrumbs/CounterPartiesLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import BaseDirectoryHeader from '@/components/SalePoints/SalePointsHeader/BaseDirectoryHeader';
import { useCounterPartyBreadCrumbs } from '@/store/queries/stores/counterparty/useCounterPartyBreadCrumbs';
import React from 'react';

const CounterPartyDirectoryHeader: React.FC = () => {
    const { data, current } = useCounterPartyBreadCrumbs();

    return (
        <>
            <BaseDirectoryHeader
                breadcrumbs={
                    <>
                        <CounterPartiesLink />
                        <InnerBreadCrumbs links={data} route={'counterparty'} />
                    </>
                }
                title={
                    <BreadCrumbLink to={`/counterparty/single/${current?.id}`}>
                        {current?.name}
                    </BreadCrumbLink>
                }
                link={`/counterparty/create/${current?.id}`}
                directoryText='Новая папка контрагентов'
                entityText='Новый контрагент'
            />
        </>
    );
};

export default CounterPartyDirectoryHeader;
