import CreateCounterPartyForm from '@/components/Form/CreateCounterPartyForm/CreateCounterPartyForm';
import Layout from '@/components/NavbarLayout/Layout';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import CounterPartyFiles from '../CounterPartyFiles/CounterPartyFiles';
import CounterPartyCreateElements from './CounterPartyCreateElements';

const CounterPartyCreate: React.FC = () => {
    return (
        <>
            <Layout childrenClass={style.formLayout}>
                <CreateCounterPartyForm
                    modals={
                        <>
                            <CounterPartyFiles />
                        </>
                    }
                >
                    <CounterPartyCreateElements />
                </CreateCounterPartyForm>
            </Layout>
        </>
    );
};

export default CounterPartyCreate;
