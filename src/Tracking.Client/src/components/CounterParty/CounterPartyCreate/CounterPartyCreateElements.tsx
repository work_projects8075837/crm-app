import CounterPartiesLink from '@/components/Elements/BreadCrumbs/CounterPartiesLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import OneEntityBaseHeader from '@/components/SalePoints/SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';
import EntityOptionsLayout from '@/components/Tickets/TicketOptions/EntityOptionsLayout';
import TicketOptionButton from '@/components/Tickets/TicketOptions/TicketOptionButton';
import { useCounterPartyBreadCrumbs } from '@/store/queries/stores/counterparty/useCounterPartyBreadCrumbs';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import { counterPartyFilesOpen } from '../CounterPartyFiles/CounterPartyFiles';
import CounterPartyGeneral from '../CounterPartyGeneral/CounterPartyGeneral';

const CounterPartyCreateElements: React.FC = () => {
    const { data } = useCounterPartyBreadCrumbs();
    return (
        <>
            <OneEntityBaseHeader
                title='Новый контрагент'
                breadcrumbs={
                    <>
                        <CounterPartiesLink />{' '}
                        <InnerBreadCrumbs links={data} route='counterparty' /> /
                        Новый контрагент
                    </>
                }
            />
            <EntityOptionsLayout>
                <TicketOptionButton
                    onClick={() => counterPartyFilesOpen.open()}
                    symbol='ticket_file'
                    text='Файлы'
                    svgSize={{ width: 9, height: 13 }}
                />
            </EntityOptionsLayout>
            <div className={style.formTabsButtons}>
                <CircleButton isActive={true} text='Общая информация' />
            </div>
            <div className={style.afterTabsMargin}>
                <CounterPartyGeneral />
            </div>
        </>
    );
};

export default CounterPartyCreateElements;
