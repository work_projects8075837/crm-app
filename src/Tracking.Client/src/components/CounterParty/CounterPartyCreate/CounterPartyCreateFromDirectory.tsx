import React from 'react';
import CounterPartyCreateProvider from '../Providers/CounterPartyCreateProvider';
import CounterPartyCreateElements from './CounterPartyCreateElements';

const CounterPartyCreateFromDirectory: React.FC = () => {
    return (
        <>
            <CounterPartyCreateProvider>
                <CounterPartyCreateElements />
            </CounterPartyCreateProvider>
        </>
    );
};

export default CounterPartyCreateFromDirectory;
