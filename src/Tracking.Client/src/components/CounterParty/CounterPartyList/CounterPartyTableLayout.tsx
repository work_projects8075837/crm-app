import TableLayout from '@/components/Tables/TableLayout/TableLayout';
import React from 'react';
import style from '../../../style/layout/counter_party.module.scss';
import TableHeader from '@/components/Tables/HeaderLayout/TableHeader';
import CounterPartyPagination from './CounterPartyPagination';
import { counterPartyCheckHandler } from './CounterPartyList';

interface CounterPartyTableLayoutProps {
    children: React.ReactNode;
}

const fields = [{ name: 'Название' }, { name: 'ИНН' }, { name: 'ОГРН' }];

const CounterPartyTableLayout: React.FC<CounterPartyTableLayoutProps> = ({
    children,
}) => {
    return (
        <>
            <TableLayout
                layoutClass={style.entityPaginatedTable}
                header={
                    <TableHeader
                        checkHandler={counterPartyCheckHandler}
                        fields={fields}
                        layoutClass={style.rowLayout}
                        fieldClass={style.headerField}
                    />
                }
                pagination={<CounterPartyPagination />}
            >
                {children}
            </TableLayout>
        </>
    );
};

export default CounterPartyTableLayout;
