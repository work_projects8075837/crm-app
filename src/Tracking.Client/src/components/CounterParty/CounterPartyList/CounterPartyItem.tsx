import DirectoryIcon from '@/components/Elements/DirectoryIcon/DirectoryIcon';
import CheckItem from '@/components/Fields/CheckItem/CheckItem';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/counter_party.module.scss';
import { counterPartyCheckHandler } from './CounterPartyList';

interface CounterPartyItemProps {
    id: string;
    name: string;
    tin: string;
    tag: string;
    is_parent: boolean;
}

const CounterPartyItem: React.FC<CounterPartyItemProps> = ({
    id,
    name,
    tin,
    tag,
    is_parent,
}) => {
    const navigate = useNavigate();

    return (
        <>
            <div
                className={style.tableItem}
                onDoubleClick={() => {
                    if (is_parent) {
                        navigate(`/counterparty/${id}/`);
                    } else {
                        navigate(`/counterparty/single/${id}/`);
                    }
                }}
            >
                <CheckItem
                    checkHandler={counterPartyCheckHandler}
                    entity={{ id, name, tin, tag }}
                />
                <span className={style.tableItemName}>
                    <span>{name}</span>
                    {is_parent && <DirectoryIcon />}
                </span>

                <span className={style.tableItemField}>
                    <span>{tin}</span>
                </span>

                <span className={style.tableItemField}>
                    <span>{tag}</span>
                </span>
            </div>
        </>
    );
};

export default CounterPartyItem;
