import LoaderCircle from '@/components/Elements/LoaderCircle/LoaderCircle';
import NullEntities from '@/components/Elements/NullEntities/NullEntities';
import EntitySelectedActions from '@/components/Elements/SelectedActions/EntitySelectedActions/EntitySelectedActions';
import { counterPartyApi } from '@/services/counterparty/counterparty';
import {
    GET_ALL_COUNTER_PARTIES_KEY,
    GET_SEARCH_COUNTER_PARTIES_KEY,
} from '@/store/queries/keys/keys';
import { useCounterPartiesTable } from '@/store/queries/stores/counterparty/useCounterPartiesTable';
import { CheckEntitiesHandler } from '@/store/states/CheckEntitiesHandler';
import { useSetCheckableEntities } from '@/store/states/hooks/useSetCheckableEntities';
import React from 'react';
import style from '../../../style/elements/loader_text.module.scss';
import CounterPartyItem from './CounterPartyItem';
import { counterPartyPagination } from './CounterPartyPagination';
import CounterPartyTableLayout from './CounterPartyTableLayout';

export const counterPartyCheckHandler = new CheckEntitiesHandler();

const CounterPartyList: React.FC = () => {
    const { data, refetch } = useCounterPartiesTable(counterPartyPagination, {
        refetchOnMount: true,
    });

    useSetCheckableEntities(counterPartyCheckHandler, data?.data);

    return (
        <>
            {data ? (
                <>
                    {data.data.length ? (
                        <>
                            <CounterPartyTableLayout>
                                {data.data.map((c) => (
                                    <CounterPartyItem key={c.id} {...c} />
                                ))}
                            </CounterPartyTableLayout>
                        </>
                    ) : (
                        <NullEntities text='Нет контрагентов' />
                    )}
                </>
            ) : (
                <LoaderCircle size={100} className={style.loaderPlaceFull} />
            )}
            <EntitySelectedActions
                keys={[
                    GET_ALL_COUNTER_PARTIES_KEY,
                    GET_SEARCH_COUNTER_PARTIES_KEY,
                ]}
                checkHandler={counterPartyCheckHandler}
                service={counterPartyApi}
                refetch={refetch}
            ></EntitySelectedActions>
        </>
    );
};

export default CounterPartyList;
