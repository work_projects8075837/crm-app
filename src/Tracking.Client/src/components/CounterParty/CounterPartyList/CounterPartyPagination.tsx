import Pagination from '@/components/Tables/Pagination/Pagination';
import PaginationButtonsList from '@/components/Tickets/TiketsPaginationButtonsList/PaginationButtonsList';
import { PaginationHandler } from '@/store/states/PaginationHandler.ts';
import React from 'react';
import { useCounterPartiesTable } from '@/store/queries/stores/counterparty/useCounterPartiesTable';
import { observer } from 'mobx-react-lite';

const COUNTER_PARTY_PAGINATION = 'COUNTER_PARTY_PAGINATION';
export const counterPartyPagination = new PaginationHandler(
    COUNTER_PARTY_PAGINATION,
);

const CounterPartyPagination: React.FC = () => {
    const { data } = useCounterPartiesTable(counterPartyPagination);
    return (
        <>
            <Pagination
                paginationHandler={counterPartyPagination}
                pages={
                    <PaginationButtonsList
                        data={data}
                        paginationHandler={counterPartyPagination}
                    />
                }
            />
        </>
    );
};

export default observer(CounterPartyPagination);
