import CounterPartiesLink from '@/components/Elements/BreadCrumbs/CounterPartiesLink';
import InnerBreadCrumbs from '@/components/Elements/BreadCrumbs/InnerBreadCrumbs';
import OneEntityBaseHeader from '@/components/SalePoints/SalePointsCreate/SalePointsCreateHeader/OneEntityBaseHeader';
import { useCounterPartyBreadCrumbs } from '@/store/queries/stores/counterparty/useCounterPartyBreadCrumbs';
import React from 'react';

const CounterPartyEditHeader: React.FC = () => {
    const { data, current } = useCounterPartyBreadCrumbs();
    return (
        <>
            <OneEntityBaseHeader
                title={current?.name}
                breadcrumbs={
                    <>
                        <CounterPartiesLink />{' '}
                        <InnerBreadCrumbs links={data} route='counterparty' />
                    </>
                }
            />
        </>
    );
};

export default CounterPartyEditHeader;
