import CircleButton from '@/components/Elements/CircleButton/CircleButton';
import ModalMenu from '@/components/Elements/ModalMenu/ModalMenu';
import ActionsMenuDeleteButton from '@/components/Fields/ActionsMenuDeleteButton/ActionsMenuDeleteButton';
import EntityOptionsLayout from '@/components/Tickets/TicketOptions/EntityOptionsLayout';
import TicketOptionButton from '@/components/Tickets/TicketOptions/TicketOptionButton';
import { counterPartyApi } from '@/services/counterparty/counterparty';
import { GET_ALL_COUNTER_PARTIES_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import style from '../../../style/layout/layout.module.scss';
import { counterPartyFilesOpen } from '../CounterPartyFiles/CounterPartyFiles';
import CounterPartyGeneral from '../CounterPartyGeneral/CounterPartyGeneral';
import CounterPartyFormProvider from '../Providers/CounterPartyFormProvider';
import CounterPartyEditHeader from './CounterPartyEditHeader';

const CounterPartyEdit: React.FC = () => {
    return (
        <>
            <CounterPartyFormProvider>
                <CounterPartyEditHeader />
                <EntityOptionsLayout>
                    <ModalMenu symbol='ticket_edit' text='Действия'>
                        <ActionsMenuDeleteButton
                            queryKey={GET_ALL_COUNTER_PARTIES_KEY}
                            service={counterPartyApi}
                            idName='counterPartyId'
                            navigateTo='/counterparty/'
                        />
                    </ModalMenu>
                    <TicketOptionButton
                        onClick={() => counterPartyFilesOpen.open()}
                        symbol='ticket_file'
                        text='Файлы'
                        svgSize={{ width: 9, height: 13 }}
                    />
                </EntityOptionsLayout>
                <div className={style.formTabsButtons}>
                    <CircleButton isActive={true} text='Общая информация' />
                </div>
                <div className={style.afterTabsMargin}>
                    <CounterPartyGeneral />
                </div>
            </CounterPartyFormProvider>
        </>
    );
};

export default CounterPartyEdit;
