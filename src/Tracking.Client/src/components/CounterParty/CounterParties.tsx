import React from 'react';
import Layout from '../NavbarLayout/Layout';
import BaseDirectoryHeader from '../SalePoints/SalePointsHeader/BaseDirectoryHeader';
import CounterPartyList from './CounterPartyList/CounterPartyList';

const CounterParties: React.FC = () => {
    return (
        <>
            <Layout>
                <BaseDirectoryHeader
                    title='Список контрагентов'
                    link='/counterparty/create/'
                    directoryText='Новая папка контрагентов'
                    entityText='Новый контрагент'
                />
                <CounterPartyList />
            </Layout>
        </>
    );
};

export default CounterParties;
