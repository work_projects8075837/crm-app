import { ITicketHistory } from '@/services/history/type';
import { IStatus } from '@/services/status/types';
import { ITicket } from '@/services/ticket/types';
import { Entity } from '../Fields/SelectEntity/SelectEntity';

const castObjectString = (s: unknown) => {
    return typeof s === 'string' ? s : !s ? null : '{} ';
};

const ignoreFields = [
    'id',
    'number',
    'comment',
    'file',
    'job_result',
    'ticket_id',
    'is_parent',
    'created_at',
    'duration_time',
    'jobs_time',
    'tag',
] as const;

type IgnoreField = (typeof ignoreFields)[number];

export type TRecoveryTicket = Omit<ITicket, IgnoreField>;

export type THistoryItemValue = IHistoryItem['old'];

export type IHistoryItem =
    | {
          type: 'string';
          old: string | null;
          current: string | null;
          user: { name: string };
          created_at: string;
          field: keyof Omit<ITicket, IgnoreField>;
      }
    | {
          type: 'editor';
          old: string | null;
          current: string | null;
          user: { name: string };
          created_at: string;
          field: keyof Omit<ITicket, IgnoreField>;
      }
    | {
          type: 'entity';
          old: Entity | null;
          current: Entity | null;
          user: { name: string };
          created_at: string;
          field: keyof Omit<ITicket, IgnoreField>;
      }
    | {
          type: 'entities';
          old: Entity[] | null;
          current: Entity[];
          user: { name: string };
          created_at: string;
          field: keyof Omit<ITicket, IgnoreField>;
      }
    | {
          type: 'status';
          old: IStatus | null;
          current: IStatus;
          user: { name: string };
          created_at: string;
          field: keyof Omit<ITicket, IgnoreField>;
      };

type TMapFields<T, V> = {
    [K in keyof T]: V;
};

export const fieldsDictionary: TMapFields<
    Omit<ITicket, IgnoreField>,
    string
> = {
    channel: 'Канал связи',
    direction: 'Направление',
    character_problem: 'Характер проблемы',
    contact: 'Контакт',
    sale_point: 'Заведение',
    counterparty: 'Контрагент',
    inventory: 'Проблемное оборудование',
    manager: 'Вовлеченные сотрудники',
    closed_at: 'Закрытие заявки',
    decision: 'Решение',
    description: 'Описание',
    finished_at: 'Завершение заявки',
    start_at: 'Начало заявки',
    status: 'Статус',
};

export const formatHistory = (histories: ITicketHistory[]) => {
    const historyItemsArr = histories.map((h) => {
        const currentState = h.current_state;
        const previousState = h.previous_state;
        const historyItems: IHistoryItem[] = [];

        for (const k in currentState) {
            if (!ignoreFields.includes(k as IgnoreField)) {
                const key = k as keyof Omit<ITicket, IgnoreField>;

                const currentTicketValue = currentState[key];
                const oldTicketValue = previousState?.[key];

                switch (true) {
                    case key === 'decision' || key === 'description':
                        if (
                            (currentTicketValue || '') !==
                            (oldTicketValue || '')
                        ) {
                            historyItems.push({
                                type: 'editor',
                                old: castObjectString(oldTicketValue),
                                current: castObjectString(currentTicketValue),
                                field: key,
                                ...h,
                            });
                        }

                        break;
                    case typeof currentTicketValue === 'string':
                        if (
                            (currentTicketValue || '') !==
                            (oldTicketValue || '')
                        ) {
                            historyItems.push({
                                type: 'string',
                                old: castObjectString(oldTicketValue),
                                current: castObjectString(currentTicketValue),
                                field: key,
                                ...h,
                            });
                        }

                        break;

                    case currentTicketValue instanceof Array:
                        if (
                            currentTicketValue.some((entity, i) => {
                                const oldTicketValueArr =
                                    oldTicketValue as Entity[];
                                entity.id !== oldTicketValueArr[i]?.id;
                            })
                        ) {
                            historyItems.push({
                                type: 'entities',
                                old: oldTicketValue as Entity[],
                                current: currentTicketValue,
                                field: key,
                                ...h,
                            });
                        }

                        break;
                    case currentTicketValue &&
                        typeof currentTicketValue !== 'string' &&
                        'background' in currentTicketValue:
                        if (
                            currentTicketValue.id !==
                            (oldTicketValue as IStatus | null)?.id
                        ) {
                            historyItems.push({
                                type: 'status',
                                old: oldTicketValue as IStatus,
                                current: currentTicketValue,
                                field: key,
                                ...h,
                            });
                        }
                        break;

                    default:
                        if (
                            currentTicketValue?.id &&
                            currentTicketValue.name &&
                            currentTicketValue.id !==
                                (oldTicketValue as Entity)?.id
                        ) {
                            historyItems.push({
                                type: 'entity',
                                old: oldTicketValue as Entity,
                                current: currentTicketValue,
                                field: key,
                                ...h,
                            });
                        }

                        break;
                }
            }
        }

        return historyItems;
    });

    return historyItemsArr.reduce((arr, historyItems) => {
        return [...arr, ...historyItems];
    }, []);
};
