export const generateCreateId = () => {
    return `created_${(+new Date()).toString(16)}`;
};
