export const countHours = (number?: number, withSeconds = false) => {
    if (!number) {
        return withSeconds
            ? { hours: 0, minutes: 0, seconds: 0 }
            : { hours: 0, minutes: 0 };
    }
    const oneSecond = 1000;
    const oneMinute = 60 * oneSecond;
    const oneHour = 60 * oneMinute;

    const seconds = Math.floor((number % oneMinute) / oneSecond);
    const minutes = Math.floor((number % oneHour) / oneMinute);
    const hours = Math.floor(number / oneHour);

    return withSeconds ? { hours, minutes, seconds } : { hours, minutes };
};
