const concatZero = (dateNumber: number) => {
    const dateChar = dateNumber.toString();
    return dateChar.length === 1 ? '0' + dateChar : dateChar;
};

export const formatDate = (UTCDate?: string | null | number) => {
    if (!UTCDate) {
        return;
    }
    const date = new Date(UTCDate);

    const dateDate = concatZero(date.getDate());
    const dateMonth = concatZero(date.getMonth() + 1);
    const dateMinutes = concatZero(date.getMinutes());
    const dateHours = concatZero(date.getHours());

    return `${dateDate}.${dateMonth}.${date.getFullYear()} ${dateHours}:${dateMinutes}`;
};

export const formatDateWithoutTime = (UTCDate?: string | null | number) => {
    if (!UTCDate) {
        return;
    }
    const date = new Date(UTCDate);

    const dateDate = concatZero(date.getDate());
    const dateMonth = concatZero(date.getMonth() + 1);

    return `${dateDate}.${dateMonth}.${date.getFullYear()}`;
};
