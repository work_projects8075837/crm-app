import toast from 'react-hot-toast';

export const toastSuccessUpdate = (text = 'Сохранено!') => {
    toast.success(text);
};
