import { formatDate } from './formatDate';

export const calculateSla = (
    duration?: number,
    fromTime: number = new Date().getTime(),
) => {
    if (!duration) return;
    const dateSla = new Date(fromTime + duration);

    return formatDate(dateSla.toUTCString());
};
