export const getIdNumber = (id?: string) => {
    return id?.split('-')[0].toUpperCase();
};
