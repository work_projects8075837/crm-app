export const formatPhoneLink = (phone?: string) => {
    return phone?.replaceAll(/[^0-9]/g, '');
};
