export const performExistCallback = (callback?: () => any) => {
    if (callback) callback();
};
