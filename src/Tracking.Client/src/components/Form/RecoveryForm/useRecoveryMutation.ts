import { authService } from '@/services/auth/auth';
import { IRecovery } from '@/services/auth/types.ts';
import { useMutation } from '@tanstack/react-query';
import { IFormErrorMutation } from '../Types/Types';
import { toast } from 'react-hot-toast';
import { SUCCESS_DURATION } from '@/components/Providers/ToastOptions/toastOptions';
import { useNavigate } from 'react-router-dom';

export const useRecoveryMutation = ({ onError }: IFormErrorMutation) => {
    const navigate = useNavigate();
    const mutation = useMutation({
        mutationFn: async ({ email }: IRecovery) => {
            return authService.recoveryPassword({ email });
        },
        onSuccess: () => {
            toast.success('Ссылка на восстановление пароля у вас на почте', {
                duration: SUCCESS_DURATION,
            });
            navigate('/login/');
        },
        onError,
    });

    return mutation;
};
