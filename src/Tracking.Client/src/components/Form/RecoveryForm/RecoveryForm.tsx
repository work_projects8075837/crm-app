import React from 'react';
import { useBaseForm } from '../hooks/useBaseForm';
import { IRecovery } from '@/services/auth/types.ts';
import BaseForm from '../BaseForm/BaseForm';
import LoginFieldLayout from '../LoginForm/layouts/LabelFieldLayout';
import EmailInput from '@/components/Fields/EmailInput/EmailInput';
import { useRecoveryMutation } from './useRecoveryMutation';
import { SERVER_FORM_ERROR_NAME } from '../Types/Types';
import { m } from 'framer-motion';
import SubmitButton from '@/components/Fields/Buttons/SubmitButton';
import RootError from '@/components/Fields/RootError/RootError';
import { ERROR_DURATION } from '@/components/Providers/ToastOptions/toastOptions';
import { toast } from 'react-hot-toast';

const RecoveryForm: React.FC = () => {
    const { form } = useBaseForm<IRecovery>();
    const recoveryMutation = useRecoveryMutation({
        onError: () => {
            toast.error('Пользователя с таким логином не существует', {
                duration: ERROR_DURATION,
            });
        },
    });

    const onSubmit = form.handleSubmit((data) => {
        recoveryMutation.mutate(data);
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: 'form--container' }}
            >
                <LoginFieldLayout label='Логин'>
                    <EmailInput name='email' placeholder='E-mail' />
                </LoginFieldLayout>
                <SubmitButton
                    text='Отправить'
                    isLoading={recoveryMutation.isLoading}
                />
                <RootError />
            </BaseForm>
        </>
    );
};

export default RecoveryForm;
