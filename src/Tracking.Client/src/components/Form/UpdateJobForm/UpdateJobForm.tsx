import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { IJobForm } from '@/components/Fields/EntitiesFields/job';
import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_JOBS_KEY,
    GET_SEARCH_JOBS_KEY,
} from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateJob } from './useUpdateJob';

interface UpdateJobFormProps {
    openHandler: OpenHandler;
    defaultValues?: IJobForm;
    children: React.ReactNode;
}

const UpdateJobForm: React.FC<UpdateJobFormProps> = ({
    openHandler,
    defaultValues,
    children,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<IJobForm>({ defaultValues });
    const { mutate, isLoading } = useUpdateJob();
    const onSubmit = form.handleSubmit((data) => {
        mutate(
            { ...data, job_id: data.job?.id },
            {
                onSuccess: (newJob) => {
                    invalidateKey(GET_ALL_JOBS_KEY);
                    totalClearKey(GET_SEARCH_JOBS_KEY);
                    openHandler.close();
                    navigate(`/jobs/${newJob.job_id || ''}`);
                },
            },
        );
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: style.modalVertAround }}
            >
                {children}
                <SaveButton {...{ isLoading }}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default UpdateJobForm;
