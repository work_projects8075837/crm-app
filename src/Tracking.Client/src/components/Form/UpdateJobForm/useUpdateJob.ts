import { updatableJobId } from '@/components/Jobs/Providers/JobCreateProvider';
import { jobApi } from '@/services/job/job';
import { IUpdateJob } from '@/services/job/types';
import { useMutation } from '@tanstack/react-query';

export const useUpdateJob = () => {
    const jobId = updatableJobId.value;
    const m = useMutation({
        mutationFn: async (data: IUpdateJob) => {
            if (!jobId) {
                throw Error();
            }
            return await jobApi.update(jobId, data).then((res) => res.data);
        },
    });
    return m;
};
