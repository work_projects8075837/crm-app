import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import {
    JOB_DURATION_NAME,
    JOB_NAME_NAME,
} from '@/components/Fields/EntitiesFields/job';
import DurationInput from '@/components/Fields/DurationInput/DurationInput';

const JobFormFields: React.FC = () => {
    return (
        <>
            <div className={style.modalVert}>
                <FormInput name={JOB_NAME_NAME} />
                <DurationInput name={JOB_DURATION_NAME} />
            </div>
        </>
    );
};

export default JobFormFields;
