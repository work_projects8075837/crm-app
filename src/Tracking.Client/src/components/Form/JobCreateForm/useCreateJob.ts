import { jobApi } from '@/services/job/job';
import { useMutation } from '@tanstack/react-query';
import { ICreateJob } from './../../../services/job/types';

export const useCreateJob = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreateJob) => {
            return await jobApi.create(data).then((res) => res.data);
        },
    });
    return mutation;
};
