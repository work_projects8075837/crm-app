import FormInput from '@/components/Fields/BaseFields/FormInput';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import DurationInput from '@/components/Fields/DurationInput/DurationInput';
import {
    IJobForm,
    JOB_DURATION_NAME,
    JOB_NAME_NAME,
} from '@/components/Fields/EntitiesFields/job';
import SelectJob from '@/components/Fields/SelectJob/SelectJob';
import { useQueryParams } from '@/hooks/useQueryParams';
import { IJob } from '@/services/job/types';
import { invalidateKey } from '@/store/queries/actions/base';
import {
    GET_ALL_JOBS_KEY,
    GET_SEARCH_JOBS_KEY,
} from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateJob } from './useCreateJob';

interface JobCreateFormProps {
    defaultValues?: Partial<IJobForm>;
    openHandler: OpenHandler;
    showParentField?: boolean;
    add?: (data: IJob) => void;
}

const JobCreateForm: React.FC<JobCreateFormProps> = ({
    openHandler,
    add,
    showParentField = true,
    defaultValues,
}) => {
    const { is_parent } = useQueryParams();
    const { form } = useBaseForm<IJobForm>({
        defaultValues: {
            ...defaultValues,
            is_parent: !!is_parent,
        },
    });

    const { mutate, isLoading } = useCreateJob();

    const onSubmit = form.handleSubmit((data) => {
        mutate(
            { ...data, job_id: data.job?.id },
            {
                onSuccess: (newJob) => {
                    if (add && !data.is_parent) add(newJob);
                    openHandler.close();
                    invalidateKey(GET_ALL_JOBS_KEY);
                    invalidateKey(GET_SEARCH_JOBS_KEY);
                },
            },
        );
    });

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ className: style.modalVertAround, onSubmit }}
            >
                <FormInput name={JOB_NAME_NAME} placeholder='Название' />

                {showParentField && <SelectJob onlyDirectory={true} />}

                {!is_parent && (
                    <DurationInput required={false} name={JOB_DURATION_NAME} />
                )}

                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default JobCreateForm;
