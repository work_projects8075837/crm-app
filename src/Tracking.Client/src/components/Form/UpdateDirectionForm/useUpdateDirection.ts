import { directionApi } from '@/services/direction/direction';
import { ICreateDirection } from '@/services/direction/types';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useUpdateDirection = () => {
    const { directionId } = useParams();
    const mutation = useMutation({
        mutationFn: async (data: Partial<ICreateDirection>) => {
            if (!directionId) {
                throw Error();
            }
            return await directionApi
                .update(directionId, data)
                .then((res) => res.data);
        },
    });

    return mutation;
};
