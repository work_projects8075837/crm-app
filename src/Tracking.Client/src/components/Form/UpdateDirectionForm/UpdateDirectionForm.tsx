import { IDirectionForm } from '@/components/Fields/EntitiesFields/direction';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import AdminGuard from '@/components/Providers/RolesGuard/AdminGuard';
import { performExistCallback } from '@/components/utils/performExistCallback';
import { toastSuccessUpdate } from '@/components/utils/toastSuccessUpdate';
import { totalClearKey } from '@/store/queries/actions/base';
import { queryClient } from '@/store/queries/client/client';
import {
    GET_ALL_DIRECTIONS_KEY,
    GET_ONE_DIRECTION_BY_ID_KEY,
    GET_SEARCH_DIRECTIONS_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateDirection } from './useUpdateDirection';

interface UpdateDirectionFormProps {
    children: React.ReactNode;
    defaultValues: IDirectionForm;
    modals?: React.ReactNode;
}

const UpdateDirectionForm: React.FC<UpdateDirectionFormProps> = ({
    defaultValues,
    children,
    modals,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<IDirectionForm>({ defaultValues });
    const { mutate, isLoading } = useUpdateDirection();

    const onSubmit = (onSuccess?: () => void) => {
        form.handleSubmit((data) => {
            mutate(data, {
                onSuccess: () => {
                    queryClient.invalidateQueries({
                        queryKey: [GET_ONE_DIRECTION_BY_ID_KEY],
                    });

                    totalClearKey(GET_ALL_DIRECTIONS_KEY);
                    totalClearKey(GET_SEARCH_DIRECTIONS_KEY);
                    toastSuccessUpdate();

                    performExistCallback(onSuccess);
                },
            });
        })();
    };

    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                htmlFormProps={{
                    className: style.formWindow,
                    onSubmit: (event) => event.preventDefault(),
                }}
            >
                <div className={style.formChildren}>{children}</div>
                <AdminGuard>
                    <FormBottom
                        isLoading={isLoading}
                        onSaveClick={() => {
                            onSubmit();
                        }}
                        onSaveCloseClick={() => {
                            onSubmit(() => {
                                navigate('/direction/');
                            });
                        }}
                    />
                </AdminGuard>
            </BaseForm>
        </>
    );
};

export default UpdateDirectionForm;
