import { FieldValues, UseFormProps, useForm } from 'react-hook-form';

export const useBaseForm = <FieldsType extends FieldValues>(
    useFormProps?: UseFormProps<FieldsType>,
) => {
    const form = useForm<FieldsType>({ ...useFormProps, mode: 'onChange' });

    return { form };
};
