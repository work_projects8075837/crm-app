import { contactApi } from '@/services/contact/contact';
import { CreateContact, IContact } from '@/services/contact/types';
import { useMutation } from '@tanstack/react-query';

interface UseCreateContactMutation {
    onSuccess: (data: IContact) => void;
}

export const useCreateContactMutation = ({
    onSuccess,
}: UseCreateContactMutation) => {
    const mutation = useMutation({
        mutationFn: async (data: CreateContact) => {
            return await contactApi.create(data).then((res) => res.data);
        },
        onSuccess,
    });

    return mutation;
};
