import AnyEmailInput from '@/components/Fields/AnyEmailInput/AnyEmailInput';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import {
    CONTACT_EMAIL_NAME,
    CONTACT_NAME_NAME,
} from '@/components/Fields/EntitiesFields/contact';
import {
    TICKET_CONTACT_NAME,
    TICKET_SALE_POINT_NAME,
} from '@/components/Fields/EntitiesFields/ticket';
import SearchSelectPhoneNumber from '@/components/Fields/SearchSelectPhoneNumber/SearchSelectPhoneNumber';
import { useFormEntityValue } from '@/components/Fields/hooks/useFormEntityValue';
import { CreateContact } from '@/services/contact/types';
import { ticketKeys } from '@/services/ticket/ticket';
import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import { GET_ALL_CONTACTS_KEY } from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateContactMutation } from './useCreateContactMutation';

interface CreateContactFormProps {
    openHandler?: OpenHandler;
    defaultName?: string;
    defaultPhone?: string;
}

const CreateContactForm: React.FC<CreateContactFormProps> = ({
    openHandler,
    defaultName,
    defaultPhone,
}) => {
    const { form } = useBaseForm<Omit<CreateContact, 'sale_points'>>({
        defaultValues: { name: defaultName || '' },
    });

    // useEffect(() => {
    //     form.setValue('name', defaultName || '');
    // }, []);

    const formSalePoint = useFormEntityValue(TICKET_SALE_POINT_NAME);
    const formContact = useFormEntityValue(TICKET_CONTACT_NAME);

    const { mutate, isLoading } = useCreateContactMutation({
        onSuccess: (contact) => {
            totalClearKey(GET_ALL_CONTACTS_KEY);
            invalidateKey(ticketKeys.GET_ALL_KEY);

            formContact.set(contact);
            openHandler?.close();
        },
    });

    const onSubmit = form.handleSubmit((data) => {
        mutate({
            ...data,
        });
    });

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{
                    className: style.modalVertAround,
                    onSubmit,
                }}
            >
                <FormInput name={CONTACT_NAME_NAME} placeholder='Введите ФИО' />
                <AnyEmailInput
                    name={CONTACT_EMAIL_NAME}
                    placeholder='E-mail'
                    options={{ required: false }}
                />

                <SearchSelectPhoneNumber defaultSearch={defaultPhone} />

                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default CreateContactForm;
