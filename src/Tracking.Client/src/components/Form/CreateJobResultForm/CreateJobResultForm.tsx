import SaveButton from '@/components/Fields/Buttons/SaveButton';
import DurationInput from '@/components/Fields/DurationInput/DurationInput';
import { useJobResultToTicketMutation } from '@/components/Tickets/TicketJobs/TicketJobItem/hooks/useJobResultToTicketMutation';
import { queryClient } from '@/store/queries/client/client';
import { GET_ONE_TICKET_BY_ID_KEY } from '@/store/queries/keys/keys';
import { resultJobModalState } from '@/store/states/ResultJobModalState.ts';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { toast } from 'react-hot-toast';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';

interface ICreateJobResultForm {
    time: number;
}

const CreateJobResultForm: React.FC = () => {
    const { form } = useBaseForm<ICreateJobResultForm>();
    const { mutate, isLoading } = useJobResultToTicketMutation();

    const onSubmit = form.handleSubmit((data) => {
        if (resultJobModalState.jobId) {
            mutate(
                {
                    job_id: resultJobModalState.jobId,
                    time: data.time,
                },
                {
                    onSuccess: () => {
                        queryClient.invalidateQueries({
                            queryKey: [GET_ONE_TICKET_BY_ID_KEY],
                        });
                        resultJobModalState.openHandler.close();
                        toast.success(
                            `Задача "${resultJobModalState.jobName}" выполнена`,
                        );
                    },
                },
            );
        }
    });

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: style.modalVertAround }}
            >
                <DurationInput name='time' />
                <SaveButton isLoading={isLoading}>Выполнить</SaveButton>
            </BaseForm>
        </>
    );
};

export default observer(CreateJobResultForm);
