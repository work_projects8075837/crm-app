import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_CASES_KEY,
    GET_ONE_CASE_BY_ID_KEY,
    GET_ONE_TICKET_BY_ID_KEY,
} from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler';
import React, { useEffect } from 'react';
import style from '../../../style/layout/side_menu.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { ICaseForm } from '../CreateCaseForm/CreateCaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateCase } from './useUpdateCase';

interface UpdateCaseFormProps {
    defaultValues: Partial<ICaseForm>;
    openHandler: OpenHandler;
    children: React.ReactNode;
}

const UpdateCaseForm: React.FC<UpdateCaseFormProps> = ({
    defaultValues,
    openHandler,
    children,
}) => {
    const { form } = useBaseForm<ICaseForm>({ defaultValues });
    const { mutate, isLoading } = useUpdateCase();
    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: () => {
                totalClearKey(GET_ALL_CASES_KEY);
                invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
                totalClearKey(GET_ONE_CASE_BY_ID_KEY);

                openHandler.close();
            },
        });
    });

    useEffect(() => {
        return () => openHandler.close();
    }, []);

    return (
        <>
            <BaseForm
                {...form}
                submit={{ isDisabled: isLoading }}
                htmlFormProps={{
                    onSubmit,
                    className: style.sideBarFormWithBottom,
                }}
            >
                {children}
            </BaseForm>
        </>
    );
};

export default UpdateCaseForm;
