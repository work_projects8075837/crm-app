import { caseId } from '@/components/Tickets/CaseDetail/CaseDetail';
import { caseApi } from '@/services/case/case';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useMutation } from '@tanstack/react-query';
import { ICaseForm } from '../CreateCaseForm/CreateCaseForm';
import { createManager } from '../CreateCaseForm/useCreateCase';

export const useUpdateCase = () => {
    const ticketQuery = useTicketByParams();

    const m = useMutation({
        mutationFn: async (data: ICaseForm) => {
            if (!ticketQuery.data || !caseId.value) {
                throw Error();
            }

            const manager = await createManager({
                ticketData: ticketQuery.data,
                user: data.user,
            });

            return await caseApi
                .update(caseId.value, {
                    ...data,
                    manager_id: manager.id,
                })
                .then((res) => res.data);
        },
    });
    return m;
};
