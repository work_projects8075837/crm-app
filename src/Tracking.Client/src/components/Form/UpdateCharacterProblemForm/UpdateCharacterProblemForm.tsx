import { ICharacterProblemForm } from '@/components/Fields/EntitiesFields/character_problem';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import AdminGuard from '@/components/Providers/RolesGuard/AdminGuard';
import { toastSuccessUpdate } from '@/components/utils/toastSuccessUpdate';
import { ICharacterProblem } from '@/services/character_problem/types';
import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import { queryClient } from '@/store/queries/client/client';
import {
    GET_ALL_CHARACTER_PROBLEMS_KEY,
    GET_ONE_CHARACTER_PROBLEM_BY_ID_KEY,
    GET_ONE_TICKET_BY_ID_KEY,
    GET_SEARCH_CHARACTER_PROBLEMS_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateCharacterProblem } from './useUpdateCharacterProblem';

interface UpdateCharacterProblemFormProps {
    modals: React.ReactNode;
    children: React.ReactNode;
    defaultValues: ICharacterProblemForm;
}

const UpdateCharacterProblemForm: React.FC<UpdateCharacterProblemFormProps> = ({
    children,
    defaultValues,
    modals,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<ICharacterProblemForm>({ defaultValues });
    const { mutate, isLoading } = useUpdateCharacterProblem();

    const onSubmit = (onSuccess?: (data: ICharacterProblem) => void) => {
        form.handleSubmit((data) => {
            mutate(
                {
                    ...data,
                    character_problem_id: data.character_problem?.id,
                },
                {
                    onSuccess: (data) => {
                        queryClient.invalidateQueries({
                            queryKey: [GET_ONE_CHARACTER_PROBLEM_BY_ID_KEY],
                        });

                        invalidateKey(GET_ONE_TICKET_BY_ID_KEY);

                        totalClearKey(GET_ALL_CHARACTER_PROBLEMS_KEY);
                        totalClearKey(GET_SEARCH_CHARACTER_PROBLEMS_KEY);
                        toastSuccessUpdate();

                        onSuccess?.(data);
                    },
                },
            );
        })();
    };
    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                htmlFormProps={{
                    className: style.formWindow,
                    onSubmit: (event) => event.preventDefault(),
                }}
            >
                <div className={style.formChildren}>{children}</div>
                <AdminGuard>
                    <FormBottom
                        isLoading={isLoading}
                        onSaveClick={() => {
                            onSubmit();
                        }}
                        onSaveCloseClick={() => {
                            onSubmit((data) =>
                                navigate(
                                    `/character_problem/${
                                        data.character_problem_id
                                            ? `${data.character_problem_id}/`
                                            : ''
                                    }`,
                                ),
                            );
                        }}
                    />
                </AdminGuard>
            </BaseForm>
        </>
    );
};

export default UpdateCharacterProblemForm;
