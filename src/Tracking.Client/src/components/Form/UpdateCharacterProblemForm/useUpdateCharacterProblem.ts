import { characterProblemApi } from '@/services/character_problem/character_problem';
import { ICreateCharacterProblem } from '@/services/character_problem/types';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useUpdateCharacterProblem = () => {
    const { characterProblemId } = useParams();

    const mutation = useMutation({
        mutationFn: async (data: Partial<ICreateCharacterProblem>) => {
            if (!characterProblemId) {
                throw Error();
            }

            return await characterProblemApi
                .update(characterProblemId, {
                    ...data,
                })
                .then((res) => res.data);
        },
    });
    return mutation;
};
