import CheckboxInput from '@/components/Fields/BaseFields/CheckboxInput';
import SubmitButton from '@/components/Fields/Buttons/SubmitButton';
import EmailInput from '@/components/Fields/EmailInput/EmailInput';
import PasswordInput from '@/components/Fields/PasswordInput/PasswordInput';
import { ERROR_DURATION } from '@/components/Providers/ToastOptions/toastOptions';
import { ILogin } from '@/services/auth/types.ts';
import React from 'react';
import { toast } from 'react-hot-toast';
import styles from '../../../style/forms/login.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import LoginFormLayout from './LoginFormLayout';
import LoginFieldLayout from './layouts/LabelFieldLayout';
import { useLoginMutation } from './useLoginMutation';

export interface LoginForm extends ILogin {
    isRemember?: boolean;
}

const LoginForm: React.FC = () => {
    const { form } = useBaseForm<LoginForm>({
        defaultValues: {
            isRemember: true,
        },
    });

    const loginMutation = useLoginMutation({
        onError: () => {
            toast.error('Неверный email или пароль', {
                duration: ERROR_DURATION,
            });
        },
    });

    const onSubmit = form.handleSubmit(
        async ({ email, password, isRemember }) => {
            loginMutation.mutate({ email, password, isRemember });
        },
    );

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: 'form--container' }}
            >
                <LoginFormLayout
                    remeberMeInput={
                        <CheckboxInput
                            label='Запомнить меня'
                            name='isRemember'
                            labelClassName={styles.remember}
                        />
                    }
                >
                    <LoginFieldLayout label='Логин'>
                        <EmailInput name='email' placeholder='E-mail' />
                    </LoginFieldLayout>
                    <LoginFieldLayout label='Пароль'>
                        <PasswordInput
                            name='password'
                            placeholder='********'
                            hard={false}
                        />
                    </LoginFieldLayout>
                </LoginFormLayout>
                <SubmitButton
                    text={'Войти'}
                    isLoading={loginMutation.isLoading}
                />
            </BaseForm>
        </>
    );
};

export default LoginForm;
