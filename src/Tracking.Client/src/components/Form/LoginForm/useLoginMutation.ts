import { authService } from '@/services/auth/auth';
import { CookiesStorage } from '@/services/utils/CookieStorage';
import { parseJwt } from '@/services/utils/parseJwt/parseJwt';
import { queryClient } from '@/store/queries/client/client';
import { GET_AUTHORIZATION_KEY } from '@/store/queries/keys/auth';
import { useMutation } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import { LoginForm } from './LoginForm';

interface UseLoginMutationProps {
    onError: () => void;
}

export const useLoginMutation = ({ onError }: UseLoginMutationProps) => {
    const navigate = useNavigate();

    const loginMutation = useMutation({
        mutationFn: async ({ email, password }: LoginForm) => {
            return authService.login({ email, password });
        },
        onSuccess: ({ data }, { isRemember }) => {
            if (isRemember) CookiesStorage.setItem('refresh', data.refresh);
            CookiesStorage.setItem('access', data.access);
            const { sub, roles } = parseJwt(data.access);
            queryClient.setQueryData([GET_AUTHORIZATION_KEY], {
                userId: sub,
                roles,
            });

            navigate('/');
        },
        onError,
    });

    return loginMutation;
};
