import React from 'react';

interface LabelFieldLayoutProps {
    children: React.ReactNode;
    isLabel?: boolean;
    label: string;
    className?: string;
}

const LabelFieldLayout: React.FC<LabelFieldLayoutProps> = ({
    children,
    isLabel = true,
    label,
    className = '',
}) => {
    return (
        <>
            {isLabel ? (
                <label className={`form-group mb ${className}`}>
                    <span className='label'>{label}</span>
                    {children}
                </label>
            ) : (
                <div className={`form-group mb ${className}`}>
                    <span className='label'>{label}</span>
                    {children}
                </div>
            )}
        </>
    );
};

export default LabelFieldLayout;
