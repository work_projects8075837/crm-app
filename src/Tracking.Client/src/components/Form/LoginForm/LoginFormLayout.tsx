import React from 'react';
import { Link } from 'react-router-dom';

interface LoginFormLayoutProps {
    children: React.ReactNode;
    remeberMeInput: React.ReactNode;
}

const LoginFormLayout: React.FC<LoginFormLayoutProps> = ({
    children,
    remeberMeInput,
}) => {
    return (
        <>
            {children}
            <div className='password--refresh'>
                {remeberMeInput}

                <Link
                    to={'/recovery'}
                    className={'auth--btn auth--btn--margin'}
                >
                    Забыли пароль?
                </Link>
            </div>
        </>
    );
};

export default LoginFormLayout;
