import { IContactForm } from '@/components/Fields/EntitiesFields/contact';
import { IContact } from '@/services/contact/types';
import { ticketKeys } from '@/services/ticket/ticket';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_ALL_CONTACTS_KEY } from '@/store/queries/keys/keys';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import { toast } from 'react-hot-toast';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateOnlyContactMutation } from './useCreateOnlyContactMutation';
interface CreateOnlyContactFormProps {
    openHandler?: OpenHandler;
    children: React.ReactNode;
}

const CreateOnlyContactForm: React.FC<CreateOnlyContactFormProps> = ({
    openHandler,
    children,
}) => {
    const { form } = useBaseForm<IContactForm>();
    const { mutate, isLoading } = useCreateOnlyContactMutation();
    const currentUser = useCurrentUser();

    const onSubmit = (onSuccess?: (contact: IContact) => void) => {
        form.handleSubmit((data) => {
            mutate(
                { ...data, phone_number: data.phone_number || [] },
                {
                    onSuccess: (data) => {
                        if (onSuccess) onSuccess(data);
                        invalidateKey(GET_ALL_CONTACTS_KEY);
                        invalidateKey(ticketKeys.GET_ALL_KEY);

                        openHandler?.close();
                    },
                    onError: () => {
                        toast.error(
                            'Контакт с таким номером телефона уже существует',
                        );
                    },
                },
            );
        })();
    };
    return (
        <>
            <BaseForm
                {...form}
                submit={{
                    onSubmit,
                    isDisabled: isLoading,
                }}
                htmlFormProps={{
                    className: style.modalVertAround,
                    onSubmit: () => onSubmit(),
                }}
            >
                {children}
            </BaseForm>
        </>
    );
};

export default CreateOnlyContactForm;
