import AnyEmailInput from '@/components/Fields/AnyEmailInput/AnyEmailInput';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import {
    CONTACT_EMAIL_NAME,
    CONTACT_NAME_NAME,
} from '@/components/Fields/EntitiesFields/contact';
import SearchSelectPhoneNumber from '@/components/Fields/SearchSelectPhoneNumber/SearchSelectPhoneNumber';
import React from 'react';

const ContactFields: React.FC = () => {
    return (
        <>
            <FormInput name={CONTACT_NAME_NAME} placeholder='Введите ФИО' />
            <AnyEmailInput
                name={CONTACT_EMAIL_NAME}
                placeholder='E-mail'
                options={{ required: false }}
            />
            <SearchSelectPhoneNumber />
        </>
    );
};

export default ContactFields;
