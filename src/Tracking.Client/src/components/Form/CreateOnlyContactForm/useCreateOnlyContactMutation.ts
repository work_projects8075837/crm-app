import { contactApi } from '@/services/contact/contact';
import { CreateContact } from '@/services/contact/types';
import { useMutation } from '@tanstack/react-query';

export const useCreateOnlyContactMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: CreateContact) => {
            return await contactApi.create(data).then((res) => res.data);
        },
    });

    return mutation;
};
