import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { TICKET_MEMBERS_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { IShortMember } from '@/components/Tickets/hooks/useFormMembers';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';

interface AddMembersFormProps {
    children: React.ReactNode;
    openHandler: OpenHandler;
}

interface AddMembersProps {
    manager: IShortMember[];
}

const AddMembersForm: React.FC<AddMembersFormProps> = ({
    children,
    openHandler,
}) => {
    const relativeForm = useFormContext();

    const { form } = useBaseForm<AddMembersProps>({
        defaultValues: { manager: relativeForm.getValues(TICKET_MEMBERS_NAME) },
    });

    const onSubmit = form.handleSubmit(({ manager }) => {
        relativeForm.setValue(TICKET_MEMBERS_NAME, manager);
        openHandler.close();
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ className: style.modalVertAround, onSubmit }}
            >
                {children}
                <SaveButton>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default AddMembersForm;
