import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { IConnectionForm } from '@/components/Fields/EntitiesFields/connection';
import { INVENTORY_CONNECTIONS_NAME } from '@/components/Fields/EntitiesFields/inventory';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { IConnection } from '@/services/connection/types';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateConnection } from './useCreateConnection';

interface CreateConnectionFormProps {
    openHandler: OpenHandler;
    children: React.ReactNode;
}

const CreateConnectionForm: React.FC<CreateConnectionFormProps> = ({
    openHandler,
    children,
}) => {
    const { add } = useFormEntityValueArray<IConnection>(
        INVENTORY_CONNECTIONS_NAME,
    );
    const { form } = useBaseForm<IConnectionForm>();
    const { mutate, isLoading } = useCreateConnection();

    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: (newConnection) => {
                add(newConnection);
                openHandler.close();
            },
        });
    });

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{
                    className: style.modalVertAround,
                    onSubmit,
                }}
            >
                {children}
                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default CreateConnectionForm;
