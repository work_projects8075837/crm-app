import { IConnectionForm } from '@/components/Fields/EntitiesFields/connection';
import { configurationApi } from '@/services/configuration/configuration';
import { connectionApi } from '@/services/connection/connection';
import { useMutation } from '@tanstack/react-query';

export const useCreateConnection = () => {
    const m = useMutation({
        mutationFn: async (data: IConnectionForm) => {
            if (!data.type) {
                throw Error();
            }
            const configuration = await configurationApi
                .create({
                    ...data,
                })
                .then((res) => res.data);

            const connection = await connectionApi
                .create({
                    type: data.type.type,
                    configuration: [configuration],
                })
                .then((res) => res.data);

            return connection;
        },
    });

    return m;
};
