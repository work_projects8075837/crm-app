import { IInventoryForm } from '@/components/Fields/EntitiesFields/inventory';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { toastSuccessUpdate } from '@/components/utils/toastSuccessUpdate';
import { IInventory } from '@/services/inventory/types';
import { totalClearKey } from '@/store/queries/actions/base';
import { queryClient } from '@/store/queries/client/client';
import {
    GET_ALL_INVENTORIES_KEY,
    GET_ONE_INVENTORY_BY_ID_KEY,
    GET_SEARCH_INVENTORIES_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateInventory } from './useUpdateInventory';

interface UpdateInventoryFormProps {
    children: React.ReactNode;
    defaultValues: IInventoryForm;
    modals?: React.ReactNode;
}

const UpdateInventoryForm: React.FC<UpdateInventoryFormProps> = ({
    modals,
    children,
    defaultValues,
}) => {
    const navigate = useNavigate();
    const { salePointId } = useParams();
    const { form } = useBaseForm<IInventoryForm>({ defaultValues });
    const { mutate, isLoading } = useUpdateInventory();

    const onSubmit = (onSuccess?: (newInventory: IInventory) => void) => {
        form.handleSubmit((data) => {
            mutate(
                { ...data, inventory_id: data.inventory?.id },
                {
                    onSuccess: (newData) => {
                        queryClient.invalidateQueries({
                            queryKey: [GET_ONE_INVENTORY_BY_ID_KEY],
                        });

                        totalClearKey(GET_ALL_INVENTORIES_KEY);
                        totalClearKey(GET_SEARCH_INVENTORIES_KEY);
                        toastSuccessUpdate();

                        if (onSuccess) onSuccess(newData);
                    },
                },
            );
        })();
    };
    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                htmlFormProps={{
                    className: style.formWindow,
                    onSubmit: (event) => event.preventDefault(),
                }}
            >
                <div className={style.formChildren}>{children}</div>
                <FormBottom
                    isLoading={isLoading}
                    onSaveClick={() => {
                        onSubmit();
                    }}
                    onSaveCloseClick={() => {
                        onSubmit((data) => {
                            navigate(
                                `${
                                    salePointId
                                        ? `/sale_point/single/${salePointId}`
                                        : ''
                                }/${
                                    !data.connection.length || !salePointId
                                        ? `inventory/${data.inventory_id || ''}`
                                        : 'connections/'
                                }`,
                            );
                        });
                    }}
                />
            </BaseForm>
        </>
    );
};

export default UpdateInventoryForm;
