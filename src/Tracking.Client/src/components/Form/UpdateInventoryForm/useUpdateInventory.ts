import { inventoryApi } from '@/services/inventory/inventory';
import { IUpdateInventory } from '@/services/inventory/types';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useUpdateInventory = () => {
    const { inventoryId } = useParams();
    const mutation = useMutation({
        mutationFn: async (data: IUpdateInventory) => {
            if (!inventoryId) {
                throw Error();
            }
            return await inventoryApi
                .update(inventoryId, data)
                .then((res) => res.data);
        },
    });
    return mutation;
};
