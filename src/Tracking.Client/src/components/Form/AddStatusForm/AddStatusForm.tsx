import CheckboxInput from '@/components/Fields/BaseFields/CheckboxInput';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { DIRECTION_STATUSES_NAME } from '@/components/Fields/EntitiesFields/direction';
import {
    IStatusForm,
    STATUS_NAME_NAME,
    STATUS_SLA_NAME,
} from '@/components/Fields/EntitiesFields/status';
import StatusStyleSelect from '@/components/Fields/StatusStyleSelect/StatusStyleSelect';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { IStatus } from '@/services/status/types';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_ALL_STATUSES_KEY } from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useAddStatusMutation } from './useAddStatusMutation';

interface AddStatusFormProps {
    openHandler: OpenHandler;
}

const AddStatusForm: React.FC<AddStatusFormProps> = ({ openHandler }) => {
    const { form } = useBaseForm<IStatusForm>({
        defaultValues: { sla: true },
    });

    const { add } = useFormEntityValueArray<IStatus>(DIRECTION_STATUSES_NAME);

    const { mutate, isLoading } = useAddStatusMutation();

    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: (newStatus) => {
                add(newStatus);
                invalidateKey(GET_ALL_STATUSES_KEY);

                openHandler.close();
            },
        });
    });

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ className: style.modalVertAround, onSubmit }}
            >
                <FormInput name={STATUS_NAME_NAME} placeholder='Название' />
                <StatusStyleSelect
                    styles={[
                        {
                            color: '#E1980C',
                            background: '#FFE978',
                            preview_link: '/status/new.svg',
                        },
                        {
                            color: '#65A300',
                            background: '#C2FF5F',
                            preview_link: '/status/success.svg',
                        },
                        {
                            color: '#D50000',
                            background: '#FF7F7F',
                            preview_link: '/status/red.svg',
                        },
                    ]}
                />

                <CheckboxInput name={STATUS_SLA_NAME} label='Считать SLA?' />

                <SaveButton isLoading={isLoading} className='mt15'>
                    Сохранить
                </SaveButton>
            </BaseForm>
        </>
    );
};

export default AddStatusForm;

// const d = [
//     {
//         name: 'Новая',
//         sla: true,
//         is_finish: false,
//         is_default: true,
//         color: '#E1980C',
//         background: '#FFE978',
//         preview_link: '/status/new.svg',
//     },
//     {
//         name: 'В работе',
//         sla: true,
//         is_finish: false,
//         is_default: false,
//         color: '#E1980C',
//         background: '#FFE978',
//         preview_link: '/status/new.svg',
//     },
//     {
//         name: 'Возобновлена',
//         sla: false,
//         is_finish: false,
//         is_default: false,
//         color: '#E1980C',
//         background: '#FFE978',
//         preview_link: '/status/new.svg',
//     },
//     {
//         name: 'Завершена',
//         sla: false,
//         is_finish: true,
//         is_default: false,
//         color: '#65A300',
//         background: '#C2FF5F',
//         preview_link: '/status/success.svg',
//     },
// ];

// console.log(JSON.stringify(d));
