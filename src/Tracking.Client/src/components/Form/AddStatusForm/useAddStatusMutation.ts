import { statusApi } from '@/services/status/status';
import { ICreateStatus } from '@/services/status/types';
import { useMutation } from '@tanstack/react-query';

export const useAddStatusMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreateStatus) => {
            return await statusApi.create(data).then((res) => res.data);
        },
    });

    return mutation;
};
