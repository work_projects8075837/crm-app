import { IUser } from '@/services/user/types';
import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_CASES_KEY,
    GET_ONE_TICKET_BY_ID_KEY,
} from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';
import style from '../../../style/layout/side_menu.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateCase } from './useCreateCase';

export interface ICaseForm {
    name: string;
    finish_date: string;
    finish_time: string;
    user: IUser;
    comment: string;
}

interface CreateCaseFormProps {
    children: React.ReactNode;
    openHandler: OpenHandler;
}

const CreateCaseForm: React.FC<CreateCaseFormProps> = ({
    children,
    openHandler,
}) => {
    const { form } = useBaseForm<ICaseForm>();
    const { mutate, isLoading } = useCreateCase();

    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: () => {
                totalClearKey(GET_ALL_CASES_KEY);
                invalidateKey(GET_ONE_TICKET_BY_ID_KEY);

                openHandler.close();
            },
        });
    });
    return (
        <>
            <BaseForm
                {...form}
                submit={{ isDisabled: isLoading }}
                htmlFormProps={{
                    onSubmit,
                    className: style.sideBarFormWithBottom,
                }}
            >
                {children}
            </BaseForm>
        </>
    );
};

export default CreateCaseForm;
