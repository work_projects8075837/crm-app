import { TICKET_MEMBERS_TYPES } from '@/components/Fields/EntitiesFields/ticket';
import { caseApi } from '@/services/case/case';
import { memberApi } from '@/services/member/member';
import { ICurrentTicket } from '@/services/ticket/types';
import { IUser } from '@/services/user/types';
import { useTicketByParams } from '@/store/queries/stores/ticket/useTicketByParams';
import { useMutation } from '@tanstack/react-query';
import { ICaseForm } from './CreateCaseForm';

interface ICreateManagerProps {
    ticketData: ICurrentTicket;
    user: IUser;
}

export const createManager = async ({
    ticketData,
    user,
}: ICreateManagerProps) => {
    const findMember = ticketData.manager.find((m) => m.user.id === user.id);
    const ticketId = ticketData.id;
    if (findMember) {
        return findMember;
    } else {
        const manager = await memberApi
            .create({
                type: TICKET_MEMBERS_TYPES.involved,
                user_id: user.id,
                ticket_id: ticketId,
            })
            .then((res) => res.data);

        return manager;
    }
};

export const useCreateCase = () => {
    const ticketData = useTicketByParams();
    const ticketId = ticketData.data?.id;
    const m = useMutation({
        mutationFn: async (data: ICaseForm) => {
            if (!ticketId || !ticketData.data) {
                throw Error();
            }

            const manager = await createManager({
                ticketData: ticketData.data,
                user: data.user,
            });

            return await caseApi
                .create({
                    ...data,
                    ticket_id: ticketId,
                    manager_id: manager.id,
                    is_completed: false,
                })
                .then((res) => res.data);
        },
    });
    return m;
};
