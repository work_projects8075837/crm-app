import { generateQuery } from '@/services/utils/generateQuery/generateQuery';
import React from 'react';
import { FormProvider } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import SubmitProvider from '../BaseForm/SubmitProvider';
import { useBaseForm } from '../hooks/useBaseForm';

interface FilterFormProps {
    children: React.ReactNode;
}

type IFilterFormValue = string | number;

interface IFilterForm {
    [key: string]: IFilterFormValue | IFilterFormValue[];
}

const FilterForm: React.FC<FilterFormProps> = ({ children }) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<IFilterForm>({
        defaultValues: {
            is_hard: 'true',
        },
    });

    const onSubmit = form.handleSubmit((data) => {
        const workValues = Object.keys(data).reduce((obj, key) => {
            // if (key.includes('/')) {
            //     const keyValue = data[key] || undefined;
            //     const keys = key.split('/');
            //     const concatObj = keys.reduce(
            //         (o, k) => ({ ...o, [k]: keyValue }),
            //         {},
            //     );
            //     return { ...obj, ...concatObj };
            // }

            return { ...obj, [key]: data[key] || undefined };
        }, {});

        navigate(`./${generateQuery(workValues)}`);
    });

    return (
        <>
            <FormProvider {...form}>
                <SubmitProvider {...{ onSubmit }}>{children}</SubmitProvider>
            </FormProvider>
        </>
    );
};

export default FilterForm;
