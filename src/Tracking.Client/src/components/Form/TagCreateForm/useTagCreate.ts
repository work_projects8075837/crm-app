import { TICKET_TAGS_NAME } from '@/components/Fields/EntitiesFields/ticket';
import { ITag } from '@/services/tag/types';
import { useFormContext } from 'react-hook-form';

export const useTagCreate = () => {
    const relativeForm = useFormContext();

    const createTag = ({ name }: ITag) => {
        const tagsJson = relativeForm.getValues(TICKET_TAGS_NAME);

        if (!tagsJson) {
            relativeForm.setValue(TICKET_TAGS_NAME, JSON.stringify([{ name }]));
            return;
        }

        const tags: ITag[] = JSON.parse(tagsJson);
        relativeForm.setValue(
            TICKET_TAGS_NAME,
            JSON.stringify([...tags, { name }]),
        );
    };

    return createTag;
};
