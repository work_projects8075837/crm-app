import { tagApi } from '@/services/tag/tag';
import { ITag, TagCreate } from '@/services/tag/types';
import { useMutation } from '@tanstack/react-query';

interface UseTagCreateMutation {
    onSuccess?: (data: ITag) => void;
}

export const useTagCreateMutation = ({ onSuccess }: UseTagCreateMutation) => {
    const mutation = useMutation({
        mutationFn: async ({ name }: TagCreate) => {
            return tagApi.create({ name }).then((res) => res.data);
        },
        onSuccess: onSuccess,
    });

    return mutation;
};
