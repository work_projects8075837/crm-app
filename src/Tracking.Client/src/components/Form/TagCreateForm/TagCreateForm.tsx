import { TICKET_TAGS_NAME } from '@/components/Fields/EntitiesFields/ticket';
import TagsSearchCreateSelect from '@/components/Fields/TagsSearchCreateSelect/TagsSearchCreateSelect';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import TicketSaveButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveButton';
import { ITag } from '@/services/tag/types';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from './../hooks/useBaseForm';

interface TagCreateFormProps {
    close: () => void;
}

type TagForm = {
    tag: ITag[];
};

const TagCreateForm: React.FC<TagCreateFormProps> = ({ close }) => {
    const relativeForm = useFormContext();
    const { setArray } = useFormEntityValueArray(TICKET_TAGS_NAME);

    const { form } = useBaseForm<TagForm>({
        defaultValues: {
            tag: relativeForm.getValues(TICKET_TAGS_NAME),
        },
    });

    const onSubmit = form.handleSubmit(({ tag }) => {
        setArray(tag);
        close();
    });

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ className: style.modalVertAround, onSubmit }}
            >
                <TagsSearchCreateSelect />

                <TicketSaveButton />
            </BaseForm>
        </>
    );
};

export default TagCreateForm;
