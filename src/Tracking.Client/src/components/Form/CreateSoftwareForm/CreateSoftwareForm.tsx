import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { INVENTORY_SOFTWARE_NAME } from '@/components/Fields/EntitiesFields/inventory';
import { ISoftwareForm } from '@/components/Fields/EntitiesFields/software';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_SOFTWARE_KEY,
    GET_SEARCH_SOFTWARE_KEY,
} from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateSoftwareMutation } from './useCreateSoftwareMutation';

interface CreateSoftwareFormProps {
    children: React.ReactNode;
    openHandler: OpenHandler;
}

const CreateSoftwareForm: React.FC<CreateSoftwareFormProps> = ({
    children,
    openHandler,
}) => {
    const { add } = useFormEntityValueArray(INVENTORY_SOFTWARE_NAME);
    const { form } = useBaseForm<ISoftwareForm>();
    const { mutate, isLoading } = useCreateSoftwareMutation();
    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: (newSoftware) => {
                totalClearKey(GET_ALL_SOFTWARE_KEY);
                totalClearKey(GET_SEARCH_SOFTWARE_KEY);
                add(newSoftware);
                openHandler.close();
            },
        });
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ className: style.modalVertAround, onSubmit }}
            >
                {children}
                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default CreateSoftwareForm;
