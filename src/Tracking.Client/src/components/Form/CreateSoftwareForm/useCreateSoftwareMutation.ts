import { softwareApi } from '@/services/software/software';
import { ICreateSoftware } from '@/services/software/types';
import { useMutation } from '@tanstack/react-query';

export const useCreateSoftwareMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreateSoftware) => {
            return await softwareApi.create(data).then((res) => res.data);
        },
    });

    return mutation;
};
