import {
    ITicketForm,
    TICKET_MEMBERS_TYPES,
} from '@/components/Fields/EntitiesFields/ticket';
import TicketSaveButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveButton';
import TicketSaveCloseButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveCloseButton';
import { IChannel } from '@/services/channel/types';
import { IDirection } from '@/services/direction/types';
import { ITicket } from '@/services/ticket/types';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { duplicateTicketState } from '@/store/states/duplicateTicketState.ts';
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/ticket.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from './../hooks/useBaseForm';
import { formatTicketData } from './formatTicketCreateData';
import { useCreateTicketMutation } from './useCreateTicketMutation';

// console.log(formatTicketData(data));
// console.log(JSON.stringify(formatTicketData(data)));

interface TicketCreateFormProps {
    children: React.ReactNode;
    modals?: React.ReactNode;
    parentTicket?: ITicket;
    channel?: IChannel;
    direction?: IDirection;
}

const TicketCreateForm: React.FC<TicketCreateFormProps> = ({
    children,
    modals,
    parentTicket,
    channel,
    direction,
}) => {
    const { form } = useBaseForm<ITicketForm>({
        defaultValues: {
            ...duplicateTicketState.ticketForm,
            channel,
            direction,
            ticket: parentTicket && {
                id: parentTicket?.id,
                character_problem: parentTicket?.character_problem,
            },
            character_problem: undefined,
        },
    });

    const currentUser = useCurrentUser();
    const { mutate, isLoading } = useCreateTicketMutation();
    const navigate = useNavigate();

    useEffect(() => {
        if (duplicateTicketState.ticketForm) {
            duplicateTicketState.clear();
        }
    }, []);

    const onSubmit = (onSuccess?: (newTicket: ITicket) => void) => {
        form.handleSubmit((data) => {
            if (currentUser.data) {
                const currentUserShortMember = {
                    type: TICKET_MEMBERS_TYPES.author,
                    user: {
                        id: currentUser.data.id,
                        name: currentUser.data.name,
                        phone: currentUser.data.phone,
                    },
                };

                data.manager = data.manager
                    ? [...data.manager, currentUserShortMember]
                    : [currentUserShortMember];
            }

            mutate(formatTicketData({ ...data }), {
                onSuccess: async (newTicket) => {
                    if (onSuccess) onSuccess(newTicket);
                },
            });
        })();
    };

    return (
        <BaseForm
            outerFormChildren={modals}
            {...form}
            submit={{ isDisabled: isLoading, onSubmit }}
            htmlFormProps={{
                className: style.ticketCreateWindow,
                onSubmit: (event) => event.preventDefault(),
            }}
        >
            {children}
            {/* <TicketField /> */}
            <div className={style.ticketSubmitBottom}>
                {isLoading ? (
                    <>Сохранение...</>
                ) : (
                    <>
                        <TicketSaveButton
                            onClick={() =>
                                onSubmit((newTicket) =>
                                    navigate(`/ticket/${newTicket.id}/`),
                                )
                            }
                            className={style.ticketSubmitButtonLimited}
                        />
                        <TicketSaveCloseButton
                            onClick={() =>
                                onSubmit((newTicket) => {
                                    navigate(
                                        newTicket.ticket_id
                                            ? `/ticket/${newTicket.ticket_id}/sub_tickets/`
                                            : `/`,
                                    );
                                })
                            }
                            className={style.ticketSubmitButtonLimited}
                        />
                    </>
                )}
            </div>
        </BaseForm>
    );
};

export default TicketCreateForm;
