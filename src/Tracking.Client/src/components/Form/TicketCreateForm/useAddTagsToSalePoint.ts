import { salePointApi } from '@/services/sale_point/sale_point';
import { ISalePoint } from '@/services/sale_point/types';
import { ITag } from '@/services/tag/types';
import { useMutation } from '@tanstack/react-query';

interface IAddTagsToSalePointMutateData {
    salePointData: ISalePoint;
    tags: ITag[];
}

export const useAddTagsToSalePoint = () => {
    const mutation = useMutation({
        mutationFn: async ({
            tags,
            salePointData,
        }: IAddTagsToSalePointMutateData) => {
            if (!salePointData) {
                throw Error();
            }
            const uniqueTags = tags.filter(
                (tag) => !salePointData?.tag.some(({ id }) => id === tag.id),
            );
        },
    });
    return mutation;
};

export const addTagsToSalePoint = async ({
    tags,
    salePointData,
}: IAddTagsToSalePointMutateData) => {
    const uniqueTags = tags.filter(
        (tag) => !salePointData?.tag.some(({ id }) => id === tag.id),
    );

    await salePointApi.update(salePointData.id, {
        tag: [...salePointData.tag, ...uniqueTags],
    });
};
