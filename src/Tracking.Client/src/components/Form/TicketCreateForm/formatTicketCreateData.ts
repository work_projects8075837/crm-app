import { ITicketForm } from '@/components/Fields/EntitiesFields/ticket';

export const formatTicketData = (data: ITicketForm) => {
    const {
        channel,
        character_problem,
        contact,
        direction,
        sale_point,
        counterparty,
        status_id,
        file,
        is_parent,
        ticket,
    } = data;

    const ticketData = {
        channel_id: channel && channel.id,
        character_problem: character_problem,
        contact_id: contact && contact.id,
        contact: contact,
        description: data.description,
        decision: data.decision,
        direction_id: direction && direction.id,
        sale_point: sale_point,
        counterparty_id: counterparty && counterparty.id,
        status_id: status_id,
        file: file,
        tag: data?.tag,
        manager: data?.manager?.map((m) => ({ ...m, user_id: m.user.id })),
        ticket_id: ticket?.id,
        is_parent,
    };

    return ticketData;
};
