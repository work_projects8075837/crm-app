import { questionForm } from '@/components/Elements/DialogForm/questionForm';
import FormInput from '@/components/Fields/BaseFields/FormInput';
import { IContact } from '@/services/contact/types';
import { contactPositionApi } from '@/services/contact_position/contact_position';

export const addContactToSalePoint = async (
    contact: IContact,
    salePointId: string,
) => {
    const isIncluded = !!contact?.position.find(
        (p) => p.sale_point.id === salePointId,
    );
    if (!isIncluded) {
        const data = await questionForm<{ position: string }>({
            title: `Дожность ${contact.name}`,
            fields: (
                <>
                    <FormInput placeholder='Должность' name='position' />
                </>
            ),
        });

        await contactPositionApi.create({
            sale_point_id: salePointId,
            contact_id: contact.id,
            is_responsible: false,
            position: data?.position || 'Сотрудник',
        });
    }
};
