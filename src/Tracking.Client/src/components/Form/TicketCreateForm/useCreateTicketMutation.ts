import { ICharacterProblem } from '@/services/character_problem/types';
import { IContact } from '@/services/contact/types';
import { memberApi } from '@/services/member/member';
import { CreatableMember } from '@/services/member/types';
import { ISalePoint } from '@/services/sale_point/types';
import { ticketApi } from '@/services/ticket/ticket';
import { ICreateTicket } from '@/services/ticket/types';
import { IdEntity } from '@/services/types';
import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_CONTACTS_KEY,
    GET_All_TICKETS_KEY,
    GET_ONE_TICKET_BY_ID_KEY,
} from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';
import { addContactToSalePoint } from './addContactsToSalePoint';
import { addTagsToSalePoint } from './useAddTagsToSalePoint';

interface ICreateTicketMutation
    extends Omit<
        ICreateTicket,
        'manager' | 'contact_id' | 'sale_point_id' | 'character_problem_id'
    > {
    manager?: CreatableMember[];
    contact?: IContact;
    sale_point?: ISalePoint;
    character_problem?: ICharacterProblem;
}

export const createMembers = async (
    ticket_id: string,
    creatable_members?: CreatableMember[],
) => {
    const createdMembers: IdEntity[] = [];
    if (creatable_members) {
        await Promise.allSettled(
            creatable_members.map((createMember) =>
                memberApi
                    .create({ ...createMember, ticket_id })
                    .then((res) => res.data),
            ),
        ).then((promises) => {
            promises.forEach((result) => {
                if (result.status === 'fulfilled') {
                    createdMembers.push(result.value);
                }
            });
        });
    }
    return createdMembers;
};

export const useCreateTicketMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreateTicketMutation) => {
            if (data.contact && data.sale_point?.id) {
                await addContactToSalePoint(data.contact, data.sale_point?.id);
            }

            if (data.sale_point && data.character_problem) {
                await addTagsToSalePoint({
                    salePointData: data.sale_point,
                    tags: data.character_problem.tag,
                });
            }

            const newTicket = await ticketApi
                .create({
                    ...data,
                    character_problem_id: data.character_problem?.id,
                    sale_point_id: data.sale_point?.id,
                })
                .then((res) => res.data);

            if (newTicket.ticket_id) {
                await ticketApi.update(newTicket.ticket_id, {
                    is_parent: true,
                });
            }

            await createMembers(newTicket.id, data.manager);

            return newTicket;
        },
        onSuccess: async () => {
            invalidateKey(GET_ONE_TICKET_BY_ID_KEY);
            totalClearKey(GET_All_TICKETS_KEY);
            invalidateKey(GET_ALL_CONTACTS_KEY);
        },
    });

    return mutation;
};
