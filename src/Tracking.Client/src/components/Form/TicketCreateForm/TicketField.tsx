import React from 'react';
import { useFormContext, useWatch } from 'react-hook-form';

const TicketField: React.FC = () => {
    const { register } = useFormContext();
    const f = useWatch({ name: 'ticket' });
    return (
        <>
            <input
                {...register('ticket', {
                    required: false,
                })}
                disabled={true}
                className='hiddenInput'
            />
        </>
    );
};

export default TicketField;
