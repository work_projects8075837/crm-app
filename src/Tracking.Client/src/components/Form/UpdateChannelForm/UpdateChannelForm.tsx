import { channelsEditOpen } from '@/components/Channels/ChannelsEditModal/ChannelsEditModal';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { IChannelForm } from '@/components/Fields/EntitiesFields/channel';
import { channelKeys } from '@/services/channel/channel';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_ALL_CHANNELS_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateChannel } from './useUpdateChannel';

interface UpdateChannelFormProps {
    children: React.ReactNode;
    defaultValues?: IChannelForm;
}

const UpdateChannelForm: React.FC<UpdateChannelFormProps> = ({
    children,
    defaultValues,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<IChannelForm>({ defaultValues });
    const { mutate, isLoading } = useUpdateChannel();
    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: () => {
                invalidateKey(GET_ALL_CHANNELS_KEY);
                invalidateKey(channelKeys.GET_ONE_KEY);
                channelsEditOpen.close();
                navigate('/channel/');
            },
        });
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ className: style.modalVertAround, onSubmit }}
            >
                {children}
                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default UpdateChannelForm;
