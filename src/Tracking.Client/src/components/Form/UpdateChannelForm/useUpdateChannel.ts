import { channelApi } from '@/services/channel/channel';
import { IUpdateChannel } from '@/services/channel/types';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useUpdateChannel = () => {
    const { channelId } = useParams();
    const m = useMutation({
        mutationFn: async (data: IUpdateChannel) => {
            if (!channelId) {
                throw Error();
            }
            return await channelApi.update(channelId, data);
        },
    });

    return m;
};
