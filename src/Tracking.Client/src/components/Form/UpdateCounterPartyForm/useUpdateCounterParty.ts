import { counterPartyApi } from '@/services/counterparty/counterparty';
import { IUpdateCounterParty } from '@/services/counterparty/types';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useUpdateCounterParty = () => {
    const { counterPartyId } = useParams();
    const mutation = useMutation({
        mutationFn: async (data: IUpdateCounterParty) => {
            if (!counterPartyId) {
                throw Error();
            }
            return await counterPartyApi
                .update(counterPartyId, data)
                .then((res) => res.data);
        },
    });

    return mutation;
};
