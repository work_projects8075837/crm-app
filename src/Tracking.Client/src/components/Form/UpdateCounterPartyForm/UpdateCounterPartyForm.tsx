import { ICounterPartyForm } from '@/components/Fields/EntitiesFields/counter_party';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { toastSuccessUpdate } from '@/components/utils/toastSuccessUpdate';
import { ICounterparty } from '@/services/counterparty/types';
import { totalClearKey } from '@/store/queries/actions/base';
import { queryClient } from '@/store/queries/client/client';
import {
    GET_ALL_COUNTER_PARTIES_KEY,
    GET_ONE_COUNTER_PARTY_BY_ID_KEY,
    GET_SEARCH_COUNTER_PARTIES_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateCounterParty } from './useUpdateCounterParty';

interface UpdateCounterPartyFormProps {
    modals: React.ReactNode;
    children: React.ReactNode;
    defaultValues?: ICounterPartyForm;
}

const UpdateCounterPartyForm: React.FC<UpdateCounterPartyFormProps> = ({
    modals,
    children,
    defaultValues,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<ICounterPartyForm>({ defaultValues });
    const { mutate, isLoading } = useUpdateCounterParty();
    const onSubmit = (onSuccess?: (data: ICounterparty) => void) => {
        form.handleSubmit((data) => {
            mutate(
                {
                    ...data,
                    counterparty_id: data.counterparty?.id,
                },
                {
                    onSuccess: (UCounterParty) => {
                        queryClient.invalidateQueries({
                            queryKey: [GET_ONE_COUNTER_PARTY_BY_ID_KEY],
                        });

                        totalClearKey(GET_ALL_COUNTER_PARTIES_KEY);
                        totalClearKey(GET_SEARCH_COUNTER_PARTIES_KEY);
                        toastSuccessUpdate();

                        onSuccess?.(UCounterParty);
                    },
                },
            );
        })();
    };
    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                htmlFormProps={{
                    className: style.formWindow,
                    onSubmit: (event) => {
                        event.preventDefault();
                    },
                }}
            >
                <div className={style.formChildren}>{children}</div>

                <FormBottom
                    isLoading={isLoading}
                    onSaveClick={() => {
                        onSubmit();
                    }}
                    onSaveCloseClick={() => {
                        onSubmit((data) => {
                            navigate(
                                `/counterparty/${
                                    data.counterparty_id
                                        ? `${data.counterparty_id}/`
                                        : ''
                                }`,
                            );
                        });
                    }}
                />
            </BaseForm>
        </>
    );
};

export default UpdateCounterPartyForm;
