import { IConnectionForm } from '@/components/Fields/EntitiesFields/connection';
import { configurationApi } from '@/services/configuration/configuration';
import { connectionApi } from '@/services/connection/connection';
import { useMutation } from '@tanstack/react-query';
import { updatableConnection } from './UpdateConnectionForm';

export const useUpdateConnection = () => {
    const m = useMutation({
        mutationFn: async (data: IConnectionForm) => {
            if (!updatableConnection.value) {
                throw Error();
            }

            await configurationApi.update(
                updatableConnection.value.configuration[0].id,
                data,
            );

            const connection = connectionApi
                .update(updatableConnection.value?.id, {
                    type: data.type?.type,
                })
                .then((res) => res.data);

            return connection;
        },
    });
    return m;
};
