import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { IConnectionForm } from '@/components/Fields/EntitiesFields/connection';
import { INVENTORY_CONNECTIONS_NAME } from '@/components/Fields/EntitiesFields/inventory';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { IConnection } from '@/services/connection/types';
import { OpenHandler } from '@/store/states/OpenHandler';
import { ValueHandler } from '@/store/states/SearchHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateConnection } from './useUpdateConnection';

export const updatableConnection = new ValueHandler<IConnection>();

interface UpdateConnectionFormProps {
    children: React.ReactNode;
    openHandler: OpenHandler;
    defaultValues: IConnectionForm;
}

const UpdateConnectionForm: React.FC<UpdateConnectionFormProps> = ({
    children,
    defaultValues,
    openHandler,
}) => {
    const { add } = useFormEntityValueArray<IConnection>(
        INVENTORY_CONNECTIONS_NAME,
    );

    const { form } = useBaseForm<IConnectionForm>({ defaultValues });

    const { mutate, isLoading } = useUpdateConnection();

    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: (newConnection) => {
                add(newConnection);
                openHandler.close();
            },
        });
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{
                    className: style.modalVertAround,
                    onSubmit,
                }}
            >
                {children}
                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default observer(UpdateConnectionForm);
