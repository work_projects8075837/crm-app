export interface IFormErrorMutation {
    onError: () => void;
}

export interface OnSuccessMutationProps {
    onSuccess: () => void;
}

export const SERVER_FORM_ERROR_NAME = 'root.server' as const;
