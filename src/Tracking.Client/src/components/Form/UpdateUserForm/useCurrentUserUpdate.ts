import { authService } from '@/services/auth/auth';
import { IUpdateUser } from '@/services/auth/types';
import { useMutation } from '@tanstack/react-query';

export const useCurrentUserUpdate = () => {
    return useMutation({
        mutationFn: async (data: IUpdateUser) => {
            return await authService.update(data);
        },
    });
};
