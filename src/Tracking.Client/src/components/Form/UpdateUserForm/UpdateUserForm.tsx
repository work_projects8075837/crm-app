import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { performExistCallback } from '@/components/utils/performExistCallback';
import { IChannel } from '@/services/channel/types';
import { IDirection } from '@/services/direction/types';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_CURRENT_USER_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCurrentUserUpdate } from './useCurrentUserUpdate';

interface IUserForm {
    name: string;
    phone?: string;
    email: string;
    position: string;
    channel?: IChannel;
    direction?: IDirection;
}

interface UpdateUserFormProps {
    children: React.ReactNode;
    defaultValues: IUserForm;
}

const UpdateUserForm: React.FC<UpdateUserFormProps> = ({
    children,
    defaultValues,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<IUserForm>({ defaultValues });
    const { mutate, isLoading } = useCurrentUserUpdate();
    const onSubmit = (onSuccess?: () => void) =>
        form.handleSubmit((data) => {
            mutate(
                {
                    ...data,
                    channel_id: data.channel?.id || null,
                    direction_id: data.direction?.id,
                },
                {
                    onSuccess: () => {
                        invalidateKey(GET_CURRENT_USER_KEY);
                        performExistCallback(onSuccess);
                    },
                },
            );
        })();
    return (
        <>
            <BaseForm {...form} htmlFormProps={{ className: style.formWindow }}>
                <div className={style.formChildren}>{children}</div>
                <FormBottom
                    {...{ isLoading }}
                    onSaveClick={() => {
                        onSubmit();
                    }}
                    onSaveCloseClick={() => {
                        onSubmit(() => {
                            navigate('/');
                        });
                    }}
                />
            </BaseForm>
        </>
    );
};

export default UpdateUserForm;
