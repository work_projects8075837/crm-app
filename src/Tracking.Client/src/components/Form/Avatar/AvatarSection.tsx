import {ChangeEvent, FC} from "react";

export interface AvatarSectionProps {
    baseImage: string;
    user: any;
    uploadImage: (e: ChangeEvent<HTMLInputElement>) => void;
}

const AvatarSection: FC<AvatarSectionProps> = ({baseImage, user, uploadImage}) => (
    <div className="form-group profile--avatar">
        <div className="form-task form-profile">
            <label>Фотография</label>
            <label className="avatar-df">
                <img src={baseImage || user?.photo || "/user--navbar.svg"} alt="Avatar"/>
                <input type="file" onChange={uploadImage}/>
                <div className="profile--avatar--text">Загрузить фотографию</div>
            </label>
        </div>
    </div>
);

export default AvatarSection;