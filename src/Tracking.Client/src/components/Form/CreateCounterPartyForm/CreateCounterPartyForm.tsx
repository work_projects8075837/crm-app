import { ICounterPartyForm } from '@/components/Fields/EntitiesFields/counter_party';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { useQueryParams } from '@/hooks/useQueryParams';
import { ICounterparty } from '@/services/counterparty/types';
import { totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_COUNTER_PARTIES_KEY,
    GET_SEARCH_COUNTER_PARTIES_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateCounterPartyMutation } from './useCreateCounterPartyMutation';

interface CreateCounterPartyFormProps {
    children: React.ReactNode;
    parentCounterParty?: ICounterparty;
    modals?: React.ReactNode;
}

const CreateCounterPartyForm: React.FC<CreateCounterPartyFormProps> = ({
    children,
    parentCounterParty,
    modals,
}) => {
    const navigate = useNavigate();
    const { is_parent } = useQueryParams();
    const { form } = useBaseForm<ICounterPartyForm>({
        defaultValues: {
            counterparty: parentCounterParty,
            is_parent: !!is_parent,
        },
    });
    const { isLoading, mutate } = useCreateCounterPartyMutation();

    const onSubmit = (onSuccess: (newCounterParty: ICounterparty) => void) => {
        form.handleSubmit((data) => {
            mutate(
                {
                    ...data,
                    counterparty_id: data.counterparty?.id,
                },
                {
                    onSuccess: (newCounterParty) => {
                        totalClearKey(GET_ALL_COUNTER_PARTIES_KEY);
                        totalClearKey(GET_SEARCH_COUNTER_PARTIES_KEY);

                        if (onSuccess) onSuccess(newCounterParty);
                    },
                },
            );
        })();
    };
    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                htmlFormProps={{
                    onSubmit: (event) => event.preventDefault(),
                    className: style.formWindow,
                }}
            >
                <div className={style.formChildren}>{children}</div>
                <FormBottom
                    isLoading={isLoading}
                    onSaveClick={() => {
                        onSubmit((data) => {
                            navigate(`/counterparty/single/${data.id}/`);
                        });
                    }}
                    onSaveCloseClick={() => {
                        onSubmit((data) => {
                            navigate(
                                `/counterparty/${data.counterparty_id || ''}`,
                            );
                        });
                    }}
                />
            </BaseForm>
        </>
    );
};

export default CreateCounterPartyForm;
