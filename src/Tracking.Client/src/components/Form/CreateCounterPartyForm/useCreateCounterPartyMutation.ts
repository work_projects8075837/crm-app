import { counterPartyApi } from '@/services/counterparty/counterparty';
import { ICreateCounterParty } from '@/services/counterparty/types';
import { useMutation } from '@tanstack/react-query';

export const useCreateCounterPartyMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreateCounterParty) => {
            return await counterPartyApi.create(data).then((res) => res.data);
        },
    });

    return mutation;
};
