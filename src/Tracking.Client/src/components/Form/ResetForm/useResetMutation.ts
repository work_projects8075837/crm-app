import { useMutation } from '@tanstack/react-query';
import { IFormErrorMutation } from './../Types/Types';
import { authService } from '@/services/auth/auth';
import { IResetPassword } from '@/services/auth/types.ts';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';

export const useResetMutation = ({ onError }: IFormErrorMutation) => {
    const navigate = useNavigate();
    const mutation = useMutation({
        mutationFn: ({ password, token }: IResetPassword) => {
            return authService.resetPassword({ password, token });
        },
        onSuccess: () => {
            toast.success('Пароль успешно обновлён! Войдите в аккаунт');
            navigate('/login/');
        },
        onError,
    });

    return mutation;
};
