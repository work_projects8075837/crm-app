import SubmitButton from '@/components/Fields/Buttons/SubmitButton';
import PasswordInput from '@/components/Fields/PasswordInput/PasswordInput';
import RepeatPasswordInput from '@/components/Fields/RepeatPassword/RepeatPasswordInput';
import RootError from '@/components/Fields/RootError/RootError';
import { ERROR_DURATION } from '@/components/Providers/ToastOptions/toastOptions';
import { useQueryParams } from '@/hooks/useQueryParams';
import React from 'react';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import BaseForm from '../BaseForm/BaseForm';
import LoginFieldLayout from '../LoginForm/layouts/LabelFieldLayout';
import { useBaseForm } from '../hooks/useBaseForm';
import { useResetMutation } from './useResetMutation';

interface IResetForm {
    password: string;
    password_repeat: string;
}

const ResetForm: React.FC = () => {
    const navigate = useNavigate();
    const { token } = useQueryParams();

    const onError = () => {
        toast.error(
            'Эта ссылка устарела или была повреждена - получите новую',
            { duration: ERROR_DURATION },
        );
        navigate('/recovery/');
    };

    const { form } = useBaseForm<IResetForm>();
    const resetMutation = useResetMutation({ onError });
    const onSubmit = form.handleSubmit(({ password }) => {
        if (token) {
            resetMutation.mutate({ password, token });
        } else {
            onError();
        }
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: 'form--container' }}
            >
                <LoginFieldLayout label='Пароль'>
                    <PasswordInput
                        name='password'
                        placeholder='Придумайте пароль'
                    />
                </LoginFieldLayout>

                <LoginFieldLayout label='Пароль'>
                    <RepeatPasswordInput
                        placeholder='Подтвердите пароль'
                        name='password_repeat'
                    />
                </LoginFieldLayout>

                <SubmitButton
                    text={'Изменить пароль'}
                    isLoading={resetMutation.isLoading}
                />
                <RootError />
            </BaseForm>
        </>
    );
};

export default ResetForm;
