import { ITicketForm } from '@/components/Fields/EntitiesFields/ticket';

export const formatTicketUpdateData = (data: ITicketForm) => {
    const {
        channel,
        character_problem,
        contact,
        direction,
        sale_point,
        counterparty,
        status_id,
        file,
        tag,
        manager,
    } = data;

    const updatable_members = manager?.filter((member) => !!member.id);
    const creatable_members = manager
        ?.filter((member) => !member.id)
        .map((member) => {
            return {
                type: member.type,
                user_id: member.user.id,
            };
        });

    const ticketData = {
        channel_id: channel && channel.id,
        character_problem_id: character_problem?.id,
        contact_id: contact && contact.id,
        description: data.description,
        decision: data.decision,
        direction_id: direction && direction.id,
        sale_point_id: sale_point && sale_point.id,
        counterparty_id: counterparty && counterparty.id,
        status_id: status_id,
        file: file,
        tag: tag,
        updatable_members,
        creatable_members,
        contact,
    };

    return ticketData;
};
