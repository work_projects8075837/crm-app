import { ITicketForm } from '@/components/Fields/EntitiesFields/ticket';
import { formatTicketFormDefaultValues } from '@/components/Tickets/Providers/TicketEditFormProvider/formatTicketFormDefaultValues';
import TicketSaveButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveButton';
import TicketSaveCloseButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveCloseButton';
import { performExistCallback } from '@/components/utils/performExistCallback';
import { historyKeys } from '@/services/history/history';
import { invalidateKey, totalClearKey } from '@/store/queries/actions/base';
import {
    GET_All_TICKETS_KEY,
    GET_SEARCH_TICKETS_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { FormProvider } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/ticket.module.scss';
import SubmitProvider from '../BaseForm/SubmitProvider';
import { useBaseForm } from '../hooks/useBaseForm';
import { formatTicketUpdateData } from './formatTicketUpdateData';
import { useUpdateTicketMutation } from './useUpdateTicketMutation';

interface UpdateTicketFormProps {
    children: React.ReactNode;
    ticketForm: ITicketForm;
    modals?: React.ReactNode;
    formBottom?: React.ReactNode;
}

const UpdateTicketForm: React.FC<UpdateTicketFormProps> = ({
    children,
    ticketForm,
    modals,
    formBottom,
}) => {
    const { form } = useBaseForm<ITicketForm>({
        defaultValues: { ...ticketForm },
    });
    const { mutate, isLoading } = useUpdateTicketMutation();
    const navigate = useNavigate();
    const onSubmit = (onSuccess?: () => void) => {
        form.handleSubmit((data) => {
            mutate(formatTicketUpdateData(data), {
                onSuccess: (ticket) => {
                    totalClearKey(GET_All_TICKETS_KEY);
                    totalClearKey(GET_SEARCH_TICKETS_KEY);
                    invalidateKey(historyKeys.GET_ALL_KEY);

                    form.reset(formatTicketFormDefaultValues(ticket));

                    performExistCallback(onSuccess);
                },
            });
        })();
    };
    return (
        <>
            <FormProvider {...form}>
                <div className={style.ticketCreateWindow}>
                    <SubmitProvider {...{ onSubmit }}>
                        {children}

                        <div className={style.ticketSubmitBottom}>
                            {formBottom ? (
                                formBottom
                            ) : (
                                <>
                                    {isLoading ? (
                                        <>Сохранение...</>
                                    ) : (
                                        <>
                                            <TicketSaveButton
                                                onClick={() => onSubmit()}
                                                className={
                                                    style.ticketSubmitButtonLimited
                                                }
                                            />
                                            <TicketSaveCloseButton
                                                onClick={() =>
                                                    onSubmit(() => {
                                                        navigate('/');
                                                    })
                                                }
                                                className={
                                                    style.ticketSubmitButtonLimited
                                                }
                                            />
                                        </>
                                    )}
                                </>
                            )}
                        </div>
                    </SubmitProvider>
                </div>
                {modals}
            </FormProvider>
        </>
    );
};

export default UpdateTicketForm;
