import { IContact } from '@/services/contact/types';
import { CreatableMember } from '@/services/member/types';
import { ticketApi } from '@/services/ticket/ticket';
import { IUpdateTicket } from '@/services/ticket/types';
import { IdEntity } from '@/services/types';
import { queryClient } from '@/store/queries/client/client';
import { GET_ONE_TICKET_BY_ID_KEY } from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import { addContactToSalePoint } from '../TicketCreateForm/addContactsToSalePoint';
import { createMembers } from '../TicketCreateForm/useCreateTicketMutation';

interface IUpdateTicketMutation extends Partial<IUpdateTicket> {
    updatable_members?: IdEntity[];
    creatable_members?: CreatableMember[];
    contact?: IContact;
}

export const useUpdateTicketMutation = () => {
    const { ticketId } = useParams();
    const mutation = useMutation({
        mutationFn: async (data: IUpdateTicketMutation) => {
            if (!ticketId) {
                throw Error();
            }

            const createdMembers = await createMembers(
                ticketId,
                data.creatable_members,
            );

            if (data.contact && data.sale_point_id) {
                await addContactToSalePoint(data.contact, data.sale_point_id);
            }

            return await ticketApi
                .update(ticketId, {
                    ...data,
                })
                .then((res) => res.data);
        },
        onSuccess: async () => {
            await queryClient.invalidateQueries({
                queryKey: [GET_ONE_TICKET_BY_ID_KEY],
            });
        },
    });

    return mutation;
};
