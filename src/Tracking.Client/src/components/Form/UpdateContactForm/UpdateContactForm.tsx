import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { IContactForm } from '@/components/Fields/EntitiesFields/contact';
import { invalidateKey } from '@/store/queries/actions/base';
import {
    GET_ALL_CONTACTS_KEY,
    GET_ONE_CONTACT_BY_ID_KEY,
    GET_SEARCH_CONTACTS_KEY,
} from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateContact } from './useUpdateContact';

interface UpdateContactFormProps {
    children: React.ReactNode;
    defaultValues?: IContactForm;
    openHandler: OpenHandler;
}

const UpdateContactForm: React.FC<UpdateContactFormProps> = ({
    children,
    defaultValues,
    openHandler,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm({ defaultValues });
    const { mutate, isLoading } = useUpdateContact();
    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess() {
                invalidateKey(GET_ALL_CONTACTS_KEY);
                invalidateKey(GET_SEARCH_CONTACTS_KEY);
                invalidateKey(GET_ONE_CONTACT_BY_ID_KEY);
                openHandler.close();
                navigate('/contact/');
            },
        });
    });

    return (
        <>
            <BaseForm
                {...form}
                submit={{
                    onSubmit,
                    isDisabled: isLoading,
                }}
                htmlFormProps={{
                    className: style.modalVertAround,
                    onSubmit: () => onSubmit(),
                }}
            >
                {children}
                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default UpdateContactForm;
