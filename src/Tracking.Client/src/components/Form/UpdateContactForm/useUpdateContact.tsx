import { contactApi } from '@/services/contact/contact';
import { IUpdateContact } from '@/services/contact/types';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useUpdateContact = () => {
    const { contactId } = useParams();
    const mutation = useMutation({
        mutationFn: async (data: IUpdateContact) => {
            if (!contactId) {
                throw Error();
            }
            return await contactApi
                .update(contactId, data)
                .then((res) => res.data);
        },
    });
    return mutation;
};
