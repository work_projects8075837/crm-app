import { channelApi } from '@/services/channel/channel';
import { ICreateChannel } from '@/services/channel/types';
import { useMutation } from '@tanstack/react-query';

export const useCreateChannelMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreateChannel) => {
            return await channelApi.create(data).then((res) => res.data);
        },
    });

    return mutation;
};
