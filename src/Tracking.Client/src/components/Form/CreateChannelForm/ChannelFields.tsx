import React from 'react';

import FormInput from '@/components/Fields/BaseFields/FormInput';
import NumberInput from '@/components/Fields/BaseFields/NumberInput';
import {
    CHANNEL_MULTIPLE_NAME,
    CHANNEL_NAME_NAME,
} from '@/components/Fields/EntitiesFields/channel';

const ChannelFields: React.FC = () => {
    return (
        <>
            <FormInput name={CHANNEL_NAME_NAME} placeholder='Название' />

            <NumberInput
                name={CHANNEL_MULTIPLE_NAME}
                placeholder='Коэффициент'
            />
        </>
    );
};

export default ChannelFields;
