import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { IChannelForm } from '@/components/Fields/EntitiesFields/channel';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_ALL_CHANNELS_KEY } from '@/store/queries/keys/keys';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import ChannelFields from './ChannelFields';
import { useCreateChannelMutation } from './useCreateChannelMutation';

interface CreateChannelFormProps {
    openHandler: OpenHandler;
}

const CreateChannelForm: React.FC<CreateChannelFormProps> = ({
    openHandler,
}) => {
    const { form } = useBaseForm<IChannelForm>();
    const { mutate, isLoading } = useCreateChannelMutation();

    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: () => {
                invalidateKey(GET_ALL_CHANNELS_KEY);

                openHandler.close();
            },
        });
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ className: style.modalVertAround, onSubmit }}
            >
                <ChannelFields />

                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default CreateChannelForm;
