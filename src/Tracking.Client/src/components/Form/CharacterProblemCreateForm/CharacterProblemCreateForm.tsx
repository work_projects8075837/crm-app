import { ICharacterProblemForm } from '@/components/Fields/EntitiesFields/character_problem';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { useQueryParams } from '@/hooks/useQueryParams';
import { ICharacterProblem } from '@/services/character_problem/types';
import { totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_CHARACTER_PROBLEMS_KEY,
    GET_SEARCH_CHARACTER_PROBLEMS_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/sale_point.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCharacterProblemCreateMutation } from './useCharacterProblemCreateMutation';

interface CharacterProblemCreateFormProps {
    children: React.ReactNode;
    modals?: React.ReactNode;
    parentCharacterProblem?: ICharacterProblem;
}

const CharacterProblemCreateForm: React.FC<CharacterProblemCreateFormProps> = ({
    modals,
    children,
    parentCharacterProblem,
}) => {
    const navigate = useNavigate();
    const { is_parent } = useQueryParams();
    const { form } = useBaseForm<ICharacterProblemForm>({
        defaultValues: {
            character_problem: parentCharacterProblem,
            is_parent: !!is_parent,
        },
    });
    const { mutate, isLoading } = useCharacterProblemCreateMutation();
    const onSubmit = (onSuccess: (data: ICharacterProblem) => void) => {
        form.handleSubmit((data) => {
            mutate(
                {
                    ...data,
                    character_problem_id: data.character_problem?.id,
                },
                {
                    onSuccess: (data) => {
                        totalClearKey(GET_ALL_CHARACTER_PROBLEMS_KEY);
                        totalClearKey(GET_SEARCH_CHARACTER_PROBLEMS_KEY);

                        if (onSuccess) onSuccess(data);
                    },
                },
            );
        })();
    };
    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                submit={{ onSubmit, isDisabled: isLoading }}
                htmlFormProps={{
                    className: style.formWindow,
                    onSubmit: (event) => event.preventDefault(),
                }}
            >
                {children}
                <FormBottom
                    isLoading={isLoading}
                    onSaveClick={() => {
                        onSubmit((data) => {
                            navigate(`/character_problem/single/${data.id}/`);
                        });
                    }}
                    onSaveCloseClick={() => {
                        onSubmit((data) => {
                            navigate(
                                `/character_problem/${
                                    data.character_problem_id || ''
                                }`,
                            );
                        });
                    }}
                />
            </BaseForm>
        </>
    );
};

export default CharacterProblemCreateForm;
