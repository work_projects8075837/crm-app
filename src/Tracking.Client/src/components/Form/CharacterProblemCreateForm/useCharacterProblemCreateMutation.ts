import { characterProblemApi } from '@/services/character_problem/character_problem';
import { ICreateCharacterProblem } from '@/services/character_problem/types';
import { useMutation } from '@tanstack/react-query';

export const useCharacterProblemCreateMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreateCharacterProblem) => {
            const newCharacterProblem = await characterProblemApi
                .create(data)
                .then((res) => res.data);

            return newCharacterProblem;
        },
    });

    return mutation;
};
