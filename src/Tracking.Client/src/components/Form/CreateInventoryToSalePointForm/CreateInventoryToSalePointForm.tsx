import { IInventoryForm } from '@/components/Fields/EntitiesFields/inventory';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { useQueryParams } from '@/hooks/useQueryParams';
import { IInventory } from '@/services/inventory/types';
import { totalClearKey } from '@/store/queries/actions/base';
import { queryClient } from '@/store/queries/client/client';
import {
    GET_ALL_INVENTORIES_KEY,
    GET_ONE_SALE_POINT_BY_ID_KEY,
    GET_SEARCH_INVENTORIES_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateInventoryMutation } from './useCreateInventoryMutation';

interface CreateInventoryToSalePointFormProps {
    modals?: React.ReactNode;
    children: React.ReactNode;
    parentInventory?: IInventory | null;
}

const CreateInventoryToSalePointForm: React.FC<
    CreateInventoryToSalePointFormProps
> = ({ children, modals, parentInventory }) => {
    const { salePointId } = useParams();
    const { is_parent, is_connection } = useQueryParams();
    const navigate = useNavigate();
    const { form } = useBaseForm<IInventoryForm>({
        defaultValues: {
            inventory: parentInventory,
            is_parent: !!is_parent,
        },
    });
    const { mutate, isLoading } = useCreateInventoryMutation();
    const onSubmit = (onSuccess: (data: IInventory) => void) => {
        form.handleSubmit((data) => {
            mutate(data, {
                onSuccess: (newInventory) => {
                    queryClient.invalidateQueries({
                        queryKey: [
                            GET_ONE_SALE_POINT_BY_ID_KEY,
                            { salePointId },
                        ],
                    });
                    totalClearKey(GET_ALL_INVENTORIES_KEY);
                    totalClearKey(GET_SEARCH_INVENTORIES_KEY);

                    onSuccess(newInventory);
                },
            });
        })();
    };
    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                htmlFormProps={{
                    onSubmit: (event) => event.preventDefault(),
                    className: style.formWindow,
                }}
            >
                <div className={style.formChildren}>{children}</div>
                <FormBottom
                    isLoading={isLoading}
                    onSaveClick={() => {
                        onSubmit((data) => {
                            navigate(
                                `${
                                    salePointId
                                        ? `/sale_point/single/${salePointId}`
                                        : ''
                                }/inventory/single/${data.id}/?${
                                    data.connection.length || is_connection
                                        ? 'is_connection=1'
                                        : ''
                                }`,
                            );
                        });
                    }}
                    onSaveCloseClick={() => {
                        onSubmit((data) => {
                            navigate(
                                `${
                                    salePointId
                                        ? `/sale_point/single/${salePointId}`
                                        : ''
                                }/${
                                    !data.connection.length
                                        ? 'inventory/'
                                        : 'connections/'
                                }${data.inventory_id || ''}`,
                            );
                        });
                    }}
                />
            </BaseForm>
        </>
    );
};

export default CreateInventoryToSalePointForm;
