import { IInventoryForm } from '@/components/Fields/EntitiesFields/inventory';
import { connectionApi } from '@/services/connection/connection';
import { IConnection } from '@/services/connection/types';
import { salePointApi } from '@/services/sale_point/sale_point';
import { IdEntity } from '@/services/types';
import { useSalePointByParams } from '@/store/queries/stores/sale_point/useSalePointByParams';
import { useMutation } from '@tanstack/react-query';
import { inventoryApi } from './../../../services/inventory/inventory';

export const createConnections = async (connections?: IConnection[]) => {
    const created_connections: IdEntity[] = [];

    if (connections) {
        for await (const connection of connections) {
            const newConnection = await connectionApi
                .create(connection)
                .then((res) => res.data);
            created_connections.push(newConnection);
        }
    }
    return created_connections;
};

export const useCreateInventoryMutation = () => {
    const currentSalePoint = useSalePointByParams();
    const salePointId = currentSalePoint.data?.id;
    const mutation = useMutation({
        mutationFn: async (data: IInventoryForm) => {
            const newInventory = await inventoryApi
                .create({
                    ...data,
                    inventory_id: data.inventory?.id,
                })
                .then((res) => res.data);

            if (currentSalePoint.data && salePointId) {
                await salePointApi.update(salePointId, {
                    inventory: [
                        ...currentSalePoint.data.inventory,
                        newInventory,
                    ],
                });
            }

            return newInventory;
        },
    });

    return mutation;
};
