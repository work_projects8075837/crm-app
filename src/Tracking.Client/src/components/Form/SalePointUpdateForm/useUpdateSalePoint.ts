import {
    ISalePointForm,
    ISalePointFormContact,
} from '@/components/Fields/EntitiesFields/sale_point';
import { SERVICE_STATUSES } from '@/components/Fields/SelectServiceStatus/SelectServiceStatus';
import { contactPositionApi } from '@/services/contact_position/contact_position';
import { IContactPosition } from '@/services/contact_position/types';
import { salePointApi } from '@/services/sale_point/sale_point';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import { createContactPositions } from '../SalePointCreateForm/useCreateSalePointMutation';

export const updateContactPositions = async (
    contacts?: ISalePointFormContact[],
) => {
    const contactPositions: IContactPosition[] = [];

    if (!contacts) {
        return contactPositions;
    }

    for await (const contact of contacts) {
        const contactPosition = await contactPositionApi
            .update(contact.id, {
                ...contact,
                position: contact.position_name,
            })
            .then((res) => res.data);
        contactPositions.push(contactPosition);
    }

    return contactPositions;
};

export const useUpdateSalePoint = () => {
    const { salePointId } = useParams();
    const mutation = useMutation({
        mutationFn: async (data: ISalePointForm) => {
            if (!salePointId) {
                throw Error();
            }

            const newContacts = data.position?.filter(
                (c) => c.id?.includes('create'),
            );
            const updateContacts = data.position?.filter(
                (c) => !c.id?.includes('create'),
            );

            await createContactPositions(salePointId, newContacts);
            await updateContactPositions(updateContacts);

            const salePoint = await salePointApi
                .update(salePointId, {
                    ...data,
                    sale_point_id: data.sale_point?.id,
                    director_contact_id: data.director_contact?.id,
                    user_id: data.user?.id,
                    description: data.description,
                    status: data.status?.id || SERVICE_STATUSES.not_subscriber,
                })
                .then((res) => res.data);

            return salePoint;
        },
    });
    return mutation;
};
