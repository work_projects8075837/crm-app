import { ISalePointForm } from '@/components/Fields/EntitiesFields/sale_point';
import { SERVICE_STATUSES } from '@/components/Fields/SelectServiceStatus/SelectServiceStatus';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { toastSuccessUpdate } from '@/components/utils/toastSuccessUpdate';
import { useUniquenessErrorHandler } from '@/hooks/useUniquenessErrorHandler';
import { ISalePoint } from '@/services/sale_point/types';
import { totalClearKey } from '@/store/queries/actions/base';
import { queryClient } from '@/store/queries/client/client';
import {
    GET_ALL_SALE_POINTS_KEY,
    GET_ONE_SALE_POINT_BY_ID_KEY,
    GET_SEARCH_SALE_POINTS_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useUpdateSalePoint } from './useUpdateSalePoint';

interface SalePointUpdateFormProps {
    modals?: React.ReactNode;
    children: React.ReactNode;
    salePointFrom: ISalePointForm;
}

const SalePointUpdateForm: React.FC<SalePointUpdateFormProps> = ({
    modals,
    children,
    salePointFrom,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<ISalePointForm>({
        defaultValues: salePointFrom,
    });

    const { handleErrors } = useUniquenessErrorHandler(form.setError);

    const { mutate, isLoading } = useUpdateSalePoint();

    const onSubmit = (onSuccess?: (data: ISalePoint) => void) => {
        form.handleSubmit((data) => {
            mutate(data, {
                onSuccess: (USalePoint) => {
                    if (!data.status) {
                        form.setValue('status', {
                            name: SERVICE_STATUSES.not_subscriber,
                            id: SERVICE_STATUSES.not_subscriber,
                        });
                    }
                    queryClient.invalidateQueries({
                        queryKey: [GET_ONE_SALE_POINT_BY_ID_KEY],
                    });
                    totalClearKey(GET_ALL_SALE_POINTS_KEY);
                    totalClearKey(GET_SEARCH_SALE_POINTS_KEY);
                    toastSuccessUpdate();
                    onSuccess?.(USalePoint);
                },
                onError: handleErrors,
            });
        })();
    };

    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                htmlFormProps={{
                    onSubmit: (event) => event.preventDefault(),
                    className: style.formWindow,
                }}
            >
                <div className={style.formChildren}>{children}</div>
                <FormBottom
                    isLoading={isLoading}
                    onSaveClick={() => {
                        onSubmit();
                    }}
                    onSaveCloseClick={() => {
                        onSubmit((data) => {
                            navigate(
                                `/sale_point/${
                                    data.sale_point_id
                                        ? `${data.sale_point_id}`
                                        : ''
                                }`,
                            );
                        });
                    }}
                />
            </BaseForm>
        </>
    );
};

export default SalePointUpdateForm;
