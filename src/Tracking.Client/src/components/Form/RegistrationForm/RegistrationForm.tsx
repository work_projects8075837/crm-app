import FormInput from '@/components/Fields/BaseFields/FormInput';
import SubmitButton from '@/components/Fields/Buttons/SubmitButton';
import EmailInput from '@/components/Fields/EmailInput/EmailInput';
import PasswordInput from '@/components/Fields/PasswordInput/PasswordInput';
import RepeatPasswordInput from '@/components/Fields/RepeatPassword/RepeatPasswordInput';
import RootError from '@/components/Fields/RootError/RootError';
import { IUserRegistration } from '@/services/auth/types.ts';
import React from 'react';
import BaseForm from '../BaseForm/BaseForm';
import LoginFieldLayout from '../LoginForm/layouts/LabelFieldLayout';
import { useBaseForm } from '../hooks/useBaseForm';
import { useRegistrationMutation } from './useRegistrationMutation';

const RegistrationForm: React.FC = () => {
    const { form } = useBaseForm<IUserRegistration>();
    const registrationMutation = useRegistrationMutation({
        onError: () => {
            form.setError('email', {
                type: 'validate',
                message: 'Пользователь с таким E-mail уже существует',
            });
        },
    });

    const onSubmit = form.handleSubmit((data) => {
        registrationMutation.mutate(data);
    });

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: 'form--container' }}
            >
                <LoginFieldLayout label='ФИО'>
                    <FormInput name='name' placeholder='Введите Ваше ФИО' />
                </LoginFieldLayout>

                <LoginFieldLayout label='Логин'>
                    <EmailInput name='email' placeholder='E-mail' />
                </LoginFieldLayout>

                <LoginFieldLayout label='Пароль'>
                    <PasswordInput
                        name='password'
                        placeholder='Придумайте пароль'
                    />
                </LoginFieldLayout>

                <LoginFieldLayout label='Пароль'>
                    <RepeatPasswordInput
                        placeholder='Подтвердите пароль'
                        name='password_repeat'
                    />
                </LoginFieldLayout>

                <SubmitButton
                    text={'Регистрация'}
                    isLoading={registrationMutation.isLoading}
                />
                <RootError />
            </BaseForm>
        </>
    );
};

export default RegistrationForm;
