import { authService } from '@/services/auth/auth';
import { IUserRegistration } from '@/services/auth/types.ts';
import { useMutation } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import { IFormErrorMutation } from '../Types/Types';
import toast from 'react-hot-toast';
import { SUCCESS_DURATION } from '@/components/Providers/ToastOptions/toastOptions';

export const useRegistrationMutation = ({ onError }: IFormErrorMutation) => {
    const navigate = useNavigate();

    const registrationMutation = useMutation({
        mutationFn: ({ name, email, password }: IUserRegistration) => {
            return authService.registration({ name, email, password });
        },
        onSuccess: () => {
            toast.success('Войдите в аккунт', { duration: SUCCESS_DURATION });
            navigate('/login/');
        },
        onError,
    });

    return registrationMutation;
};
