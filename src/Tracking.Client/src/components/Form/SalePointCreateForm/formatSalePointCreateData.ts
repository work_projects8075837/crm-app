import { ISalePointForm } from '@/components/Fields/EntitiesFields/sale_point';
import { ICreateSalePoint } from '@/services/sale_point/types';

export const formatSalePointCreateData = (
    data: ISalePointForm,
): ICreateSalePoint => {
    return {
        ...data,
        director_contact_id: data.director_contact?.id,
        description: data.description,
        counterparty: data.counterparty,
        sale_point_id: data.sale_point?.id,
        user_id: data.user?.id,
        status: data.status?.id || '',
    };
};
