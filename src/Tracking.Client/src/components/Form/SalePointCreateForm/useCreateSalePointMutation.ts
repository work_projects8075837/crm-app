import {
    ISalePointForm,
    ISalePointFormContact,
} from '@/components/Fields/EntitiesFields/sale_point';
import { contactPositionApi } from '@/services/contact_position/contact_position';
import { IContactPosition } from '@/services/contact_position/types';
import { salePointApi } from '@/services/sale_point/sale_point';
import { useMutation } from '@tanstack/react-query';
import { formatSalePointCreateData } from './formatSalePointCreateData';

export const createContactPositions = async (
    salePointId: string,
    contacts?: ISalePointFormContact[],
) => {
    const contactPositions: IContactPosition[] = [];

    if (!contacts) {
        return contactPositions;
    }

    await Promise.allSettled(
        contacts.map((contact) =>
            contactPositionApi
                .create({
                    ...contact,
                    position: contact.position_name,
                    sale_point_id: salePointId,
                })
                .then((res) => res.data),
        ),
    ).then((promises) => {
        promises.forEach((result) => {
            if (result.status === 'fulfilled') {
                contactPositions.push(result.value);
            }
        });
    });

    return contactPositions;
};

export const useCreateSalePointMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: ISalePointForm) => {
            const salePoint = await salePointApi
                .create(formatSalePointCreateData(data))
                .then((res) => res.data);

            const contacts = await createContactPositions(
                salePoint.id,
                data.position,
            );

            return salePoint;
        },
    });

    return mutation;
};
