import { ISalePointForm } from '@/components/Fields/EntitiesFields/sale_point';
import TicketSaveButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveButton';
import TicketSaveCloseButton from '@/components/Tickets/TicketCreate/TicketSaveBottom/TicketSaveCloseButton';
import { useQueryParams } from '@/hooks/useQueryParams';
import { useUniquenessErrorHandler } from '@/hooks/useUniquenessErrorHandler';
import { ISalePoint } from '@/services/sale_point/types';
import { totalClearKey } from '@/store/queries/actions/base';
import { GET_ALL_SALE_POINTS_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/sale_point.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { GET_SEARCH_SALE_POINTS_KEY } from './../../../store/queries/keys/keys';
import { useCreateSalePointMutation } from './useCreateSalePointMutation';

interface SalePointCreateFormProps {
    children: React.ReactNode;
    modals?: React.ReactNode;
    parentSalePoint?: ISalePoint | null;
}

const SalePointCreateForm: React.FC<SalePointCreateFormProps> = ({
    children,
    modals,
    parentSalePoint,
}) => {
    const navigate = useNavigate();
    const { is_parent } = useQueryParams();
    const { form } = useBaseForm<ISalePointForm>({
        defaultValues: {
            sale_point: parentSalePoint,
            is_parent: !!is_parent,
        },
    });
    const { handleErrors } = useUniquenessErrorHandler(form.setError);

    const { isLoading, mutate } = useCreateSalePointMutation();

    const onSubmit = (onSuccess?: (data: ISalePoint) => void) => {
        form.handleSubmit((data) => {
            mutate(data, {
                onSuccess: (newSalePoint) => {
                    totalClearKey(GET_ALL_SALE_POINTS_KEY);
                    totalClearKey(GET_SEARCH_SALE_POINTS_KEY);

                    if (onSuccess) onSuccess(newSalePoint);
                },
                onError: handleErrors,
            });
        })();
    };

    return (
        <BaseForm
            {...form}
            submit={{ onSubmit }}
            outerFormChildren={modals}
            htmlFormProps={{
                className: style.formWindow,
                onSubmit: (event) => event.preventDefault(),
            }}
        >
            {children}
            <div className={style.formSubmitBottom}>
                {isLoading ? (
                    <>Сохранение...</>
                ) : (
                    <>
                        <TicketSaveButton
                            // isDisabled={!form.formState.isValid}
                            onClick={() =>
                                onSubmit((data) => {
                                    navigate(`/sale_point/single/${data.id}`);
                                })
                            }
                            className={style.formSubmitLimitedButton}
                        />
                        <TicketSaveCloseButton
                            // isDisabled={!form.formState.isValid}
                            onClick={() =>
                                onSubmit((data) => {
                                    navigate(
                                        `/sale_point/${
                                            data.sale_point_id || ''
                                        }`,
                                    );
                                })
                            }
                            className={style.formSubmitLimitedButton}
                        />
                    </>
                )}
            </div>
        </BaseForm>
    );
};

export default SalePointCreateForm;
