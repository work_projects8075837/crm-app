import { Entity } from '@/components/Fields/SelectEntity/SelectEntity';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';

interface SelectEntityFormProps {
    openHandler: OpenHandler;
    name: string;
    children: React.ReactNode;
    addMode?: boolean;
    converter?: (data: any) => Entity;
}

const SelectEntityForm: React.FC<SelectEntityFormProps> = ({
    children,
    openHandler,
    name,
    addMode = false,
    converter,
}) => {
    const { add } = useFormEntityValueArray(name);
    const relativeForm = useFormContext();
    const { form } = useBaseForm({
        defaultValues: { [name]: relativeForm.getValues(name) },
    });
    const onSubmit = form.handleSubmit((data) => {
        if (addMode) {
            const d = converter ? converter(data) : (data as Entity);
            add(d);
        } else {
            relativeForm.setValue(name, data[name]);
        }
        openHandler.close();
    });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: style.modalVertAround }}
            >
                {children}
            </BaseForm>
        </>
    );
};

export default SelectEntityForm;
