import React from 'react';
import { useBaseForm } from '../hooks/useBaseForm';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { ICounterPartyForm } from '@/components/Fields/EntitiesFields/counter_party';

export interface EditCounterPartyFormProps {
    children: React.ReactNode;
    defaultValues?: ICounterPartyForm;
}

const EditCounterPartyForm: React.FC<EditCounterPartyFormProps> = ({
    children,
    defaultValues,
}) => {
    const { form } = useBaseForm<ICounterPartyForm>({ defaultValues });
    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{
                    onSubmit: (event) => event.preventDefault(),
                    className: style.formWindow,
                }}
            >
                <div className={style.formChildren}>{children}</div>
                <FormBottom />
            </BaseForm>
        </>
    );
};

export default EditCounterPartyForm;
