import { commentApi } from '@/services/comment/comment';
import { IUpdateComment } from '@/services/comment/types';
import { useMutation } from '@tanstack/react-query';

export const useUpdateComment = () => {
    return useMutation({
        mutationFn: async (data: { id: string } & IUpdateComment) => {
            return await commentApi
                .update(data.id, data)
                .then((res) => res.data);
        },
    });
};
