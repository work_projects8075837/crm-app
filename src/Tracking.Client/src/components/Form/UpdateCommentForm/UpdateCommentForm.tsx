import BlueButton from '@/components/Fields/Buttons/BlueButton';
import { isEditorContentExist } from '@/components/Fields/SalePointSelect/SalePointSelect';
import { invalidateKey } from '@/store/queries/actions/base';
import { updateComment } from '@/store/queries/actions/comment';
import { GET_ALL_COMMENTS_KEY } from '@/store/queries/keys/keys';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useParams } from 'react-router-dom';
import style from '../../../style/elements/blue_button.module.scss';
import ticketStyle from '../../../style/layout/ticket.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { updatableComment } from './UpdateCommentProvider';
import { useUpdateComment } from './useUpdateComment';
interface UpdateCommentFormProps {
    children: React.ReactNode;
    commentValues: { description: string; id: string };
}

const UpdateCommentForm: React.FC<UpdateCommentFormProps> = ({
    children,
    commentValues,
}) => {
    const { ticketId } = useParams();
    const { form } = useBaseForm<{ description: string }>({
        defaultValues: commentValues,
    });
    const { mutate, isLoading } = useUpdateComment();
    const onSubmit = form.handleSubmit((data) => {
        mutate(
            { ...data, id: commentValues.id },
            {
                onSuccess: (newComment) => {
                    updateComment(commentValues.id, newComment, ticketId);
                    invalidateKey(GET_ALL_COMMENTS_KEY);
                    form.setValue('description', '');
                    updatableComment.setValue(null);
                },
            },
        );
    });

    const isDisabled = !isEditorContentExist(form.watch('description'));

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: ticketStyle.commentForm }}
            >
                {children}
                <BlueButton
                    disabled={isLoading || isDisabled}
                    className={style.shortWidth}
                >
                    {isLoading ? 'Загрузка...' : 'Изменить'}
                </BlueButton>
            </BaseForm>
        </>
    );
};

export default observer(UpdateCommentForm);
