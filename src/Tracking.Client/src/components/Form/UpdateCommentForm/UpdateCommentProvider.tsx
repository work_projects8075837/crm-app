import EditorInput from '@/components/Fields/EditorInput/EditorInput';
import { ValueHandler } from '@/store/states/SearchHandler';
import { observer } from 'mobx-react-lite';
import React from 'react';
import AddCommentForm from '../AddCommentForm/AddCommentForm';
import UpdateCommentForm from './UpdateCommentForm';

export const updatableComment = new ValueHandler<{
    description: string;
    id: string;
}>();

const UpdateCommentProvider: React.FC = () => {
    return (
        <>
            {updatableComment.value ? (
                <UpdateCommentForm commentValues={updatableComment.value}>
                    <EditorInput className='smallEditor' name='description' />
                </UpdateCommentForm>
            ) : (
                <AddCommentForm>
                    <EditorInput className='smallEditor' name='description' />
                </AddCommentForm>
            )}
        </>
    );
};

export default observer(UpdateCommentProvider);
