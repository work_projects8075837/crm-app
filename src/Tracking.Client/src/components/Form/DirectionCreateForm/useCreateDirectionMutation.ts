import { directionApi } from '@/services/direction/direction';
import { ICreateDirection } from '@/services/direction/types';
import { useMutation } from '@tanstack/react-query';

export const useCreateDirectionMutation = () => {
    const mutation = useMutation({
        mutationFn: async (data: ICreateDirection) => {
            const newDirection = await directionApi
                .create(data)
                .then((res) => res.data);

            return newDirection;
        },
    });

    return mutation;
};
