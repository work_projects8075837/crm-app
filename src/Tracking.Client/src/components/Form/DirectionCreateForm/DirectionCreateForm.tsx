import { IDirectionForm } from '@/components/Fields/EntitiesFields/direction';
import FormBottom from '@/components/Layouts/FormBottom/FormBottom';
import { IDirection } from '@/services/direction/types';
import { totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_DIRECTIONS_KEY,
    GET_SEARCH_DIRECTIONS_KEY,
} from '@/store/queries/keys/keys';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../../style/layout/layout.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useCreateDirectionMutation } from './useCreateDirectionMutation';

interface DirectionCreateFormProps {
    children: React.ReactNode;
    modals?: React.ReactNode;
}

const DirectionCreateForm: React.FC<DirectionCreateFormProps> = ({
    children,
    modals,
}) => {
    const navigate = useNavigate();
    const { form } = useBaseForm<IDirectionForm>();
    const { mutate, isLoading } = useCreateDirectionMutation();
    const onSubmit = (onSuccess: (data: IDirection) => void) => {
        form.handleSubmit((data) => {
            mutate(data, {
                onSuccess: (newDirection) => {
                    totalClearKey(GET_ALL_DIRECTIONS_KEY);
                    totalClearKey(GET_SEARCH_DIRECTIONS_KEY);

                    onSuccess(newDirection);
                },
            });
        })();
    };
    return (
        <>
            <BaseForm
                outerFormChildren={modals}
                {...form}
                htmlFormProps={{
                    onSubmit: (event) => {
                        event.preventDefault();
                    },
                    className: style.formWindow,
                }}
            >
                <div className={style.formChildren}>{children}</div>
                <FormBottom
                    isLoading={isLoading}
                    onSaveClick={() => {
                        onSubmit((newDirection) => {
                            navigate(`/direction/single/${newDirection.id}/`);
                        });
                    }}
                    onSaveCloseClick={() => {
                        onSubmit(() => {
                            navigate(`/direction/`);
                        });
                    }}
                />
            </BaseForm>
        </>
    );
};

export default DirectionCreateForm;
