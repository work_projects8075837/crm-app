import { useContext } from 'react';
import { SubmitContext } from './SubmitProvider';

export const useSubmit = () => {
    const submit = useContext(SubmitContext);

    return submit;
};
