import React, { FormHTMLAttributes } from 'react';
import { FieldValues, FormProvider, FormProviderProps } from 'react-hook-form';
import SubmitProvider, { ISubmitContext } from './SubmitProvider';

interface BaseFormProps<
    TFieldValues extends FieldValues,
    TContext = any,
    TTransformedValues extends FieldValues | undefined = undefined,
> extends FormProviderProps<TFieldValues, TContext, TTransformedValues> {
    outerFormChildren?: React.ReactNode;
    htmlFormProps?: FormHTMLAttributes<HTMLFormElement>;
    submit?: ISubmitContext;
}

type BaseFormT = <
    TFieldValues extends FieldValues,
    TContext = any,
    TTransformedValues extends FieldValues | undefined = undefined,
>(
    props: BaseFormProps<TFieldValues, TContext, TTransformedValues>,
) => React.JSX.Element;

const BaseForm: BaseFormT = ({
    children,
    outerFormChildren,
    htmlFormProps,
    submit,
    ...formProps
}) => {
    return (
        <>
            <FormProvider {...formProps}>
                <form
                    {...htmlFormProps}
                    onSubmit={(event) => {
                        event.preventDefault();
                        if (htmlFormProps?.onSubmit) {
                            htmlFormProps.onSubmit(event);
                        }
                    }}
                >
                    <SubmitProvider {...submit}>{children}</SubmitProvider>
                </form>
                {outerFormChildren}
            </FormProvider>
        </>
    );
};

export default BaseForm;
