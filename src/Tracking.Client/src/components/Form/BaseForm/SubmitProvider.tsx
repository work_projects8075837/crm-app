import React, { createContext } from 'react';

export interface ISubmitContext {
    isDisabled?: boolean;
    onSubmit?: (a?: any) => void;
}

export const SubmitContext = createContext<ISubmitContext>({});

interface SubmitProviderProps extends ISubmitContext {
    children: React.ReactNode;
}

const SubmitProvider: React.FC<SubmitProviderProps> = ({
    children,
    isDisabled,
    onSubmit,
}) => {
    return (
        <SubmitContext.Provider value={{ isDisabled, onSubmit }}>
            {children}
        </SubmitContext.Provider>
    );
};

export default SubmitProvider;
