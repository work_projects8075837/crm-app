import { OpenHandler } from '@/store/states/OpenHandler.ts';
import React from 'react';
import style from '../../../style/layout/modal_window.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import SaveButton from '@/components/Fields/Buttons/SaveButton';
import { useCreateShortCounterParty } from './useCreateShortCounterParty';
import { IShortCounterPartyForm } from '@/components/Fields/EntitiesFields/counter_party';
import { useFormEntityValueArray } from '@/components/Fields/hooks/useFormEntityValueArray';
import { SALE_POINT_COUNTER_PARTIES_NAME } from '@/components/Fields/EntitiesFields/sale_point';
import { totalClearKey } from '@/store/queries/actions/base';
import {
    GET_ALL_COUNTER_PARTIES_KEY,
    GET_SEARCH_COUNTER_PARTIES_KEY,
} from '@/store/queries/keys/keys';

interface CreateShortCounterPartyFormProps {
    children: React.ReactNode;
    openHandler: OpenHandler;
}

const CreateShortCounterPartyForm: React.FC<
    CreateShortCounterPartyFormProps
> = ({ children, openHandler }) => {
    const { add } = useFormEntityValueArray(SALE_POINT_COUNTER_PARTIES_NAME);
    const { form } = useBaseForm<IShortCounterPartyForm>();
    const { mutate, isLoading } = useCreateShortCounterParty();
    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: (newSalePoint) => {
                add(newSalePoint);
                openHandler.close();
                totalClearKey(GET_ALL_COUNTER_PARTIES_KEY);
                totalClearKey(GET_SEARCH_COUNTER_PARTIES_KEY);
            },
        });
    });

    return (
        <>
            <BaseForm
                {...form}
                htmlFormProps={{ onSubmit, className: style.modalVertAround }}
            >
                {children}
                <SaveButton isLoading={isLoading}>Сохранить</SaveButton>
            </BaseForm>
        </>
    );
};

export default CreateShortCounterPartyForm;
