import { IShortCounterPartyForm } from '@/components/Fields/EntitiesFields/counter_party';
import { counterPartyApi } from '@/services/counterparty/counterparty';
import { queryClient } from '@/store/queries/client/client';
import { GET_ALL_COUNTER_PARTIES_KEY } from '@/store/queries/keys/keys';
import { useMutation } from '@tanstack/react-query';

export const useCreateShortCounterParty = () => {
    const mutation = useMutation({
        mutationFn: async (data: IShortCounterPartyForm) => {
            return await counterPartyApi.create(data).then((res) => res.data);
        },
        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [GET_ALL_COUNTER_PARTIES_KEY],
            });
        },
    });

    return mutation;
};
