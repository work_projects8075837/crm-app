import BlueButton from '@/components/Fields/Buttons/BlueButton';
import { isEditorContentExist } from '@/components/Fields/SalePointSelect/SalePointSelect';
import { invalidateKey } from '@/store/queries/actions/base';
import { GET_ALL_COMMENTS_KEY } from '@/store/queries/keys/keys';
import React from 'react';
import style from '../../../style/elements/blue_button.module.scss';
import ticketStyle from '../../../style/layout/ticket.module.scss';
import BaseForm from '../BaseForm/BaseForm';
import { useBaseForm } from '../hooks/useBaseForm';
import { useAddComment } from './useAddComment';

interface AddCommentFormProps {
    children: React.ReactNode;
}

const AddCommentForm: React.FC<AddCommentFormProps> = ({ children }) => {
    const { form } = useBaseForm<{ description: string }>();
    const { mutate, isLoading } = useAddComment();
    const onSubmit = form.handleSubmit((data) => {
        mutate(data, {
            onSuccess: () => {
                invalidateKey(GET_ALL_COMMENTS_KEY);
                form.setValue('description', '');
            },
        });
    });
    const isDisabled = !isEditorContentExist(form.watch('description'));

    return (
        <BaseForm
            {...form}
            htmlFormProps={{ onSubmit, className: ticketStyle.commentForm }}
        >
            {children}
            <BlueButton
                disabled={isLoading || isDisabled}
                className={style.shortWidth}
            >
                {isLoading ? 'Загрузка...' : 'Отправить'}
            </BlueButton>
        </BaseForm>
    );
};

export default AddCommentForm;
