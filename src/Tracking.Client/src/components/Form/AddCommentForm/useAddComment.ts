import { commentApi } from '@/services/comment/comment';
import { addComment } from '@/store/queries/actions/comment';
import { useCurrentUser } from '@/store/queries/stores/user/useCurrentUser';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

export const useAddComment = () => {
    const { ticketId } = useParams();
    const userQ = useCurrentUser();
    return useMutation({
        mutationFn: async ({ description }: { description: string }) => {
            if (!ticketId || !userQ.data) {
                throw Error();
            }

            const comment = await commentApi
                .create({
                    description,
                    user_id: userQ.data.id,
                    ticket: [{ id: ticketId }],
                })
                .then((res) => res.data);

            addComment(comment, ticketId);

            return comment;
        },
    });
};
