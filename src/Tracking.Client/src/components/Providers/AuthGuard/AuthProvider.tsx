import Loader from '@/Loader/Loader';
import React from 'react';
import WebsocketProvider from './WebsocketProvider';
import { useAuthQuery } from './useAuthQuery';

export const USER_ROLE = 'User';
export const ADMIN_ROLE = 'Admin';
export const ALL_ROLES = 'ALL_ROLES';

interface AuthProviderProps {
    children: React.ReactNode;
    allowedRoles?: string[];
}

const AuthProvider: React.FC<AuthProviderProps> = ({
    children,
    allowedRoles,
}) => {
    const { isLoading } = useAuthQuery(allowedRoles);
    return (
        <>
            {!isLoading ? (
                <WebsocketProvider>{children}</WebsocketProvider>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default AuthProvider;
