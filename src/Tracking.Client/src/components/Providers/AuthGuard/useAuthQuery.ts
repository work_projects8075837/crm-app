import { CookiesStorage } from '@/services/utils/CookieStorage';
import { serviceManager } from '@/services/utils/instance/ServiceManager';
import { parseJwt } from '@/services/utils/parseJwt/parseJwt';
import { GET_AUTHORIZATION_KEY } from '@/store/queries/keys/auth';
import { useQuery } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import { ALL_ROLES } from './AuthProvider';

export const useAuthQuery = (allowedRoles = [ALL_ROLES]) => {
    const navigate = useNavigate();
    const query = useQuery({
        queryKey: [GET_AUTHORIZATION_KEY],
        queryFn: async () => {
            let access: string | undefined;
            if (CookiesStorage.getItem('refresh')) {
                access = await serviceManager.authorize().catch((error) => {
                    navigate('/login/');
                    throw error;
                });
                CookiesStorage.setItem('access', access);
            } else {
                access = CookiesStorage.getItem('access');
                if (!access) {
                    navigate('/login/');
                    throw new Error();
                }
            }

            const { sub, roles } = parseJwt(access);

            if (
                roles &&
                !allowedRoles.includes(ALL_ROLES) &&
                !roles.some((role) => allowedRoles.includes(role))
            ) {
                toast.error('Нет права на доступ');
                navigate('/');
            }

            return { userId: sub, roles };
        },
        refetchIntervalInBackground: false,
        refetchOnWindowFocus: false,
        refetchOnMount: false,
        staleTime: Infinity,
    });

    const checkRoles = (checkRoles: string[]) => {
        return query.data?.roles || !checkRoles.includes(ALL_ROLES)
            ? query.data?.roles?.some((role) => checkRoles.includes(role))
            : true;
    };

    return { ...query, checkRoles };
};
