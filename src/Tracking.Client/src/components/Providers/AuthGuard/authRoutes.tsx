export const NOT_AUTH_ROUTES = [
    'login',
    'registration',
    'recovery',
    'reset',
] as const;
