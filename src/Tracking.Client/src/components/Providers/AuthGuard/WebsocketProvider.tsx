import { noteSound } from '@/hooks/useNotificationSound/useNotificationSound';
import { ICase } from '@/services/case/types';
import { createWebsocketConsumer } from '@/services/websocket/WebsocketConsumer';
import { invalidateKey } from '@/store/queries/actions/base';
import { getUserId } from '@/store/queries/actions/user';
import { GET_AUTHORIZATION_KEY } from '@/store/queries/keys/auth';
import {
    GET_ALL_CASES_KEY,
    GET_ALL_NOTIFICATIONS_KEY,
    GET_ALL_READ_STATUSES_KEY,
} from '@/store/queries/keys/keys';
import React, { useEffect } from 'react';
import { useAuthQuery } from './useAuthQuery';

interface WebsocketProviderProps {
    children: React.ReactNode;
}

type TWSMessage =
    | {
          type: 'notification';
          data: {
              notification: { id: string };
              user: {
                  id: string;
              };
          };
      }
    | {
          type: 'user';
          data: {
              user_id: string;
          };
      }
    | {
          type: 'case';
          data: {
              data: ICase;
              event: string;
          };
      };

const { createConnection, cleanup } = createWebsocketConsumer<TWSMessage>({
    handlers: [
        {
            type: 'user',
            handler: (message) => {
                if (
                    message.type === 'user' &&
                    message.data.user_id === getUserId()
                ) {
                    invalidateKey(GET_AUTHORIZATION_KEY);
                }
            },
        },
        {
            type: 'notification',
            handler: (message) => {
                if (
                    message.type === 'notification' &&
                    message.data.user.id === getUserId()
                ) {
                    noteSound.play();

                    invalidateKey(GET_ALL_NOTIFICATIONS_KEY);
                    invalidateKey(GET_ALL_READ_STATUSES_KEY);
                }
            },
        },
        {
            type: 'case',
            handler: (message) => {
                if (
                    message.type === 'case' &&
                    message.data.data.manager?.user.id === getUserId()
                ) {
                    noteSound.play();

                    invalidateKey(GET_ALL_CASES_KEY);
                }
            },
        },
    ],
});

const WebsocketProvider: React.FC<WebsocketProviderProps> = ({ children }) => {
    const { data } = useAuthQuery();

    useEffect(() => {
        if (data?.userId) {
            createConnection();
        }
        return () => cleanup();
    }, [!!data]);
    return <>{children}</>;
};

export default WebsocketProvider;
