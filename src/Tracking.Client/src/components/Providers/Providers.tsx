import React from 'react';
import { Toaster } from 'react-hot-toast';
import { BrowserRouter } from 'react-router-dom';
import { QueryClientProvider } from '@tanstack/react-query';
import ToasterWithOptions from './ToastOptions/ToasterWithOptions';
import AuthProvider from './AuthGuard/AuthProvider';
import { queryClient } from '@/store/queries/client/client';

interface ProvidersProps {
    children: React.ReactNode;
}

const Providers: React.FC<ProvidersProps> = ({ children }) => {
    return (
        <>
            <BrowserRouter>
                <ToasterWithOptions />
                <QueryClientProvider client={queryClient}>
                    {children}
                </QueryClientProvider>
            </BrowserRouter>
        </>
    );
};

export default Providers;
