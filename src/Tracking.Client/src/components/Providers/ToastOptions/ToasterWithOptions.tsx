import React from 'react';
import { Toaster } from 'react-hot-toast';
import styles from './toast.module.scss';

const ToasterWithOptions: React.FC = () => {
    return (
        <>
            <Toaster
                toastOptions={{
                    className: styles.toast,
                }}
            />
        </>
    );
};

export default React.memo(ToasterWithOptions);
