import React from 'react';
import { ADMIN_ROLE } from '../AuthGuard/AuthProvider';
import RolesGuard from './RolesGuard';

interface AdminGuardProps {
    children: React.ReactNode;
}

const AdminGuard: React.FC<AdminGuardProps> = ({ children }) => {
    return (
        <>
            <RolesGuard roles={[ADMIN_ROLE]}>{children}</RolesGuard>
        </>
    );
};

export default AdminGuard;
