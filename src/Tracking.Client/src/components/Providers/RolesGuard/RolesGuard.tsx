import React from 'react';
import { useAuthQuery } from '../AuthGuard/useAuthQuery';

interface RolesGuardProps {
    roles: string[];
    children: React.ReactNode;
}

const RolesGuard: React.FC<RolesGuardProps> = ({ roles, children }) => {
    const { data, checkRoles } = useAuthQuery();

    return <>{data && checkRoles(roles) && children}</>;
};

export default RolesGuard;
