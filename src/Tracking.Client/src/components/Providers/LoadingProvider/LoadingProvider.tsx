import React from 'react';

const LoadingProvider: React.ForwardRefRenderFunction<HTMLDivElement> = (
    props,
    ref,
) => {
    return <div ref={ref}></div>;
};

export default React.forwardRef(LoadingProvider);
