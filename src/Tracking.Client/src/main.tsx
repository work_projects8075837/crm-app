import ReactDOM from 'react-dom/client';
import Application from './Application.tsx';
import './style/style.css';
import Providers from './components/Providers/Providers.tsx';

export const ROOT_ELEMENT = ReactDOM.createRoot(
    document.getElementById('root')!,
);

ROOT_ELEMENT.render(
    <>
        <Providers>
            <Application />
        </Providers>
    </>,
);
