import { LoaderCircleProps } from '@/components/Elements/LoaderCircle/LoaderCircle';
import styles from '@/style/layout/layout.module.scss';
import React from 'react';

type LoaderProps = LoaderCircleProps;

const Loader: React.FC<LoaderProps> = ({
    size = 160,
    borderWidth = 5,
    color = '#1e293b',
    className = '',
}) => {
    return (
        <div className={`${styles.loaderWindow} ${className}`}>
            <span
                className='loader'
                style={{
                    width: size,
                    height: size,
                    borderWidth,
                    borderColor: color,
                    borderBottomColor: 'transparent',
                }}
            ></span>
        </div>
    );
};

export default Loader;
