import { useState } from 'react';

export interface ITextInputModel {
    onChange: React.ChangeEventHandler<HTMLInputElement>;
    value: string;
}

export type UseTextInputModelReturn = [
    string,
    ITextInputModel,
    React.Dispatch<React.SetStateAction<string>>,
];

export const useTextInputModel = (
    defaultValue = '',
    pattern?: RegExp,
): UseTextInputModelReturn => {
    const [value, setValue] = useState(defaultValue);
    const onChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        if (!pattern || event.currentTarget.value.match(pattern)) {
            setValue(event.currentTarget.value);
        }
    };
    return [value, { value, onChange }, setValue];
};

export const useSearchInputModel = (def = '', pattern?: RegExp) => {
    const [value, model, setSearch] = useTextInputModel(def, pattern);

    return { value, model, setSearch };
};
