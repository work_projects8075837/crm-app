import { useCallback, useEffect, useRef, useState } from 'react';

interface UseSelectorHandlerProps {
    defaultOpen?: boolean;
    transition?: number;
    onOpen?: () => void;
    onClose?: () => void;
    afterOpen?: () => void;
    afterClose?: () => void;
}

const callExistingCallback = (callback?: () => void) => {
    if (callback) callback();
};

export const useSelectorHandler = <T extends HTMLElement>({
    defaultOpen = true,
    transition = 500,
    onOpen,
    onClose,
    afterOpen,
    afterClose,
}: UseSelectorHandlerProps) => {
    const trackRefElem = useRef<T>(null);
    const heightRef = useRef<number | null>(null);
    const [isOpen, setOpen] = useState<boolean>(defaultOpen);
    const [count, setCount] = useState<number>(0);

    const measureHeight = () => {
        if (trackRefElem.current) {
            heightRef.current = trackRefElem.current.clientHeight;
        }
    };

    const insertOpenHeight = () => {
        if (trackRefElem.current) {
            trackRefElem.current.style.height = `${heightRef.current}px`;
        }
    };

    const enableTransition = () => {
        if (trackRefElem.current) {
            trackRefElem.current.style.transition = `height ${transition}ms`;
        }
    };

    const disableTransition = () => {
        if (trackRefElem.current) {
            trackRefElem.current.style.transition = `all ${0}ms`;
        }
    };

    const performClose = () => {
        if (trackRefElem.current) {
            trackRefElem.current.style.height = '0px';
        }
    };

    const invalidateElementHeight = () => {
        disableTransition();
        measureHeight();
        insertOpenHeight();
        enableTransition();
    };

    const performOpen = () => {
        if (trackRefElem.current) {
            disableTransition();
            trackRefElem.current.style.opacity = '0';
            trackRefElem.current.style.position = 'absolute';
            trackRefElem.current.style.height = 'max-content';
            measureHeight();
            performClose();
            trackRefElem.current.style.position = 'relative';
            trackRefElem.current.style.opacity = '1';
            enableTransition();

            setTimeout(insertOpenHeight);
        }
    };

    useEffect(() => {
        if (trackRefElem.current) {
            // clientHeight - обрезает нижний padding
            trackRefElem.current.style.willChange = 'height';
        }
    }, []);

    useEffect(() => {
        invalidateElementHeight();
    }, [count]);

    const trigger = () => setCount((c) => c + 1);

    const close = useCallback(() => {
        measureHeight();

        setTimeout(performClose);

        callExistingCallback(onClose);

        setTimeout(() => {
            setOpen(false);
            callExistingCallback(afterClose);
        }, transition);
    }, []);

    const open = useCallback(() => {
        performOpen();

        callExistingCallback(onOpen);

        setOpen(true);
        setTimeout(() => {
            callExistingCallback(afterOpen);
        }, transition);
    }, []);

    const toggle = useCallback(() => {
        if (isOpen) close();
        else open();
    }, [isOpen]);

    return { trackRefElem, heightRef, close, open, toggle, isOpen, trigger };
};
