import { AxiosError } from 'axios';
import { FieldValues, UseFormSetError } from 'react-hook-form';

export interface UniquenessFields {
    address: string;
    phone: string;
    email: string;
}

export const useUniquenessErrorHandler = (
    setError: UseFormSetError<UniquenessFields>,
    prefix = 'Заведение',
) => {
    const handleErrors = (error: unknown) => {
        if (error instanceof AxiosError) {
            const errorDetail: string = error.response?.data.detail;
            if (errorDetail.includes('address')) {
                setError('address', {
                    type: 'validate',
                    message: `${prefix} с таким адресом уже существует`,
                });
            } else if (errorDetail.includes('phone')) {
                setError('phone', {
                    type: 'required',
                    message: `${prefix} с таким контакным телефоном уже существует`,
                });
            } else if (errorDetail.includes('email')) {
                setError('email', {
                    type: 'required',
                    message: `${prefix} с такой электронной почтой уже существует`,
                });
            }
        }
    };

    return { handleErrors };
};
