import { useCallback, useState } from 'react';

export interface IBooleanHandler {
    on: () => void;
    off: () => void;
    toggle: () => void;
}

export const useBooleanState = (defaultTurn = false) => {
    const [isTurn, setTurn] = useState<boolean>(defaultTurn);
    const on = useCallback(() => setTurn(true), []);
    const off = useCallback(() => setTurn(false), []);
    const toggle = useCallback(() => setTurn((turn) => !turn), [isTurn]);

    return [isTurn, { toggle, on, off }, setTurn] as const;
};
