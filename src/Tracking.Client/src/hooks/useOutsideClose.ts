import { useEffect, useRef } from 'react';

export const useOutsideClose = <TRef extends HTMLElement = HTMLDivElement>(
    close: () => void,
) => {
    const ref = useRef<TRef>(null);

    useEffect(() => {
        const onOutsideClick = (event: MouseEvent) => {
            const container = ref.current;

            if (container && !container.contains(event.target as Node)) {
                close();
            }
        };
        document.addEventListener('click', onOutsideClick);
        return () => document.removeEventListener('click', onOutsideClick);
    }, []);

    return ref;
};
