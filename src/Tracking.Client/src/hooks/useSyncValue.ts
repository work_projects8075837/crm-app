import { useEffect } from 'react';

export const useSyncValue = <T>(value: T, setTarget: (newValue: T) => void) => {
    useEffect(() => {
        setTarget(value);
    }, [value]);
};
