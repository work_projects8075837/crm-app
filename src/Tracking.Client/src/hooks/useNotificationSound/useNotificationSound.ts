import { Howl } from 'howler';

export const noteSound = new Howl({
    src: ['/click.mp3'],
    html5: true,
});
