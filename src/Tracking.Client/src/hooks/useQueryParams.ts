import React from 'react';
import { useLocation } from 'react-router-dom';

export const useQueryParams = () => {
    const { search } = useLocation();

    return React.useMemo(() => {
        const paramsList = search
            .slice(1)
            .split('&')
            .filter((p) => !!p)
            .map((param) => {
                const [key, value] = param.split('=', 2);

                // const handledValue = value.includes(';')
                //     ? value.split(';')
                //     : value;

                return [key, value as string | undefined] as const;
            });

        const params: { [key: string]: string | undefined } = {};

        for (const p of paramsList) {
            params[p[0]] = p[1];
        }

        return params;
    }, [search]);
};

export const useArrayQueryParameter = (name: string) => {
    const params = useQueryParams();

    return React.useMemo(() => {
        const value = params[name];
        if (value) {
            return value.includes(';') ? value.split(';') : [value];
        }
    }, [name, params]);
};
