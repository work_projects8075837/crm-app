import AuthLayout from '@/components/AuthLayout/AuthLayout';
import LoginForm from '@/components/Form/LoginForm/LoginForm';
import AuthProvider from '@/components/Providers/AuthGuard/AuthProvider';
import React from 'react';
import { Link, useHref, useLocation } from 'react-router-dom';

const Login: React.FC = () => {
    return (
        <>
            {/* <AuthProvider> */}
            <AuthLayout
                title='С возвращением!'
                subTitle='Войдите в систему, чтобы приступить к работе'
                afterChildren={
                    <>
                        <span>У Вас нет аккаунта?</span>
                        <Link
                            to={'/registration/'}
                            className={
                                'auth--btn auth--btn--margin auth-btn--underline'
                            }
                        >
                            Зарегистрироваться в системе
                        </Link>
                    </>
                }
            >
                <LoginForm />
            </AuthLayout>
            {/* </AuthProvider> */}
        </>
    );
};

export default Login;
