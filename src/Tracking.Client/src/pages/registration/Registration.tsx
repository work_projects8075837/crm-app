import AuthLayout from '@/components/AuthLayout/AuthLayout';
import RegistrationForm from '@/components/Form/RegistrationForm/RegistrationForm';
import React from 'react';
import { Link } from 'react-router-dom';

const Registration: React.FC = () => {
    return (
        <>
            <AuthLayout
                title='Welcome!'
                subTitle='Заполните поля ниже, чтобы зарегистрироваться в системе'
                afterChildren={
                    <>
                        <span>У меня уже есть аккаунт!</span>
                        <Link
                            to={'/login/'}
                            className={
                                'auth--btn auth--btn--margin auth-btn--underline'
                            }
                        >
                            Войти
                        </Link>
                    </>
                }
            >
                <RegistrationForm />
            </AuthLayout>
        </>
    );
};

export default Registration;
