import AuthLayout from '@/components/AuthLayout/AuthLayout';
import ResetForm from '@/components/Form/ResetForm/ResetForm';
import React from 'react';
import { Link } from 'react-router-dom';

const Reset: React.FC = () => {
    return (
        <>
            <AuthLayout
                title='Создание нового пароля'
                subTitle='Придумайте новый пароль, чтобы прступить к работе'
                afterChildren={
                    <>
                        <Link to={'/login/'} className='auth--btn back--login'>
                            Войти в систему
                        </Link>
                    </>
                }
            >
                <ResetForm />
            </AuthLayout>
        </>
    );
};

export default Reset;
