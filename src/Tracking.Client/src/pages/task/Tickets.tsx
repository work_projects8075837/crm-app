import FilterToggler from '@/components/Filter/Buttons/FilterToggler';
import FilterModal from '@/components/Filter/FilterModal/FilterModal';
import { IFilterModalHandler } from '@/components/Filter/FilterModalForm/FilterModalForm';
import BaseEntityHeader from '@/components/SalePoints/SalePointsHeader/BaseEntityHeader.tsx';
import { ticketFields } from '@/components/Tickets/TicketFields/TicketFields';
import TicketFilter from '@/components/Tickets/TicketFilter/TicketFilter';
import { ticketFilter } from '@/components/Tickets/TicketFilter/filter';
import TicketsList from '@/components/Tickets/TicketsList/TicketsList';
import { OpenHandler } from '@/store/states/OpenHandler';
import Layout from '../../components/NavbarLayout/Layout';

export const ticketFilterOpen = new OpenHandler();

export const ticketFilterOpener = new OpenHandler();

export const ticketFieldsOpen = new OpenHandler();

const Task = () => {
    return (
        <Layout>
            <BaseEntityHeader
                title={'Список заявок'}
                link={'/ticket/create/'}
                linkText='Новая заявка'
            >
                <FilterToggler openHandler={ticketFilterOpener} />
            </BaseEntityHeader>

            <TicketFilter />

            <FilterModal
                openHandler={ticketFilterOpen}
                filterHandler={ticketFilter as IFilterModalHandler}
            />

            <FilterModal
                openHandler={ticketFieldsOpen}
                filterHandler={ticketFields as IFilterModalHandler}
            />

            <TicketsList />
        </Layout>
    );
};

export default Task;
