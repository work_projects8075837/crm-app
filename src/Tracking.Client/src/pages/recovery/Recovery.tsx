import RecoveryForm from '@/components/Form/RecoveryForm/RecoveryForm';
import React from 'react';
import { Link } from 'react-router-dom';
import styles from '@/style/layout/auth.module.scss';
import Svg from '@/components/Elements/Svg/Svg';
import AuthLayout from '@/components/AuthLayout/AuthLayout';

const Recovery: React.FC = () => {
    return (
        <>
            <AuthLayout
                title='Забыли пароль?'
                subTitle='Восстановите пароль, чтобы приступить к работе'
                afterChildren={
                    <Link to={'/login/'} className='auth--btn back--login'>
                        « Назад ко входу
                    </Link>
                }
            >
                <RecoveryForm />
            </AuthLayout>
        </>
    );
};

export default Recovery;
