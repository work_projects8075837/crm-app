import { IContact } from '../contact/types';
import { ICounterparty } from '../counterparty/types';
import { IFile } from '../file/types';
import { IInventory } from '../inventory/types';
import { IPhoneNumber } from '../phone_number/types';
import { ITag } from '../tag/types';
import { IdEntity } from '../types';
import { IUser } from '../user/types';

export interface ISalePoint {
    position: ISalePointContact[];
    counterparty: ICounterparty[];
    file: IFile[];
    id: string;
    inventory: IInventory[];
    name: string;
    email: string;
    phone: string;
    address: string;
    sale_point_id: string | null;
    tag: ITag[];
    is_parent: boolean;
    user?: IUser;
    description: string;
    regulations?: string;
    service_finish?: string;
    status: string;
    phone_number: IPhoneNumber[];
}

export interface ISalePointContact {
    id: string;
    position: string;
    is_responsible: boolean;
    contact: IContact;
}

export interface ISalePointById {
    salePointId?: string;
}

export interface ICreateSalePoint {
    file?: IdEntity[];
    name: string;
    address: string;
    phone: string;
    email: string;
    director_contact_id?: string;
    counterparty?: IdEntity[];
    // sale_point_status:
    position?: IdEntity[];
    description?: string;
    sale_point_id?: string;
    tag?: ITag[];
    user_id?: string;
    regulations?: string;
    service_finish?: string;
    status?: string;
    phone_number?: IdEntity[];
}

export interface UpdateSalePoint extends Partial<ICreateSalePoint> {
    inventory?: IInventory[];
}

export type TSalePoint = ISalePoint & { [key: string]: string };

export interface ISalePointFilter {
    sale_point_sale_point_id: string | null;
    sale_point_name: string;
    is_parent: boolean;
}
