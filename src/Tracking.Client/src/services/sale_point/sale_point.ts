import { IOptionsProps } from '@/store/queries/stores/base/useAuthorizedQuery';
import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    ICreateSalePoint,
    ISalePoint,
    ISalePointFilter,
    UpdateSalePoint,
} from './types';

export const salePointService = servicerCreator.createService<
    ISalePoint,
    ICreateSalePoint,
    TPagination,
    ISalePointFilter,
    ISalePoint,
    UpdateSalePoint
>({ route: 'sale_point', searchFields: ['sale_point_name'] });

export const salePointApi = salePointService.api;

export const salePointKeys = salePointService.keys;

export const salePointHooks = {
    ...salePointService.hooks,
    useOne: (id?: string, options?: IOptionsProps) => {
        return salePointService.utils.useOneQuery(
            async () => {
                if (!id) {
                    throw Error();
                }
                const counterParty = await salePointApi
                    .getOne(id)
                    .then((res) => res.data);

                return {
                    ...counterParty,
                    sale_point: counterParty.sale_point_id
                        ? await salePointApi
                              .getOne(counterParty.sale_point_id)
                              .then((res) => res.data)
                        : null,
                };
            },
            id,
            options,
        );
    },
};
