import { IUser } from '../user/types';

export interface ICreateMember {
    type: string;
    user_id: string;
    ticket_id: string;
}

export interface CreatableMember {
    type: string;
    user_id: string;
}

export interface IMember {
    id: string;
    type: string;
    user: IUser;
}

export interface IMemberFilter {
    manager_user_name: string;
}
