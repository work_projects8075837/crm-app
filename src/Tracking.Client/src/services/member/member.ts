import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { ICreateMember, IMember, IMemberFilter } from './types';

class MemberService {
    async create(data: ICreateMember) {
        return await serviceManager.post<IMember>(`/manager/`, data, {
            isAuth: true,
        });
    }

    async update(id: string, data: { type?: string }) {
        return await serviceManager.patch<IMember>(`/manager/${id}/`, data, {
            isAuth: true,
        });
    }

    async delete(id: string) {
        return await serviceManager.delete(`/manager/${id}/`, {
            isAuth: true,
        });
    }
}

export const memberService = servicerCreator.createService<
    IMember,
    ICreateMember,
    TPagination,
    IMemberFilter
>({ route: 'manager', searchFields: ['manager_user_name'] });

export const memberKeys = memberService.keys;

export const memberApi = memberService.api;

export const memberHooks = memberService.hooks;
