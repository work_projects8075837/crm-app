import { IContact } from '../contact/types'
import { ISalePoint } from '../sale_point/types'

export interface IContactPosition {
    id: string;
    contact: IContact;
    sale_point: ISalePoint;
    is_responsible: boolean;
    position: string;
}

export interface IPosition {
    id: string;
    contact_id: string;
    sale_point: {
        id: string;
        name: string;
        email: string;
    };
    position: string;
    is_responsible: boolean;
}

export interface ICreateContactPosition {
    contact_id: string;
    sale_point_id: string;
    position: string;
    is_responsible: boolean;
}

export type IUpdateContactPosition = Partial<ICreateContactPosition>;

export interface IContactPositionFilter {
    position_position: string;
}
