import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    IContactPosition,
    IContactPositionFilter,
    ICreateContactPosition,
    IUpdateContactPosition,
} from './types';

class ContactPositionService {
    async create(data: ICreateContactPosition) {
        return await serviceManager.post<IContactPosition>('/position/', data, {
            isAuth: true,
        });
    }

    async update(id: string, data: IUpdateContactPosition) {
        return await serviceManager.patch<IContactPosition>(
            `/position/${id}/`,
            data,
            {
                isAuth: true,
            },
        );
    }

    async delete(id: string) {
        return await serviceManager.delete(`/position/${id}/`, {
            isAuth: true,
        });
    }
}

export const contactPositionService = servicerCreator.createService<
    IContactPosition,
    ICreateContactPosition,
    TPagination,
    IContactPositionFilter,
    IContactPosition,
    IUpdateContactPosition
>({ route: 'position', searchFields: ['position_position'] });

export const contactPositionKeys = contactPositionService.keys;

export const contactPositionApi = contactPositionService.api;

export const contactPositionHooks = contactPositionService.hooks;
