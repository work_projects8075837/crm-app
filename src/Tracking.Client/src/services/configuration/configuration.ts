import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    IConfiguration,
    IConfigurationFilter,
    ICreateConfiguration,
    IUpdateConfiguration,
} from './types';

export const configurationService = servicerCreator.createService<
    IConfiguration,
    ICreateConfiguration,
    TPagination,
    IConfigurationFilter,
    IConfiguration,
    IUpdateConfiguration
>({ route: 'configuration', searchFields: ['connection_type'] });

export const configurationKeys = configurationService.keys;

export const configurationApi = configurationService.api;

export const configurationHooks = configurationService.hooks;
