export interface IConfiguration {
    id: string;
    login?: string;
    password?: string;
    other?: string;
}

export interface ICreateConfiguration extends Omit<IConfiguration, 'id'> {
    connection_id?: string;
}

export type IUpdateConfiguration = Partial<ICreateConfiguration>;

export interface IConfigurationFilter {
    connection_type: string;
}
