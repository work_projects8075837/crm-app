import { serviceManager } from '../utils/instance/ServiceManager';
import { IFile } from './types';

class FileService {
    async create(files: FileList | File[]) {
        return serviceManager.post<IFile[]>(
            '/file/',
            { files },
            {
                isAuth: true,
                headers: {
                    'Content-Type': 'multipart/form-data',
                },

                formSerializer: {
                    metaTokens: false,
                    dots: false,
                    indexes: null,
                },
            },
        );
    }
}

export const fileService = new FileService();
