export interface IFile {
    id: string;
    path: string;
    size: number;
    user: { name: string };
    name: string;
    created_at: string;
}
