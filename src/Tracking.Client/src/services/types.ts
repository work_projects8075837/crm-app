export interface PaginatedData<T> {
    next: string | null;
    prev: string | null;
    pages: number;
    data: T;
}

export interface PaginationParameters {
    page: number;
    offset: number;
}

export interface IdEntity {
    id?: string;
}

export interface IDeepInnerEntity {
    id: string;
    name: string;
}

export interface TDict<T = string> {
    [key: string]: T;
}
