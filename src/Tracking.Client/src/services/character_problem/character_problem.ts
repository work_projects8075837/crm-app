import { IOptionsProps } from '@/store/queries/stores/base/useAuthorizedQuery';
import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    ICharacterProblem,
    ICharacterProblemFilter,
    ICreateCharacterProblem,
} from './types';

const characterProblemService = servicerCreator.createService<
    ICharacterProblem,
    ICreateCharacterProblem,
    TPagination,
    ICharacterProblemFilter
>({
    route: 'character_problem',
    searchFields: ['character_problem_name'],
});

export const characterProblemApi = characterProblemService.api;

export const characterProblemKeys = characterProblemService.keys;

export const characterProblemHooks = {
    ...characterProblemService.hooks,
    useOne: (id?: string, options?: IOptionsProps) => {
        return characterProblemService.utils.useOneQuery(
            async () => {
                if (!id) {
                    throw Error();
                }
                const counterParty = await characterProblemApi
                    .getOne(id)
                    .then((res) => res.data);

                return {
                    ...counterParty,
                    character_problem: counterParty.character_problem_id
                        ? await characterProblemApi
                              .getOne(counterParty.character_problem_id)
                              .then((res) => res.data)
                        : null,
                };
            },
            id,
            options,
        );
    },
};
