import { IJob } from '../job/types';
import { ITag } from '../tag/types';
import { IdEntity, TDict } from '../types';

export interface ICharacterProblem {
    id: string;
    name: string;
    sla: number;
    url: string;
    job: IJob[];
    tag: ITag[];
    is_parent: boolean;
    character_problem_id: string | null;
}

export interface IGetAllCharactersProblems {
    page?: number;
    offset?: number;
}

export interface ICreateCharacterProblem {
    name: string;
    sla: number;
    url: string;
    job: IdEntity[];
    tag: ITag[];
    is_parent: boolean;
    character_problem_id?: string | null;
}

export type TCharacterProblem = ICharacterProblem & TDict;

export type ICharacterProblemFilter = Partial<{
    character_problem_character_problem_id?: string | null;
    character_problem_name: string;
}>;
