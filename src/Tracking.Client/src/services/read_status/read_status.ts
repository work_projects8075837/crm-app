import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { PaginatedData } from '../types';
import { generateQuery } from '../utils/generateQuery/generateQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    ICreateReadStatus,
    IReadStatus,
    IUpdateReadStatus,
    ReadStatusFilter,
} from './types';
('./types');

class ReadStatusService {
    async getAll(data?: TPagination & ReadStatusFilter) {
        return await serviceManager.get<PaginatedData<IReadStatus[]>>(
            `/read_status/${generateQuery(data, false)}`,
            {
                isAuth: true,
            },
        );
    }
    async getSearch(search?: string) {
        return await serviceManager.get<PaginatedData<IReadStatus[]>>(
            `/read_status/${generateQuery({
                notification_name: search,
            })}`,
            {
                isAuth: true,
            },
        );
    }

    async getOneById(id: string) {
        return await serviceManager.get<IReadStatus>(`/read_status/${id}/`, {
            isAuth: true,
        });
    }

    async create(data: ICreateReadStatus) {
        return await serviceManager.post<IReadStatus>(`/read_status/`, data, {
            isAuth: true,
        });
    }

    async update(id: string, data: IUpdateReadStatus) {
        return await serviceManager.patch<IReadStatus>(
            `/read_status/${id}/`,
            data,
            {
                isAuth: true,
            },
        );
    }

    async confirm(data: string[]) {
        return await serviceManager.post(
            `/read_status/confirm/`,
            { ids: data },
            {
                isAuth: true,
            },
        );
    }
}

export const readStatusService = servicerCreator.createService<
    IReadStatus,
    ICreateReadStatus,
    TPagination,
    ReadStatusFilter
>({ route: 'read_status', searchFields: ['notification_name'] });

export const readStatusKeys = readStatusService.keys;

export const readStatusApi = {
    ...readStatusService.api,
    async confirm(data: string[]) {
        return await serviceManager.post(
            `/read_status/confirm/`,
            { ids: data },
            {
                isAuth: true,
            },
        );
    },
};

export const readStatusHooks = readStatusService.hooks;
