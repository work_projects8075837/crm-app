import { INotification } from '../notification/types';
import { IdEntity } from '../types';
import { IUser } from '../user/types';

export interface IReadStatus {
    id: string;
    read: boolean;
    user: IUser;
    notification: Omit<INotification, 'read_status'>[];
}

export interface ICreateReadStatus {
    read: boolean;
    user_id: string;
    notification: IdEntity[];
}

export type IUpdateReadStatus = Partial<ICreateReadStatus>;

export type ReadStatusFilter = Partial<{
    read_status_user_id: string;
    read_status_is_read: boolean;
    notification_name: string;
}>;
