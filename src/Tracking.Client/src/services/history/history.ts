import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { ITicketFilter, ITicketHistory } from './type';

export const historyService = servicerCreator.createService<
    ITicketHistory,
    Partial<ITicketHistory>,
    TPagination,
    ITicketFilter
>({ route: 'history', searchFields: [] });

export const historyHooks = historyService.hooks;

export const historyApi = historyService.api;

export const historyKeys = historyService.keys;
