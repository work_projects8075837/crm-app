import { ITicket } from '../ticket/types';

export interface ITicketHistory {
    id: string;
    is_hide: boolean;
    event: string;
    ticket_id: string;
    previous_state: ITicket | null;
    current_state: ITicket;
    user: {
        name: string;
    };
    created_at: string;
}

export interface ITicketFilter {
    ticket_history_ticket_id: string;
}

/**
import { ITicketHistory } from './../../services/history/type';
 * {
      "id": "3e43dd40-8585-4c4c-9ff3-35ecf370b3ae",
      "files": [],
      "number": 6,
      "description": {
        "content": "<p>ывава</p>"
      },
      "created_at": "2023-11-01T15:24:18.447256",
      "finished_at": null,
      "closed_at": null,
      "current_status": {
        "id": "9149f208-713d-47d1-b39c-cd5e970dc699",
        "name": "Недозвон",
        "sla": false,
        "color": "#D50000",
        "background": "#FF7F7F",
        "preview_link": "https://dragon.dedibyte.ru/media/red.svg",
        "direction_id": "e4a7f1fb-dbb0-499a-9b61-55edd2ac1102"
      },
      "channel": {
        "id": "2e8aba06-acf2-44d9-a7e8-94c4a382ebc5",
        "name": "Через знакомых",
        "multiple": 2
      },
      "direction": {
        "id": "e4a7f1fb-dbb0-499a-9b61-55edd2ac1102",
        "name": "Звонок клиенту",
        "statuses": [
          {
            "id": "7dd1a8c8-1527-4ddf-bd76-1099c27ebc1b",
            "name": "Выполнено",
            "sla": false,
            "color": "#65A300",
            "background": "#C2FF5F",
            "preview_link": "https://dragon.dedibyte.ru/media/success.svg",
            "direction_id": "e4a7f1fb-dbb0-499a-9b61-55edd2ac1102"
          },
          {
            "id": "e12c0f69-bece-45d7-b46f-0750df18cbb8",
            "name": "Новая",
            "sla": true,
            "color": "#E1980C",
            "background": "#FFE978",
            "preview_link": "https://dragon.dedibyte.ru/media/new.svg",
            "direction_id": "e4a7f1fb-dbb0-499a-9b61-55edd2ac1102"
          },
          {
            "id": "9149f208-713d-47d1-b39c-cd5e970dc699",
            "name": "Недозвон",
            "sla": false,
            "color": "#D50000",
            "background": "#FF7F7F",
            "preview_link": "https://dragon.dedibyte.ru/media/red.svg",
            "direction_id": "e4a7f1fb-dbb0-499a-9b61-55edd2ac1102"
          }
        ]
      },
      "character_problem": {
        "id": "b7464648-3c06-4f28-84b1-e15b6f4f5ad7",
        "name": "Поломка компьютера",
        "sla": 80000,
        "url": "https://dragon.dedibyte.ru/admin",
        "jobs": [
          {
            "id": "760665e2-4bb7-45ba-a296-37ba3c3dfc05",
            "name": "Включить компьютер",
            "duration": 100,
            "character_problem_id": "b7464648-3c06-4f28-84b1-e15b6f4f5ad7"
          },
          {
            "id": "b4ab32da-13ee-4318-b694-10305ec09370",
            "name": "Перезагрузить",
            "duration": 0,
            "character_problem_id": "b7464648-3c06-4f28-84b1-e15b6f4f5ad7"
          }
        ]
      },
      "contact": {
        "id": "e237026d-0214-416e-8fcb-b6fa4a7e1200",
        "name": "Бог",
        "phone": "+7 (900) 000 00-00",
        "position": "Не последний чел",
        "email": "testov@gmali.com"
      },
      "sale_point": {
        "id": "10150cd0-952f-4fc3-9d0b-0581091e3e0c",
        "name": "Шаурма у ашота",
        "sale_point_id": null,
        "counterparties": [
          {
            "id": "d9707f0b-dea8-4e3b-a3f5-bab5d973e563",
            "tag": "Общепит",
            "name": "Петров Петр Петрович",
            "phone": "+7 934 133 33 12",
            "tin": "7743013902",
            "address": "ул Пушкина, 123",
            "comment": "Хороший клиент",
            "email": "some@some.com",
            "files": []
          }
        ],
        "contacts": [
          {
            "id": "e237026d-0214-416e-8fcb-b6fa4a7e1200",
            "name": "Бог",
            "phone": "+7 (900) 000 00-00",
            "position": "Не последний чел",
            "email": "testov@gmali.com"
          },
          {
            "id": "4e43534d-c9d3-4150-8a5a-125d5f4dac2b",
            "name": "Чел 1",
            "phone": "+7 952 571-05-51",
            "position": "Не последний чел",
            "email": "work@flagman-it.ru"
          },
          {
            "id": "dffd1cc6-07da-4086-b93f-a43f39b803ea",
            "name": "Исус",
            "phone": "+7 (777) 777 77-77",
            "position": "Опущенный",
            "email": "testov@gmali.com"
          },
          {
            "id": "f39bdba9-871e-468e-b66b-6da364fad87f",
            "name": "Тестовый Тест Тестович",
            "phone": "+7 952 571-05-51",
            "position": "Не последний чел",
            "email": "n.kulpinov@flagman-it.ru"
          },
          {
            "id": "6ec530a8-f0be-4197-87cb-ee411ac556f5",
            "name": "Важный чел",
            "phone": "+7 952 571-05-51",
            "position": "Опущенный",
            "email": "granof.web@gmail.com"
          },
          {
            "id": "6d1ce54c-f753-4b1a-b697-e8ea0d3fba7e",
            "name": "123",
            "phone": "+7 952 571-05-51",
            "position": "Опущенный",
            "email": "testov@gmali.com"
          }
        ],
        "managers": [],
        "inventories": [
          {
            "id": "aa08fcfb-fbe0-4825-a164-35bb44c23da9",
            "name": "Гриль",
            "serial_number": "949tdkwkw3",
            "is_work": false,
            "parent_id": null,
            "sale_point_id": "10150cd0-952f-4fc3-9d0b-0581091e3e0c",
            "connections": [],
            "software": [
              {
                "id": "883bd5c3-3c05-4d7f-81aa-d9b24623403a",
                "name": "Гриль OS",
                "version": "1.1.1"
              }
            ]
          },
          {
            "id": "27247e2e-28cb-4caf-817b-65b8657b2c17",
            "name": "Лопатка на пульте управления",
            "serial_number": "efrfru33owp3",
            "is_work": false,
            "parent_id": null,
            "sale_point_id": "10150cd0-952f-4fc3-9d0b-0581091e3e0c",
            "connections": [
              {
                "id": "9d842021-2e8f-48db-afba-452c35bc714d",
                "type": "TeamViewer",
                "received": {}
              }
            ],
            "software": []
          }
        ],
        "files": []
      },
      "counterparty": null,
      "tags": [
        {
          "id": "60ef0d1c-f394-444d-87ca-f617d8cf61a3",
          "name": "ee"
        },
        {
          "id": "45647d1f-0ffa-49fa-baec-15d2c9aff345",
          "name": "ccccc"
        },
        {
          "id": "403b69e7-0f0f-4580-8997-4c2ae5abf8b6",
          "name": "eefd"
        }
      ],
      "members": [
        {
          "id": "ecc71a76-3eee-40de-902c-23facdb27811",
          "type": "Ответственный",
          "user": {
            "id": "26d15201-a6a5-4a7e-9404-045b980e80ff",
            "name": "Петров Петр Петрович",
            "status": "",
            "email": "email@flagman-it.ru",
            "phone": "",
            "activate": true,
            "post": "Человек",
            "file": "",
            "blocking": false,
            "roles": [
              {
                "id": "4f12d88b-0050-4bfb-9ad0-27fd20c83ec0",
                "name": "User",
                "permission": []
              }
            ]
          }
        }
      ],
      "comments": [],
      "ticket_jobs": [],
      "job_results": []
    }
 */
