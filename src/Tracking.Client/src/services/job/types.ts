import { ICharacterProblem } from '../character_problem/types';

export interface IJob {
    character_problem: ICharacterProblem;
    duration?: number;
    id: string;
    name: string;
    is_parent: boolean;
    job_id?: string;
}

export interface ICreateJob {
    name: string;
    duration?: number;
    job_id?: string;
}

export type IUpdateJob = Partial<ICreateJob>;

export interface IJobFilterFields {
    job_job_id: string | null;
    job_name: string;
    is_hard: boolean;
}

export type JobFilter = Partial<IJobFilterFields>;
