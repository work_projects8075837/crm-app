import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { PaginatedData } from '../types';
import { generateQuery } from '../utils/generateQuery/generateQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { ICreateJob, IJob, IUpdateJob, JobFilter } from './types';

class JobService {
    async getAll(data: TPagination & JobFilter) {
        return await serviceManager.get<PaginatedData<IJob[]>>(
            `/job/${generateQuery(data, false)}`,
            {
                isAuth: true,
            },
        );
    }

    async getAllSearch(search?: string, jobId?: string | null) {
        return await serviceManager.get<PaginatedData<IJob[]>>(
            `/job/${generateQuery(
                {
                    job_name: search,
                    job_job_id: jobId,
                },
                false,
            )}`,
            {
                isAuth: true,
            },
        );
    }

    async getOneById(id: string) {
        return await serviceManager.get<IJob>(`/job/${id}/`, {
            isAuth: true,
        });
    }

    async create(data: ICreateJob) {
        return await serviceManager.post<IJob>(`/job/`, data, {
            isAuth: true,
        });
    }

    async update(id: string, data: IUpdateJob) {
        return await serviceManager.patch<IJob>(`/job/${id}/`, data, {
            isAuth: true,
        });
    }

    async deleteArray(ids: string[]) {
        return await serviceManager.post(
            `/job/delete/list/`,
            { ids },
            {
                isAuth: true,
            },
        );
    }
}

export const jobService = servicerCreator.createService<
    IJob,
    ICreateJob,
    TPagination,
    JobFilter,
    IJob,
    IUpdateJob
>({ route: 'job', searchFields: ['job_name'] });

export const jobKeys = jobService.keys;

export const jobApi = jobService.api;

export const jobHooks = jobService.hooks;
