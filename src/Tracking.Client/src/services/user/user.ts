import { PaginatedData } from '../types';
import { generateQuery } from '../utils/generateQuery/generateQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { IUser } from './types';

class UserService {
    async getAll(search?: string) {
        const queryString = generateQuery({ user_name: search });

        return await serviceManager.get<PaginatedData<IUser[]>>(
            `/auth/user/${queryString}`,
            {
                isAuth: true,
            },
        );
    }
    async getCurrentUser() {
        return await serviceManager.get<IUser>('/auth/me/', {
            isAuth: true,
        });
    }
}

export const userService = new UserService();
