import { IChannel } from '../channel/types';
import { IDirection } from '../direction/types';

export type UserStatusT = 'free' | 'work' | 'rest' | 'out';

export interface IUser {
    activate: boolean;
    blocking: boolean;
    email: string;
    file: string;
    id: string;
    name: string;
    phone?: string;
    position: string;
    role: IRole[];
    status: UserStatusT;
    channel: IChannel;
    direction: IDirection;
}

export interface IRole {
    name: string;
    permission: IPermission;
}

export interface IPermission {
    can: string;
}
