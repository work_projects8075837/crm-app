import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { ICreateDirection, IDirection, IDirectionFilter } from './types';

export const directionService = servicerCreator.createService<
    IDirection,
    ICreateDirection,
    TPagination,
    IDirectionFilter,
    IDirection
>({ route: 'direction', searchFields: ['direction_name'] });

export const directionKeys = directionService.keys;

export const directionApi = directionService.api;

export const directionHooks = directionService.hooks;
