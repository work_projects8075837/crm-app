import { IStatus } from '../status/types';
import { IdEntity } from '../types';

export interface IDirection {
    id: string;
    name: string;
    status: IStatus[];
}

export interface IDirectionById {
    directionId: string;
}

export interface ICreateDirection {
    name: string;
    status?: IdEntity[];
}

export interface IDirectionFilter {
    direction_name: string;
}
