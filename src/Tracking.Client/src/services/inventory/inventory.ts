import { IOptionsProps } from '@/store/queries/stores/base/useAuthorizedQuery';
import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    ICreateInventory,
    IInventory,
    IInventoryFilter,
    IUpdateInventory,
} from './types';

export const inventoryService = servicerCreator.createService<
    IInventory,
    ICreateInventory,
    TPagination,
    IInventoryFilter,
    IInventory,
    IUpdateInventory
>({ route: 'inventory', searchFields: ['inventory_name'] });

export const inventoryKeys = inventoryService.keys;

export const inventoryApi = inventoryService.api;

export const inventoryHooks = {
    ...inventoryService.hooks,
    useOne: (id?: string, options?: IOptionsProps) => {
        return inventoryService.utils.useOneQuery(
            async () => {
                if (!id) {
                    throw Error();
                }
                const counterParty = await inventoryApi
                    .getOne(id)
                    .then((res) => res.data);

                return {
                    ...counterParty,
                    inventory: counterParty.inventory_id
                        ? await inventoryApi
                              .getOne(counterParty.inventory_id)
                              .then((res) => res.data)
                        : null,
                };
            },
            id,
            options,
        );
    },
};
