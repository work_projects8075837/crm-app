import { IConnection } from '../connection/types';
import { ISoftware } from '../software/types';
import { IdEntity } from '../types';

export interface IInventory {
    connection: IConnection[];
    id: string;
    is_work: boolean;
    name: string;
    inventory_id?: string | null;
    serial_number: string;
    software: ISoftware[];
    is_parent: boolean;
}

export interface ICreateInventory {
    connection: IdEntity[];
    name: string;
    serial_number?: string;
    software: ISoftware[];
    is_parent?: boolean;
    inventory_id?: string;
}

export type IUpdateInventory = Partial<ICreateInventory>;

export interface IInventoryFilter {
    inventory_is_connection: boolean;
    is_parent: boolean;
    inventory_name: string;
    inventory_sale_point_id: string | null;
    inventory_inventory_id: string | null;
}

export type TInventoryFilter = Partial<IInventoryFilter>;
