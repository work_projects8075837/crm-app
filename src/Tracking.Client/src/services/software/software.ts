import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { ICreateSoftware, ISoftware, ISoftwareFilter } from './types';

export const softwareService = servicerCreator.createService<
    ISoftware,
    ICreateSoftware,
    TPagination,
    ISoftwareFilter
>({ route: 'software', searchFields: ['software_name'] });

export const softwareApi = softwareService.api;

export const softwareKeys = softwareService.keys;

export const softwareHooks = softwareService.hooks;
