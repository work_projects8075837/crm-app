export interface ISoftware {
    id: string;
    name: string;
    version: string;
}

export type ICreateSoftware = Omit<ISoftware, 'id'>;

export interface ISoftwareFilter {
    software_name: string;
}
