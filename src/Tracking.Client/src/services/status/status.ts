import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { ICreateStatus, IStatus, IStatusFilter } from './types';

const statusService = servicerCreator.createService<
    IStatus,
    ICreateStatus,
    TPagination,
    IStatusFilter
>({
    route: 'status',
    searchFields: ['status_name'],
});

export const statusApi = statusService.api;

export const statusKeys = statusService.keys;

const { useList, useSearch } = statusService.hooks;

export { useList as useStatusList, useSearch as useStatusSearch };
