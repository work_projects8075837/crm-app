export interface IStatus {
    sla: boolean;
    background: string;
    color: string;
    id: string;
    name: string;
    preview_link: string;
    is_default?: boolean;
    is_finish?: boolean;
}

export type ICreateStatus = Omit<IStatus, 'id'>;

export interface IStatusFilter {
    status_name: string;
}
