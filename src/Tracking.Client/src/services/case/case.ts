import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { PaginatedData } from '../types';
import { generateQuery } from '../utils/generateQuery/generateQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    CaseFilter,
    ICase,
    ICaseFilter,
    ICreateCase,
    IUpdateCase,
} from './types';

class CaseService {
    async getAll(data?: TPagination & CaseFilter) {
        return await serviceManager.get<PaginatedData<ICase[]>>(
            `/case/${generateQuery(data, false)}`,
            { isAuth: true },
        );
    }

    async getOneById(id: string) {
        return await serviceManager.get<ICase>(`/case/${id}/`, {
            isAuth: true,
        });
    }

    async getSearch(search?: string) {
        return await serviceManager.get<PaginatedData<ICase[]>>(
            `/channel/${generateQuery({ case_name: search })}`,
            { isAuth: true },
        );
    }

    async create(data: ICreateCase) {
        return await serviceManager.post<ICase>(`/case/`, data, {
            isAuth: true,
        });
    }

    async update(id: string, data: IUpdateCase) {
        return await serviceManager.patch<ICase>(`/case/${id}/`, data, {
            isAuth: true,
        });
    }

    async delete(id: string) {
        return await serviceManager.delete<ICase>(`/case/${id}/`, {
            isAuth: true,
        });
    }
}

export const caseService = servicerCreator.createService<
    ICase,
    ICreateCase,
    TPagination,
    ICaseFilter,
    ICase,
    IUpdateCase
>({ route: 'case', searchFields: ['case_name'] });

export const caseKeys = caseService.keys;

export const caseApi = caseService.api;

export const caseHooks = caseService.hooks;
