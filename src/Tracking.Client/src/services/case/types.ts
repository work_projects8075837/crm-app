import { IMember } from '../member/types';

export interface ICase {
    id: string;
    name: string;
    finish_date: string;
    finish_time: string;
    manager?: IMember;
    comment: string;
    ticket_id: string;
    is_completed: boolean;
}

export interface ICreateCase {
    name: string;
    finish_date: string;
    finish_time: string;
    manager_id: string;
    ticket_id: string;
    comment: string;
    is_completed?: boolean;
}

export interface IUpdateCase extends Partial<ICreateCase> {
    is_completed?: boolean;
}

export interface ICaseFilter {
    case_ticket_id: string;
    manager_user_id: string;
    case_name: string;
}

export type CaseFilter = Partial<ICaseFilter>;
