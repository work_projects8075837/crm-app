import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { CreateResultJob, IJobResult, JobResultFilter } from './types';

export const jobResultService = servicerCreator.createService<
    IJobResult,
    CreateResultJob,
    TPagination,
    JobResultFilter
>({
    route: 'job_result',
    searchFields: ['job_name'],
});

export const jobResultApi = jobResultService.api;
