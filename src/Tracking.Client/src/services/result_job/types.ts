import { IJob } from '../job/types';
import { IMember } from '../member/types';

export interface IJobResult {
    id: string;
    job: IJob | null;
    time: number;
    manager: IMember | null;
}

export interface CreateResultJob {
    manager_id: string;
    job_id: string;
    time: number;
}

export interface CreateResultJobToTicket extends CreateResultJob {
    ticketId: string;
}

export type JobResultFilter = Partial<{
    ticket_id: string;
    job_name: string;
}>;
