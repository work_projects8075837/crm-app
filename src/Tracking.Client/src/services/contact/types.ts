import { IPosition } from '../contact_position/types';
import { IPhoneNumber } from '../phone_number/types';
import { IdEntity } from '../types';

export interface IContact {
    email: string;
    id: string;
    name: string;
    phone_number: IPhoneNumber[];
    position: IPosition[];
}

export interface CreateContact
    extends Omit<Omit<Omit<IContact, 'id'>, 'phone_number'>, 'position'> {
    phone_number?: IdEntity[];
}

export type IUpdateContact = Partial<CreateContact>;

export interface IContactFilter {
    is_hard: boolean;
    contact_name: string;
    phone_number_name: string;
    contact_email: string;
}
