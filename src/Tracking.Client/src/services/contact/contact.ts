import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { CreateContact, IContact, IContactFilter } from './types';

export const contactService = servicerCreator.createService<
    IContact,
    CreateContact,
    TPagination,
    IContactFilter
>({
    route: 'contact',
    searchFields: ['contact_email', 'contact_name', 'phone_number_name'],
});

export const contactApi = contactService.api;

export const contactKeys = contactService.keys;

export const contactHooks = contactService.hooks;
