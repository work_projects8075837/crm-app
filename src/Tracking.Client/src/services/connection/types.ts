import { IConfiguration } from '../configuration/types';
import { IdEntity } from '../types';

export interface IConnection {
    id: string;
    type: string;
    configuration: IConfiguration[];
}

export interface IData {
    ip: string;
    password: string;
    username: string;
}

export interface ICreateConnection {
    type: string;
    configuration?: IdEntity[];
}

export type IUpdateConnection = Partial<ICreateConnection>;

export interface IConnectionFilter {
    connection_type: string;
}
