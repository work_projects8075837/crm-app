import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    IConnection,
    IConnectionFilter,
    ICreateConnection,
    IUpdateConnection,
} from './types';

class ConnectionService {
    async create(data: ICreateConnection) {
        return await serviceManager.post<IConnection>('/connection/', data, {
            isAuth: true,
        });
    }

    async update(id: string, data: IUpdateConnection) {
        return await serviceManager.patch<IConnection>(
            `/connection/${id}/`,
            data,
            {
                isAuth: true,
            },
        );
    }

    async delete(id: string) {
        return await serviceManager.delete(`/connection/${id}/`, {
            isAuth: true,
        });
    }
}

export const connectionService = servicerCreator.createService<
    IConnection,
    ICreateConnection,
    TPagination,
    IConnectionFilter,
    IConnection,
    IUpdateConnection
>({ route: 'connection', searchFields: ['connection_type'] });

export const connectionKeys = connectionService.keys;

export const connectionApi = connectionService.api;

export const connectionHooks = connectionService.hooks;
