import { CookiesStorage } from '../utils/CookieStorage';
import { BASE_DOMAIN } from '../utils/instance/ServiceManager';

const handlingTypes = ['user', 'notification'];

interface Msg {
    type: string;
    data: unknown;
}

type IWebsocketHandler<M extends Msg> = {
    type: string;
    handler: (data: M) => void;
};

export const createWebsocketConsumer = <M extends Msg>({
    route = 'ws',
    handlers,
}: {
    route?: string;
    handlers: IWebsocketHandler<M>[];
}) => {
    let ws: WebSocket | null = null;
    const openHandler = () => {
        console.log('Websocket connection Open');
    };

    const cleanup = () => {
        console.log('Websocket connection Closed');
        if (!ws) return;
        ws.removeEventListener('open', openHandler);
        ws.removeEventListener('close', closeHandler);
        ws.removeEventListener('message', messageHandler);
        ws.close();
    };

    const closeHandler = () => {
        cleanup();
        setTimeout(createConnection, 3000);
    };

    const messageHandler = (event: MessageEvent<string>) => {
        try {
            const message: M = JSON.parse(
                event.data
                    .replaceAll("'", '"')
                    .replaceAll('True', 'true')
                    .replaceAll('False', 'false')
                    .replaceAll('None', 'null'),
            );
            console.log(message);
            const type = message.type;

            if (type) {
                for (const handler of handlers) {
                    if (handler.type === type) {
                        handler.handler(message);
                        break;
                    }
                }
            }
        } catch (e) {
            console.log(e);
            console.log(event.data);
        }
    };

    const createConnection = () => {
        if (ws) cleanup();

        ws = new WebSocket(
            `${
                BASE_DOMAIN.includes('localhost') ? 'ws' : 'wss'
            }://${BASE_DOMAIN}/${route}?token=${CookiesStorage.getItem(
                'access',
            )}`,
        );

        ws.addEventListener('open', openHandler);
        ws.addEventListener('message', messageHandler);
        ws.addEventListener('close', closeHandler);
    };

    return {
        ws,
        openHandler,
        close,
        cleanup,
        createConnection,
        messageHandler,
    };
};
