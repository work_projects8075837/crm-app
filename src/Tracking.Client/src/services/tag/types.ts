export interface ITag {
    id: string;
    name: string;
}

export type TagCreate = Omit<ITag, 'id'>;

export interface ITagFilter {
    tag_name: string;
}
