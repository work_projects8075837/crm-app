import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { generateQuery } from '../utils/generateQuery/generateQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import { PaginatedData } from './../types';
import { ITag, ITagFilter, TagCreate } from './types';

class TagService {
    async getAll(pag?: TPagination) {
        const queryString = generateQuery(pag);
        return await serviceManager.get<PaginatedData<ITag[]>>(
            `/tag/${queryString}`,
            {
                isAuth: true,
            },
        );
    }

    async getSearch(search?: string) {
        const queryString = generateQuery({ tag_name: search });
        return await serviceManager.get<PaginatedData<ITag[]>>(
            `/tag/${queryString}`,
            {
                isAuth: true,
            },
        );
    }

    async create({ name }: TagCreate) {
        return await serviceManager.post<ITag>(
            '/tag/',
            { name },
            {
                isAuth: true,
            },
        );
    }

    async update(id: string, data: TagCreate) {
        return await serviceManager.patch<ITag>(`/tag/${id}/`, data, {
            isAuth: true,
        });
    }

    async deleteArray(ids: string[]) {
        return await serviceManager.post(
            `/tag/delete/list/`,
            { ids },
            {
                isAuth: true,
            },
        );
    }
}

export const tagService = servicerCreator.createService<
    ITag,
    TagCreate,
    TPagination,
    ITagFilter,
    ITag
>({ route: 'tag', searchFields: ['tag_name'] });

export const tagKeys = tagService.keys;

export const tagApi = tagService.api;

export const tagHooks = tagService.hooks;
