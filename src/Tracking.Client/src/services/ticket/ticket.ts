import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    ICreateTicket,
    ICurrentTicket,
    ITicket,
    ITicketFilter,
    IUpdateTicket,
} from './types';

export const ticketService = servicerCreator.createService<
    ITicket,
    ICreateTicket,
    TPagination,
    ITicketFilter,
    ICurrentTicket,
    Partial<IUpdateTicket>
>({
    route: 'ticket',
    searchFields: ['ticket_number'],
});

export const ticketApi = ticketService.api;

export const ticketKeys = ticketService.keys;

const { useList, useOne, useSearch } = ticketService.hooks;

export {
    useOne as useTicketById,
    useList as useTicketList,
    useSearch as useTicketSearch,
};
