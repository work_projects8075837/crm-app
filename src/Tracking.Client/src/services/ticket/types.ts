import { ICase } from '../case/types';
import { IChannel } from '../channel/types';
import { ICharacterProblem } from '../character_problem/types';
import { IComment } from '../comment/types';
import { IContact } from '../contact/types';
import { ICounterparty } from '../counterparty/types';
import { IDirection } from '../direction/types';
import { IFile } from '../file/types';
import { IInventory } from '../inventory/types';
import { IMember } from '../member/types';
import { IJobResult } from '../result_job/types';
import { ISalePoint } from '../sale_point/types';
import { IStatus } from '../status/types';
import { ITag } from '../tag/types';
import { IdEntity } from '../types';
import { TDict } from './../types';

export interface ITicket {
    number?: number;
    channel?: IChannel;
    character_problem?: ICharacterProblem;
    comment: IComment[];
    contact?: IContact;
    description?: string;
    decision?: string;
    direction?: IDirection;
    file: IFile[];
    id: string;
    job_result: IJobResult[];
    manager: IMember[];
    sale_point?: ISalePoint;
    tag: ITag[];
    status?: IStatus;
    counterparty?: ICounterparty;
    ticket_id: string | null;
    is_parent: boolean;

    finished_at?: string | null;
    closed_at?: string;
    created_at: string;
    duration_time: number;
    jobs_time: number | null;

    start_at?: string;

    inventory: IInventory[];
}

export interface ITicketFilter {
    ticket_number: string;
    ticket_ticket_id?: string | null;

    sale_point_id: string;
    sale_point_name: string;
    sale_point_email: string;
    sale_point_phone: string;
    sale_point_address: string;

    character_problem_id: string;
    character_problem_name: string;

    manager_user_id: string;

    channel_name: string;

    contact_name: string;
    contact_email: string;

    direction_name: string;

    status_name: string;

    phone_number_name: string;

    ticket_created_at_from: string;
    ticket_created_at_to: string;
}

export type TTicket = ITicket & TDict<string | undefined>;

export interface ICurrentTicket extends ITicket {
    case: ICase[];
}

export interface ICreateTicket {
    channel_id?: string;
    character_problem_id?: string;
    contact_id?: string;
    description?: string;
    decision?: string;
    direction_id?: string;
    sale_point_id?: string;
    tag?: IdEntity[];
    is_parent?: boolean;
    ticket_id?: string | null;
    file?: IdEntity[];
    status_id?: string;

    start_at?: string;

    inventory?: IdEntity[];
}

export interface ITicketById {
    ticketId: string;
}

export interface IUpdateTicket extends ICreateTicket {
    file?: IdEntity[];
    finished_at?: string | null;
    jobs_time: number | null;
    job_result: IdEntity[];
}
