import { IdEntity } from '../types';

export interface INotification {
    id: string;
    name: string;
    description?: string;
    model: string;
    link_id?: string;
    read_status: {
        id: string;
        read: boolean;
        user_id?: string | null;
        notification_id?: string | null;
    }[];
    created_at: string;
}

export interface ICreateNotification {
    name: string;
    description?: string;
    model: string;
    read_status: IdEntity[];
}

export type IUpdateNotification = Partial<ICreateNotification>;

export type NotificationFilter = Partial<{
    read_status_user_id: string;
    notification_name: string;
}>;
