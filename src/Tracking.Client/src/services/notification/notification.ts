import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    ICreateNotification,
    INotification,
    NotificationFilter,
} from './types';

export const notificationService = servicerCreator.createService<
    INotification,
    ICreateNotification,
    TPagination,
    NotificationFilter
>({ route: 'notification', searchFields: ['notification_name'] });

export const notificationKeys = notificationService.keys;

export const notificationApi = notificationService.api;

export const notificationHooks = notificationService.hooks;
