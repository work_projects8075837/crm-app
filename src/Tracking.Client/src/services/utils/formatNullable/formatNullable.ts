type IData = {
    [key: string]: any | undefined | null;
};

export const formatNullable = <D = any>(data?: D) => {
    if (!data) {
        return data;
    }
    const newData: IData = { ...data };
    for (const key in data) {
        if (
            !data[key] &&
            data[key] !== false &&
            data[key] !== null &&
            data[key] !== 0
        ) {
            newData[key] = undefined;
        } else if (data[key] === null) {
            newData[key] = null;
        }
    }

    return newData;
};
