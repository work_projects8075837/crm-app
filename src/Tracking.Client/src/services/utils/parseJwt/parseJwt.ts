import { ITokenData } from '@/services/auth/types.ts';
import jwt_decode from 'jwt-decode';

export const parseJwt = (access: string): ITokenData => {
    return jwt_decode<ITokenData>(access);
};
