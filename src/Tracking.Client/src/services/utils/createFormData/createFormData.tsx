type DataT = {
    [key: string]: any | any[];
};

export const createFormData = (data: DataT) => {
    const formData = new FormData();

    for (const key in data) {
        const item = data[key];

        if (item instanceof Array || item instanceof FileList) {
            for (const itemChild of item) {
                formData.append(key, itemChild);
            }
        } else {
            formData.append(key, item);
        }
    }

    return formData;
};
