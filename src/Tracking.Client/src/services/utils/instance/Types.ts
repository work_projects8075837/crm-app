import {
    Axios,
    AxiosDefaults,
    AxiosHeaderValue,
    AxiosRequestConfig,
    AxiosResponse,
    HeadersDefaults,
} from 'axios';

export interface AuthorizeResultI {
    access: string;
}

export interface ServiceRequestConfig<D> extends AxiosRequestConfig<D> {
    isAuth?: boolean;
}

export interface AxiosInstanceWithServiceRequestConfig extends Axios {
    <T = any, R = AxiosResponse<T>, D = any>(
        config: ServiceRequestConfig<D>,
    ): Promise<R>;
    <T = any, R = AxiosResponse<T>, D = any>(
        url: string,
        config?: ServiceRequestConfig<D>,
    ): Promise<R>;

    defaults: Omit<AxiosDefaults, 'headers'> & {
        headers: HeadersDefaults & {
            [key: string]: AxiosHeaderValue;
        };
    };
}
