import { GET_AUTHORIZATION_KEY } from '@/store/queries/keys/auth';
import axios from 'axios';
import { ServiceManager } from './Class';

export const BASE_DOMAIN: string =
    import.meta.env.VITE_BASE_DOMAIN || 'v2.dedibyte.ru';

console.log(BASE_DOMAIN);

const instance = axios.create({
    baseURL: `http${
        BASE_DOMAIN.includes('localhost') ? '' : 's'
    }://${BASE_DOMAIN}/api`,
});

export const serviceManager = new ServiceManager(
    instance,
    '/auth/refresh/',
    GET_AUTHORIZATION_KEY,
);
