import { AxiosResponse } from 'axios';
import { ServiceManager } from '../Class';

export interface ICreateServiceParameters<TEntityFilter, TGetters> {
    readonly route: string;
    searchFields: Array<keyof TEntityFilter>;
    extraEndpoints?: TGetters;
}

export interface IEndpointOptions {
    isAuth?: boolean;
}

export interface IEndpointsOptions {
    getAll: IEndpointOptions;
    create: IEndpointOptions;
    getOne: IEndpointOptions;
    update: IEndpointOptions;
    delete: IEndpointOptions;
}

export type TGetEndpoint = <T, R>(query: T) => Promise<AxiosResponse<R>>;

export type TPostEndpoint = <T, R>(data: T) => Promise<AxiosResponse<R>>;

export type TGetOneEndpoint = <T, R>(
    id: string,
    data: T,
) => Promise<AxiosResponse<R>>;

export type TUpdateEndpoint = <T, R>(
    id: string,
    data: T,
) => Promise<AxiosResponse<R>>;

export type IGetters<T> = {
    [K in keyof T]: (
        route: string,
        serviceManager: ServiceManager,
    ) => <Q>(query: Q) => T[K];
};

export type IPosters<T> = {
    [K in keyof T]: <T, R>(
        route: string,
        serviceManager: ServiceManager,
        data: T,
    ) => Promise<AxiosResponse<R>>;
};

export type IGettersOne<T> = {
    [K in keyof T]: <R>(
        route: string,
        serviceManager: ServiceManager,
        id: string,
    ) => Promise<AxiosResponse<R>>;
};

export type IUpdaters<T> = {
    [K in keyof T]: <T, R>(
        route: string,
        serviceManager: ServiceManager,
        id: string,
        data: T,
    ) => Promise<AxiosResponse<R>>;
};

export interface IExtraEndpoints<G, P, U, O> {
    getters?: IGetters<G>;
    posters?: IPosters<P>;
    updaters?: IUpdaters<U>;
    gettersOne?: IGettersOne<O>;
}

export interface IServicerConstructorOptions {
    endpoints?: Partial<IEndpointsOptions>;
}

export interface IServiceCreator<
    TEntity = any,
    TCreateEntity = any,
    TPag = any,
    TFilter = any,
    TDetailEntity = TEntity,
    TUpdateEntity = Partial<TCreateEntity>,
> {
    entity: TEntity;
    create: TCreateEntity;
    pagination: TPag;
    filter: TFilter;
    detail: TDetailEntity;
    update: TUpdateEntity;
}

export interface IDefaultFilter {
    is_hard: boolean;
    is_hide: boolean;
}
