import { useDebounce } from '@/hooks/useDebounce';
import { PaginatedData } from '@/services/types';
import {
    IOptionsProps,
    useAuthorizedQuery,
} from '@/store/queries/stores/base/useAuthorizedQuery';
import { generateQuery } from '../../generateQuery/generateQuery';
import { ServiceManager } from '../Class';
import { IDefaultFilter, IServicerConstructorOptions } from './types';

export class Servicer {
    constructor(
        protected serviceManager: ServiceManager,
        protected options?: IServicerConstructorOptions,
    ) {}

    private createKeys(route: string) {
        const upperRoute = route.toUpperCase();

        const keys = {
            GET_ALL_KEY: `GET_ALL_${upperRoute}_KEY`,
            GET_ONE_KEY: `GET_ONE_${upperRoute}_KEY`,
        };

        return keys;
    }

    public createBaseService<
        TEntity,
        TCreateEntity,
        TPag = unknown,
        TFilter = unknown,
        TDetailEntity = TEntity,
        TUpdateEntity = Partial<TCreateEntity>,
    >({
        route,
        searchFields,
    }: {
        readonly route: string;
        searchFields: Array<keyof TFilter>;
    }) {
        const serviceManager = this.serviceManager;
        const options = this.options;

        const keys = this.createKeys(route);

        const api = {
            async getAll<T = Partial<TPag & TFilter & IDefaultFilter>>(
                query?: T,
            ) {
                return serviceManager.get<PaginatedData<TEntity[]>>(
                    `/${route}/${generateQuery(query, false)}`,
                    { isAuth: options?.endpoints?.getAll?.isAuth ?? true },
                );
            },

            async create(data: TCreateEntity) {
                return serviceManager.post<TDetailEntity>(`/${route}/`, data, {
                    isAuth: options?.endpoints?.create?.isAuth ?? true,
                });
            },

            async getOne(id: string) {
                return serviceManager.get<TDetailEntity>(`/${route}/${id}/`, {
                    isAuth: options?.endpoints?.getOne?.isAuth ?? true,
                });
            },

            async update(id: string, data: TUpdateEntity) {
                return serviceManager.patch<TDetailEntity>(
                    `/${route}/${id}/`,
                    data,
                    {
                        isAuth: options?.endpoints?.update?.isAuth ?? true,
                    },
                );
            },

            async delete(id: string) {
                return serviceManager.delete(`/${route}/${id}/`, {
                    isAuth: options?.endpoints?.delete?.isAuth ?? true,
                });
            },
        };

        const useListQuery = <R>(
            queryFn: () => Promise<R>,
            query?: Partial<TPag & TFilter & IDefaultFilter>,
            options?: IOptionsProps,
        ) => {
            return useAuthorizedQuery({
                mainKey: keys.GET_ALL_KEY,
                variablesKeys: { ...query },
                ...options,
                queryFn,
            });
        };

        const useOneQuery = <R>(
            queryFn: () => Promise<R>,
            id?: string,
            options?: IOptionsProps,
        ) => {
            return useAuthorizedQuery({
                mainKey: keys.GET_ONE_KEY,
                variablesKeys: { id },
                enabled: !!id,
                ...options,
                queryFn,
            });
        };

        const hooks = {
            useList: (
                query?: Partial<TPag & TFilter & IDefaultFilter>,
                options?: IOptionsProps,
            ) => {
                return useListQuery(
                    async () => {
                        return await api.getAll(query).then((res) => res.data);
                    },
                    query,
                    options,
                );
            },

            useOne: (id?: string, options?: IOptionsProps) => {
                return useOneQuery(
                    async () => {
                        if (!id) {
                            throw Error(
                                `${route.toUpperCase()} - id is undefined. enabled was not provided`,
                            );
                        }
                        return await api.getOne(id).then((res) => res.data);
                    },
                    id,
                    options,
                );
            },

            useSearch: (
                search?: string | null,
                filter?: Partial<TPag & TFilter & IDefaultFilter>,
                options?: IOptionsProps,
            ) => {
                const debouncedSearch = useDebounce(search, 500);

                const searchFilter = searchFields.reduce((filter, field) => {
                    return { ...filter, [field]: debouncedSearch };
                }, {});

                const allFilter = { ...filter, ...searchFilter };

                return useAuthorizedQuery({
                    mainKey: keys.GET_ALL_KEY,
                    variablesKeys: { ...allFilter },
                    ...options,
                    queryFn: async () => {
                        return await api
                            .getAll({
                                is_hard:
                                    Object.keys(allFilter).length > 1
                                        ? false
                                        : true,
                                ...allFilter,
                            })
                            .then((res) => res.data);
                    },
                });
            },
        };

        return {
            api,
            hooks,
            keys,
            utils: { useListQuery, useOneQuery },
        };
    }
}
