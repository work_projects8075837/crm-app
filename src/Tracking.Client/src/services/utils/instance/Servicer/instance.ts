import { serviceManager } from '../ServiceManager';
import { ServiceCreator } from './ServiceCreator';

export const servicerCreator = new ServiceCreator(serviceManager);
