import { ServiceManager } from '../Class';
import { Servicer } from './Servicer';
import { IServicerConstructorOptions } from './types';

export class ServiceCreator extends Servicer {
    constructor(sm: ServiceManager, o?: IServicerConstructorOptions) {
        super(sm, o);
    }

    createService<
        TEntity,
        TCreateEntity,
        TPag = unknown,
        TFilter = unknown,
        TDetailEntity = TEntity,
        TUpdateEntity = Partial<TCreateEntity>,
    >({
        route,
        searchFields,
    }: {
        readonly route: string;
        searchFields: (keyof TFilter)[];
    }) {
        const serviceInstance = this.createBaseService<
            TEntity,
            TCreateEntity,
            TPag,
            TFilter,
            TDetailEntity,
            TUpdateEntity
        >({ route, searchFields });

        const serviceManager = this.serviceManager;

        const service = {
            ...serviceInstance,
            api: {
                ...serviceInstance.api,
                async deleteArray(ids: string[]) {
                    return await serviceManager.post(
                        `/${route}/delete/list/`,
                        { ids },
                        {
                            isAuth: true,
                        },
                    );
                },
                async hideArray(ids: string[]) {
                    return await serviceManager.patch(
                        `/${route}/hide/list/`,
                        { ids },
                        {
                            isAuth: true,
                        },
                    );
                },
                async hide(id: string) {
                    return await serviceManager.patch(
                        `/${route}/hide/${id}/`,
                        undefined,
                        {
                            isAuth: true,
                        },
                    );
                },
            },
        };

        return service;
    }
}
