import { queryClient } from '@/store/queries/client/client';
import { AxiosInstance, AxiosResponse } from 'axios';
import { CookiesStorage } from '../CookieStorage';
import { formatNullable } from '../formatNullable/formatNullable';
import { AuthorizeResultI, ServiceRequestConfig } from './Types';

export class ServiceManager {
    constructor(
        private api: AxiosInstance,
        private authUrl: string,
        private authKey: string,
    ) {}

    public async authorize() {
        const refresh = CookiesStorage.getItem('refresh');
        if (!refresh) {
            this.unauthorize();
            throw Error();
        }

        const access = await this.api
            .post<AuthorizeResultI>(
                this.authUrl,
                {},
                { headers: { Authorization: `Bearer ${refresh}` } },
            )
            .then((response) => response.data.access)
            .catch(async () => {
                this.unauthorize();

                throw Error();
            });

        return access;
    }

    public unauthorize() {
        CookiesStorage.removeItem('refresh');
        CookiesStorage.removeItem('access');
        queryClient.invalidateQueries({
            queryKey: [this.authKey],
        });
    }

    private async setAuthHeaders<D>(config?: ServiceRequestConfig<D>) {
        if (!config?.isAuth) {
            return;
        }

        if (!CookiesStorage.getItem('access')) {
            const access = await this.authorize();
            CookiesStorage.setItem('access', access);
        }
        const access = CookiesStorage.getItem('access');

        if (config.headers) {
            config.headers.Authorization = `Bearer ${access}`;
        } else {
            config.headers = { Authorization: `Bearer ${access}` };
        }
    }

    private async createAuthMiddlewareEndpoint<R, D>(
        axiosPromise: (
            url: string,
            config?: ServiceRequestConfig<D>,
            data?: D,
        ) => Promise<R>,
        url: string,
        config?: ServiceRequestConfig<D>,
        data?: D,
    ): Promise<R> {
        this.setAuthHeaders(config);
        const query = async (
            url: string,
            config?: ServiceRequestConfig<D>,
            data?: D,
        ): Promise<R> => {
            return axiosPromise(url, config, data).catch(async (error) => {
                if (error?.response?.status === 401 && config?.isAuth) {
                    const access = await this.authorize();
                    CookiesStorage.setItem('access', access);
                    const newConfig = {
                        ...config,
                        headers: {
                            ...config?.headers,
                            Authorization: `Bearer ${access}`,
                        },
                    };
                    return query(url, newConfig, data);
                }
                throw error;
            });
        };

        return query(url, config, data);
    }

    public async post<T = any, R = AxiosResponse<T>, D = any>(
        url: string,
        data?: D,
        config?: ServiceRequestConfig<D>,
    ): Promise<R> {
        const customPost = (
            url: string,
            config?: ServiceRequestConfig<D>,
            data?: D,
        ): Promise<R> => {
            return this.api.post(url, formatNullable(data), config);
        };

        return await this.createAuthMiddlewareEndpoint<R, D>(
            customPost,
            url,
            config,
            data,
        );
    }

    public async patch<T = any, R = AxiosResponse<T>, D = any>(
        url: string,
        data?: D,
        config?: ServiceRequestConfig<D>,
    ): Promise<R> {
        const customPost = (
            url: string,
            config?: ServiceRequestConfig<D>,
            data?: D,
        ): Promise<R> => {
            return this.api.patch(url, formatNullable(data), config);
        };

        return await this.createAuthMiddlewareEndpoint<R, D>(
            customPost,
            url,
            config,
            data,
        );
    }

    public async delete<T = any, R = AxiosResponse<T>, D = any>(
        url: string,
        config?: ServiceRequestConfig<D>,
    ): Promise<R> {
        const customDelete = (
            url: string,
            config?: ServiceRequestConfig<D>,
        ): Promise<R> => {
            return this.api.delete(url, config);
        };

        return await this.createAuthMiddlewareEndpoint<R, D>(
            customDelete,
            url,
            config,
        );
    }

    public async get<T = any, R = AxiosResponse<T>, D = any>(
        url: string,
        config?: ServiceRequestConfig<D>,
    ): Promise<R> {
        const customGet = (
            url: string,
            config?: ServiceRequestConfig<D>,
        ): Promise<R> => {
            return this.api.get(url, config);
        };

        return await this.createAuthMiddlewareEndpoint<R, D>(
            customGet,
            url,
            config,
        );
    }
}
