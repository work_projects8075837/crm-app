import { Path } from 'react-hook-form';
type CookieValue = string[];
type Cookie = CookieValue[];
interface IData {
    refresh?: string;
    access?: string;
}

export const CookiesStorage = {
    getCookies() {
        const cookies: Cookie = document.cookie
            .split('; ')
            .map((cookString) => cookString.split('='));
        let data: IData = {};
        cookies.forEach((cookie: CookieValue) => {
            data = { ...data, ...{ [cookie[0]]: cookie[1] } };
        });

        return data;
    },

    getItem(cookieName: keyof IData) {
        const data = this.getCookies();
        return data[cookieName];
    },

    setItem(cookieName: keyof IData, cookieValue: string) {
        const DateNow = new Date();

        const Day = DateNow.getDate();
        const Month = DateNow.getMonth();
        const Year = DateNow.getFullYear();

        const expires: string = new Date(Year, Month, Day + 10).toUTCString();

        document.cookie = `${cookieName}=${cookieValue}; domain=${
            window.location.hostname.split(':')[0]
        }; expires=${expires}; path=/;`;
    },

    removeItem(cookieName: string) {
        const expires: string = new Date().toUTCString();

        document.cookie = `${cookieName}=${null}; domain=${
            window.location.hostname.split(':')[0]
        }; expires=${expires}; path=/;`;
    },

    removeAllItems() {
        const cookieNames = Object.keys(this.getCookies());

        cookieNames.forEach((cookie) => this.removeItem(cookie));
    },
};
