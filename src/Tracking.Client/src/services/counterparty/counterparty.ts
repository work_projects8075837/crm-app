import { IOptionsProps } from '@/store/queries/stores/base/useAuthorizedQuery';
import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    ICounterPartyFilter,
    ICounterparty,
    ICreateCounterParty,
    IUpdateCounterParty,
} from './types';

export const counterPartyService = servicerCreator.createService<
    ICounterparty,
    ICreateCounterParty,
    TPagination,
    ICounterPartyFilter,
    ICounterparty,
    IUpdateCounterParty
>({ route: 'counterparty', searchFields: ['counterparty_name'] });

export const counterPartyKeys = counterPartyService.keys;

export const counterPartyApi = counterPartyService.api;

export const counterPartyHooks = {
    ...counterPartyService.hooks,
    useOne: (id?: string, options?: IOptionsProps) => {
        return counterPartyService.utils.useOneQuery(
            async () => {
                if (!id) {
                    throw Error();
                }
                const counterParty = await counterPartyApi
                    .getOne(id)
                    .then((res) => res.data);

                return {
                    ...counterParty,
                    counter_party: counterParty.counterparty_id
                        ? await counterPartyApi
                              .getOne(counterParty.counterparty_id)
                              .then((res) => res.data)
                        : null,
                };
            },
            id,
            options,
        );
    },
};
