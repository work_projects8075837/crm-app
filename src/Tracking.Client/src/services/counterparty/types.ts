import { IFile } from '../file/types';
import { IdEntity, TDict } from '../types';

export interface ICounterparty {
    comment: string;
    file: IFile[];
    id: string;
    name: string;
    tag: string;
    tin: string;
    counterparty_id?: string | null;
    is_parent: boolean;
}

export interface ICounterPartyById {
    counterPartyId: string;
}

export interface ICreateCounterParty {
    name: string;
    comment?: string;
    file?: IdEntity[];
    tag: string;
    tin: string;
    is_parent?: boolean;
    counterparty_id?: string | null;
}

export type IUpdateCounterParty = Partial<ICreateCounterParty>;

export type TCounterParty = ICounterparty & TDict;

export interface ICounterPartyFilter {
    counterparty_name: string;
    counterparty_counterparty_id: string | null;
}
