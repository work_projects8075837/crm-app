import { IUser } from '../user/types';

export interface ILogin {
    email: string;
    password: string;
}

export interface ILoginResult {
    access: string;
    refresh: string;
}

export interface ITokenData {
    sub: string;
    exp: number;
    roles?: string[];
}

export interface IToken {
    access: string;
    refresh: string;
    expires: string;
    role: string;
}

export interface IUserRegistration {
    name: string;
    email: string;
    password: string;
}

export interface IRecovery {
    email: string;
}

export interface IResetPassword {
    password: string;
    token: string;
}

export interface ISetUserStatus {
    status: 'free' | 'work' | 'rest' | 'out';
}

export interface IUpdateUserFields
    extends Omit<IUser, 'channel' | 'direction'> {
    channel_id: string | null;
    direction_id: string | null;
}

export type IUpdateUser = Partial<IUpdateUserFields>;
