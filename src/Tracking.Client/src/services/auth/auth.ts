import { IUser } from '../user/types';
import { serviceManager } from '../utils/instance/ServiceManager';
import {
    ILogin,
    ILoginResult,
    IRecovery,
    IResetPassword,
    ISetUserStatus,
    IUpdateUser,
    IUserRegistration,
} from './types.ts';

class AuthService {
    async login({ email, password }: ILogin) {
        const res = await serviceManager.post<ILoginResult>('/auth/login/', {
            email,
            password,
        });
        return res;
    }

    async registration({ name, email, password }: IUserRegistration) {
        return await serviceManager.post('/auth/signup/', {
            name,
            email,
            password,
        });
    }

    async recoveryPassword({ email }: IRecovery) {
        return await serviceManager.post('/auth/recovery-password/', {
            email,
        });
    }

    async resetPassword({ token, password }: IResetPassword) {
        return await serviceManager.post('/auth/reset-password/', {
            token,
            password,
        });
    }

    async setUserStatus({ status }: ISetUserStatus) {
        return await serviceManager.patch<IUser>(
            '/auth/me/',
            {
                status,
            },
            { isAuth: true },
        );
    }

    async update(data: IUpdateUser) {
        return await serviceManager.patch<IUser>('/auth/me/', data, {
            isAuth: true,
        });
    }
}

export const authService = new AuthService();
