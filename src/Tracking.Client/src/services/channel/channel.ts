import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { PaginatedData } from '../types';
import { generateQuery } from '../utils/generateQuery/generateQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    IChannel,
    IChannelFilter,
    ICreateChannel,
    IUpdateChannel,
} from './types';

class ChannelService {
    async getAll(pagination?: TPagination) {
        return await serviceManager.get<PaginatedData<IChannel[]>>(
            `/channel/${generateQuery(pagination)}`,
            { isAuth: true },
        );
    }

    async getOneById(id: string) {
        return await serviceManager.get<IChannel>(`/channel/${id}/`, {
            isAuth: true,
        });
    }

    async getSearch(search?: string) {
        return await serviceManager.get<PaginatedData<IChannel[]>>(
            `/channel/${generateQuery({ channel_name: search })}`,
            { isAuth: true },
        );
    }

    async create(data: ICreateChannel) {
        return await serviceManager.post<IChannel>(`/channel/`, data, {
            isAuth: true,
        });
    }

    async update(id: string, data: IUpdateChannel) {
        return await serviceManager.patch<IChannel>(`/channel/${id}/`, data, {
            isAuth: true,
        });
    }

    async deleteArray(ids: string[]) {
        return await serviceManager.post(
            `/channel/delete/list/`,
            { ids },
            {
                isAuth: true,
            },
        );
    }
}

export const channelService = servicerCreator.createService<
    IChannel,
    ICreateChannel,
    TPagination,
    IChannelFilter,
    IChannel,
    IUpdateChannel
>({ route: 'channel', searchFields: ['channel_name'] });

export const channelKeys = channelService.keys;

export const channelApi = channelService.api;

export const channelHooks = channelService.hooks;
