export interface IChannel {
    id: string;
    multiple: number;
    name: string;
}

export interface ICreateChannel {
    name: string;
    multiple: number;
}

export type IUpdateChannel = Partial<ICreateChannel>;

export interface IChannelFilter {
    channel_name: string;
}
