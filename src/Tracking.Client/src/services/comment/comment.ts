import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { PaginatedData } from '../types';
import { generateQuery } from '../utils/generateQuery/generateQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from '../utils/instance/Servicer/instance';
import {
    IComment,
    ICommentFilter,
    ICreateComment,
    IUpdateComment,
} from './types';

class CommentService {
    async getAll(data?: TPagination & ICommentFilter) {
        return await serviceManager.get<PaginatedData<IComment[]>>(
            `/comment/${generateQuery(data)}`,
            {
                isAuth: true,
            },
        );
    }

    async getSearch(search?: string) {
        return await serviceManager.get<PaginatedData<IComment[]>>(
            `/comment/${generateQuery({
                comment_name: search,
            })}`,
            {
                isAuth: true,
            },
        );
    }

    async getOne(id: string) {
        return await serviceManager.get<IComment>(`/comment/${id}/`, {
            isAuth: true,
        });
    }

    async create(data: ICreateComment) {
        return await serviceManager.post<IComment>('/comment/', data, {
            isAuth: true,
        });
    }

    async update(id: string, data: IUpdateComment) {
        return await serviceManager.patch<IComment>(`/comment/${id}/`, data, {
            isAuth: true,
        });
    }

    async delete(id: string) {
        return await serviceManager.delete<IComment>(`/comment/${id}/`, {
            isAuth: true,
        });
    }

    async deleteArray(ids: string[]) {
        return await serviceManager.post(
            `/comment/delete/list/`,
            { ids },
            {
                isAuth: true,
            },
        );
    }
}

export const commentService = servicerCreator.createService<
    IComment,
    ICreateComment,
    TPagination,
    ICommentFilter,
    IComment,
    IUpdateComment
>({ route: 'comment', searchFields: ['comment_name'] });

export const commentKeys = commentService.keys;

export const commentApi = commentService.api;

export const commentHooks = commentService.hooks;
