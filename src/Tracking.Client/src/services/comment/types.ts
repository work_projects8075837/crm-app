import { IdEntity } from '../types';
import { IUser } from '../user/types';

export interface IComment {
    id: string;
    description: string;
    date_create: string;
    date_update: string;
    user: IUser;
}

export type TComment = IComment;

export interface ICreateComment {
    description: string;
    user_id: string;
    ticket: IdEntity[];
}

export type IUpdateComment = Partial<ICreateComment>;

export type ICommentFilter = Partial<{
    ticket_id: string;
    comment_name: string;
}>;
