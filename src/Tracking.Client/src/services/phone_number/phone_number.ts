import { TPagination } from '@/store/queries/stores/base/usePaginationQuery';
import { PaginatedData } from '../types';
import { generateQuery } from '../utils/generateQuery/generateQuery';
import { serviceManager } from '../utils/instance/ServiceManager';
import { servicerCreator } from './../utils/instance/Servicer/instance';
import { ICreatePhoneNumber, IPhoneNumber, IPhoneNumberFilter } from './types';

class PhoneNumberService {
    async getSearch(search?: string) {
        return await serviceManager.get<PaginatedData<IPhoneNumber[]>>(
            `/phone_number/${generateQuery({
                phone_number_name: search?.replace('+', ''),
            })}`,
            {
                isAuth: true,
            },
        );
    }

    async create(data: ICreatePhoneNumber) {
        return await serviceManager.post<IPhoneNumber>(`/phone_number/`, data, {
            isAuth: true,
        });
    }
}

export const phoneNumberService = servicerCreator.createService<
    IPhoneNumber,
    ICreatePhoneNumber,
    TPagination,
    IPhoneNumberFilter
>({ route: 'phone_number', searchFields: ['phone_number_name'] });

export const phoneNumberKeys = phoneNumberService.keys;

export const phoneNumberApi = phoneNumberService.api;

export const phoneNumberHooks = phoneNumberService.hooks;
