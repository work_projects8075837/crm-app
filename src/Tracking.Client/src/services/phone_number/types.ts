export interface IPhoneNumber {
    id: string;
    name: string;
}

export type ICreatePhoneNumber = Omit<IPhoneNumber, 'id'>;

export interface IPhoneNumberFilter {
    phone_number_name: string;
}
