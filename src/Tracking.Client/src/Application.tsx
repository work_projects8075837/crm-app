import { Route, Routes } from 'react-router-dom';
import Channels from './components/Channels/Channels';
import CharacterProblemCreate from './components/CharacterProblems/CharacterProblemCreate/CharacterProblemCreate';
import CharacterProblemCreateFromDirectory from './components/CharacterProblems/CharacterProblemCreate/CharacterProblemCreateFromDirectory';
import CharacterProblemDirectory from './components/CharacterProblems/CharacterProblemDirectory/CharacterProblemDirectory';
import CharacterProblemEdit from './components/CharacterProblems/CharacterProblemEdit/CharacterProblemEdit';
import CharacterProblemJobsPage from './components/CharacterProblems/CharacterProblemJobsPage/CharacterProblemJobsPage';
import CharacterProblemsPage from './components/CharacterProblems/CharacterProblemsPage/CharacterProblemsPage';
import Contacts from './components/Contacts/Contacts';
import CounterParties from './components/CounterParty/CounterParties';
import CounterPartyCreate from './components/CounterParty/CounterPartyCreate/CounterPartyCreate';
import CounterPartyCreateFromDirectory from './components/CounterParty/CounterPartyCreate/CounterPartyCreateFromDirectory';
import CounterPartyDirectory from './components/CounterParty/CounterPartyDirectory/CounterPartyDirectory';
import CounterPartyEdit from './components/CounterParty/CounterPartyEdit/CounterPartyEdit';
import DirectionCreate from './components/Directions/DirectionCreate/DirectionCreate';
import DirectionEdit from './components/Directions/DirectionEdit/DirectionEdit';
import Directions from './components/Directions/Directions';
import Equipments from './components/Equipments/Equipments';
import EquipmentsDirectory from './components/Equipments/EquipmentsDirectory';
import Jobs from './components/Jobs/Jobs';
import JobsDirectory from './components/Jobs/JobsDirectory/JobsDirectory';
import Modules from './components/Modules/Modules';
import AuthProvider, {
    ADMIN_ROLE,
} from './components/Providers/AuthGuard/AuthProvider';
import ConnectionsDirectory from './components/SalePoints/SalePointConnections/ConnectionsDirectory/ConnectionsDirectory';
import SalePointConnections from './components/SalePoints/SalePointConnections/SalePointConnections';
import SalePointEdit from './components/SalePoints/SalePointEdit/SalePointEdit';
import InventoryCreate from './components/SalePoints/SalePointEditInventory/InventoryCreate/InventoryCreate';
import InventoryDirectoryCreate from './components/SalePoints/SalePointEditInventory/InventoryCreate/InventoryDirectoryCreate';
import InventoryDirectory from './components/SalePoints/SalePointEditInventory/InventoryDirectory/InventoryDirectory';
import InventoryEdit from './components/SalePoints/SalePointEditInventory/InventoryEdit/InventoryEdit';
import SalePointEditInventory from './components/SalePoints/SalePointEditInventory/SalePointEditInventory';
import SalePointRegulations from './components/SalePoints/SalePointRegulations/SalePointRegulations';
import SalePoints from './components/SalePoints/SalePoints';
import SalePointsCreate from './components/SalePoints/SalePointsCreate/SalePointsCreate';
import SalePointsCreateFromDirectory from './components/SalePoints/SalePointsCreate/SalePointsCreateFromDirectory';
import SalePointsDirectory from './components/SalePoints/SalePointsDirectory/SalePointsDirectory';
import TicketEditComments from './components/Tickets/Comments/TicketEditComments';
import TicketCreate from './components/Tickets/TicketCreate/TicketCreate';
import TicketCreateChild from './components/Tickets/TicketCreateChild';
import TicketEdit from './components/Tickets/TicketEdit/TicketEdit';
import TicketEditJobs from './components/Tickets/TicketEdit/TicketEditJobs/TicketEditJobs';
import TicketEditSubTickets from './components/Tickets/TicketEdit/TicketEditSubTickets/TicketEditSubTickets';
import TicketJobResults from './components/Tickets/TicketJobResults/TicketJobResults';
import UserProfile from './components/UserProfile/UserProfile';
import Login from './pages/login/Login';
import Recovery from './pages/recovery/Recovery';
import Registration from './pages/registration/Registration';
import Reset from './pages/reset/Reset';
import Task from './pages/task/Tickets';

export default function Application() {
    // const [isInitialized, setIsInitialized] = useState(false);
    // const [error, setError] = useState<string | null>(null);

    // useEffect(() => {
    //     if (!isInitialized) {
    //         ConfigurationManager.GetInstance()
    //             .fetch()
    //             .then(() => {
    //                 setIsInitialized(true);
    //                 serviceManager.api.baseUrl =
    //                     ConfigurationManager.GetInstance().getItem(
    //                         'BASE_URL',
    //                         'BASE_URL',
    //                     );
    //             })
    //             .catch(() => setError('Ошибка загрузки конфигурации'));
    //     }
    // }, [isInitialized]);

    // if (!isInitialized) {
    //     return <p>{error}</p>;
    // }

    return (
        <Routes>
            <Route path={'/login'} element={<Login />} />
            <Route path={'/registration'} element={<Registration />} />
            <Route path={'/recovery'} element={<Recovery />} />
            <Route path={'/reset'} element={<Reset />} />

            <Route
                path={'/'}
                element={
                    <AuthProvider>
                        <Task />
                    </AuthProvider>
                }
            />
            <Route
                path={'/ticket/create/'}
                element={
                    <>
                        <AuthProvider>
                            <TicketCreate />
                        </AuthProvider>
                    </>
                }
            />
            <Route
                path={'/ticket/:ticketId'}
                element={
                    <>
                        <AuthProvider>
                            <TicketEdit />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/ticket/:ticketId/jobs'}
                element={
                    <>
                        <AuthProvider>
                            <TicketEditJobs />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/ticket/:ticketId/comments'}
                element={
                    <>
                        <AuthProvider>
                            <TicketEditComments />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/ticket/:ticketId/results'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <TicketJobResults />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/ticket/:ticketId/sub_tickets'}
                element={
                    <>
                        <AuthProvider>
                            <TicketEditSubTickets />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/ticket/:ticketId/sub_tickets/create'}
                element={
                    <>
                        <AuthProvider>
                            <TicketCreateChild />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/contact'}
                element={
                    <>
                        <AuthProvider>
                            <Contacts />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/contact/:contactId'}
                element={
                    <>
                        <AuthProvider>
                            <Contacts />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point'}
                element={
                    <>
                        <AuthProvider>
                            <SalePoints />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/create'}
                element={
                    <>
                        <AuthProvider>
                            <SalePointsCreate />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/create/:salePointId'}
                element={
                    <>
                        <AuthProvider>
                            <SalePointsCreateFromDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/:salePointId'}
                element={
                    <>
                        <AuthProvider>
                            <SalePointsDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/single/:salePointId'}
                element={
                    <>
                        <AuthProvider>
                            <SalePointEdit />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/single/:salePointId/regulations'}
                element={
                    <>
                        <AuthProvider>
                            <SalePointRegulations />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/single/:salePointId/inventory'}
                element={
                    <>
                        <AuthProvider>
                            <SalePointEditInventory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/single/:salePointId/connections'}
                element={
                    <>
                        <AuthProvider>
                            <SalePointConnections />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/single/:salePointId/inventory/:inventoryId'}
                element={
                    <>
                        <AuthProvider>
                            <InventoryDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={
                    '/sale_point/single/:salePointId/connections/:inventoryId'
                }
                element={
                    <>
                        <AuthProvider>
                            <ConnectionsDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={
                    '/sale_point/single/:salePointId/inventory/single/:inventoryId'
                }
                element={
                    <>
                        <AuthProvider>
                            <InventoryEdit />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/sale_point/single/:salePointId/inventory/create'}
                element={
                    <>
                        <AuthProvider>
                            <InventoryCreate />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={
                    '/sale_point/single/:salePointId/inventory/create/:inventoryId'
                }
                element={
                    <>
                        <AuthProvider>
                            <InventoryDirectoryCreate />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/character_problem'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <CharacterProblemsPage />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/character_problem/:characterProblemId'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <CharacterProblemDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/character_problem/create'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <CharacterProblemCreate />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/character_problem/create/:characterProblemId'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <CharacterProblemCreateFromDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/character_problem/single/:characterProblemId'}
                element={
                    <>
                        <AuthProvider>
                            <CharacterProblemEdit />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/character_problem/single/:characterProblemId/jobs'}
                element={
                    <>
                        <AuthProvider>
                            <CharacterProblemJobsPage />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/direction'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <Directions />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/direction/create'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <DirectionCreate />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/direction/single/:directionId'}
                element={
                    <>
                        <AuthProvider>
                            <DirectionEdit />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/counterparty'}
                element={
                    <>
                        <AuthProvider>
                            <CounterParties />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/counterparty/:counterPartyId'}
                element={
                    <>
                        <AuthProvider>
                            <CounterPartyDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/counterparty/create'}
                element={
                    <>
                        <AuthProvider>
                            <CounterPartyCreate />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/counterparty/create/:counterPartyId'}
                element={
                    <>
                        <AuthProvider>
                            <CounterPartyCreateFromDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/counterparty/single/:counterPartyId'}
                element={
                    <>
                        <AuthProvider>
                            <CounterPartyEdit />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/channel'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <Channels />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/channel/:channelId'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <Channels />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/jobs'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <Jobs />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/jobs/:jobId'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <JobsDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/jobs/single'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <Jobs />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/inventory'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <Equipments />
                        </AuthProvider>
                    </>
                }
            />
            <Route
                path={'/inventory/:inventoryId'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <EquipmentsDirectory />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/inventory/create'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <InventoryCreate />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/inventory/create/:inventoryId'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <InventoryDirectoryCreate />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/inventory/single/:inventoryId'}
                element={
                    <>
                        <AuthProvider>
                            <InventoryEdit />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/module'}
                element={
                    <>
                        <AuthProvider allowedRoles={[ADMIN_ROLE]}>
                            <Modules />
                        </AuthProvider>
                    </>
                }
            />

            <Route
                path={'/settings'}
                element={
                    <>
                        <AuthProvider>
                            <UserProfile />
                        </AuthProvider>
                    </>
                }
            />
        </Routes>
    );
}
