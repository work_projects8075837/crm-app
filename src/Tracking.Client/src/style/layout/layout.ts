import StyleHeader from '../layout/header.module.scss';
import StyleModalTable from './modal_table.module.scss';
import StyleTable from './table.module.scss';
import StyleTicket from './ticket.module.scss';

export { StyleHeader, StyleModalTable, StyleTable, StyleTicket };
