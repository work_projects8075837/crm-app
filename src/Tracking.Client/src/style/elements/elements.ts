import StyleBlueButton from './blue_button.module.scss';
import StyleBreadcrumb from './breadcrumb.module.scss';
import StyleLoader from './loader_text.module.scss';
import StyleTab from './tab.module.scss';

export { StyleBlueButton, StyleBreadcrumb, StyleLoader, StyleTab };
