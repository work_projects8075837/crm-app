import react from '@vitejs/plugin-react';
import path from 'path';
import { defineConfig } from 'vite';
import tsconfigPaths from 'vite-tsconfig-paths';

// https://vitejs.dev/config/
export default defineConfig({
    build: {
        chunkSizeWarningLimit: 1600,
    },
    resolve: {
        alias: { '@': path.resolve(__dirname, './src') },
    },
    plugins: [react(), tsconfigPaths()],
    define: {
        VITE_BASE_DOMAIN: process.env.VITE_BASE_DOMAIN,
    },
});
